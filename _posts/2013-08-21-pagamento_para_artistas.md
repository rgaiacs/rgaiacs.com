---
layout: post
title: "Pagamento para artistas"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Nessa semana Rick Falkvinge (o fundador do Partido Pirata) escreveu no TorrentFreak sobre o pagamento para artistas. O artigo encontra-se disponível <a href="http://torrentfreak.com/how-shall-the-artists-get-paid-isnt-a-question-its-an-insult-130818/">aqui</a> (<span data-role="download">backup nesse servidor &lt;artists-get-paid.html&gt;</span>).</p>
<div class="more">

</div>
<p>Falkvinge afirma, logo no título, que a pergunta &quot;How should artists get paid?&quot; (&quot;Como os artistas serão pagos/remunerados?&quot; em tradução literal) é um insulto a todos os artistas.</p>
<p>Para sustentar seu ponto de vista ele afirma que os artistas são remunerados de outras formas que não pela venda direta da cópia de sua produção e também que o lucro de músicos aumentou 114% depois que as pessoas passaram a compartilhar conteúdo em larga escala pela internet.</p>
<p>O segundo ponto levantado por Falkvinge é uma distinção entre a produção artística e o modelo de negócio baseado na venda de cópias da produção artística promovido pelas gravadoras. Se alguém deseja apreciar a produção de algum artista esse alguém irá arrumar uma maneira de fazê-la (seja indo em um show, comprando a produção ou baixando, mesmo que ilegalmente, da internet) e o artista não possui responsabilidade nenhuma se as gravadoras estão falindo devido as baixas vendas (vivemos em uma economia de mercado).</p>
<div class="note">
<div class="admonition-title">
<p>Note</p>
</div>
<p>Os pontos abaixo são mais radicais que os anteriores.</p>
</div>
<p>Além dos pontos anteriores, Falkvinge também menciona:</p>
<ul>
<li>O controle sobre o que as pessoas compartilham entre si não pode ser atendido sem a quebra do segredo de correspondência e monitoramento de toda forma de comunicação o que violaria liberdades humanas fundamentais que sempre devem ser favorecidas contrar o lucro de qualquer um.</li>
<li>Nenhum artista faz (ou deveria fazer) sua arte pelo dinheiro.</li>
</ul>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>copyright,compartilhamento</p>
</div>
<div class="tags">
<p>torrent</p>
</div>
<div class="comments">

</div>
