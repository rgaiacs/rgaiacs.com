---
layout: post
title: "Ponta Negra"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<div class="note">
<div class="admonition-title">
<p>Note</p>
</div>
<p>Este post decorreu <a href="http://olhapravoce.blogspot.com/2013/09/um-paraiso-chamado-maresias.html">desse post</a> (<span data-role="download">cópia nesse servidor &lt;um-paraiso-chamado-maresias.html&gt;</span>).</p>
</div>
<p>Eu não sou muito fã de praia (que é o nome que damos para a mistura de aréia, água, sal e sol). Mas como fui a trabalho para Ponta Negra (uma das praias de Natal - RN) precisava ao menos fazer uma caminhada na praia para ter o que contar aos amigos.</p>
<div class="more">

</div>
<p>De qualquer lugar próximo a praia podemos ver o Morro do Careca que é a duna na foto abaixo.</p>
<figure>
<img src="/images/DSC05237.JPG" class="align-center" style="width:80.0%" />
</figure>
<p>Já na praia/calçadão temos uma melhor visão do Morro do Careca.</p>
<figure>
<img src="/images/DSC05247.JPG" class="align-center" style="width:80.0%" />
</figure>
<p>Na minha opinião, a melhor foto que fiz foi essa que encontra-se abaixo.</p>
<figure>
<img src="/images/DSC05248.JPG" class="align-center" style="width:80.0%" />
</figure>
<p>E finalmente uma foto da praia pois o Morro do Careca não pode ser mais visitado para preservação ecológica.</p>
<figure>
<img src="/images/DSC05250.JPG" class="align-center" style="width:80.0%" />
</figure>
<p>Um lugar que gostei de conhecer foi o forte de Natal. Foi a primeira construção de Natal e um local importante para a história tanto de Natal como do Brasil (lembra da invasão holandesa?).</p>
<figure>
<img src="/images/DSC05298.JPG" class="align-center" style="width:80.0%" />
</figure>
<p>Minha passagem em Natal terminou assistindo parte do por do sol no forte.</p>
<figure>
<img src="/images/DSC05300.JPG" class="align-center" style="width:80.0%" />
</figure>
<div class="note">
<div class="admonition-title">
<p>Note</p>
</div>
<p>Achei o nascer do sol muito mais bonito. Fiz um vídeo mas ainda preciso editá-lo.</p>
</div>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>cidades</p>
</div>
<div class="tags">
<p>Natal</p>
</div>
<div class="comments">

</div>
