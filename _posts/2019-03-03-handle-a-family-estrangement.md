---
layout: post
title: "Handle a family estrangement"
author: "Raniere Silva"
categories: blog
tags: []
image:
  feature: 2019-03-03-handle-a-family-estrangement.jpg
  author: Marco Verch
  licence: CC-BY
  original_url: https://flic.kr/p/RgaZTU
---

Around last Christmas,
[Anna Goldfarb](https://AnnaGoldfarb.com)'s
article "[The Best Way to Handle a Family Estrangement](https://medium.com/s/story/the-best-way-to-handle-a-family-estrangement-9709b87be669)"
([archive PDF avaiable](/pdf/handle-a-family-estrangement.pdf))
found my RSS reader.
This was the same time that I was processing the wish of my parents to not spend Christmas with me in the UK.

Anna's says in her article:

> when someone else is setting those boundaries,
> it’s crucial to the future of your relationship that you respect them.
> Someone who has been cut off
> "may continue to send messages, cards and presents, or criticism when they know they are unwanted,"
> [Becca] Bland [(chief executive of Stand Alone, a UK-based charity that supports adults who are estranged from their families)] says,
> but "research suggests this is more harmful than helpful if the other person has asked for time and space."

and I learnt the hard way how much this advice is gold.