---
layout: post
title: "Paranoia"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p><a href="http://www.imdb.com/title/tt1413495/">Paranoia</a> é o nome de um filme a ser lançado em outubro desse ano e também o nome que <a href="http://www.imdb.com/title/tt0486822/">Disturbia</a> recebeu aqui no Brasil. Mas esse post é sobre o artigo &quot;<a href="http://mediagoblin.org/news/paranoia-optimization.html">Paranoia Optimization for Our Modern Times</a>&quot; (<span data-role="download">cópia
nesse servidor &lt;paranoia-optimization.html&gt;</span>) de Deborah Nicholson que tive a oportunidade de conhecer no FISL14.</p>
<div class="more">

</div>
<p>Deb inicia falando de sua infância, nos anos 80, em que resolveu aprender a ser paranoica depois de assistir alguns filmes sobre espiões e acreditar na possibilidade de estar sendo vigiada. Depois dessa introdução, ela afirma que hoje os usuários da internet estão sendo constantemente vigiados (se não acredita nisso, procure por <a href="http://pt.wikipedia.org/wiki/PRISM_(programa_de_vigil%C3%A2ncia)">Prism</a>) e que o único motivo de não estarmos sofrendo as consequências de nossos atos ilícitos é &quot;sorte&quot; que uma hora ou outra irá acabar e por isso precisamos ser paranoicos.</p>
<p>Infelizmente paranoia apenas não basta pois ela leva a conclusão que <a href="http://en.wikipedia.org/wiki/WarGames">Joshua</a> chegou, i.e., &quot;that the only winning move is not to play&quot; (&quot;que o único movimento para vencer é não jogar&quot; em tradução literal). Precisamos ser paranoicos da maneira correta que é adotarmos uma estratégia de longo termo que consiste em construir alternativas altamente descentralizadas que as pessoas queiram utilizar no lugar dos serviços centralizados utilizados pela grande maioria hoje como Facebook, Flickr, Google Docs.</p>
<p>Serviços altamente descentralizados são aqueles que o usuário é capaz de utilizá-lo totalmente desconectado da internet e que também possui a capacidade de comunicar-se via internet com inúmeros outros servidores. Esses serviços são uma solução porque quanto maior o número de servidores mais difícil para uma pessoa ou entidade conseguir recolher informações de um mesmo indivíduo.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>internet,liberdade,privacidade</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
