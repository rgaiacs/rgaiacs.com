---
layout: post
title: "Singapore"
author: "Raniere Silva"
categories: travel
tags: ["Asia", "Singapore"]
image:
  feature: 2019-06-02-singapore.jpg
  author:  Raniere Silva
  licence: CC-BY
---

I went to Singapore to visit my best friend, Eric,
and attend his wedding (no photos of the wedding here to protect his privacy).

{% include figure.html filename="2019-06-02-singapore-2.jpg" alternative_text="Photo of Singapore" caption="Esplanade." author="Raniere Silva" licence="CC-BY" %}

{% include figure.html filename="2019-06-02-singapore-3.jpg" alternative_text="Photo of Singapore" caption="Singapore Flyer and Gardens in the Bay." author="Raniere Silva" licence="CC-BY" %}

{% include figure.html filename="2019-06-02-singapore-4.jpg" alternative_text="Statue of Chopin" caption="Statue of Chopin in the Botanic Garden." author="Raniere Silva" licence="CC-BY" %}

{% include figure.html filename="2019-06-02-singapore-5.jpg" alternative_text="Lake" caption="Lake in the Botanic Garden." author="Raniere Silva" licence="CC-BY" %}

{% include figure.html filename="2019-06-02-singapore-6.jpg" alternative_text="Raniere and Merlion" caption="Merlion and I." author="Raniere Silva" licence="CC-BY" %}

Thanks to Majella for the last photo. She told me that everyone that visit Singapore need a photo like that.