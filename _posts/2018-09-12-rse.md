---
layout: post
title: "O encontro de engenharia, pesquisa e software"
author: "Raniere Silva"
categories: blog
tags: [Research Software Engineer, talk]
image:
  feature: 2018-09-12-rse.jpg
  author: Raniere Silva
  licence: CC-BY
---

Hoje eu falei sobre Research Software Engineer na Universidade Federal do Ceará.

## Resumo

No mercado de trabalho atual, indivíduos com experiência em engenharia, pesquisa e software são bastante valiosos mas nem sempre formalmente valorisados. Essa palestra irá cobrir a ascensão da
carreira em Research Software Engineers nos últimos anos que iniciou-se no Reino Unido e em menos de 5 anos expandiu-se para Estados Unidos, Canadá, Alemanha, Holanda, Africa do Sul, Nova Zelandia e
Brasil. Research Software Engineers foi o tema de uma conferência na Universidade de Birmingham, Reino Unido nos dias 3 e 4 de Setembro de 2018 que contou mais de 300 participantes.

## Apresentação

Na semana passada,
fui encontrar com uma amiga no laboratório dela para depois fazermos um lanche
e, quando cheguei no laboratório,
a aluna de iniciação científica dela estava salvando as últimas imagens
obtidas através de uma ponta metálica.

Estavamos na UNICAMP mas esse evento poderia ter acontecido na UFC, no LNLC, no CERN.
O que me chamou a atenção foi que estamos em 2018
e o computador que essa aluna estava usando rodava Windows 95,
que vez ou outra apresenta ao usuário a famosa tela azul da morte.
A interface do programe que a aluna usava era cinza,
não salvava automaticamente as imagens
e backup nas núvens é algo fora do vocabulário da época.
Eu perguntei para minha amiga o motivo que estavam usando Windows 95
e a resposta dela foi que a versão do software que o grupo dela tinha comprado
apenas rodava naquela versão do Windows.
Estou contando isso ilustrar que não é de hoje que engenheiros e pesquisadores
dependem de software para realizarem o seu trabalho.
Mas nesse mesmo período,
software passou a fazer parte integral do nosso dia-a-dia.

Mês passado,
eu estava lendo o livro "The One Device" de Brian Merchant,
um ótimo livro que recomendo fortemente a leitura,
e gostaria de ler parte dele para vocês em uma tradução livre.

> Em 9 de Janeiro de 2007, Steve Jobs entrou no palco da Macworld vestindo
> seu famoso uniforme de camisa preta, jeans azul e sapatos brancos.
> Depois de 20 minutos do seu pronunciamento anual,
> Jobs parou como se para refazer seu pensamento.
>
> "Uma vez ou outra,
> um produto revolucionário aparece no mercado e muda tudo." ele disse.
> "Bem, hoje, estamos introduzindo três produtos revolucionários dessa categoria.
> O primeiro é um iPod com tela wide-screen e controle touch.
> O segundo é um celular revolucionário.
> E o terceiro é um dispositivo de comunicação via internet como nunca visto antes.
> Um iPod, um celular, e um dispositivo de comunicação via internet ...
>
> "Você entendeu? Não existe três dispositivos separados,
> esse é um dispositivo, e chamaremos ele de iPhone.
>
> "Hoje," ele adicionou, "Apple irá reinventar o telefone."
>
> E ela fez.

Brian conta que o ecosistema do iPhone em 2007 quando o iPhone foi lançado é muito diferente do que conhecemos hoje.
Por exemplo, não existia AppleStore.
Mas o mundo nunca mais seria o mesmo.

Na mesma época,
em um palco sem muitos holofotes,
uma profissâo revolucionária surgiu.
Assim como o iPhone colocou três dispositivos dentro de um,
essa nova profissão agregou habilidades de três outras:
engenheiro, pesquisador e programador de computadores.
E o nome dela é Research Software Engineer.
