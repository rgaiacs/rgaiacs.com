---
layout: post
title: Conhecendo o Firefox OS
author: "Raniere Silva"
categories: ftb
tags: [ftb]
image:
  feature: ftb.jpg
---
[Firefox OS](http://www.mozilla.org/en-US/firefoxos/) é o nome do
projeto da Mozilla para um sistema operacional móvel baseado em HTML5,
CSS3 e Javascript.

As vendas de aparelhos com o Firefox OS ainda não iniciaram mas semana
passada foi anunciado um dispositivo a ser vendido (provavelmente ainda
em fevereiro) para desenvolvedores. \[GP13\]\_

Hoje, a forma mais fácil de testar o Firefox OS é por meio do simulador
disponibilizado como um add-on para o Firefox (navegador) \[FS\]\_.
Versões de desenvolvimento do simulador estão disponíveis em
<ftp://ftp.mozilla.org/pub/labs/r2d2b2g/>. (O download do simulador pode
demorar vários minutos devido ao tamanho deste, \~100MB.)

Galeria de Imagens
==================

Na figura abaixo temos a tela travada (esquerda), tela inicial (centro)
e lista de aplicativos (direita) do Firefox OS.

![](/images/ff_home.png)

Várias das funcionalidades de um smartphone já estão prontas como você
pode ver pela lista de aplicativos na figura acima e também pelas
imagens abaixo de alguns aplicativos específicos.

![](/images/ff_dial.png)

![Tela mostrando a função de agenda de contatos do Firefox
OS.](ff_contact.png)

![Tela mostrando a função de galeria de fotos do Firefox
OS.](ff_gallery.png)

![Tela mostrando a função de galeria de musicas do Firefox
OS.](ff_music.png)

![Tela mostrando a função de galeria de videos do Firefox
OS.](ff_video.png)

Testando aplicativos
====================

Se você desejar testar o Firefox OS pelo simulador ficam algumas dicas.

1.  Você terá que reiniciar o simulador várias vezes para que ele possa
    atualizar a "sand\_box" que ele utiliza.
2.  Na versão 1.0 do simulador parece existir um bug na função telefone.
    Nas outras versões de desenvolvedores que testei já não encontrei
    este bug.
3.  Para testar a galeria de imagens/música/vídeos você deve adicionar
    os arquivos para testes nos diretórios

    -   \$HOME/Pictures para imagens,
    -   \$HOME/Musics para músicas, e
    -   \$HOME/Videos para vídeos.

    Além disso, parece existir o suporte apenas para os formatos abertos
    (.ogg, .ogv e .webm).

4.  Ainda não descobri como simular o cartão de memória (sd).

**Referências**
