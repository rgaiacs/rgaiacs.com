---
layout: post
title: Org Mode - Evento vs Agendamento
author: "Raniere Silva"
categories: ftb
tags: [ftb]
image:
  feature: ftb.jpg
---
[Org Mode](http://orgmode.org/) é um modo do
[Emacs](https://www.gnu.org/software/emacs/) para gerenciamento de
tarefas. O Org Mode possui tanta funcionalidade que a curva de
aprendizado dele não é muito amigável. Uma das coisas que eu demorei
bastante tempo para aprender foi a diferença entre evento e agendamento
que explico nesse poste e espero lhe ajudar.

![](/images/org1.jpg){width="80%"}

Criando Agenda
==============

Abrimos um novo arquivo e salvamos o como `tutorial.org`. Depois
adicionamos o à sua lista de agendas utilizando `C-c [`.

![](/images/org2.jpg){width="80%"}

Adicionando uma tarefa
======================

Tarefas ou itens são identificados por `*` no início da linha.
Adicionamos :

    * Exemplo de evento
    * Exemplo de agendamento

no arquivo `tutorial.org`

![](/images/org3.jpg){width="80%"}

Visualizando sua agenda
=======================

Podemos verificar as tarefas para a semana utilizando
`M-x org-agenda-list`.

![](/images/org4.jpg){width="80%"}

<div class="admonition">

Dica

Para voltar para a outra janela, utilizamos `C-x o`.

</div>

Evento
======

Eventos possuem uma data para início e fim. Utilizando `C-c .` podemos
adicionar essa informação no nosso exemplo.

![](/images/org5.jpg){width="80%"}

Depois de adicionar a data, podemos atualizar a visualização das tarefas
da semana e verificamos que o evento encontra-se listado.

![](/images/org6.jpg){width="80%"}

Agendamento
===========

Eventos também podem ser agendados para iniciarem em algum momento.
Utilizando `C-c s` podemos agendar nosso exemplo.

![](/images/org7.jpg){width="80%"}

Depois de agendarmos a tarefa, podemos atualizar a visualização das
tarefas da semana e verificamos que evento encontra-se listado.

![](/images/org8.jpg){width="80%"}

Evento vs Agendamento
=====================

Eventos costumam ter horário para início e fim de forma que você
**perde** ele caso se atrase, esqueça, ...

Agendamentos costumam ter horário apenas para início. Enquanto
agendamentos não forem marcados como feitos eles são mostrados como
atrasados ao visualizar as tarefas da semana.

![](/images/org9.jpg){width="80%"}

Para marcar uma tarefa como feita, adicione `DONE` antes da descrição da
tarefa.

![](/images/org10.jpg){width="80%"}

Contagem de tempo
=================

Tanto evento como agendamentos podem ter contagem de tempo gasto
associados. Para começar a contagem do tempo utilize `C-c C-x i` e para
encerrar a contagem do tempo utilize `C-c C-x o`.

![](/images/org11.jpg){width="80%"}

Uma vez que você salvou o tempo que gastou nas suas atividades, é
possível obter uma tabela sintetizando essa informação utilizando
`M-x org-clock-report`.

![](/images/org12.jpg){width="80%"}

Relatório em HTML
=================

Por último, você pode obter um relatório com toda essa informação
utilizando a função de exportação nativa do Org Mode ou através do
[Pandoc](http://pandoc.org/).
