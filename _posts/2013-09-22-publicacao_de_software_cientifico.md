---
layout: post
title: "Publicação de Software Científico"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<div class="note">
<div class="admonition-title">
<p>Note</p>
</div>
<p>Este é um cross-post. Ele também foi publicado em <a href="http://www.cienciaaberta.net/publicacao-de-software-cientifico/" class="uri">http://www.cienciaaberta.net/publicacao-de-software-cientifico/</a>.</p>
</div>
<div class="note">
<div class="admonition-title">
<p>Note</p>
</div>
<p>Este post é baseado &quot;<a href="http://gael-varoquaux.info/blog/?p=170">no post Publishing scientific software matters</a>&quot; de Gaël Varoquaux e no artigo &quot;<a href="http://hal.inria.fr/docs/00/85/86/63/PDF/paper.pdf">Publishing scientific software matters</a>&quot; de Christophe Pradal, Gaël Varoquaux e Has Peter Langtangen (<span data-role="download">cópia do pré-print
do artigo nesse servidor &lt;pssm.pdf&gt;</span>).</p>
</div>
<p>Cada vez mais softwares estão desempenhando papel fundamental na produção científica, seja transformando um modelo teórico em simulações, controlando experimentos ou filtrando dados para serem analisados. Infelizmente algumas coisas precisam ser melhoradas.</p>
<div class="more">

</div>
<p>O primeiro problema é que várias pesquisas e softwares são construídos em cima de ferramentas proprietários como Matlab e Mathematica de forma que só é possível revisar a pesquisa e o software após adquirir tais ferramentas.</p>
<p>Uma das vantagens de modelar um problema matematicamente é que assim fica mais fácil procurar por uma forma de resolvê-lo que já tenha sido bastante estudada. De forma semelhante, seria interessante poder utilizar um software já existente ou adaptá-lo para resolver o problema desejado devido a sua estrutura matemática. Infelizmente grande maioria dos softwares são escritos direcionados para problemas específicos de forma que reusá-lo não é uma tarefa fácil, o que é o segundo problema.</p>
<p>O terceiro problema é que muitos dos softwares não encontram-se disponíveis publicamente sendo necessário contactar os autores para adquirir uma cópia quando eles podem fornecê-la. Quando falamos de ciência estamos indiretamente falando de resultados reprodutíveis, então &quot;se não é aberto e verificável por outros, então não é ciência, ou engenharia, ou qualquer que seja o nome pelo qual você chama o que fazemos&quot; (tradução literal de citação anônima presente &quot;The scientific method in practice: reproducibility in the computational sciences &quot; de V. Stodden.</p>
<p>Espero que em breve consigamos resolver estes problemas.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>ciência aberta</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
