---
layout: post
title: "Virtual Keyboard for Tablets"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Yesterday a friend wrote a blog post <a href="http://andregarzia.com/posts/en/fxostabletkeyboard/">regarding the Firefox OS Tablet Keyboard</a> where he compare the virtual keyboard of Firefox OS Tablet with the virtual keyboard of others players (webOS, Android and iOS).</p>
<div class="more">

</div>
<p>IMHO, one of the main points of the blog post was:</p>
<blockquote>
<p>&quot;To make our keyboard more useful we could use some new keys such as (...) number row. We could use a number row on top or some clever way to input numbers.&quot;</p>
</blockquote>
<p>Flatfish (AKA Firefox OS Tablet from <a href="https://wiki.mozilla.org/FirefoxOS/TCP">TCP</a>) is my first tablet (yes, I didn't have much experience using virtual keyboards at tablets) and I'm not a UX expert but let add this number row. What do you think about:</p>
<p><img src="/images/fxos-keyboard-for-tablet-v0.0a.svg" alt="Suggestion of keyboard for tablet." class="align-center" style="width:100.0%" /></p>
<p>The image above is a SVw file. You can <span data-role="download">download it &lt;fxos-keyboard-for-tablet-v0.0a.svg&gt;</span>, edit using <a href="http://inkscape.org">Inkscape</a> or any other vetorial drawing tool and share your changes with the community.</p>
<h1 id="tips-to-edit">Tips to edit</h1>
<p><img src="/images/inkscape.png" alt="Screenshot of Inkscape." class="align-center" style="width:50.0%" /></p>
<ol type="1">
<li>If you just want to replace the face of some key you can use the &quot;Create and edit text objects&quot; from the dock (or press <code>F8</code>), select the key face you want to change and do it.</li>
<li>If you want to make more changes you will need to &quot;Ungroup&quot; (press <code>Shift + CTRL + G</code>) the image.</li>
</ol>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>TCP</p>
</div>
<div class="tags">
<p>TCP</p>
</div>
<div class="comments">

</div>
