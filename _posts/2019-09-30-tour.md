---
layout: post
title: "Final words of my Namibia tour"
author: "Raniere Silva"
categories: travel
tags: ["Africa", "Namibia"]
image:
  feature: 2019-09-30-tour.jpg
  author:  Raniere Silva
  licence: CC-BY
---

I'm not a fan of guided tour
but I'm glad that I booked the
"[7 Day Pride of Namibia Camping Safari](https://www.wilddog-safaris.com/safaris/prideofnamibia/)"
offered by [Wild Dog Safaris](https://www.wilddog-safaris.com/).

{% include figure.html filename="2019-09-30-tour-2.jpg" alternative_text="Photo of group" caption="Group photo." author="Raniere Silva" licence="CC-BY" %}