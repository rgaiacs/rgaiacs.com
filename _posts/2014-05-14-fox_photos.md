---
layout: post
title: "Top 10 Fox's Photos at FISL"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Here you will find the top 10 Fox's Photos that I take during FISL.</p>
<div class="more">

</div>
<figure>
<img src="/images/fox9.jpg" alt="Qaiq, one of Brazilian Community member, is a great artist and drwa the Fox in Mozilla&#39;s room." class="align-center" style="width:50.0%" /><figcaption>Qaiq, one of Brazilian Community member, is a great artist and drwa the Fox in Mozilla's room.</figcaption>
</figure>
<figure>
<img src="/images/fox8.jpg" alt="Fox and one of Brazilian Community member." class="align-center" style="width:50.0%" /><figcaption>Fox and one of Brazilian Community member.</figcaption>
</figure>
<figure>
<img src="/images/fox7.jpg" alt="Fox playing soccer." class="align-center" style="width:50.0%" /><figcaption>Fox playing soccer.</figcaption>
</figure>
<figure>
<img src="/images/fox6.jpg" alt="Fox and his friends: Tux and Gnu." class="align-center" style="width:50.0%" /><figcaption>Fox and his friends: Tux and Gnu.</figcaption>
</figure>
<figure>
<img src="/images/fox5.jpg" alt="Fox wearing &quot;Red Hat&quot;." class="align-center" style="width:50.0%" /><figcaption>Fox wearing &quot;Red Hat&quot;.</figcaption>
</figure>
<figure>
<img src="/images/fox4.jpg" alt="Fox dancing during the launch party." class="align-center" style="width:50.0%" /><figcaption>Fox dancing during the launch party.</figcaption>
</figure>
<figure>
<img src="/images/fox3.jpg" alt="Fox and some attends of FISL. The little girl really loves Fox." class="align-center" style="width:50.0%" /><figcaption>Fox and some attends of FISL. The little girl really loves Fox.</figcaption>
</figure>
<figure>
<img src="/images/fox2.jpg" alt="Fox support trash recycling." class="align-center" style="width:50.0%" /><figcaption>Fox support trash recycling.</figcaption>
</figure>
<figure>
<img src="/images/fox1.jpg" alt="Fox with a cam recoder." class="align-center" style="width:50.0%" /><figcaption>Fox with a cam recoder.</figcaption>
</figure>
<figure>
<img src="/images/fox0.jpg" alt="Fox kissing the hand of a girl." class="align-center" style="width:50.0%" /><figcaption>Fox kissing the hand of a girl.</figcaption>
</figure>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>none</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
