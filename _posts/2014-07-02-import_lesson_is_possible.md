---
layout: post
title: "Import Lesson - Is it Possible?"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Some time ago, Greg Wilson <a href="http://software-carpentry.org/blog/2014/04/import-lesson.html">wrote about the idea of import lessons</a>. This is food for thought about it.</p>
<div class="more">

</div>
<h1 id="version-control-and-possibility-to-fork-lessons">Version Control and Possibility to Fork Lessons</h1>
<p>For individual lesson we should use a modern distribute version control system like Git or Mercurial.</p>
<p>The problem is what use to deal with the collection of lessons when import each one. We can think in use <a href="http://git-scm.com/book/en/Git-Tools-Submodules">Git Submodule</a> or <a href="http://git-scm.com/book/en/Git-Tools-Subtree-Merging">Git Subtree</a> but from my experience the better approach is use a tool on top of Git or Mercurial like <a href="https://code.google.com/p/git-repo/">git-repo</a>. This approach will avoid the user learn the complexity of Git Submodule and let the user choose the version control system more suitable to he/she needs.</p>
<h1 id="dependencies">Dependencies</h1>
<p>For <a href="http://en.wikipedia.org/wiki/Package_management_system">package management system</a> is easy to resolve dependencies since it envolves only know two informations: (1) the name of required package and (2) the version of it. For educational resources this can be much more complex but for now lets keep simple and say that to resolve the dependencies of one lessons you only need to know three informations: (1) the protocol to use when download the required lesson, (2) the URL of the required lesson and (3) the version of it.</p>
<h1 id="format">Format</h1>
<p>The format of the lessons can also be a problem. After think a little about it the best solution that I find was request that every lessons be available as HTML or inform a rule to build it as HTML. The reasons to use HTML are:</p>
<ul>
<li>is an open format;</li>
<li>is the format used at the Web (and EPUB);</li>
<li>for many others formats exist a tool to convert it into HTML.</li>
</ul>
<h1 id="metadata-and-manifest">Metadata and Manifest</h1>
<p>To import lessons we will need that lessons have a few metadata and the format to store it should be JSON. The minimal list of metadata that I could list was:</p>
<dl>
<dt>Authors</dt>
<dd><p>Ordered list of following informations about the authors.</p>
<dl>
<dt>Name</dt>
<dd><p>Full name of the author.</p>
</dd>
<dt>Contact</dt>
<dd><p>Email of the author.</p>
</dd>
</dl>
</dd>
<dt>Title</dt>
<dd><p>Title of the lesson.</p>
</dd>
<dt>Language</dt>
<dd><p>Language of the lesson.</p>
</dd>
<dt>License</dt>
<dd><p>Lesson <strong>must</strong> be under an copyleft license.</p>
</dd>
<dt>URL</dt>
<dd><p>URL where the lesson is available or more information can be found.</p>
</dd>
<dt>Require</dt>
<dd><p>Unordered list of following informations about the required lessons.</p>
<dl>
<dt>Name</dt>
<dd><p>String used as reference at <code>NAV</code> field.</p>
</dd>
<dt>Protocol</dt>
<dd><p>Protocol to be used to download the lesson. Git, Mercurial or other version control system.</p>
</dd>
<dt>Remote</dt>
<dd><p>URL where the lesson is available to download.</p>
</dd>
<dt>Revision</dt>
<dd><p>Revision required.</p>
</dd>
</dl>
</dd>
<dt>Protocol</dt>
<dd><p>Protocol to be used to download this lesson. Git, Mercurial or other version control system.</p>
</dd>
<dt>Remote</dt>
<dd><p>URL where this lesson is available to download.</p>
</dd>
<dt>Build</dt>
<dd><p>Command used to build the lesson to HTML.</p>
</dd>
<dt>NAV</dt>
<dd><p>Ordered list of following information about the lessons that are part of this one.</p>
<dl>
<dt>Name</dt>
<dd><p>The path of one file of this lesson or <code>Name</code> of a required lesson.</p>
</dd>
</dl>
</dd>
</dl>
<h1 id="lesson-example">Lesson Example</h1>
<p>Let say that John Doe wrote a lesson about the solar system that the only file is <code>index.html</code> and use git to version it. The possible JSON is :</p>
<pre><code>{
    &quot;authors&quot;: [{&quot;name&quot;: &quot;John Doe&quot;, &quot;contact&quot;: &quot;john.doe@mail.com&quot;}],
    &quot;title&quot;: &quot;Solar System&quot;,
    &quot;language&quot;: &quot;en&quot;,
    &quot;license&quot;: &quot;cc-by&quot;,
    &quot;url&quot;: &quot;http://john-doe.github.io/solar-system&quot;,
    &quot;require&quot;: [],
    &quot;protocol&quot;: &quot;git&quot;,
    &quot;remote&quot;: &quot;http://github.com/john-doe/solar-system&quot;,
    &quot;nav&quot;: [{&quot;name&quot;: &quot;index.html&quot;}]
}</code></pre>
<h1 id="collection-example">Collection Example</h1>
<p>Let say that Jane Doe is going to create a collection from Software Carpentry lessons for a Git course. The possible JSON is :</p>
<pre><code>{
    &quot;authors&quot;: [{&quot;name&quot;: Jane Doe&quot;, &quot;contact&quot;:jane.doe@mail.com&quot;}],
    &quot;title&quot;: &quot;Jane&#39;s Git Course&quot;,
    &quot;language&quot;: &quot;en&quot;,
    &quot;license&quot;: &quot;cc-by&quot;,
    &quot;url&quot;: &quot;http://jane-doe.github.io/git-course&quot;,
    &quot;require&quot;:[
        {&quot;name&quot;: &quot;shell-filedir&quot;,
         &quot;protocol&quot;: &quot;git&quot;,
         &quot;remote&quot;: &quot;http://github.com/swcarpentry/shell-filedir&quot;,
         &quot;revision&quot;: &quot;master&quot;},
        {&quot;name&quot;: &quot;shell-create&quot;,
         &quot;protocol&quot;: &quot;git&quot;,
         &quot;remote&quot;: &quot;http://github.com/swcarpentry/shell-create&quot;,
         &quot;revision&quot;: &quot;master&quot;},
        {&quot;name&quot;: &quot;git-backup&quot;,
         &quot;protocol&quot;: &quot;git&quot;,
         &quot;remote&quot;: &quot;http://github.com/swcarpentry/git-backup&quot;,
         &quot;revision&quot;: &quot;master&quot;},
        {&quot;name&quot;: &quot;git-collab&quot;,
         &quot;protocol&quot;: &quot;git&quot;,
         &quot;remote&quot;: &quot;http://github.com/swcarpentry/git-collab&quot;,
         &quot;revision&quot;: &quot;master&quot;},
        {&quot;name&quot;: &quot;git-conflict&quot;,
         &quot;protocol&quot;: &quot;git&quot;,
         &quot;remote&quot;: &quot;http://github.com/swcarpentry/git-conflict&quot;,
         &quot;revision&quot;: &quot;master&quot;}
    ],
    &quot;protocol&quot;: &quot;git&quot;,
    &quot;remote&quot;: &quot;http://github.com/jane-doe/git-course&quot;,
    &quot;nav&quot;: [
        {&quot;name&quot;: &quot;index.html&quot;},
        {&quot;name&quot;: &quot;shell-filedir&quot;},
        {&quot;name&quot;: &quot;shell-create&quot;},
        {&quot;name&quot;: &quot;git-backup&quot;},
        {&quot;name&quot;: &quot;git-collab&quot;},
        {&quot;name&quot;: &quot;git-conflict&quot;},
        {&quot;name&quot;: &quot;exercises.html&quot;}
    ]
}</code></pre>
<h1 id="prototype-and-mozilla-science-lab-multi-site-sprint">Prototype and Mozilla Science Lab Multi-Site Sprint</h1>
<p>For the sprint that Mozilla Science Lab will run on July 22-23 I believe that we can implement a tool to import lessons following the suggestion above and test it to handle Software Carpentry's shell, git and mercurial lessons.</p>
<div class="admonition">
<p>Update</p>
<p>One nice discussion happened at the <a href="https://lists.okfn.org/pipermail/open-education/2014-July/000498.html">Open Education mail list</a> from Open Knowledge.</p>
<p>Rémi <a href="https://github.com/twitwi/lesson-manager">setup one repository</a> to be used at Mozilla Science Lab Sprint.</p>
</div>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>none</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
