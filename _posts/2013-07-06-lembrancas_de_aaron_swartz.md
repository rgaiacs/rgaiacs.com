---
layout: post
title: "Lembranças de Aaron Swartz"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Essa palestra foi apresentada por Seth Schoen (em português) e sem dúvida foi a que mais gostei (fiquei tão emocionado que chorei durante ela).</p>
<figure>
<img src="/images/lembrancas_de_aaron_swartz_0.jpg" class="align-center" style="width:80.0%" />
</figure>
<p>Para que não sabe <a href="http://en.wikipedia.org/wiki/Aaron_Swartz">Aaron Swartz</a> foi um garoto prodígio na área de computação que esteve envolvido com a criação de metadados, informações que possam ser legíveis por máquinas, compartilhamento do conhecimento de forma aberta (Creative Commons), descentralização da internet, privacidade e anonimato, ...</p>
<p>No início desse ano Swartz suicidou-se devido a ser declarado culpado por baixar vários artigos científicos no MIT. Seth foi um grande amigo de Swartz que não teve a oportunidade de ajudá-lo pois esse não falou sobre seus problemas legais uma vez que isso poderia &quot;incriminá-lo&quot; frente aos tribunais.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>FISL</p>
</div>
<div class="tags">
<p>FISL14</p>
</div>
<div class="comments">

</div>
