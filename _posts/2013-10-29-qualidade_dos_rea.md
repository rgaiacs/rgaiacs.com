---
layout: post
title: "Qualidade dos REA"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<div class="note">
<div class="admonition-title">
<p>Note</p>
</div>
<p>Este post foi baseado em &quot;<a href="http://opencontent.org/blog/archives/2947">On Quality and OER</a>&quot; de David Wiley (<span data-role="download">cópia nesse servidor &lt;quality-oer.html&gt;</span>).</p>
</div>
<p>Todos já ouvimos o seguinte pensamento:</p>
<blockquote>
<p>&quot;Não julgue um livro pela sua capa.&quot;</p>
</blockquote>
<p>E isso é uma grande verdade para os Recursos Educacionais Abertos (REA) pois muitos acreditam que os REAs possuem conteúdo de qualidade duvidosa em relação aos seus concorrentes fechados e isso nem sempre é verdade.</p>
<div class="more">

</div>
<h1 id="definindo-qualidade">Definindo qualidade</h1>
<p>Muitos associam a qualidade de um recurso educacional a forma como é apresentado (se o layout é bonito, se as imagens possuem alta definição, ...). Infelizmente essa associação é falha e a qualidade de um recurso educacional deve ser medido para responder apenas &quot;o quanto os alunos aprendem ao utilizar esse material?&quot;.</p>
<h1 id="trocando-todos-por-alguns">Trocando &quot;Todos&quot; por &quot;Alguns&quot;</h1>
<p>Considere o melhor livro didático que você já utilizou e imagine que na página informativa dos direitos autorais estive escrito &quot;Alguns Direitos Reservados -este livro é licenciado sob CC-BY&quot; ao invés de &quot;Todos os Direitos Reservados&quot;. O livro perdeu qualidade devido a mudança de licença? NÃO.</p>
<p>A qualidade de um livro não é uma função da licença por ele utilizado de modo que:</p>
<ul>
<li>existem REA de alta qualidade e</li>
<li>transformar um material em REA não vai melhorar sua qualidade instantaneamente.</li>
</ul>
<h1 id="palavras-dos-especialistas">Palavras dos especialistas</h1>
<p>Somos levados a acreditar que um conteúdo criado colaborativamente sem nenhum critério para a seleção daqueles capacitados para contribuir (como é o caso da Wikipédia) irá possuir vários erros. Em 2005 a revista Nature fez um experimento para aferir essa suposição e chegou a conclusão de que a Wikipédia é tão precisa quanto a Enciclopédia Britânica.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>REA</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
