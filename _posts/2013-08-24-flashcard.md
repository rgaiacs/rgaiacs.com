---
layout: post
title: Flashcard
author: "Raniere Silva"
categories: ftb
tags: [ftb]
image:
  feature: ftb.jpg
---
Flashcard são cartões utilizados para auxiliar o aprendizado de algum
conteúdo. Um dos softwares com a funcionalidade de Flashcard é o
[AnyMemo - Memorize Anything](http://anymemo.org/) cujo código é
licenciado sob GPLv2 e pode ser instalado tanto pelo
[F-droid](https://f-droid.org/) como pelo Google Play.

Vários flashcard já encontram-se disponíveis em alguns repositórios
[como esse aqui](http://anymemo.org/index.php?page=databases). É comum
desejarmos adaptar um flashcard já existente e vou mostrar como fazê-lo.

Formatos
========

O formato nativo do AnyMemo é o "AnyMemo Database" que consiste em um
SQLite3. Tomando como exemplo esse flashcard &lt;hiragana.xml.db&gt;
temos :

    $ sqlite3 hiragana.xml.db
    SQLite version 3.7.9 2011-11-01 00:52:41
    Enter ".help" for instructions
    Enter SQL statements terminated with a ";"
    sqlite> .schema
    CREATE TABLE android_metadata(locale TEXT DEFAULT en_US);
    CREATE TABLE control_tbl(ctrl_key text unique, value text);
    CREATE TABLE dict_tbl(_id INTEGER PRIMARY KEY ASC AUTOINCREMENT, question TEXT,
    answer TEXT, note TEXT, category TEXT);
    CREATE TABLE learn_tbl(_id INTEGER PRIMARY KEY ASC AUTOINCREMENT, date_learn,
    interval INTEGER, grade INTEGER, easiness REAL, acq_reps INTEGER, ret_reps
    INTEGER, lapses INTEGER, acq_reps_since_lapse INTEGER, ret_reps_since_lapse
    INTEGER);
    sqlite> .q

Deste modo, para modificar o flashcard é preciso um pouco de
conhecimento de SQLite ou utilizar uma interface gráfica.

<div class="admonition note">

Além do formato nativo ele pode importar:

-   Comma separated CSV file
-   Tab separated TXT file
-   SuperMemo PPC XML
-   Mnemosyne XML
-   QA Text file

</div>

SQLite Manager para Firefox
===========================

Como interface gráfica iremos utilizar o [SQLite
Manager](https://addons.mozilla.org/en-US/firefox/addon/sqlite-manager/)
para o Firefox. Depois de instalá-lo e reiniciar o Firefox, selecione
`Ferramentas -> SQLite Manager`.

![](/images/sqlite_manager00.jpg){width="80%"}

Na nova janela que abrir, selecione na barra superior
`Banco de dados -> Conectar Banco de Dados`. Ele irá perguntar qual
arquivo que é para carregar (nesse caso selecione `hiragana.xml.bd`).

![](/images/sqlite_manager01.jpg){width="80%"}

<div class="admonition note">

As modificações efetuadas são salvas no próprio arquivo e por esse
motivo é recomendado fazer um backup do mesmo antes de qualquer
alteração.

</div>

Nas opções na esquerda, selecione `Tabelas -> dict_tbl`.

![](/images/sqlite_manager02.jpg){width="80%"}

Nas abas, selecione `Navegação & Busca`.

![](/images/sqlite_manager03.jpg){width="80%"}

Nessa janela, é possível adicionar modificar (inclusive remover) os
cartões existentes e também adicionar novos.

Em `hiragana.xml.db` encontramos todos os hiraganas. Eu gostaria de um
flashcard com apenas os hiraganas listados
[aqui](http://www.nhk.or.jp/lesson/english/syllabary/index.html#tabSylla).
Para isso irei remover aqueles que não desejo (selecionar os cartões
desejados para remoção e precionar o botão `Apagar` logo acima da lista
de cartões).

![](/images/sqlite_manager04.jpg){width="80%"}

Uma vez concluída as alterações, selecione na barra superior
`Banco de dados -> Encerrar conexão`.

**Referências**
