---
layout: post
title: "Flatfish and (La)TeX"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>This is a &quot;How to&quot; for people that are tracking my GSoC project and in this post you will find steps to test the first prototype of my GSoC project. I hope that you like it and I will love to get any type of feedbacks.</p>
<div class="admonition">
<p>Note</p>
<p>This blog post have some screenshots of my Flatfish running</p>
<ul>
<li><strong>OS version</strong>: 2.0.0.0-prerelease</li>
<li><strong>Firmware revision</strong>: flatfish_20140519-1653</li>
<li><strong>Hardware revision</strong>: flatfish</li>
<li><strong>Platform version</strong>: 32.0a1</li>
<li><strong>Build Identifier</strong>: 20140519171236</li>
<li><strong>Git commit info</strong>: 2014-06-03 14:21:30 0f51c8fa</li>
</ul>
</div>
<div class="more">

</div>
<h1 id="requirements">Requirements</h1>
<ul>
<li>Desktop/Laptop (I'm using a GNU/Linux distribution but shouldn't be hard to use a machine running Windows or OS X).</li>
<li>Bash, Make, Git</li>
<li>Android SDK Tools (at least <a href="http://developer.android.com/tools/help/adb.html">adb</a>)</li>
<li>Device running Firefox OS (I'm using Flatfish but it should work with any other device) or Firefox OS Simulator</li>
</ul>
<div class="admonition">
<p>Note</p>
<p>If you will use a device running Firefox OS and you didn't setup udev rules you will need to do it. For informations read <a href="https://developer.mozilla.org/en-US/Firefox_OS/Firefox_OS_build_prerequisites#For_Linux.3A_configure_the_udev_rule_for_your_phone">configure the udev rule for your phone</a> at MDN.</p>
</div>
<div class="admonition">
<p>Note</p>
<p>If you will use Firefox OS Simulator please take a look at <a href="http://ftb.rgaiacs.com/2014/03/29/hacking_gaia.html">this tutorial that explain how udpate Gaia for Firefox OS Simulator</a>.</p>
</div>
<h1 id="setup">Setup</h1>
<p>You will need my clone of <code>mozilla-b2g/gaia</code> at GitHub and move to <code>gsoc</code> branch. :</p>
<pre><code>$ git clone https://github.com/r-gaia-cs/gaia.git
$ cd gaia
$ git checkout gsoc</code></pre>
<div class="admonition">
<p>Note</p>
<p>If you already have <code>mozilla-b2g/gaia</code> you can add my clone as a remote repository. :</p>
<pre><code>$ cd gaia
$ git remote add raniere https://github.com/r-gaia-cs/gaia.git
$ git fetch raniere
$ git checkout raniere/gsoc</code></pre>
</div>
<h1 id="flashing">Flashing</h1>
<p>Connect your device in one of the USB ports of your desktop/laptop and check if adb recognize it. :</p>
<pre><code>$ adb devices
List of devices attached 
FLATFISH_123456 device</code></pre>
<div class="admonition">
<p>Warning</p>
<p>The next step will remove all the user data from the device. If you have important data in your device you will need to backup it first.</p>
</div>
<p>With the device connect and recognize:</p>
<pre><code>$ GAIA_KEYBOARD_LAYOUTS=en,math,trig,greek PRODUCTION=1 NOFTU=1 REMOTE_DEBUGER=1 GAIA_DEVICE_TYPE=tablet make reset-gaia</code></pre>
<div class="admonition">
<p>Note</p>
<p>If you are using a phone device:</p>
<pre><code>$ GAIA_KEYBOARD_LAYOUTS=en,math,trig,greek PRODUCTION=1 NOFTU=1 REMOTE_DEBUGER=1 make reset-gaia</code></pre>
</div>
<div class="admonition">
<p>Note</p>
<p>If you are using Firefox OS Simulator:</p>
<pre><code>$ GAIA_KEYBOARD_LAYOUTS=en,math,trig,greek make</code></pre>
</div>
<p>Your device will reboot.</p>
<figure>
<img src="/images/home.png" class="align-center" style="width:50.0%" />
</figure>
<h1 id="enable-keyboards">Enable Keyboards</h1>
<p>Now we need to enable the keyboards. Go to &quot;Settings&quot; -&gt; &quot;Keyboards&quot; -&gt; &quot;Selected keyboards&quot; -&gt; &quot;Add more keyboards&quot;.</p>
<figure>
<img src="/images/settings0.png" alt="Screenshot of Settings." class="align-center" style="width:50.0%" /><figcaption>Screenshot of Settings.</figcaption>
</figure>
<figure>
<img src="/images/settings1.png" alt="Screenshot of &quot;Keyboards&quot;." class="align-center" style="width:50.0%" /><figcaption>Screenshot of &quot;Keyboards&quot;.</figcaption>
</figure>
<figure>
<img src="/images/settings2.png" alt="Screenshot of &quot;Selected keyboars&quot;." class="align-center" style="width:50.0%" /><figcaption>Screenshot of &quot;Selected keyboars&quot;.</figcaption>
</figure>
<figure>
<img src="/images/settings3.png" alt="Screenshot of &quot;Add more keyboards&quot;." class="align-center" style="width:50.0%" /><figcaption>Screenshot of &quot;Add more keyboards&quot;.</figcaption>
</figure>
<p>Select all the keyboards available.</p>
<figure>
<img src="/images/settings4.png" alt="Screenshot of &quot;Add more keyboards&quot; with all keyboards selected." class="align-center" style="width:50.0%" /><figcaption>Screenshot of &quot;Add more keyboards&quot; with all keyboards selected.</figcaption>
</figure>
<p>Go back to &quot;Selected keyboards&quot;.</p>
<figure>
<img src="/images/settings5.png" alt="Screenshot of &quot;Selected keyboards&quot; after add some keyboards." class="align-center" style="width:50.0%" /><figcaption>Screenshot of &quot;Selected keyboards&quot; after add some keyboards.</figcaption>
</figure>
<p>Go to Home Screen.</p>
<h1 id="testing">Testing</h1>
<p>You can test the keyboard with any app that use the keyboard. I will be using <a href="http://r-gaia-cs.github.io/TeXEditor/" class="uri">http://r-gaia-cs.github.io/TeXEditor/</a> from the web browser.</p>
<p>Open the web browser.</p>
<figure>
<img src="/images/browser.png" alt="Screenshot of Web Browser app." class="align-center" style="width:50.0%" /><figcaption>Screenshot of Web Browser app.</figcaption>
</figure>
<p>Go to <a href="http://r-gaia-cs.github.io/TeXEditor/" class="uri">http://r-gaia-cs.github.io/TeXEditor/</a>.</p>
<figure>
<img src="/images/texeditor0.png" alt="Screenshot of homescreen of TeX Editor app." class="align-center" style="width:50.0%" /><figcaption>Screenshot of homescreen of TeX Editor app.</figcaption>
</figure>
<p>Type some text. To access the math keyboards use the IME selector key.</p>
<figure>
<img src="/images/texeditor1.png" alt="Screenshot of TeX Editor app with some text." class="align-center" style="width:50.0%" /><figcaption>Screenshot of TeX Editor app with some text.</figcaption>
</figure>
<figure>
<img src="/images/texeditor2.png" alt="Screenshot of TeX Editor app with Greek keyboard." class="align-center" style="width:50.0%" /><figcaption>Screenshot of TeX Editor app with Greek keyboard.</figcaption>
</figure>
<figure>
<img src="/images/texeditor3.png" alt="Screenshot of TeX Editor app after type &quot;A&quot;." class="align-center" style="width:50.0%" /><figcaption>Screenshot of TeX Editor app after type &quot;A&quot;.</figcaption>
</figure>
<figure>
<img src="/images/texeditor4.png" alt="Screenshot of TeX Editor app with the Math keyboard." class="align-center" style="width:50.0%" /><figcaption>Screenshot of TeX Editor app with the Math keyboard.</figcaption>
</figure>
<figure>
<img src="/images/texeditor5.png" alt="Screenshot of TeX Editor app after type the superscript key." class="align-center" style="width:50.0%" /><figcaption>Screenshot of TeX Editor app after type the superscript key.</figcaption>
</figure>
<figure>
<img src="/images/texeditor6.png" alt="Screenshot of TeX Editor app after close LaTeX math environment." class="align-center" style="width:50.0%" /><figcaption>Screenshot of TeX Editor app after close LaTeX math environment.</figcaption>
</figure>
<p>When you end to input your text you can preview it using the option at the top right of the screen.</p>
<figure>
<img src="/images/texeditor7.png" alt="Screenshot of TeX Editor app with the preview of source code typed." class="align-center" style="width:50.0%" /><figcaption>Screenshot of TeX Editor app with the preview of source code typed.</figcaption>
</figure>
<h1 id="still-need-to-be-done">Still need to be done</h1>
<p>The keyboards still need many improvements like</p>
<ul>
<li>Improve switch,</li>
<li>Improve layout,</li>
<li>Improve style.</li>
</ul>
<p>I will write more about it in another blog post.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>TCP,GSOC</p>
</div>
<div class="tags">
<p>TCP,GSOC2014</p>
</div>
<div class="comments">

</div>
