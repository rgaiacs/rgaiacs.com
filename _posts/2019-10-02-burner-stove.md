---
layout: post
title: "Four-burner stove"
author: "Raniere Silva"
categories: travel
tags: ["Africa"]
image:
  feature: 2019-10-02-burner-stove.jpg
  author: Kevin Schmid
  licence: CC-BY
  original_url: https://unsplash.com/photos/39KMFsgTLz0
---

I read "[Essentialism: The Disciplined Pursuit of Less](https://gregmckeown.com/book/)" by Greg McKeown
and I liked the following fragment:

> In a pice called "[Laugh, Kookaburra](https://www.newyorker.com/magazine/2009/08/24/laugh-kookaburra)"
> published in The New Yorker,
> David Sedaris gives a humorous account of his experience touring the Australian "bush".
> While hiking,
> his friend and guide for the day shares something she has heard in passing at a management class.
> "Imagine a four-burner stove,"
> she instructs the members of the party.
> "One burner represents your family, one is your friends,
> the third is your health,
> and the fourth is your work.
> In order to be successful you have to cut off on of your burners.
> And in order to be really successful you have to cut off two."

When I look back,
I noticed that between Janury, 2016 and December, 2018
my priorities were work followed by friends and health.
After a burnout,
I dropped work for a while and kept friends and health as priorities.
I'm planning to take November and December to focus 80% in health
and, starting in 2020, focus 80% on work, again.