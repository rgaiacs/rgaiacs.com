---
layout: post
title: "Baralho de reflexões"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Ontem, 08/06/2012, eu participei do <a href="http://www.cienciaaberta.net/?page_id=18">workshop do Encontro pelo Conhecimento Livre</a> que ocorreu na <a href="http://www.casanexocultural.com.br/">Casa Nexo</a>.</p>
<p>O workshop foi muito bom e o espaço escolhido é maravilhoso. Mas neste post queria mostrar um lindo painel que encontrei no muro da Casa Nexo (ver figura abaixo).</p>
<figure>
<img src="/images/IMG_20130608_101923.jpg" class="align-center" style="width:80.0%" />
</figure>
<div class="more">

</div>
<p>No painel encontramos alguns pequenos textos, como:</p>
<pre><code>Os jornais pioram porque vendem menos ou vendem menos porque pioram?</code></pre>
<p>e :</p>
<pre><code>Qualquer semelhança com a realidade é mera coincidência.</code></pre>
<p>O texto que mais gostei, foi:</p>
<pre><code>Outra casa foi demolida:
( ) Não tem problema, era uma casa velha
( ) Vai ser um lindo prédio novo
( ) Preservar a memória é coisa do passado
( ) Ponto para a especulação imobiliária
( ) BH, cidade em demolição</code></pre>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>none</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
