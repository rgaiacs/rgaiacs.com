---
layout: post
title: MathML tests for Firefox
author: "Raniere Silva"
categories: ftb
tags: [ftb]
image:
  feature: ftb.jpg
---
Firefox é o único browser no momento que possue suporte a MathML (em
browser que utilizando WebKit existe um patch esperando aprovação, em
browser que utilizando o Blink não existe previsão de reaproveitar o
patch do WebKit e no IE apenas com extensões).

Uma das partes importante da implementação do MathML é a escrita de
testes e é isso que será apresentado nesse post.

O bug [897065](https://bugzil.la/897065) refere-se a incorporação de
mais testes e é um ótimo ponto de partida e alguns dos testes já
escritos encontram-se
[aqui](http://mxr.mozilla.org/mozilla-central/source/layout/reftests/mathml/)
(na árvore do código fonte do Firefox você encontrará esses testes em
`layout/reftests/mathml`).

Reftest
=======

O reftest compara pixel a pixel da tela (800x1000) de duas páginas web e
considera que o teste foi um sucesso no caso das telas serem idênticas.
Uma das vantagens dessa ferramenta deve-se ao fato de que existe mais de
uma maneira de obter o mesmo efeito visual em um browser de forma que
uma marcação complexa pode ser expressa na forma de uma marcação mais
simples.

Testando
========

Para executar todos os testes, acesse a raíz do código fonte do Firefox
e :

    $ ./mach reftest

Para executar apenas um conjunto de testes:

    $ ./mach reftest caminho/para/reftest.list

<div class="admonition note">

`mach` é um script em python que acompanha o código fonte do Firefox.

</div>

O primeiro teste
================

<div class="admonition warning">

Será preciso baixar e compilar o Firefox para conseguir rodar os tests
pois os binários distribuídos são compilados utilizando `disable-tests`.

</div>

Escreva um arquivo chamado `foo.html` contendo:

    <html>
    <head>
    <title>reftest0001</title>
    </head>
    <body>
    <strong>Hello!</strong>
    </body>
    </html>

Escreva um arquivo chamado `bar.html` contendo:

    <html>
    <head>
    <title>reftest0001</title>
    </head>
    <body>
    <b>Hello!</b>
    </body>
    </html>

Escreva um arquivo chamado `reftest.list` contendo:

    == foo.html bar.html

Execute o teste utilizando:

    $ ./mach reftest path/to/reftest.list 2>&1 | grep REFTEST
    REFTEST PASS: file:///Users/ray/mozilla-central/path/to/foo.html

**Referências**
