---
layout: post
title: "4th MathML Meeting"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>This is a report about the 4th Mozilla MathML IRC Meeting (see the announcement <a href="https://groups.google.com/forum/#!topic/mozilla.dev.tech.mathml/pRrgPk9MV1E">here</a>). The topics of the meeting can be found <a href="https://etherpad.mozilla.org/mathml-2014-03">in this PAD</a> (<span data-role="download">local copy of the PAD &lt;pad.txt&gt;</span>) and the <a href="http://logbot.glob.com.au/?c=mozilla%23mathml&amp;s=13+Mar+2014&amp;e=13+Mar+2014">IRC log</a> (<span data-role="download">local copy of the IRC log &lt;irc.txt&gt;</span>) is also available.</p>
<p>In the last 4 weeks the MathML team closed 6 bugs, worked in others 5 and open 4 new ones (this are only the ones tracked by Bugzilla, there are others issues related with MathML that the team worked on, like Frédéric's <a href="http://www.ulule.com/mathematics-ebooks/">Mathematics in ebooks</a>).</p>
<p>The next meeting will be in April 11th at #mathml IRC channel. Please add topics in the <a href="https://etherpad.mozilla.org/mathml-2014-04">PAD</a>.</p>
<p>We didn't have the time for the next meeting because Daylight Saving Time start for some folks and end for others. We want to know what time is suitable for you participate in next meeting and for that we are running <a href="http://mathml.limequery.com/index.php/396944/lang-en">a survey</a> powered by <a href="http://www.limesurvey.org/en/">LimeSurvey</a>, a open source tool (if for any reason you aren't comfortable to use LimeSurvey you can <a href="mailto:raniere@ime.unicamp.br">send me a email</a>).</p>
<div class="more">

</div>
<h1 id="texzilla">TeXZilla</h1>
<p><a href="https://github.com/fred-wang/TeXZilla">TeXZilla</a> is the (La)TeX to MathML Javascript parser that Frédéric wrote. We made some improvements on it (like <a href="http://w3c.github.io/webcomponents/spec/shadow/">Shadow DOM</a> support wrote by Frédéric) and the <a href="https://github.com/fred-wang/TeXZilla/wiki#applications-and-web-pages-using-texzilla">applications that using it</a>.</p>
<div class="admonition">
<p>Correction</p>
<p>Shadow DOM is implemented in Gecko 30 and TeXZilla currently use X-tag polyfill as explained by Frédéric in <a href="https://groups.google.com/d/msg/mozilla.dev.tech.mathml/RDcdlVDG0Cs/1fn-7h0J5rcJ">mozilla.dev.tech.mathml list</a>:</p>
<blockquote>
<p>the &lt;x-tex&gt; custom element (<a href="https://github.com/fred-wang/x-tex" class="uri">https://github.com/fred-wang/x-tex</a>) is currently implemented via the X-tag polyfill (<a href="http://x-tags.org/" class="uri">http://x-tags.org/</a>) which does not use shadow DOM. However, teoli said shadow DOM is implemented in Gecko 30 and that the custom element spec will allow to just name the element &lt;tex&gt; so this could be improved in the future. At the moment that's mostly a proof of concept. As a concrete example,</p>
<p>TeXelement = document.createElement(&quot;x-tex&quot;); TeXelement.textContent = &quot;x^2 + y^2&quot;;</p>
<p>is not equivalent to</p>
<p>&lt;x-tex&gt;x^2 + y^2&lt;/x-tex&gt;</p>
<p>(<a href="https://github.com/fred-wang/x-tex/issues/3" class="uri">https://github.com/fred-wang/x-tex/issues/3</a>).</p>
</blockquote>
</div>
<h1 id="mdn">MDN</h1>
<p>We had send a patch for MDN with a plugin for MDN's editor and this plugin was already deploy. Frédéric said that &quot;If you want to insert/edit MathML on MDN, there is now a 'square root of x' button to open a TeXZilla dialog&quot;.</p>
<p>Almost half of people accessing MDN use Chrome that don't support MathML. This users will get a bad first impression of MathML just because it won't work in Chrome. Just for example, you will find below a screenshot of <a href="https://developer.mozilla.org/en-US/docs/Web/MathML/Examples/MathML_Pythagorean_Theorem">MDN's MathML Pythagorean Theorem</a> in Chromium and Firefox.</p>
<figure>
<img src="/images/screenshot.jpg" class="align-center" style="width:80.0%" />
</figure>
<p>Frédéric <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=981409">suggested use a small CSS fallback</a> to &quot;fix&quot; basic MathML elements for browsers that don't support MathML. Folks from MDN agree to use this fallback and we just need to add it properly in MDN source tree.</p>
<p>And for last, MathML documentation at MDN has a <a href="https://developer.mozilla.org/en-US/docs/MDN/Doc_status/MathML">status page</a> that is awesome.</p>
<h1 id="gecko">Gecko</h1>
<p>Among the contributions of the team we had:</p>
<ul>
<li>Frédéric Wang done <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=663740">some code refactoring</a>,</li>
<li>Jamkes Kitchener <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=330964">improve mtable support</a>,</li>
<li>Some &quot;regression&quot; fixed by Jesse Ruderman and James Kitchener and</li>
<li>Others fixes/patch review/bug report by Karl Tomlinson, Robert O'Callahan and Andrew McCreight.</li>
</ul>
<p>We also had the contribution of new members:</p>
<ul>
<li>Branko Kzrnaric <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=914031">implement menclose notation &quot;phasorangle&quot;</a> and</li>
<li>Anuj Agarwal worked to <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=973917">fix a problem in the rendering of msqrt amd menclose</a>.</li>
</ul>
<h1 id="gsoc">GSOC</h1>
<p>I will aplly for GSOC to extend Firefox OS keyboard to support math. You can find more information in my previous post.</p>
<h1 id="how-to-help---newcomers">How to help - newcomers</h1>
<ul>
<li>Write a plugin for your <a href="https://en.wikipedia.org/wiki/Content_management_system">CMS</a> (e.g. Drupal and Wordpress) that use <a href="https://github.com/fred-wang/TeXZilla">TeXZilla</a>. For a example, see <a href="http://ckeditor.com/addon/texzilla">this plugin</a> for <a href="http://ckeditor.com">CKEditor</a>.</li>
<li>Test <a href="https://github.com/fred-wang/TeXZilla/wiki/Advanced-Usages#wiki-using-texzilla-as-a-web-server">TeXZilla server-side conversion</a>.</li>
<li>Write some <a href="http://www.texample.net/tikz/examples/pgfplots/">TikZ demos</a> to show that <a href="https://github.com/brucemiller/LaTeXML">LaTeXML</a> can handle it.</li>
<li>Improve the <code>&lt;x-tex&gt;</code> element of TeXZilla without using any polyfill library.</li>
<li>Translate part of MathML documentation to your mother language. You can find more information in <a href="https://developer.mozilla.org/en-US/docs/MDN/Doc_status/MathML">this documentation status page</a>.</li>
</ul>
<h1 id="how-to-help">How to help</h1>
<ul>
<li>The patch for MathML in Wikipedia still need some review. If you know PHP please take a look at <a href="https://gerrit.wikimedia.org/r/#/projects/mediawiki/extensions/Math,dashboards/default" class="uri">https://gerrit.wikimedia.org/r/#/projects/mediawiki/extensions/Math,dashboards/default</a>.</li>
<li>The <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=981409">CSS fallback</a> for Chrome need to be in <a href="https://github.com/mozilla/kuma">MDN</a> source tree. If you already familiar with it or Django get in touch.</li>
</ul>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>MathML,Mozilla</p>
</div>
<div class="tags">
<p>MathML meetings</p>
</div>
<div class="comments">

</div>
