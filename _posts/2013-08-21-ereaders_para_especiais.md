---
layout: post
title: "Leitores eletrônicos para &quot;especiais&quot;"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Ontem apareceu uma ótima reportagem sobre acessibilidade (não em relação a preço, mas em adequação a usuários com necessidades especiais) de leitores eletrônicos no &quot;Good Ereaders&quot; que pode ser visualizada <a href="http://goodereader.com/blog/electronic-readers/ebook-readers-for-the-disabled-are-there-enough-choices-available">aqui</a> (<span data-role="download">backup nesse servidor &lt;ereaders-for-the-disabled.html&gt;</span>).</p>
<div class="more">

</div>
<p>Os leitores eletrônicos já encontram-se no mercado e com preço acessível a pelo menos 2 anos. Embora o formato EPUB3 seja baseado no padrão HTML de forma que pessoas com alguma deficiência visual não devessem ter &quot;muita&quot; dificuldade em utilizar esse formato de arquivo nos equipamentos já habituados a utilizar (leitores de telas tanto braile como oral) a quase totalidade dos equipamentos comercializados não encontra-se adaptado para nenhuma pessoa com algum tipo de deficiência como, por exemplo, o caso de pessoas com dificuldades motoras que não conseguem utilizar os dispositivos devido a tela touch ou botões inacessíveis.</p>
<p>A reportagem afirma que os três grandes fabricantes de leitores eletrônicos nos Estados Unidos (Sony, Amazon e Kobo) estão tentando não terem seus produtos enquadrados nas leis de acessibilidade pois eles apenas renderizam textos.</p>
<div class="note">
<div class="admonition-title">
<p>Note</p>
</div>
<p>Em uma das primeiras versões do Kindle existia uma porta para fone de ouvido e a funcionalidade &quot;Read to me&quot; (&quot;Leia para mim&quot; em tradução literal). Essa porta já não encontra-se nos modelos atuais e a funcionalidade também não.</p>
</div>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>e-coisas</p>
</div>
<div class="tags">
<p>ereader,acessibilidade</p>
</div>
<div class="comments">

</div>
