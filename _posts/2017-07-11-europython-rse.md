---
layout: post
title: "EuroPython 2017 - Lightning talk"
author: "Raniere Silva"
categories: python 
tags: [jekyll]
image:
  feature: 2017-07-11-europython-rse.jpg
  author: Sarahhoa
  original_url: https://flic.kr/p/KZhMoQ
  licence: CC-BY
---

[I decided to present a lightning talk today](https://twitter.com/rgaiacs/status/884659230363529216)
at [EuroPython 2017](https://ep2017.europython.eu).
The slides of my presentation are available [here]({{ site.url }}/slides/2017-07-11-europython-rse.pdf).


