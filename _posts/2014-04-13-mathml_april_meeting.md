---
layout: post
title: "MathML April Meeting"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>This is a report about the Mozilla MathML April IRC Meeting (see the announcement <a href="https://groups.google.com/forum/#!topic/mozilla.dev.tech.mathml/YIXvOb4VTHg">here</a>). The topics of the meeting can be found <a href="https://etherpad.mozilla.org/mathml-2014-04">in this PAD</a> (<span data-role="download">local copy of the PAD &lt;pad.txt&gt;</span>) and the <a href="http://logs.glob.uno/?c=mozilla%23mathml&amp;s=10%20Apr%202014&amp;e=10%20Apr%202014">IRC log</a> (<span data-role="download">local copy of the IRC log &lt;irc.txt&gt;</span>) is also available.</p>
<p>In the last 4 weeks the MathML team closed 13 bugs, worked in others 7 and open 2 new ones (this are only the ones tracked by Bugzilla).</p>
<p>The next meeting will be in May 15th at #mathml IRC channel. Please add topics in the <a href="https://etherpad.mozilla.org/mathml-2014-05">PAD</a>.</p>
<div class="more">

</div>
<h1 id="mathui-2014">MathUI 2014</h1>
<p>Do you know that the object of research of some people are Mathematical User Interfaces?</p>
<figure>
<img src="/images/libreoffice-ui.png" class="align-center" style="width:25.0%" />
</figure>
<p>Some of them you will be at <a href="http://cermat.org/events/MathUI/14/">MathUI 2014 9th Workshop on Mathematical User Interfaces</a> and hope that some folks of the MathML Team too.</p>
<p>The deadline for submission of works is May 15th, you still have one month.</p>
<h1 id="texzilla">TeXZilla</h1>
<p><a href="https://github.com/fred-wang/TeXZilla">TeXZilla</a> is the (La)TeX to MathML Javascript parser that Frédéric wrote. Now it can be used by SeaMonkey and many others applications that uses Gecko.</p>
<figure>
<img src="/images/seamonkey.png" class="align-center" style="width:25.0%" />
</figure>
<p>And I submit my demo to <a href="https://marketplace.firefox.com/app/texzilla-1/">MarketPlace</a>.</p>
<h1 id="firefox-os-tablet-contribution-program">Firefox OS Tablet Contribution Program</h1>
<p>I, Anuj and Frédéric will be part of the <a href="https://wiki.mozilla.org/FirefoxOS/TCP">Firefox OS Tablet Contribution Program</a>. We will receive a tablet with Firefox OS in the next couple of weeks to check for bugs (including MathML related).</p>
<p>If you have interest, please wait for news that we will publish as soon as we get the tablets.</p>
<h1 id="gecko">Gecko</h1>
<p>Among the contributions of the team we had:</p>
<ul>
<li>Anuj Agarwal <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=975747">convert gOperatorTable to a modern hashtable</a>;</li>
<li>James Kitchener solve the issue with <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=518592">dotless i's and j's in mi</a> and <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=986799">rtl mtable padding</a>;</li>
<li>Others fixes/patch review/bug report by Andrew McCreight, Daniel Holbert, Frédéric Wang, Jesse Ruderman, Karl Tomlinson, Khaled Hosny Robert O'Callahan, Ryan VanderMeulen.</li>
</ul>
<h1 id="how-to-help---newcomers">How to help - newcomers</h1>
<ul>
<li>Translate part of MathML documentation to your mother language. You can find more information in <a href="https://developer.mozilla.org/en-US/docs/MDN/Doc_status/MathML">this documentation status page</a>.</li>
</ul>
<h1 id="how-to-help">How to help</h1>
<ul>
<li>Need to document 4 bugs for Gecko 28 and 29.</li>
<li>The patch for MathML in Wikipedia still need some review. If you know PHP please take a look at <a href="https://gerrit.wikimedia.org/r/#/projects/mediawiki/extensions/Math,dashboards/default" class="uri">https://gerrit.wikimedia.org/r/#/projects/mediawiki/extensions/Math,dashboards/default</a>.</li>
</ul>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>MathML,Mozilla</p>
</div>
<div class="tags">
<p>MathML meetings</p>
</div>
<div class="comments">

</div>
