---
layout: post
title: "Academicos e hackers"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Em <span data-role="doc">../14/formatacao_de_trabalho</span> falo sobre minha revolta em ser obrigado a utilizar um formato fechado para enviar o resumo de um trabalho. Outra coisa que me revoltou nos últimos dias foi a diferença do mundo acadêmico e hacker que é muito bem descrita por Gabriella Coleman em seu livro “Coding Freedom: The Aesthetics and the Ethics of Hacking” (uma cópia eletrónica da obra é disponibilizada na <a href="http://gabriellacoleman.org/">página pessoal da autora</a> tanto em <a href="http://gabriellacoleman.org/Coleman-Coding-Freedom.pdf">pdf</a> como em <a href="http://gabriellacoleman.org/Coleman-Coding-Freedom.epub">epub</a> que é licenciado sob CC BY-NC-ND 2.5) no seguinte trecho:</p>
<blockquote>
<p>Unlike academics — who at times religiously guard their data or findings until published, or only circulate them among a small group of trusted peers — hackers freely share their findings, insights, and solutions. More than ever, and especially in the context of free software projects, hackers see their productive mutual aid as the underlying living credo driving free software philosophy, and the methodology of collaboration and openness. Hackers maintain that this mode of production is responsible for better hackers and better technology.</p>
</blockquote>
<div class="more">

</div>
<p>Como minha área de trabalho/pesquisa encontra-se na interseção entre acadêmicos e hackers acabo convivendo com esses dois tipos de pessoas. Considero-me muito mais um hacker do que um acadêmico, ao compartilhar livremente minhas descobertas, e minha revolta decorreu depois de conversar com um acadêmico.</p>
<p>Felizmente, nessa semana, um email diminuiu minha revolta. Esse email foi enviado por um &quot;conhecido&quot; com quem tinha trocado meia dúzia de emails anteriormente, Natham Cohen, para a lista <a href="https://groups.google.com/forum/?fromgroups#!forum/sage-devel">sage-devel</a>. Nesse email, copiado abaixo (<a href="https://groups.google.com/forum/?fromgroups#!topic/sage-devel/KmhKR0HRU2Y">thread aqui</a>), ele pergunta sobre transformar acadêmicos em hackers. :</p>
<pre><code>Helloooooooooooooooo everybody !!!

I write here because I have a question to ask to a large mathematical crowd
: a friend and I discussed at lunch, and we would like to do some &quot;open
research&quot;, that is to have some kind of wikipedia-like website for ...
research.
What we would put there is &quot;whatever we work on&quot;. Definitions, drafts,
actually the very tex files on which we work day after day. It would
contain the result of our experiments, and we would like to write there and
&quot;aloud&quot; all the notes we would take for ourselves when working on a
mathematical problem. We would not forget anything, and it would be public.

I ask this question here because you probably have opinions and advices
about this, because I remember that William once published his latex folder
on his web page... And because I am curious of what you would have to say,
all in all O_o

The point is that we may eventually end up with &quot;free scientific journals&quot;
at some point, but we could also do all this much earlier in the research
process. That is, *while* the research is going on.

What do you think ? It would be nice if some of you are interested to join
in, or if you know that something like that already exists so that we can
join it ourselves, or if you have a nice tool in mind to make this kind of
publication easier. Including LaTeX and pictures seems to be all we need,
as well as some nice way to create links between pages.

I&#39;m all ears. Have fun everybody !!!

Nathann</code></pre>
<p>Espero que essa transformação não demore muito.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>none</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
