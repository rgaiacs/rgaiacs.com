---
layout: post
title: Org Mode and Literate Programming
author: "Raniere Silva"
categories: ftb
tags: [ftb]
image:
  feature: ftb.jpg
---
Last week [Diego and I taught a Sofware Carpentry workshop at
Universidade Federal de Santa
Catarina](http://blog.rgaiacs.com/2014/12/13/swc_at_ufsc.html). During
the workshop, Diego mention [knirt](http://yihui.name/knitr/), a tool
for [literate programming](http://en.wikipedia.org/Literate_programming)
with Markdown and R. After the workshop I googled and found that you can
use [GNU Emacs](http://www.gnu.org/software/emacs/) for literate
programming. This post is about how to do it.

<div class="admonition">

Note

I will leave for another post to talk about knitr vs Org Mode.

</div>

Emacs
=====

First you will need to install Emacs.

![](/images/emacs-home.jpg){width="80%"}

If you install a new version of Emacs it will be ship with Org Mode and
Babel, the library that we need for literate programming.

After you install Emacs, you will need to add something like the
following lines to `~/.emacs`:

    (org-babel-do-load-languages
     'org-babel-load-languages
     '((emacs-lisp . nil)
       (R . t)))

The previous lines disable the evaluation of `emacs-lisp` code and
enable the evaluation of R code.

<div class="admonition">

Note

I will use R for this post but you can use Org Mode for **almost** any
language.

</div>

Org Mode Markup
===============

To start with Org Mode you only need to know that:

1.  `*` is used to mark headings.
2.  empty line is used to mark the end of one paragraph and the begin of
    another one.

![](/images/emacs-markup.jpg){width="80%"}

<div class="admonition">

Note

You can use AMS LaTeX notation with Org Mode.

</div>

Code block
==========

Code blocks are of the form :

    #+BEGIN_SRC LANG
    code
    #+END_SRC

where `LANG` is the language of the code block and `code` is your code.

![](/images/emacs-code.jpg){width="80%"}

Code Evaluation
===============

To evaluate the code, you can use the shortcut `C-c C-c` when the cursor
is in your code.

The code block will be evaluate and the result you be add.

![](/images/emacs-result.jpg){width="80%"}

Plots
=====

For plots you will need to help Org Mode by adding some informations at
the code block. For example you can use:

    #+begin_src R :results output graphics :file plot.png :width 400 :height 300
    samples <- c(1,2,3,4)
    plot(samples)
    #+end_src

When you evaluate the code it will create the file `plot.png` and the
result will be a link to that file.

![](/images/emacs-plot.jpg){width="80%"}

If you like to preview the plot you can use
`M-x org-display-inline-images`.

![](/images/emacs-inline.jpg){width="80%"}

Export
======

Org Mode can convert your `*.org` file to HTML and LaTeX. If you need to
convert your `*.org` to this **or other format** you can use
[Pandoc](http://pandoc.org). :

    $ pandoc -v | head -n 1
    pandoc 1.13.2
    $ pandoc -s -f org -t html -o sample.html sample.org
    % pandoc -s -f org -t odt -o sample.odt sample.org
    pandoc: Could not find image `file:plot.png', skipping...
    % pandoc -s -f org -t docx -o sample.docx sample.org
    pandoc: Could not find image `file:plot.png', skipping...

<div class="admonition">

Note

You can solve the warning about the image by replace `file:plot.png`
with `plot.png`.

I also [reported this bug](https://github.com/jgm/pandoc/issues/1823)
for Pandoc's developers.

</div>

<div class="admonition">

Note

You can check the convertion of `*.org` to HTML
&lt;sample.html&gt;, LibreOffice Writer format (or ODT)
&lt;sample.odt&gt; and Microsoft Office Word format (or DOCX)
&lt;sample.docx&gt;. I applied the quicly fix before converted \*.org.

</div>

Shortcuts
=========

  Shortcut      Command                       Action
  ------------- ----------------------------- ---------------------------------
  `C-x C-f`                                   Open file.
  `C-x C-s`                                   Save file.
  `C-x C-c`                                   Close Emacs.
  `C-c C-v d`   `org-babel-demarcate-block`   Create code block.
  `C-C '`                                     Edit code block (or exit it).
  `C-x C-s`                                   Will write the code block back.
  `C-c C-c`                                   Evaluate code blocks.
  `C-c C-v e`                                 Evaluate code blocks.
