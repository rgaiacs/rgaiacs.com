---
layout: post
title: "Apps Freezing for MathUI"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Today I update and freeze the apps for <a href="http://cermat.org/events/MathUI/14/">MathUI</a> Demo Session. At this post you will find some screenshots of the apps.</p>
<figure>
<img src="/images/home.png" class="align-center" style="width:50.0%" />
</figure>
<div class="more">

</div>
<div class="admonition">
<p>Note</p>
<p>The screenshots was took using the tablet from <a href="https://wiki.mozilla.org/FirefoxOS/TCP">Firefox OS Tablet Contribution Program</a> running this week build:</p>
<ul>
<li>OS version: 2.1.0.0-prerelease</li>
<li>Platform version: 33.0a1</li>
<li>Build Identifier: 20140703235229</li>
<li>Git commit info: 2014-0704 13:48:49 299b1836</li>
</ul>
<p>I <strong>must</strong> say that this build have a patch that provide math font support.</p>
</div>
<figure>
<img src="/images/version.png" class="align-center" style="width:50.0%" />
</figure>
<h1 id="math-cheat-sheet">Math Cheat Sheet</h1>
<p><a href="http://r-gaia-cs.github.io/math-cheat-sheet/">Math Cheat Sheet</a> is a list of popular math equations.</p>
<figure>
<img src="/images/math-cheat-sheet0.png" alt="Some equations from Math Cheat Sheet." class="align-center" style="width:50.0%" /><figcaption>Some equations from Math Cheat Sheet.</figcaption>
</figure>
<figure>
<img src="/images/math-cheat-sheet1.png" alt="Table of contents from Math Cheat Sheet." class="align-center" style="width:50.0%" /><figcaption>Table of contents from Math Cheat Sheet.</figcaption>
</figure>
<figure>
<img src="/images/math-cheat-sheet2.png" alt="More equations from Math Cheat Sheet." class="align-center" style="width:50.0%" /><figcaption>More equations from Math Cheat Sheet.</figcaption>
</figure>
<h1 id="texzilla">TeXZilla</h1>
<p><a href="http://r-gaia-cs.github.io/TeXZilla-webapp/">TeXZilla</a> let the user type (La)TeX math expressions and save it. It has some help keys for common expressions.</p>
<figure>
<img src="/images/texzilla0.png" alt="Homescreen of TeXZilla." class="align-center" style="width:50.0%" /><figcaption>Homescreen of TeXZilla.</figcaption>
</figure>
<figure>
<img src="/images/texzilla1.png" alt="TeXZilla and English keyboard." class="align-center" style="width:50.0%" /><figcaption>TeXZilla and English keyboard.</figcaption>
</figure>
<figure>
<img src="/images/texzilla2.png" alt="After type some text." class="align-center" style="width:50.0%" /><figcaption>After type some text.</figcaption>
</figure>
<p>The English keyboard still use the old layout. I already send a patch to update it to follow the <a href="https://bug983043.bugzilla.mozilla.org/attachment.cgi?id=8448520">last recommendation</a> but still need to send a unit/integration test.</p>
<figure>
<img src="/images/texzilla3.png" alt="After add/save the equation." class="align-center" style="width:50.0%" /><figcaption>After add/save the equation.</figcaption>
</figure>
<p>This app store the equations using IndexedDB what allows the equations to be restored when reopen the app.</p>
<h1 id="dynalgebra">DynAlgebra</h1>
<p><a href="http://r-gaia-cs.github.io/DynAlgebra/">DynAlgebra</a> is a proof of concept of do interactive algebra calculations.</p>
<figure>
<img src="/images/dynalgebra0.png" alt="Homescreen of DynAlgebra." class="align-center" style="width:50.0%" /><figcaption>Homescreen of DynAlgebra.</figcaption>
</figure>
<figure>
<img src="/images/dynalgebra1.png" alt="User must input equations using (La)TeX expressions." class="align-center" style="width:50.0%" /><figcaption>User must input equations using (La)TeX expressions.</figcaption>
</figure>
<figure>
<img src="/images/dynalgebra2.png" alt="After add some equation." class="align-center" style="width:50.0%" /><figcaption>After add some equation.</figcaption>
</figure>
<figure>
<img src="/images/dynalgebra3.png" alt="After click on some operations." class="align-center" style="width:50.0%" /><figcaption>After click on some operations.</figcaption>
</figure>
<p>DynAlgebra don't use any internal structure to handle the algebra manipulations. All the computations are based on MathML DOM structure. This approach has some issues due &quot;bad&quot; MathML trees but has the advantage that enable dynamic algebra computation to any MathML that available at the Web.</p>
<p>Unfortunately, right now, it don't save the equations.</p>
<h1 id="tex-editor">TeX Editor</h1>
<p><a href="http://r-gaia-cs.github.io/TeXEditor/">TeX Editor</a> is another proof of concept of write small texts using an subset of TeX system, right now the math environments.</p>
<figure>
<img src="/images/texeditor0.png" alt="Homescreen of TeX Editor." class="align-center" style="width:50.0%" /><figcaption>Homescreen of TeX Editor.</figcaption>
</figure>
<p>This app as design to be used as a test case for Firefox OS LaTeX keyboard.</p>
<figure>
<img src="/images/texeditor1.png" alt="Math Keyboard for Firefox OS." class="align-center" style="width:50.0%" /><figcaption>Math Keyboard for Firefox OS.</figcaption>
</figure>
<figure>
<img src="/images/texeditor2.png" alt="Alternative key when typing text." class="align-center" style="width:50.0%" /><figcaption>Alternative key when typing text.</figcaption>
</figure>
<figure>
<img src="/images/texeditor3.png" alt="Preview of the expession typed." class="align-center" style="width:50.0%" /><figcaption>Preview of the expession typed.</figcaption>
</figure>
<p>This app allow the user open and save files to SD Card. This function only works at Firefox OS due restrictions of API.</p>
<h1 id="feedbacks">Feedbacks</h1>
<p>I will love to get feedbacks about this apps and will write more about after MathUI.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>Firefox OS,MathML</p>
</div>
<div class="tags">
<p>MathUI2014,TCP,GSOC2014</p>
</div>
<div class="comments">

</div>
