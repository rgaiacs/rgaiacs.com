---
layout: post
title: "Movimento Passe Livre"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Nas últimas duas semanas ocorreu no Brasil uma série de manifestações que atenderam ao nome de &quot;Movimento Passe Livre&quot; e foram iniciadas pelo aumento das passagens do transporte público da maior cidade do país, São Paulo.</p>
<p>Nos último dias autores de blogs que acompanho escreveram um pouco sobre esse movimento. A seguir você encontra uma seleção destes posts.</p>
<div class="more">

</div>
<h1 id="manifestação-em-livros">Manifestação em livros</h1>
<p><a href="http://www.ebookbr.com/2013/06/manifestacao-em-livros.html">Esse</a> (<span data-role="download">cópia local &lt;manifestacao-em-livros.html&gt;</span>) foi um dos melhores posts sobre as manifestações que eu li. Embora o autor não seja fã do PT ele escreveu muito bem.</p>
<h1 id="os-vintes-centavos-mais-valiosos-do-brasil">Os vintes centavos mais valiosos do Brasil</h1>
<p><a href="http://xaoquadrado.wordpress.com/2013/06/19/os-vinte-centavos-mais-valiosos-do-brasil/">Nesse post</a> (<span data-role="download">cópia local &lt;os-vinte-centavos-mais-valiosos-do-brasil.html&gt;</span>) fala da sua experiência com o transporte púbico de Campinas.</p>
<h1 id="é-hora-de-voltar-para-casa">É hora de voltar para casa</h1>
<p><a href="http://xaoquadrado.wordpress.com/2013/06/21/e-hora-de-voltar-para-casa/">Nesse outro post</a> (<span data-role="download">cópia local &lt;e-hora-de-voltar-para-casa.html&gt;</span>), da mesma autora de &quot;Os vintes centavos mais valiosos do Brasil&quot;, encontra-se uma ótima reflexão do que deve-se ser feito continuamente.</p>
<h1 id="contra-corrupção">Contra corrupção</h1>
<p><a href="http://olhapravoce.blogspot.com/2013/06/contra-corrupcao.html">Já esse post</a> (<span data-role="download">cópia local &lt;contra-corrupcao.html&gt;</span>) a autora mostra sua indignação com o país.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>none</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
