---
layout: post
title: "Git Lesson at SECOMP"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>SECOMP is the name of Computer Science Week that happens at University of Campinas. They invite me to teach Git last Friday and here you will find some thoughts about it.</p>
<div class="more">

</div>
<h1 id="sinopse">Sinopse</h1>
<p>I used Software Carpentry's Git lesson because (1) I like it, (2) is the best way to test it, (3) as Software Carpentry contributor is one way to advertise it.</p>
<p>For Git lesson, Software Carpentry used as example the case of a paper that Wolfman and Dracula have to write about a new expedition to Mars. Since this course was targeting computer science students I choose to use Star Wars saga as the source for the example. At the first part we cover the basic local Git commands and at the second part we cover the remote commands.</p>
<p>The big differences from Software Carpentry's lesson was that (1) instead of use two local copies of the remote repository I use the online interface of GitHub because I believe that this make easy for students understand the commands and (2) instead of <code>push</code> I teach <code>fetch</code> and <code>merge</code> (right now Software Carpentry's instructors are discussing if we should or not do this change).</p>
<h1 id="feedbacks">Feedbacks</h1>
<p>In general student liked the course. The comment that call my attention was that commands presented at the second part was a little fast (specially if compared with the first part). <strong>I still trying to figure out how to know if I need to slow down my speed.</strong></p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>Software Carpentry,Mozilla Brasil</p>
</div>
<div class="tags">
<p>Git,SECOMP,UNICAMP</p>
</div>
<div class="comments">

</div>
