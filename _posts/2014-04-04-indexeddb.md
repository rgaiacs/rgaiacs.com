---
layout: post
title: IndexedDB
author: "Raniere Silva"
categories: ftb
tags: [ftb]
image:
  feature: ftb.jpg
---
IndexedDB é uma forma de guardar informações no browser. Neste post
vamos criar uma página que possui um campo de texto e um botão que ao
pressionado salva o conteúdo da caixa de texto.

Layout
======

O layout é bem simples. Download do layout aqui. &lt;00.html&gt;

![](/images/00.png){width="25%"}

Banco de Dados
==============

Banco de dados é consiste de um ou mais objetos de armazenamento. Cada
banco de dados é identificado por um nome e uma versão.

Para interagir com um banco de dados é preciso abrir uma conexão.

Download da criação do banco de dados. &lt;01.html&gt;

Objetos de Armazenamentos
=========================

Os bancos de dados são organizados por objetos de armazenamento nos
quais os dados são armazenados como pares chave-valor.

Ao criar o banco de dados é importante criar o objeto de armazenamento
que é identificado por um nome.

Download da criação do objeto de armazenamento. &lt;02.html&gt;

Transações
==========

Transações são a forma como adicionamos, recuperamos e removemos dados
dos objetos de armazenamento (e consequentemente do banco de dados).
Cada transação é identificada por um conjunto de objetos de
armazenamentos e a permissão de acesso.

<div class="admonition hint">

Sempre especificque apenas os objetos de armazenamentos que irá
precisar.

</div>

<div class="admonition hint">

Apenas espcifique permissão `readwrite` quando necessário.

</div>

Download da criação da transaçãonto. &lt;03.html&gt;

Requisição de Objeto de Armazenamento
=====================================

Depois que a transação foi criado, é possível consultar e alterar os
objetos de armazenamentos associados a ela.

Download da requisição do objeto de armazenamento. &lt;04.html&gt;

Adicionando Elementos
=====================

Para adicionar novos elementos utiliza-se o método `add()` do objeto de
armazenamento que possui como argumento um JSON.

Download da função de adicionar nova entrada. &lt;05.html&gt;
