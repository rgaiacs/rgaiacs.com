---
layout: post
title: "GSoC: June 30 - August 03"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<div class="admonition">
<p>Note</p>
<p>Sorry to not write about my Google Summer of Code project last week.</p>
</div>
<p>This is one of the reports about my GSoC project and cover the tenth and eleventh week of “Students coding”.</p>
<p>At this two weeks I work on small issues at my keyboard and some bugs at the native keyboard that will improve my one.</p>
<p>Bellow you will find more details about the past two week and the plans for the last two final weeks.</p>
<div class="more">

</div>
<h1 id="review-of-proposal">Review of Proposal</h1>
<p>From <a href="https://www.google-melange.com/gsoc/proposal/public/google/gsoc2014/r_gaia_cs/5629499534213120">my proposal</a> all the big points was addressed. One that wasn't is the plain math layout because it is <strong>very easy</strong> since it didn't need the changes from the math latex layout.</p>
<h1 id="rebase">Rebase</h1>
<p>During my GSoC, at the begin of every week I rebase my work to Gaia's HEAD to avoid conflicts &quot;impossible&quot; to resolve. Once again I had some problems due <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=1023729">some changes at Gaia's keyboard source code</a> but this was already solved. Thanks very much to Tim Guan-tin Chien for the help in solving my problem.</p>
<p>Value and CompositeKey Fields for Alternative Keys --------------------------------------------------</p>
<p>Also know as <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=1011482">Bug 1011482</a> this was one of my first patch and we are trying to push it to Gaia's source tree. I need it to provide, for example, variants of integral key.</p>
<p><strong>Status</strong>: patch sent.</p>
<h1 id="requesting-uppercase-and-lowercase-for-compositekey">Requesting Uppercase and Lowercase for CompositeKey</h1>
<p>Also know as <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=1011661">Bug 1011661</a> this is need because Greek letters have an different rule to lowercase and uppercase.</p>
<p><strong>Status</strong>: patch sent.</p>
<h1 id="alternate-keys-list-overflows">Alternate Keys List Overflows</h1>
<p>Also know as <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=934209">Bug 934209</a> this is a little old bug (almost one year old) that only affect a few layouts. Since it affect my layout I spend some time trying to address it. This isn't a easy one and I lost some time trying to figure out how to reorder the list of alternate keys to be at the form recommended by the UX team.</p>
<p><strong>Status</strong>: prototype sent.</p>
<h1 id="others-bugs">Others Bugs</h1>
<ul>
<li>Rebased and updated <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=1022609">Bug 1022609</a> and <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=1015309">Bug 1015309</a> - symbol layout.</li>
<li>Reported <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=1047875">Bug 1047875</a>
<ul>
<li>problem with home button.</li>
</ul></li>
<li>Reported <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=1047998">Bug 1047998</a>
<ul>
<li>problem with long number at dial.</li>
</ul></li>
</ul>
<h1 id="plans-for-august-04---august-10">Plans for August 04 - August 10</h1>
<ul>
<li>Build prototype to send to marketplace.</li>
<li>Tests.</li>
</ul>
<h1 id="plans-for-august-11---august-17">Plans for August 11 - August 17</h1>
<ul>
<li>Polish the layout.</li>
<li>Send to marketplace.</li>
</ul>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>GSOC</p>
</div>
<div class="tags">
<p>GSOC2014</p>
</div>
<div class="comments">

</div>
