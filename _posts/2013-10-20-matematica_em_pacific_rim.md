---
layout: post
title: "Matemática em Pacific Rim"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p><a href="http://www.imdb.com/title/tt1663662/?ref_=nv_sr_1">Pacific Rim</a> em um filme de 2013 que assisti essa semana no qual somos atacados por monstros gigantes vindos do fundo do mar e por causa disso desenvolvemos robôs gigantes para lugar contra esses monstros.</p>
<p>Uma cena do filme achei muito boa (<span data-role="download">assista ela aqui
&lt;math-in-pacific-rim.webm&gt;</span>) na qual um matemático/físico (não lembro se é informado a formação do personagem) fala sobre sua teoria que foi desenvolvido utilizando a combinação de quadro-negro e giz para depois mostrar uma simulação por meio de um sistema holográfico.</p>
<div class="more">

</div>
<p>Isso mesmo, no filme fomos capazes de criar projetores holgráficos e robôs gigantes mas fomos incapazes de substituir o quadro-negro e giz para a comunidade científica.</p>
<p>Em um <span data-role="doc">post anterior &lt;../10/mathml_js&gt;</span> eu disse que estava trabalhando em uma &quot;solução&quot; para esse problema e gostaria de avisar que já encontra-se disponível em prova conceitual em <a href="http://r-gaia-cs.github.io/mathml.js" class="uri">http://r-gaia-cs.github.io/mathml.js</a>.</p>
<div class="note">
<div class="admonition-title">
<p>Note</p>
</div>
<p>Também submeti uma palestra para sobre meu projeto para a próxima edição da <a href="http://www.campus-party.com.br/2014/index.html">Campus Party</a>. A submissão de palestras na área de software livre acaba hoje, corra que ainda dá tempo de submeter sua apresentação por meio <a href="http://www.slcampusparty.com.br/chamada-de-trabalhos">desse formulário</a>.</p>
</div>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>MathML,Campus Party</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
