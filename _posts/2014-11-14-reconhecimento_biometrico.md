---
layout: post
title: "Senhas e Reconhecimento Biometrico"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Ontem eu fui no shopping para comprar algumas coisas e aproveitei para passar no banco. Nessa agência, todos os terminais de auto atendimento possuem leitor biométrico (digital) e você só pode fazer qualquer transação após realizar seu cadastro biométrico. Neste post vou tentar lhe explicar porque você deveria evitar utilizar biometria como senha.</p>
<div class="more">

</div>
<h1 id="two-factor-authentication">Two Factor Authentication</h1>
<p><a href="https://en.wikipedia.org/wiki/Two_factor_authentication">Two factor authentication</a> (ou <a href="https://pt.wikipedia.org/wiki/Identifica%C3%A7%C3%A3o_por_dois_fatores">identificação por dois fatores</a> como eu resolvi traduzir na Wikipédia) consiste no fato de utilizar duas informações para provar que você realmente é você. As informações utilizadas podem ser divididas em três categorias:</p>
<ul>
<li>algo que o usuário sabe, e.g. uma senha;</li>
<li>algo que o usuário possui, e.g. um cartão magnético;</li>
<li>algo &quot;inerente&quot; ao usuário, e.g. sua digital.</li>
</ul>
<p>A identificação por dois fatores é largamente utilizada no dia-a-dia. Se você ainda não conseguiu ligar os pontos alguns exemplos são:</p>
<ol type="1">
<li>operações bancárias onde você precisa do seu cartão e da sua senha,</li>
<li>voto nas eleições presidenciais onde você precisa do seu título eleitoral e da sua assinatura,</li>
<li>entrada na repartição de trabalho onde você precisa do seu crachá e do &quot;seu rosto&quot;.</li>
</ol>
<h1 id="reuso-de-senhas">Reuso de Senhas</h1>
<p>Como você já deve ter ouvido falar, não é muito seguro reutilizar suas senhas. Na <a href="https://en.wikipedia.org/wiki/Password">Wikipédia</a> encontramos</p>
<blockquote>
<p>&quot;It is common practice amongst computer users to reuse the same password on multiple sites. This presents a substantial security risk, since an attacker need only compromise a single site in order to gain access to other sites the victim uses. This problem is exacerbated by also reusing usernames, and by websites requiring email logins, as it makes it easier for an attacker to track a single user across multiple sites. Password reuse can be avoided or minimused by using mnemonic techniques, writing passwords down on paper, or using a password manager.&quot;</p>
</blockquote>
<p>Infelizmente, com o crescente número de serviços que utilizamos acaba sendo comum o reuso das senhas. Também na Wikipédia encontramos</p>
<blockquote>
<p>&quot;It has been argued by Redmond researchers Dinei Florencio and Cormac Herley, together with Paul C. van Oorschot of Carleton University, Canada, that password reuse is inevitable, and that users should reuse passwords for low-security websites (which contain little personal data and no financial information, for example) and instead focus their efforts on remember long, complex passwords for a few important accounts, such as banks accounts.&quot;</p>
</blockquote>
<h1 id="juntando-os-pontos">Juntando os Pontos</h1>
<p>Se especialistas dizem que devemos evitar reutilizar senhas porque eu devo utilizar minha digital (ou outro identificador biométrico) como minha senha nos vários serviços que sou <strong>obrigado</strong> a utilizar dado que minha digital não vai mudar? Me parece que tem algo muito errado nisso.</p>
<h1 id="extrapolações">Extrapolações</h1>
<p>No livro <a href="http://www.sfwriter.com/scmi.htm">Mindscan</a> existe um ótimo exemplo de um dos problemas em utilizar biometria para identificação.</p>
<h1 id="conclusões">Conclusões</h1>
<p>Dados os problemas em utilizar biometria como senha falta agora procurar por alternativas.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>internet,liberdade,LPBRSP,privacidade</p>
</div>
<div class="tags">
<p>Mindscan,Robert J. Sawyer</p>
</div>
<div class="comments">

</div>
