---
layout: post
title: "Qualidade de código"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Mike Jacksob escrever um ótimo post, &quot;<a href="http://software-carpentry.org/blog/2013/08/intecol13-good-code.html">What Makes Good Code Good at INTECOL13</a> (<span data-role="download">cópia nesse servidor &lt;intecol13-good-code.html&gt;</span>) no blog da Software Carpentry falando sobre qualidade de código.</p>
<div class="more">

</div>
<h1 id="características-de-um-bom-código">Características de um bom código</h1>
<ul>
<li>Legível,</li>
<li>Bem projetado,</li>
<li>Correto,</li>
<li>Robusto,</li>
<li>Eficiente,</li>
<li>Flexível,</li>
<li>Documentando,</li>
<li>Livre/Aberto,</li>
<li>Refereciável,</li>
<li>Versionado,</li>
<li>Disponível,</li>
<li>Usável.</li>
</ul>
<h1 id="motivos-de-péssimos-códigos-acadêmicos">Motivos de péssimos códigos acadêmicos</h1>
<ul>
<li>&quot;Egoísmo&quot;,</li>
<li>Falta de tempo,</li>
<li>Falta de prêmio,</li>
<li>Falta de treinamento,</li>
<li>Falta de suporte (por programadores experientes),</li>
<li>Falta de divulgação.</li>
</ul>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>academia,código</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
