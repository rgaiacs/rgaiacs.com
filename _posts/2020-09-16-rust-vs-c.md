---
layout: post
title: "Teach Rust! Do NOT Teach C!"
author: "Raniere Silva"
categories: coding
tags: ["Rust", "C", "programming languages", "sustainability"]
image:
  feature: 2020-09-16-rust-vs-c.jpg
  author: John Schnobrich
  licence: Unsplash License
  original_url: https://unsplash.com/photos/FlPc9_VocJ4
---

In March 2020,
I enrolled in a Master in Computer Science
and
**all** students **must** attend the "Analysis of Algorithms" course.
This "Analysis of Algorithms" course has a few projects
and
the first project is to implement a [trie](https://en.wikipedia.org/wiki/Trie),
also called digital tree or prefix tree,
in C.

Students' experience with C varies
and
the project throw the students under the bus!
For students without previous contact with
project organisation,
documentation,
testing
and
other software programming practices,
use C for this project might re-inforce unsustainable software development.

I was reading the official Rust [Getting started](https://www.rust-lang.org/learn/get-started) page
and
the "Hello, world!" mentions everything necessary for a new user:

- how to make a new project? `cargo new hello-rust`
- how to keep metadata for the project? `Cargo.toml`
- how to keep dependencies for the project? `Cargo.toml`
- how to compile the source code? `cargo build`
- how to run the project? `cargo run`
- how to run the project's test? `cargo test`
- how to build the project's documentation? `cargo doc`

For C,
we don't have a official "Getting started" page
so
we will get the first result from Google.

{% include figure.html filename="2020-09-16-rust-vs-c-web-search.jpg" alternative_text="Screenshot of search using Google" caption="Screenshot of search using Google." author="Raniere Silva" licence="CC-BY" %}

ev3dev's [Getting Started with C](https://www.ev3dev.org/docs/tutorials/getting-started-with-c/)
is fabulous because it mentions `Makefile`,
it helps towards the project sustainability.
Unfortunately,
it doesn't cover metadata, dependencies, tests, and documentation.
Other introductions to C,
for example Wikibooks' [C Programming/Intro exercise](https://en.wikibooks.org/wiki/C_Programming/Intro_exercise)
doesn't even mention `Makefile`.

Next time,
consider require students to use a programming language
that has a lower bar to new users regarding sustainability!