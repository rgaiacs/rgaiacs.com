---
layout: post
title: "Sea Point"
author: "Raniere Silva"
categories: teaching
tags: ["Africa", "South Africa", "Cape Town"]
image:
  feature: 2019-09-13-sea-point.jpg
  author:  Raniere Silva
  licence: CC-BY
---

A few photos from Cape Town.

{% include figure.html filename="2019-09-13-sea-point-2.jpg" alternative_text="Photo of Cape Town." caption="North view from Sea Point." author="Raniere Silva" licence="CC-BY" %}

{% include figure.html filename="2019-09-13-sea-point-3.jpg" alternative_text="Photo of Cape Town." caption="South view from Sea Point." author="Raniere Silva" licence="CC-BY" %}

{% include figure.html filename="2019-09-13-sea-point-4.jpg" alternative_text="Photo of art sculture." caption="Rhinoceros sculture." author="Raniere Silva" licence="CC-BY" %}