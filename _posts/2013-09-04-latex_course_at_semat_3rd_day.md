---
layout: post
title: "LaTeX Course at 3rd Day of Semat"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Today was the third and last day of my course about LaTeX that I talk (<span data-role="doc">in this post &lt;../01/latex_course&gt;</span>). Around 10 students attend it and you will find more information about that below.</p>
<div class="more">

</div>
<p>Today we have some problems with the room that we use in the others days since it has two courses allocated. We have to go to other room that haven't a video projector and took around 30 minutes to set it up.</p>
<p>When I finally start the class I try to follow the plan with the pads (<span data-role="download">one for the contents &lt;semat-latex3.txt&gt;</span> and <span data-role="download">other for
feedbacks &lt;../02/semat-feedback-latex.txt&gt;</span>) and the rounds:</p>
<dl>
<dt>Round 3.1</dt>
<dd><p>Yesterday I forgot to show a very common error that novices in LaTeX do. I show this error and explain it.</p>
</dd>
<dt>Round 3.2</dt>
<dd><p>Show how to insert bitmap pictures in LaTeX. It took more time than I expect.</p>
</dd>
<dt>Round 3.3</dt>
<dd><p>Show how to produce tables in LaTeX. Some things with tables are similar for the same thing with pictures and for that I go faster than I first planned.</p>
</dd>
<dt>Round 3.4 (Extra)</dt>
<dd><p>After the oficial end of the class I show some TikZ examples.</p>
</dd>
</dl>
<h1 id="change-of-ide">Change of IDE</h1>
<p>The computers in the new room haven't installed the IDE that we use in the previous days. We lost some time to the students can get how to use the new IDE.</p>
<p>This is one example of why the command line is a good thing. Once you learn it will always can go to it and do what you want.</p>
<h1 id="concept-map">Concept Map</h1>
<p>In my first plan for the course I think in ask students to do a second concept map in the end of the course. Because of the problem with the room I haven't time to do it and have no sure if it can be use to get what the students learn in the class.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>educação,ensino,Software Carpentry</p>
</div>
<div class="tags">
<p>LaTeX</p>
</div>
<div class="comments">

</div>
