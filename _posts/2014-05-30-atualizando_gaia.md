---
layout: post
title: Atualizando o Gaia do Firefox OS
author: "Raniere Silva"
categories: ftb
tags: [ftb]
image:
  feature: ftb.jpg
---
<div class="admonition important">

Este post foi atualizado em 06 de Junho de 2014.

</div>

Em um post anterior &lt;../../03/29/hacking\_gaia&gt; foi apresentado
como atualizar o Gaia no [Simulador do Firefox
OS](https://ftp.mozilla.org/pub/mozilla.org/labs/fxos-simulator/) que é
utilizado pelo App Manager. Nesse post será apresentado como atualizar o
Gaia presente em um dos seus dispositivos.

<div class="admonition note">

Passei muito tempo tentando compilar todo o Firefox OS sem necessidade
porque não tinha encontrado como apenas atualizar o Gaia. Instruções de
como atualizar o Gaia já estavam presentes no artigo [Hacking
Gaia](https://developer.mozilla.org/en-US/Firefox_OS/Platform/Gaia/Hacking)
do MDN mas eu ainda não tinha notado. Acabei fazendo [uma
edição](https://developer.mozilla.org/en-US/Firefox_OS/Platform/Gaia/Hacking$compare?to=606285&from=601471)
nesse artigo para deixar essa informação mais visível.

</div>

Preparação
==========

1.  Baixar o repositório gaia:

        $ git clone git://github.com/mozilla-b2g/gaia.git

2.  Acessar o diretório criado:

        $ cd gaia

Compilando e Atualizando
========================

<div class="admonition note">

Se ao executar os comandos presentes nesse artigo tiver com a mensagem
`waiting for device>` sugiro tentar o mesmo comando como super usuário
(também conhecido como root).

Agradecimentos ao Rodrigo Waters por avisar desse problema.

</div>

Para compilar o Gaia e atualizá-lo no dispositivo conectado utilize:

    $ make install-gaia

Ao final do processo de atualização o dispositivo será reiniciado.

<div class="admonition note">

`install-gaia` preserva as configurações de usuário. Para compilar o
Gaia e atualizar o dispositivo removendo as configurações antigas
utilize:

    $ make reset-gaia

</div>

<div class="admonition note">

É possível atualizar apenas um dos aplicativos que compõe o Gaia
utilizando:

    $ APP=nome-do-aplicativo-desejado make install-gaia

Infelizmente ao tentar essa opção tive problemas.

</div>

Errors e Solução Durante a Compilação e Atualização
===================================================

Até o momento os erros que tive foram decorrentes de alterações minhas
no `Makefile` ou em algum dos aplicativos.

Erros e Soluções após a Atualização
===================================

<div class="admonition error">

Dispositivo trava ao tentar iniciar o Gaia

Esse erro deve ocorrer pelo fato do Gaia precisar rodar como uma
*packaged app*. Resolvi esse problema utilizando:

    $ PRODUCTION=1 make install-gaia

</div>

Outras Opções de Compilação
===========================

Algumas opções de compilação que podem ser úteis:

`DEVICE_DEBUG=1`

:   Habilita a possibilidade de depuração pelo ADB.

`REMOTE_DEBUGGER=1`

:   Habilita a opção de depuração remota.

`GAIA_OPTIMIZE=1`

:   Comprime os aplicativos.

`HAIDA=1`

:   Habilita as funcionalidades do [Projeto
    Haida](https://wiki.mozilla.org/FirefoxOS/Haida).

`NOFTU=1`

:   Desabilita a ajuda na primeira vez que o dispositivo é ligado.

`NO_LOCK_SCREEN=1`

:   Desabilita o travamento da tela.

Adicionando dados
=================

<div class="admonition note">

Para isso você vai precisar do utilitário `mid3v2`.

</div>

Para adicionar dados e arquivos de exemplos:

    $ make reference-workload-light

<div class="admonition note">

Se você precisar de mais exemplos troque `light` por `medium`, `heavy`
ou `x-heavy` no comando anterior.

</div>

Compilando e Atualizando o Flatfish
===================================

<div class="admonition note">

Evite utilizar a opção `DEVICE_DEBUG=1` porque o Flatfish "não gosta
dela". Maiores informações no [Bug
1018385](https://bugzilla.mozilla.org/show_bug.cgi?id=1018385).

</div>

<div class="admonition note">

Evite utilizar versões que possuem como padrão o Homescreen vertical,
introduzido na versão `2210dbd0a581fc319c6e44bb1d06f8e9bd796058`, pois
ele não funciona corretamente no Flatfish (a versão
`a1d1ccfc56f9af8cd4c5995c43e5557d7bee0e33` foi a mais nova que funcionou
comigo). Maiores informações no [Bug
1021327](https://bugzilla.mozilla.org/show_bug.cgi?id=1021327).

</div>

Para atualizar o Flatfish deve-se utilizar:

    $ GAIA_DEVICE_TYPE=tablet make install-gaia

Exceto pelo [Bug
1018385](https://bugzilla.mozilla.org/show_bug.cgi?id=1018385), os
problemas e soluções anteriores são válidos.

Exemplo
=======

No meu hamachi, também conhecido como Alcatel One Touch, tinha
disponíveis os teclados:

-   Number,
-   Deutsch,
-   ???,
-   English,
-   Español e
-   Français.

![](/images/old-keyboard-list.png){width="33%"}

Para atualizar o Gaia e alterar os teclados disponíveis utilizei:

    $ GAIA_KEYBOARD_LAYOUTS=en,en-Colemak,en-Dvorak,en-Neo,pt-BR PRODUCTION=1 HAIDA=1 NOFTU=1 DEVICE_DEBUG=1 REMOTE_DEBUGER=1 make reset-gaia

Depois da atualização os teclados disponíveis são:

-   Number,
-   English,
-   Colemak,
-   Dvorak,
-   Neo e
-   Português.

![](/images/new-keyboard-list.png){width="33%"}
