---
layout: post
title: Compilando o Emulador do Firefox OS
author: "Raniere Silva"
categories: ftb
tags: [ftb]
image:
  feature: ftb.jpg
---
<div class="admonition note">

Este post encontra-se em construção.

</div>

Neste post será apresentado como compilar o emulador do Firefox OS.

Preparação
==========

1.  Baixar o repositório B2G:

        $ git clone git://github.com/mozilla-b2g/B2G.git

2.  Acessar o diretório criado:

        $ cd B2G

3.  Configurar o diretório para `emulator-x86` (se estiver utilizando um
    sistema operacional 32-bits utilize `emulator`):

        $ ./config.sh emulator-x86 2>&1 | tee /tmp/firefoxos-config

<div class="admonition note">

No comando anterior, `2>&1 | tee /tmp/firefoxos-config` serve para além
de informar o progresso também criar o arquivo `/tmp/firefoxos-config`
com o log do processo de configuração.

</div>

Compilando
==========

<div class="admonition note">

Essa etapa deve demorar aproximadamente 4 horas para ser concluída.
Recomenda-se que enquanto sua máquina compila o emulador do Firefox OS
você faça outro trabalho.

</div>

Para compilar:

    $ ./build.sh 2>&1 | tee /tmp/firefoxos-build

<div class="admonition note">

No comando anterior, `2>&1 | tee /tmp/firefoxos-build` serve para além
de informar o progresso também criar o arquivo `/tmp/firefoxos-build`
com o log do processo de compilação.

</div>

Espero que sua compilação não falhe. Se falhar, espero que encontre a
solução na lista de problemas que tive.

Erros e Soluções Durante a Compilação
=====================================

<div class="admonition error">

`/usr/bin/ld: cannot find -lX11`

Você precisa da biblioteca X11, versão 32-bits, que faz parte do Xorg.
Algumas distribuições GNU/Linux separam as libs em pacotes separados,
e.g. Debian. Instale a lib:

    # apt-get install libx11-dev:i386

e compilar novamente.

</div>

<div class="admonition error">

`/usr/bin/ld: cannot find -lGL`

Semelhante ao erro anterior:

    # apt-get install ibgl1-mesa-dev* libglu1-measa-dev*

</div>

<div class="admonition error">

`configure: error: yasm is a required build tool for this architecture ...`

A solução é instalar `yasm`:

    # apt-get install yasm

</div>

Executando
==========

Quando tiver terminado de compilar:

    $ ./run-emulator.sh

O emulador será iniciado.

Erros e Soluções ao Executar
============================

<div class="admonition error">

`Failed to load libGL.so`

    # apt-get install libgl1-mesa-dev:i386

</div>

<div class="admonition error">

`emulator: No kvm device file detected`

</div>

<div class="admonition error">

`emulator: can't connect to ADB server: Connection refused`

</div>

Referências
===========
