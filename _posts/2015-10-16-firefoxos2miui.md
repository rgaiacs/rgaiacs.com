---
layout: post
title: "Do Firefox OS para o MIUI"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<div class="admonition">
<p>Nota</p>
<p>Em <a href="https://blog.mozilla.org/blog/2015/12/09/firefox-os-pivot-to-connected-devices/">09 de Dezembro a Mozilla anunciou</a> que estaria encerrando suas atividades voltadas ao desenvolvimento do Firefox OS para celulares.</p>
</div>
<p>Usei o <a href="https://www.mozilla.org/en-US/firefox/os/1.1/">Firefox OS</a> por mais de um ano no meu celular principal.</p>
<p><img src="/images/firefoxos-home.png" alt="Screenshot do Firefox OS." class="align-center" style="width:80.0%" /></p>
<p>Embora o sistema operacional fosse novo gostei bastante de utilizá-lo por consegui-lo hackear, utilizava minha própria build com alguns patches adicionais. Infelizmente, de Julho para cá, algumas coisas me deixaram insatisfeitos:</p>
<ol type="1">
<li>o teclado virtual estava muito lento <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=1122274">devido a regressão</a>;</li>
<li>minhas builds estavam quebradas, provavelmente devido a outra regressão que não é fácil localizar devido aos vários repositórios utilizados durante a compilação além do dispositivo que utilizava não ter mais suporte oficial;</li>
<li>nas últimas semanas a compilação do Firefox OS simplesmente falhava.</li>
</ol>
<p>Depois de muita consideração, resolvi abandonar o Firefox OS e experimentar o <a href="https://en.wikipedia.org/wiki/MIUI">MIUI</a> que pode ser considerado um fork do <a href="http://www.cyanogenmod.org/">CyanogenMod</a>, que é um fork do Android, desenvolvido pela Xiaomi.</p>
<p><img src="/images/miui-home.png" alt="Screenshot do MIUI." class="align-center" style="width:80.0%" /></p>
<div class="more">

</div>
<h1 id="por-que-miui">Por que MIUI?</h1>
<p><img src="/images/miui-info.png" alt="Screenshot das configurações do MIUI." class="align-center" style="width:80.0%" /></p>
<p>Queria um dispositivo com <a href="http://www.replicant.us/">Replicant</a> mas os <a href="http://www.replicant.us/supported-devices.php">dispositivos suportados</a> não são fáceis de encontrar atualmente.</p>
<p>Pela indisponibilidade de um dispositivo rodando Replicant queria um dispositivo com CyanogenMod. Existe uma longa lista de dispositivos suportados pelo CyanogenMod tanto <a href="https://wiki.cyanogenmod.org/w/Devices">oficialmente</a> como <a href="https://wiki.cyanogenmod.org/w/Unofficial_Ports">não-oficialmente</a> e depois de olhar vários dispositivos acabei escolhendo o <a href="http://br.mi.com/redmi2-pro">Xiaomi Redmi 2 Pro</a>. O Xiaomi Redmi 2 Pro <strong>não possui suporte ao CyanogenMod ofialmente e nem não-oficialmente</strong> mas imaginei que não seria muito difícil criar um port do CyanogenMod para ele tomando como ponto de parte o suporte <strong>não-oficial</strong> existente para o Xiaomi Redmi 2 dado que a versão Pro possui apenas mais memória (RAM e de armazenamento).</p>
<p>No XDA Forums existe uma thread sobre o <a href="http://forum.xda-developers.com/android/development/hm201481x-cyanogenmod-11-0-t3159419">CyanogenMod 11.0</a> e outra sobre o <a href="http://forum.xda-developers.com/android/development/redmi-2-cyanogenmod-12-1-t3189949">CyanogenMod 12.1</a>. Os repositórios no GitHub são <a href="https://github.com/zwliew/android_device_xiaomi_hm2014811">android_device_xiaomi_hm2014811</a>, <a href="https://github.com/zwliew/proprietary_vendor_xiaomi">proprietary_vendor_xiaomi</a> e <a href="https://github.com/zwliew/android_kernel_xiaomi_msm8916">android_kernel_xiaomi_msm8916</a>.</p>
<p>Por que eu não escolhi um aparelho com suporte oficial como o Motorola Moto G? Porque o aparelho vendido no Brasil também não tem suporte oficial.</p>
<p>Vamos ver se consigo utilizar o CyanogenMod no Xiaomi Redmi 2 Pro.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>Firefox OS,Android,Mozilla Brasil</p>
</div>
<div class="tags">
<p>Firefox OS,MIUI,Xiaomi,Redmi 2 Pro</p>
</div>
<div class="comments">

</div>
