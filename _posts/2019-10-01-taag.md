---
layout: post
title: "Flying with TAAG"
author: "Raniere Silva"
categories: travel
tags: ["Africa"]
image:
  feature: 2019-10-01-taag.jpg
  author:  Raniere Silva
  licence: CC-BY
---

My flight from Windhoek to São Paulo with a connection in Luanda was operated by TAAG.
One of the things that I was very surprised is that they serve you a hot mean
during the two hours flight from Windhoek and Luanda
with the option for a vegan mean and wine.
After so many Ryanair flights,
I feel like I was at first class.