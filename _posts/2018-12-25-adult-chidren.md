---
layout: post
title: "Adult Children"
author: "Raniere Silva"
categories: blog
tags: []
image:
  feature: 2018-12-25-adult-children.jpg
  author: mrhayata
  licence: CC-BY-SA
  original_url: https://flic.kr/p/4iBxGP
---

In "Principles", Ray Dalio wrote

> Like a parent with adult children,
> I want you all to be strong, independent thinkers
> who will do well without me.
> I have done my best to bring you to this point;
> now is the time for you to setp up
> and for me to fade away.
