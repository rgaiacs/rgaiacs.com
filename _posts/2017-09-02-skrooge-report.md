---
layout: post
title: "Skrooge: Forecast report"
author: "Raniere Silva"
categories: tools
tags: [jekyll]
image:
  feature: 2017-09-02-skrooge-report.jpg
  author: Sean MacEntee
  original_url: https://flic.kr/p/kkWaCN
  licence: CC-BY
---

I'm trying to better manage my personal finances and for that I spend some time looking at accounting software that offer a good forecasting report. Understand how to get the report from [Skrooge](https://skrooge.org/) wasn't very easy so I'm going to write how I could get it.

## Account

Skrooge support different accounts so you need to create one. Just fill the information on the form and the "Add" button will be enabled.

![Skrooge Account screenshot](/images/2017-09-02-skrooge-report-account.png)

## Operations

Operations are connected to one and only one account. Just fill the information in the form and the "Add" button will be enabled.

![Skrooge Operations screenshot](/images/2017-09-02-skrooge-report-operations.png)

## Scheduled Operations

Scheduled operations need to be created from operations. Right click in one operation and a option to create a scheduled operation will be available. Scheduled operations can be view on their own tab. **Remember to configure the scheduled operation accordingly**.

![Skrooge Scheduled screenshot](/images/2017-09-02-skrooge-report-scheduled.png)

## Report

Skrooge offer many options for the report. For the one that I was looking, I need to select

- Mode: Cumulated sum of operations
- Current: year(s)
- including future: enabled
- Forecast: Scheduled
- Forecast slice: more than one

The result is below.

![Skrooge Visualisation screenshot](/images/2017-09-02-skrooge-report-visualisation.png)

## Conclusion

I'm quite happy with the visualisation. I will probably stay with Skrooge for a little while.