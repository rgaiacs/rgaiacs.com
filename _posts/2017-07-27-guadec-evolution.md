---
layout: post
title: "GUADEC 2017: My Experience with Evolution"
author: "Raniere Silva"
categories: tools
tags: [jekyll]
image:
  feature: 2017-07-27-guadec-evolution.jpg
  author: Jeff Djevdet
  original_url: https://flic.kr/p/pAxdLu
  licence: CC-BY
---

In a previous post,
I talked about e-mail client that I used.
At the current moment,
I'm using [Evolution](https://wiki.gnome.org/Apps/Evolution),
[Gnome](https://www.gnome.org/)'s official
"personal information management application that provides integrated mail, calendaring and address book functionality",
for more than half-year
and there are some things that annoys me very much.

Before I start list all the things that make me unhappy when using Evolution,
I need to say that I understand how complex a personal information management.

## Composer

1. Most of the time I want to past things "as text" but there is not short cut
   for it.

   **Note**: This was reported as [Bug 776813](https://bugzilla.gnome.org/show_bug.cgi?id=776813#c0).
2. Sometimes editing the content of the previous message make it messy.
3. Sometimes undo doesn't work.
4. Number of characters for word wrapping break links.
5. Can't edit template used for reply and forward.
6. Tab key don't increase list level.
7. Can't edit HTML direct.

## Send Account Overrides

I think this is what I was looking for a long time but I couldn't figure out
how it works.

## Search

1. The fields that are being use for the search are hidden.
   Would be great to have them visible.

## Archiving

1. Would be great to archive threads instead of emails.

   **Note**: I don't know if other e-mail clients have this feature.
