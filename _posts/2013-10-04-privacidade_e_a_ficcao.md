---
layout: post
title: "Privacidade e a Ficção"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<div class="warning">
<div class="admonition-title">
<p>Warning</p>
</div>
<p>Este post possui spoiler.</p>
</div>
<p>Alguns dias atrás eu e o <a href="https://identi.ca/panaggio">Panaggio</a> estavamos falando de nossas expectativas em relação a terceira temporada de &quot;<a href="http://www.imdb.com/title/tt1839578/?ref_=sr_1">Person of Interest</a>&quot;.</p>
<div class="more">

</div>
<div class="note">
<div class="admonition-title">
<p>Note</p>
</div>
<p>Para o caso de você nunca ter ouvido falar sobre &quot;Person of Interest&quot; vou fazer um pequeno resumo.</p>
<p>Harold Finch é um &quot;magnata&quot; e gênio da computação criou &quot;The Machine&quot; (é assim que ela é referenciada no seriado), um programa que rodava (até o final da segunda temporada) em um cluster do governo americano e estava conectado a TODOS os microfones e TODAS as cameras do planeta. &quot;The Machine&quot; utiliza as informações capturadas pelos microfones e cameras para prever acontecimentos. Destes acontecimentos, grande parte é descartada por não ser de grande importância para o governo e Finch recebe o primeiro número de CPF do trecho descartado diariamente.</p>
<p>No primeiro episódio, Finch contrata John Reese (um ex-agente especial) para ajudá-lo com os números fornecido pela &quot;The Machine&quot;.</p>
</div>
<p>Nossa conversa foi motivada pelo fato de que durante as duas primeiras temporadas existia &quot;uma conspiração&quot; de que algo semelhante a &quot;The Machine&quot; do seriado realmente existia e que no intervalo entre a segunda e terceira temporada a conspiração passou a ser fato (que atende pelo no de <a href="http://en.wikipedia.org/wiki/PRISM_(surveillance_program)">PRISM</a>, graças a <a href="http://en.wikipedia.org/wiki/Edward_Snowden">Edward Snowden</a>).</p>
<p>O Panaggio acreditava que os roteiristas iriam preparar algo bem bacana para a terceira temporada que tenderia mais para alertar as pessoas da importância da privacidade do que fortalecer o pensamento de que &quot;não tenho nada para esconder então está tudo bem&quot;.</p>
<p>Fico feliz em dizer que logo no segundo episódio o CPF fornecido é de Wayne (uma mistura de Sergey Brin, Larry Page e Nark Zuckerberg). Isso mesmo, o CPF é do dono de uma &quot;ponto com&quot; que vende informações sobre pessoas para que estiver disposto a comprar (<span data-role="download">descrição completa aqui &lt;po01.webm&gt;</span>).</p>
<p>Fico mais feliz ainda pela cena em que Wayne parabeniza seu &quot;sócio&quot; pela gravidez que ainda não foi anunciada mas que o programa da companhia de Wayne já descobri com base nas compras do supermercado (<span data-role="download">assista essa cena
aqui &lt;po02.webm&gt;</span>).</p>
<p>Depois dessa você ainda vai continuar utilizando Facebook, Google Plus e similares?</p>
<p>Para terminar, infelizmente não gostei das revelações presentes no fim do episódio. Mas creio que já escrivi spoiler suficiente por hoje.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>liberdade,privacidade</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
