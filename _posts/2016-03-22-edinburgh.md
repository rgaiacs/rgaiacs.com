---
layout: post
title: "Edinburgh"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<figure>
<img src="/images/IMG_20160320_173955.jpg" alt="Estação ferroviária." class="align-center" style="width:80.0%" /><figcaption>Estação ferroviária.</figcaption>
</figure>
<p>Nesse post só tem foto porque não estou com tempo para escrever muito. :-(</p>
<div class="more">

</div>
<figure>
<img src="/images/IMG_20160321_090320.jpg" alt="Um castelo." class="align-center" style="width:80.0%" /><figcaption>Um castelo.</figcaption>
</figure>
<figure>
<img src="/images/IMG_20160321_182324_HDR.jpg" alt="Acomodações estudantis ao fundo." class="align-center" style="width:80.0%" /><figcaption>Acomodações estudantis ao fundo.</figcaption>
</figure>
<figure>
<img src="/images/IMG_20160321_182342_HDR.jpg" alt="Obras." class="align-center" style="width:80.0%" /><figcaption>Obras.</figcaption>
</figure>
<figure>
<img src="/images/IMG_20160321_182928_HDR.jpg" alt="Cuidado com os carros!" class="align-center" style="width:80.0%" /><figcaption>Cuidado com os carros!</figcaption>
</figure>
<figure>
<img src="/images/IMG_20160321_183100.jpg" alt="Igreja." class="align-center" style="width:80.0%" /><figcaption>Igreja.</figcaption>
</figure>
<figure>
<img src="/images/IMG_20160321_183206_HDR.jpg" alt="Esse cachorrinho é famoso." class="align-center" style="width:80.0%" /><figcaption>Esse cachorrinho é famoso.</figcaption>
</figure>
<figure>
<img src="/images/IMG_20160321_183403_HDR.jpg" alt="Cemitério." class="align-center" style="width:80.0%" /><figcaption>Cemitério.</figcaption>
</figure>
<figure>
<img src="/images/IMG_20160321_183705_HDR.jpg" alt="Fundo do cemitério." class="align-center" style="width:80.0%" /><figcaption>Fundo do cemitério.</figcaption>
</figure>
<figure>
<img src="/images/IMG_20160321_184150_HDR.jpg" alt="Caminho até a igreja ao fundo." class="align-center" style="width:80.0%" /><figcaption>Caminho até a igreja ao fundo.</figcaption>
</figure>
<figure>
<img src="/images/IMG_20160321_184336_HDR.jpg" alt="Antiga praça de execução." class="align-center" style="width:80.0%" /><figcaption>Antiga praça de execução.</figcaption>
</figure>
<figure>
<img src="/images/IMG_20160321_184922_HDR.jpg" alt="Outra igreja." class="align-center" style="width:80.0%" /><figcaption>Outra igreja.</figcaption>
</figure>
<figure>
<img src="/images/IMG_20160321_185229_HDR.jpg" alt="Outro castelo." class="align-center" style="width:80.0%" /><figcaption>Outro castelo.</figcaption>
</figure>
<figure>
<img src="/images/IMG_20160321_185426_HDR.jpg" alt="Vista da cidade." class="align-center" style="width:80.0%" /><figcaption>Vista da cidade.</figcaption>
</figure>
<figure>
<img src="/images/IMG_20160321_191425_HDR.jpg" alt="Pátio da universidade." class="align-center" style="width:80.0%" /><figcaption>Pátio da universidade.</figcaption>
</figure>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>none</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
