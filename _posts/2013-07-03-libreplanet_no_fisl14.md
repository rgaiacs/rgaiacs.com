---
layout: post
title: "LibrePlanet São Paulo no FISL14"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>O <a href="http://libreplanet.org/wiki/LP-BR-SP">LibrePlanet São Paulo</a> durante essa semana esteve no FISL14 como um grupo de usuário.</p>
<figure>
<img src="/images/libreplanet_no_fisl14_0.jpg" class="align-center" style="width:80.0%" />
</figure>
<p>Nosso espaço de usuários estava cercado pelo Fedora, Mozilla e Gnome. Foram uma ótima companhia.</p>
<figure>
<img src="/images/libreplanet_no_fisl14_1.jpg" class="align-center" style="width:80.0%" />
</figure>
<p>Para o próximo ano precisamos levar mais membros para que sempre tenha alguém no nosso espaço.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>FISL</p>
</div>
<div class="tags">
<p>FISL14</p>
</div>
<div class="comments">

</div>
