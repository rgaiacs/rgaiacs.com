---
layout: post
title: "Graduation"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>And after a while my graduation day finally has a date: August 14, 2015.</p>
<div class="more">

</div>
<p>The convocation will be held at the &quot;<a href="http://www.preac.unicamp.br/cdc/?page_id=21">Centro de Convenções da UNICAMP</a>&quot; and you are invited to watch the ceremony that starts at 7pm and should go until 10pm.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>academia,educação,pessoal</p>
</div>
<div class="tags">
<p>UNICAMP</p>
</div>
<div class="comments">

</div>
