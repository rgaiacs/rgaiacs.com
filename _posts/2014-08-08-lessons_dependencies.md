---
layout: post
title: "Trying to Organize Lessons Dependencies"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>At a <span data-role="doc">previous post &lt;../01/lesson_manager&gt;</span> I wrote about steps to build a lesson manager. At this post you will find some words about issues for the first step are keep me wake at night.</p>
<div class="more">

</div>
<div class="admonition">
<p>Note</p>
<p>This was based on the experience at <a href="http://mozillascience.org/the-mozsprint-heard-round-the-world/">Mozilla Science Lab Summer Sprint 2014</a>. <a href="https://github.com/SoftwareCarpentryLessonManager/lesson-manager">Notes about the lesson manager project</a> are available.</p>
</div>
<h1 id="package-managers">Package Managers</h1>
<p>We can divide package managers in three groups:</p>
<dl>
<dt>Global</dt>
<dd><p>When the files are installed at <code>/bin</code>, <code>/lib</code>, <code>/usr/bin</code>, <code>/usr/lib</code>, <code>/usr/share</code>, ... E.g. <a href="https://wiki.debian.org/apt-get">apt-get</a>, <a href="https://fedoraproject.org/wiki/Yum">yum</a>, ...</p>
</dd>
<dt>User Space</dt>
<dd><p>When the files are installed at <code>~/bin</code>, <code>~/lib</code>, ... E.g. <a href="https://rubygems.org/">gems</a>, <a href="https://npmjs.org/">npm</a>, ...</p>
</dd>
<dt>Local</dt>
<dd><p>When the files are installed at the currently directory (or in a sub-directory of the currently directory). E.g. <a href="https://bower.io/">bower</a>, <a href="https://component.io/">component</a>, ...</p>
</dd>
</dl>
<p>IMHO, the more suitable is the local ones because will be easy to users know where the lesson was installed.</p>
<h1 id="lessons-tree">Lessons Tree</h1>
<p>Lessons has a dependence tree (black lines at the figure below) to be solved by the package manager but learners will use a path (read lines at the figure below) to read the lessons.</p>
<figure>
<img src="/images/lesson-tree.svg" class="align-center" style="width:80.0%" />
</figure>
<p>One of the nice features of bower is that all the dependencies are installed as siblings, i.e. :</p>
<pre><code>$ bower --config.directory=. install https://github.com/SoftwareCarpentryLessonManager/novice-git-backup
$ ls -1
novice-git-backup/
novice-git-intro/
novice-shell-filedir/
novice-shell-intro/</code></pre>
<p>The problem is that our learner don't have a tip for where he/she must start.</p>
<p>We can try solve the previous problem prepend a number to each directory, e.g. :</p>
<pre><code>$ ls -1
01-novice-shell-intro/
02-novice-shell-filedir/
03-novice-git-intro/
04-novice-git-backup/</code></pre>
<p>Unfortunately bower doesn't has a feature to add the correctly number and include some number at the package metadata will easily make someone get duplicate numbers.</p>
<h1 id="create-metadata-when-install-lesson">Create Metadata When Install Lesson</h1>
<p>One of the features that bower doesn't have (and we try to address at the sprint) is that when using :</p>
<pre><code>$ bower --config.directory=. install https://github.com/SoftwareCarpentryLessonManager/novice-git-backup</code></pre>
<p>bower won't create a metadata file to make possible update the lesson using :</p>
<pre><code>$ bower update</code></pre>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>none</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
