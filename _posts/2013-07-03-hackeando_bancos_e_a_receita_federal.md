---
layout: post
title: "Hackeando Bancos e a Receita Federal"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>O Thadeu Cascardo e Alexandre Oliva apresentaram &quot;Hackeando Bancos e a Receita Federal&quot; que foi a palestra mais cheia, tirando outros nomes famosos no FISL como Stallman e Mandog, que eu assisti.</p>
<figure>
<img src="/images/hackeando_bancos_e_a_receita_federal_0.jpg" class="align-center" style="width:80.0%" />
</figure>
<p>O Oliva falou sobre seu trabalho com o <a href="http://www.fsfla.org/svn/fsfla/software/irpf-livre-2013/">irpf-livre</a> que é uma versão livre do software utilizado para declaração do imposto de renda produzida via engenharia reversa por ele.</p>
<p>O Cascardo falou sobre o <a href="http://cascardo.info/blog/Receitanet/">rnetclient</a> que é uma versão livre do software utilizado para o envio da declaração do imposto de renda (também produzido por meio de engenharia reversa).</p>
<p>O próximo projeto do Cascardo é possibilitar acessar informações bancárias por meio de software livre. Algo muito interessante e que não sabia é que na Europa, ele citou o caso da Alemanhã, todos os bancos utilizam um protocolo padrão para comunicação com seus clientes de forma que o usuário pode configurar um software de planilha eletrônica para pegar informações bancárias e realizar operações como pagamentos e transferências (isso parece já ter sido implementado no <a href="http://www.gnucash.org/">GNUCash</a>).</p>
<figure>
<img src="/images/hackeando_bancos_e_a_receita_federal_1.jpg" class="align-center" style="width:80.0%" />
</figure>
<p>E não menos importante, eles também falaram sobre as 4 liberdades.</p>
<figure>
<img src="/images/hackeando_bancos_e_a_receita_federal_2.jpg" class="align-center" style="width:80.0%" />
</figure>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>FISL</p>
</div>
<div class="tags">
<p>FISL14</p>
</div>
<div class="comments">

</div>
