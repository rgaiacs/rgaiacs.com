---
layout: post
title: "Dia do Bibliotecário no SIBiUSP"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<div class="admonition">
<p>Nota</p>
<p>As fotos que ilustram esse post foram tiradas pela equipe do SIBiUSP e disponibilizadas <a href="https://www.facebook.com/sibiusp/photos_stream?ref=page_internal">aqui</a>.</p>
</div>
<p>Como anunciado <span data-role="doc">anteriormente &lt;../11/dia_do_bibliotecario&gt;</span> eu participei do Dia do Bibliotecário no SIBiUSP.</p>
<p><img src="/images/raniere-apresentando.jpg" alt="Raniere Silva apresentando" class="align-center" /></p>
<div class="more">

</div>
<p>Antes do seminário começar o <a href="http://itec.if.usp.br/~coral/">Coral do Instituto de Física</a> sob a regência do Regente Munir Sabag fez uma ótima apresentação.</p>
<p><img src="/images/coral.jpg" alt="Coral do Instituto de Física" class="align-center" /></p>
<p>O seminário começou com a apresentação do Prof. Dr. Murilo Bastos da Cunha da UnB que apresentou uma séria de novas oportunidades para as bibliotecas de forma que elas possam se reinventar.</p>
<p>A segunda apresentação foi do Prof. Dr. Demi Getschko do NIC.br que falou sobre a questão da preservação digital.</p>
<p>Em seguida a Profa. Dra. Kelly Rosa Braghetto do IME-USP falou sobre ciência aberta e dados abertos.</p>
<p>E por último eu falei sobre algumas possibilidades para as bibliotecas do ponto de vista do usuário.</p>
<p><img src="/images/debate.jpg" alt="Palestrante durante a sessão de perguntas." class="align-center" /></p>
<p>Depois das apresentações teve uma sessão de perguntas e respostas.</p>
<p><img src="/images/mesa.jpg" alt="Palestrantes após o seminário." class="align-center" /></p>
<p>O SIBiUSP está de parabéns por ter organizado o ótimo evento que junto diversos atores com as mais diversas experiências para conversar sobre um assunto tão importante que é a sobrevivência das bibliotecas em uma época onde a informação migrou para a forma digital e encontra-se nas nuvens.</p>
<p><img src="/images/publico.jpg" alt="Público do seminário." class="align-center" /></p>
<p>Algo que gostaria muito é que o SIBiUSP também realiza-se algo na <a href="http://www.openaccessweek.org/">Open Access Week</a> desse ano cujo tema é &quot;<a href="http://www.openaccessweek.org/profiles/blogs/2015-theme">Open for Collaboration</a>&quot;.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>academia,educação,ensino,internet</p>
</div>
<div class="tags">
<p>USP,SIBiUSP,Dia do Bibliotecário</p>
</div>
<div class="comments">

</div>
