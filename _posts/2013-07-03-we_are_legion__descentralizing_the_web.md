---
layout: post
title: "We are Legion: Descentralizing the web"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>&quot;We are Legion: Descentralizing the web&quot; foi apresentada por Deb Nicholson, evangelista do MediaGoblin.</p>
<figure>
<img src="/images/we_are_legion__descentralizing_the_web_0.jpg" class="align-center" style="width:80.0%" />
</figure>
<p>A apresentação começou com base no vídeo da campanha de doação do MediaGoblin do ano passado (ver vídeo <a href="http://balance.fsf.org/video/mediagoblin_campaign_pitch-small.webm">aqui</a>).</p>
<p>O email é um ótimo exemplo de tecnologia descentralizada que funciona. Ele começou em uma rede interna que logo depois migrou para a internet de forma descentralizada.</p>
<p>Dentre os problemas de serviços centralizados, temos:</p>
<ul>
<li>vigilância,</li>
<li>censura,</li>
<li>pouca diversidade, ...</li>
</ul>
<p>Alguns projetos que buscam tornar a internet descentralizada:</p>
<ul>
<li><a href="http://freedomboxfoundation.org/">FreedomBox</a>: um projeto de computadores em um único chip que rode um servidor web.</li>
<li><a href="https://www.openshift.com/">Openshift</a>: um projeto da Red Hat para aplicações nas nuvens.</li>
<li><a href="https://juju.ubuntu.com/">Juju</a>: um projeto da Canonical para aplicações nas nuvens.</li>
<li><a href="https://arkos.io/">Arkos</a>: um sistema operacional para rodar no Raspberry Pi.</li>
</ul>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>FISL</p>
</div>
<div class="tags">
<p>FISL14</p>
</div>
<div class="comments">

</div>
