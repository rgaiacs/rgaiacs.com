---
layout: post
title: "LaTeX Course at 1st Day of EnCPos"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Yesterday was the first day of my course about LaTeX that I talk some days ago (<span data-role="doc">see the post here &lt;../14/latex_course_at_ecnpos&gt;</span>. Around 20 students had subscribed for the course but only 10 come for the first day.</p>
<div class="more">

</div>
<p>I try to follow the plan with the pads (<span data-role="download">one for the contents
&lt;encpos-latex1.txt&gt;</span> and <span data-role="download">other for feedbacks
&lt;encpos-feedback-latex1.txt&gt;</span>) and the rounds:</p>
<dl>
<dt>Round 1.1</dt>
<dd><p>Some explanation about the pad and solving problems with the machines</p>
</dd>
<dt>Round 1.2</dt>
<dd><p>Introduction about TeX and Latex with some backgrounds.</p>
<p>This time we will not use a LaTeX IDE. Some explanations about the terminal will be given.</p>
</dd>
<dt>Round 1.3</dt>
<dd><p>Write some examples of LaTeX and give some basic explanations about the preamble, commands, environments and paragraphs.</p>
</dd>
<dt>Round 1.4</dt>
<dd><p>Compile the examples of the previous round and use of others programs that are shipped in the LaTeX distribution.</p>
</dd>
<dt>Round 1.5</dt>
<dd><p>Time to write a feedback of this class and answer to questions.</p>
</dd>
</dl>
<h1 id="the-pad">The pad</h1>
<p>None of the students edit the pad this time but thats probably because that we just keep in the basic of compilation process.</p>
<p>Using four spaces at the begin of the lines to mark codes didn't make any problem (last time I used HTML tags and have lots of troubles).</p>
<h1 id="the-black-screen">The black screen</h1>
<p>Since only a few students attended in the first day I can help all of than to use the terminal to compile the examples of TeX and LaTeX documents. (This time I add information both for Unix-like and Windows operation system.)</p>
<h1 id="the-video-projector">The video projector</h1>
<p>This time most of the time the screen in the video projector was showing the terminal that I was working on. (I hope that not make anyone uncomfortable and the use of Vim as the text editor crazy).</p>
<p>To show the PDF files I use <a href="http://pwmt.org/projects/zathura/">zathura</a> but I think that wasn't a big problem because all the students know how to open a PDF file.</p>
<h1 id="the-feedbacks">The feedbacks</h1>
<p>This time time three students give me a feedback and I think that's a great number. Unfortunately all three are very similar, they said that the class was good and that their main goal for the course was to learn more about the preamble.</p>
<p>This goal are expected because the proposal for the course when the organization of the meeting call me and will be cover in the second day.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>educação,Software Carpentry</p>
</div>
<div class="tags">
<p>LaTeX,EnCPos</p>
</div>
<div class="comments">

</div>
