---
layout: post
title: "Integrando GDB e SystemTap na depuração de aplicativos"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>A última palestra que assisti no FISL14 foi &quot;Integrando GDB e SystemTap na depuração de aplicativos&quot; do amigo Sergio Durigan Junior.</p>
<figure>
<img src="/images/integrando_gdb_e_systemtap_0.jpg" class="align-center" style="width:80.0%" />
</figure>
<p>Para quem não sabe, o GDB é o The GNU Debugger e SystemTap é uma &quot;biblioteca&quot; para adição de marcações no código compilado que pode ser utilizada para depuração.</p>
<figure>
<img src="/images/integrando_gdb_e_systemtap_1.jpg" class="align-center" style="width:80.0%" />
</figure>
<p>O trabalho realizado pelo Sergio foi construir uma ponte entre o SystemTap e o GDB. As vantagens desse trabalho é a facilitação na depuração de um código sem a necessidade de incluir informações de depuração (que aumentam muito o tamanho do executável) e a independência de linhas para pontos de paradas relacionados ao System Tap.</p>
<figure>
<img src="/images/integrando_gdb_e_systemtap_2.jpg" class="align-center" style="width:80.0%" />
</figure>
<p>Fiquei feliz da sala não estar muito fazia. Antes e durante o FISL14 ficávamos brincando que pela apresentação do Sergio ser no último horário estariam na palestra apenas seus amigos.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>FISL</p>
</div>
<div class="tags">
<p>FISL14</p>
</div>
<div class="comments">

</div>
