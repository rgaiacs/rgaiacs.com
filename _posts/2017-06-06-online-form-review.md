---
layout: post
title: "Online Form Review"
author: "Raniere Silva"
categories: tools
tags: [jekyll]
image:
  feature: 2017-06-06-online-form-review-cover.jpg
  author: Flazingo Photos
  original_url: https://flic.kr/p/nuRKsv
  licence: CC-BY
---

Some times we need to send a form to gather data.
Provide a user friend form is very important
and I'm unhappy with some of the tools
that I'm using in the past months.

In the next few posts I will look at some of the online form tools available
and try to discovery if the have the features that I want.

## List of desired features

1. Possibility to download the answers

   Because we want to use Python or R for our data analyse.

2. Email notification

   To owner of the survey and survey responder.

3. Full range of answer type

   Is required to support

   - short text
   - paragraph
   - multiple choice
   - checkboxes
   - dropdown
   - linear scale
   - data
   - time

   Extra points if support

   - multiple choice grid
   - file upload

4. Mark question as required

   Because otherwise people will not answer some questions.

5. Split the form into sections

   Because forms can be very very very long.

6. Customisation on the fly

   For the case that some sections could be skipped based on some answers.

7. Response validation

   Special for short text that is often used for email.

8. [Word Completion](https://en.wikipedia.org/wiki/Autocomplete)

   Because some dropdown can be very very very long.

   Extra points if new entries are accepted.

## Evaluation table

<table>
<thead>
<tr>
<th>Criteria</th>
<th>Maximum of Points</th>
</tr>
</thead>
<tbody>
<tr>
<td>Possibility to download the answers</td>
<td>1</td>
</tr>
<tr>
<td>Email notification</td>
<td>1</td>
</tr>
<tr>
<td>Full range of answer type</td>
<td>2</td>
</tr>
<tr>
<tr>
<td>Mark question as required</td>
<td>1</td>
</tr>
<tr>
<td>Split the form into sections</td>
<td>1</td>
</tr>
<tr>
<td>Customisation on the fly</td>
<td>1</td>
</tr>
<tr>
<td>Response validation</td>
<td>1</td>
</tr>
<tr>
<td>Word Completion</td>
<td>2</td>
</tr>
</tbody>
<tfoot>
<tr>
<td>Total</td>
<td>10</td>
</tr>
</tfoot>
</table>
