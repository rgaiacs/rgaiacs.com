---
layout: post
title: Firefox OS - MathML in EPUB
author: "Raniere Silva"
categories: ftb
tags: [ftb]
image:
  feature: ftb.jpg
---
This is a review of the Firefox OS EPUB readers app MathML support. For
testing I used [A First Course in Linear by Robert A.
Beezer](http://code.google.com/p/epub-samples/downloads/detail?name=linear-algebra-20120306.epub&can=2&q=)
from IDPF [epub-sample
repository](http://code.google.com/p/epub-samples/).

Current there is two EPUB readers app in Firefox Marketplace:
[EPUBReader](https://marketplace.firefox.com/app/epubreader?src=search)
and [Ab FB2
reader](https://marketplace.firefox.com/app/ab-fb2-reader?src=search).
The Ab FB2 reader won't be able to load the file.

When open the file it show the cover.

![](/images/2013-12-18-20-39-11.png)

Moving to the second chapter we find the presence of some MathML
elements.

![](/images/2013-12-18-20-39-56.png)

The subscripts and superscripts look nice for me but there are some gray
box representing some element that can't be display.

Some pages next, we find more MathML elements.

![](/images/what-is-la.png)

The equations in the page at the middle look nice but we have a overflow
in the page at the right.

The picture below is a example of what can happen with equations when we
increase the font size, it overflow the page size.

![](/images/increase-font-size.png)

Conclusion
==========

The EPUBReader for Firefox OS is one of the few apps that support MathML
and most of the time display it nice. Long equations is the biggest
problem at the moment.

**Update in 24/12/2013 based on Frédéric Wang's comment**: The problems
with font are cover in
<https://bugzilla.mozilla.org/show_bug.cgi?id=775060> and
<https://bugzilla.mozilla.org/show_bug.cgi?id=693968>. And the
linebreaking is cover in
<https://bugzilla.mozilla.org/show_bug.cgi?id=534962>.
