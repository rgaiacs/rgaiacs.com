---
layout: post
title: "Teaching Django in Namibia"
author: "Raniere Silva"
categories: travel
tags: ["Africa", "Namibia", "Windhoek"]
image:
  feature: 2019-06-28-namibia-django.jpg
  author:  Raniere Silva
  licence: CC-BY
---

African proverb "it takes a village to raise a child"
is the best way to summarise my exprience teaching Django in Namibia.
It reminded me of many teaching workshops that I co-organised in Brazil
when we asked for help of all our friends to have enough hands to make the event happen
and, most of the time, we were putting our own money to help our community.

My trip to Namibia started when
my friend and [Django Girls](https://djangogirls.org) co-coach [Karina](https://twitter.com/karina__in) wrote a [blog post](https://karinos.wordpress.com/2019/06/29/this-is-what-i-want-for-my-birthday/) saying

> For my Birthday I would like:
>
> More kids in the world to have possibility to learn programming.
>
> One of the options to achieve this is to help Jessica and Python Namibia Society to raise money for computer labs in the schools of Namibia.

Jessica and I knew each other from [PyCon UK](https://pyconuk.org/) and [The Carpentries](https://carpentries.org)
so I droped a email to Jessica asking how she and the Society were.
A few emails after,
she invited me to visit Namibia and teach Django given that they were having trouble to find a instructor
and I was on my sabbatical.
Six weeks later,
I was flying to Namibia.

The first time that I went to the University of Namibia was to help with the installation party
and my first impression is captured in the photo below.

{% include figure.html filename="2019-06-28-namibia-django-1.jpg" alternative_text="Photo of University gate" caption="University Gate." author="Raniere Silva" licence="CC-BY" %}

I thought that I was a character of 1995's movie "[Dangerous Minds](https://www.imdb.com/title/tt0112792/)"
(later I found that I should have ask the driver to drop me at the main gate).
Different from the movie,
the university was very safe and people very friendly.
University of Namibia doesn't look at all with US universities represented at 2014's documentary "[Ivory Tower](https://www.imdb.com/title/tt3263520/)",
it reminds me very much of the university at my home town,
but students are very skilled and seeking knowledge.

The Python Week of Code had two tracks:
introduction to Python, led by [Jessica](https://twitter.com/JessicaUpani),
and introduction to Django, led by me.
At the begin of every day,
all learners watched a talk to inspire them.
From all the fabulous keynotes,
my favourite was the one presented by [Jana Marie Backhaus](https://www.instagram.com/germanactress/).
Jana is a actress, comedian, radio host, voice artist, writer and hospital clown.
She spoke a lot about creativity, an important skills to developers.
The slides of my presentation is available at [slides.com](https://slides.com/rgaiacs/python-namibia-week-of-code-getting-your-first-remote-job).

{% include figure.html filename="2019-06-28-namibia-django-2.jpg" alternative_text="Photo of Ngatatue Mate speaking" caption="Ngatatue Mate speaking during his keynote." author="Raniere Silva" licence="CC-BY" %}

I think that web programming is the most difficult thing to teach someone who doesn't have a solid base of programming
because, usually, learners will have master many technologies at the same time.
I enjoyed teach Django,
my notes are available at [gitlab.com](https://gitlab.com/python-week-of-code-namibia-2019/notes),
but, in the future, I will recommend do a long version of a Django Girls workshop
where 2-3 learners are assign to a coach,
learners follow the lesson in their own pace,
and coaches are available to answer questions
and solve bugs.

{% include figure.html filename="2019-06-28-namibia-django-3.jpg" alternative_text="Photo of learners." caption="Learners during the classes." author="Raniere Silva" licence="CC-BY" %}

Each day of the week,
learners had 1-2 hours to work in a project.
The projects helped them to play with Python and Django
and be exposed to new built-in exceptions that instructors help to resolve.
In the last day,
they presented their projects to the class.
Projects were inspirational
and I hope that learners continue to work on them to improve their skills,
which might be hard for some given that they don't have a computer at their home.

I want to register that was a pleasure to meet the leadership of Python Namibia Society:
Ngatatue Mate,
Ngazetungue Muheue,
Vakuna Murangi,
Vernon Swanepool,
Unotjari Kandjavera,
and
Paulus Hauwanga.
Special thanks to Jessica Upani, a driving force to promote Python in Namibia, for the invitation to participate in the Week of Code.
And thanks to all the sponsors:
Integrated Management Solutions (IMS),
AZTech Consulting,
Python Software Foundation,
University of Namibia,
Django Foundation,
and
Django Denmark.

{% include figure.html filename="2019-06-28-namibia-django-4.jpg" alternative_text="Group photo." caption="Group photo at the end of the event." author="Raniere Silva" licence="CC-BY" %}

