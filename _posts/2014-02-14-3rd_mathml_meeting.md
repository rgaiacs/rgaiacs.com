---
layout: post
title: "3rd mathml meeting"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>This is a report about the 3rd Mozilla MathML IRC Meeting (see the announcement <a href="https://groups.google.com/forum/#!topic/mozilla.dev.tech.mathml/R4osz1ytuXs">here</a>). The topics of the meeting can be found <a href="https://etherpad.mozilla.org/0JHTjusvnW">in this PAD</a> (<span data-role="download">local copy of the PAD &lt;pad.txt&gt;</span>) and the <a href="http://logbot.glob.com.au/?c=mozilla%23mathml&amp;s=13%20Feb%202014&amp;e=13%20Feb%202014">IRC log</a> (<span data-role="download">local copy of the IRC log &lt;irc.txt&gt;</span>).</p>
<p>The next meeting will be in March 13th 9PM UTC at #mathml IRC channel. Please add topics in the <a href="https://etherpad.mozilla.org/mathml-2014-03">PAD</a>.</p>
<div class="more">

</div>
<h1 id="irc-log">IRC Log</h1>
<p>The channel is now being log. You should be more careful otherwise you will end up in <a href="https://wiki.mozilla.org/IRC#Quotes_Database">Qoutes Database</a>.</p>
<h1 id="mathml-in-wikipedia">MathML in Wikipedia</h1>
<p>In our last meeting Moritz request help to review the patch that enable Wikipedia's native support to MathML. Look like that some progress has been made but Moritz wasn't present to give more details. You can look for more information in <a href="https://gerrit.wikimedia.org/r/#/projects/mediawiki/extensions/Math,dashboards/default">Wikimedia Code Review</a>.</p>
<h1 id="documentation">Documentation</h1>
<p>MDN now has a <a href="https://developer.mozilla.org/en-US/docs/Web/MathML/Documentation_status">status page</a> with lots of useful information like articles needing editorial reviews, wording and grammar, and technical reviews, content.</p>
<p>fscholz is working in something similar to track translation of articles what is really great.</p>
<h1 id="add-ons">Add-ons</h1>
<p>fredw wrote some add-on:</p>
<ul>
<li><a href="https://addons.mozilla.org/en-US/firefox/addon/mathjax-native-mathml/">MathJax Native MathML</a> and</li>
<li><a href="https://addons.mozilla.org/en-US/firefox/addon/mathml-copy/">MathML Copy</a>.</li>
</ul>
<h1 id="texzilla">TeXZilla</h1>
<p><a href="https://github.com/fred-wang/TeXZilla">TeXZilla</a>, the (La)TeX to MathML javascript parser, can be tested as a <a href="https://addons.mozilla.org/en-US/firefox/addon/texzilla/">add on</a> that was accepted by AMO and can be safely used in other add-ons. Probably a web app for Firefox OS will be release too.</p>
<p>The MDN plugin that use TeXZilla is almost finish and should be release soon.</p>
<p>There are some refactoring be doing in TeXZilla to improve internal structure that is required to fix some issues.</p>
<h1 id="gecko---builds">Gecko - Builds</h1>
<p>WG9s provides a <a href="http://www.wg9s.com/mozilla/firefox/">nightly build of Firefox with some MathML related patchs</a> that now had tests enabled.</p>
<p>Unfortunately he has no capability to do (Mac) OS X builds. If you have a Mac and want to help just get in touch.</p>
<h1 id="gecko---fixed-bugs">Gecko - Fixed Bugs</h1>
<p>James fixed and fredw and karl reviewed <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=941611">bug 941611</a> related with incorrect preferred width of italic characters.</p>
<p>fredw fixed and karl reviewed <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=663740">bug 663740</a> related with nsMathMLChar measuring.</p>
<p>James fixed the issue with primes.</p>
<h1 id="gecko---work-in-progress">Gecko - Work in progress</h1>
<p>fredw still work on the Open Type MATH.</p>
<p>Jonathan Kew has worked on the bug with ascent/descent.</p>
<p>fredw send a new patch to <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=407059">bug 407059</a>.</p>
<p>WG9s send a new patch to <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=969906">bug 969906</a>.</p>
<h1 id="latexml">LaTeXML</h1>
<p>bruce is working on consolidating lots of changes in LaTeXML like the epub conversion and graphics package support. And a couple of Michael Kohlhase's students from Jacobs have been working on XSLT to generate openoffice/word formats.</p>
<p>Some work to support XeTeX will, probably, be done.</p>
<h1 id="firefox-os">Firefox OS</h1>
<p>As said before, probably a web app that use TeXZilla will be release for Firefox OS.</p>
<p>fredw suggest that Firefox OS can have a virtual keyboard to insert special math symbols and this, maybe, can be a GSOC project (have to check it with Firefox OS developers).</p>
<p>Last month Asa Dotzler <a href="https://hacks.mozilla.org/2014/01/mozilla-launches-contribution-program-to-help-deliver-firefox-os-to-tablets/">announce</a> that Mozilla will lauch a contribution program to help deliver Firefox OS to tablets. Will be great to test and report the MathML support in tablets with Firefox OS.</p>
<h1 id="gsoc">GSOC</h1>
<p>The <a href="https://wiki.mozilla.org/Community:SummerOfCode14:Brainstorming">brainstorming for GSOC</a> is open. If anyone wants to propose an idea and mentor a student for a MathML project, fell free to contribute.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>MathML,Mozilla</p>
</div>
<div class="tags">
<p>MathML meetings</p>
</div>
<div class="comments">

</div>
