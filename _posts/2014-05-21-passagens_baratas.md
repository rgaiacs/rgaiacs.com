---
layout: post
title: "Passagens baratas"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Embora os vários buscadores lhe sugiram inúmeros sites/agregadores onde você pode comprar passagens baratas é muito melhor ter de antemão a lista de sites onde deve-se procurar as passagens e é isso que você irá encontrar nesse post.</p>
<div class="more">

</div>
<p>A lista de sites encontram-se em ordem alfabética.</p>
<h1 id="sites-nacionais">Sites &quot;nacionais&quot;</h1>
<ul>
<li><a href="http://www.edestinos.com.br" class="uri">http://www.edestinos.com.br</a></li>
<li><p><a href="http://www.edreams.com.br" class="uri">http://www.edreams.com.br</a></p>
<p>Não disponibiliza uma URL &quot;legível&quot;.</p></li>
<li><a href="http://www.expedia.com.br" class="uri">http://www.expedia.com.br</a></li>
<li><a href="http://www.kayak.com.br" class="uri">http://www.kayak.com.br</a></li>
</ul>
<h1 id="sites-internacionais">Sites internacionais</h1>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>bookmarks</p>
</div>
<div class="tags">
<p>passagens</p>
</div>
<div class="comments">

</div>
