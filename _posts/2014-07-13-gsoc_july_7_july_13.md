---
layout: post
title: "GSoC: July 07 - July 13"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>This is one of the reports about my GSoC project and cover the eighth week of “Students coding”.</p>
<p>At this seventh week I give a talk at <a href="http://cermat.org/events/MathUI/14/">MathUI 2014 - 9th Workshop on Mathematical User Interfaces</a> and fix small bugs at the keyboard. Bellow you will find more details about the past week and the plans for this one.</p>
<div class="more">

</div>
<h1 id="mathui">MathUI</h1>
<p>MathUI was a awesome. I and Frédéric present <a href="http://fred-wang.github.io/MathUI2014/">our work</a> and met for the first time with others contributors of MathML support at Firefox. It was also a opportunity to have people testing the keyboard.</p>
<h1 id="feedbacks-from-mathui">Feedbacks from MathUI</h1>
<p>People, specially those that used tablets with Windows, suggest that the keyboard have &quot;arrow&quot; keys since was difficult to get the cursor at the desired position. After the conference I search at Bugzilla and discovery that <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=804796">UX team will not include arrows at the keyboard</a> what makes me a little unhappy.</p>
<p>People also ask about <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=1029437">text selection</a> and <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=747798">copy and paste</a>, two features that I already know be missing and some contributors are working trying to address. Related with the copy and paste feature, people also ask about copy MathML equation.</p>
<h1 id="composite-key-for-alternatives">Composite Key for Alternatives</h1>
<p>One of the first bugs that I address at my GSoC project was the <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=1011482">missing of composite keys for alternative keys</a>. My patch was blocked by the priority to release version 2.0 of Firefox OS but during this process Timothy Guan-tin Chien <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=1029356">starting use lists for the alternative keys</a> and we agree to use the same structure of visible keys. I rebase my patch but <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=1037942">one regression</a> is blocking my patch. I hope to have my patch merged this week.</p>
<h1 id="others-bugs">Others Bugs</h1>
<p>I also work to improve the backspace of the math environment key and to address the issue of need of uppercase value at alternate keys.</p>
<h1 id="plans-for-july-14---july-20">Plans for July 14 - July 20</h1>
<p>Continue to work at small issues.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>GSOC</p>
</div>
<div class="tags">
<p>GSOC2014</p>
</div>
<div class="comments">

</div>
