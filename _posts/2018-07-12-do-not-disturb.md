---
layout: post
title: "Do not disturb"
author: "Raniere Silva"
categories: blog
tags: [phone, android]
image:
  feature: 2018-07-12-do-not-disturb.jpg
  author: Richard Leeming
  original_url: https://flic.kr/p/dzGbtN
  licence: CC-BY-SA
---

Work or contribute with a worldwide distributed team
means that you will receive email and messages all day long.
Two years ago,
I changed the configuration of my email client
([Gnome's Evolution](https://help.gnome.org/users/evolution/stable/) on my laptop
and
[K-9 Mail](https://k9mail.github.io/) on my Android mobile phone)
to not automatically fetch emails
so that the stream of new item on my inbox isn't continuous
to the point of disturb my life because I'm replying one of the items or waiting for the reply of another one.

When configuring my [new phone](https://www.nokia.com/en_gb/phones/nokia-7-plus),
because of the [problem with my previous one]({% link _posts/2018-07-01-phone.md %})
and a thing other things going on my life,
I decided to apply the same configuration of my email client
to all the other chat apps that I have on my phone.

What does this new configuration mean?
It means that, if you message me on WhatsApp/Telegram/Signal/Skype/Messenger/Riot/Slack,
I will only see your message the next time that I open the app that you used.
**If you want or need to have a reply sooner than later,
call me.**

## WhatsApp

{% include figure.html filename="2018-07-12-do-not-disturb-whatsapp.png" alternative_text="Screenshot of my WhatsApp notification settings" caption="Screenshot of my WhatsApp notification settings" author="Raniere Silva" licence="CC-BY" %}

## Telegram

{% include figure.html filename="2018-07-12-do-not-disturb-telegram.png" alternative_text="Screenshot of my Telegram notification settings" caption="Screenshot of my Telegram notification settings" author="Raniere Silva" licence="CC-BY" %}

## Signal

{% include figure.html filename="2018-07-12-do-not-disturb-signal.png" alternative_text="Screenshot of my Signal notification settings" caption="Screenshot of my Signal notification settings" author="Raniere Silva" licence="CC-BY" %}

## Skype

{% include figure.html filename="2018-07-12-do-not-disturb-skype.png" alternative_text="Screenshot of my Skype notification settings" caption="Screenshot of my Skype notification settings" author="Raniere Silva" licence="CC-BY" %}

## Messenger

{% include figure.html filename="2018-07-12-do-not-disturb-messenger.png" alternative_text="Screenshot of my Messenger notification settings" caption="Screenshot of my Messenger notification settings" author="Raniere Silva" licence="CC-BY" %}

## Slack

{% include figure.html filename="2018-07-12-do-not-disturb-slack.png" alternative_text="Screenshot of my Slack notification settings" caption="Screenshot of my Slack notification settings" author="Raniere Silva" licence="CC-BY" %}