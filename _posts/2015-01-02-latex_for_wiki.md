---
layout: post
title: Encontrado a melhor implementação de Wiki?
author: "Raniere Silva"
categories: ftb
tags: [ftb]
image:
  feature: ftb.jpg
---
Wiki é algo que tem me fascinado nos últimos meses, principalmente
depois do [Encontro do Grupo de Trabalho em Ciência
Aberta](http://cienciaaberta.net/encontro2014/) realizado no fim de
Agosto. Nesse post você vai descobrir o que eu procuro no software de
uma wiki e qual foi o melhor que eu encontrei até agora.

Requisitos
==========

1.  Utilize um [sistema distribuído para controle de
    versão](https://en.wikipedia.org/wiki/Distributed_revision_control).

    Isso possibilita o trabalho offline e facilita o backup dos textos.
2.  Suporte a uma linguagem de marcação já conhecida.

    O uso de uma linguagem de marcação já difundida é um ponto positivo
    pois evita dos usuários terem que aprender uma nova linguagem.

    Também deseja-se que essa linguagem de marcação tenha suporte a
    escrita matemática, preferencialmente utilizando
    [LaTeX](https://pt.wikipedia.org/wiki/LaTeX) como a linguagem de
    marcação e [MathML](https://pt.wikipedia.org/wiki/MathML) como o
    formato enviado ao browser do usuário para renderização.
3.  Suporte a [categorização de
    páginas](https://pt.wikipedia.org/wiki/Ajuda:Tutorial/Liga%C3%A7%C3%B5es_internas#Categorias).

    No [MediaWiki](http://mediawiki.org/), o software responsável pela
    Wikipédia, existe
    [listas](https://en.wikipedia.org/wiki/Wikipedia:Categories,_lists,_and_navigation_templates#Lists),
    páginas estruturada com links para outras páginas, e
    [categorias](https://en.wikipedia.org/wiki/Wikipedia:Categories,_lists,_and_navigation_templates#Categories),
    páginas **não**-estruturadas com links para outras páginas.

    A grande vantagem do uso de categorias é a página da categoria é
    criada automaticamente.
4.  Suporte a
    [redirecionamento](https://www.mediawiki.org/wiki/Help:Redirects/pt-br).

    Redirecionamento é uma forma de "duplicar" uma página para que ela
    possa ser acessada por mais de um endereço.

Sistema Distribuído para Controle de Versão
===========================================

Existe vários softwares para a criação de wikis, [ver essa
lista](https://en.wikipedia.org/wiki/List_of_wiki_software) [ou essa
outra](https://en.wikipedia.org/wiki/Comparison_of_wiki_software), mas
apenas quatro que utilizam um sistema distribuído para controle de
versão

-   [ikiwiki](https://en.wikipedia.org/wiki/Ikiwiki) ou
    [Wagn](https://en.wikipedia.org/wiki/Wagn_(software)),
-   [Waliki](https://github.com/mgaitan/waliki),
-   [Gitit](https://en.wikipedia.org/wiki/Gitit_(software)),
-   [Gollum](https://github.com/github/gollum).

<div class="admonition">

Atualização

[No
stackoverflow](https://stackoverflow.com/questions/8255749/wikis-with-vcs-backends),
é mencionado [Hatta](http://hatta-wiki.org/) que possui uma [lista de
projetos similares](http://hatta-wiki.org/Similar%20projects):

-   [ymcGitWiki](https://www.openhub.net/p/ymcgitwiki)
-   [Smug](http://www.mcnabbs.org/andrew/smug/) (alterações precisam ser
    aprovadas)
-   [Sputnik](http://sputnik.freewisdom.org/)
-   [Wooki](http://wooki.sourceforge.net/) (P2P network)

</div>

Linguagem de Marcação
=====================

O Gitit utiliza o [Pandoc](http://pandoc.org/) para a conversão da
linguagem de marcação em HTML e como ele suporta um grande número de
linguagens de marcação é provável que você encontre uma que seu público
já conheça.

Além disso, o Pandoc suportam a escrita matemática utilizando LaTeX,
tanto nativamente como via Javascript.

Categorias
==========

Cada página do Gitit pode ter um cabeçalho do tipo YAML. Nesse cabeçalho
você pode informar as categorias utilizando a palavra chave
`categories`.

<div class="admonition">

Nota

O Gitit não suporta categorias formadas por mais de uma palavra ([ver
esse bug reportado a mais de dois
anos](https://github.com/jgm/gitit/issues/309)). Enquanto eu não aprendo
Haskell para enviar um patch que corrija isso, uma forma de contornar é
utilizar `_`.

</div>

<div class="admonition">

Nota

Algumas wikis, como o ikiwiki, denominam categorias de tags. Para
maiores detalhes, ver <http://ikiwiki.info/tags/>.

</div>

Redirecionamentos
=================

No cabeçalho pode-se, utilizando a palavra chave `redirect` ([maiores
informações
aqui](https://github.com/duairc/gitit/commit/85c89fa3f96ca11dbd02f70a19bf474b82ba69b4)),
informar a página para a qual o artigo deve redirecionar.

Instalação do Gitit
===================

Só foi preciso seguir os passos presentes em
<https://github.com/jgm/gitit>:

    $ git clone git@github.com:jgm/gitit.git
    $ cd gitit
    $ sudo cabal install --global
    $ gitit --version
    gitit version 0.10.6.1 +plugins
    Copyright (C) 2008 John MacFarlane
    This is free software; see the source for copying conditions.  There is no
    warranty, not even for merchantability or fitness for a particular purpose.

Testando Gitit
==============

Depois de instalar o Gitit é hora de testar:

    $ cd ~
    $ mkdir wiki
    $ cd wiki
    $ gitit

Depois de iniciar o Gitit, você pode acessar <http://localhost:5001> no
seu navegador web para visualizar sua instância do Gitit.

<div class="admonition">

Nota

Para encerrar o Gitit, prescione `CTRL+C` no terminal.

</div>

Encontrando Repositório Git
===========================

O repositório Git é o diretório que possui a pasta `.git` que pode ser
encontrado utilizando:

    $ find . -name '.git' -type d
    ./wikidata/.git

Sabendo onde encontra-se o repositório Git podemos acessá-lo e ver os
arquivos presentes nele. :

    $ cd wikidata 
    $ ls -1
    Front Page.page
    Gitit User’s Guide.page
    Help.page

Se desejarmos também podemos cloná-lo em outro lugar. :

    $ cd /tmp
    $ clone /tmp/wiki/wikidata/.git clone-do-gitit

Alterando as Configurações
==========================

Para iniciar o Gitit com configurações não padrões, utilizamos:

    $ gitit -f arquivo-de-configuracao

Para gerar um arquivo de configuração, utilizamos:

    $ gitit --print-default-config > config

Podemos alterar o arquivo de configurações para que a linguagem de
marcação padrão seja LaTeX e que expressões matemáticas sejam
convertidas para MathML:

    $ sed -i 's/default-page-type: Markdown/default-page-type: LaTeX/' config 
    $ sed -i 's/math: mathjax/math: MathML/' config

Para testar as novas configurações, utilizamos:

    $ gitit -f config

E acessando <http://localhost:5001> pode-se testar o Gitit.

Screenshots
===========

![](/images/gitit-edit0.jpg){width="80%"}

![](/images/gitit-view0.jpg){width="80%"}

![](/images/gitit-edit1.jpg){width="80%"}

![](/images/gitit-view1.jpg){width="80%"}

![](/images/gitit-category.jpg){width="80%"}
