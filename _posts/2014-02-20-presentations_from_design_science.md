---
layout: post
title: "Presentations from Design Science"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p><a href="http://www.dessci.com">Design Science</a> is a company that develop some products using MathML. They make some presentations available at <a href="http://www.dessci.com/en/company/events.htm" class="uri">http://www.dessci.com/en/company/events.htm</a> and I want to talk a little about some of them.</p>
<div class="more">

</div>
<h1 id="a-mathml-progress-report">A MathML Progress Report</h1>
<div class="note">
<div class="admonition-title">
<p>Note</p>
</div>
<p>Presentation__ and <span data-role="download">local copy &lt;MathMLProgressReport.pdf&gt;</span>.</p>
</div>
<p>I hope to see line wrapping, line breaking and elementary math implemented soon.</p>
<figure>
<img src="/images/conclusion.jpg" class="align-center" style="width:40.0%" />
</figure>
<h1 id="the-path-to-accessible-math">The Path to Accessible Math</h1>
<div class="note">
<div class="admonition-title">
<p>Note</p>
</div>
<p>Presentation__ and <span data-role="download">local copy &lt;accessiblemath.pdf&gt;</span>.</p>
</div>
<p>To math be accessible we need to support blindness, low vision and print-related learning disabilities.</p>
<h1 id="equation-editor-and-mathtype-tips-to-make-your-life-easier">Equation Editor and MathType: Tips to make your life easier</h1>
<div class="note">
<div class="admonition-title">
<p>Note</p>
</div>
<p>Presentation__ and <span data-role="download">local copy &lt;tipstricks-full.pdf&gt;</span>.</p>
</div>
<p>This is specific for Design Science product. =(</p>
<h1 id="facebook-twitter-and-other-social-mediateaching-tools-really">Facebook, Twitter, and Other Social Media—Teaching Tools? Really?</h1>
<div class="note">
<div class="admonition-title">
<p>Note</p>
</div>
<p>Presentation__ and <span data-role="download">local copy &lt;mathtype_with_social_media-full_handout.pdf&gt;</span>.</p>
</div>
<p>This have a long list of tools that support math.</p>
<h1 id="what-you-need-to-know-about-the-math-stack-mathml-mathjax-html5-and-epub-3">What you need to know about the Math Stack: MathML, MathJax, HTML5, and EPUB 3</h1>
<div class="note">
<div class="admonition-title">
<p>Note</p>
</div>
<p>Presentation__ and <span data-role="download">local copy &lt;MathStack.pdf&gt;</span>.</p>
</div>
<p>Hope to see more support from EPUB readers.</p>
<h1 id="creating-and-using-a-blog-to-enhance-math-and-science-instruction">Creating and Using a Blog to Enhance Math and Science Instruction</h1>
<div class="note">
<div class="admonition-title">
<p>Note</p>
</div>
<p>Presentation__ and <span data-role="download">local copy &lt;blogs_for_math_science.pdf&gt;</span>.</p>
</div>
<p>List of tools for blog.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>MathML</p>
</div>
<div class="tags">
<p>acessibilidade,ebooks,ereader,MathML</p>
</div>
<div class="comments">

</div>
