---
layout: post
title: "Pré-requisitos em disciplinas"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Em um post <span data-role="doc">anterior &lt;../21/como_o_aprendizado_funciona&gt;</span> falei sobre o treinamento que estou fazendo com a <a href="http://software-carpentry.org/">Software Carpentry</a> e do livro que está sendo utilizado de referência (&quot;How Learning Works: Seven Research-Based Principles for Smart Teaching&quot; de Susan A. Ambrose et. al.). Neste post irei falar sobre o primeiro capítulo desse livro: &quot;How does student's prior knowledge affect their learning?&quot; (&quot;Como o conhecimento do aluno afeta seu aprendizado?&quot; em tradução literal).</p>
<div class="more">

</div>
<p>A primeira coisa que o livro informa é que o conhecimento prévio do aluno pode ajudá-lo ou atrapalhá-lo no seu processo de aprendizagem. Se o conhecimento prévio for</p>
<ul>
<li>apropriado,</li>
<li>suficiente e</li>
<li>correto</li>
</ul>
<p>ele irá auxiliar o aluno mas se for</p>
<ul>
<li>inapropriado,</li>
<li>insuficiente e</li>
<li>incorreto</li>
</ul>
<p>muito provavelmente irá prejudicá-lo.</p>
<p>O papel do instrutor/professor é descobrir o conhecimento prévio do aluno por meio de avaliações/questionários/perguntas em sala e adequar suas aulas com base nesse conhecimento prévio.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>educação,ensino,Software Carpentry</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
