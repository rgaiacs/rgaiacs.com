---
layout: post
title: "Leap of faith"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<div class="note">
<div class="admonition-title">
<p>Note</p>
</div>
<p>Este post é inspirado em <a href="http://olhapravoce.blogspot.com/2013/09/morando-sozinha_8896.html">Morando sozinha ...</a> de Michele (<span data-role="download">cópia nesse servidor &lt;morando-sozinha_8896.html&gt;</span>)</p>
</div>
<div class="note">
<div class="admonition-title">
<p>Note</p>
</div>
<p><a href="http://en.wikipedia.org/wiki/Leap_of_faith">Leap of faith</a> é uma expressão inglesa para o ato de acreditar em algo sem esta ter sido demonstrada. Apenas achei que seria um título adequado esse post.</p>
</div>
<p>No seu texto, Michele falou sobre sua experiencia sair da casas dos pais para ir morar sozinha (que não minha opinião é um ótimo relato).</p>
<div class="more">

</div>
<p>Uma parte que gostei foi sobre a questão do emprego (acho que por estar procurando um depois de desistir da academia, mas isso fica para outro post):</p>
<blockquote>
<p>Sair de casa requer: SEMPRE ESTAR TRABAHANDO, porque é você quem te sustenta! E detalhe: Um emprego, senão bom, pelo menos que ganhe bem, pra pagar aluguel, pra comer o mês todo, e ainda ter as mordomias que tinha quando morava na casa dos pais...</p>
<p>Se quando morava em casa, você ficava de saco cheio da empresa, já pedia as contas porque você tinha as &quot;costas quentes&quot;, agora é completamente diferente... Agora você tem que aguentar muuuuuuito desaforo, não pode simplesmente virar as costas e dizer tchau... Seja pra juntar mais dinheiro pra poder sair do emprego, seja porque é tanta conta que não há condições de ficar um dia sem trabalhar, enfim... Saúde??? É o de menos... Você tem que trabalhar, bem ou péssimo, pra ter o dinheiro do aluguel, mesmo que não tenha o da comida!</p>
</blockquote>
<p>Uma coisa engraçada da vida é que ao sairmos da nossa zona de conforto acabamos aprendendo coisas que demoraríamos muito ou nunca aprenderíamos como relatado em &quot;e em cinco meses eu amadureci o que talvez demoraria aaaaaanos... O que na verdade demorou anos, antes de tudo!&quot;.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>none</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
