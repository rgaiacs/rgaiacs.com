---
layout: post
title: "Contagem regressiva para o FISL"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>O <a href="http://softwarelivre.org/fisl14">FISL14</a>, 14º Forum Internacional de Software Livre, acontece nessa semana que inicia hoje. A grade &quot;provisória&quot; de palestras encontra-se <a href="http://fisl.org.br/14/papers_ng/public/fast_grid?event_id=3">aqui</a> e mesmo que você não vá para Porto Alegre participar muito provavelmente poderá assistí-las pela internet com transmissão ao vivo.</p>
<p>Uma vez que o número de palestras é grande e várias delas ocorrem simultaneamente, a seguir você irá encontrar minha pré-seleção de palestras a serem assistidas.</p>
<div class="more">

</div>
<table>
<caption>Dia 0: 03-07-2013</caption>
<colgroup>
<col style="width: 11%" />
<col style="width: 44%" />
<col style="width: 44%" />
</colgroup>
<thead>
<tr class="header">
<th>Hora</th>
<th>Título</th>
<th>Palestrantes</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>11-13</td>
<td>Copyfight: muito além do download grátis</td>
<td>Adriano Belisário, Bruno Tarin, Tobias Andersson, Tadzia de Oliva Maya</td>
</tr>
<tr class="even">
<td>13-14</td>
<td>We are Legion: Decentralizing the Web</td>
<td>Deb Nicholson</td>
</tr>
<tr class="odd">
<td>14-15</td>
<td>Aaron Swartz: the rise of monopolies &amp; the fight against totalitarianism</td>
<td>Neville Roy Singham</td>
</tr>
<tr class="even">
<td>15-16</td>
<td>Hackeando Bancos e a Receita Federal</td>
<td>Thadeu Lima de Souza Cascardo, Alexandre Oliva</td>
</tr>
<tr class="odd">
<td>18-19</td>
<td>Monografias, Dissertações e Teses com Git e LaTeX</td>
<td>Raniere Gaia Costa da Silva</td>
</tr>
</tbody>
</table>
<table>
<caption>Dia 1: 04-07-2013</caption>
<colgroup>
<col style="width: 11%" />
<col style="width: 44%" />
<col style="width: 44%" />
</colgroup>
<thead>
<tr class="header">
<th>Hora</th>
<th>Título</th>
<th>Palestrantes</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>11-13</td>
<td>GNU, 30 anos, e a Chave para Entender o GNU/Linux</td>
<td>Alexandre Oliva</td>
</tr>
<tr class="even">
<td>16-17</td>
<td>Independence Day for Brazil</td>
<td>Jon &quot;MadDog&quot; Hall</td>
</tr>
<tr class="odd">
<td>18-20</td>
<td>Encontro de Hackers GNU: desafios estratégicos para o Software Livre -2a edição</td>
<td>Rodrigo R. Silva, Alexandre Oliva, Felipe Sanches</td>
</tr>
</tbody>
</table>
<table>
<caption>Dia 2: 05-07-2013</caption>
<colgroup>
<col style="width: 11%" />
<col style="width: 44%" />
<col style="width: 44%" />
</colgroup>
<thead>
<tr class="header">
<th>Hora</th>
<th>Título</th>
<th>Palestrantes</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>14-15</td>
<td>IRC, Wiki, Git e Fóruns como ferramentas para ensino de Linux e colaboratividade</td>
<td>Gabriel de Souza Fedel</td>
</tr>
<tr class="even">
<td>17-18</td>
<td>Livros eletrônicos interativos com HTML5 e ePUB3</td>
<td>Jomar Silva</td>
</tr>
<tr class="odd">
<td>18-19</td>
<td>Open Knowledge Foundation Brasil, a Rede pelo Conhecimento Livre</td>
<td>Everton Zanella Alvarenga</td>
</tr>
</tbody>
</table>
<table>
<caption>Dia 3: 06-07-2013</caption>
<colgroup>
<col style="width: 11%" />
<col style="width: 44%" />
<col style="width: 44%" />
</colgroup>
<thead>
<tr class="header">
<th>Hora</th>
<th>Título</th>
<th>Palestrantes</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>12-13</td>
<td>Palestra sobre as licenças. O que realmente é livre?</td>
<td>Patricia Fisch</td>
</tr>
<tr class="even">
<td>14-15</td>
<td>Bitcoin, passado, presente e futuro</td>
<td>Martin Loy</td>
</tr>
<tr class="odd">
<td>14-16</td>
<td>Encontro Comunitário do LibrePlanet São Paulo</td>
<td>Raniere Gaia Costa da Silva, Ricardo Panaggio, Sergio Durigan Junior, Capi Etheriel</td>
</tr>
<tr class="even">
<td>15-17</td>
<td>A Cultura hacker como estratégia para uma educação libertadora</td>
<td>Anderson Fernandes de Alencar, Nelson De Luca Pretto, Alexandre Oliva, Ronald Emerson Scherolt da Costa, Wilkens Lenon da Silva de Andrade</td>
</tr>
<tr class="odd">
<td>17-18</td>
<td>Integrando GDB e SystemTap na depuração de aplicativos</td>
<td>Sergio Durigan Junior</td>
</tr>
</tbody>
</table>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>FISL</p>
</div>
<div class="tags">
<p>FISL14</p>
</div>
<div class="comments">

</div>
