---
layout: post
title: Math at Firefox OS
author: "Raniere Silva"
categories: ftb
tags: [ftb]
image:
  feature: ftb.jpg
---
<div class="admonition">

Note

Last year, I promissed to [Bill Gianopoulos](http://www.wg9s.com/) that
I will write my steps to build my image of Firefox OS with math support.
After many months I finally did it.

</div>

Last year, before I started my Google Summer of Code project I wrote a
few blog posts about hacking Firefox OS:

-   Atualizando Alcatel One Touch Fire
    &lt;../../../2014/05/19/atualizando\_alcatel&gt;
-   Compilando Firefox OS para o Alcatel One Touch Fire
    &lt;../../../2014/05/20/compilando\_fxos&gt;
-   Compilando o Emulador do Firefox OS
    &lt;../../../2014/05/26/compilando\_firefox\_os\_emulator&gt;
-   Atualizando o Gaia do Firefox OS
    &lt;../../../2014/05/30/atualizando\_gaia&gt;
-   Another Hacking Gaia Tutorial
    &lt;../../../2014/06/06/another\_hacking\_gaia\_tutorial&gt;

One thing that I didn't write was how to add math support and you will
find in this post.

First Time
==========

Before you continue, please install the software listed
[here](https://developer.mozilla.org/en-US/Firefox_OS/Firefox_OS_build_prerequisites).

Clone B2G repository :

    $ git clone git://github.com/mozilla-b2g/B2G.git

and change the current directory to `B2G` directory :

    $ cd B2G

Configure the files for the device you have :

    $ ./config.sh device-name

where `device-name` is the name of your device.

Gecko has native support to MathML and because of it Firefox OS also has
it. The problem is that to math be correctly display you need fonts that
support math. To add the fonts :

    $ cd external/moztt
    $ git remote add math git://github.com/fred-wang/moztt
    $ git fetch math
    $ git checkout math/master
    $ git checkout -b add-math-support
    $ git rebase b2g/master
    $ cd -

You are almost ready to build your Firefox OS image. Before you do it
you need to connect your device with your machine using USB. To check
that your computer find your device :

    $ adb devices

Now you can build your Firefox OS image :

    $ ./build.sh

<div class="admonition">

Note

You need to connect your device because you have to copy some drivers
from your device.

</div>

Second Time
===========

If you already built your Firefox OS image once you should follow this
steps.

Update the B2G repository :

    $ git pull origin master

and update the files specific for your device :

    $ ./repo sync

When update the files specific for your device, your previous changes to
the fonts will be revert and you need to redo it :

    $ cd external/moztt
    $ git checkout add-math-support
    $ git rebase b2g/master
    $ cd -

And you are ready to build your Firefox OS image :

    $ ./build.sh

Flashing your Image
===================

<div class="admonition">

Warning

The steps in this section can **brick** your device.

</div>

<div class="admonition">

Note

Is completing one year that almost every week I build my onw version of
Firefox OS and flash it to my only cell phone. Until now I don't have
problems **but be warned that you can lose your device when flashing
it**.

</div>

Once you built your own Firefox OS image you can flash it to your device
:

    $ ./flash.sh

Congrats. You are using your own image of Firefox OS.

![Screenshot of my device running Firefox OS 3.0 with Gecko
39.0a1.](settings.png)

![Some equations as render on Firefox OS with properly
fonts.](math-basic.png)

![More equations as render on Firefox OS with properly
fonts.](math-advanced.png)

Tip
===

If you have two or more devices, use different directories for each
device. I have hamachi and flatfish and what I did was :

    $ git clone git://github.com/mozilla-b2g/B2G.git B2G-hamachi
    $ cd B2G-hamachi
    $ ./config.sh hamachi
    $ cd -
    $ git clone git://github.com/mozilla-b2g/B2G.git B2G-flatfish
    $ cd B2G-flatfish
    $ ./config.sh flatfish

References
==========

[MDN](https://developer.mozilla.org/en-US/Firefox_OS) has many useful
informations.
