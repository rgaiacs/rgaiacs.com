---
layout: post
title: "Let's Encrypt with Docker"
author: "Raniere Silva"
categories: web
tags: ["Encryption"]
image:
  feature: 2020-07-07-lets-encrypt.jpg
  author: iMattSmart
  licence: Splash License
  original_url: https://unsplash.com/photos/Vp3oWLsPOss
---

[Let's Encrypt](https://letsencrypt.org/) is a free, automated, and open certificate authority.
With it,
you can add a digital certificate to your website
and
contribute for information shared between users and your website
are private.

Last week,
I discover that use Let's Encrypt
with Docker requires copy a couple of lines
to your `docker-compose.yml`
and edit a few lines.
I used
[jrcs/letsencrypt-nginx-proxy-companion](https://hub.docker.com/r/jrcs/letsencrypt-nginx-proxy-companion)
by [jrcs](https://hub.docker.com/u/jrcs)
and
[jwilder/nginx-proxy](https://hub.docker.com/r/jwilder/nginx-proxy)
by [jwilder](https://hub.docker.com/u/jwilder).


```
version: "3.7"
services:
  nginx-proxy:
    image: jwilder/nginx-proxy
    container_name: nginx-proxy
    ports:
      - 80:80
      - 443:443
    volumes:
      - conf:/etc/nginx/conf.d
      - vhost:/etc/nginx/vhost.d
      - html:/usr/share/nginx/html
      - dhparam:/etc/nginx/dhparam
      - certs:/etc/nginx/certs:ro
      - /var/run/docker.sock:/tmp/docker.sock:ro
    network_mode: bridge

  letsencrypt:
    image: jrcs/letsencrypt-nginx-proxy-companion
    container_name: nginx-proxy-le
    volumes:
      - conf:/etc/nginx/conf.d
      - vhost:/etc/nginx/vhost.d
      - html:/usr/share/nginx/html
      - dhparam:/etc/nginx/dhparam
      - certs:/etc/nginx/certs:rw
      - /var/run/docker.sock:/var/run/docker.sock:ro
    network_mode: bridge

volumes:
  conf:
  vhost:
  html:
  dhparam:
  certs:
```

to make it work,
I added

```
environment:
  - VIRTUAL_HOST=foo.bar.com
  - LETSENCRYPT_HOST=foo.bar.com
  - LETSENCRYPT_EMAIL=mail@foo.bar.com
```

to the container that is serving my website.

And everything worked out of the box.