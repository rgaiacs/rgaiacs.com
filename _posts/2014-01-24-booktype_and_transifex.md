---
layout: post
title: Booktype and Transifex
author: "Raniere Silva"
categories: ftb
tags: [ftb]
image:
  feature: ftb.jpg
---
This week I worked in the [portuguese translation of Open Education
Handbook](http://education.okfn.org/portuguese-translation-of-open-education-handbook/).
In this post I cover the problem that we have using Booktype and
Transifex for the translation.

Every book in Booktype can be export as a zip archive following the
steps in [Booktype
manual](http://sourcefabric.booktype.pro/booktype-16-for-authors-and-publishers/publishing/).
The problem that we have is related on how Booktype named the chapter
files in the zip archive.

Steps to reproduce the problem
==============================

Create a book in Booktype.

![](/images/booktype01.png){width="100%"}

Add the first chapter.

![](/images/booktype02.png){width="100%"}

Export the book:

    $ wget http://booktype.okfn.org/export/test/export/ -O test1.zip

Create the second chapter.

![](/images/booktype03.png){width="100%"}

**Move** (you can do it dragging and dropping the one of the chapter)
the second chapter to be the first.

![](/images/booktype04.png){width="100%"}

Export the book:

    $ wget http://booktype.okfn.org/export/test/export/ -O test2.zip

The problem
===========

Now you should have two files: `test1.zip` and `test2.zip`. Let's check
what we have in both files:

    $ unzip -l test1.zip 
    Archive:  test1.zip
      Length      Date    Time    Name
    ---------  ---------- -----   ----
           23  1980-01-01 00:00   mimetype
           48  1980-01-01 00:00   ch000_first-chapter.html
         1095  1980-01-01 00:00   info.json
    ---------                     -------
         1166                     3 files
    $ unzip -l test2.zip 
    Archive:  test2.zip
      Length      Date    Time    Name
    ---------  ---------- -----   ----
           23  1980-01-01 00:00   mimetype
           49  1980-01-01 00:00   ch000_second-chapter.html
           48  1980-01-01 00:00   ch001_first-chapter.html
         1450  1980-01-01 00:00   info.json
    ---------                     -------
         1570                     4 files

As you can see inside `test1.zip` we have `ch000_first-chapter.html` and
in `test2.zip` we have `ch001_first-chapter.html` but the content of
this HTML files are the same as you check below:

    $ unzip test1.zip ch000_first-chapter.html
    Archive:  test1.zip
      inflating: ch000_first-chapter.html  
    $ unzip test2.zip ch001_first-chapter.html
    Archive:  test2.zip
      inflating: ch001_first-chapter.html  
    $ diff ch000_first-chapter.html ch001_first-chapter.html

The problem is that for continuous translation/localization (if you
don't know what this means read [this article at LingoHub's
blogs](http://blog.lingohub.com/industry-updates/2013/12/continuous-translation/))
with Transifex ([look at the script that we use in
GitHub](https://github.com/r-gaia-cs/efd2014-campinas)) we can't change
the name of the file otherwise we will need to translate the file again.

Solutions?
==========

I can thought in three ways to solve this problem:

1.  code some scripts to discover what files had it's name change and
    "fix" it;
2.  change how Booktype store the book and export it;
3.  use another set of tools if you plan continous
    translation/localization (maybe using MediaWiki based on[^1]
    and[^2]).

**References**

[^1]: [WikiMedia Meta-Wiki - Help:Preparing a page for
    translation/en](https://meta.wikimedia.org/wiki/Help:Preparing_a_page_for_translation/en)

[^2]: [MediaWiki -
    Help:Extension:Translate](https://www.mediawiki.org/wiki/Help:Extension:Translate)
