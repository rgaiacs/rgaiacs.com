---
layout: post
title: "Reprodutibilidade da ciência"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>&quot;A reprodutibilidade de uma experiência científica é uma das condições que permitem incluir no processo de progresso do conhecimento científico as observações realizadas durante a experiência. Essa condição origina-se no princípio de que não se pode tirar conclusões senão de um evento bem descrito, que aconteceu várias vezes, provocado por pessoas distintas. Essa condição permite se livrar de efeitos aleatórios que podem afetar os resultados, de erros de julgamento ou de manipulações por parte dos cientistas.&quot; (<a href="http://pt.wikipedia.org/wiki/Reprodutibilidade">Wikipédia</a>)</p>
<p>Ao passarmos a utilizar mais e mais ferramentas computacionais nos experimentos científicos um dos requisitos para a reprodutibilidade do experimento é a disponibilidade do código fonte dos programas utilizados e dos dados de entrada. Infelizmente, hoje, essa não é uma prática comum e foi tema de uma <a href="http://lists.software-carpentry.org/pipermail/discuss_lists.software-carpentry.org/2013-October/001056.html">thread</a> na lista de discursão da <a href="http://software-carpentry.org">Software Carpentry</a> sobre qual seria a porcentagem de artigos científicos que disponibilizam os códigos fontes e dados de entrada.</p>
<p>Essa quantidade vai variar de área para área (algumas áreas da computação disponibilizam os códigos mais que outras) mas as estimativas mais animadoras em quase todos os emais era de ~5% e as mais &quot;realistas&quot; de &lt;1%.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>Software Carpentry,academia,ciência aberta</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
