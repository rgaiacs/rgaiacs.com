---
layout: post
title: "Desligando Facebook e Google"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Em 24/06/2013, <a href="http://blog.ziade.org/resume.html">Tarek Ziadé</a> escreveu um post intitulado &quot;<a href="http://blog.ziade.org/2013/06/24/shutting-down-facebook-amp-google/">Shutting down Facebook &amp; Google</a>&quot; que gostei muito. O post de Ziadé é licenciado com CC BY-NC-ND e uma cópia encontra-se <span data-role="download">aqui &lt;shutting-down.html&gt;</span>.</p>
<div class="more">

</div>
<p>Existem três partes do post que gostei muito:</p>
<ol type="1">
<li><p>Ele fala sobre os escândalos recentes envolvendo privacidade:</p>
<blockquote>
<p>However, the recent privacy scandals made me think a bit about how I use all these services.</p>
<p>First of all, G+ and Facebook provide features to share privately some data to a specific set of people. We've seen that our privacy is not guaranteed. They'll give your data away if they're asked.</p>
<p>And well, they are commercial companies that need to sell ads, so your private content is used anyways. Just look at the ads that are displayed besides your e-mail when you use GMail.</p>
<p>So while there's an incentive to let you share data thinking it's not public, shit happens. So I don't see the point of pushing anything non-public in those silos. And it turns out that I am mostly a consumer.</p>
</blockquote></li>
<li><p>A importância de se opor a perda da privacidade:</p>
<blockquote>
<p>But what I am doing is still important. It's a political act that will shape the future if we're a bunch of people acting like this. We need to show through our online habits and actions that we care about our privacy.</p>
</blockquote></li>
<li><p>E a tarefa de ensinar:</p>
<blockquote>
<p>Now my next task is to teach my kids how important privacy is, so that they clearly understand what's going on when they are using the internets.</p>
</blockquote></li>
</ol>
<p>Assim como Ziadé, no fim do ano passado tomei a descisão de abandonar softwares proprietários e serviços oferecidos gratuitamente que são pagos com a perda de privacidade. Tenho falado sobre isso com alguns amigos e preciso começar a ensinar outras pessoas (como não tenho filhos, devo começar com meus pais).</p>
<p>A seguir você encontrá uma lista (de uso mais frequente para o menos frequente) de serviços que abandonei ou pretendo fazê-lo e o que fiz para substituí-los.</p>
<h1 id="celular">Celular</h1>
<p>O celular é um grade vilão da privacidade. Ziadé diz que está utilizando o &quot;Firefox OS&quot; e por isso está em &quot;boas mãos&quot;. Eu discordo dele porque você só mudou de rótulo o problema.</p>
<p>Infelizmente, para mim é bastante difícil não ter um pois passo boa parte do dia fora de casa e é incômodo ficar falando ao telefone do lado de pessoas que estão tentando estudar.</p>
<h1 id="ferramenta-de-busca-do-google">Ferramenta de busca do Google</h1>
<p>Ainda não parei de utilizar. Embora exista o DuckDuckGo e StartPage os resultados do Google são muito bons.</p>
<h1 id="gmail">Gmail</h1>
<p>Gmail é o serviço de email do Google. Sem sombra de dúvida abandonar o Gmail não foi uma tarefa fácil. Passei a utilizar mais a conta da faculdade e também criei uma conta no <a href="https://mail.riseup.net/">riseup</a>. Pelo grande volume de email que recebo e o pequeno espaço das minhas novas caixas de entradas tive que configurar o <a href="http://fetchmail.berlios.de/">fetchmail</a>, <a href="http://www.procmail.org/">procmail</a>, msmtp &lt;http://msmtp.sourceforge.net/&gt; e o <a href="http://www.mutt.org/">mutt</a>.</p>
<p>Ainda mantenho meu endereço de email do Gmail para receber emails de pessoas que não foram notificadas de minha mudança. Com o tempo as pessoas tomam conhecimento do meu novo email e poderei fechar a conta.</p>
<h1 id="google-reader">Google Reader</h1>
<p>Google Reader é um leitor/agregador de feeds rss e atom que foi descontinuado. Inicialmente substitui o Google Reader pelo <a href="http://www.newsbeuter.org">newsbeuter</a> que é muito bom. Hoje utilizo <a href="http://www.dt.e-technik.uni-dortmund.de/~ma/leafnode/beta/">leafnode-2</a> com <a href="http://user42.tuxfamily.org/rss2leafnode/index.html">rss2leafnode</a> e <a href="http://slrn.sourceforge.net/">slrn</a> (newsreader).</p>
<h1 id="google-calendar">Google Calendar</h1>
<p>O calendário go Google foi substituído pelo <a href="http://calcurse.org/">calcurse</a> e pelo <a href="http://taskwarrior.org/projects/show/taskwarrior">taskwarrior</a>.</p>
<h1 id="twitter">Twitter</h1>
<p>Foi sumariamente abandonado e substitui por esse blog e assinatura de feeds de vários blogs. O <a href="http://identi.ca/">identi.ca</a> e o <a href="http://pump.io/">pump.io</a> são serviços equivalentes mas acabei não criando conta neles.</p>
<h1 id="facebook">Facebook</h1>
<p>Foi sumariamente abandonado e substitui por esse blog e assinatura de feeds de vários blogs.</p>
<h1 id="google">Google+</h1>
<p>Foi sumariamente abandonado e substitui por esse blog e assinatura de feeds de vários blogs.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>privacidade</p>
</div>
<div class="tags">
<p>alternativas</p>
</div>
<div class="comments">

</div>
