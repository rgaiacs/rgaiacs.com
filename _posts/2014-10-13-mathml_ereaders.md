---
layout: post
title: MathML on ereaders
author: "Raniere Silva"
categories: ftb
tags: [ftb]
image:
  feature: ftb.jpg
---
[Ereaders](https://en.wikipedia.org/wiki/Ereaders) are the best gadget
to read because of their e-ink screen. Most of the ereaders available in
the market support [EPUB](https://en.wikipedia.org/wiki/EPUB), a open
standard for digital books. The [last release of EPUB
specification](http://www.idpf.org/epub/301/spec/) says for mathematical
equations must be used [MathML](http://www.w3.org/mathml).

Unfortunately, none of ereaders available at the market support MathML.
(**If you know one please let me know**, you can contact me [by
email](mailto:raniere@ime.unicamp.br).)

<div class="admonition">

Note

Ereaders aren't the same as EPUB readers. You can find at least one EPUB
reader that support MathML for all the popular operating system.

</div>

[Frédéric Wang](http://github.com/fred-wang/) wrote a [small CSS
collection to emulate MathML](http://github.com/fred-wang/mathml.css)
when native support isn't available and this blog post about trying to
use this collection to get MathML on ereaders.

Layout Engine
=============

Briefly, we have two free/open source layout engine:
[Gecko](https://en.wikipedia.org/wiki/Gecko_%28software%29) and
[WebKit](https://en.wikipedia.org/wiki/WebKit). Gecko has native MathML
support and powers Firefox. WebKit also has native MathML support and
powers [many web
browser](https://en.wikipedia.org/wiki/List_of_web_browsers#WebKit-based)
like Chromium/Google Chrome, Safari and [Gnome
Web](https://en.wikipedia.org/wiki/Web_(web_browser)).

Some web browsers, like Chromium/Google Chrome, disable the MathML
support from WebKit.

![From left to right: Firefox, Chromium and Gnome
Web.](web-browsers.jpg){width="80%"}

For that reason that Frédéric had to write the CSS that works very well
considering CSS's limitations.

![](/images/chromium-with-css.jpg){width="80%"}

Could we use Frédéric's CSS for ereaders?

<div class="admonition">

Note

As far as I can tell, most of the ereaders available at the market use
WebKit because they are based on Android (e.g.
[Nook](https://en.wikipedia.org/wiki/Barnes_%26_Noble_Nook) and other
from Europe) or the company that develop it has [public available code
repositories related with WebKit](https://github.com/kobolabs) (e.g.
[Kobo](https://en.wikipedia.org/wiki/Kobo_eReader)).

</div>

Sample
======

Before test it we need create a small EPUB with MathML. For that we will
use Markdown and [Pandoc](http://johnmacfarlane.net/pandoc/). To start
you will need this Markdown file &lt;sample.md&gt; and
Frédéric's CSS file
&lt;mathml.css&gt;. :

    $ cat sample.md
    # MathML Sample

    $f(x)$, de uma função tal que $f(w) = \frac{1}{2} e^{x}$.

    $f(x)$, de uma função tal que $$f(w) = \frac{1}{2} e^{x}$$.

    A integral de Fourier é $f(x) = \frac{1}{\sqrt{2 \pi}} \int_{-\infty}^{+\infty} \frac{1}{\sqrt{2 \pi}} \int_{-\infty}^{+\infty} f(x) e^{i w x} dx e^{-i w x} dw.$

    A integral de Fourier é $$f(x) = \frac{1}{\sqrt{2 \pi}} \int_{-\infty}^{+\infty} \frac{1}{\sqrt{2 \pi}} \int_{-\infty}^{+\infty} f(x) e^{i w x} dx e^{-i w x} dw.$$
    $ pandoc --version
    pandoc 1.13.1
    $ pandoc -f markdown -t epub --mathml -o sample-mathml.epub sample.md
    $ pandoc -f markdown -t epub --mathml --epub-stylesheet=mathml.css -o sample-css.epub sample.md

<div class="admonition">

Note

Download the EPUB files: sample-mathml.epub and sample-css.epub.

</div>

Testing - Nook
==============

For [Nook Simple
Touch](https://en.wikipedia.org/wiki/Nook_Simple_Touch), released on
2011 and runs Android 2.1, sample-mathml.epub looks the same as
Chromium.

![](/images/nook-mathml.jpg){width="80%"}

Is impossible to read sample-css.epub because many equations get its own
page.

![](/images/nook-css.jpg){width="80%"}

Testing - Kobo
==============

<div class="admonition">

Thanks

Testing on Kobo was possible thanks to [Ivan Sichmann
Freitas](https://github.com/isf) who lent me his device.

</div>

For [Kobo Aura HD](https://en.wikipedia.org/wiki/Kobo_Aura_HD), released
on 2013, sample-mathml.epub also looks the same as Chromium.

![](/images/kobo-mathml.jpg){width="80%"}

sample-css.epub didn't have the same problem from Nook but the result
wasn't the expected.

![](/images/kobo-css.jpg){width="80%"}

Future works
============

1.  Check if Kobo Web Browser has the same problem with the CSS.
2.  Try to get the same problem with Chromium.
3.  Try to improve the CSS?
