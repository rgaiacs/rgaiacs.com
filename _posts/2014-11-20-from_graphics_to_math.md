---
layout: post
title: "From Graphics to Math"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<div class="admonition">
<p>Note</p>
<p>I recomend that you use <a href="http://firefox.com/">Firefox</a> to read this post.</p>
</div>
<p>Yesterday, <a href="http://juretriglav.si/">Jure Triglav</a> said</p>
<blockquote>
<p>&quot;I dropped by to let you know that I wrote a new post on scientific data visualization: <a href="http://juretriglav.si/standards-for-graphic-presentation/" class="uri">http://juretriglav.si/standards-for-graphic-presentation/</a> and would love any kind of feedback you could give me&quot;</p>
</blockquote>
<p>at <a href="https://kiwiirc.com/client/irc.mozilla.org/#sciencelab">#sciencelab</a> (a nice place to talk with people about how to do science in the 21st).</p>
<p>Jure's post is awesome (and you <strong>should</strong> read it). The main point of it are that &quot;a committee of several really smart people from all branches of science and engineering&quot; wrote a document for standardizing scientific graphics in <strong>1914</strong>.</p>
<p>One of the rules at this document &quot;is essentially saying that data should accompany the figure. A hundred years later, we're still far from applying this simple but powerful rule. Projects like <a href="http://contentmine.org/">The Content Mine</a> expend a tremendous amount of energy trying to get data back from figures, and even then it's a very lossy process. All of that could be avoided if we just follow this one simple rule.&quot;</p>
<p>At Jure's post you will find what you can do to <strong>follow this one simple rule</strong> when working with graphics. In this post I want to extend this simple rule to mathematical expressions.</p>
<div class="more">

</div>
<p>In 1914 the data of mathematical expressions could always be found with the &quot;figure&quot; of it. What I mean here is that for everyone living in 1914 that see, for example,</p>
<math xmlns="http://www.w3.org/1998/Math/MathML" alttext="a^{2}+b^{2}=c^{2}" display="block">
  <mrow>
    <mrow>
      <msup>
        <mi>a</mi>
        <mn>2</mn>
      </msup>
      <mo>+</mo>
      <msup>
        <mi>b</mi>
        <mn>2</mn>
      </msup>
    </mrow>
    <mo>=</mo>
    <msup>
      <mi>c</mi>
      <mn>2</mn>
    </msup>
  </mrow>
</math>
<p>will understand that &quot;the square of the c is equal to the square of a plus the square of b&quot;.</p>
<p>With the creation of computers and the use of it to replace paper we start to see, for example,</p>
<p><img src="/images/pythagorean_theorem.jpg" alt="image" /></p>
<p>that looks very similar to the first example and, except if you are a program, you probably will understand it in the same way of the first example.</p>
<p>In the same way of the graphics we are, mostly, using figures to visualize mathematical expressions (data) and doing that we lost the data. And in the same way that a committee of several really smart people wrote a document to address the problem with graphics in 1914 another committee wrote, in 1998, a document to address the problem with mathematical expressions (this document is know as <a href="http://www.w3.org/TR/MathML3/">Mathematical Markup Language (MathML)</a> and is a W3C Recommendation).</p>
<h1 id="what-are-we-losing-when-the-data-is-lost">What are we losing when the data is lost?</h1>
<p>In the case of the graphics you need the data to (1) recreate the figures, what is very important for the reproducibility of science, (2) extract more information and (3) use it with other set of data to get new information.</p>
<p>In the case of mathematical expressions you need the data to (1) search and (2) use it as input for machine solve it. <a href="http://www.mozillazine.org/articles/article572.html">MPT wrote one article in 1999</a> about things that you can <strong>only</strong> accomplished with MathML and is a good start point to think about it.</p>
<h1 id="what-can-we-do-next">What can we do next?</h1>
<p>We, mostly, need to convince publishers that they should delivery HTML with MathML instead of PDF's and support native MathML support (get math at mobile devices is hard). If you want to know more or are interested in helping, <a href="raniere@ime.unicamp.br">send me a email</a>.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>academia,ciência aberta,MathML</p>
</div>
<div class="tags">
<p>MathML</p>
</div>
<div class="comments">

</div>
