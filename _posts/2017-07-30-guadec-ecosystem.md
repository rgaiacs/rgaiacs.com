---
layout: post
title: "GUADEC 2017: Ecosystem"
author: "Raniere Silva"
categories: conference
tags: [jekyll]
image:
  feature: 2017-07-30-guadec-ecosystem.jpg
  author: s9-4pr
  original_url: https://flic.kr/p/Tb832Y
  licence: CC-BY
---

Today is the thirday and last day of [GUADEC 2017](https://2017.guadec.org/)
as a conference. There will be more three day as a unconference.

On the previous two days,
I learned how much a integrated ecosystem of software application
can make the user experience more pleasured
and, for the case of first time users, more friendly.
Unfortunately,
have the completely ecosystem is hard to implement
and keep consistence between the softwares is even more difficult.

For example,
[Evolution Data Server](https://wiki.gnome.org/Apps/Evolution)
allows that different Gnome software access the same information
and improve the user experience.
Unfortunately,
now that most users need email and chat on different devices,
e.g. home desktop, working laptop, mobile phone, tablet, ...,
the use of Google products is much more attractive than the alternatives.

I still considering
if I will get my hands durt with Lisp, when hacking Gnus, or Vala, when hacking Evolution.
Wait the next episodes.
