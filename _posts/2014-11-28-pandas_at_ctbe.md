---
layout: post
title: "Pandas at CTBE"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<div class="admonition">
<p>Thanks</p>
<p>I would like to thanks Marcelo Valadar for the invitation to taught a Pandas introduction at <a href="http://ctbe.cnpem.br/">CTBE</a>.</p>
</div>
<div class="admonition">
<p>Thanks</p>
<p>I would like to thanks very much to Wanderson Luiz that help me solving the issues that the students had during the lesson. I hope that he finishs his PhD soon and after that get involved with <a href="http://software-carpentry.org/">Software Carpentry</a> to have fun helping researchers to program (<a href="http://software-carpentry.org/v5/novice/matlab/index.html">even with MATLAB</a>).</p>
</div>
<p>Today I taught a Pandas' introduction at the &quot;Laboratório Nacional de Ciência e Tecnologia do Bioetanol&quot; (CTBE), one of the laboratories of <a href="http://www.cnpem.br/">&quot;Centro Nacional de Pesquisa em Energia e Materiais&quot; (CNPEM)</a>.</p>
<figure>
<img src="/images/room.jpg" class="align-center" style="width:80.0%" />
</figure>
<div class="more">

</div>
<h1 id="opening">Opening</h1>
<p>I chose start the lesson showing <a href="https://www.youtube.com/watch?v=3eiUffqU8QI">&quot;The Paper of the Future&quot; made by Josh Peek</a> (see below).</p>
<iframe width="560" height="315" src="//www.youtube.com/embed/3eiUffqU8QI"
frameborder="0" allowfullscreen></iframe>
<p>I watched this video at the begin of this week, thanks the recommendation of <a href="http://drclimate.wordpress.com/">Damien Irving</a>, and like it since it push the scientific communication to the 21st century.</p>
<figure>
<img src="/images/twitter.jpg" class="align-center" />
</figure>
<p>After the video I talk a little about why use Python and Pandas.</p>
<h1 id="topics-covered">Topics covered</h1>
<p>During the two and half hours of the lesson I taught</p>
<ul>
<li>IPython Notebook User Interface</li>
<li>Creation of variables</li>
<li>Operations with variables</li>
<li>Use of libraries/packages</li>
<li>Creation of lists</li>
<li>Operations with lists</li>
<li>Creation of dictionaries</li>
<li>Operations with dictionaries</li>
<li>Loading of CSV in Pandas</li>
<li>Selection of lines from Pandas's Dataframe</li>
<li>Selection of columns from Pandas' Dataframe</li>
<li>Filtering at Pandas' Dataframe</li>
<li>Plots from Pandas's Dataframe</li>
</ul>
<p>My notes are available <a href="https://r-gaia-cs.github.io/2014-11-28-ctbe/python/index.html">here</a> (but they are in Portuguese <strong>only</strong>).</p>
<h1 id="feedbacks">Feedbacks</h1>
<figure>
<img src="/images/students.jpg" class="align-center" style="width:80.0%" />
</figure>
<p><span data-role="download">From the feedbacks &lt;feedbacks.md&gt;</span>, all the students like the lesson. The only complain was the short duration of the lesson. I speed up at the end when presenting how to create plots but I hope that wasn't a big problem because create a plot with pandas only require that you type :</p>
<pre><code>my_dataset.plot()</code></pre>
<p>I only regret that we haven't time to do some exercises covering plots.</p>
<h1 id="next-steps">Next Steps</h1>
<figure>
<img src="/images/marcelo.jpg" class="align-center" style="width:80.0%" />
</figure>
<p>The year is almost at the end and is impossible to organize a <a href="http://software-carpentry.org/">Software Carpentry</a> workshop before it ends. Fortunately is a nice opportunity to start planning a Software Carpentry two days (enough time for students not complain about lack of time) workshop for 2015.</p>
<p>If you are more interested with data you can start planning the first ever <a href="http://datacarpentry.org/">Data Carpentry</a> two days workshop in Brazil for 2015.</p>
<p>In case you are interest in organizing any of the workshops suggested you can <a href="mailto:raniere@ime.unicamp.br">contact me</a> and I will be happy to help you starting your plans.</p>
<div class="admonition">
<p>Discloser</p>
<p>On December 11th and 12th I will be on Florianópolis with <a href="http://acropora.bio.mq.edu.au/people/diego-barneche/">Diego Barneche</a> for a R based Software Carpentry workshop. Feel free to advertise it for friends living in Florianópolis.</p>
</div>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>academia,ciência aberta,educação,ensino,Software Carpentry</p>
</div>
<div class="tags">
<p>CTBE,CNPEM</p>
</div>
<div class="comments">

</div>
