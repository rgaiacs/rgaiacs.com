---
layout: post
title: Contatos no Firefox OS
author: "Raniere Silva"
categories: ftb
tags: [ftb]
image:
  feature: ftb.jpg
---
<div class="admonition warning">

Este post está em construção.

</div>

[Firefox OS](https://www.mozilla.org/en-US/firefox/os/) é o sistema
operacional baseado em tecnologias web desenvolvido pela Mozilla para
celulares (e futuramente tablets e desktop).

Neste post você vai aprender como importar seus contatos no formato
[vCard](http://en.wikipedia.org/wiki/VCard) para o Firefox OS.

Versão 1.3
==========

<div class="admonition note">

Essa versão ainda não foi lançada. Você pode testá-la utilizando o
[simulador](https://ftp.mozilla.org/pub/mozilla.org/labs/fxos-simulator/)'.

</div>

Na tela inicial, na parte inferior encontra-se o ícone para a agenda de
contatos.

![](/images/ffos1.3-contacts01.png){width="25%"}

No canto superior direito, existe o ícone de configuração da agenda de
contatos. Selecione-o para ter acesso a opção de importar contatos.

![](/images/ffos1.3-contacts02.png){width="25%"}

Ao selecionar a opção de importar contatos, ele irá procurar por
arquivos vCard e importar os contatos nos arquivos encontrados.

![](/images/ffos1.3-contacts03.png){width="25%"}

**Referências**
