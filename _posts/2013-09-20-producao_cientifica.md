---
layout: post
title: "Produção Científica e Integridade"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Este post trata de uma das messas redondas que assisti no <span data-role="doc">SBNeC &lt;sbnec&gt;</span> cujo título era &quot;Produção Científica e Integridade em Pesquisas: Consensos, Demandas e Conflitos&quot; que foi coordenada por Sonia Vasconcelos da Universidade Federal do Rio de Janeiro e contou com a presença de:</p>
<ul>
<li>Claude Pirmez da Fundação Osvald Cruz que apresentou &quot;A Integridade em Pesquisa nas Intituições: Como Lidar com Conflitos?&quot;;</li>
<li>Maria Vargas da Universidade Federal Fluminense que apresentou &quot;Integridade Científica e Formação de Jovens Pesquisadores: Por Onde Começar?&quot;;</li>
<li>Rosemary Shinkai da PUC do Rio Grande do Sul que apresentou &quot;Integridade em Pesquisa: O que Mudou no Cenário Editorial?&quot;;</li>
<li>Sonia Vasconcelos da Universidade Federal do Rio de Janeiro que apresentou &quot;Integridade em Pesquisa e Correção da Literatura Científica: Desafios Contemporâneos&quot;.</li>
</ul>
<div class="more">

</div>
<p>Em todos os títulos encontra-se a palavra &quot;integridade&quot; que foi abordada tanto na questão de resultados apresentados como também nos pedidos de projetos (uso de frases de impacto, <em>buzz words</em>, promessas que não podem ser cumpridas, ...) e envolvia formas de má condução como:</p>
<ul>
<li>fabricação de resultados,</li>
<li>falsificação de resultados,</li>
<li>plágio,</li>
<li><em>ghostwriting</em>, ...</li>
</ul>
<p>Também foi abordado a questão do retratamento que é um tabu e mencionado o <a href="http://www.crossref.org/crossmark/">CrossMark</a>.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>academia</p>
</div>
<div class="tags">
<p>SBNeC</p>
</div>
<div class="comments">

</div>
