---
layout: post
title: "Escrita Colaborativa"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Os membros da comunidade de software livre (ou aberto) utilizam técnicas para o controle de versão desde seu início e hoje a ferramenta mais utilizada para isso é o <a href="http://git-scm.com/">Git</a>.</p>
<p>Associado ao Git existem vários serviços on-line para manter uma cópia das versões salvas onde o principal nome é o <a href="https://github.com/">GitHub</a> cujo slogan é &quot;Social coding&quot;, ou &quot;Código social&quot; em uma tradução literal.</p>
<p>Fora da comunidade de software livre a idéia o uso de controle de versão está apenas dando os primeiros passos que iniciaram com a Wikipédia e passou pelo Google Docs e o &quot;Time Machine&quot; da Apple.</p>
<p>O controle de versão da Wikipédia e do Google Docs funciona de maneira diferente do Git e isso será abordado a seguir com foco na comunidade científica.</p>
<div class="more">

</div>
<p>Minha motivação para este post foi o artigo de Konrad Lawson &quot;<a href="http://chronicle.com/blogs/profhacker/the-limitations-of-github-for-writers/48299">The Limitations of GitHub for Writers</a>&quot;.</p>
<p>Ao comparar o controle de versão da Wikipédia, Google Docs e Git a grande vantagem do Git é a funcionalidade denominada <em>branch</em> que possibilita a existência de duas versões ao mesmo tempo que posteriormente serão aglutinadas em um processo denominado <em>merge</em>.</p>
<p>Já a grande desvantagem do Git é que ele foi projetado para ser utilizado a partir da linha de comando (do terminal, do DOS ou da tela preta) o que desmotiva a grande maioria dos usuários.</p>
<p>Para a comunidade científica, a adoção do controle de versão abre inúmeras portas para aumentar a colaboração, principalmente em uma éoca em que o termo <em>open data</em> está na moda.</p>
<p>No seu artigo, Konrad Lawson, lista alguns serviços para a produção de textos de forma colaborativa:</p>
<ul>
<li><a href="http://authorea.com">Authorea</a></li>
<li><a href="http://draftin.com/">Draftin</a></li>
<li><a href="http://editorially.com/">Editorially</a></li>
</ul>
<p>A essa lista gostaria de adicionar <a href="http://fiduswriter.com/">Figuswriter</a>. Eu não testei nenhum dos serviços acima pois todos necessitam da criação de uma conta (e eu não gosto muito disso).</p>
<p>De toda forma, na minha opinião o que falta é um serviço que integre soluções como o Git e as listadas agora a pouco.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>academia</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
