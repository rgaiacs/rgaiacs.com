---
layout: post
title: "Math Virtual Keyboard Specifications"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<div class="admonition">
<p>Note</p>
<p>I should have done this at the first day of GSoC. =(</p>
</div>
<p><a href="https://bugzilla.mozilla.org/show_bug.cgi?id=983043">Bug 983043</a> is a meta bug where Firefox OS UX team make available the most updated version of Firefox OS Keyboard Specifications. Have the specifications in hand is awesome because you know what you need to do.</p>
<p>Instead of publish the specification for the math keyboard I started coding. This was good to discovery the features that I need to implement and discovery how Bugzilla work but it didn't help solving the problem of switching from one keyboard to another and this is the topic of this post.</p>
<div class="more">

</div>
<p>Firefox OS for input text has three layers:</p>
<ul>
<li>letter,</li>
<li>alternate and</li>
<li>symbol.</li>
</ul>
<p>Switch between any this layers is simple as explain at the picture below toke from &quot;<a href="https://bugzilla.mozilla.org/attachment.cgi?id=8434805">Keyboard Layout &amp; Behavior for Phone Version 2.1 [Jun 5, 2014]</a>.</p>
<figure>
<img src="/images/fxos-keyboard-v2.1.svg" class="align-center" style="width:100.0%" />
</figure>
<p>For handle math I need three additional layers:</p>
<ul>
<li>greek letters,</li>
<li>math symbols and</li>
<li>math functions.</li>
</ul>
<p>Right now I could only think in two ways to switch between all this layers.</p>
<h1 id="ime-switch-key">IME Switch Key</h1>
<p>One of the ways that I think is using the IME switch key and make the extra layers available as different IMEs.</p>
<figure>
<img src="/images/fxos-math-keyboard-v0.0a.svg" class="align-center" style="width:100.0%" />
</figure>
<p>This option is bad because the user will need to press the IME switch key many times to get the desire layers.</p>
<div class="admonition">
<p>Note</p>
<p>I'm not sure if some features like &quot;word&quot; suggestion can work across different IME.</p>
</div>
<h1 id="row-for-switch">Row for Switch</h1>
<p>The other way is using a extra row that list all the layers and the when the user press one of the key from this extra row the layer change.</p>
<figure>
<img src="/images/fxos-math-keyboard-4rows-v0.0a.svg" class="align-center" style="width:100.0%" />
</figure>
<div class="admonition">
<p>Note</p>
<p>I'm not sure if with this changes the keyboard could be available at the Gaia source.</p>
</div>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>GSOC</p>
</div>
<div class="tags">
<p>GSOC2014</p>
</div>
<div class="comments">

</div>
