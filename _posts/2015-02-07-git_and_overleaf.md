---
layout: post
title: Git and Overleaf
author: "Raniere Silva"
categories: ftb
tags: [ftb]
image:
  feature: ftb.jpg
---
In a past post &lt;../../01/20/git\_and\_sharelatex&gt; I wrote about
[Git](http://git-scm.com/)/[GitHub](http://github.com/) support at
[ShareLaTeX](http://sharelatex.com/). To be fair with similar services,
I will write at this post about [Overleaf](https://www.overleaf.com/),
the new brand for WriteLaTeX, support for Git, see their announcement
[here](https://www.overleaf.com/blog/195-new-collaborate-online-and-offline-with-overleaf-and-git-beta#.VNYTkpSirww),
and in another post about [Authorea](http://authorea.com/) support.

Create Overleaf Project
=======================

First, open <https://www.overleaf.com/> at your web browser.

![](/images/overleaf1.jpg){width="80%"}

Select "Sign in" at the top right corner of the page.

![](/images/overleaf2.jpg){width="80%"}

Enter your credentials to access your Overleaf account.

![](/images/overleaf3.jpg){width="80%"}

Select "Create new project" and one template (I will select the sample
paper).

![](/images/overleaf4.jpg){width="80%"}

Cloning Overleaf Project
========================

The URL of my newest project is
<https://www.overleaf.com/2215649thxpzv#/5661908/> so I clone it with :

    $ git clone https://git.overleaf.com/2215649thxpzv sample-paper
    Cloning into 'sample-paper'...
    remote: Counting objects: 4, done
    remote: Finding sources: 100% (4/4)
    remote: Getting sizes: 100% (3/3)
    remote: Compressing objects: 100% (100208/100208)
    remote: Total 4 (delta 0), reused 0 (delta 0)
    Unpacking objects: 100% (4/4), done.
    Checking connectivity... done.

After clone the project I can change the working directory to the one
created by Git :

    $ cd sample-paper

and check the files in my project :

    $ ls
    frog.jpg  main.tex

Send Changes to Overleaf
========================

Once I have the project in my machine I want to make changes locally and
backup it later at Overleaf server. As a exemple, I will change the
author's name. :

    $ sed -i 's/author{You}/author{Raniere Silva}/' main.tex
    $ git diff
    diff --git a/main.tex b/main.tex
    index 7ec4e69..457f0e2 100644
    --- a/main.tex
    +++ b/main.tex
    @@ -8,7 +8,7 @@

     \title{Your Paper}

    -\author{You}
    +\author{Raniere Silva}

     \date{\today}

After made some changes, I need to commit it. :

    $ git commit -am 'Set my name as author'
    [master b94883d] Set my name as author
     1 file changed, 1 insertion(+), 1 deletion(-)

The last step is send it to Overleaf. :

    $ git push origin master
    Counting objects: 3, done.
    Delta compression using up to 4 threads.
    Compressing objects: 100% (3/3), done.
    Writing objects: 100% (3/3), 329 bytes | 0 bytes/s, done.
    Total 3 (delta 1), reused 0 (delta 0)
    remote: Resolving deltas: 100% (1/1)
    remote: Updating references: 100% (1/1)
    To https://git.overleaf.com/2215649thxpzv
       0839be1..b94883d  master -> master

After you push your changes, your web browser will load the new version
of your project. **Awesome!!!**

![](/images/overleaf5.jpg){width="80%"}

Testing Forced Push
===================

Normally Git will not allow you push **fast-forward** changes because
this is dangerous to your project but you can overwrite this behavior if
you want. Since Overleaf doesn't required authentication right now,
[they are working on that for
v1.0](https://twitter.com/jdleesmiller/status/555825862764154880), allow
forced push is **very** dangerous. Let try to explore this. :

    $ sed -i 's/author{Raniere Silva}/author{Eve}/' main.tex
    $ git diff
    diff --git a/main.tex b/main.tex
    index 457f0e2..abbfe03 100644
    --- a/main.tex
    +++ b/main.tex
    @@ -8,7 +8,7 @@

     \title{Your Paper}

    -\author{Raniere Silva}
    +\author{Eve}

     \date{\today}

    $ git commit -a --amend -m 'Eve attack'
    [master 5af7efe] Eve attack
     Date: Sat Feb 7 11:52:07 2015 -0200
     1 file changed, 1 insertion(+), 1 deletion(-)
    $ git push -f origin master
    Counting objects: 3, done.
    Delta compression using up to 4 threads.
    Compressing objects: 100% (3/3), done.
    Writing objects: 100% (3/3), 315 bytes | 0 bytes/s, done.
    Total 3 (delta 1), reused 0 (delta 0)
    remote: Resolving deltas: 100% (1/1)
    remote: error: forced push prohibited
    remote: hint: You can't git push --force to a Overleaf project.
    remote: hint: Try to put your changes on top of the current head.
    remote: hint: If everything else fails, delete and reclone your repository, make your changes, then push again.
    To https://git.overleaf.com/2215649thxpzv
     ! [remote rejected] master -> master (forced push prohibited)
    error: failed to push some refs to 'https://git.overleaf.com/2215649thxpzv'

Great that Overleaf did their homework. =)

<div class="admonition">

Note

We can restore the `master` branch with :

    $ git checkout origin/master
    Note: checking out 'origin/master'.

    You are in 'detached HEAD' state. You can look around, make experimental
    changes and commit them, and you can discard any commits you make in this
    state without impacting any branches by performing another checkout.

    If you want to create a new branch to retain commits you create, you may
    do so (now or later) by using -b with the checkout command again. Example:

      git checkout -b new_branch_name

    HEAD is now at b94883d... Set my name as author
    $ git branch -f master
    $ git checkout master
    Switched to branch 'master'
    Your branch is up-to-date with 'origin/master'.

</div>

Testing Conflicts
=================

To test conflicts, let first set the title of the project using the web
interface.

![](/images/overleaf6.jpg){width="80%"}

Later, let set the title of the project at our local copy. :

    $ sed -i 's/title{Your Paper}/title{Local title}/' main.tex
    $ git diff
    diff --git a/main.tex b/main.tex
    index 457f0e2..dbe8bd7 100644
    --- a/main.tex
    +++ b/main.tex
    @@ -6,7 +6,7 @@
     \usepackage{graphicx}
     \usepackage[colorinlistoftodos]{todonotes}

    -\title{Your Paper}
    +\title{Local title}

     \author{Raniere Silva}

    $ git commit -am 'Set title'
    [master 486d8d4] Set title
     1 file changed, 1 insertion(+), 1 deletion(-)

And try send it. :

    $ git push origin master
    To https://git.overleaf.com/2215649thxpzv
     ! [rejected]        master -> master (fetch first)
    error: failed to push some refs to 'https://git.overleaf.com/2215649thxpzv'
    hint: Updates were rejected because the remote contains work that you do
    hint: not have locally. This is usually caused by another repository pushing
    hint: to the same ref. You may want to first integrate the remote changes
    hint: (e.g., 'git pull ...') before pushing again.
    hint: See the 'Note about fast-forwards' in 'git push --help' for details.

As expected, Overleaf rejected the push. We should pull the changes,
merge it and try the push again. :

    $ git fetch origin
    remote: Counting objects: 5, done
    remote: Finding sources: 100% (3/3)
    remote: Getting sizes: 100% (4/4)
    remote: Compressing objects: 100% (3137/3137)
    remote: Total 3 (delta 0), reused 0 (delta 0)
    Unpacking objects: 100% (3/3), done.
    From https://git.overleaf.com/2215649thxpzv
       b94883d..f878414  master     -> origin/master
    $ git log --oneline --graph --all --decorate
    * 486d8d4 (HEAD, master) Set title
    | * f878414 (origin/master, origin/HEAD) Update on Overleaf.
    |/
    * b94883d Set my name as author
    * 0839be1 Update on Overleaf.
    $ git merge origin/master
    Auto-merging main.tex
    CONFLICT (content): Merge conflict in main.tex
    Automatic merge failed; fix conflicts and then commit the result.
    $ git diff
    diff --cc main.tex
    index dbe8bd7,9bbbba9..0000000
    --- a/main.tex
    +++ b/main.tex
    @@@ -6,7 -6,7 +6,11 @@@
      \usepackage{graphicx}
      \usepackage[colorinlistoftodos]{todonotes}

    ++<<<<<<< HEAD
     +\title{Local title}
    ++=======
    + \title{Web title}
    ++>>>>>>> origin/master

      \author{Raniere Silva}

After solve the conflict:

    $ git commit -am 'Merge local changes'
    [master fd7d10a] Merge local changes
    $ git push push origin master
    Counting objects: 6, done.
    Delta compression using up to 4 threads.
    Compressing objects: 100% (6/6), done.
    Writing objects: 100% (6/6), 603 bytes | 0 bytes/s, done.
    Total 6 (delta 2), reused 0 (delta 0)
    remote: Resolving deltas: 100% (2/2)
    remote: Updating references: 100% (1/1)
    To https://git.overleaf.com/2215649thxpzv
       f878414..fd7d10a  master -> master

Again, after you push your changes, your web browser will load the new
version of your project.

![](/images/overleaf7.jpg){width="80%"}

Online Revisions
================

Would be nice to have access to Git revisions from the web interface. :

    $ git log --oneline --graph --decorate --all
    *   fd7d10a (HEAD, origin/master, origin/HEAD, master) Merge local changes
    |\
    | * f878414 Update on Overleaf.
    * | 486d8d4 Set title
    |/
    * b94883d Set my name as author
    * 0839be1 Update on Overleaf.

Unfortunately this didn't happen.

![](/images/overleaf8.jpg){width="80%"}

Also, would be nice to have access to web revisions at Git.

![](/images/overleaf8.jpg){width="80%"}

Unfortunately this also didn't happen. :

    $ git fetch origin
    $ git log --oneline --graph --decorate --all
    *   fd7d10a (HEAD, origin/master, origin/HEAD, master) Merge local changes
    |\
    | * f878414 Update on Overleaf.
    * | 486d8d4 Set title
    |/
    * b94883d Set my name as author
    * 0839be1 Update on Overleaf.

Conclusions
===========

Overleaf support for Git should make Git and non-Git users happy
collaborating with each other since every one can continue using their
favorite workflow. Some features, like bridge between web interface and
Git revisions, would be nice to have. Also **a project management/issue
tracking** will be a great enhancement.

Extra
=====

When writing this post I discovery that Overleaf has a WYSIWYG-like
editor. This is a very nice feature!

![](/images/overleaf-rich.jpg){width="80%"}
