---
layout: post
title: "Every (and Another) Day"
author: "Raniere Silva"
categories: books
tags: [jekyll]
image:
  feature: 2017-07-04-every-and-another-day.jpg
  author: Kelsey Meyer
  original_url: https://flic.kr/p/W2mMiU
  licence: CC-BY
---

I know that I failed to write more on this blog.
I still need to write about
"[The Hidden School](http://www.peacefulwarrior.com/the-hidden-school/)"
but today I will write a little about
"[Every Day](https://en.wikipedia.org/wiki/Every_Day_(book))"
and
"[Another Day](https://en.wikipedia.org/wiki/Another_Day_(2015))",
two books by [David Levithan](https://en.wikipedia.org/wiki/David_Levithan).

Both books are about the romance between A and Rhiannon.
The first book, Every Day, follows A and the second book, Another Day, follows Rhiannon.
What make the romance between the two hard is that
**every day** A wake up in a different body.

If I can take something home from the reading is that you always need to be
prepare to change.
