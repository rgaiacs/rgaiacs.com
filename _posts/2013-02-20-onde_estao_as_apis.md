---
layout: post
title: "Onde estao as APIs"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>API ou &quot;Application Programming Interface&quot; é um conjunto de rotinas e padrões estabelecidos por um software para a utilização das suas funcionalidades por aplicativos que não pretendem envolver-se em detalhes da implementação do software, mas apenas usar seus serviços.<a href="#fn1" class="footnote-ref" id="fnref1"><sup>1</sup></a></p>
<p>APIs são importantíssimas no desenvolvimento de software porque sem elas seria preciso reimplementar várias bibliotecas. No ambiente desktop é extremamente fácil encontrar a API desejada, seja como uma &quot;man page&quot; ou por meio de um ambiente de desenvolvimento (e.g., perldoc, pydoc, javadoc, ...).</p>
<p>No contexto de desenvolvimento web, uma API é um conjunto definido de mensagens de requisição e resposta HTTP, geralmente expressado nos formatos XML ou JSON. <a href="#fn2" class="footnote-ref" id="fnref2"><sup>2</sup></a> Infelizmente, a grande maioria dos serviços web não fornece uma API e quando fornece é preciso registrar-se para conseguir utilizá-la.</p>
<h1 id="referências">Referências</h1>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>none</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
<section class="footnotes">
<hr />
<ol>
<li id="fn1"><p><a href="http://pt.wikipedia.org/wiki/API" class="uri">http://pt.wikipedia.org/wiki/API</a><a href="#fnref1" class="footnote-back">↩</a></p></li>
<li id="fn2"><p><a href="http://pt.wikipedia.org/wiki/API" class="uri">http://pt.wikipedia.org/wiki/API</a><a href="#fnref2" class="footnote-back">↩</a></p></li>
</ol>
</section>
