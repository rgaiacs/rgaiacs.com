---
layout: post
title: "MathML November Meeting"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<div class="admonition">
<p>Note</p>
<p>Sorry for the delay to write this.</p>
</div>
<p>This is a report about the Mozilla MathML November IRC Meeting (see the announcement <a href="https://groups.google.com/d/msg/mozilla.dev.tech.mathml/9TnVeS9-eq8/dj9PXSSMdBsJ">here</a>). The topics of the meeting can be found <a href="https://etherpad.mozilla.org/mathml-2014-11">in this PAD</a> (<span data-role="download">local copy of the
PAD &lt;pad.txt&gt;</span>) and the <a href="http://logs.glob.uno/?c=mozilla%23mathml&amp;s=12+Nov+2014&amp;e=12+Nov+2014">IRC log</a> (<span data-role="download">local copy of the IRC log &lt;irc.txt&gt;</span>) is also available.</p>
<p>The next meeting will be in January 7th at 8pm UTC (<a href="http://www.timeanddate.com/worldclock/meetingdetails.html?year=2015&amp;month=1&amp;day=7&amp;hour=20&amp;min=0&amp;sec=0&amp;p1=240&amp;p2=137&amp;p3=179&amp;p4=233&amp;p5=195&amp;p6=37">check the time at your location here</a>). Please add topics in the <a href="https://etherpad.mozilla.org/mathml-2015-01">PAD</a>.</p>
<div class="admonition">
<p>Note</p>
<p>Yes. Our December meeting was cancelled. =(</p>
</div>
<div class="more">

</div>
<h1 id="mathml-community">MathML Community</h1>
<p>I'm thinking in how get more people involved with MathML and increase the non programmers user base of MathML (probably get more involved with <a href="http://webmaker.org/">Mozilla's WebMaker Project</a>).</p>
<h1 id="meetings-format">Meetings Format</h1>
<p>In the past months we had try use WebRTC based solutions for video conference but I'm not 100% sure that it is the best option right now because:</p>
<ol type="1">
<li>it requires more bandwidth,</li>
<li>it only support a very limited number of user at the same time,</li>
<li>it isn't recorded (or people can avoid to participate on it because of it will be record).</li>
</ol>
<p>I don't want to ask contributors to install/use proprietary software neither create a account in some server. That's was the reason that we aren't going to use Google Hangout or Skype.</p>
<p>I will try find if Mozilla can offer us a toll-free numbers for the meetings.</p>
<h1 id="bot-at-mathml">Bot at #mathml</h1>
<p>I created a <a href="http://irc-wiki.org/Supybot">Supybot</a>, currently named mathbot (if you have a better name please <a href="mailto:raniere@ime.unicamp.br">let me know</a>), to help the communication at our IRC channel.</p>
<p>The <a href="http://ubottu.com/supydocs/plugins/Later.html">Latter plugin</a> is enabled and you can used it to leave a message to someone when he/she is offline (he/she will receive the message the next time he/she join our channel).</p>
<p>I'm finishing to change the <a href="https://gitorious.org/supybot-plugins/progval-supybot-plugins/source/5bbcfa92fe96b6c11c610af211796a0ba634bb4c:Trigger">Trigger plugin</a> to welcome new user when they join our channel. This was inspired by <a href="http://helpmebot.org.uk/wiki/Main_Page">Wikipedia's Helpmebot</a> and <a href="https://openhatch.org/">OpenHatch</a>'s <a href="https://github.com/shaunagm/WelcomeBot">WelcomeBot</a>.</p>
<p>I'm also thinking in enable <a href="http://ubottu.com/supydocs/plugins/Bugzilla.html">Bugzilla plugin</a>.</p>
<p>If you want another plugin enabled <a href="mailto:raniere@ime.unicamp.br">get in touch</a>.</p>
<h1 id="wikipedia">Wikipedia</h1>
<p>Moritz &quot;want to create a rfc to make mathml the default rendering mode&quot; of <a href="https://www.mediawiki.org/wiki/Extension:Math">MediWiki Math Extension</a>. If you want to follow the progress of this extension you can check its <a href="https://www.mediawiki.org/wiki/Extension:Math/Roadmap">roadmap</a>.</p>
<h1 id="authoring-tools">Authoring Tools</h1>
<p>Regards authoring tools Frédéric worked to <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=1077291">&quot;add some UI to easily insert mathematical symbols into the LaTeX input box&quot; presented at SeaMonkey</a>. You can help testing it.</p>
<h1 id="gecko">Gecko</h1>
<p>Some progress was made with font Inflation and automatic font installation due the work of, in alphabetic order, Frédéric Wang, James Kitchener, Karl Tomlinson, Robert O'Callahan and Ryan VanderMeulen.</p>
<h1 id="firefoxs-10th-aniversarry">Firefox's 10th Aniversarry</h1>
<p>This month <a href="https://blog.mozilla.org/blog/2014/11/10/celebrating-10-years-of-firefox-2/">Firefox is completing 10 years old</a> and Mozilla did a <a href="https://wiki.mozilla.org/Firefox/Decade">big celebration for it</a>. Some &quot;funny&quot; things about it and MathML:</p>
<ul>
<li>&quot;MathML was introduced in 1998 as the first application of XML&quot; [<a href="http://www.w3.org/Math/Documents/mathml-history.html">W3C</a>] so MathML is 16 years old.</li>
<li><a href="http://www.mozillazine.org/talkback.html?article=560">MathML Project at Mozilla started at the begin of 1999</a> so we already have 15 years.</li>
<li><a href="http://www.mozillazine.org/talkback.html?article=811">MathML landed at the end of 1999</a></li>
<li><a href="https://developer.mozilla.org/en-US/docs/Mozilla/MathML_Project/Updates/The_First_International_MathML_Conference_%28October_2000%29">The First International MathML Conference</a> happens in 200.</li>
</ul>
<h1 id="upcoming-events">Upcoming Events</h1>
<table>
<thead>
<tr class="header">
<th>Date</th>
<th>Name</th>
<th>Web Site</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>2014/12/07-10</td>
<td>Web Engines Hackfest 20144</td>
<td><a href="http://www.webengineshackfest.org/" class="uri">http://www.webengineshackfest.org/</a></td>
</tr>
<tr class="even">
<td>2015/07/13-17</td>
<td>Conference on Intelligent Computer Mathematics (CICM)</td>
<td><a href="http://www.cicm-conference.org/2015/" class="uri">http://www.cicm-conference.org/2015/</a></td>
</tr>
</tbody>
</table>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>MathML,Mozilla</p>
</div>
<div class="tags">
<p>MathML meetings</p>
</div>
<div class="comments">

</div>
