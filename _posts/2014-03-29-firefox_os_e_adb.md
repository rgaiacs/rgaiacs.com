---
layout: post
title: Firefox OS e ADB
author: "Raniere Silva"
categories: ftb
tags: [ftb]
image:
  feature: ftb.jpg
---
[Firefox OS](https://www.mozilla.org/en-US/firefox/os/) é o sistema
operacional desenvolvido pela Mozilla. Para acessar algumas
funcionalidades internas dos aparelhos com Firefox OS é necessário
utilizar o [Android Debug Bridge
(ADB)](http://developer.android.com/tools/help/adb.html).

Nesse post será mostrado como instalar e utilizar o ADB para acessar um
aparelho com Firefox OS.

Instalação para Arch Linux
==========================

Antes de mais nada é preciso habilitar o "multilib" para o gerenciador
de pacotes. No arquivo `/etc/pacman.conf`, certifique-se de ter as
linhas abaixo:

    [multilib]
    Include = /etc/pacman.d/mirrorlist

Depois é hora de instalar os pacotes de desenvolvimento do Android:

    # yaourt -Sy android-sdk android-sdk-platform-tools android-sdk-build-tools

Os pacotes de desenvolvimento do Android são instalados em
`/opt/android-sdk/tools/` e `/opt/android-sdk/platform-tools/`.

Para que o aparelhos seja reconhecido:

    # yaourt android-udev

Conectando Aparelho
===================

Habilite o acesso remoto pelo aparelho:

    Settings > Device information > More information > Developer > Remote debugging

Para descobrir o seu aparelho:

    # /opt/android-sdk/platform-tools/adb kill-server
    # /opt/android-sdk/platform-tools/adb start-server
    * daemon not running. starting it now on port 5037 *
    * daemon started successfully *
    # /opt/android-sdk/platform-tools/adb devices     
    List of devices attached 
    MSM7627A        device

Se nenhum dispositivo for listado, você deve ter esquecido de habilitar
o acesso remoto ou seu computador não estão conseguindo reconhecer o
aparelho.

Para obter acesso:

    # /opt/android-sdk/platform-tools/adb shell

Agora você deve está na shell do seu aparelho:

    shell@android:/ $

**Referências**
