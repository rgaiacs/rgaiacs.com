---
layout: post
title: Sobre este blog
author: "Raniere Silva"
categories: ftb
tags: [ftb]
image:
  feature: ftb.jpg
---
Este blog foi criado para reunir alguns hacks desenvolvidos por mim
envolvendo software livre.

Tentarei ser o mais claro possível nos post mas se você desejar tentar
reproduzí-los espero que você possua alguma distro GNU/LINUX instalada
na sua máquina e um mínimo de conhecimento de como utilizar o terminal.

Inicialmente meu maior interesse é em hackear

1.  Firefox OS,
2.  Kobo ereader,
3.  EPUB.
