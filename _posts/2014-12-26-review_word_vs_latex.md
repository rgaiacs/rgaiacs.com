---
layout: post
title: "Review &quot;An Efficiency Comparison of Document Preparation Systems Used in Academic Research and Development&quot;"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Today someone email of of the mailing list that I subscribed about a article wrote by Markus Knauff and Jelica Nejasmic with the title &quot;<a href="http://www.plosone.org/article/info%3Adoi%2F10.1371%2Fjournal.pone.0115069">An Efficiency Comparison of Document Preparation Systems Used in Academic Research and Development</a>&quot; and published at <a href="http://www.plosone.org">PLOS ONE</a>. <strong>This is a open access journal so if you want to read the article before read this post just click on the first link and enjoy the article.</strong></p>
<p>At this post I will make some comments about the article.</p>
<div class="more">

</div>
<h1 id="how-you-can-compare-apples-and-oranges">How you can compare apples and oranges?</h1>
<p>At the introduction we have</p>
<blockquote>
<p>Microsoft Word is based on a principle called &quot;What you see is what you get&quot; (WYSIWYG), which means that the user immediately sees the document on the screen as it will appear on the printed page. LaTeX, in contrast, embodies the principle of &quot;What you get is what you mean&quot; (WYGIWYM), which implies that the document is not directly displayed on the screen and changes, such as format settings, are not immediately visible.</p>
</blockquote>
<p>The authors are saying that Microsoft Word and LaTeX are different starting from their designs. So how do you want to compare it?</p>
<h1 id="novices-vs-experts">Novices vs Experts</h1>
<p>About the division of the participants in groups we have</p>
<blockquote>
<p>The participants were divided into 4 groups with 10 participants in each group: Word novices, Word experts, LaTeX novices, and LaTeX experts. Participants were classified as &quot;novices&quot; if they had less than 500 hours of experience with the respective program and “experts” if they had more than 1000 hours of experience with the respective program.</p>
</blockquote>
<p>This classification of novices and experts is too much simplistic. With this separation will not be hard to find a Word expert that doesn't know the difference of add a figure as embed element or as a link.</p>
<div class="admonition">
<p>Note</p>
<p>Figures included as a link are automatically updated when another application change it. LaTeX only support this type of image inclusion so for a far comparison should be requested that Word users use <strong>only</strong> this type of figure inclusion.</p>
</div>
<div class="admonition">
<p>Note</p>
<p>In the same way that with this criterion is easy to find Word expert that doesn't know basic features of Word is also easy to find LaTeX expert that doesn't know basic features of LaTeX.</p>
</div>
<h1 id="the-continuous-text">The continuous text</h1>
<div class="admonition">
<p>Note</p>
<p>You will find the <a href="http://www.plosone.org/article/info:doi/10.1371/journal.pone.0115069.g001/largerimage">text used here</a>.</p>
</div>
<p>The continuous text looks fair to me.</p>
<p>From the result, I still have questions of how much the LaTeX users know of the IDE they use.</p>
<h1 id="the-table-text">The table text</h1>
<div class="admonition">
<p>Note</p>
<p>You will find the <a href="http://www.plosone.org/article/info:doi/10.1371/journal.pone.0115069.g002/largerimage">table used here</a>.</p>
</div>
<p>The table has multi columns and multi lines that, <strong>in LaTeX</strong>, request the use of the packages <code>multicols</code> and <code>multirow</code>, respectively. Reproduce this table will be almost impossible for any LaTeX novice user and any LaTeX expert will probably use an third party tool, like <a href="http://sourceforge.net/projects/calc2latex/">Calc2LaTeX</a>, to get this done.</p>
<p>The result is, more or less, what I expected. Word and LaTeX users had the same amount of orthographic and grammatical mistakes but the amount of formatting erros and typos is high among LaTeX users (probably because the requested table isn't easy to be reproduce in LaTeX). I don't know how the amount of written text was calculate so I will not make any comment.</p>
<h1 id="the-equation-text">The equation text</h1>
<div class="admonition">
<p>Note</p>
<p>You will find the <a href="http://www.plosone.org/article/info:doi/10.1371/journal.pone.0115069.g003/largerimage">equations used here</a>.</p>
</div>
<p>Except for the equation numbering doesn't start at one this should be easy for any Word or LaTeX expert. I expect that LaTeX novice have some issues with the last equation since it request the alignment of equations and the use of <code>\nonumber</code> to get ride of the equation numbering in the first two lines.</p>
<div class="admonition">
<p>Note</p>
<p>In LaTeX, the last equation should be type as:</p>
<pre><code>\begin{align}
  \frac{\partial}{\partial \phi_c(j)} \frac{1}{a(\phi_c)} &amp;= \frac{-1}{(a
  (\phi_c))^2} \frac{\partial}{\partial \phi_c(j)} a(\phi_c) \nonumber \\
  &amp;= \frac{-1}{(a(\phi_c))^2} \sum_{r \in \mathcal{R}} \exp(C(r)/c) x_r(j))
  \nonumber \\
  &amp;= \frac{-1}{a(\phi_c)} p_c(x(j))
\end{align}</code></pre>
<p>If you like you could replace <code>(</code> with <code>\left(</code> and <code>)</code> with <code>\right)</code>.</p>
</div>
<p>As expect in this case LaTeX has a small advantage.</p>
<h1 id="usability-questionnaire">Usability questionnaire</h1>
<p>This is the part that I'm little happy about since it confirms what many of us &quot;know&quot;:</p>
<ul>
<li>&quot;Word users rated their respective software as less efficient than LaTeX users&quot;,</li>
<li>&quot;LaTeX users rated the learnability of their respective software as poorer than Word users&quot;.</li>
</ul>
<h1 id="discussion">Discussion</h1>
<p>The author first said that</p>
<blockquote>
<p>&quot;Another characteristic of our study is that it is practically impossible to evaluate LaTeX without also evaluating the used editors. In fact, our research measured the efficiency of Word against LaTeX in combination with some editor interfaces.&quot;</p>
</blockquote>
<p>and next that</p>
<blockquote>
<p>&quot;our results suggest that LaTeX reduces the user's productivity and results in more orthographical, grammatical, and formatting errors, more typos, and less written text than Microsoft Word over the same duration of time.&quot;</p>
</blockquote>
<p>From my experience, the fact that more orthographical and grammatical erros among LaTeX users is due the fact that many LaTeX IDE don't have the spell check feature enable out of the box and many users don't configure it. I would like to see how Word users performs with Word spell check disable.</p>
<h1 id="others-reviews">Others reviews</h1>
<p>You may also like to read others reviews:</p>
<ul>
<li><a href="http://serialmentor.com/blog/2014/12/27/post-publication-review-of-the-plos-one-paper-comparing-ms-word-and-latex-how-not-to-compare-document-preparation">Post-publication review of the PLOS ONE paper comparing MS Word and LaTeX: How not to compare document preparation</a> by Claus Wilke.</li>
<li><a href="http://khinsen.wordpress.com/2014/12/29/drawing-conclusions-from-empirical-science/">Drawing conclusions from empirical science</a> by Konrad Hinsen.</li>
</ul>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>academia,ciência aberta,educação</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
