---
layout: post
title: Atualizando Alcatel One Touch Fire
author: "Raniere Silva"
categories: ftb
tags: [ftb]
image:
  feature: ftb.jpg
---
Em um post anterior &lt;../../03/29/firefox\_os\_e\_adb&gt; foi
apresentando com utilizar o adb para acessar o Alcatel One Touch Fire.
Neste post será mostrado como rootar e atualizar esse aparelho.

Glossário
=========

Antes de mais nada é importante saber o significado de alguns termos:

-   *fastboot mode*:

    É um modo que segue um protocolo para reescrever uma ou mais
    partições de um dispositivo.

    Para entrar no *fastboot mode* do Alcatel One Touch Fire:

    1.  Desligue o aparelho.
    2.  Ligue o aparelho.
    3.  Mantenha o botão para reduzir o volume pressionado por 5
        segundos.

    ou :

        $ adb reboot bootloader

    essa última opção parece não funcionar como o Alcatel One Touch
    Fire.

    Se o aparelho parar na tela com o logo da Alcatel significa que você
    está no *fastboot mode*. Para verificar que o aparelho está no
    *fastboot mode*:

        $ fastboot devices
        MSM7627A        fastboot

    Se seu dispositivo não for listado, tente executar o mesmo comando
    como super usuário (também conhecido como `root`).

    Para sair do *fastboot mode*:

        $ fastboot reboot

Fazendo Backup
==============

<div class="admonition note">

É importante você fazer o backup antes de continuar para conseguir
restaurar seu aparelho caso tenha algum problema.

</div>

Seguir os passos em[^1].

Atualizando
===========

Em <http://elsimpicuitico.wordpress.com/firefoxos/> você irá encontrar
várias versões do Firefox OS para baixar. Escolha a que mais lhe agrada
e inicie o download.

Depois que o download tiver sido concluído, verifique se o arquivo não
foi corrompido:

    $ ls
    images-hamachi-v1.4-20140430.tar.xz  md5-correto
    $ cat md5-correto 
    e4609a00fa8274302cc77e5304d99888  images-hamachi-v1.4-20140430.tar.xz
    $ md5sum -c md5-correto
    images-hamachi-v1.4-20140430.tar.xz: OK

Se o arquivo estiver OK, é hora de descompactá-lo:

    $ tar -xvf images-hamachi-v1.4-20140430.tar.xz 
    images-hamachi-v1.4-20140430/
    images-hamachi-v1.4-20140430/fastboot.exe
    images-hamachi-v1.4-20140430/userdata.img
    images-hamachi-v1.4-20140430/flash.sh
    images-hamachi-v1.4-20140430/AdbWinApi.dll
    images-hamachi-v1.4-20140430/fastboot
    images-hamachi-v1.4-20140430/flash.bat
    images-hamachi-v1.4-20140430/AdbWinUsbApi.dll
    images-hamachi-v1.4-20140430/adb.mac
    images-hamachi-v1.4-20140430/adb
    images-hamachi-v1.4-20140430/recovery.img
    images-hamachi-v1.4-20140430/adb.exe
    images-hamachi-v1.4-20140430/boot.img
    images-hamachi-v1.4-20140430/fastboot.mac
    images-hamachi-v1.4-20140430/flash_mac.sh
    images-hamachi-v1.4-20140430/system.img

E entramos no diretório criado:

    $ cd images-hamachi-v1.4-20140430

**Com o aparelho no modo *fastboot modo*** executamos os comandos a
seguir:

    $ fastboot flash boot boot.img 
    sending 'boot' (6144 KB)...
    OKAY [  0.568s]
    writing 'boot'...
    OKAY [  1.316s]
    finished. total time: 1.884s
    $ fastboot flash userdata userdata.img 
    sending 'userdata' (4 KB)...
    OKAY [  0.004s]
    writing 'userdata'...
    OKAY [  1.918s]
    finished. total time: 1.922s
    $ fastboot flash system system.img
    sending 'system' (181021 KB)...
    OKAY [ 16.662s]
    writing 'system'...
    OKAY [ 39.086s]
    finished. total time: 55.748s
    $ fastboot flash recovery recovery.img 
    sending 'recovery' (7112 KB)...
    OKAY [  0.651s]
    writing 'recovery'...
    OKAY [  1.611s]
    finished. total time: 2.262s
    $ fastboot erase cache
    erasing 'cache'...
    OKAY [  0.478s]
    finished. total time: 0.478s

<div class="admonition note">

Se os comandos anteriores apresentarem problemas com a mensagem
`<waiting for device>` tente executar os comandos com permissão de super
usuário (também conhecido como `root`).

</div>

E para finalizar reiniciamos o aparelhos:

    $ fastboot reboot

Testando
========

Depois de reiniciar o aparelho ele deve iniciar com a nova imagem.

![](/images/firefox_os_atualizado.png){width="60%"}

Se tudo correu bem, agora é só aproveitar o sistema atualizado.

Referências
===========

[^1]: [\[Tutorial\] Backup Alcatel One Touch
    Fire](https://support.mozilla.org/pt-BR/forums/firefox-os-portuguese-forum/710009?last=57854)
