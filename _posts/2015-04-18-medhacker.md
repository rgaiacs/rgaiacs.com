---
layout: post
title: "MedHacker"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<div class="admonition">
<p>Agradecimento</p>
<p>Agradeço ao Prof. Paulo Schor pelo convite para participar da conversa sobre inovação aberta na última sexta-feira, 17/04/2015.</p>
</div>
<div class="admonition">
<p>Agradecimento</p>
<p>Agradeço ao Otto Heringer pela recomendação feita ao Prof. Paulo Schor.</p>
</div>
<div class="admonition">
<p>Agradecimento</p>
<p>Agradeço ao Eric Lopes pelo companhia na última sexta-feira.</p>
</div>
<div class="admonition">
<p>Slides</p>
<p>Meus slides <span data-role="download">encontram-se aqui &lt;2015-04-17-medhacker.zip&gt;</span>.</p>
</div>
<p>No dia 17/04/2015 eu participei de uma conversa sobre inovação aberta promovida pelo <a href="http://www2.unifesp.br/ligas/medhacker/">MedHackar</a> que foi maravilhosa.</p>
<div class="more">

</div>
<h1 id="você-tem-fome-de-que">Você tem fome de que?</h1>
<p>A programação do dia começou com uma apresentação da <a href="http://www.discombobulate.me">Paloma Oliveira</a> sobre sua experiência em transitar entre mundos/comunidades diferentes: artes plásticas, computação/software livre, biologia, ...</p>
<p>Já fazia bastante tempo que eu não assistia uma palestra não-técnica tão boa como a da Paloma.<a href="#fn1" class="footnote-ref" id="fnref1"><sup>1</sup></a> Ela falou de algumas coisas que estive pensando nos últimos tempos, principalmente, sobre como trazer pessoas para uma causa.</p>
<p>Pensei em convidar a Paloma para participar do <a href="http://libreplanetbr.org/flisol-campinas/2015/index.html">FLISOL</a> mas ela me disse que já tinha outros compromissos agendados.</p>
<h1 id="as-várias-faces-da-inovação-aberta">As várias faces da &quot;Inovação Aberta&quot;</h1>
<p>Meu objetivo durante minha fala era colocar algumas coisas para começar uma reflexão sobre &quot;inovação aberta&quot;. Tenho certeza que corri bastante com minha fala porque eu queria deixar bastante tempo para a conversa.</p>
<h1 id="conversa">Conversa</h1>
<p>Depois da minha fala, tivemos uma conversa super interessante sobre propriedade intelectual.<a href="#fn2" class="footnote-ref" id="fnref2"><sup>2</sup></a></p>
<p>A conversa foi enriquecida com falas do vários presentes, principalmente, (em ordem alfabética) Jair Chagas, Marlon Ribeiro, Paloma Oliveira e Paulo Schor.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>academia,ciência aberta,compartilhamento,liberdade</p>
</div>
<div class="tags">
<p>MedHacker,UNIFESP,São Paulo</p>
</div>
<div class="comments">

</div>
<section class="footnotes">
<hr />
<ol>
<li id="fn1"><p>A última que eu me lembro foi no <a href="http://softwarelivre.org/fisl14">FISl14</a> em 2013 proferida por <a href="http://www.techtudo.com.br/noticias/noticia/2013/07/amigo-pessoal-de-aaron-swartz-fala-sobre-sua-vida-e-legado-no-fisl.html">Seth Schoen sobre Aaron Swartz</a>. Você pode assisti-la em <a href="http://hemingway.softwarelivre.org/fisl14/high/40t/sala40t-high-201307061301.ogg" class="uri">http://hemingway.softwarelivre.org/fisl14/high/40t/sala40t-high-201307061301.ogg</a>.<a href="#fnref1" class="footnote-back">↩</a></p></li>
<li id="fn2"><p>A sugestão da Paloma de montar um círculo ajudou muito nesse ponto. =)<a href="#fnref2" class="footnote-back">↩</a></p></li>
</ol>
</section>
