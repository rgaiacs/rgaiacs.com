---
layout: post
title: "Recuperando Senha da Plataforma Lattes"
author: "Raniere Silva"
categories: apps
tags: ["Plataforma Lattes"]
image:
  feature: 2020-02-20-senha-lattes.jpg
  author:  Shane Avery
  licence: Unsplash License
  original_url: https://unsplash.com/photos/OHnvp41aDzE
---

A [Plataforma Lattes](http://lattes.cnpq.br/) é um sistema de currículos virtual
criado e mantido pelo Conselho Nacional de Desenvolvimento Científico e Tecnológico (CNPq).
Ela é usada amplamente no meio acadêmico brasileiro nos processos de seleção.
Eu tinha meu email acadêmico cadastrado na plataforma
e esse email foi descontinuado quando terminei a graduação.
A senha estava salva em uma máquina antiga
e a função de recuperacão de senha não funcionava
porque o email era enviado para um endereço que não existia.

Depois de procurar um pouco na internet,
encontrei no [Start-Up Brasil](https://www.startupbrasil.org.br/)
a seguinte orientação:

> Enviar um e-mail para suporte-senha@cnpq.br com as informações solicitadas abaixo para o envio de sua senha.
>
> – Nome completo:
> – CPF:
> – RG:
> – Data de nascimento:
> – Nome do pai e/ou da mãe:
> – E-mail para recebimento da senha:
>
> Fonte: https://www.startupbrasil.org.br/2014/01/26/perdi-minha-senha-do-cv-lattes-como-recupera-la-nacionais/

Reporto que o procedimento funciona.
Espero que isso seja útil para outras pessoas.