---
layout: post
title: "Mathml October Meeting"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<figure>
<img src="/images/mathml.jpg" class="align-center" style="width:60.0%" />
</figure>
<p>This is a report about the Mozilla MathML October Meeting (<a href="https://groups.google.com/d/msg/mozilla.dev.tech.mathml/h_D-RrQDoq4/_43Ou4B84oUJ">see the announce</a>). The topics of the meeting can be found <a href="https://etherpad.mozilla.org/mathml-2014-10">in this PAD</a> (<span data-role="download">local copy of the
PAD &lt;pad.txt&gt;</span>). This meeting happens all at <a href="http://appear.in/mozilla-mathml">appear.in</a> and because of that we don't have a log.</p>
<p>The next meeting will be in November 14th (note that November 14th is Friday). Some countries will move to winter time and others to summer time so we will change the time and announce it later on <a href="https://lists.mozilla.org/listinfo/dev-tech-mathml">mozilla.dev.tech.mathml</a>. Please add topics in the <a href="https://etherpad.mozilla.org/mathml-2014-11">PAD</a>.</p>
<div class="more">

</div>
<h1 id="appear.in">APPEAR.IN</h1>
<p>We still testing <a href="https://appear.in" class="uri">https://appear.in</a> for the meetings. Until know it works fine but we need to find a solution to accommodate the increase of participantes.</p>
<h1 id="mediawiki">MediaWiki</h1>
<p><a href="https://github.com/physikerwelt">Moritz Schubotz</a> and <a href="https://github.com/fred-wang">Frédéric Wang</a> still working for MediaWiki support MathML. They discuss some issues in the last few days at #mathml IRC channel and if you have interest you can <a href="http://logs.glob.uno/?c=mozilla%23mathml&amp;s=10+Oct+2014&amp;e=12+Oct+2014">read the log</a>.</p>
<h1 id="katex">KaTeX</h1>
<p><a href="https://github.com/Khan/KaTeX">KaTeX</a> is Khan Academy's &quot;fork&quot; of MathJax announced/released last month. You can read a little about it at one of <a href="https://groups.google.com/d/msg/mathjax-users/vStfa_uxbCk/qFwIHIcIlK0J">MathJax Users discussions</a>.</p>
<h1 id="texzilla">TeXZilla</h1>
<p>Frédéric release a <a href="https://github.com/fred-wang/TeXZilla/releases">new version of TeXZilla</a> that has some bug fixes.</p>
<h1 id="commonmark">CommonMark</h1>
<p><a href="http://commonmark.org/">CommonMark</a> is &quot;yet another&quot; Markdown specification. Markdown is widely used in websites and some implementations support LaTeX math subset, some using MathJax and others converting it to MathML.</p>
<p>Some people are talking about what will be the math support &quot;recommendation&quot; at <a href="http://talk.commonmark.org/t/mathematics-extension/457" class="uri">http://talk.commonmark.org/t/mathematics-extension/457</a>.</p>
<h1 id="gecko">Gecko</h1>
<p>Some bugs related with MathML support by Gecko was closed. You find link to the bugs <a href="https://etherpad.mozilla.org/mathml-2014-10">in the etherpad used for this meeting</a>.</p>
<h1 id="gecko---talos">Gecko - Talos</h1>
<p>At last meeting we mention one <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=1043358">regression bug that makes gecko spend more time to render MathML</a>. James Kitchener <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=1073796">send a patch</a> to Talos so that we can be notify if this regression happens again.</p>
<h1 id="web-engine-hackfest-2014">Web Engine Hackfest 2014</h1>
<p>The &quot;WebKitGTK+ Hackfest&quot; was rename to &quot;<a href="http://webengineshackfest.org/">Web Engines Hackfest</a>&quot; and &quot;will take place in A Coruña, Spain from Sunday, December 7th to Wednesday, December 10th.&quot;</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>MathML,Mozilla</p>
</div>
<div class="tags">
<p>MathML meetings</p>
</div>
<div class="comments">

</div>
