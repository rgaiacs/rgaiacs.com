---
layout: post
title: Chaves SSH
author: "Raniere Silva"
categories: ftb
tags: [ftb]
image:
  feature: ftb.jpg
---
Chave SSH é uma maneira eficiente de conectar-se, de maneira segura, com
computadores remotos por meio da internet para execução de várias
atividades. Muitos usuários utilizam chave SSH apenas para comunicar-se
com algum servidor de repositórios git/hg e gerenciar essas chaves é uma
coisa que muitas não mencionado.

Neste post vamos ver como criar chaves SSH e gerenciá-las.

Criação
=======

No diretório \~/.ssh, execute :

    $ ssh-keygen

Será pedido algumas informações sendo que o arquivo de saída pode ser
deixado em branco mas as senhas não. Uma vez que a chave tenha sido
criada, adicione-a ao gerenciador de chaves utilizando :

    $ ssh-add id_rsa

Gerenciamento
=============

Para o gerenciamento das chaves SSH utiliza-se o arquivo config que deve
ser algo como:

    Host host1.com
        User foo
        Hostname host1.com
        PreferredAuthentications publickey
        IdentityFile ~/.ssh/host1/id_rsa
    Host host2.com
        Hostname host2.com
        PreferredAuthentications publickey
        IdentityFile ~/.ssh/host2/id_rsa

Referências
===========
