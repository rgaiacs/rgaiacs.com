---
layout: post
title: "Reportando Problemas"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Ao utilizar qualquer tipo de programa o usuário irá encontrar algum tipo de problema. Se você ainda não encontrou nenhum problema com os programas que utiliza é porque você utilizou-o poucas vezes.</p>
<p>Umas das melhores coisas em utilizar programas de código livre/aberto (como os listados em <a href="https://prism-break.org/en/all/">Prism Break</a>) é a possibilidade do usuário reportar seus problemas diretamente aos desenvolvedores e que estes ficam muito felizes em tomar conhecimento dos problemas.</p>
<p>Neste post vou comentar como reportar os problemas de forma correta.</p>
<div class="more">

</div>
<h1 id="encontrei-um-problema">Encontrei um problema</h1>
<p>Se você encontrou um problema, não se desespere. Você pode estar com sorte e seu problema já ter sido reportado e corrigido.</p>
<p>O primeiro passo é descobrir onde os problemas são reportados e as opções são (a) lista de email e/ou (b) bug tracker. Normalmente na página oficial do programa que está utilizando é fácil encontrar o link para onde os bugs são reportados.</p>
<div class="note">
<div class="admonition-title">
<p>Note</p>
</div>
<p>O link para o bug tracker de alguns programas famosos são:</p>
<ul>
<li>Firefox: <a href="http://bugzilla.mozilla.org/" class="uri">http://bugzilla.mozilla.org/</a></li>
<li>LibreOffice: <a href="https://bugs.freedesktop.org/" class="uri">https://bugs.freedesktop.org/</a></li>
<li>OpenOffice: <a href="https://issues.apache.org/ooo/" class="uri">https://issues.apache.org/ooo/</a></li>
<li>Pidgin: <a href="https://developer.pidgin.im/wiki/BugTracking" class="uri">https://developer.pidgin.im/wiki/BugTracking</a></li>
<li>Inkscape: <a href="https://bugs.launchpad.net/inkscape" class="uri">https://bugs.launchpad.net/inkscape</a></li>
</ul>
</div>
<div class="note">
<div class="admonition-title">
<p>Note</p>
</div>
<p>Vários projetos estão sendo hospedados no GitHub e utilizam o issue track nativo. Na barra lateral á direita, procure por &quot;Issues&quot;.</p>
</div>
<h1 id="meu-problema-ainda-não-foi-reportado">Meu problema ainda não foi reportado</h1>
<p>Se seu problema ainda não foi reportado você encontra-se diante de um dilema: (a) utilizar uma solução paliativa ou (b) reportar o problema encontrado.</p>
<p>Se você optar em reportar o problema será necessário gastar um tempo para conseguir algumas informações para que o problema possa ser reproduzido (se o desenvolvedor não conseguir reproduzir seu problema ele não será corrigido).</p>
<div class="note">
<div class="admonition-title">
<p>Note</p>
</div>
<p>Considere atualizar o seu programa para a versão mais recente pois pode ser que seu problema tenha sido corrigido sem ninguém ter notado.</p>
</div>
<h1 id="reportando-o-problema">Reportando o problema</h1>
<div class="note">
<div class="admonition-title">
<p>Note</p>
</div>
<p>Embora seja desagradável ter que criar uma conta, você terá que fazer isso para pode registrar seu problema.</p>
</div>
<p>Ao reportar seu problema, informe:</p>
<ul>
<li>programa, se existe mais de um;</li>
<li>versão;</li>
<li>sistema operacional;</li>
<li>passos a serem reproduzidos;</li>
<li>resultado obtido ao seguir os passos;</li>
<li>resultado esperado.</li>
</ul>
<p>Um <em>template</em>, em inglês, encontra-se <span data-role="download">aqui &lt;bug-template.txt&gt;</span>.</p>
<h1 id="referências">Referências</h1>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>código</p>
</div>
<div class="tags">
<p>bug</p>
</div>
<div class="comments">

</div>
