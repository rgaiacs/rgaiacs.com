---
layout: post
title: "Software Carpentry Workshop at UFSC"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<figure>
<img src="/images/IMG_0006.jpg" class="align-center" style="width:80.0%" />
</figure>
<div class="admonition">
<p>Thanks</p>
<p>I would like to thanks to Diego Barneche for the invitation to be one of the instructors of the first Software Carpentry workshop at the Federal University of Santa Catarina.</p>
<p>Diego, I like very much met you in person and love the time in Florianópolis.</p>
</div>
<div class="admonition">
<p>Thanks</p>
<p>Also, I wouldd like to thanks Renato Morais Araujo and Juliano A. Bogoni, the local organizer, for the iniciative and hard work to make this workshop possible.</p>
</div>
<p>Last Thursday and Friday, Diego and I ran a <a href="ftware-carpentry.org/">Software Carpentry</a> <a href="http://dbarneche.github.io/2014-12-11-ufsc/">workshop</a> at the Federal University of Santa Catarina, based on Florianópolis or <a href="https://pt.wikipedia.org/wiki/Florian%C3%B3polis">Ilha da Magia</a>. This was a R based workshop that also cover Unix Shell and Git.</p>
<div class="more">

</div>
<h1 id="rt-day---r">1rt Day - R</h1>
<figure>
<img src="/images/IMG_0012.jpg" class="align-center" style="width:80.0%" />
</figure>
<p>The first day was dedicated to R. Diego taught the basic of R (variables, arithmetic operations) and creation of functions without problems. Some students already knew this basic of R stuffs so they probably got bored during the morning.</p>
<figure>
<img src="/images/IMG_0014.jpg" class="align-center" style="width:80.0%" />
</figure>
<p>After the lunch, I lead a &quot;conversation&quot; about how organize R projects and taught control flow (<code>if/else</code> and <code>for</code>). After a little break, Diego taught the <code>plyr</code> package that, probably, was something new for all the students.</p>
<h1 id="nd-day---r">2nd Day - R</h1>
<figure>
<img src="/images/IMG_0023.jpg" class="align-center" style="width:80.0%" />
</figure>
<p>The second day started with a lesson about test driver development in R using <code>testthat</code> package by Diego. This was new for <strong>all</strong> the students.</p>
<p>At Software Carpentry we had some <a href="http://software-carpentry.org/blog/2014/10/why-we-dont-teach-testing.html">discussion about teaching test</a> due point float computations. Diego only mention point float computation testing to avoid the issues raised at Software Carpentry discussion but the need to write test for functions was made very clear.</p>
<h1 id="nd-day---unix-shell">2nd Day - Unix Shell</h1>
<p>After the test driver development and a break, I taught the Unix Shell. I ran a little bit on this session to cover <a href="http://software-carpentry.org/v5/novice/ref/01-shell.html">all the program</a> with some exercises.</p>
<p>As always, wasn't clear for the students why use shell if they could use R but after the first exercise (<a href="file:///home/raniere/software-carpentry/2014-12-11-ufsc/_site/shell.zip">organizing one repository</a>) and examples of <code>find</code>, <code>sed</code> and <code>convert</code> (from <a href="http://imagemagick.org/">ImageMagick</a>) the students understood why, in some cases, is useful to use the shell.</p>
<h1 id="nd-day---git">2nd Day - Git</h1>
<p>After the lunch, I taught the Git lesson. This took more time than expected (but I had previously said to Diego that this would probably happen).</p>
<p>We cover all the program with some interruptions when I had to go help some student solve a problem since we didn't had helpers at this workshop.</p>
<figure>
<img src="/images/IMG_0024.jpg" class="align-center" style="width:80.0%" />
</figure>
<h1 id="nd-day---wrap">2nd Day - Wrap</h1>
<figure>
<img src="/images/IMG_0026.jpg" class="align-center" style="width:80.0%" />
</figure>
<p>After the Git session, Diego ran a session to wrap what the students saw in this two days. He started the session talking about Open Access and after that we showed knitr and RMarkdown.</p>
<h1 id="problems">Problems</h1>
<ul>
<li><p>In Windows machines you can only install packages using :</p>
<pre><code>install.packages(&quot;testthat&quot;)</code></pre>
<p>if you are running RStudio as admin.</p></li>
<li>For the R version (3.0.2) of one student the <code>testthat</code> package isn't available.</li>
<li>Two students needed to reboot Git Bash a few times due problems. This happens with Git 1.8.4-preview20130916 and Git 1.9.4-preview20140611.</li>
</ul>
<h1 id="feedbacks">Feedbacks</h1>
<div class="admonition">
<p>Note</p>
<p>If you speek Portuguese you can read the <span data-role="download">raw version of the
feedbacks &lt;feedback.md&gt;</span>.</p>
</div>
<p>Students like the workshop. As always they complained about much information for little time.</p>
<figure>
<img src="/images/IMG_0018.jpg" alt="Instructors are prepared and made the lessons available. Since the lessons are available on the internet, we can study later what we didn&#39;t follow during the workshop." class="align-center" style="width:80.0%" /><figcaption>Instructors are prepared and made the lessons available. Since the lessons are available on the internet, we can study later what we didn't follow during the workshop.</figcaption>
</figure>
<figure>
<img src="/images/IMG_0028.jpg" alt="Many thanks for the instructors to ran this workshop. I belive that you already know the big problem, much information for little time. This type of workshop is very important and I&#39;m sure that it will improve our science skills." class="align-center" style="width:80.0%" /><figcaption>Many thanks for the instructors to ran this workshop. I belive that you already know the big problem, much information for little time. This type of workshop is very important and I'm sure that it will improve our science skills.</figcaption>
</figure>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>academia,ciência aberta,educação,Software Carpentry</p>
</div>
<div class="tags">
<p>UFSC,Florianópolis</p>
</div>
<div class="comments">

</div>
