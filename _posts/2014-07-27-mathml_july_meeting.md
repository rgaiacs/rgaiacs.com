---
layout: post
title: "MathML July Meeting"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<div class="admonition">
<p>Note</p>
<p>Sorry for the delay in write this.</p>
</div>
<p>This is a report about the Mozilla MathML July IRC Meeting (see the announcement <a href="https://groups.google.com/d/msg/mozilla.dev.tech.mathml/Aux6QYLuSeQ/dDf1d7JOx6EJ">here</a>). The topics of the meeting can be found <a href="https://etherpad.mozilla.org/mathml-2014-07">in this PAD</a> (<span data-role="download">local copy of the
PAD &lt;pad.txt&gt;</span>) and the <a href="http://logs.glob.uno/?c=mozilla%23mathml&amp;s=17+Jul+2014&amp;e=17+Jul+2014">IRC log</a> (<span data-role="download">local copy of the IRC log &lt;irc.txt&gt;</span>) is also available.</p>
<p>In the last 4 weeks the MathML team closed 4 bugs, worked in one other. This are only the ones tracked by Bugzilla.</p>
<p>The next meeting will be in July 14th at 8pm UTC (<strong>note that it will be in an different time from the last meating</strong>, more information below). Please add topics in the <a href="https://etherpad.mozilla.org/mathml-2014-08">PAD</a>.</p>
<div class="more">

</div>
<p>This was the fastest meeting that we had and with the smallest number of attendant. I want to believe that this happens because most of ours contributors participated at MathUI and was getting their life back to track.</p>
<h1 id="mathui-2014">MathUI 2014</h1>
<p><a href="http://cermat.org/events/MathUI/14/">MathUI</a> was amazing. You can find some notes about it <span data-role="doc">at this other post &lt;../19/cicm&gt;</span>.</p>
<h1 id="next-meeting">Next Meeting</h1>
<p>I was talking with Frédéric and we want to try doing our meeting with video, in particular using WebRTC technology. Because of it we will do next meeting at 8pm UTC so people in Europe can be at home during the meeting (we know that this isn't very nice to our global community of contributors but we still trying to find one solution to accommodate all time zones, if we need one suggestion we will love to hear).</p>
<p>Some of the options for WebRTC video conferences are <a href="https://appear.in">appear.in</a> and <a href="https://talky.io">talky.io</a>. We are going to use appear.in and to participate in our next meeting (August 14th at <strong>8pm UTC</strong>) you just need to:</p>
<ol type="1">
<li>Open <a href="http://appear.in/mozilla-mathml" class="uri">http://appear.in/mozilla-mathml</a>.</li>
<li>Accept the request to your web browser access your camera and microphone.</li>
<li>Enjoy our meeting.</li>
</ol>
<p>WebRTC has one &quot;small issue&quot; right now, as present at <a href="https://appear.in/information/faq">appear.in FAQ</a>:</p>
<blockquote>
<p>It is currently not possible to be more than 8 participants in the room, as this will cause the application to crash because of the number of connections being handled by the browser. The main limiting factor is the bandwidth of the participants, as everything is streamed peer to peer. If you experience poor quality when talking to many people, you can try disabling your camera.</p>
</blockquote>
<p>I hope that every one interested in our meeting could find one seat. We will also be at our IRC channel and use it as fall back.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>MathML,Mozilla</p>
</div>
<div class="tags">
<p>MathML meetings</p>
</div>
<div class="comments">

</div>
