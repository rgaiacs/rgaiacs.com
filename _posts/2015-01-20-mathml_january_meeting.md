---
layout: post
title: "Mathml January Meeting"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>This is a report about the Mozilla MathML January IRC Meeting (see the announcement <a href="https://groups.google.com/d/msg/mozilla.dev.tech.mathml/4KR74fhXAUY/RNZqR91fx0YJ">here</a>). The topics of the meeting can be found <a href="https://etherpad.mozilla.org/mathml-2015-01">in this PAD</a> (<span data-role="download">local copy of the
PAD &lt;pad.txt&gt;</span>) and the <a href="http://logs.glob.uno/?c=mozilla%23mathml&amp;s=14+Jan+2015&amp;e=14+Jan+2015">IRC log</a> (<span data-role="download">local copy of the IRC log &lt;irc.txt&gt;</span>) is also available.</p>
<p>The next meeting will be in March 11th at 8pm UTC (<a href="http://www.timeanddate.com/worldclock/meetingdetails.html?year=2015&amp;month=3&amp;day=11&amp;hour=20&amp;min=0&amp;sec=0&amp;p1=240&amp;p2=137&amp;p3=179&amp;p4=233&amp;p5=195&amp;p6=37">check the time at your location here</a>). Please add topics in the <a href="https://etherpad.mozilla.org/mathml-2015-03">PAD</a>.</p>
<div class="admonition">
<p>Note</p>
<p>Our February meeting was cancelled. =(</p>
</div>
<div class="more">

</div>
<h1 id="mathml-and-firefox-os">MathML and Firefox OS</h1>
<p><a href="https://developer.mozilla.org/en-US/Firefox_OS">Firefox OS</a>, the mobile operating system developed by Mozilla that is based on Linux and Firefox's powerful Gecko rendering engine, has native MathML support but the rendering isn't very good due the lack of properly font.</p>
<p>Last year, when working on my Google Summer of Code project, I compiled my own patched version of Firefox OS to have good MathML support. The patched version include Latin Modern Math font from <a href="https://github.com/fred-wang/moztt">this git repository</a>.</p>
<p>I promised to Bill that I will wrote a blog post with my steps to compile my patched version of Firefox OS to my <a href="https://developer.mozilla.org/en-US/Firefox_OS/Developer_phone_guide/Phone_specs">Alcatel One Touch Fire</a> and the <a href="https://wiki.mozilla.org/FirefoxOS/TCP">tablet prototype</a> (that I stop compile due <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=1087096" class="uri">https://bugzilla.mozilla.org/show_bug.cgi?id=1087096</a>).</p>
<h1 id="mathml-on-epubs">MathML on EPUBs</h1>
<p><a href="http://www.idpf.org/epub/30/spec/epub30-contentdocs.html#sec-xhtml-mathml">EPUB3 specification</a> says that we can use embedded <strong>Presentation</strong> MathML in EPUBs. At this meeting we have some discussion about this and EPUB readers support.</p>
<h1 id="vote-for-mathml-support-in-internet-explorer">Vote for MathML Support in Internet Explorer</h1>
<p>Internet Explorer (or IE) isn't the most used web browser but if we want that people use MathML is important that every web browser support it. So here is a step-by-step to vote for MathML support in IE.</p>
<ol type="1">
<li>Go to <a href="https://wpdev.uservoice.com/forums/257854-internet-explorer-platform/suggestions/6508572-mathml" class="uri">https://wpdev.uservoice.com/forums/257854-internet-explorer-platform/suggestions/6508572-mathml</a>.</li>
</ol>
<figure>
<img src="/images/vote-mathml-in-ie0.jpg" class="align-center" style="width:80.0%" />
</figure>
<ol start="2" type="1">
<li>Select &quot;Vote&quot; at the left column.</li>
</ol>
<figure>
<img src="/images/vote-mathml-in-ie1.jpg" class="align-center" style="width:80.0%" />
</figure>
<ol start="3" type="1">
<li>Fill your email address and later your name.</li>
</ol>
<figure>
<img src="/images/vote-mathml-in-ie2.jpg" class="align-center" style="width:80.0%" />
</figure>
<ol start="4" type="1">
<li>Select how many votes you want to assign (<strong>probably 3</strong>).</li>
<li>Confirme your vote following the instructions send to your email inbox.</li>
</ol>
<h1 id="upcoming-events">Upcoming Events</h1>
<table>
<thead>
<tr class="header">
<th>Date</th>
<th>Name</th>
<th>Web Site</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>2015/02/09-20</td>
<td>Google Summer of Code 2015: Organization applications</td>
<td><a href="https://www.google-melange.com/gsoc/homepage/google/gsoc2015" class="uri">https://www.google-melange.com/gsoc/homepage/google/gsoc2015</a></td>
</tr>
<tr class="even">
<td>2015/07/13-17</td>
<td>Conference on Intelligent Computer Mathematics (CICM)</td>
<td><a href="http://www.cicm-conference.org/2015/" class="uri">http://www.cicm-conference.org/2015/</a></td>
</tr>
</tbody>
</table>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>MathML,Mozilla</p>
</div>
<div class="tags">
<p>MathML meetings</p>
</div>
<div class="comments">

</div>
