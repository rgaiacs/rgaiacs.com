---
layout: post
title: "Testando código"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Algumas semanas atrás <a href="http://drclimate.wordpress.com/who-is-dr-climate/">Damien Irving</a> escreveu <a href="http://drclimate.wordpress.com/">esse ótimo post sobre como testar códigos</a> (<span data-role="download">cópia
nesse servidor &lt;testing-your-code.html&gt;</span>) no qual consegui resumir de forma bastante clara o tema.</p>
<div class="more">

</div>
<p>Damien fala sobre a importância de utilizar testes ao programar (mas vou deixar isso para um próximo post) e sobre os tipos de testes:</p>
<dl>
<dt>teste unitário</dt>
<dd><p>são testes para unidades do código, normalmente funções e métodos (é o mais fácil de ser preparado)</p>
</dd>
<dt>teste de integração</dt>
<dd><p>são testes para verificar se a cadeia de funções não vai quebrar (não é muito fácil de ser preparado)</p>
</dd>
</dl>
<p>Ele também falou sobre a técnica de programação dirigida por teste na qual primeiro escreve-se os testes e depois o programa.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>academia,ciência aberta,Software Carpentry</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
