---
layout: post
title: "Mathml April Meeting"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>This is a report about the Mozilla April IRC Meeting (see the announcement <a href="https://groups.google.com/d/msg/mozilla.dev.tech.mathml/snioQWxV31g/zPR9d8GVxnEJ">here</a>). The topics of the meeting can be found <a href="https://etherpad.mozilla.org/mathml-2015-04">in this PAD</a> (<span data-role="download">local copy of the PAD &lt;pad.txt&gt;</span>) and the <a href="http://logs.glob.uno/?c=mozilla%23mathml&amp;s=8+Apr+2015&amp;e=8+Apr+2015">IRC log</a> (<span data-role="download">local copy of the IRC log &lt;irc.txt&gt;</span>) is also available.</p>
<p>The next meeting will be in May 13th at 8pm UTC (<a href="http://www.timeanddate.com/worldclock/meetingdetails.html?year=2015&amp;month=5&amp;day=13&amp;hour=20&amp;min=0&amp;sec=0&amp;p1=240&amp;p2=137&amp;p3=179&amp;p4=233&amp;p5=195&amp;p6=37">check the time at your location here</a>). Please add topics in the <a href="https://etherpad.mozilla.org/mathml-2015-05">PAD</a>.</p>
<div class="more">

</div>
<h1 id="firefox-add-ons">Firefox Add-ons</h1>
<p>In case you didn't know, Firefox has a long list of <a href="https://addons.mozilla.org/en-US/firefox/">add-ons</a> and a <a href="https://addons.mozilla.org/en-US/firefox/search/?q=mathml">few related to MathML</a>.</p>
<p>Frédéric updated some of his add-ons, e.g. <a href="https://addons.mozilla.org/en-US/firefox/addon/mathml-copy/">MathML Copy</a> now supports arbitrary annotations.</p>
<p>If you want to create a new add-on you can starting looking at <a href="https://developer.mozilla.org/en-US/Add-ons">MDN</a>.</p>
<h1 id="latex-to-mathml">LaTeX to MathML</h1>
<p>Write MathML by hand is painful and because of that normally people get it from a WYSIWYG editor or by translating from LaTeX/<a href="http://asciimath.org/">AsciiMath</a>.</p>
<p>During the meeting we had a nice discussion about different tools create different MathML for the same equation (for a example check <a href="http://stackoverflow.com/questions/29541445/does-one-rendering-have-multiple-mathml-expressions/29544682#29544682">this question on <a href="http://stackoverflow.com" class="uri">http://stackoverflow.com</a></a>).</p>
<p>We also have Rúnar Berg Baugsson Sigríðarson talking about two of this projects: <a href="https://github.com/runarberg/ascii2mathml">Ascii2MathML</a> and <a href="https://github.com/runarberg/markdown-it-math">Markdown-it-math</a>.</p>
<h1 id="gecko">Gecko</h1>
<p>Related to Gecko, I'm trying to fix the <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=1011020">bug related with invisible bars</a> and Frédéric Wang is investigating the use of <a href="https://www.gnu.org/software/freefont/">GNU FreeSerif font</a> that now support OpenType Math.</p>
<h1 id="others">Others</h1>
<ul>
<li>If you didn't read <a href="http://www.mathjax.org/making-math-on-wikipedia-more-awesome-an-interview-with-moritz-schubotz/">Moritz interview</a> you will want to do it.</li>
<li><a href="http://hypothes.is/">Hypothes.is</a> has a call from grant related with <a href="http://anno.fund/">open annotation</a>.</li>
</ul>
<h1 id="upcoming-events">Upcoming Events</h1>
<table>
<thead>
<tr class="header">
<th>Date</th>
<th>Name</th>
<th>Web Site</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>2015/07/13-17</td>
<td>Conference on Intelligent Computer Mathematics (CICM)</td>
<td><a href="http://www.cicm-conference.org/2015/" class="uri">http://www.cicm-conference.org/2015/</a></td>
</tr>
<tr class="even">
<td>2015/12/07-09</td>
<td>Web Engines Hackfest 2015</td>
<td><a href="http://www.webengineshackfest.org/" class="uri">http://www.webengineshackfest.org/</a></td>
</tr>
</tbody>
</table>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>MathML,Mozilla</p>
</div>
<div class="tags">
<p>MathML meetings</p>
</div>
<div class="comments">

</div>
