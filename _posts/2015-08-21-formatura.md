---
layout: post
title: "Formatura"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Em <span data-role="doc">um post anterior &lt;../../07/17/graduation&gt;</span> eu mencionei que finalmente estaria me formando. Na última sexta-feira ocorreu minha formatura.</p>
<figure>
<img src="/images/CANON034.JPG" alt="Eu ao lado de meus pais." class="align-center" style="width:80.0%" /><figcaption>Eu ao lado de meus pais.</figcaption>
</figure>
<figure>
<img src="/images/CANON021.JPG" alt="Eu ao lado do professor homenageado pela turma ao receber o diploma." class="align-center" style="width:80.0%" /><figcaption>Eu ao lado do professor homenageado pela turma ao receber o diploma.</figcaption>
</figure>
<p>Agradeço a todos que desejaram-me parabéns e sucesso na próxima jornada.</p>
<div class="more">

</div>
<h1 id="amigos">Amigos</h1>
<p>Uma jornada é muito triste quando feita sozinha. Agradecimentos especiais a todos aqueles que estiveram próximo durante a graduação e fizeram essa minha jornada menos solitária. Para aqueles que puderam comparecer na formatura, foi uma honra contar com sua presença.</p>
<figure>
<img src="/images/SAM_0044.JPG" alt="Da direita para a esquerda, minha prima Dora, eu, meu pai e minha prima Rosa." class="align-center" style="width:80.0%" /><figcaption>Da direita para a esquerda, minha prima Dora, eu, meu pai e minha prima Rosa.</figcaption>
</figure>
<figure>
<img src="/images/SAM_0065.JPG" alt="Da direita para a esquerda, Mônica, eu e Fernando." class="align-center" style="width:80.0%" /><figcaption>Da direita para a esquerda, Mônica, eu e Fernando.</figcaption>
</figure>
<p>Foi uma grande surpresa contar com a presença da Mônica e do Fernando.</p>
<figure>
<img src="/images/SAM_0060.JPG" alt="Da direita para a esquerda, Fernando, Eric, eu, Pedro e Ivan." class="align-center" style="width:80.0%" /><figcaption>Da direita para a esquerda, Fernando, Eric, eu, Pedro e Ivan.</figcaption>
</figure>
<p>Não consigo lembrar porque o Gabriel não apareceu nessa foto.</p>
<figure>
<img src="/images/CANON057.JPG" alt="Da direita para a esquerda, Vitor, eu e Paula." class="align-center" style="width:80.0%" /><figcaption>Da direita para a esquerda, Vitor, eu e Paula.</figcaption>
</figure>
<figure>
<img src="/images/SAM_0041.JPG" alt="Minha amiga Erika." class="align-center" style="width:80.0%" /><figcaption>Minha amiga Erika.</figcaption>
</figure>
<p>A foto anterior é uma das minhas favoritas mas o fundo está horrível!!!</p>
<p>E ao selecionar as fotos para este post eu descobri que não tenho nenhuma foto com o meu irmão. (T_T)</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>academia,educação,pessoal</p>
</div>
<div class="tags">
<p>UNICAMP,formatura</p>
</div>
<div class="comments">

</div>
