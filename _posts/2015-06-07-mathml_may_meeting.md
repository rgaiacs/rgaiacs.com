---
layout: post
title: "Mathml May Meeting"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<div class="admonition">
<p>Note</p>
<p>Sorry for the delay in write this post.</p>
</div>
<p>This is a report about the Mozilla May IRC Meeting (see the announcement <a href="https://groups.google.com/d/msg/mozilla.dev.tech.mathml/wJsbRftFmc0/jHY85YZu1KMJ">here</a>). The topics of the meeting can be found <a href="https://etherpad.mozilla.org/mathml-2015-05">in this PAD</a> (<span data-role="download">local copy of the PAD &lt;pad.txt&gt;</span>) and the <a href="http://logs.glob.uno/?c=mozilla%23mathml&amp;s=13+May+2015&amp;e=13+May+2015">IRC log</a> (<span data-role="download">local copy of the IRC log &lt;irc.txt&gt;</span>) is also available.</p>
<p>The next meeting will be in June 10th at 8pm UTC (<a href="http://www.timeanddate.com/worldclock/meetingdetails.html?year=2015&amp;month=6&amp;day=10&amp;hour=20&amp;min=0&amp;sec=0&amp;p1=240&amp;p2=137&amp;p3=179&amp;p4=233&amp;p5=195&amp;p6=37">check the time at your location here</a>). Please add topics in the <a href="https://etherpad.mozilla.org/mathml-2015-06">PAD</a>.</p>
<div class="more">

</div>
<h1 id="fonts">Fonts</h1>
<p>Frédéric talked about the challenge to have fonts that supports math on users devices.</p>
<h1 id="accessibility">Accessibility</h1>
<p>Frédéric also talked about the progress of MathML accessibility.</p>
<h1 id="upcoming-events">Upcoming Events</h1>
<table>
<thead>
<tr class="header">
<th>Date</th>
<th>Name</th>
<th>Web Site</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>2015/07/13-17</td>
<td>Conference on Intelligent Computer Mathematics (CICM)</td>
<td><a href="http://www.cicm-conference.org/2015/" class="uri">http://www.cicm-conference.org/2015/</a></td>
</tr>
<tr class="even">
<td>2015/12/07-09</td>
<td>Web Engines Hackfest 2015</td>
<td><a href="http://www.webengineshackfest.org/" class="uri">http://www.webengineshackfest.org/</a></td>
</tr>
</tbody>
</table>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>MathML,Mozilla</p>
</div>
<div class="tags">
<p>MathML meetings</p>
</div>
<div class="comments">

</div>
