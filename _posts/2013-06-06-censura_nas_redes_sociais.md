---
layout: post
title: "Censura nas redes sociais"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Apareceu um artigo muito bom na <a href="http://www.revista.espiritolivre.org">&quot;Revista Espírito Livre&quot;</a> sobre a questão da liberdade de expressão e as redes sociaisi<a href="#fn1" class="footnote-ref" id="fnref1"><sup>1</sup></a> que foi originalmente publicado no <a href="http://www.observatoriodaimprensa.com.br">Observatório da Imprensa</a><a href="#fn2" class="footnote-ref" id="fnref2"><sup>2</sup></a> e corresponde a tradução de Jô Amado (com edição de Letícia Nunes) do artigo &quot;Social networks face free-speech tests&quot; de April Dembosky (com colaboração de Robert Cookson) publicado no Financial Times<a href="#fn3" class="footnote-ref" id="fnref3"><sup>3</sup></a>.</p>
<div class="more">

</div>
<p>Como informado no texto:</p>
<blockquote>
<p>Facilitar a livre expressão ao mesmo tempo em que se controla o conteúdo ofensivo tornou-se um teste controvertido e um desafio tecnológico para redes sociais (...)</p>
</blockquote>
<p>Em um post anterior, <span data-role="doc">../../05/18/liberdade_de_expressao_e_direito_autoral</span>, disse que &quot;uma vez considerado o respeito mútuo entre os indivíduos, deveríamos pensar sobre nossa (...) liberdade [de expressão]&quot;. No que se refere as redes sociais, o desafio é separar o conteúdo que viola o respeito a algum indivíduo daquele que não o faz porque muitas vezes essa separação não é muito nítida.</p>
<p>Devido a essa dificuldade de separar o joio do trigo, a maioria das redes sociais atuais deixam/removem conteúdo que não deveriam. Na minha opinião, o uso de redes sociais distribuídas e federadas como <a href="http://status.net/">status.net</a>, <a href="http://pump.io/">pump.io</a>, ... resolve o problema, pelo menos na questão da liberdade de expressão.</p>
<p><strong>Referências</strong></p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>none</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
<section class="footnotes">
<hr />
<ol>
<li id="fn1"><p><a href="http://www.revista.espiritolivre.org/redes-sociais-enfrentam-teste-de-liberdade-de-expressao" class="uri">http://www.revista.espiritolivre.org/redes-sociais-enfrentam-teste-de-liberdade-de-expressao</a><a href="#fnref1" class="footnote-back">↩</a></p></li>
<li id="fn2"><p><a href="http://www.observatoriodaimprensa.com.br/news/view/_ed749_redes_sociais_enfrentam_teste_de_liberdade_de_expressao" class="uri">http://www.observatoriodaimprensa.com.br/news/view/_ed749_redes_sociais_enfrentam_teste_de_liberdade_de_expressao</a> (<span data-role="download">cópia local &lt;oi-rsetle.html&gt;</span>)<a href="#fnref2" class="footnote-back">↩</a></p></li>
<li id="fn3"><p><a href="http://www.ft.com/cms/s/0/9346aedc-c843-11e2-8cb7-00144feab7de.html#axzz2VP2B2vAea" class="uri">http://www.ft.com/cms/s/0/9346aedc-c843-11e2-8cb7-00144feab7de.html#axzz2VP2B2vAea</a> (<span data-role="download">cópia local &lt;ft-snffst.html&gt;</span>)<a href="#fnref3" class="footnote-back">↩</a></p></li>
</ol>
</section>
