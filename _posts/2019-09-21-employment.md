---
layout: post
title: "Employment"
author: "Raniere Silva"
categories: travelling
tags: ["Africa", "Botswana", "Gaborone"]
image:
  feature: 2019-09-21-employment.jpg
  author:  Raniere Silva
  licence: CC-BY
---

One of the thing that surprised me when visiting Gabore, Botswana
is that, during rush hour,
you will see traffic officers in main road junction
despite the traffic lights.
This reminded the conversation that I had with my dear friend Anelda van der Walt
about employment.

What should governments and societies do to employ people when they face a new wave of automation coming?
Should they be very protective of people and create laws to keep people doing their jobs?
Should they create programs to enable people to transition from their current job to new ones?
Or should they adopt [universal basic income](https://en.wikipedia.org/wiki/Basic_income)?

I'm bias toward the universal basic income or reduction of the work hours.