---
layout: post
title: "Screenshot Gallery of Tablet running Firefox OS"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>This is a collection of 20 screenshots of Firefox OS running on a tablet. I hope that you enjoy it.</p>
<div class="note">
<div class="admonition-title">
<p>Note</p>
</div>
<p>The screenshots was take with:</p>
<ul>
<li><strong>OS version</strong>: 2.0.0.0-prerelease</li>
<li><strong>Firmware revision</strong>: flatfish_20140519-1653</li>
<li><strong>Hardware revision</strong>: flatfish</li>
<li><strong>Platform revision</strong>: 32.0a1</li>
<li><strong>Build Identifier</strong>: 20140519171236</li>
<li><strong>Git commit info</strong>: 2014-05-09 18:18:19 7fe1f921</li>
</ul>
</div>
<div class="more">

</div>
<figure>
<img src="/images/00-lockscreen.png" alt="Lock screen." class="align-center" style="width:50.0%" /><figcaption>Lock screen.</figcaption>
</figure>
<figure>
<img src="/images/01-home0.png" alt="First home screen." class="align-center" style="width:50.0%" /><figcaption>First home screen.</figcaption>
</figure>
<figure>
<img src="/images/02-home1.png" alt="Second home screen." class="align-center" style="width:50.0%" /><figcaption>Second home screen.</figcaption>
</figure>
<figure>
<img src="/images/03-notification.png" alt="Notification." class="align-center" style="width:50.0%" /><figcaption>Notification.</figcaption>
</figure>
<figure>
<img src="/images/04-settings0.png" alt="Settings app at Wifi." class="align-center" style="width:50.0%" /><figcaption>Settings app at Wifi.</figcaption>
</figure>
<figure>
<img src="/images/05-settings1.png" alt="Settings app at Device Informations." class="align-center" style="width:50.0%" /><figcaption>Settings app at Device Informations.</figcaption>
</figure>
<figure>
<img src="/images/06-camera0.png" alt="Camera app." class="align-center" style="width:50.0%" /><figcaption>Camera app.</figcaption>
</figure>
<figure>
<img src="/images/07-camera1.png" alt="Camera app using frontal camera." class="align-center" style="width:50.0%" /><figcaption>Camera app using frontal camera.</figcaption>
</figure>
<figure>
<img src="/images/08-clock0.png" alt="Clock app." class="align-center" style="width:50.0%" /><figcaption>Clock app.</figcaption>
</figure>
<figure>
<img src="/images/09-clock1.png" alt="Timer app." class="align-center" style="width:50.0%" /><figcaption>Timer app.</figcaption>
</figure>
<figure>
<img src="/images/10-clock2.png" alt="Stopwatch app." class="align-center" style="width:50.0%" /><figcaption>Stopwatch app.</figcaption>
</figure>
<figure>
<img src="/images/11-webbrowser0.png" alt="Web browser." class="align-center" style="width:50.0%" /><figcaption>Web browser.</figcaption>
</figure>
<figure>
<img src="/images/12-webbrowser1.png" alt="Web browser and keyboard." class="align-center" style="width:50.0%" /><figcaption>Web browser and keyboard.</figcaption>
</figure>
<figure>
<img src="/images/13-webbrowser2.png" alt="Web browser at mozilla.org." class="align-center" style="width:50.0%" /><figcaption>Web browser at mozilla.org.</figcaption>
</figure>
<figure>
<img src="/images/14-contacts0.png" alt="Contacts app." class="align-center" style="width:50.0%" /><figcaption>Contacts app.</figcaption>
</figure>
<figure>
<img src="/images/15-contacts1.png" alt="Adding new contact at Contacts app." class="align-center" style="width:50.0%" /><figcaption>Adding new contact at Contacts app.</figcaption>
</figure>
<figure>
<img src="/images/16-calendar0.png" alt="Calendar app." class="align-center" style="width:50.0%" /><figcaption>Calendar app.</figcaption>
</figure>
<figure>
<img src="/images/17-calendar1.png" alt="Adding new event at Calendar app." class="align-center" style="width:50.0%" /><figcaption>Adding new event at Calendar app.</figcaption>
</figure>
<figure>
<img src="/images/18-email0.png" alt="Email app and keyboard." class="align-center" style="width:50.0%" /><figcaption>Email app and keyboard.</figcaption>
</figure>
<figure>
<img src="/images/19-email1.png" alt="Inbox of Email app." class="align-center" style="width:50.0%" /><figcaption>Inbox of Email app.</figcaption>
</figure>
<figure>
<img src="/images/20-listofapps.png" alt="List of apps running." class="align-center" style="width:50.0%" /><figcaption>List of apps running.</figcaption>
</figure>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>Firefox OS,TCP</p>
</div>
<div class="tags">
<p>Firefox OS,TCP</p>
</div>
<div class="comments">

</div>
