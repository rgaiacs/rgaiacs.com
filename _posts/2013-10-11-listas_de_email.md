---
layout: post
title: Listas de email
author: "Raniere Silva"
categories: ftb
tags: [ftb]
image:
  feature: ftb.jpg
---
O email é uma das formas eletrônicas mais antigas de comunicação e que
ainda continua sendo utilizanda hoje em dia. Um "tipo especial" de
endereço de email são os de listas que de forma resumida funciona como
um apelido para outros endereços de email com interesses comuns.

Alguns dos softwares LIVRES para gerenciamento de listas de email são,
em ordem alfabética:

-   [Dada Mail](http://en.wikipedia.org/wiki/Dada_Mail),
-   [GNU Mailman](http://en.wikipedia.org/wiki/GNU_Mailman),
-   [GroupServer](http://en.wikipedia.org/wiki/GroupServer),
-   [Smartlist](http://en.wikipedia.org/wiki/SmartList),
-   [Sympa](http://en.wikipedia.org/wiki/Sympa).

E alguns serviços GRATUITOS são, em ordem alfabética:

-   Google Groups,
-   MSN Groups,
-   Yahoo! Groups.

A grande maioria das listas de email oferecem uma interface web para
gerenciamento da mesma mas algumas vezes é necessário fazê-lo utilizando
emails e é isso que será abordado a seguir.

GNU Mailman
===========

O GNU Mailman é um servidor de lista de email muito popular e a
interface de email é descrita [neste trecho do
manual](http://www.list.org/mailman-member/node10.html). Considerando o
email fictício `minhalista@exemplo.br` de uma lista teremos:

`minhalista@exemplo.br`

:   o endereço a ser utilizado para enviar mensagens.

`minhalista-join@exemplo.br`

:   o endereço a ser utilizado para requerer inscrição. Também pode ser
    utilizado `minhalista-subscribe@exemplo.br`.

`minhalista-leave@exemplo.br`

:   o endereço a ser utilizado para cancelar a inscrição. Também pode
    ser utilizado `minhalista-unsubscribe@example.br`.

`minhalista-owner@exemplo.br`

:   encaminha os emails para o dono da lista.

`minhalista-request@exemplo.br`

:   os emails recebidos são processados por um robô que executa as ações
    indicadas no email. Os comandos reconhecidos encontram-se aqui
    &lt;http://www.list.org/mailman-member/node41.html\#a:commands&gt;)\_.

    Uma forma de obter informações sobre a lista é enviar um email para
    esse endereço com o título "help".

Sympa
=====

O Sympa é outro servidor de lista de email. O funcionamento é muito
parecido com o GNU Mailman.

Google Groups
=============

O Google Groups é um serviço GRATUITO do Google que gerencia uma lista
de email. Nos últimos anos ficou bastante famoso, dentre outros motivos:

-   levar o nome do Google,
-   ser GRATUITO,
-   possuir uma interface fácil/amigável,
-   ferramenta de busca eficiente (o que não está presente em outros
    serviços ou softwares),
-   grande espaço para armazenamento.

No caso de possuir uma conta em algum serviço do Google a melhor forma
de utilizar o Google Groups é pela interface web. Caso esse não seja o
caso a única forma é por meio da muito limitada interface de email.
Considerando o email fictício `minhalista@googlegroups.com` de uma lista
teremos:

`minhalista@googlegroups.com`

:   o endereço a ser utilizado para enviar mensagens.

`minhalista+subscribe@googlegroups.com`

:   o endereço a ser utilizado para requerer inscrição.

`minhalista+unsubscribe@googlegroups.com`

:   o endereço a ser utilizado para cancelar a inscrição.

`minhalista+owner@googlegroups.com`

:   encaminha os emails para o dono da lista.

`minhalista+help@googlegroups.com`

:   requisita um email com informações de ajuda.

Outras ações só são possíveis pela interface web.

**Referências**
