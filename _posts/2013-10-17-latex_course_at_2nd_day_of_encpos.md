---
layout: post
title: "LaTeX Course at 2nd Day of EnCPos"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Today was the second day of my course about LaTeX that I talk some days ago (<span data-role="doc">see the post here &lt;../14/latex_course_at_ecnpos&gt;</span>.</p>
<div class="more">

</div>
<p>I try to follow the plan with the pads (<span data-role="download">one for the contents
&lt;encpos-latex1.txt&gt;</span> and <span data-role="download">other for feedbacks
&lt;encpos-feedback-latex1.txt&gt;</span>) and the rounds:</p>
<dl>
<dt>Round 2.1</dt>
<dd><p>Information about encoding, inputenc package and fontenc package.</p>
</dd>
<dt>Round 2.2</dt>
<dd><p>Information about internationalization, babel package and indentfirst package.</p>
</dd>
<dt>Round 2.3</dt>
<dd><p>Information about table of contents, hyperlinks and hyperref package.</p>
</dd>
<dt>Round 2.4</dt>
<dd><p>Information about bitmap and graphicx package.</p>
</dd>
<dt>Round 2.5</dt>
<dd><p>Time to write a feedback of this class and answer to questions.</p>
</dd>
</dl>
<h1 id="problems-and-solutions">Problems and solutions</h1>
<p>This class we start with <span data-role="download">this document &lt;encpos2start.tex&gt;</span> that have only the documentclass declaration in the preamble so that the students can see the problem and fix it with the inclusion of some package.</p>
<h1 id="the-video-projector">The video projector</h1>
<p>This time the video projector turn off in the first 20 minutes of the class and won't turn on. It have to be replaced by another.</p>
<p>Turing the change of projector that took around 20 minutes I continue the class using only the pad. This was the time when I request progress feedbacks in the pad and look like that the students haven't any big problem following the class.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>educação,Software Carpentry</p>
</div>
<div class="tags">
<p>LaTeX,EnCPos</p>
</div>
<div class="comments">

</div>
