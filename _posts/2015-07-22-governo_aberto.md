---
layout: post
title: "Agente de Governo Aberto"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>A cidade de São Paulo está com <a href="http://saopauloaberta.prefeitura.sp.gov.br/index.php/noticia/tudosobreoedital/">um editar</a> aberto com o título de &quot;Agente de Governo Aberto&quot;. Se você está envolvido com</p>
<ul>
<li>software livre,</li>
<li>dados abertos ou</li>
<li>cultura livre</li>
</ul>
<p>você deve enviar uma proposta para esse editar. <strong>A data limite para envio de propostas é 07 de agosto de 2015.</strong></p>
<div class="more">

</div>
<p>Como o tema é governo aberto acho nada mais justo que qualquer pessoa tenha acesso às propostas enviadas. Por esse motivo, estou disponibilizando as propostas que enviei:</p>
<ul>
<li><span data-role="download">Tecnologia Aberta e Colaborativa &lt;paga-software.odt&gt;</span>,</li>
<li><span data-role="download">Transparência e Dados Abertos &lt;paga-data.odt&gt;</span>.</li>
</ul>
<p>As propostas encontram-se no formato <a href="https://pt.wikipedia.org/wiki/OpenDocument">ODT</a> que o formato requisitado. <strong>Meus parabéns para a organização do edital por terem utilizado o ODT.</strong></p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>ciência aberta,educação,internet,liberdade,LPBRSP,software livre,Software Carpentry,Webmaker</p>
</div>
<div class="tags">
<p>Webmaker Brasil,São Paulo,Agente de Governo Aberto</p>
</div>
<div class="comments">

</div>
