---
layout: post
title: Sync Address Book
author: "Raniere Silva"
categories: ftb
tags: [ftb]
image:
  feature: ftb.jpg
---
In a previous post &lt;../../../2015/10/24/calypso&gt; I wrote about
[Calypso](http://keithp.com/blogs/calypso/), a CalDAV/CardDAV/WebDAV
server. At this post you find some tips to use
[DAVdroid](https://davdroid.bitfire.at/) with Calypso.

<div class="admonition">

Note

To follow the tips you need Calypso running and DAVdroid installed on
your smartphone. To install Calypso, follow [this
steps](http://keithp.com/cgi-bin/gitweb.cgi?p=calypso.git;a=blob;f=README;h=cce7666522dbcf04cff02f1c9aa9f665437fafe1;hb=HEAD).
And to install DAVdroid, check [this
page](https://davdroid.bitfire.at/download).

</div>

Gotta Import 'Em All
====================

Ran :

    $ calypso.py --import private/YOUR_NAME your-vcard.vcf

only imported the first contact at `your-vcard.vcf`. I wrote a patch to
fix this behaviour that is available at
<http://keithp.com/pipermail/calypso/2016-January/000089.html>.

Add new contact
===============

DAVdroid will only send your contacts to the server if you save them
into a DAVdroid account.

1.  Go into "Contacts".
2.  Select "Add new contact".
3.  Select the "Save to ..." option.
4.  Select the DAVdroid account.

Remove account
==============

To remove one account:

1.  Go into "Settings".
2.  Got into "Accounts".
3.  Go into "All".
4.  Go into "DAVdroid".
5.  Select the account you want to remove.
6.  Select "More" options.
7.  Select "Remove account".
