---
layout: post
title: "O que é ciência?"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Semana passada <a href="http://kaythaney.com/2013/08/15/the-life-scienc/">Kaitlin Thaney publicou</a> a seguinte frase de John Wilbanks:</p>
<blockquote>
<p>&quot;The life sciences would seem, on the surface, ideal for open source. It’s a world built on disclosure – whether publication or patent – it doesn’t count until you tell the world. It’s a world where the knowledge itself snaps together in a fashion that looks eerily like a wiki, where one person only makes a small set of edits in an experiment that establishes a new fact. And it’s a world where the penalty for redundancy is high – no one in their right mind wants to spend scarce research dollars on a problem that has been solved already, a lead that is a dead end, a target guaranteed to lead to side effects.&quot;</p>
</blockquote>
<p>Que, em tradução literal, seria algo como:</p>
<blockquote>
<p>&quot;As ciências biológicas deveriam ver, na superfície, princípios para o código aberto. As ciências biológicas são um mundo construído sob descobertas -baseada em publicações ou patentes - que não importam até serem divulgadas para o mundo. É um mundo onde o conhecimento por se só parece como uma wiki, onde cada pessoa apenas produz uma pequena quantidade de edições em um experimento que estabelece um novo fato. E é um mundo em que a penalidade para redundância é alta - ninguém em plena consciência deseja gastar os escarsos financiamentos em um problema que já tenha sido resolvido, em um caminho sem saída, em um alvo que irá produzir resultados com efeitos colaterais.&quot;</p>
</blockquote>
<div class="more">

</div>
<p>Gostei muido desse texto de John Wilbanks que apareceu originalmente em um de seus textos publicados em <a href="http://fastercures.tumblr.com/post/56790751132/understanding-open-science">Faster Cures</a> (<span data-role="download">cópia nesse servidor &lt;understanding-open-science.html&gt;</span>) pois resume muito bem o que a ciência é.</p>
<p>Ainda em seu texto, John afirma que o custo de um medicamento entrar no mercado são, em média, 17 anos e bilhões de dólares (e acredito que isso é válido para todas as outras áreas tecnológias que não a de software). Ele ainda afirma que a maneira de reduzir esse custo é seguir uma metodologia de &quot;código aberto&quot; (colaboração baseada em padrões, licenças abertas, ...).</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>ciência aberta</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
