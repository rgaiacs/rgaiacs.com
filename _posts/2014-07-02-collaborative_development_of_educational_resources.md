---
layout: post
title: "Collaborative Development of Educational Resources"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>In the last few years I tried push the collaborative development of educational resources with GitHub for undergraduates students and fail. My vote for the reason that I fail is, as suggest by <a href="http://software-carpentry.org/blog/2014/03/collaborative-lesson-development.html">Justin Kitzes</a>, that</p>
<blockquote>
<p>&quot;potential contributors need a certain level of familiarity and comfort with the tools that enable contributions (such as version control or a browser-based Wiki editor).&quot;</p>
</blockquote>
<p>Another possible reason is that, most of the time, educational resources are used offline what make difficult for people contribute.</p>
<p>Below you will find food for thought about this topic.</p>
<div class="more">

</div>
<h1 id="bazzar">Bazzar</h1>
<p>At <a href="http://www.catb.org/esr/writings/cathedral-bazaar/">The Cathedral and the Bazaar</a> Eric S. Raymond wrote about the success of free/open source software development. This type of development wasn't restricted to software source code but also cover the documentation of such softwares.</p>
<figure>
<img src="/images/bazzar.png" class="align-center" style="width:50.0%" />
</figure>
<p>At that time, this type of development require people to exchange <a href="http://en.wikipedia.org/wiki/Patch_%28computing%29">patches</a> by email and was restricted to who was internet access and knows how handle patches.</p>
<h1 id="wikipedia">Wikipedia</h1>
<p>In 2001 <a href="http://www.wikipedia.org/">Wikipedia</a> was launch. It make easy to every one with internet access to contribute with the free/open encyclopedia since Wikipedia's user don't need to know handle patches.</p>
<figure>
<img src="/images/wikipedia.png" class="align-center" style="width:50.0%" />
</figure>
<p>With the success of Wikipedia, many sisters sites was launch, e.g. <a href="http://www.wikibooks.org/">Wikibooks</a>, to store books, and <a href="http://www.wikiversity.org/">Wikiversity</a>, to store courses.</p>
<figure>
<img src="/images/wikibooks.png" class="align-center" style="width:50.0%" />
</figure>
<p>Unfortunately, this sisters sites didn't have the same success of Wikipedia and one possible reason for it is, as suggest by <a href="http://software-carpentry.org/blog/2014/03/collaborative-lesson-development.html">Greg Wilson</a>, that</p>
<blockquote>
<p>&quot;while lots of people in education can write and edit lesson plans, they appear not to have taken on the broader role of managing the development of collaborative materials.&quot;</p>
</blockquote>
<h1 id="github">GitHub</h1>
<p>In 2008, <a href="http://github.com/">GitHub</a> was launch and enforce the &quot;fork model&quot; for collaboration. To understand the &quot;fork model&quot; lets go back to the early days of free/open source softwares. During the 80s and 90s, free/open source software developers exchange their contributions/patches using email and the maintainer of the software was responsible to accept or not the contributions. Some times, contributors had their patches reject by the maintainer and fork the software, i.e., start maintainer a new software that is the original one with some patches.</p>
<figure>
<img src="/images/github.png" class="align-center" style="width:50.0%" />
</figure>
<p>When using GitHub, the first step to contribute with some project is fork it. After you make changes in your fork you can send the changes back to the original project. This approach has some advantages since it make easy to someone without computer background get the version with the changes he/she need.</p>
<h1 id="what-next">What next?</h1>
<ul>
<li>If MediaWiki, the software that powered Wikipedia, support a &quot;fork model&quot; sisters sites like Wikibooks and Wikiversity would have more success?</li>
<li>If a software to import lessons exist educators will use it?</li>
</ul>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>none</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
