---
layout: post
title: "Price Book"
author: "Raniere Silva"
categories: apps
tags: ["Life Style"]
image:
  feature: 2020-02-27-price-book.jpg
  author: Fabian Blank
  licence: Unsplash License
  original_url: https://unsplash.com/photos/pElSkGRA2NU
---

When moving cities,
one of the challenges
that I face is to rewire my brain
to know the local prices
and where I will find good deals.

I searched for a Android app
to help me keep track of prices of grocery shop
and
I decided to give a try to [Price Reminder (Price Book)](https://play.google.com/store/apps/details?id=abid.pricereminder).
So far I'm happy with it.

Price Reminder (Price Book) isn't open source.
I searched at [F-Droid](https://f-droid.org/en/)
but I didn't find a similar app.