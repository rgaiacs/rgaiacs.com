---
layout: post
title: "Dominíos e dados"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>No vocabulário técnico da internet, domínio é uma sequência de caracteres que remete ao endereço de um computador/servidor ligado a rede mundial de computadores. Exemplos de domínios são: <a href="google.com" class="uri">google.com</a>, <a href="http://google.com.br">google.com.br</a>, <a href="http://facebook.com">facebook.com</a>, <a href="http://fsf.org">fsf.org</a>, <a href="http://gnu.org">gnu.org</a>, ...</p>
<p>A relação entre um domínio e o endereço do servidor que possue o conteúdo da página associada ao domínio é feita por algumas poucas organizações/empresas (para maiores informações comece por <a href="http://pt.wikipedia.org/wiki/Registrar">este artigo da wikipédia</a>). Essas organizações/empresas possuem um enorme poder de censura pois podem tornar inacessível uma enorme quantidade de páginas da web.</p>
<div class="more">

</div>
<p>Além da questão de domínio, temos também a questão dos dados (das páginas). Hoje em dia uma quantidade significativa da informação de cada usuário da internet encontram-se em servidores de terceiros como Google, Facebook, Yahoo, Amazon, ... que podem tanto ficar inacessível temporariamente ou permanentemente dependendo da vontade dessas empresas.</p>
<p>Nessa semana o site KickassTorrents (um dos maiores sites de torrent da atualidade, muito provavelmente atrás apenas do The Pirate Bay) ficou temporariamente indisponível (ler sobre em <a href="http://torrentfreak.com/kickasstorrents-domain-seized-after-music-industry-complaint-130614/">TorrentFreak</a>) devido a questões jurídicas envolvendo material protegido por direito autoral.</p>
<p>O motivo do site ficar indisponível é que &quot;empresa&quot; responsável pelo domínio (antigo, o KickassTorrents já está de volta em um novo domínio, <a href="http://kickass.to">kickass.to</a>) <a href="http://kat.ph">kat.ph</a> removeu o endereço do servidor que hospedava a página de seu banco de dados (devido a uma decisão judicial).</p>
<p>É importante destacar que o KickassTorrents só voltou ao ar pois &quot;eles&quot; estavam hospedando os próprios dados (ou possuiam cópia dos mesmos). Se não fosse por isso, teria acontecido algo semelhante ao antigo Megaupload.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>none</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
