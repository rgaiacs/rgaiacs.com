---
layout: post
title: "2nd MathML meeting"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p><strong>Note</strong>: This post was update in January 29th and we already have the log of #mathml at <a href="http://logbot.glob.com.au/?c=mozilla%23mathml">http://logbot.glob.com.au/?c=mozilla%23mathml</a>.</p>
<p>This is a report about the 2nd Mozilla MathML IRC Meeting and the first one of 2014 (see the announcement <a href="https://groups.google.com/d/msg/mozilla.dev.tech.mathml/MLtLdZ_9scw/98cRcbeFZLoJ">here</a>). The topic of the meeting can be found in <a href="https://etherpad.mozilla.org/rXDQX1Zebq">this PAD</a> (<span data-role="download">local copy of the PAD
&lt;pad.txt&gt;</span>) and the <span data-role="download">IRC log is here&lt;irc.txt&gt;</span>.</p>
<p>In case you just want to know how you can help the Mozilla MathML community, go to the <a href="#need-help">Need help</a> section.</p>
<p>The next meeting will be in February 13th 9PM UTC at #mathml IRC channel. Please add topics in the <a href="https://etherpad.mozilla.org/0JHTjusvnW">PAD</a>.</p>
<div class="more">

</div>
<h1 id="fosdem">FOSDEM</h1>
<p><a href="https://fosdem.org/2014/">FOSDEM</a> is a free event that offers open source communities a place to meet, share ideas and collaborate. Some folks from the Mozilla community go to the event but look like that no one of the MathML community will be there.</p>
<p>More information about the FOSDEM can be found in <a href="https://wiki.mozilla.org/Fosdem:2014">Mozilla Wiki</a>.</p>
<h1 id="mozilla-science-lab">Mozilla Science Lab</h1>
<p><a href="http://mozillascience.org/">Mozilla Science Lab</a> is a Mozilla project to push the open science movement (open access, open data, open education, ...). Last month they was talking about &quot;<a href="http://mozillascience.org/what-should-we-teach-about-publishing-on-the-web/">What should we teach about publishing on the web?</a> (you can join the discussion on <a href="https://github.com/swcarpentry/bc/issues/199">GitHub</a>).</p>
<p>At first the answer for the question should be easy: HTML. But there are some topics related to this question that I will try to summarize:</p>
<ul>
<li>For text you should use HTML but what should you use for code and data?</li>
<li>How work with others in a web world? Maybe use some version control system?</li>
<li>What tools/markup languages to use? LaTeX, Markdown, ODT (LibreOffice), DOCX (Word), ...?</li>
</ul>
<h1 id="bugs">Bugs</h1>
<p>jkitch is working in <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=442637">bug 442637</a> that cover <code>&lt;msup&gt;</code> with glyphs.</p>
<p>fredw is working on support for Open Type fonts with a MATH table and WG9s has integrated fredw's patches in <a href="http://www.wg9s.com/mozilla/firefox/">his builds</a>.</p>
<p>fredw proposed to deprecate <code>&lt;mstyle&gt;</code> and &quot;only rely on normal CSS inheritance&quot;. If you want more information about that look in the log after 19:35:16.</p>
<p>karl suggest that <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=941611">bug 941611</a> about width of italic characters should be fix ASAP. This bug only happen in Windows and Mac OS X.</p>
<p>jkitch and jfkthame talk a little about <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=442637">bug 442637</a> (ssty correction for scripts) and 941611.</p>
<h1 id="mathml-in-wikipedia">MathML in Wikipedia</h1>
<p><strong>Update</strong>: fredw wrote a great post about that <a href="http://www.maths-informatique-jeux.com/blog/frederic/?post/2014/01/13/Improvements-to-Mathematics-on-Wikipedia">in his blog</a>.</p>
<p>physikerwelt implemented MathML support for <a href="http://www.mediawiki.org/wiki/MediaWiki">MediaWiki</a> (the patch still need to be review).</p>
<p>After the patch be add in the master branch of MediaWiki and go to production Wikipedia will send MathML for Firefox and SVG for browsers without MathML support.</p>
<p><strong>OFF TOPIC</strong>: There is a plugin for MediaWiki Visual Editor that handle mathematical equations. More information in the <a href="http://www.mediawiki.org/wiki/Extension:VisualEditor/Mathematical_equations_plugin">plugin page</a>.</p>
<h1 id="mathml-in-mdn">MathML in MDN</h1>
<p>MDN use <a href="http://ckeditor.com/">CKEditor</a> with MathML support but you need go to the source view to insert MathML expressions.</p>
<p>fredw wrote a <a href="https://github.com/fred-wang/TeXZilla">Javascript parser from (La)TeX to MathML</a> and I wrote a <a href="https://github.com/r-gaia-cs/CKEditor-TeXZilla">plugin for CKEditor</a> that use fredw's parser to insert MathML.</p>
<p>Look like that the plugin only work with Firefox (Chrome and IE show the MathML and the semantics and fredw report that it not work with Safari).</p>
<h1 id="webgl">WebGL</h1>
<p>fredw wrote a <a href="http://www.maths-informatique-jeux.com/international/mathml-in-webgl/index.html">demo</a> that use WebGL and MathML and has plan to write others.</p>
<h1 id="irc-meeting">IRC meeting</h1>
<p>Some folks want to try another format for the meeting (toll free number, jabber/sip, WebRTC, hangout/skype, ...) but for the next meeting we will stay in the IRC.</p>
<h1 id="need-help">Need help</h1>
<ul>
<li><p>Review patch to MediaWiki for MathML support</p>
<p><strong>Required</strong>: PHP and Javascript knowledge.</p>
<p><strong>Tips</strong>: <a href="http://www.formulasearchengine.com/review" class="uri">http://www.formulasearchengine.com/review</a></p>
<p><strong>Link</strong>: <a href="https://gerrit.wikimedia.org/r/#/c/85801/" class="uri">https://gerrit.wikimedia.org/r/#/c/85801/</a></p></li>
<li><p>Help in bug 941611 (width of italic characters)</p>
<p><strong>Required</strong>: machine with Windows or Mac OS X.</p>
<p><strong>Link</strong>: <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=941611" class="uri">https://bugzilla.mozilla.org/show_bug.cgi?id=941611</a></p></li>
<li><p>Suggestions for demos with WebGL and MathML</p>
<p><strong>Sample</strong>: <a href="http://www.maths-informatique-jeux.com/international/mathml-in-webgl/" class="uri">http://www.maths-informatique-jeux.com/international/mathml-in-webgl/</a></p></li>
</ul>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>MathML,Mozilla</p>
</div>
<div class="tags">
<p>MathML meetings</p>
</div>
<div class="comments">

</div>
