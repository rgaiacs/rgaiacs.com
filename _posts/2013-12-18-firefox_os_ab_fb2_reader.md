---
layout: post
title: Firefox OS - Ab FB2 reader
author: "Raniere Silva"
categories: ftb
tags: [ftb]
image:
  feature: ftb.jpg
---
This is a review of the Firefox OS [Ab FB2
reader](https://marketplace.firefox.com/app/ab-fb2-reader?src=search)
app. Look like that it's source code is available in [Google
Code](https://code.google.com/p/abfb2/) and it's GPLv3. For testing this
app I used:

-   [A Christmas Carol in Prose; Being a Ghost Story of Christmas by
    Charles Dickens](http://www.gutenberg.org/ebooks/46) from Project
    Gutenberg and
-   [Moby Dick by Herman
    Melville](http://code.google.com/p/epub-samples/downloads/detail?name=moby-dick-20120118.epub&can=2&q=)
    from IDPF [epub-sample
    repository](http://code.google.com/p/epub-samples/).

The home screen of Ab FB2 reader isn't very clear.

![](/images/2013-12-18-20-45-02.png)

After select the file it will try to "load" it all at once and because
of that can't open Moby Dick (Being a Ghost Story open after some time).

![](/images/2013-12-18-20-55-58.png)

Once the book is open it keep the book selection and if you try to go to
the next page moving your finger from left to right it scroll the
window, what shouldn't happen.

![](/images/2013-12-18-21-01-09.png)

To remove the book selection you have to move your finger from bottom to
top **twice** and after that you can change the page.

![](/images/removing-file-selection.png)

The book selection has a selector for the chapter/section. After click
you will see a list of option, after select the chapter/section you want
press the OK button (if you want to go back keep the selection in the
current chapter/section and press the OK button).

![](/images/using-table-of-contents.png)

The worst part of this app is the pagination since it let some lines be
cut in the bottom and top of each page.

![](/images/bad-page-format.png)

If you slide your fingers apart the font will increase and if you slide
your fingers together the font will decrease.

![](/images/change-font-size.png)

Some times when trying change the font it will select part of the text.

Conclusion
==========

This app is a very bad EPUB reader. The interface isn't easy to use, has
a bad pagination algorithm and crash when open some EPUBs.
