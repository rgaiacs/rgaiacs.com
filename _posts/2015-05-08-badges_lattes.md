---
layout: post
title: "Open Badges e a Plataforma Lattes"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<div class="admonition">
<p>Nota</p>
<p>Eu comecei a escrever esse post em Janeiro de 2014 e ele ficou como rascunho por mais de um ano.</p>
<p>Resolvi retomá-lo dado algumas conversas que resurgiram.</p>
</div>
<p>Em Janeiro de 2014, um colega me disse que não comprava a proposta de micro-certificação que também é conhecido por badges. Tive que atualizar meu currículo Lattes e me aborreci bastante por não ter aprendido (se é que alguém sabe) como realizar essa tarefa. Para diminuir meu aborrecimento resolvi escrever este post sobre como o modelo de micro-certificação pode tornar o currículo Lattes muito mais amigável.</p>
<div class="more">

</div>
<p>A <a href="http://lattes.cnpq.br/">Plataforma Lattes</a> é um (des)serviço mantido pelo CNPq (vinculado ao Ministério da Ciência, Tecnologia e Inovação) que hospeda o currículo dos pesquisadores brasileiros. Pesquisadores brasileiros só podem receber financiamento do CNPq se (1) possuírem o currículo na Plataforma Lattes e (2) se o currículo estiver atualizado. Infelizmente, para atualizar o currículo o usuário da plataforma precisam inserir <strong>manualmente</strong> todas as informações por meio de uma interface bastante confusa.</p>
<p>O <a href="http://openbadges.org/">Open Badges</a> é um padrão aberto desenvolvido principalmente pela Fundação Mozilla para representar as habilidades/conquistas/premiações de um indivíduo. Uma badge consiste em um arquivo PNG (sim um arquivo de imagem) com vários metadados de maneira que ela pode ser <strong>facilmente</strong> armazenada, transferida e visualizada.</p>
<p>A maior quantidade de informações que adicionamos no Lattes está relacionado com artigos, posters, softwares, patentes, participações/apresentações em eventos, organização de eventos, ... Boa parte dessas informações poderiam ser transferidas através de Open Badges de forma a facilitar a vida do usuário que ao invés de navegar em um interface confusa precisaria apenas enviar um arquivo PNG para a Plataforma Lattes, algo que espera-se ele estar habituado a fazer pois vários serviços na internet permitem ao usuário subir uma foto.</p>
<p>Os receios do meu colega eram (1) qualquer pessoa poder emitir suas próprias certificações e (2) averiguar veracidade da certificação.</p>
<p>A Plataforma Lattes não resolve o problema (1). É permitido você organizar uma sessão de poster, apresentar seu poster na sessão que organizou e adicionar esse trabalho no seu currículo Lattes.</p>
<p>Atualmente a Plataforma Lattes também não resolve o problema (2). Se eu adiciono no meu currículo Lattes que eu escrevi um software não existe ninguém que vá certificar que eu realmente escrevi o software. Felizmente, a adoção de Open Badges oferece uma solução para esse problema. Embora qualquer um possa emitir uma badge, fazê-lo demanda muito mais trabalho do que simplesmente preencher um formulário pois você precisa assinar digitalmente a badge ou disponibilizar publicamente uma cópia da badge em um servidor web. Por esse motivo, apenas algumas pessoas iriam emitir badges e isso &quot;resolveria&quot; o problema (2).</p>
<p>Vamos para um exemplo para deixar as coisas simples. Atualmente, adicionar participação em um evento no Lattes requer o preenchimento de um formulário que é algo simples, depois que você entende a interface confusa, e por isso é fácil adicionar um dado errado. Se você tiver apenas que enviar um PNG que a organização do congresso lhe enviou por email você irá economizar tempo e ter certeza que a informação está correta. Para a organização do congresso, enviar uma Open Badge não é trabalho adicional porque ela já iria emitir um certificado (em papel ou em PDF) para você.</p>
<p>Será que o Lattes consegue entrar na <a href="https://en.wikipedia.org/wiki/Semantic_Web#Web_4.0">Web 3.0</a>?</p>
<div class="admonition">
<p>Mozilla Science Lab Sprint 2015</p>
<p><a href="http://www.mozillascience.org/announcing-our-global-sprint-2015">Mozilla Science Lab está organizando outro sprint de inverno</a> que irá ocorrer em 4 e 5 de Junho. Um dos projetos participantes no sprint será <a href="http://www.mozillascience.org/projects/contributorship-badges">Contributorship Badges</a> que está relacionado ao uso de Open Badges pela comunidade científica.</p>
<p>Se você não quiser esperar pelo sprint, o código encontra-se em <a href="https://github.com/mozillascience/PaperBadger" class="uri">https://github.com/mozillascience/PaperBadger</a>.</p>
</div>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>academia,ciência aberta,Mozilla Science Lab</p>
</div>
<div class="tags">
<p>Lattes,Open Badges,Mozilla Science Lab Sprint 2015</p>
</div>
<div class="comments">

</div>
