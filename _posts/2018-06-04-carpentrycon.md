---
layout: post
title: "CarpentryCon2018"
author: "Raniere Silva"
categories: blog
tags: [CarpentryCon, CarpentryCon2018, The Carpentries, Software Carpentry, Data Carpentry, Dublin]
image:
  feature: 2018-06-04-carpentrycon.jpg
  author: Bérénice Batut
  original_url: https://flic.kr/p/252fVid
  licence: CC-BY
---

Last week,
I went to Dublin for the first ever [CarpentryCon](http://carpentrycon.org)
organised by [The Carpentries](http://carpentries.org),
formely know as [Software Carpentry](http://software-carpentry.org) and [Data Carpentry](http://data-carpentry.org).

I got involved with Software Carpentry
for the first time in 2013
when the [Mozilla Science Lab](https://science.mozilla.org/) was [announced](https://blog.mozilla.org/blog/2013/06/14/5992/).
The announcement says

> Digital literacy for scientists
>
> Kaitlin is joined by Greg Wilson, the founder of Software Carpentry, a program that teaches basic computing skills to researchers to help them become more productive. Over the past year, Software Carpentry has run over 70 workshops for more than 2200 attendees, and is on track to double those numbers over the next 12 months. As part of the Mozilla Science Lab, Software Carpentry will explore what “digital literacy” means for scientific researchers and how these digital skills can further aid their work.

On the occasion of the Mozilla ScienceLab launch,
[Kaitlin Thaney](https://kaythaney.com/) wrote

> The first member of my team is [Greg Wilson](http://twitter.com/gvwilson), founder of [Software Carpentry](http://software-carpentry.org/), a program that teaches basic computational literacy to researchers to help them be more productive. I’ve long admired Greg’s work in this space, in providing an entry point for students to learn things like version control, data management, basic scripting. In the last year alone, they’ve run over 70 events for more than 2,200 attendees – all led by volunteers – and are on track to double both numbers in the coming twelve months. More importantly, Software Carpentry is our first step in exploring what “digital literacy” ought to be for researchers and what they need to know to actually do it.

[on her blog](https://kaythaney.com/2013/06/14/announcing-the-mozilla-science-lab/).

In the following years,
I went from lucker to member of the Executive Council.
It was a facinating journey
and CarpentryCon showed that every second volunteered
did worth to create the inspiring community
that The Carpentries is.

So many things happened during CarpentryCon
that even write a very long book would not be enough to cover everything.
And select the highlights is really hard!
So I will leave you with my personal reflection.

{% include figure.html filename="2018-06-04-carpentrycon-intro.jpg" alternative_text="Photo of Story Circle Icebreaker" caption="Story Circle Icebreaker" author="Bérénice Batut" original_url="https://flic.kr/p/26piCkK" licence="CC-BY" %}

You can classify meetings into two buckets based on the number of people that are attending and you meet before.
For me,
meetings where I already know some people are more fun
and the organisers agreed with me because they started CarpentryCon with a "Story Circle Icebreaker".
Attendees were splited into small groups where every participant had 4 minutes to introduce themselves.
The activity were great to make the conference more welcome to people that only join recently
or weren't super active.
My feedback on this activity is to make sure that all attendees are in one group
and to avoid people that already know each other in the same group.
[PuLP](https://pythonhosted.org/PuLP/) is a great library to help with this task
and we have people in the community that can help.

{% include figure.html filename="2018-06-04-carpentrycon-allies.jpg" alternative_text="Photo of Valerie Aurora during Focus on Allies keynote" caption="Valerie Aurora during Focus on Allies keynote" author="Bérénice Batut" original_url="https://flic.kr/p/27Gt5iq" licence="CC-BY" %}

The first keynote
was delivery by Valerie Aurora
who talked about allies during your journey.
Valerie shared [her slides](https://frameshiftconsultingdotcom.files.wordpress.com/2018/05/carpentryconslides.pdf) on [her blog](https://frameshiftconsultingdotcom.files.wordpress.com/)
that start with one definition for allies

> Ally: a member of a social group that enjoys some privilege that is **working to end oppression** and **understand their own privilege**

and continue with plenty examples of how they work:

01. An ally self-educates
02. An ally listens
03. An ally gives credit
04. An ally asks for consent from the target
05. An ally keeps the focus on helping targets
06. An ally speaks up & draws fire
07. An ally uses their energy wisely
08. An ally spends money
09. An ally uses their social capital
10. An ally acts even when it’s uncomfortable
11. An ally sacrifices personal gain
12. An ally follows leaders from marginalized groups
13. An ally makes mistakes - and apologizes

In this world, I couldn't have a better ally than Greg Wilson.

{% include figure.html filename="2018-06-04-carpentrycon-greg.jpg" alternative_text="Photo of Greg Wilson" caption="Greg Wilson" author="Bérénice Batut" original_url="https://flic.kr/p/27M2z78" licence="CC-BY" %}

Greg Wilson,
the co-founder of Software Carpentry 20 years ago,
was the second keynote.
"Late Night Thoughts on Listening to Ike Quebec"
was a talk about say "good bye" in 10 tips:

01. Be sure you mean it.
02. Do it when others think it's time.
03. Tell people what, when, and why.
04. Don't pick a successor by yourself.
05. Train them before you go.
06. When you leave, leave.
07. Have some fun before you go.
08. Reflect on what you learned.
09. Remember the good things too.
10. Do something next.

I recommend that you read [his blog post covering the presentation](http://third-bit.com/2018/05/30/late-nights-thoughts.html)
because is the best summary of it until the video record is available.

Talking about the next thing,
Africa is where a lot of The Carpentries exciting things are happening.

{% include figure.html filename="2018-06-04-carpentrycon-africa-task-force.jpg" alternative_text="Photo of Anelda van der Walt and members of Africa Task Force" caption="Anelda van der Walt and members of Africa Task Force" author="Bérénice Batut" original_url="https://flic.kr/p/27GryrU" licence="CC-BY" %}

The last keynote,
It Takes a Village,
started with Anelda van der Walt on stage 
who soon started calling
all her Africa Task Force allies
that were in CarpentryCon
to join her on stage
and talk what they were doing.
Anelda's action was a great demonstration
of lessons taught by Valerie Aurora and Greg Wilson
on the day before.

{% include figure.html filename="2018-06-04-carpentrycon-latinamerica.jpg" alternative_text="Photo of members from Latin America" caption="Members from Latin America" author="Bérénice Batut" original_url="https://flic.kr/p/252fyts" licence="CC-BY" %}

My take home from CarpentryCon was
that we are better prepared to expand The Carpentries activites
in Latin America now than we were in 2014
when I taught the [first workshop in Brazil](/blog/furg.html) with Fernando Mayer
but we still looking for a leader,
like Anelda van der Waltfor,
and more allies.
Drop me a [email](mailto:raniere@rgaiacs.com) to discuss workshops in Latin America,
including funds.