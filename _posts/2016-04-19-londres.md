---
layout: post
title: "Londres"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<figure>
<img src="/images/IMG_20160419_192419.jpg" alt="Ronda gigante de Londres." class="align-center" style="width:80.0%" /><figcaption>Ronda gigante de Londres.</figcaption>
</figure>
<p>Um pequeno passeio em Londres.</p>
<div class="more">

</div>
<p>Passando em alguns dos pontos mais famosos de Londres.</p>
<figure>
<img src="/images/IMG_20160419_155720.jpg" alt="People of London. O texto no topo da pedra é interessante mas difícil de ler porque ele está em caracol." class="align-center" style="width:80.0%" /><figcaption>People of London. O texto no topo da pedra é interessante mas difícil de ler porque ele está em caracol.</figcaption>
</figure>
<figure>
<img src="/images/IMG_20160419_160249.jpg" alt="Prédio próximo da Bank Station." class="align-center" style="width:80.0%" /><figcaption>Prédio próximo da Bank Station.</figcaption>
</figure>
<figure>
<img src="/images/IMG_20160419_161434_1.jpg" alt="Esquilos. &lt;3" class="align-center" style="width:80.0%" /><figcaption>Esquilos. &lt;3</figcaption>
</figure>
<figure>
<img src="/images/IMG_20160419_162251.jpg" alt="Outro prédio próximo da Bank Station." class="align-center" style="width:80.0%" /><figcaption>Outro prédio próximo da Bank Station.</figcaption>
</figure>
<figure>
<img src="/images/IMG_20160419_191905.jpg" alt="Se você passar pelo tunel debaixo da Waterloo Station você vai encontrar vários dezenos nas paredes." class="align-center" style="width:80.0%" /><figcaption>Se você passar pelo tunel debaixo da Waterloo Station você vai encontrar vários dezenos nas paredes.</figcaption>
</figure>
<figure>
<img src="/images/IMG_20160419_192751.jpg" alt="Olá o Big Ben no fundo!" class="align-center" style="width:80.0%" /><figcaption>Olá o Big Ben no fundo!</figcaption>
</figure>
<figure>
<img src="/images/IMG_20160419_192934.jpg" alt="O caminho da rainha." class="align-center" style="width:80.0%" /><figcaption>O caminho da rainha.</figcaption>
</figure>
<figure>
<img src="/images/IMG_20160419_193055.jpg" alt="Vista da roda gigante de Londres pela ponte." class="align-center" style="width:80.0%" /><figcaption>Vista da roda gigante de Londres pela ponte.</figcaption>
</figure>
<figure>
<img src="/images/IMG_20160419_193402.jpg" alt="Eu e o Big Ben." class="align-center" style="width:80.0%" /><figcaption>Eu e o Big Ben.</figcaption>
</figure>
<figure>
<img src="/images/IMG_20160419_195846.jpg" alt="Um dos festivais da primavera." class="align-center" style="width:80.0%" /><figcaption>Um dos festivais da primavera.</figcaption>
</figure>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>none</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
