---
layout: post
title: F-Droid no Xiaomi Redmi 2 Pro
author: "Raniere Silva"
categories: ftb
tags: [ftb]
image:
  feature: ftb.jpg
---
O [Xiaomi Redmi 2 Pro](http://br.mi.com/redmi2-pro) não é um aparelho
com software 100% livre/aberto mas é um fork do
[CyanogenMod](http://www.cyanogenmod.org/), que é um fork do Android.

![](/images/155.png){width="80%"}

Ele vem com o aplicativo da Play Store, a loja de aplicativos do Google,
instalado o que é muito bom pois evita a fragmentação do ecosistema
Android que já é bastante fragmentado. **Mas** se você prefere utilizar
software livre o que você gostaria de ter no seu celular é o aplicativo
do [F-Droid](http://f-droid.org/). Para quem não conhece,

> F-Droid é um catálogo de aplicativos FOSS (Software de código livre e
> aberto) para a plataforma Android. O cliente/aplicativo torna fácil
> navegar, instalar e manter atualizado os aplicativos no seu
> dispositivo.

Quais as grandes vantagens do F-Droid em relação à Play Store?

1.  Apenas aplicativos livres estão disponíveis no F-Droid.
2.  Todos os aplicativos possuem qualidade excelente e são gratuitos
    (**embora uma doação seja recomendada**).
3.  Você não é obrigado a utilizar o aplicativo para instalar os
    aplicativos. **Você pode baixar o instalador através do navegador
    web do seu celular.**
4.  Você não precisa de nenhuma conta para utilizar o F-Droid. (Da
    última vez que eu testei você precisava de uma conta do Google para
    utilizar a Play Store.)

Vamos instalar o F-Droid!!!

Abra o navegador web do celular.

![](/images/156.png){width="80%"}

Acesse <https://f-droid.org/> no celular.

![](/images/157.png){width="80%"}

Click em "installable" e salve o arquivo.

![](/images/158.png){width="80%"}

Quando terminar de baixar o arquivo, execute-o.

![](/images/161.png){width="80%"}

Aceite a instalação do aplicativo. **Estava esperando ter que habilitar
ou desabilitar alguma coisa nas configurações de modo que fiquei muito
feliz em não precisar.**

![](/images/163.png){width="80%"}

Confirme a instalação do aplicativo.

![](/images/164.png){width="80%"}

Não quero que o Google fique verificando o que eu faço no meu
dispositivo e portanto eu selecionei "Decline".

![](/images/166.png){width="80%"}

O aplicativo foi instalado com sucesso. Iniciamos o aplicativo.

![](/images/167.png){width="80%"}

Espere alguns instantes até que o aplicativo baixe o catálogo.

![](/images/168.png){width="80%"}

Agora você pode buscar e instalar os aplicativos que deseja.

![](/images/169.png){width="80%"}

Vamos buscar por um cliente para chat on-line que suporte o protocolo
XMPP.

![](/images/170.png){width="80%"}

Ao buscar por 'xmpp' encontramos vários resultados.

![](/images/171.png){width="80%"}

Vamos selecionar o aplicativo "Conversations".

![](/images/172.png){width="80%"}

Selecione instalá-lo.

![](/images/173.png){width="80%"}

Novamente a mesma tela pedindo permissão encontrada anteriormente.
Aceite.

![](/images/174.png){width="80%"}

Confirme a instalação.

![](/images/175.png){width="80%"}

Não aceite ser vigiado pelo Google.

![](/images/176.png){width="80%"}

Depois que o aplicativo tiver sido instalado é possível utilizá-lo.

![](/images/177.png){width="80%"}
