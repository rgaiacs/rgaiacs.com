---
layout: post
title: "The Hidden School"
author: "Raniere Silva"
categories: books
tags: [jekyll]
image:
  feature: 2017-06-06-hidden-school-cover.jpg
  author: Jack Wallsten
  original_url: https://flic.kr/p/wVH1o1
  licence: CC-BY
---

One of the books that I enjoyed reading was "[The Way of the Peaceful Warrior](https://en.wikipedia.org/wiki/Way_of_the_Peaceful_Warrior)"
by [Dan Millman](https://en.wikipedia.org/wiki/Dan_Millman).
I read the book many years after I watched the "[Peaceful Warrior](http://www.imdb.com/title/tt0438315/)",
a film adaption of Dan's book.

Today is the release of the newest of Dan's books,
"[The Hidden School](http://www.peacefulwarrior.com/the-hidden-school/)", 
and that is the book that I will be reading in the next few weeks.
