---
layout: post
title: "Breaking Lesson Manager Project"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>On August 1st I talk with Greg Wilson about the Lesson Manager. At this post I will try to sumarize what I learn from our conversation.</p>
<div class="more">

</div>
<p>The most important lesson learned is breaking the project into small parts:</p>
<figure>
<img src="/images/lesson_manager.svg" class="align-center" style="width:100.0%" />
</figure>
<h1 id="phase-1">Phase 1</h1>
<div class="admonition">
<p>Note</p>
<p>This phase was kick off during the <a href="http://mozillascience.org/the-mozsprint-heard-round-the-world/">first Mozilla Science Lab Summer Sprint</a> and will be amazing to continuing it during <a href="http://2014.mozillafestival.org/">Mozilla Festival</a>.</p>
</div>
<p>The objective of this first phase is build an prototype that people can use. For example one student could run :</p>
<pre><code>$ lesson-manager import exploratory-computing-with-python</code></pre>
<p>and get all the notebooks from Mark Bakker or run :</p>
<pre><code>$ lesson-manager import swc-git</code></pre>
<p>and get Software Carpentry's Git and Shell lessons.</p>
<p><strong>Why use lesson-manager instead of googling for the lesson?</strong> Because having the lessons locally enable students to interact/explore/hack the lessons (and provide it at one web page increase instructor's work).</p>
<p><strong>Why use IPython Notebook?</strong> (1) Because it allow instructors to include code samples or interactive content, (2) many people are already using it and (3) is possible to use it not only for Python but also for Sage, Julia, R, ...</p>
<h1 id="phase-2.a">Phase 2.A</h1>
<p>For students, version control isn't very important since they &quot;only care&quot; of major release of the lessons. But for instructors this is a very important feature since they already use version control on all their works.</p>
<p>Unfortunately isn't clear yet the best approach (submodule vs <a href="https://code.google.com/p/git-repo/">repo</a> vs ...) to support version control.</p>
<h1 id="phase-2.b">Phase 2.B</h1>
<p>For groups, organizations and some people ensure that the lessons have the same feel and look is important. This can only be achieve by some standardization.</p>
<h1 id="phase-2.c">Phase 2.C</h1>
<p>Instead of make that one lesson depends on another specific lesson, e.g. Software Carpentry's Git lesson depends on Software Carpentry's Shell lesson, will be nice that lessons depends on keywords/topics/... This isn't easy to do and also need some standardization.</p>
<h1 id="phase-2.d">Phase 2.D</h1>
<p>If the IPython Notebook use one Python package the Lesson Manager must install this package too. Find the packages being used isn't difficult but the use of virtual environments and non-Python dependencies is an challenge.</p>
<h1 id="phase-2.e">Phase 2.E</h1>
<p>Data is all important.</p>
<h1 id="phase-2.f">Phase 2.F</h1>
<p>Although IPython Notebook is <strong>AWESOME</strong> some people could want to write their lessons in another format (e.g. <a href="http://www.mathworks.com/help/matlab/ref/publish.html">MATLAB</a>) or the only format available is PDF (for scientific papers).</p>
<h1 id="phase-3.c">Phase 3.C</h1>
<p>Once lessons depends on keywords/topics/... will be easy to build a map to visualize the dependencies of some lessons like <a href="http://www.metacademy.org/">Metacademy</a>.</p>
<h1 id="phase-3.e">Phase 3.E</h1>
<p>Handle CSV or SQLite files with a few megabytes is easy. When you want using database of a few gigabytes this start to be a problem.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>none</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
