---
layout: post
title: "Ciência, tecnologia e acesso aberto"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<div class="note">
<div class="admonition-title">
<p>Note</p>
</div>
<p>Neste post pelo termo ciência entende-se &quot;o estudo racional das coisas em geral ou de algum assunto específico&quot; e pelo termo tecnologia entede-se &quot;um maquinário sofisticado&quot;.</p>
</div>
<p>Neste post irei falar da relação de ciência e tecnologia que pode ser representado pelo diagrama abaixo e também da importância do acesso aberto.</p>
<p><img src="/images/ciclo.svg" alt="digrama mostrando a relação entre ciência e tecnologia" class="align-center" style="width:50.0%" /></p>
<div class="more">

</div>
<p>Pelo diagrama verificamos que a ciência fornece novas idéias para a produção de novas tecnologias enquanto que a tecnologia fornece novas ferramentas para o desenvolvimento da ciência. Não é fácil construir exemplos para essa afirmação mas vou listar alguns:</p>
<ul>
<li>descoberta de melhores condutores elétricos que passam a ser utilizado na fabricação de chips para computadores que serão utilizados para pesquisar outros possíveis condutores elétricos,</li>
<li>descoberta de materiais pouco reativos que passam a ser utilizados na fabricação de instrumentos laboratoriais para pesquisa de novos materiais.</li>
</ul>
<p>Infelizmente temos os seguintes problemas:</p>
<ul>
<li>as novas ideias costuma ser veiculadas em jornais científicos que são muito caros,</li>
<li>os artigos nos jornais científicos muitas vezes não fornecem todas as informações necessárias para reproduzir o experimento e verificar os resultados obtidos,</li>
<li>as novas tecnologias também costumam ser comercializadas com preço elevado e</li>
<li>as especificações dos instrumentos são guardados como segredo industrial ou protegido por patentes.</li>
</ul>
<p>Esses quatro problemas apenas diminuem a velocidade em que nossa sociedade avança.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>academia,ciência aberta,Software Carpentry</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
