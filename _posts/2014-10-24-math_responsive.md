---
layout: post
title: "Responsive Design and Math"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>From <a href="https://en.wikipedia.org/wiki/Responsive_web_design">Wikipedia</a></p>
<blockquote>
<p>&quot;Responsive web design (RWD) is a web design approach aimed at crafting sites to provide an optimal viewing experience—easy reading and navigation with a minimum of resizing, panning, and scrolling—across a wide range of devices (from mobile phones to desktop computer monitors).&quot;</p>
</blockquote>
<p>Responsive design is a nice feature for web pages and EPUBs. At this post you will get an overview of the status of responsive design for math.</p>
<div class="more">

</div>
<h1 id="sample">Sample</h1>
<p>For the overview we need at least one example. I choose use LaTeX and <a href="https://github.com/brucemiller/LaTeXML">LaTeXML</a> to get the example in more than one format.</p>
<p>Starting from <span data-role="download">sample.tex</span> :</p>
<pre><code>$ latexmk -pdf sample.tex
$ latexmlc --format=epub --split --mathimages --nomathsvg --nopresentationmathml --destination=sample.epub sample.tex
$ latexmlc --format=epub --destination=sample-mathml.epub sample.tex
$ latexmlc --format=html5 --nomathimages --nomathsvg --destination=sample.html sample.tex</code></pre>
<div class="admonition">
<p>Files</p>
<ul>
<li><span data-role="download">sample.pdf</span></li>
<li><span data-role="download">sample.epub</span></li>
<li><span data-role="download">sample-mathml.epub</span></li>
<li><span data-role="download">sample.html</span></li>
</ul>
</div>
<h1 id="pdf">PDF</h1>
<p>The purpose of PDF is be the digital version of something that were printed. Because of this PDF doesn't support responsive design and read it on small screen is painful.</p>
<figure>
<img src="/images/pdf-screenshot.gif" />
</figure>
<h1 id="epub">EPUB</h1>
<p>Before EPUB3 you need to use bitmap images to get math in EPUB. Like PDF, bitmap images doesn't support responsive design. What you can do is set the image width to the screen width (this makes long equation be impossible to read) or set the image width to the original size (this makes long equation be crop or overflow across other pages).</p>
<figure>
<img src="/images/epub-screenshot.gif" />
</figure>
<h1 id="epub3-with-mathml">EPUB3 with MathML</h1>
<p>Starting with EPUB3 you can/<strong>should</strong> use MathML to get math in EPUB. Since MathML is &quot;powered&quot; by XML, the same technology that &quot;powered&quot; HTML, you can have responsive design on it.</p>
<figure>
<img src="/images/mathml-screenshot.gif" />
</figure>
<p>Unfortunately, <a href="http://www.w3.org/TR/MathML3/chapter3.html#presm.linebreaking">linebreaking of expressions</a> is something that aren't implemented in MathML render softwares neither at MathML authoring tools.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>academia,ciência aberta,educação,ensino,Firefox OS,livros,MathML,REA</p>
</div>
<div class="tags">
<p>ebooks,ereader,LaTeX,LaTeXML,MathML</p>
</div>
<div class="comments">

</div>
