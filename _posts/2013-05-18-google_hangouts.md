---
layout: post
title: Google Hangouts
author: "Raniere Silva"
categories: ftb
tags: [ftb]
image:
  feature: ftb.jpg
---
<div class="admonition warning">

Embora o título sugira que este seja um post em favor do Google
Hangouts, este post na verdade é o oposto.

</div>

<div class="admonition note">

Meus agradecimentos ao [Panaggio](http://identi.ca/panaggio) pela
notificação que motivou este post.

</div>

Para que não conhece, [XMPP](http://xmpp.org/) é um padrão "aberto"
baseado em XML para comunicação de texto (e vídeo) pela infraestrutura
da internet. (mais informações
[aqui](http://pt.wikipedia.org/wiki/Extensible_Messaging_and_Presence_Protocol)).

O XMPP, também conhecido por Jabber, é utilizado por vários serviços de
conversa via texto, incluíndo: Google Talk e Facebook Chat.[^1]

Ocorre que pelas informações veiculadas, o Google Hangouts não utiliza o
padrão XMPP:

> With Hangouts, Singhal says Google had to make the difficult decision
> to drop the very "open" XMPP standard that it helped pioneer.[^2]

A decisão do Google de abandonar o padrão XMPP permitiu a esse melhorar
a experiência do usuário ao integrar melhor seus serviços, embora fosse
necessário reescrever todos os produtos como disse Singhal.

A maioria das pessoas vão perguntar: "Se a experiência do usuário
melhorou, qual o problema em não utilizar mais o XMPP?". A resposta
encontra-se a seguir.

Serviços Federados e Padrões Abertos
====================================

A utilização de padrões abertos possibilita a existência de um serviço
ser oferecido por mais de uma pessoa/empresa e da portabilidade do
mesmo. Nos últimos anos, no Brasil, presenciamos uma onda de
portabilidade em várias áreas, inclusive, não tecnológicas como o de
seguro-saúde.

A existência de um serviço federado possibilita a comunicação entre
usuários que utilizem diferentes fornecedores do serviço. O melhor
exemplo de um serviço federado é a rede de telefonia no qual é possível
ligar para um outro número independentemente da companhia a qual
pertença o número de origem ou destino.

A escolha do Google eu não mais utilizar o padrão XMPP significa que que
utilizar o Google Hangouts só poderá comunicar, utilizando esse serviço,
com outras pessoas que também são usuários do mesmo.

Problemas com a nuvem
=====================

O uso do armazenamento de informação via serviços de terceiros é algo
que sempre deve ser ponderado.

> Hangouts keep your messages in the cloud — which isn’t exactly
> revolutionary, but since it's Google's cloud, there are some unique
> benefits. Every Hangouts conversation is stored online (and is
> accessible from any Hangouts app), but there is an option to toggle
> off history if you'd like to go off the record. The service’s Google+
> integration is one of the best features in the entire product: every
> photo that you or a friend posts is automatically saved in a private,
> shared album on Google+. For example, after a year of using Hangouts,
> it will be easy not just to trace the text conversations your budding
> relationship has produced, but to track the photos you’ve shared over
> time. The feature is so obvious and so compelling that it could
> theoretically do what few Google initiatives have managed thus far —
> give a huge number of users a real sense of affinity with Google+.

Ao deixar salvo sua informação você possibilita ao Google expandir o
conhecimento dele sobre você. Como a solução não é LIVRE não existe
garantia de que que a informação não será guardada ao desabilitar a
funcionalidade de gravação.

**Referencias**

[^1]: <http://adium.im/help/pgs/Accounts-ListOfServices.html>

[^2]: <http://www.theverge.com/2013/5/15/4318830/inside-hangouts-googles-big-fix-for-its-messaging-mess>
