---
layout: post
title: "Aaron Swartz: the rise of monopolies and the fight against totalitarianism"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p><a href="http://en.wikipedia.org/wiki/Neville_Roy_Singham">Neville Roy Singham</a> foi que realizou a palestra &quot;Aaron Swartz: the rise of monopolies and the fight against totalitarianism&quot;.</p>
<figure>
<img src="/images/aaron_swartz__the_rise_of_monopolies_and_the_fight_against_totalitarianism_0.jpg" class="align-center" style="width:80.0%" />
</figure>
<p>Um dos focos dessa palestra foi o recém anunciado <a href="http://en.wikipedia.org/wiki/PRISM">PRISM</a>, um programa de vigilância do governo americano, por <a href="http://en.wikipedia.org/wiki/Edward_Snowden">Edward Snowden</a>.</p>
<p>A seguir alguns pontos da palestra que gostei:</p>
<ul>
<li><p>&quot;Big Data is the future and we own it.&quot; CIA</p>
<p><a href="http://en.wikipedia.org/wiki/Big_data">Big Data</a> é um termo utilizado para o grande volume de informação armazenado/processado nos últimos anos (podemos considerar os últimos cinco). Essa informação possui origem nos inúmeros censores utilizados atualmente (dentre eles os aparelhos celulares) e nos logs de requisições de páginas da internet (botões das redes sociais).</p>
<p>Grandes empresas do ramo de informática (e.g., Google, Amazon, Apple, Microsoft, Yahoo,.. ) usam essa informação para, dentre outras coisas, criar um perfil dos usuários dos seus serviços. Como agora sabemos, essa informação é compartilhada com a Agência de Segurança Nacional dos Estados Unidos.</p></li>
<li><p>&quot;It is nearly withinout grasp to compute on all human generated infomartion.&quot;</p>
<p>O Big Data também está relacionado com o processamento da informação acumulada que só é possível pelas grandes empresas da área de informação e órgãos do governo.</p></li>
<li><p>&quot;The good citizien complex.&quot;</p>
<p>O complexo do bom cidadão pode ser enunciado como: se não tenho nada a esconder não existe problema no governo violar minha privacidade para pegar alguns criminosos.</p>
<p>Neville comparou esse complexo com a <a href="https://pt.wikipedia.org/wiki/S%C3%ADndrome_de_Estocolmo">síndrome de Estocolmo</a> o que achei muito bom.</p></li>
<li><p>Democrático ou não?</p>
<p>Foi apresentado duas comparações:</p>
<ol>
<li>armas de fogo e orgivas nucleares,</li>
<li>software da década de 80 e <a href="https://pt.wikipedia.org/wiki/Saas">SaaS (software como serviço)</a>.</li>
</ol>
<p>A constituição estadunidense considera armas de fogo algo democrática (e muitas pessoas uma orgiva nuclear não). De maneira semelhante o software da década de 80 era democrático mas o Saas não é.</p></li>
</ul>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>FISL</p>
</div>
<div class="tags">
<p>FISL14</p>
</div>
<div class="comments">

</div>
