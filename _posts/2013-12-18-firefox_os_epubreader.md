---
layout: post
title: Firefox OS - EPUBReader
author: "Raniere Silva"
categories: ftb
tags: [ftb]
image:
  feature: ftb.jpg
---
This is a review of the Firefox OS
[EPUBReader](https://marketplace.firefox.com/app/epubreader?src=search)
app. I can't find the license of this app but probably it's similar to
the one of [EPUBReader Firefox
extension](https://addons.mozilla.org/en-US/firefox/addon/epubreader/)
that can be found
[here](https://addons.mozilla.org/en-US/firefox/addon/epubreader/license/1.4.2.4)
(it's not compatible with GPL). For testing this app I used:

-   [A Christmas Carol in Prose; Being a Ghost Story of Christmas by
    Charles Dickens](http://www.gutenberg.org/ebooks/46) from Project
    Gutenberg and
-   [Moby Dick by Herman
    Melville](http://code.google.com/p/epub-samples/downloads/detail?name=moby-dick-20120118.epub&can=2&q=)
    from IDPF [epub-sample
    repository](http://code.google.com/p/epub-samples/).

It has a very simple home page with a single button in the top that list
the files that can be open.

![](/images/2013-12-18-20-30-27.png)

After press the button we go to the list of files available. The list is
very clear.

![](/images/2013-12-18-20-30-34.png)

The file selected open very quickly and to go to the next page you just
have to slide your finger from left to right (or right to left if you
want the previous one).

![](/images/2013-12-18-20-30-50.png)

Click in the center of the screen will show some options. One of them is
the table of contents (the box with lines inside).

![](/images/table-of-contents.png)

The pagination looks very good.

![](/images/examples-of-pages.png)

You can change the font size using the options after click in the center
of the screen.

![](/images/increase-font-size.png)

Conclusions
===========

It's a very good app. Some times the change the font size didn't work
and the change of chapter/section take some seconds.
