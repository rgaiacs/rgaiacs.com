---
layout: post
title: Padrões de código
author: "Raniere Silva"
categories: ftb
tags: [ftb]
image:
  feature: ftb.jpg
---
Toda vez que crio um novo projeto em que vou trabalhar fico perdido em
quais arquivos este projeto deveria ter, como estes arquivos deveriam
estar organizados e qual estilo deveria seguir. Como essas questões
dependem do tipo de projeto fico mais perdido ainda. Este *post* é uma
tentativa de deixar isso mais claro.

Normativas gerais
=================

1.  Sempre utilizar um sistema de controle de versão.

    **Motivo**: Controlar o avanço do projeto.
2.  Preferencialmente utilizar *lowercase* para nome de arquivos e
    pastas.

    **Motivo**: Evita ter que pressionar a tecla *shitf*.

    **Excessões**: Arquivos `AUTHORS`, `ChangeLog`, `CONTRIBUTING`,
    `COPYING`, `INSTALL`, `Makefile`, `NEWS`, `README` e `THANKS` que
    devem seguir a "*GNU coding standards*".
3.  Nomes de arquivos e pastas devem ser em inglês.

    **Motivo**: Nunca se sabe quando o projeto será internacionalizado.
4.  Nomes de arquivos e pastas devem utilizar `_` no lugar de espaço.

    **Motivo**: Espaços nos nomes de arquivis e pastas dificultam o uso
    do terminal. Alguns projetos utilizam `-` ao invés de `_` para
    evitar pressionar a tecla *shift* o que é interessante mas que
    algumas vezes pode deixar dúvidas se o `-` está no lugar de um
    espaço em branco ou se é mesmo um `-`.
5.  Nomes de pastas deve utilizar o menor número de caracteres.

    **Motivo**: Reduzir o número de caractes a serem digitados.

Programas de computador
=======================

Embora isso irá depender da linguagem utilizada, uma boa estrutura é:

-   \`projeto\`: raiz do projeto.
    -   `AUTHORS`: lista os nomes e endereços de emails dos autores.
    -   `ChangeLog`: descrever as mudanças no projeto entre uma versão e
        outra.
    -   `configure`: *shell script* para configuração do projeto.
    -   `CONTRIBUTING`: regras de como proceder para contribuir com o
        projeto.
    -   `COPYING`: licença do projeto.
    -   `doc`: a documentação do projeto.
    -   `INSTALL`: instruções para instalar o projeto.
    -   `Makefile`: *Makefile* para compilar/instalar o projeto.
    -   `NEWS`: anúncio das novas versões.
    -   `README`: descrição do projeto.
    -   `src`: os códigos do projeto.
    -   `test`: os códigos de teste unitário.
    -   `THANKS`: agradecimentos a contribuições ao projeto, e.g.,
        revisão, comentários, informação de *bugs*, ...

Livros/Relatórios técnicos
==========================

Embora isso irá depender da linguagem de marcação e do "compilador" que
estiver sendo utilizado, uma boa estrutura é:

-   \`projeto\`: raiz do relatório.
    -   `AUTHORS`: lista os nomes e endereços de emails dos autores.
    -   `ChangeLog`: descrever as mudanças no relatório entre uma versão
        e outra.
    -   `CONTRIBUTING`: regras de como proceder para contribuir com o
        projeto.
    -   `COPYING`: licença do projeto.
    -   `image`: as figuras utilizadas.
    -   `Makefile`: *Makefile* para compilar/instalar o projeto.
    -   `NEWS`: anúncio das novas versões.
    -   `README`: descrição do projeto.
    -   `THANKS`: agradecimentos a contribuições ao projeto, e.g.,
        revisão, comentários, informação de *bugs*, ...

Páginas web
===========

A seguinte estrutura deve ser utilizada:

-   \`projeto\`: raiz do projeto.
    -   `audio`: os arquivos de som utilizados.
    -   `AUTHORS`: lista os nomes e endereços de emails dos autores.
    -   `ChangeLog`: descrever as mudanças no projeto entre uma versão e
        outra.
    -   `CONTRIBUTING`: regras de como proceder para contribuir com o
        projeto.
    -   `COPYING`: licença do projeto.
    -   `css`: os arquivos de estilos.
    -   `image`: as figuras utilizadas.
    -   `INSTALL`: instruções para instalar o projeto.
    -   `js`: os arquivos javascripts.
    -   `NEWS`: anúncio das novas versões.
    -   `pdf`: os arquivos pdf.
    -   `README`: descrição do projeto.
    -   `robots.txt`: restringir o acesso de robôs.
    -   `THANKS`: agradecimentos a contribuições ao projeto, e.g.,
        revisão, comentários, informação de *bugs*, ...
    -   `video`: os arquivos de vídeo utilizados.
