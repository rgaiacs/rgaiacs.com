---
layout: post
title: "LaTeX Course at 3rd Day of EnCPos"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Last Friday was the last day of my course about LaTeX that I talk some days ago (<span data-role="doc">see the post here &lt;../14/latex_course_at_ecnpos&gt;</span>.</p>
<div class="more">

</div>
<p>I try to follow the plan with the pads (<span data-role="download">one for the contents
&lt;encpos-latex.txt&gt;</span> and <span data-role="download">other for feedbacks
&lt;encpos-feedback-latex.txt&gt;</span>) and the rounds:</p>
<dl>
<dt>Round 3.1 and 3.2</dt>
<dd><p>I like to try use GitHub Gist for review the codes of the students. I will ask the students to retype <span data-role="download">this pdf &lt;../14/gist-ref.pdf&gt;</span> (build from <span data-role="download">this file &lt;../14/gist-ref.tex&gt;</span>) starting from <span data-role="download">this LaTeX
document &lt;../14/gist.tex&gt;</span> (<span data-role="download">the pdf &lt;../14/gist.pdf&gt;</span>).</p>
<p>After the students retype the document they must copy and past it in a GitHub Gist and add the link in the pad. I will review the codes as soon as they finished the active.</p>
</dd>
<dt>Round 3.3</dt>
<dd><p>Introduction to in-line and display math mode.</p>
</dd>
<dt>Round 3.4</dt>
<dd><p>Some examples of BibTeX.</p>
</dd>
<dt>Round 3.5</dt>
<dd><p>Some examples of TikZ.</p>
</dd>
</dl>
<h1 id="exercise">Exercise</h1>
<p>I just end to teach a small course about LaTeX and ask the students to reproduce a little page (only half of it have text) in 40 minutes (students that already use LaTeX finish it in 20 minutes and novices only do half of the exercise) and after finish create a gist with their code (I review the code after the class). It was a very small experiment but seems to me that the students like the exercise.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>educação,Software Carpentry</p>
</div>
<div class="tags">
<p>LaTeX,EnCPos</p>
</div>
<div class="comments">

</div>
