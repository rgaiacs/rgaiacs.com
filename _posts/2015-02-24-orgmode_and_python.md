---
layout: post
title: Org Mode e Python
author: "Raniere Silva"
categories: ftb
tags: [ftb]
image:
  feature: ftb.jpg
---
[Org Mode](http://orgmode.org/) é um modo do Emacs para gerenciar notas
e manter lista de tarefas. O Org Mode possui uma "extensão", chamada Org
Babel, que possibilita o uso de várias linguagens de programação de
dentro do Org Mode. Nesse post vamos ver como utilizar Python dentro do
Org Mode.

<div class="admonition">

Nota

Antes de ler esse post você talvez queira ler
esse outro post &lt;../../../2014/12/17/orgmode\_lt&gt;.

</div>

Configuração
============

Adicione em \~/.emacs :

    (org-babel-do-load-languages
     'org-babel-load-languages
     '((python . t)))

Olá mundo
=========

<div class="admonition">

Download

Baixe o arquivo dessa sessão: sample1.org.

</div>

Blocos de código são delimitador por :

    #+BEGIN_SRC python

e :

    #+END_SRC

<div class="admonition">

Nota

Infelizmente não existe um atalho para adicionar os delimitadores.

Para editar o código mais facilmente você pode utilizar `C-c '`.

</div>

Escrevemos o "Olá mundo" da seguinte forma:

    $+BEGIN_SRC python
    return "Ola mundo"
    #+END_SRC

Para executar o código utilizamos `C-c C-c`. Ao executar o código irá
aparecer :

    #+RESULTS:
    : Ola mundo

<div class="admonition">

Nota

Para executar todo o buffer utilize `C-c C-v b`.

</div>

Opções
======

Cada bloco de código aceita um conjunto de opções. Para adicionar uma
nova opção utilize `C-c C-v j`.

<div class="admonition">

Nota

Utilize TAB para visualizar as opções disponíveis.

</div>

Algumas opções importantes são:

`:results`

:   Para informar o tipo de resultado a ser incluído. Para imagens
    utilize `file` como valor.

`:file`

:   Para informar o arquivo a ser utilizado para salvar a saída.

`:exports`

:   Informa o que deve ser exportado quando convertendo o arquivo do Org
    Mode. As opções são `code`, `results`, `both` ou `none`.

`:session`

:   Cria uma sessão com o nome utilizado.

Gráficos
========

<div class="admonition">

Download

Baixe o arquivo dessa sessão: sample2.org.

</div>

Para gráficos é preciso utilizar a opção `:results`. :

    #+BEGIN_SRC python :results file
    import sympy
    x = sympy.S('x')
    sympy.plot(x).save('plot.png')
    return 'plot.png'
    #+END_SRC

    #+RESULTS:
    [[file:plot.png]]

<div class="admonition">

Nota

É possível mostrar as figuras no Emacs utilizando
`M-x org-display-inline-images`.

</div>

![](/images/inline-plot.jpg){width="80%"}

Sessão
======

<div class="admonition">

Download

Baixe o arquivo dessa sessão: sample3.org.

</div>

Se você precisar do resultado de um bloco em outro, deve-se utilizar a
opção `:session`. :

    #+BEGIN_SRC python :session main
    import sympy
    x = sympy.S('x')
    eq =  x**2 + 5 * x + 4
    eq
    #+END_SRC

    #+RESULTS:
    : x**2 + 5*x + 4

    #+BEGIN_SRC python :session main
    sympy.solve(eq)
    #+END_SRC

    #+RESULTS:
    | -4 | -1 |

Referências
===========
