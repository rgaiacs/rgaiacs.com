---
layout: post
title: "Copyfight: muito além do download grátis"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>O debate &quot;Copyfight: muito além do download grátis&quot; iniciou a programação do FISL na maior das salas. A mesa foi composta por: Tobias Andersson, Tadzia Maya, Adriano Belisário, Bruno Tarin e Lucas Alberto Santos, Leo Foleto.</p>
<figure>
<img src="/images/copyfight__muito_alem_do_download_gratis_0.jpg" class="align-center" style="width:80.0%" />
</figure>
<p>A seguir você encontrará informações sobre cada um dos membros da mesa e sobre o que eles falaram.</p>
<h1 id="adriano-belisário">Adriano Belisário</h1>
<p>Autor do livro &quot;Copyfight&quot; (<a href="http://www.pesquisamundi.org/2012/12/livro-copyfight-discute-propriedade.html">post sobre o livro</a>).</p>
<p>Ele criticou a propriedade intelectual (copyright mais especificamente) por ser um modo de controlar que tipo de material que pode circular e um mecanismo de criar escassez para uma commodity.</p>
<h1 id="tobias-andersson">Tobias Andersson</h1>
<p>Um dos fundadores do Pirate Bay.</p>
<p>Destacou que no Brasil falamos de patentes de drogas enquanto que em muitos lugares ainda falam apenas de filmes e música. E também que já possuímos as tecnologias e também sabemos utilizá-las para fomentar uma internet livre.</p>
<h1 id="tadzia-de-oliva-maya">Tadzia de Oliva Maya</h1>
<p>Jornalista de formação, está envolvida com a Casa de Sementes Livres.</p>
<p>Ela começou sua fala com um texto de Richard Stallman:</p>
<blockquote>
<p>A maioria das pessoas não aprende as habilidades de um carpinteiro profissional, mas muitas pessoas fazem alguns trabalhos simples e quase todo o mundo algumas vezes martela um prego na madeira. Imagine uma proposta para proibir e impedir todo o mundo, menos os carpinteiros autorizados, de fazer qualquer trabalho em madeira. Você acreditaria no argumento de que &quot;esta liberdade é inútil para os que não são carpinteiros&quot;? Richard Stallman</p>
</blockquote>
<p>Ela utilizou o texto do Stallman para fazer um paralelo com a agricultura que cada vez mais utiliza menos agricultores e mais máquinas, insumos químicos, sementes geneticamente modificadas, ...</p>
<p>Sementes livres, caipiras ou crioulas foram aprimoradas por agricultores, famílias, culturas, ... durante décadas e hoje vem sendo apropriada por indústrias através de patentes. Essa privatização da natureza irá em pouco tempo impossibilitar de podermos cultivar nossa própria comida como hoje somos impossibilitados de escrever nossos próprios códigos nos celulares e tablets. (Maiores informações sobre sementes livres <a href="http://gaiasustentavel.net/2013/04/25/sementes-e-comunidades-copyleft/">aqui</a>.)</p>
<figure>
<img src="/images/sementes_livres.jpg" class="align-center" style="width:80.0%" />
</figure>
<p><a href="http://pt.wikipedia.org/wiki/Agroecologia">Agroecologia</a> é um movimento social deseja acesso a terra, reforma agrária, sementes livres, ... e a mídia só fala dos orgânicos (que são como a &quot;inclusão digital utilizando Microsoft&quot;).</p>
<p>Algo que falta é um esforço social de participar de encontros, reuniões de bairros, associações, ...</p>
<h1 id="leonardo-foleto">Leonardo Foleto</h1>
<p>Da Casa de Cultura de Porto Alegre/Mário Quintana e participante do ônibus hacker.</p>
<p>Afirmou que a luta para outras liberdades que não a de software e propriedade intelectual ainda é muito pouco difundida.</p>
<p>Disse que se uma lei coloca grande parte da sociedade como ilegal, como é o caso da lei de direitos autorais, essa lei precisa se adequar a sociedade e não o contrário.</p>
<p>Também é preciso falar mais para &quot;novatos&quot; e não apenas para membros das comunidades de software livre, hardware livre, cultura livre, ...</p>
<h1 id="bruno-tarin">Bruno Tarin</h1>
<p>Autor do livro &quot;Copyfight&quot; e membro da Universidade Nômade.</p>
<p>Os regimes de trabalho mudaram muito nos últimos anos. O que nossos pais/avôs entendem como trabalho e o que fazemos hoje em dia é muito diferente, além de hoje não existir mais uma separação clara entre trabalho e lazer.</p>
<h1 id="lucas-alberto-souza-santos">Lucas Alberto Souza Santos</h1>
<p>Membro da Associação Software Livre.</p>
<p>Uma frase que gostei muito: &quot;Uma luta contra o copyright é uma luta contra o capitalismo e a acumulação de capital.&quot;</p>
<p>Também deixou um questionamento: &quot;Para onde vamos depois da liberdade vencer o capitalismo?&quot;</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>FISL</p>
</div>
<div class="tags">
<p>FISL14</p>
</div>
<div class="comments">

</div>
