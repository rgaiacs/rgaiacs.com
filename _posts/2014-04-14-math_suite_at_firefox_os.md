---
layout: post
title: "Math Suite at Firefox OS"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>In the <a href="http://home.web.cern.ch/about/updates/2014/03/world-wide-web-born-cern-25-years-ago">first 25 year of the web</a> math was mostly treat as a second class citizen (how many sites with lots of equations do you know?) and those that work with math still do it offline.</p>
<p>Math on the web is almost a chicken-egg problem and Firefox OS, the mobile operational system developed by Mozilla, can help solve it. Below I will try to explain how Firefox OS can do it.</p>
<div class="more">

</div>
<p>W3C, the international standards organization for the World Wide Web, published a standard related to math, named MathML, that didn't get much attention from web browsers developers for years. Thanks the hard work of <a href="https://developer.mozilla.org/en-US/docs/Mozilla/MathML_Project">many contributors</a> Firefox OS support part of MathML specifications and will fulfill the need of many users.</p>
<p>In the last years, there was a increase demand for digital education contents, including math, and Firefox OS can be used as a platform for this contents.</p>
<h1 id="suite-stack">Suite Stack</h1>
<p>For &quot;Math Suite&quot; I denote a collection of applications to read, write, manipulate and share equations that can be used at K-12 and Higher education for teachers/instructors and students.</p>
<figure>
<img src="/images/stack.svg" class="align-center" style="width:50.0%" />
</figure>
<p>This collection of application is powered by a common Javascript library that enable:</p>
<ul>
<li>Copy and paste equations,</li>
<li>WYSIWYG edit,</li>
<li>Convert from/to (La)TeX,</li>
<li>Compute some basic operations,</li>
<li>Search among the equations in a document, ...</li>
</ul>
<p>working directly with MathML.</p>
<p>And the work of rendering the MathML expressions is left to the web browser engine.</p>
<h1 id="examples">Examples</h1>
<p>I already wrote some demos for Firefox OS that use MathML:</p>
<ul>
<li><a href="https://marketplace.firefox.com/app/math-cheat-sheet/">Math Cheat Sheet</a> is a collection of equations,</li>
<li><a href="https://marketplace.firefox.com/app/texzilla-1/">TeXZilla</a> is a (La)TeX to MathML annotation tool.</li>
</ul>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>Firefox OS,MathML</p>
</div>
<div class="tags">
<p>Firefox OS,MathML</p>
</div>
<div class="comments">

</div>
