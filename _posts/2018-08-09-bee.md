---
layout: post
title: "Bee in the City"
author: "Raniere Silva"
categories: blog
tags: [Manchester, bee, photos]
image:
  feature: 2018-08-09-bee.jpg
  author: Raniere Silva
  licence: CC-BY
---

From 23 July until 23 September,
Manchester is the home of more than 100 giant bees,
like the one that open this blog post,
as part of [Bee in the City project](https://beeinthecitymcr.co.uk/).

I saw a few in the city center last month
but only yesterday I noticed the one that is in front of my working place.

{% include figure.html filename="2018-08-09-bee-kilburn.jpg" alternative_text="Photo of the The Love Bee in front of Kilburn Building" caption="The Love Bee in front of Kilburn Building" author="Raniere Silva" licence="CC-BY" %}

It is called The Love Bee
and was created by Nisha Grover.

{% include figure.html filename="2018-08-09-bee-face.jpg" alternative_text="Frontal photo of the The Love Bee" caption="The Love Bee" author="Raniere Silva" licence="CC-BY" %}

{% include figure.html filename="2018-08-09-bee-back.jpg" alternative_text="Back photo of the The Love Bee" caption="The Love Bee" author="Raniere Silva" licence="CC-BY" %}

The location of all the bees are available in [this map](https://beeinthecitymcr.co.uk/wp-content/uploads/2018/07/180723-BITC_TrailMap_A4Online.pdf).

And the last opportunity to see all the bees before the auctioned
will be between 12/10/2018 and 14/10/2018
on the [Farewell Weekend](https://beeinthecitymcr.co.uk/event/farewell-weekend/).