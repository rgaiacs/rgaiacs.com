---
layout: post
title: "Mathml August Meeting"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>This is a report about the Mozilla MathML August IRC Meeting (see the announcement <a href="https://groups.google.com/d/msg/mozilla.dev.tech.mathml/2g2909vdlQ8/qXryo1VLNlYJ">here</a>). The topics of the meeting can be found <a href="https://etherpad.mozilla.org/mathml-2014-08">in this PAD</a> (<span data-role="download">local copy of the
PAD &lt;pad.txt&gt;</span>) and the <a href="http://logs.glob.uno/?c=mozilla%23mathml&amp;s=14+Aug+2014&amp;e=14+Aug+2014">IRC log</a> (<span data-role="download">local copy of the IRC log &lt;irc.txt&gt;</span>) is also available.</p>
<p>In the last 4 weeks the MathML team closed 5 bugs, worked in other 6 and open one bug. This are only the ones tracked by Bugzilla.</p>
<p>The next meeting will be in September 11th at 8pm UTC. Please add topics in the <a href="https://etherpad.mozilla.org/mathml-2014-09">PAD</a>.</p>
<div class="more">

</div>
<h1 id="appear.in">APPEAR.IN</h1>
<figure>
<img src="/images/mathml-at-appearin.png" class="align-center" style="width:60.0%" />
</figure>
<p>This is the first time that we try use WebRTC solutions for the meeting. The advantage of use WebRTC is that it can make us spend less time with the meeting (we are currently using two hours for the meeting at IRC) but the side effect is that some people couldn't participate at it because still at work or their main device doesn't have video/audio support.</p>
<p>We are going to continuing try using it a few more times and request that people use headphones.</p>
<h1 id="meeting-not-only-for-mozilla-related-topics">Meeting not only for Mozilla related topics</h1>
<p>Our meeting isn't limited to Gecko, as most of the readers of this report must know. I and Frédéric discovery that we must make it clear when invite people to this meeting.</p>
<p>And we also must find one way to reminder people of it without spam theirs inbox.</p>
<h1 id="mediawiki">MediaWiki</h1>
<p>Moritz gave an update of the state of MathML at MediaWiki. Moritz's work was deployed at <a href="http://en.wikipedia.beta.wmflabs.org/wiki/Main_Page">beta-labs</a> and <a href="http://en.wikipedia.beta.wmflabs.org/wiki/List_of_mathematical_series">some examples are available here</a> <a href="http://en.wikipedia.beta.wmflabs.org/wiki/List_of_formulae_involving_%CF%80">and here</a>.`</p>
<h1 id="latexml">LaTeXML</h1>
<p>Deyan gave an update of the state of LaTeXML. Bruce is working to fix small issues and Deyan in rewrite part of the code into C to improve performance.</p>
<p>Furthermore, Frédéric is trying to use LaTeXML to build some Web Apps for Firefox OS and <a href="https://github.com/brucemiller/LaTeXML/issues/510">report some bugs when trying it</a>.</p>
<h1 id="slow-render-of-mathml">Slow render of MathML</h1>
<p>Bruce <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=1043358">reported that Gecko is slow when rendering MathML</a>. Most of MathML contributors had worked trying to solve this bug.</p>
<p>I want to set a server to benchmark some pages and avoid this happens in the future. Karl suggest that I use <a href="https://wiki.mozilla.org/Buildbot/Talos">Talos</a>.</p>
<h1 id="end-of-google-summer-of-code">End of Google Summer of Code</h1>
<p>The Google Summer of Code end this week. The result of Raniere's project can be found at <a href="http://r-gaia-cs.github.io/gsoc2014/" class="uri">http://r-gaia-cs.github.io/gsoc2014/</a>.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>MathML,Mozilla</p>
</div>
<div class="tags">
<p>MathML meetings</p>
</div>
<div class="comments">

</div>
