---
layout: post
title: "Chapada dos Veadeiros"
author: "Raniere Silva"
categories: blog
tags: [Brazil, camping, photos]
image:
  feature: 2018-09-05-chapada-dos-veadeiros.jpg
  author: Raniere Silva
  licence: CC-BY
---

At the end of August,
I visit Chapada dos Veadeiros in Brazil
and did the Sete Quedas walk.

{% include figure.html filename="2018-09-05-chapada-dos-veadeiros-map.jpg" alternative_text="Photo of the map' garden" caption="Map" author="Raniere Silva" licence="CC-BY" %}

## Drive

We left Brasília around 5:30am
and didn't encounter any traffic on our way.
We were also blessed with a great sun rise.

{% include figure.html filename="2018-09-05-chapada-dos-veadeiros-0.jpg" alternative_text="Photo of sun rise in the road" caption="Sun rise in the road" author="Raniere Silva" licence="CC-BY" %}

The journey from Brasília until Chapada dos Veadeiros National Park was around 3 hous long.
And the landscape is amazing.

{% include figure.html filename="2018-09-05-chapada-dos-veadeiros-1.jpg" alternative_text="Photo of road and chapada" caption="Road and chapada" author="Raniere Silva" licence="CC-BY" %}

{% include figure.html filename="2018-09-05-chapada-dos-veadeiros-2.jpg" alternative_text="Photo of road and chapada" caption="Road and chapada" author="Raniere Silva" licence="CC-BY" %}

## Day 1

All the walk is in the trail
and you will find sings all the way.
Is almost impossible to get lost.

{% include figure.html filename="2018-09-05-chapada-dos-veadeiros-3.jpg" alternative_text="Photo of walk" caption="Walk" author="Raniere Silva" licence="CC-BY" %}

Visitation to the national park is only possible during the dry season
because when the river is not dry the walk is impossible.

{% include figure.html filename="2018-09-05-chapada-dos-veadeiros-4.jpg" alternative_text="Photo of dry river" caption="Dry river" author="Raniere Silva" licence="CC-BY" %}

{% include figure.html filename="2018-09-05-chapada-dos-veadeiros-5.jpg" alternative_text="Photo of bridge over small river" caption="Bridge over small river" author="Raniere Silva" licence="CC-BY" %}

Some rivers doesn't get complitely dry
and their water is cold,
great combination for the walk on a very warm day.

{% include figure.html filename="2018-09-05-chapada-dos-veadeiros-6.jpg" alternative_text="Photo of dry river" caption="Dry river" author="Raniere Silva" licence="CC-BY" %}

The Sete Quedas walk is classified as moderate.

{% include figure.html filename="2018-09-05-chapada-dos-veadeiros-7.jpg" alternative_text="Photo of dry river" caption="Dry river" author="Raniere Silva" licence="CC-BY" %}

We arrived at the camping site around 4pm
and could enjoy the river that isn't deep.

{% include figure.html filename="2018-09-05-chapada-dos-veadeiros-8.jpg" alternative_text="Photo of dry river" caption="Dry river" author="Raniere Silva" licence="CC-BY" %}

The sunset is around 6pm
and is something out of this world.

{% include figure.html filename="2018-09-05-chapada-dos-veadeiros-9.jpg" alternative_text="Photo of dry river" caption="Dry river" author="Raniere Silva" licence="CC-BY" %}

## Camping

The camping is very simple
and the only facility is the composting toilet.

{% include figure.html filename="2018-09-05-chapada-dos-veadeiros-camping.jpg" alternative_text="Photo of dry river" caption="Dry river" author="Raniere Silva" licence="CC-BY" %}

{% include figure.html filename="2018-09-05-chapada-dos-veadeiros-tent.jpg" alternative_text="Photo of dry river" caption="Dry river" author="Raniere Silva" licence="CC-BY" %}

The park authority request visitors to not make noise during the night
to not disturb the wild life
and **all garbage** must be take out with you.
