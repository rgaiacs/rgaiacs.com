---
layout: post
title: "Revisão de &quot;Programar é bom para as crianças? Uma visão crítica sobre o ensino de programação nas escolas&quot;"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>No periódico <a href="http://periodicos.letras.ufmg.br/index.php/textolivre/">Texto Livre: Linguagem e Tecnologia</a> foi publicado o artigo <a href="http://periodicos.letras.ufmg.br/index.php/textolivre/article/view/6143">Programar é bom para as crianças? Uma visão crítica sobre o ensino de programação nas escolas</a> de Wendell Bento Geraldes.</p>
<div class="admonition">
<p>Nota</p>
<p>O artigo está sob CC-BY-NC-ND então você pode ao menos lê-lo e compartilhá-lo sem medo.</p>
</div>
<div class="more">

</div>
<p>A conclusão do autor é que</p>
<blockquote>
<p>Respeitando-se os estágios de desenvolvimento intelectual das crianças, é possível incluir o ensino de programação de computadores nas escolas sem causar prejuízo nenhum ao aluno e trazer vários benefícios ao processo de ensino e aprendizagem de outras disciplinas.</p>
</blockquote>
<p>Isso me faz lembrar o que eu costumo sentir falta em textos das áreas de humanas: cadê dados concretos para comprovar sua afirmativa?</p>
<h1 id="comentários">Comentários</h1>
<blockquote>
<p>Recentemente, Linus Torvalds, o criador do sistema operacional Linux, disse em uma entrevista (LOVE, 2014): &quot;Na verdade, eu não acredito que todos devem necessariamente tentar aprender a programar [...]. Não é como saber ler e escrever e fazer contas básicas&quot;.</p>
</blockquote>
<p>Desenvolver projetos como o Linux, o motor de busca do Google, a linha do tempo do Facebook, ... não é para qualquer pessoa assim como escrever um romance não é para qualquer um.</p>
<p>O objetivo de aprender a programar é empoderar as pessoas a resolverem problemas simples do seu dia a dia, e.g. baixar todas os papeis de parede disponibilizados em uma página sem ter que clicar em cada um, da mesma forma como saber ler e escrever ajuda a resolver problemas cotidianos, e.g. deixar instruções para seu colega de trabalho porque você teve que sair mais cedo e não vai poder passar essas instruções verbalmente.</p>
<blockquote>
<p>O argumento de alguns especialistas da área de Tecnologia da Informação (TI) é o mesmo apresentado por Torvalds: para eles, a programação de computadores é algo que requer raciocínio lógico apurado e capacidade de resolver problemas com alto grau de complexidade, características que nem todas as pessoas possuem.</p>
</blockquote>
<p>Interpretação de texto é uma tarefa altamente complexa do ponto de vista computacional e mesmo assim ensinamos, ou tentamos, essa habilidade para as nossas crianças. Não deveríamos utilizar a complexidade de um disciplina para não ensiná-la a alguém.</p>
<blockquote>
<p>Nesse sentido, o ensino de programação nas escolas não produziria bons resultados, pois os desenvolvedores formados no ambiente escolar não seriam bem qualificados.</p>
<p>O mercado de TI seria então inundado de pessoas mal preparadas, e a qualidade dos serviços nessa área poderia ser prejudicada.</p>
</blockquote>
<p>Ensinar uma pessoa a ler para tomar conhecimento das notícias de sua cidade é uma tarefa completamente diferente de ensinar alguém a ler para julgar se uma obra merece ou não o nobel de literatura. Da mesma forma, ensinar alguém a programar para que ela possa resolver seus problemas diários é diferente de ensinar alguém a programar para que ela crie o próximo Google, Facebook, ...</p>
<p>Nosso objetivo não deve ser ensinar as crianças a programar como se isso fosse ser o futuro emprego delas.</p>
<blockquote>
<p>Segundo Setzer, a introdução precoce de computadores prejudica a infância e a juventude e pode causar desastres mentais nas pessoas que, por serem mentais, não podem ser vistos (SETZER, 2002).</p>
<p>Ainda segundo Setzer, &quot;Ao usar o computador, a criança é obrigada a exercer um tipo de pensamento que deveria empregar somente em idade mais avançada. Com isso podemos dizer que os computadores roubam das crianças sua necessária infantilidade. Elas são obrigadas a pensar e usar uma linguagem que deveria ser dominada apenas por adultos&quot; (SETZER, 2002).</p>
</blockquote>
<p>Uma época falavam que apender um segundo idioma muito cedo era ruim. Depois começaram a dizer que deve-se aprender um segundo (e terceiro) idioma o mais cedo possível.</p>
<blockquote>
<p>Muitos educadores acreditam nessa teoria e afirmam categoricamente que o ensino de programação prejudica a infância, por retirar delas a oportunidade de socialização com outras crianças através das brincadeiras e jogos próprios dessa fase.</p>
</blockquote>
<p>A diferença entre o remédio e o veneno está na dose. O que está sendo defendido não é que crianças saibam programar um mini Facebook aos dez anos mas que elas saibam que o Facebook não é uma bola de cristal da mesma forma como o telefone não é. Alguém ainda lembra de pegar duas latas, juntá-las por um fio e ficar brincando de telefone?</p>
<p>Além disso, programadores possuem a fama de serem anti-sociais o que não é necessariamente verdade. Muitos programadores adoram oportunidades de socializarem com seus colegas.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>none</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
