---
layout: post
title: Aniversários no BBDB e avisos no Org Mode
author: "Raniere Silva"
categories: ftb
tags: [ftb]
image:
  feature: ftb.jpg
---
O [BBDB](http://savannah.nongnu.org/projects/bbdb) é uma agenda de
contatos e o [Org Mode](http://orgmode.org/) é um gerenciador de
tarefas. Ambos funcionam no [Emacs](https://www.gnu.org/software/emacs/)
e nesse post vou mostrar como fazer o BBDB e o Org Mode conversarem para
que o aniverário dos seus contatos apareçam na sua agenda.

Configurando o BBDB
===================

Para abrir o BBDB você pode utilizar `M-x bbdb` e obterá a lista de seus
contatos no buffer.

![](/images/bbdb.png){width="80%"}

Mova o cursor para um dos seus contatos, pressione `i` para adicionar um
novo campo, digite `anniversary` como o nome do campo, e por último a
data do aniversário do seu contato no formato `AAAA-MM-DD` ou `MM-DD`.

<div class="admonition">

Aviso

Se essa é a primeira vez que você está adicionando esse campo o BBDB vai
lhe perguntar se você realmente quer criar o campo. **Confirme que
sim.**

</div>

Depois de adicionar o aniversário do seu contato essa informação será
mostrada no buffer.

![](/images/bbdb-with-anniversary.png){width="80%"}

Configurando o Org Mode
=======================

Em um dos seus arquivos Org Mode, adicione :

    * Anniversaries
    %%(org-bbdb-anniversaries)

<div class="admonition">

Aviso

`* Anniversaries %%(org-bbdb-anniversaries)` deve começar no início da
linha. O Org Mode tentará identar essa linha e você deve desfazer a
identação.

</div>

Visualizando os aniversários
============================

Utilize `C-c a a` para visualizar os aniversários.

![](/images/anniversaries.png){width="80%"}

Adicionando categorias
======================

Você pode utilizar categorias para filtar os aniversários dos outros
itens na agenda. Por exemplo, :

    * Anniversaries
      :PROPERTIES: 
      :CATEGORY: Aniversário
      :END:      
    %%(org-bbdb-anniversaries)

Quando você verificar os aniversários novamente terá

![](/images/anniversaries-cat.png){width="80%"}

Mais de um aniversário
======================

Depois de um aniversário você pode utilizar `C-q C-j` para adicionar um
segundo, terceiro, ... aniversário, por exemplo, o aniversário de
casamento.

Depois de adicionar o aniversário de casamento no BBDB você terá

![](/images/bbdb-with-wedding.png){width="80%"}

E ao visualizar sua agenda

![](/images/wedding.png){width="80%"}
