---
layout: post
title: "More than &quot;I can use your slides&quot;"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>A few days ago a friend of mine, Greg Wilson, wrote about <a href="http://third-bit.com/2014/10/27/shuttleworth.html">his Shuttleworth Foundation Fellowship Application</a>. Here are some of my thoughts after read Greg's application.</p>
<div class="more">

</div>
<h1 id="technology">Technology</h1>
<p>Greg's said that</p>
<blockquote>
<p>Open education shouldn't mean &quot;I can use your slides&quot;, but rather, &quot;We can all work together to make those slides (and exercises, and videos) better for everyone.&quot; The technical tools for doing this have been around for years; I think they can help educators just as much as they've helped programmers, but somebody has to get the ball rolling.</p>
</blockquote>
<p>I agree that we already have the software available for free (it is OPEN SOURCE) for a couple of years but I'm not sure about the technical structure. I don't know how was Canada or US some years ago but I'm almost sure that Brazil didn't have the structure for make this possible (computers isn't very cheap in Brazil neither internet connection) until a few years ago.</p>
<h1 id="adding-students-into-the-equation">Adding Students into the Equation</h1>
<p>I know that when Greg says &quot;We can all work together&quot; he include students but I would like to make it clean. Students can offer great feedback to the lesson and help improve it with side notes.</p>
<div class="note">
<div class="admonition-title">
<p>Note</p>
</div>
<p>Isn't new but I only discovery it last week and I want to do it early: <a href="http://fed.wiki.org/" class="uri">http://fed.wiki.org/</a>.</p>
</div>
<h1 id="doing-something-useful">Doing Something Useful</h1>
<p>Maybe engaging students in something more useful other than tests?</p>
<p>Most of the books that I used during my undergraduate was in English and this was a big problem for some of my colegues. I know that English is the lingua franca from our time but in some countries students didn't learn it. Maybe engaging students to translate good educational resources to their native languages?</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>academia,ciência aberta,educação,REA,Software Carpentry</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
