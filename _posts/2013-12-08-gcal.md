---
layout: post
title: GNU gcal
author: "Raniere Silva"
categories: ftb
tags: [ftb]
image:
  feature: ftb.jpg
---
[Gcal](http://www.gnu.org/software/gcal/) é um calendário para o
terminal com várias funcionalidade não presentes no `ncal` escrito por
Wolfqanq Helbiq para o FreeBSD que foi inspirado no `cal` do AT&T UNIX.

Instalação
==========

Debian:

    # apt-get install gcal

Fedora:

    # yum install gcal

Para outras distribuições pode ser necessário compilar o código fonte.

Básico
======

Em alguns pontos ele é compatível com o `cal`:

    $ gcal

        December 2013
     Su Mo Tu We Th Fr Sa
      1  2  3  4  5  6  7
      8  9 10 11 12 13 14
     15 16 17 18 19 20 21
     22 23 24 25 26 27 28
     29 30 31

Diário
======

Uma das funcionalidades do `gcal` que não estão presentes no `ncal` é o
uso de alguns arquivos para salvar compromissos.

O arquivo padrão de configuração do `gcal` é `~/.gcalrc` e nele você
pode adicionar compromissos da forma:

    yyyy[mm[dd|wwwn]] [ whitespace text ] newline

Também é possível incluir outros arquivos utilizando:

    #include "nome do arquivo"

Abaixo um pequeno exemplo:

    $ date
    Sun Dec  8 13:38:54 BRST 2013
    $ cat ~/.gcalrc
    20131208 Escrever post sobre o gcal
    $ gcal --list-of-fixed-dates

    Fixed date list:

    Sun, Dec   8th 2013: Escrever post sobre o gcal

O controle dos compromissos a serem listados é feito por opções passadas
ao `gcal`:

`-c`

:   Compromissos de hoje

`-ct`

:   Compromissos de amanhã

`-cw`

:   Compromissos da semana

`-cm`

:   Compromissos do mês

`-cy_`

:   Compromissos do ano

`-cdt@t7`

:   Compromissos de hoje, amanhã e da próxima semana

**Referências**
