---
layout: post
title: "Math Fonts at Firefox OS"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>When dealing with math you need to have at least one font that has all the symbols needed with support to scale it. With at least one font properly installed, when visit <a href="https://www.mozilla.org/en-US/styleguide/products/firefox-os/typeface/" class="uri">https://www.mozilla.org/en-US/styleguide/products/firefox-os/typeface/</a> you will see something like:</p>
<figure>
<img src="/images/test-with-font.png" alt="Screenshot of Firefox." class="align-center" style="width:80.0%" /><figcaption>Screenshot of Firefox.</figcaption>
</figure>
<p>Otherwise, you will see something like:</p>
<figure>
<img src="/images/test-without-font.png" alt="Screenshot of Firefox OS at Flatfish." class="align-center" style="width:80.0%" /><figcaption>Screenshot of Firefox OS at Flatfish.</figcaption>
</figure>
<p>Today I work trying to solve the <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=775060">lack of math fonts at Firefox OS</a>.</p>
<div class="more">

</div>
<h1 id="adding-latin-modern-math-font">Adding Latin Modern Math font</h1>
<p>Frédéric wrote a <a href="https://github.com/mozilla-b2g/moztt/pull/42">patch</a> to add Latin Modern Math font in the list of the fonts shipped at Firefox OS. I apply this patch in my source tree, build Firefox OS and flash Flatfish with my building. Using <code>adb</code> I check that <code>latinmodern-math.woff</code> can be found inside <code>/system/fonts</code> and go to test it. I open <span data-role="download">this file
&lt;font-family-fira.html&gt;</span> that use the system default font and next open <span data-role="download">this other file &lt;font-family-latinmodern.html&gt;</span> that has an CSS rule to use Latin Modern.</p>
<figure>
<img src="/images/font-family-fira.png" alt="Screenshot of first file (the one using default font)." class="align-center" style="width:80.0%" /><figcaption>Screenshot of first file (the one using default font).</figcaption>
</figure>
<figure>
<img src="/images/font-family-latinmodern.png" alt="Screenshot of second file (the one using Latin Modern font)." class="align-center" style="width:80.0%" /><figcaption>Screenshot of second file (the one using Latin Modern font).</figcaption>
</figure>
<p>If you look the text at the screenshot closely you will note that the font being use is different what imply that the CSS rule is working. The question that remains is &quot;Why Firefox OS don't render the &quot;image&quot; as an grid since this happens at Firefox?&quot;.</p>
<h1 id="screenshot-gallery-using-web-fonts">Screenshot Gallery using Web Fonts</h1>
<figure>
<img src="/images/flatfish-default.png" alt="Screenshot using default font." class="align-center" style="width:80.0%" /><figcaption>Screenshot using default font.</figcaption>
</figure>
<figure>
<img src="/images/flatfish-asana.png" alt="Screenshot using Asana font." class="align-center" style="width:80.0%" /><figcaption>Screenshot using Asana font.</figcaption>
</figure>
<figure>
<img src="/images/flatfish-latin-modern.png" alt="Screenshot using Latin Modern font." class="align-center" style="width:80.0%" /><figcaption>Screenshot using Latin Modern font.</figcaption>
</figure>
<figure>
<img src="/images/flatfish-stix.png" alt="Screenshot using STIX font." class="align-center" style="width:80.0%" /><figcaption>Screenshot using STIX font.</figcaption>
</figure>
<figure>
<img src="/images/flatfish-pagella.png" alt="Screenshot using Pagella font." class="align-center" style="width:80.0%" /><figcaption>Screenshot using Pagella font.</figcaption>
</figure>
<figure>
<img src="/images/flatfish-termes.png" alt="Screenshot using Termes font." class="align-center" style="width:80.0%" /><figcaption>Screenshot using Termes font.</figcaption>
</figure>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>MathML,TCP</p>
</div>
<div class="tags">
<p>MathML,TCP</p>
</div>
<div class="comments">

</div>
