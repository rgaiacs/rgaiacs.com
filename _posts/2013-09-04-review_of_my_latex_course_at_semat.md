---
layout: post
title: "Review of my LaTeX Course at SEMAT"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>In my last four post I write about the course of LaTeX that I teach this week. This is the last post about it and cover some topics that I didn't write about in the previous ones.</p>
<div class="more">

</div>
<h1 id="time">Time</h1>
<p>The course was composed of three days with one class of one hour and 15 minutes that start at 14:00pm.</p>
<p>IMHO, this wasn't the best time to start because most of the students still back from their lunch time and I lost at least 5 minutes waiting.</p>
<h1 id="structure">Structure</h1>
<p>The course was part of SEMAT, a regional conference for undergraduate students. At the same time of the course others seminars are running in others rooms. Maybe some students of the first day choose go to the seminar over the course.</p>
<h1 id="motivation">Motivation</h1>
<p>IMHO, some of the students aren't very motivate in the first day and I wasn't capable change it. This can be one of the reasons to the small numbers of students in the second and third day.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>educação,ensino,Software Carpentry</p>
</div>
<div class="tags">
<p>LaTeX</p>
</div>
<div class="comments">

</div>
