---
layout: post
title: "eLife Innovation Sprint 2020"
author: "Raniere Silva"
categories: event
tags: ["eLife", "sprint", "hackthon"]
image:
  feature: 2020-09-04-elife-sprint.jpg
  author: Dana DeVolk
  licence: Unsplash License
  original_url: https://unsplash.com/photos/BJAB9_yM2jU
---

Since 2018,
[eLife](https://elifesciences.org/) has been running a yearly [innovation sprint](https://sprint.elifesciences.org/),
focusing on projects and ideas that have the potential to transform how research is done and shared.
I want to participate in previous editions,
but 2020 was the first year where I was available
(thanks to COVID-19 pandemic).

The eLife staff and organisers,
especially Dr Emmy Tsang,
did a fabulous job to run the sprint online.
Participants used [QiqoChat](https://qiqochat.com)
as virtual venue
and
[Slack](https://slack.com)
as instant messaging provider.
This was the first time that I used QiqoChat
and
QiqoChat integration with Zoom meeting + Google Docs
solve the problems or difficulties to navigate a table of links.

My challenges during the sprint where
time zones
and
screen space.
The event was organised around 3 hours sessions
with 1 hour break between sessions.
This schedule helped participation from different time zones
and
promote the wellbeing of participants
but time zones still impose challenges because you were "late" for the party.
Regarding screen space,
I was limited to one screen
and
had to switch between the web browser, Zoom, Slack and my text editor often.

I contributed with
[Research References Tracking Tool (R2T2)](https://sprint.elifesciences.org/research-references-tracking-tool-r2t2/)
and
[Code is Science](https://sprint.elifesciences.org/code-is-science/).

## Research References Tracking Tool (R2T2)

R2T2 records the scientific artifacts
(for example, articles or data)
used when running a Python sript.
The main user case is to automate
the creation of reference pages for a academic publication.

My main contribution was to migrate the notes from the `README.md`
to Sphinx and publish in Read The Docs.

## Code is Science

Code is Science aimed to build a catalogue of ournal policies with regards to code artefacts.
Contributors had great discussions
that Yo Yehudi
covered in http://yo-yehudi.com/science/open-source/community/elife-sprint/2020/09/04/elife-sprint-2020.html.
I only joined at the end of the first day
and
sent a couple of pull requests
to improve their website.

## Final Words

I'm curious to know how project leaders
that participate in 2018 or 2019 edition
and 2020 edition
compare the in-person vs online event.
I think was a great opportunity to explore ideas
but
missed the social human interaction that is challenge to replicate online.