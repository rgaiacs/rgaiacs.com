---
layout: post
title: "Mathml September Meeting"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<figure>
<img src="/images/mathml2.jpg" class="align-center" style="width:60.0%" />
</figure>
<p>This is a report about the Mozilla MathML September Meeting. The topics of the meeting can be found <a href="https://etherpad.mozilla.org/mathml-2014-09">in this PAD</a> (<span data-role="download">local copy of the
PAD &lt;pad.txt&gt;</span>). This meeting happens all at <a href="http://appear.in/mozilla-mathml">appear.in</a> and because of that we don't have a log.</p>
<p>The next meeting will be in October 10th at 8pm UTC (note that October 10th is Friday, we change the day in our try make the meeting suitable to as many people possible). Please add topics in the <a href="https://etherpad.mozilla.org/mathml-2014-10">PAD</a>.</p>
<div class="more">

</div>
<h1 id="appear.in">APPEAR.IN</h1>
<figure>
<img src="/images/mathml1.jpg" class="align-center" style="width:60.0%" />
</figure>
<p>This is the second time that we try use WebRTC solutions for the meeting and the first one entirely done using it.</p>
<p>The currently issues with WebRTC are:</p>
<ul>
<li>require fast internet connection (sometimes we need turn off the camera),</li>
<li>the audio (and video) latency isn't small and</li>
<li>people need to use headsets (or turn off the mic when not talking).</li>
</ul>
<p>Although the issues we are going to continue trying WebRTC. Since we want that people use MathML we should use others standards.</p>
<h1 id="mediawiki">MediaWiki</h1>
<p>I talk about the equation reference issue at MediaWiki: you can't do equation reference at MediaWiki the same way that you do it at LaTeX. This issue is the biggest complain when I asked people why they didn't write more math MediaWiki. If you like to check how you can do equation reference please <a href="https://en.wikipedia.org/wiki/User_talk:Raniere_Silva">check this wiki page</a>.</p>
<p>You can do <a href="http://www.w3.org/TR/MathML3/chapter3.html#id.3.5.3.3">equation reference using MathML</a>.</p>
<h1 id="slow-render-of-mathml">Slow render of MathML</h1>
<p>The biggest bug that people worked on was <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=1043358">the regression in the time to render MathML</a>. This was reported by Bruce and fixed with a patch by James Kitchener.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>MathML,Mozilla</p>
</div>
<div class="tags">
<p>MathML meetings</p>
</div>
<div class="comments">

</div>
