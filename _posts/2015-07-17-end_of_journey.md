---
layout: post
title: "End of one Journey ..."
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>From the <span data-role="doc">last post &lt;graduation&gt;</span> you now that one journey is ending for me. This was one pleasure journey because if you <span data-role="doc">remember &lt;../15/journey&gt;</span> <strong>&quot;tthe journey will bring us happy, not the destination.&quot;</strong></p>
<p>At this post I will write about some of the people that I met during this journey since one of the things that makes your journey <strong>fantastic</strong> are the people with you.</p>
<div class="more">

</div>
<div class="admonition">
<p>Note</p>
<p>The list is more or less in chronological order when I first met them.</p>
</div>
<ul>
<li>I'm grateful to my parents and <a href="http://fgaia.com/">my brother</a> that participate in this journey even before it started.</li>
<li>Fernando Lucchesi and Caio Correia that were computer engineers freshman long time ago. We were neighbors in the first year --- many good stories from that time.</li>
<li>Ohana Rodrigues and Paula Casagrande that were freshman with me. I still remember my first class thanks to Ohana and all the experiments from the first course of physics laboratory thanks to Paula.</li>
<li>Lívia Ohana and Eric Lopes that were junior students when I began this journey and helped me until they graduate --- many good adventures.</li>
<li>Sandra Lee that was a statistic freshman when I began this journey and a example of perseverance.</li>
<li>Gabriel Natucci, Ivan Freitas and Pedro Pozzobon that with Fernando Lucchesi, Caio Correia and others went every Saturday night for dinner together.</li>
<li><a href="http://blog.panaggio.net/">Ricardo Panaggio</a>, <a href="http://sergiodj.net/http://sergiodj.net/">Sergio Durigan Junior</a> <a href="http://cascardo.info/">Thadeu Cascardo</a> and <a href="http://krisman.be/">Gabriel Krisman Bertazi</a> that with Ivan Freitas taught me soo much about <strong>free</strong> (not open) software.</li>
<li><a href="http://abelsiqueira.github.io/">Abel Siqueira</a> and Kally Chung that joined forces with me for crazy projects related to the laboratory that we maintained for two years.</li>
<li>Wanderson Luiz, Charles Martins and <a href="http://www.flaviobarros.net">Flávio Barros</a> that with Abel Siqueira get together for morning and afternoon coffee.</li>
<li>Luiz Izidoro that was one of the few people that attended 2013 edition of Document Freedom Day in Campinas and became a good friend.</li>
<li><a href="http://www.maths-informatique-jeux.com/blog/frederic/">Frédéric Wang</a>, or <a href="http://www.infogridpacific.com/blog/igp-blog-20131120-maths-in-ebooks-wang.html">Monsieur MathML</a>, that I hope to continue working together until people take MathML seriously.</li>
<li><a href="http://third-bit.com/">Greg Wilson</a> that was a mentor in the last two years although we only met over teleconference <strong>yet</strong> (maybe we will finally met in PyCon 2016?).</li>
</ul>
<h1 id="and-another-one-begins">... and another one begins</h1>
<p>So far, the only thing that I can disclose is that I'm looking for one position that allows me</p>
<ol type="1">
<li>keep programming in Python and learn others languages (e.g. Javascript, R, Haskell and C++);</li>
<li>make my code available under a free/open source license;</li>
<li>interact online with free/open source communities;</li>
<li>attend a few free/open source events each year;</li>
<li>advocate in favor of free/open things;</li>
<li>participate in free/open projects that teach people how to program;</li>
<li>and <strong>make the world a better place</strong>.</li>
</ol>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>pessoal</p>
</div>
<div class="tags">
<p>Peaceful Warrior,UNICAMP</p>
</div>
<div class="comments">

</div>
