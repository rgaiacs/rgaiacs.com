---
layout: post
title: "Math Virtual Keyboard Specifications - Version 2"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>This is a updated to the math virtual keyboard specifications.</p>
<div class="more">

</div>
<h1 id="layout">Layout</h1>
<p>When testing the keyboard I notice that is painful go to Symbol Layout to insert <code>$</code> that marks the start and end of (La)TeX math environments. To solve this I add another key to the keyboard.</p>
<figure>
<img src="/images/fxos-math-keyboard-4rows-v0.1a.svg" class="align-center" style="width:100.0%" />
</figure>
<h1 id="alternative-keys">Alternative Keys</h1>
<div class="admonition">
<p>Note</p>
<p>You can find a full list of Unicode characters and corresponding LaTeX command at <a href="http://milde.users.sourceforge.net/LUCR/Math/unimathsymbols.pdf" class="uri">http://milde.users.sourceforge.net/LUCR/Math/unimathsymbols.pdf</a>.</p>
</div>
<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Main Key</td>
<td>Alternative Key</td>
</tr>
<tr class="even">
<td>Symbol | Unicode | (La)TeX</td>
<td>Symbol | Unicode | (La)TeX</td>
</tr>
<tr class="odd">
<td>√ | 0221A | \sqrt{}</td>
<td>n√ | | \sqrt[n]{}</td>
</tr>
<tr class="even">
<td><dl>
<dt>&lt; | 0003C | &lt;</dt>
<dd><div class="line-block">        | +
        |
        | +
        |</div>
</dd>
</dl></td>
<td><blockquote>
<p>≤ | 02264 | \leq</p>
</blockquote>
<dl>
<dt>--------+---------+---------------------------------+</dt>
<dd><p>≦ | 02266 | \leqq</p>
</dd>
<dt>--------+---------+---------------------------------+</dt>
<dd><p>≪ | 0226A | \ll</p>
</dd>
</dl></td>
</tr>
<tr class="odd">
<td><dl>
<dt>&gt; | 0003E | &gt;</dt>
<dd><div class="line-block">        | +
        |
        | +
        |</div>
</dd>
</dl></td>
<td><blockquote>
<p>≥ | 02265 | \geq</p>
</blockquote>
<dl>
<dt>--------+---------+---------------------------------+</dt>
<dd><p>≧ | 02267 | \geqq</p>
</dd>
<dt>--------+---------+---------------------------------+</dt>
<dd><p>≫ | 0226B | \gg</p>
</dd>
</dl></td>
</tr>
<tr class="even">
<td><dl>
<dt>= | 0003D | =</dt>
<dd><div class="line-block">        | +
        |
        | +
        |
        | +
        |</div>
</dd>
</dl></td>
<td><blockquote>
<p>≡ | 02261 | \equiv</p>
</blockquote>
<dl>
<dt>--------+---------+---------------------------------+</dt>
<dd><p>≠ | 02260 | \neq</p>
</dd>
<dt>--------+---------+---------------------------------+</dt>
<dd><p>~ | 0223C | \sim</p>
</dd>
<dt>--------+---------+---------------------------------+</dt>
<dd><p>≈ | 02248 | \approx</p>
</dd>
</dl></td>
</tr>
<tr class="odd">
<td><code>|</code> | 0007C | <code>|</code></td>
<td>‖ | 02016 | \|</td>
</tr>
<tr class="even">
<td><dl>
<dt>∫ | 0222B | \int</dt>
<dd><div class="line-block">        | +
        |
        | +
        |
        | +
        |
        | +
        |
        | +
        |
        | +
        |</div>
</dd>
</dl></td>
<td><blockquote>
<p>∬ | 0222C | \iint</p>
</blockquote>
<dl>
<dt>--------+---------+---------------------------------+</dt>
<dd><p>∭ | 0222D | \iint</p>
</dd>
<dt>--------+---------+---------------------------------+</dt>
<dd><p>∮ | 0222E | \oint</p>
</dd>
<dt>--------+---------+---------------------------------+</dt>
<dd><p>∯ | 0222F | \oiint</p>
</dd>
<dt>--------+---------+---------------------------------+</dt>
<dd><p>∰ | 02230 | \oiiint</p>
</dd>
<dt>--------+---------+---------------------------------+</dt>
<dd><p>∲ | 02232 | \varointclockwise</p>
</dd>
<dt>--------+---------+---------------------------------+</dt>
<dd><p>∳ | 02233 | \ointctrclockwise</p>
</dd>
</dl></td>
</tr>
<tr class="odd">
<td><dl>
<dt>( | 00028 | (</dt>
<dd><div class="line-block">        | +
        |
        | +
        |
        | +
        |
        | +
        |
        | +
        |</div>
</dd>
</dl></td>
<td><blockquote>
<p>[ | 0005b | [</p>
</blockquote>
<dl>
<dt>--------+---------+---------------------------------+</dt>
<dd><p>{ | 0007b | \{</p>
</dd>
<dt>--------+---------+---------------------------------+</dt>
<dd><p>⟨ | 027E8 | \langle</p>
</dd>
<dt>--------+---------+---------------------------------+</dt>
<dd><p>⟪ | 027EA | \lang</p>
</dd>
<dt>--------+---------+---------------------------------+</dt>
<dd><p>⌈ | 02308 | \lceil</p>
</dd>
<dt>--------+---------+---------------------------------+</dt>
<dd><p>⌊ | 0230A | \lfloor</p>
</dd>
</dl></td>
</tr>
<tr class="even">
<td><dl>
<dt>) | 00029 | )</dt>
<dd><div class="line-block">        | +
        |
        | +
        |
        | +
        |
        | +
        |
        | +
        |</div>
</dd>
</dl></td>
<td><blockquote>
<p>] | 0005c | ]</p>
</blockquote>
<dl>
<dt>--------+---------+---------------------------------+</dt>
<dd><p>} | 0007c | \}</p>
</dd>
<dt>--------+---------+---------------------------------+</dt>
<dd><p>⟩ | 027E9 | \rangle</p>
</dd>
<dt>--------+---------+---------------------------------+</dt>
<dd><p>⟫ | 027EB | \rang</p>
</dd>
<dt>--------+---------+---------------------------------+</dt>
<dd><p>⌉ | 02309 | \rceil</p>
</dd>
<dt>--------+---------+---------------------------------+</dt>
<dd><p>⌋ | 0230B | \rfloor</p>
</dd>
</dl></td>
</tr>
<tr class="odd">
<td><dl>
<dt>a b | | \begin{matrix}\end{matrix}</dt>
<dd><div class="line-block">        | +
        |
        | +
        |
        | +
        |
        | +
        |
        | +
        |</div>
</dd>
</dl></td>
<td><blockquote>
<p>(a b) | | \begin{pmatrix}\end{pmatrix}</p>
</blockquote>
<dl>
<dt>--------+---------+---------------------------------+</dt>
<dd><p>[a b] | | \begin{bmatrix}\end{bmatrix}</p>
</dd>
<dt>--------+---------+---------------------------------+</dt>
<dd><p>{a b} | | \begin{Bmatrix}\end{Bmatrix}</p>
</dd>
<dt>--------+---------+---------------------------------+</dt>
<dd><p>|a b| | | \begin{vmatrix}\end{vmatrix}</p>
</dd>
<dt>--------+---------+---------------------------------+</dt>
<dd><p>‖a b‖ | | \begin{Vmatrix}\end{Vmatrix}</p>
</dd>
<dt>--------+---------+---------------------------------+</dt>
<dd><p>{a | | \begin{cases}\end{cases}</p>
</dd>
</dl></td>
</tr>
<tr class="even">
<td><dl>
<dt>x̂ | 00302 | \hat{x}</dt>
<dd><div class="line-block">        | +
        |
        | +
        |
        | +
        |
        | +
        |
        | +
        |
        | +
        |
        | +
        |
        | +
        |
        | +
        |</div>
</dd>
</dl></td>
<td><blockquote>
<p>x̌ | 0030C | \check{x}</p>
</blockquote>
<dl>
<dt>--------+---------+---------------------------------+</dt>
<dd><p>x̀ | 00300 | \grave{x}</p>
</dd>
<dt>--------+---------+---------------------------------+</dt>
<dd><p>x́ | 00303 | \acute{x}</p>
</dd>
<dt>--------+---------+---------------------------------+</dt>
<dd><p>x̃ | 00303 | \tilde{x}</p>
</dd>
<dt>--------+---------+---------------------------------+</dt>
<dd><p>x̄ | 00304 | \bar{x}</p>
</dd>
<dt>--------+---------+---------------------------------+</dt>
<dd><p>x̆ | 00306 | \breve{x}</p>
</dd>
<dt>--------+---------+---------------------------------+</dt>
<dd><p>x̊ | 0030A | \mathring{x}</p>
</dd>
<dt>--------+---------+---------------------------------+</dt>
<dd><p>ẋ | 00307 | \dot{x}</p>
</dd>
<dt>--------+---------+---------------------------------+</dt>
<dd><p>ẍ | 00308 | \ddot{x}</p>
</dd>
<dt>--------+---------+---------------------------------+</dt>
<dd><p>x⃑ | 020D1 | \vec{x}</p>
</dd>
</dl></td>
</tr>
<tr class="odd">
<td><dl>
<dt>ℕ | 02115 | \mathbb{N}</dt>
<dd><div class="line-block">        | +
        |
        | +
        |
        | +
        |
        | +
        |
        | +
        |
        | +
        |
        | +
        |</div>
</dd>
</dl></td>
<td><blockquote>
<p>ℤ | 02124 | \mathbb{Z}</p>
</blockquote>
<dl>
<dt>--------+---------+---------------------------------+</dt>
<dd><p>ℚ | 0211A | \mathbb{Q}</p>
</dd>
<dt>--------+---------+---------------------------------+</dt>
<dd><p>ℝ | 0211D | \mathbb{R}</p>
</dd>
<dt>--------+---------+---------------------------------+</dt>
<dd><p>ℂ | 02102 | \mathbb{C}</p>
</dd>
<dt>--------+---------+---------------------------------+</dt>
<dd><p>ℙ | 02119 | \mathbb{P}</p>
</dd>
<dt>--------+---------+---------------------------------+</dt>
<dd><p>ℍ | 0210D | \mathbb{H}</p>
</dd>
<dt>--------+---------+---------------------------------+</dt>
<dd><p>ℜ | 0211C | \Re</p>
</dd>
<dt>--------+---------+---------------------------------+</dt>
<dd><p>ℑ | 02111 | \Im</p>
</dd>
</dl></td>
</tr>
<tr class="even">
<td><dl>
<dt>← | 02190 | \leftarrow</dt>
<dd><div class="line-block">        | +
        |
        | +
        |
        | +
        |
        | +
        |</div>
</dd>
</dl></td>
<td><blockquote>
<p>↚ | 0219A | \nleftarrow</p>
</blockquote>
<dl>
<dt>--------+---------+---------------------------------+</dt>
<dd><p>⟵ | 027F5 | \longleftarrow</p>
</dd>
<dt>--------+---------+---------------------------------+</dt>
<dd><p>⇐ | 021D0 | \Leftarrow</p>
</dd>
<dt>--------+---------+---------------------------------+</dt>
<dd><p>⇍ | 021CD | \nLeftarrow</p>
</dd>
<dt>--------+---------+---------------------------------+</dt>
<dd><p>⟸ | 027F8 | \Longleftarrow</p>
</dd>
</dl></td>
</tr>
<tr class="odd">
<td><dl>
<dt>↔ | 02194 | \leftrightarrow</dt>
<dd><div class="line-block">        | +
        |
        | +
        |
        | +
        |
        | +
        |</div>
</dd>
</dl></td>
<td><blockquote>
<p>↮ | 021AE | \nleftrightarrow</p>
</blockquote>
<dl>
<dt>--------+---------+---------------------------------+</dt>
<dd><p>⟷ | 027F7 | \longleftrightarrow</p>
</dd>
<dt>--------+---------+---------------------------------+</dt>
<dd><p>⇔ | 021D4 | \Leftrightarrow</p>
</dd>
<dt>--------+---------+---------------------------------+</dt>
<dd><p>⇎ | 021CE | \nLeftrightarrow</p>
</dd>
<dt>--------+---------+---------------------------------+</dt>
<dd><p>⟺ | 027FA | Longleftrightarrow</p>
</dd>
</dl></td>
</tr>
<tr class="even">
<td><dl>
<dt>→ | 02192 | \rightarrow</dt>
<dd><div class="line-block">        | +
        |
        | +
        |
        | +
        |
        | +
        |</div>
</dd>
</dl></td>
<td><blockquote>
<p>↛ | 0219B | \nrightarrow</p>
</blockquote>
<dl>
<dt>--------+---------+---------------------------------+</dt>
<dd><p>⟶ | 027F6 | \longrightarrow</p>
</dd>
<dt>--------+---------+---------------------------------+</dt>
<dd><p>⇒ | 021D2 | \Rightarrow</p>
</dd>
<dt>--------+---------+---------------------------------+</dt>
<dd><p>⇏ | 021CF | \nRightarrow</p>
</dd>
<dt>--------+---------+---------------------------------+</dt>
<dd><p>⟹ | 027F9 | \Longrightarrow</p>
</dd>
</dl></td>
</tr>
</tbody>
</table>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>GSOC</p>
</div>
<div class="tags">
<p>GSOC2014</p>
</div>
<div class="comments">

</div>
