---
layout: post
title: "Wieliczka Salt Mine"
author: "Raniere Silva"
categories: travel
tags: ["Poland", "Krakow", "Mine"]
image:
  feature: 2019-04-18-krakow.jpg
  author:  Raniere Silva
  licence: CC-BY
---

I visited Krakow, Poland last weekend
and my good friend Piotr went with me to visit Wieliczka Salt Mine.

{% include figure.html filename="2019-04-18-krakow-1.jpg" alternative_text="Salt statues" caption="Salt statues" author="Raniere Silva" licence="CC-BY" %}

{% include figure.html filename="2019-04-18-krakow-2.jpg" alternative_text="Salt" caption="Salt on the walls" author="Raniere Silva" licence="CC-BY" %}

{% include figure.html filename="2019-04-18-krakow-3.jpg" alternative_text="Church" caption="Salt Church inside the mine" author="Raniere Silva" licence="CC-BY" %}

{% include figure.html filename="2019-04-18-krakow-4.jpg" alternative_text="Altar" caption="Salt altar" author="Raniere Silva" licence="CC-BY" %}

{% include figure.html filename="2019-04-18-krakow-5.jpg" alternative_text="Lake" caption="Salt lake" author="Raniere Silva" licence="CC-BY" %}

{% include figure.html filename="2019-04-18-krakow-6.jpg" alternative_text="Anotherlake" caption="Another salt lake" author="Raniere Silva" licence="CC-BY" %}

{% include figure.html filename="2019-04-18-krakow-7.jpg" alternative_text="Entry of last tunnel" caption="I at the deepest point of the tour" author="Raniere Silva" licence="CC-BY" %}