---
layout: post
title: "Encontro Comunitário do LibrePlanet São Paulo"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Durante o FISL14 o LibrePlanet São Paulo realizou um encontro comunitário.</p>
<figure>
<img src="/images/encontro_comunitario_lp_br_sp_0.jpg" class="align-center" style="width:80.0%" />
</figure>
<p>O encontro iniciou-se com o Cascardo falando sobre os monopólios intelectual, também conhecidos como propriedade intelectual, (mostrando a diferença entre patentes, marcas registradas e direito autoral) e posteriormente aprofundando-se na lei de direito autoral brasileira. A palestra do Cascardo foi fabulosa e adorei a renomeação de termos que ele fez.</p>
<figure>
<img src="/images/encontro_comunitario_lp_br_sp_1.jpg" class="align-center" style="width:80.0%" />
</figure>
<p>Depois da apresentação sobre monopólios intelectuais, Sergio falou um pouco de software livre. Sua fala não foi muito longa pois muito já tinha sido falado sobre o tema e ele precisava poupar sua voz para a sua palestra que seria horas depois.</p>
<figure>
<img src="/images/encontro_comunitario_lp_br_sp_2.jpg" class="align-center" style="width:80.0%" />
</figure>
<p>Depois do Sergio, houve uma debate sobre redes sociais (privacidade e alternativas) e compartilhamento de arquivos (alternativas, privacidade, e software como um serviço).</p>
<figure>
<img src="/images/encontro_comunitario_lp_br_sp_3.jpg" class="align-center" style="width:80.0%" />
</figure>
<p>Ainda gostaríamos de ter falado sobre outros assuntos como Android e Firefox OS mas o tempo acabou antes.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>FISL</p>
</div>
<div class="tags">
<p>FISL14</p>
</div>
<div class="comments">

</div>
