---
layout: post
title: "Boot Restrito, a Solução Falsa e a Real"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Essa palestra foi ministrada pelo Alexandre Oliva. Logo no início da palestra eu me assustou fazendo acreditar que a máquina dele, um Lemote Yeeloong, tinha dado problema mas era apenas uma descrição dos problemas com <a href="http://en.wikipedia.org/wiki/Unified_Extensible_Firmware_Interface">UEFI</a>.</p>
<figure>
<img src="/images/boot_restrito_0.jpg" class="align-center" style="width:80.0%" />
</figure>
<p>Para quem não sabe o que é boot restrito (também chamado de boot seguro, secure boot e UEFI) fica um pequeno resumo:</p>
<blockquote>
<p>É uma especificação implementada na BIOS que permite ela iniciar apenas sistemas operacionais assinados digitalmente com uma chave armazenada na memória não volátil da placa-mãe.</p>
</blockquote>
<p>O boot restrito foi empurrado pela Microsoft no lançamento do Windows 8 de forma que esse sistema operacional só roda em máquinas compatíveis com o boot restrito. O problema com o boot restrito são:</p>
<ul>
<li>a assinatura digital só pode ser feita pelo fabricante, i.e., o usuário não pode adicionar sua própria chave;</li>
<li>possibilidade de em breve ser removido a opção de desabilitar o boot restrito;</li>
<li>dificuldade em alguns aparelhos de acessar a BIOS.</li>
</ul>
<figure>
<img src="/images/boot_restrito_1.jpg" class="align-center" style="width:80.0%" />
</figure>
<p>O Oliva lembrou a todos da tivoização, prática que deve seu nome a <a href="http://en.wikipedia.org/wiki/Tivo">TiVo</a> devido a prática de bloquear o usuário de rodar uma versão modificada do software (alguns consideram como o avó do boot restrito). Essa prática da TiVo ficou muito famosa pela TiVo utilizar software licenciado com GPL (versão 2) e ter arrumado uma forma de limitar a liberdade do usuário. Na GPL versão 3 já existe uma clausula que impossibilita a prática da tivoização.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>FISL</p>
</div>
<div class="tags">
<p>FISL14</p>
</div>
<div class="comments">

</div>
