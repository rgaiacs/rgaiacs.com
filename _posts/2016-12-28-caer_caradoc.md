---
layout: post
title: "Caer Caradoc"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Caer Raradoc is close to Church Stretton.</p>
<p><img src="/images/caer_caradoc01.jpg" alt="image" class="align-center" style="width:80.0%" /></p>
<div class="more">

</div>
<p><img src="/images/caer_caradoc02.jpg" alt="image" class="align-center" style="width:80.0%" /></p>
<p><img src="/images/caer_caradoc03.jpg" alt="image" class="align-center" style="width:80.0%" /></p>
<p><img src="/images/caer_caradoc04.jpg" alt="image" class="align-center" style="width:80.0%" /></p>
<p><img src="/images/caer_caradoc05.jpg" alt="image" class="align-center" style="width:80.0%" /></p>
<p><img src="/images/caer_caradoc06.jpg" alt="image" class="align-center" style="width:80.0%" /></p>
<p><img src="/images/caer_caradoc07.jpg" alt="image" class="align-center" style="width:80.0%" /></p>
<p><img src="/images/caer_caradoc08.jpg" alt="image" class="align-center" style="width:80.0%" /></p>
<p><img src="/images/caer_caradoc09.jpg" alt="image" class="align-center" style="width:80.0%" /></p>
<p><img src="/images/caer_caradoc10.jpg" alt="image" class="align-center" style="width:80.0%" /></p>
<p><img src="/images/caer_caradoc11.jpg" alt="image" class="align-center" style="width:80.0%" /></p>
<p><img src="/images/caer_caradoc12.jpg" alt="image" class="align-center" style="width:80.0%" /></p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>none</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
