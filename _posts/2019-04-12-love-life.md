---
layout: post
title: "101 Screts For Your Twenties: Love life"
author: "Raniere Silva"
categories: book
tags: ["101 Screts For Your Twenties"]
image:
  feature: 2019-04-12-love-life.jpg
  author:  Mike Marquez
  licence: Unsplash License
  original_url: https://unsplash.com/photos/dotlmAbHmq0
---

You can't have a book called "101 Secrets For Your Twenties"
without a single secret to help people on their love life.
Paul Angone wrote a couple of chapters about dating and marriage
and I will cover both on the same blog post.

Paul wrote

> A date is a short period of time with another human being
> getting to know ech other's story.
> Then you go home.
> Don't make a date into something more than a date.

which is the first mistake of many people.

Later,
Paul said

> you can't really define what you're looking for in a guy or girl if you haven't spent time getting to know them.
> Each date gives you a better understnding of the complexity of your taste.

And close to the end of the book,
Paul advises about The One:

> But wheter there is such a thing as The One
> is really not that important to me.
> What's important is that you make a wise
> decision when deciding if this is the right one for you.
>
> (...)
>
> So instead of searching for this magical creature called **The One**,
> I propose we should be on the hunt for **The Four**---the four different relationships
> that need to be intertwined within the **Right One**.

The four different relationships are

1. Best Friend
2. Lover
3. Business Partner
4. Wartime Ally