---
layout: post
title: "João Pessoa"
author: "Raniere Silva"
categories: travel
tags: ["João Pessoa", "Paraíba", "Brazil"]
image:
  feature: 2020-01-27-joao-pessoa.jpg
  author: Raniere Silva
  licence: CC-BY
---

I visited [João Pessoa](https://en.wikipedia.org/wiki/Jo%C3%A3o_Pessoa,_Para%C3%ADba)
and I like the city,
at least [Manaíra](https://goo.gl/maps/NeR2YJKtMRuzaZBZ9) where I stayed.
You have a good number of restaurants nearby
and Beach Road isn't crowded.

{% include figure.html filename="2020-01-27-joao-pessoa-2.jpg" alternative_text="Photo of Ponta do Seixas" caption="Ponta do Seixas." author="Raniere Silva" licence="CC-BY" %}

I also went to [Ponta do Seixas](https://en.wikipedia.org/wiki/Ponta_do_Seixas),
the easternmost point of the American continents.