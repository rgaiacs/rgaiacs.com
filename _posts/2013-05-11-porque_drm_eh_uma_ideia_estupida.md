---
layout: post
title: "Porque DRM é uma ideia estupida"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Algumas semanas atrás, Andrew Ducker publicou em seu LiveJournal (para quem não conhece pense como se fosse um blog) o artigo <a href="http://andrewducker.livejournal.com/2885549.html">Why DRM is such a fucking stupid idea</a> (&quot;Porque DRM é uma idéia muito estúpida&quot;, em tradução literal).</p>
<p>Para quem não sabe DRM ou &quot;Digital Rights Management&quot; (&quot;Gerenciador Digital de Direitos&quot; em tradução literal) é uma ferramenta utilizada para impedir a cópia de materiais protegidos pelo Direito Autoral. Todos os grandes serviços de conteúdo digital utilizam o DRM e a seguir você descobrirá porque deve evitar comprar conteúdo protegido por DRM.</p>
<div class="more">

</div>
<p>Gostei muito do artigo a a seguir você irá encontrar uma tradução do mesmo para o português.</p>
<blockquote>
<p>A editora de quadrinhos digitais JManga está fechando. Daqui a duas semanas você não será mais capaz de &quot;comprar&quot; nada deles. Daqui a dois meses, qualquer coisa que você tenha &quot;comprado&quot; deles deixará de existir. Se você tiver gasto centenas de reais na loja deles, então, você teve muita má sorte.</p>
<p>É isso que o DRM faz. Ele faz você dependente de uma autoridade central quando você deseja acessar coisas pela qual você pagou. Isso significa que se uma companhia fechar as portas, ou apenas perder o interesse de continuar com o serviço, então você perdeu o acesso a TUDO que tiver comprado deles.</p>
<p>Isso pode acontecer com empresas de qualquer tamanho. Quando o Google descobriu quer o YouTube era mais bem sucessido que seu próprio site de vídeos, eles simplemente desligaram o Google Video. E depois tiveram que ser forçados a retornar o pagamento de algumas pessoas por vídeos que eles não mais poderiam mostrar. Quando a Microsoft desligou MSN Music TODAS as músicas que foram compradas deles não mais puderam ser ouvidas. Essas companias sugerem que as pessoas possuam uma cópia de segurança para que possam não ser processadas judicialmente.</p>
<p>Quando for comprar/contratar um produto/serviço de música/vídeo fique de olhos bem abertos. Eu contratei o serviço básico de um mês do Spotify (para quem não conhece, é um serviço de música ) porque eu vejo ele da mesma forma que vejo minha assinatura de TV a cabo - estou pagando pelo acesso, não estou comprando nada. Mas se você quiser manter algo por muito tempo e que isso funcione da maneira como você gostaria que fosse, então compre APENAS se for DRM FREE (i.e., livre de DRM).</p>
</blockquote>
<h1 id="como-drm-funciona">Como DRM funciona</h1>
<p>Uma das maneiras que o DRM atua é utilizando criptografia. Para fins de exemplo, considere que você deseja ler o pouco famoso livro &quot;FOO BAR&quot; cujo enorme conteúdo é</p>
<blockquote>
<p>Este livro é protegido por Direitos Autorais.</p>
<p>Ao adquirir essa obra o detentor dos Direitos Autorais permite a você, comprar, APENAS ler seu conteúdo uma única vez sendo proibida a a leitura dessa obra por terceiros.</p>
<p>A revenda, transferência e doação são proibidas.</p>
</blockquote>
<p>Ao &quot;comprar&quot; o livro &quot;FOO BAR&quot; com DRM o que você recebe é algo do tipo :</p>
<pre><code>jA0EAwMCn/3tIah370dgycAdjgL9/4slCAk2Yn4d+ivZgrLC6XZg6yIkFJ6APUN2
N3KSpsyCfW+zIFY8LDuHzvvVQf9f4ueyFoeyrrUnCdElbXW2v1/wvNLVRYLcjKDY
uX/kFxZtn8jFlqadUgbbItqj2aT0c2GPVUOaAelKBxQBoCh7/RXTmmB3Tmd53v8j
zMC7/22Ez4NrTt6LPybc5hrkWza+l3bC51K+9wiszwA7X1AbLarOzG+KGb2v/jod
wXLKO2UPRViQ2u7KmiJprJOu2C4UmpApuqKmtCCx8Fs7a+UWlguhREalU9Dvwjg=
=IT/J</code></pre>
<p>Que quando processado pelo equipamento correto associado a conta do seu usuário, saberá converter sua compra no texto que você deseja ler. Se por algum motivo você não mais possuir o equipamento ou sua conta (ou o vendedor resolver fechar as portas) você não mais será capaz de ler o livro que você comprou.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>none</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
