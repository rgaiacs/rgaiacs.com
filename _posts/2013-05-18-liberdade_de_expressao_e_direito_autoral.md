---
layout: post
title: "Liberdade de expressão e direito autoral"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<div class="note">
<div class="admonition-title">
<p>Note</p>
</div>
<p>Este post é uma homenagem a <a href="http://pt.wikipedia.org/wiki/Tiradentes">Tiradentes</a> cuja morte, no Brasil, é comemorada em 21 de abril.</p>
</div>
<div class="note">
<div class="admonition-title">
<p>Note</p>
</div>
<p>Meus mais sinceros agradecimentos ao <a href="http://dgp.cnpq.br/buscaoperacional/detalheest.jsp?est=4692036102522136">Matheus Guimarães Mello</a> por me indicar a obra que serviu de inspiração para este post.</p>
</div>
<p>Essa semana comecei a ler &quot;Coding Freedom: The Aesthetics and the Ethics of Hacking&quot; de Gabriella Coleman (se tiver interesse a autora disponibiliza uma cópia eletrónica da obra em sua <a href="http://gabriellacoleman.org/">página pessoal</a> tanto em <a href="http://gabriellacoleman.org/Coleman-Coding-Freedom.pdf">pdf</a> como em <a href="http://gabriellacoleman.org/Coleman-Coding-Freedom.epub">epub</a> que é licenciado sob CC BY-NC-ND 2.5).</p>
<p>O trecho do texto de Coleman que me inspirou a escrever este post encontra-se na Introdução e começa assim</p>
<blockquote>
<p>While the two-hundred-year history of intellectual property has long been freighted with controversies over the scope, time limits, and purpose of various of its instruments (Hesse 2002; Johns 2006, 2010; McGill 2002), legal scholars have only recently given serious attention to the uneasy coexistence between free speech and intellectual property principles (McLeod 2007; Netanel 2008; Nimmer 1970; Tushnet 2004). Copyright law, in granting creators significant control over the reproduction and circulation of their work, limits the deployment of copyrighted material in other expressive activity, and consequently censors the public use of certain forms of expressive content. Legal scholar Ray Patterson (1968, 224) states this dynamic eloquently in terms of a clash over the fundamental values of a democratic society: “A society which has freedom of expression as a basic principle of liberty restricts that freedom to the extent that it vests ideas with legally protected property interests.”</p>
</blockquote>
<p>Para mim, a parte mais importante é a fala de Patterson no final.</p>
<div class="more">

</div>
<p>A vida em sociedade deve ter como raiz o respeito mútuo entre os indivíduos. Isso não é fácil pois temos dificuldade de entender e respeitas pessoas que são fisicamente e/ou intelectualmente diferentes de nós.</p>
<p>Uma vez considerado o respeito mútuo entre os indivíduos, deveríamos pensar sobre nossa</p>
<ul>
<li>Privacidade e</li>
<li>Liberdade.</li>
</ul>
<p>É importante que a privacidade tenha precedência sobre a liberdade para que essa não viole a privacidade. Em relação a liberdade, vou me reter a liberdade de expressão, que para mim é atualmente violada pela lei de direito autoral:</p>
<blockquote>
<p>For example, copyright, which grants authors significant control over their expression of ideas, was initially limited to fourteen years with one opportunity for renewal. Today, the copyright term in the United States has ballooned to the length of the author’s life plus seventy years, while works for hire get ninety-five years, regardless of the life of the author. The original registration requirement has also been eliminated. Most any expression—a scribble on a piece of paper, a blog post, or a song—automatically qualifies for protection, so long as it represents the author’s creation.</p>
</blockquote>
<p>Embora algumas pessoas maravilhosas, e.g., <a href="http://pt.wikipedia.org/wiki/Richard_Matthew_Stallman">Richard Matthew Stallman</a> e <a href="http://pt.wikipedia.org/wiki/Lawrence_Lessig">Lawrence Lessig</a>, tenham sido capazes de utilizar a lei de direito autoral para &quot;restaurar&quot; a liberdade de expressão com o que hoje é conhecido como <a href="http://pt.wikipedia.org/wiki/GNU_General_Public_License">GPL (ou GNU General Public License)</a> e <a href="http://pt.wikipedia.org/wiki/Creative_Commons">CC (ou Creative Commons</a>, licenças copyleft, a liberdade de expressão continua sendo violada pela existência das leis de direito autoral.</p>
<p>Enquanto lutamos e aguardamos pelo fim da lei de direito autoral ficamos com &quot;LIBERTAS QUÆ SERA TAMEN&quot; (muitas vezes traduzido como &quot;Liberdade ainda que tardia&quot;) em mente.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>none</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
