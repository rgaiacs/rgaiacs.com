---
layout: post
title: Compressão de PDF
author: "Raniere Silva"
categories: ftb
tags: [ftb]
image:
  feature: ftb.jpg
---
Muitas vezes temos um arquivo PDF cujo tamanho é muito grande e
precisamos enviá-lo por algum meio eletrônico. Neste caso gostaríamos de
comprimir esse arquivo sem perder muita qualidade.

Uma opção seria utilizar os programas `zip`, `gzip`, `bzip2` ou
similares. Infelizmente ao utilizar essa abordagem não conseguimos
reduzir o tamanho do arquivo pdf. A solução é utilizar o `gv` ou
ghostscript.

Ao procurar na internet você muito provavelmente encontrará o comando:

    gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/screen -dNOPAUSE -dQUIET -dBATCH -sOutputFile=arquivo_saida.pdf arquivo_entrada.pdf

A *flag* que controla a qualidade do pdf gerado é `-dPDFSETTINGS` e os
possíveis valores são:

-   `/screen`: para resolução baixa,
-   `/ebook`: para resolução média,
-   `/printer`: para qualidade de impressão (alta),
-   `/prepress`: para qualidade de pré-impressão,
-   `/default`: padrão.

Abaixo, encontra-se uma comparação entre os valores acima informados.

-   original.pdf &lt;original.pdf&gt; - 4.4M
-   screen.pdf &lt;screen.pdf&gt; - 212K
-   default.pdf &lt;default.pdf&gt; - 620K
-   ebook.pdf &lt;ebook.pdf&gt; - 720K
-   printer.pdf &lt;printer.pdf&gt; - 1.1M
-   prepress.pdf &lt;prepress.pdf&gt; - 1.9M
