---
layout: post
title: "Install Fest"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Na última quarta-feira, 22/04/2015, um colega me chamou para ajudar em um install fest.</p>
<div class="more">

</div>
<h1 id="organização">Organização</h1>
<p>A organização do install fest, i.e. locação de sala, divulgação, inscrições, coffee break, ... foram feitas pela <a href="http://secomp.com.br/">SECOMP</a>. Ocorreram alguns atrasos, em parte por culpa da organização, mas nada que comprometesse o install fest.</p>
<h1 id="público">Público</h1>
<p>O público eram alunos ingressantes no curso de Ciência da Computação e Engenharia da Computação da UNICAMP. Apareceram, aproximadamente, 10 alunos para instalar GNU/Linux nas suas máquinas sendo que a grande maioria queria manter o Windows.</p>
<h1 id="preparação">Preparação</h1>
<p>Para um install fest, espera-se</p>
<ol type="1">
<li>que a organização tenha disponível várias mídias de distribuições GNU/Linux para serem instaladas e</li>
<li>que os participantes tenham feito backup dos dados existentes nas suas máquinas.</li>
</ol>
<p>Os participantes fizeram a parte deles mas a organização fez o dever de casa parcialmente uma vez que existiam apenas DVD de instalação e boa parte dos participantes possuíam uma máquina sem leitor de DVD.</p>
<p>O número de pendrives disponível era muito inferior ao número de participantes e ninguém da organização tinha a ISO salva no seu computador o que atrasou muito as atividades.</p>
<h1 id="reparticionamento">Reparticionamento</h1>
<p>Para evitar problemas é aconselhável fazer o reparticionamento utilizando o Windows. Nessa parte não aconteceu grandes problemas.</p>
<h1 id="instalação">Instalação</h1>
<p>Assim que os participantes tiveram acesso a uma mídia da distribuição que queriam instalar (me lembro de Debian, Fedora, Arch) foi possível dar continuidade às atividades e eles conseguiram instalar a distribuição GNU/Linux desejável.</p>
<h1 id="configuração">Configuração</h1>
<p>Alguns participantes utilizam o netinst do Debian para instalação e estes tiveram grandes problemas para configurar o acesso à internet, que só estava disponível através da <a href="https://pt.wikipedia.org/wiki/Eduroam">eduroam</a>, e consequentemente para instalar os principais pacotes que precisavam que só foi possível refazendo a instalação utilizando o DVD de instalação.</p>
<p>Me recordo também que pelo menos um participante não tinha o driver para que sua placa de rede funciona-se e que até o final do install fest esse problema não tinha sido resolvido.</p>
<h1 id="dual-boot">Dual Boot</h1>
<p>O dual boot foi, <a href="blog.sergiodj.net/post/2013-04-01-relato-installfest-unesp-unicamp/#install-fest-missao-unicamp">mais uma vez</a>, o calcanhar de Aquiles do install fest. Depois da instalação da distribuição GNU/Linux o <a href="https://pt.wikipedia.org/wiki/Boot">bootloader</a> não listava o Windows. Várias pessoas ficaram tentando arrumar isso mas sem sucesso até o final do install fest.</p>
<div class="admonition">
<p>Nota</p>
<p>Conversando com algumas pessoas, me parece que as informações importantes para instalar uma distribuição GNU/Linux com dual boot sem ter problemas encontram-se disponíveis em <a href="https://wiki.archlinux.org/index.php/Windows_and_Arch_dual_boot#Important_information" class="uri">https://wiki.archlinux.org/index.php/Windows_and_Arch_dual_boot#Important_information</a>. Infelizmente eu não tenho uma máquina para testar.</p>
</div>
<h1 id="conclusões">Conclusões</h1>
<p>No final do install fest foram contabilizados apenas 3 instalações bem sucedidas das quais eu me recordo de ter acompanhado apenas uma send que foi bem sucedida porque o participante queria sua máquina <strong>apenas</strong> com GNU/Linux.</p>
<p>Minha conclusão é que fazer um install fest precisa ser preparada com muito cuidado, ver minha sugestão no final deste post, pois caso contrário você estará propagando a falácia de que GNU/Linux não presta.</p>
<h1 id="extra-como-organizar-um-install-fest">Extra: Como Organizar um Install Fest</h1>
<div class="admonition">
<p>Atenção</p>
<p>Essas são apenas ideias de como organizar um Install Fest para que ele seja bem sucedido. <strong>Elas nunca foram testadas</strong>.</p>
</div>
<ol type="1">
<li><p>Conseguir uma máquina para testar a instalação com dual boot.</p>
<p><strong>Se você não conseguir uma máquina de teste você deve aceitar apenas pessoas que desejarem uma máquina apenas com GNU/Linux ou cancelar o install fest por completo.</strong></p></li>
<li><p>Testar a instalação com dual boot.</p>
<p><strong>Se você não conseguir fazer a instalação com dual boot de forma satisfatório você deve aceitar apenas pessoas que desejarem uma máquina apenas com GNU/Linux ou cancelar o install fest por completo.</strong></p></li>
<li>Encontre um lugar adequado (vários pontos de energia e internet cabeada) e uma data que esse lugar esteja disponível.</li>
<li>Encontre alguém que possa lhe ajudar na organização para resolver problemas de última hora e caso tenha um coffee break que possa recebê-lo.</li>
<li>Encontre colegas que possam lhe ajudar com a instalação do GNU/Linux. Cada colega consegue auxiliar <strong>no máximo</strong> duas instalações ao mesmo tempo e uma instalação demora aproximadamente duas horas.</li>
<li><p>Prepare um formulário de inscrição para os participantes que contenha as seguintes informações:</p>
<ul>
<li>Nome</li>
<li>Contato (email ou telefone)</li>
<li>Modelo da máquina na qual será instalado o GNU/Linux</li>
<li>Distribuição desejada</li>
<li>Se deseja dual boot com Windows</li>
</ul>
<p>As vagas devem ser limitadas com base no número de pessoas que você conseguir para lhe ajudar. E.g.</p>
<ol type="1">
<li>Se você conseguir 3 pessoas para lhe ajudar por duas horas <strong>permita a inscrição de apenas 3 * 2 * (2/2) = 6 pessoas</strong>.</li>
<li>Se você conseguir 4 pessoas para lhe ajudar por seis horas <strong>permita a inscrição de apenas 4 * 2 * (6/2) = 24 pessoas</strong>.</li>
</ol>
<p>Também aconselho que seja cobrada uma taxa de inscrição, R$20,00, para custear a compra de pendrives que serão entregues aos participantes no dia com a distribuição que eles desejam já salva.</p></li>
<li>Salvar as distribuições GNU/Linux desejadas em pelo menos uma mídia por participante.</li>
<li>Consiga adesivos das distribuições GNU/Linux e programas livres. Várias pessoas gostam de adesivos.</li>
<li>Tentar se divertir no install fest.</li>
</ol>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>install fest,LPBRSP,software livre</p>
</div>
<div class="tags">
<p>UNICAMP,IC,SECOMP</p>
</div>
<div class="comments">

</div>
