---
layout: post
title: "Software Patents And What To Do About Them"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>&quot;Software Patents And What To Do About Them&quot; foi uma das palestras apresentadas por <a href="http://debnicholson.com/about/">Deb Nicholson</a> (ligada a FSF) e <a href="http://webmink.com/about/">Simon Phipps</a> (presidente da Open Source Iniciative).</p>
<figure>
<img src="/images/software_patents_0.jpg" class="align-center" style="width:80.0%" />
</figure>
<p>Nessa palestra foi apresentado a origem das patentes cujo objetivo era incentivar a geração de novos conhecimentos e sua disseminação e que hoje não é mais satisfeita.</p>
<p>Também foi dito que atualmente é impossível estar 100% protegido contra um processo por violação de patentes mas existe algumas coisas que podem ser feito:</p>
<ul>
<li>Utilizar licenças modernas como a GPLv3 que já possui clausulas sobre violação de patentes,</li>
<li>Utilização de formatos abertos pois existem acordos relacionados a esses formatos que impedem seus desenvolvedores de criar processos relacionados aos formatos,</li>
<li>Afiliar-se a organizações que assinaram acordos para não criar processos contra outros membros e que também possuem repositórios de patentes.</li>
</ul>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>FISL</p>
</div>
<div class="tags">
<p>FISL14</p>
</div>
<div class="comments">

</div>
