---
layout: post
title: "Mindscan"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Mindscan é o título do livro que estou lendo no momento e foi escrito por Robert J. Sawyer.</p>
<div class="more">

</div>
<h1 id="resumo">Resumo</h1>
<p>O livro conta a história de Jake Sullivan, um jovem que possui uma doença hereditária sem tratamento que pode matá-lo ou deixá-lo em estado vegetativo. Devido a essa sua doença, Jake tenta não se aventurar muito.</p>
<p>Já adulto, Jake vai para o lançamento realizado por uma empresa que está vendendo corpos artificiais e a transferência da consciência do cliente para o corpo artificial. Jake resolve &quot;contratar&quot; o serviço e assim começa a história.</p>
<h1 id="liberdade">Liberdade</h1>
<p>Existe um trecho interessante relacionado com as liberdades civis:</p>
<blockquote>
<p>I can see the Statue of Liberty from my office window — but they should rename the old girl the Statue of Do Exactly What the Government Says You Should Do.&quot; He shook his head. &quot;That's why I uploaded, see? Not too many of my generation left — people who actually remember what it was like to have civil liberties, before Homeland Security, before Littler v. Carvey, before every dollar bill and retail product had an RFID tracking chip in it. If we let the good old days pass from living memory, we'll never be able to get them back.&quot;</p>
</blockquote>
<h1 id="copyright">Copyright</h1>
<p>Existe um trecho, um pouco longo, relacionado com direito autoral:</p>
<blockquote>
<p>&quot;So, I've watched the ebb and flow of copyright legislation over my lifetime. It's been a battle between warring factions: those who want works to be protected forever, and those who believe works should fall into public domain as fast as possible. When I was young, works stayed in copyright for fifty years after the authors' death. Then it was lengthened to seventy years, and that's still the current figure, but it isn't long enough.&quot;</p>
<p>&quot;Why?&quot;</p>
<p>&quot;Well, because if I had a child today — not that I could — and I died tomorrow — not that I'm going to — that child would receive the royalties from my books until he or she was seventy. And then, suddenly, my child — by that point, an old man or woman — would be cut off; my work would be declared public domain, and no more royalties would ever have to be paid on it. The child of my body would be denied the benefits of the children of my mind. And that's just not right.&quot;</p>
<p>&quot;But, well, isn't the culture enriched when material goes into the public domain?&quot; I asked. &quot;Surely you wouldn't want Shakespeare or Dickens to still be protected by copyright?&quot;</p>
<p>&quot;Why not? J.K. Rowling is still in copyright; so is Stephen King and Marcos Donnelly — and they all have had, and continue to have, a huge impact on our culture.&quot;</p>
<p>&quot;I guess…&quot; I said, still not sure.</p>
<p>&quot;Look,&quot; said Karen, gently, &quot;one of your ancestors started a brewing company, right?&quot;</p>
<p>I nodded. &quot;My great grandfather, Reuben Sullivan — Old Sully, they called him.&quot;</p>
<p>&quot;Right. And you benefit financially from that to this day. Should the government instead have confiscated all the assets of Sullivan Brewing, or whatever the company's called, on the seventieth anniversary of Old Sully's death? Intellectual property is still property, and it should be treated the same as anything else human beings build or create.&quot;</p>
<p>I had a hard time with this; I never used anything but open-source software — and there was a difference between a building and an idea; there was, in fact, a material difference. &quot;So you uploaded in order to make sure you keep getting royalties on DinoWorld forever?&quot;</p>
<p>&quot;It's not just that,&quot; Karen said. &quot;In fact, it's not even principally that. When something falls into public domain, anyone can do anything with the material. You want to make a porno film with my characters? You want to write bad fiction featuring my characters? You can, once my works go into public domain. And that's not right; they're mine.&quot;</p>
<p>&quot;But by living forever, you can protect them?&quot; I said.</p>
<p>&quot;Exactly. If I don't die, they never fall into public domain.&quot;</p>
</blockquote>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>livros</p>
</div>
<div class="tags">
<p>Mindscan,Robert J. Sawyer</p>
</div>
<div class="comments">

</div>
