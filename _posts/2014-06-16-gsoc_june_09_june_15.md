---
layout: post
title: "GSoC: June 09 - June 15"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>This is one of the reports about my GSoC project and cover the forth week of “Students coding”.</p>
<p>At this forth week I start working at the Input method editors (IME). Bellow you will find more details about the past week and the plans for this one.</p>
<div class="more">

</div>
<h1 id="building-firefox-os">Building Firefox OS</h1>
<p>After some problems to build Firefox OS <a href="https://discourse.mozilla-community.org/t/building-your-own-image-rom-for-flatfish/525">I finally do it</a>. This was a important step to start working with the <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=947654">lack of fonts with math support</a>.</p>
<h1 id="alternative-keys">Alternative Keys</h1>
<p>I wrote a <span data-role="doc">list of the alternatives keys &lt;../14/math_alternative_keys&gt;</span> to keep track of the symbols present at the keyboard.</p>
<h1 id="non-linear-input">&quot;Non-linear&quot; Input</h1>
<p>One important feature is the &quot;non-linear&quot; input, i.e., change the cursor position based on the pressed key. I start work on it and record a video show it (<span data-role="download">720p &lt;demo720.mp4&gt;</span> or <span data-role="download">360p &lt;demo360.mp4&gt;</span>).</p>
<p>I still thinking if use an <a href="http://vim-latex.sourceforge.net/documentation/latex-suite/latex-macros.html#place-holder">place holder</a> or an heuristic (e.g. moving to next <code>}</code>).</p>
<h1 id="plans-for-june-16---june-22">Plans for June 16 - June 22</h1>
<ul>
<li>Improve the &quot;non-linear&quot; input.</li>
<li>Work at the font issue.</li>
</ul>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>GSOC</p>
</div>
<div class="tags">
<p>GSOC2014</p>
</div>
<div class="comments">

</div>
