---
layout: post
title: "MathML May Meeting"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>This is a report about the Mozilla MathML May IRC Meeting (see the announcement <a href="https://groups.google.com/d/msg/mozilla.dev.tech.mathml/suM2tZjLNXA/QtLsU_reqroJ">here</a>). The topics of the meeting can be found <a href="https://etherpad.mozilla.org/mathml-2014-05">in this PAD</a> (<span data-role="download">local copy of the PAD &lt;pad.txt&gt;</span>) and the <a href="http://logs.glob.uno/?c=mozilla%23mathml&amp;s=15+May+2014&amp;e=15+May+2014">IRC log</a> (<span data-role="download">local copy of the IRC log &lt;irc.txt&gt;</span>) is also available.</p>
<p>In the last 5 weeks the MathML team closed 23 bugs, worked in others 6 and open 7 new ones (this are only the ones tracked by Bugzilla).</p>
<p>The next meeting will be in June 12th at #mathml IRC channel. Please add topics in the <a href="https://etherpad.mozilla.org/mathml-2014-06">PAD</a>.</p>
<div class="more">

</div>
<h1 id="issue-with-time-zone">Issue with Time Zone</h1>
<p>We had a small issue with time zone. At <a href="https://etherpad.mozilla.org/mathml-2014-05">this meeting pad</a> we had the meeting schedule to 1pm UTC but Google Calendar doesn't have this option and I used &quot;(GMT+00:00) Londres&quot; and unfortunately Google Calendar has Daylight Saving Time (DST) correction enabled what already <a href="https://productforums.google.com/forum/#!topic/calendar/y6uaiAdZrIg">make troubles for other users</a>. For the next meeting I plan to use <a href="http://blog.gerv.net/2012/11/the-rekjavik-trick/">Iceland for the time zone</a> to avoid DST.</p>
<h1 id="mathui-2014">MathUI 2014</h1>
<p>The <a href="http://cermat.org/events/MathUI/14/">MathUI 2014 9th Workshop on Mathematical User Interfaces</a> had the deadline for submissions extended to May 22nd 2014.</p>
<p>I and Frédéric will submit a paper very soon and will happy to have feedbacks based on our <a href="http://fred-wang.github.io/MathUI2014/paper/output/index.html">pre-print</a>.</p>
<p>Others members of the team already submit their papers:</p>
<ul>
<li><a href="http://arxiv.org/abs/1404.6179">Mathoid: Robust, Scalable, Fast and Accessible Math Rendering for Wikipedia</a> by Moritz Schubotz and Gabriel Wicke</li>
<li><a href="http://arxiv.org/abs/1404.6519">Digital Repository of Mathematical Formulae</a> by Howard S. Cohl, Marjorie A. McClain, Bonita V. Saunders, Moritz Schubotz and Janelle C. Williams</li>
</ul>
<h1 id="new-version-of-latexml">New version of LaTeXML</h1>
<p><a href="https://github.com/brucemiller/LaTeXML/releases/tag/v0.8.0">Version 0.8.0 of LaTeXML</a> was released last week and is already available in many package manager systems. You can help testing it (especially on Windows).</p>
<p>If you use <a href="https://www.tug.org/texworks/">TeXworks</a> as your LaTeX IDE <a href="https://github.com/brucemiller/LaTeXML/wiki/Integrating-LaTeXML-into-TeX-editors#texworks">there is instructions to setup LaTeXML</a>.</p>
<p>And if you use <a href="http://www.lyx.org/">LyX</a>, one WYSIWYG IDE for LaTeX, there is a <a href="https://github.com/brucemiller/LaTeXML/issues/487">small bug</a> to fix before you can use it.</p>
<h1 id="mathjax-native-mathml">MathJax Native MathML</h1>
<p><a href="https://addons.mozilla.org/en-US/firefox/addon/mathjax-native-mathml/?src=search">MathJax Native MathML</a> is a addon for Firefox wrote by Frédéric Wang that makes MathJax keep MathML. Frédéric updated the addon this month and you can help (a) testing and (b) <a href="https://github.com/fred-wang/Mathzilla/tree/master/mathjax-native-mathml/locale">translating a few strings</a>.</p>
<h1 id="texzilla">TeXZilla</h1>
<p>Frédéric added to new functions to <a href="https://github.com/fred-wang/TeXZilla">TeXZilla</a>: <code>toImage</code> and <code>toImageWebGL</code>.</p>
<h1 id="mathml-accessibility">MathML Accessibility</h1>
<p>Jonathan Wei, Accessibility Team intern, gave a talk (<a href="https://air.mozilla.org/mathml-accessability/">available at Air Mozilla</a>) about MathML Accessibility. For more technical details please view the wiki page <a href="https://wiki.mozilla.org/Accessibility/MathML">Accessibility/MathML</a> and the <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=916419">Bug 916419</a>.</p>
<h1 id="gecko">Gecko</h1>
<p>The biggest improve since last meeting was the support to OpenType MATH table in gecko 31. <a href="http://blogs.msdn.com/b/murrays/archive/2014/04/27/opentype-math-tables.aspx">MurrayS3 explain OpenType MATH table at MSDN Blog</a>:</p>
<blockquote>
<p>&quot;The OpenType math tables were designed to enable programs to reproduce the elegant math typography of TeX.&quot;</p>
</blockquote>
<p>Thanks, in alphabetic order, to Cameron McCormack, David Baron, Frédéric Wang, John Daggett, Jonathan Kew, Karl Tomlinson and Robert O'Callahan for the work to support OpenType MATH table.</p>
<p>Others contributions was:</p>
<ul>
<li><a href="https://bugzilla.mozilla.org/show_bug.cgi?id=992127">LaTeX-to-MathML in Thunderbird</a> by Frédéric Wang;</li>
<li><a href="https://bugzilla.mozilla.org/show_bug.cgi?id=914031">Implement menclose notation &quot;phasorangle&quot;:</a> by Branko Krznaric, Aniket Deshpande and Anuj Agarwal;</li>
<li><a href="https://bugzilla.mozilla.org/show_bug.cgi?id=467729">Add font autoinstallation support</a> by Frédéric Wang;</li>
<li>Others fixes/patch review/bug report by, in alphabetic order, Iwz, James Kitchener, Khaled Hosny, Ryan VanderMeulen and <a href="mailto:dbarton@mathscribe.com">dbarton@mathscribe.com</a>.</li>
</ul>
<div class="admonition">
<p>Note</p>
<p>If I forgot to give you the credits for your work please let me know and sorry about it.</p>
</div>
<h1 id="documentation">Documentation</h1>
<p>Frédéric Wang updated MDN to cover all the improvements.</p>
<p>And Frédéric's CSS fallback for MathML <a href="https://github.com/mozilla/kuma/pull/2196">was merge at kuma</a>.</p>
<h1 id="gsoc">GSoC</h1>
<p>GSoC start this Monday. More updates in the future.</p>
<h1 id="fisl">FISL</h1>
<p>FISL is one the bigger FOSS event in Brazil and happened last week. I gave a <span data-role="doc">talk about MathML &lt;../11/fisl15_mathml&gt;</span> where I showed some of Frédéric's demos. The attendees, most K-12 educators, love the demos and want tools that they can use to create similar content.</p>
<h1 id="how-to-help---newcorners">How to help - newcorners</h1>
<p>Please add yourself to <a href="https://wiki.mozilla.org/MathML:Home_Page#The_Team" class="uri">https://wiki.mozilla.org/MathML:Home_Page#The_Team</a>.</p>
<ul>
<li>Test, use, write tutorials, report bugs, send patches, ... to LaTeXML.</li>
<li>Test, use, report bugs, send patches (including translations), ... to MathJax Native MathML addon.</li>
<li>Translate part of MathML documentation to your mother language. You can find more information in <a href="https://developer.mozilla.org/en-US/docs/MDN/Doc_status/MathML#Localizations">this documentation status page</a>.</li>
</ul>
<h1 id="how-to-help">How to help</h1>
<ul>
<li>The patch for MathML in Wikipedia by Moritz Schubotz still need some review (you will find some update at the <a href="http://logs.glob.uno/?c=mozilla%23mathml&amp;s=15+May+2014&amp;e=18+May+2014#c2949">begin of our meeting log</a>). If you know PHP please take a look at <a href="https://gerrit.wikimedia.org/r/#/projects/mediawiki/extensions/Math,dashboards/default" class="uri">https://gerrit.wikimedia.org/r/#/projects/mediawiki/extensions/Math,dashboards/default</a>.</li>
<li>Improve the documentation: <a href="https://developer.mozilla.org/en-US/docs/MDN/Doc_status/MathML#Dev-doc-needed_bugs" class="uri">https://developer.mozilla.org/en-US/docs/MDN/Doc_status/MathML#Dev-doc-needed_bugs</a> and <a href="https://developer.mozilla.org/en-US/docs/MDN/Doc_status/MathML#Documentation_requests" class="uri">https://developer.mozilla.org/en-US/docs/MDN/Doc_status/MathML#Documentation_requests</a></li>
</ul>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>MathML,Mozilla</p>
</div>
<div class="tags">
<p>MathML meetings</p>
</div>
<div class="comments">

</div>
