---
layout: post
title: "Florianópolis - Lagoa da Conceição"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Este post é sobre minha visita à Lagoa da Conceição em Florianópolis.</p>
<figure>
<img src="/images/porto.jpg" class="align-center" style="width:25.0%" />
</figure>
<div class="more">

</div>
<h1 id="agradecimentos">Agradecimentos</h1>
<p>Antes de mais nada devo agradecer ao Abel e Kally que tornaram a estadia em Florianópolis muito agradável.</p>
<figure>
<img src="/images/abel_e_kally.jpg" class="align-center" style="width:25.0%" />
</figure>
<p>Em segundo lugar, devo agradecer ao João e Salete, o casal que conhecemos no retorno do passeio que proporcionaram um maravilhoso tour nas praias próximos a Lagoa da Conceição.</p>
<figure>
<img src="/images/joao_e_salete.jpg" class="align-center" style="width:25.0%" />
</figure>
<h1 id="passeio-de-barco">Passeio de Barco</h1>
<p>O passeio de barco é barato, R$15,00 (incluindo ida e volta), e dura aproximadamente 40 minutos até um dos últimos pontos de parada e mais 40 minutos para retornar ao ponto de partida.</p>
<figure>
<img src="/images/vista_barco.jpg" class="align-center" style="width:25.0%" />
</figure>
<p>A vista do entorno da lagoa é muito bonita e em alguns dos pontos de parada encontram-se alguns restaurantes.</p>
<h1 id="cachoeira">Cachoeira</h1>
<p>No passeio, fomos até uma das cachoeiras existentes.</p>
<figure>
<img src="/images/cachoeira.jpg" class="align-center" style="width:25.0%" />
</figure>
<p>Devido as poucas chuvas existia pouca água correndo mas a piscina estava cheia.</p>
<h1 id="praias">Praias</h1>
<p>Próximo da barra da lagoa, onde encontra-se o cais para o passeio de barco existem duas praias: Praia da Joaquina e Praia Mole.</p>
<figure>
<img src="/images/praias.jpg" class="align-center" style="width:50.0%" />
</figure>
<h1 id="mirante">Mirante</h1>
<p>Na proximidades da lagoa existe pelo menos dois mirantes.</p>
<figure>
<img src="/images/mirante.jpg" class="align-center" style="width:25.0%" />
</figure>
<p>A vista de ambos os mirantes é maravilhosa dando para ver boa parte da lagoa.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>cidades</p>
</div>
<div class="tags">
<p>Florianópolis</p>
</div>
<div class="comments">

</div>
