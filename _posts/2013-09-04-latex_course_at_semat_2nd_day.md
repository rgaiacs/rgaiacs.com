---
layout: post
title: "LaTeX Course at 2nd Day of Semat"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Yesterday was the second day of my course about LaTeX that I talk (<span data-role="doc">in this post &lt;../01/latex_course&gt;</span>). Around 10 students attend it and you will find more information about that below.</p>
<div class="more">

</div>
<p>I try to follow the plan with the pads (<span data-role="download">one for the contents
&lt;semat-latex2.txt&gt;</span> and <span data-role="download">other for feedbacks
&lt;../02/semat-feedback-latex.txt&gt;</span>) and the rounds since no students say anything about it:</p>
<dl>
<dt>Round 2.1</dt>
<dd><p>A very quick review of the first day since one students say that I should spend more time with the GUI of TeXworks.</p>
</dd>
<dt>Round 2.2</dt>
<dd><p>Talk about the inline math mode of LaTeX.</p>
</dd>
<dt>Round 2.3</dt>
<dd><p>Talk about some mathematics commands of LaTeX. Some of this commands are symbols.</p>
</dd>
<dt>Round 2.4</dt>
<dd><p>Talk about the display math mode of LaTeX.</p>
</dd>
<dt>Round 2.5</dt>
<dd><p>Show how to use cross-reference feature of LaTeX.</p>
</dd>
</dl>
<h1 id="pad-server-in-manutention">Pad server in manutention</h1>
<p>In the first day we use <a href="https://pad.riseup.net/p/semat-latex">this pad</a> from Riseup. In the second day, before we start the class, Riseup seem to be in manutention and we have to use <a href="https://pad.riseup.net/p/semat-latex">another pad</a>.</p>
<p>If we are using other service that a Etherpad instance it will be impossible to share the codes.</p>
<h1 id="tex-live-and-miktex">TeX Live and MiKTeX</h1>
<p>Most of the students are using MiKTeX. The <code>texdoc</code> from MikTeX is difference to <code>texdoc</code> from TeX Live and :</p>
<pre><code>$ texdoc symbols</code></pre>
<p>not work in the first one. This students must have to use :</p>
<pre><code>$ texdoc comprehensive</code></pre>
<p>This is a good example of how difference versions of a software can give troubles to instructors.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>educação,ensino,Software Carpentry</p>
</div>
<div class="tags">
<p>LaTeX</p>
</div>
<div class="comments">

</div>
