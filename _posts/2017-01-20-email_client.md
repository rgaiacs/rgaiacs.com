---
layout: post
title: Seeking a email client
author: "Raniere Silva"
categories: ftb
tags: [ftb]
image:
  feature: ftb.jpg
---
In 2009, one of my friends introduced me to [Vim](http://www.vim.org/)
and its power to edit text files. After a few months I was so happy
using Vim that I couldn't write my emails on the poor web email client
[input box](https://www.w3.org/wiki/HTML/Elements/input) so the same
friend that introduced me to Vim told me about
[Mutt](http://www.mutt.org/).

I copied some dotfiles from the web, edited them and after a few hours
(maybe days) I could check and reply my emails using Mutt and Vim. If
life was a fairy tail it will end here!

To be updated with some of the last cool new stuff on the open source
world I subscribed to a few mailing list and some times I wanted to
reply to one email but never remeber which email I used to subscribe to
that mailing list. I received all my email in one inbox but I used
different addresses that were configured to forward the income email to
my main box. After searh on the internet I discovered that I could use :

    send-hook mailing-list@foo.net "set from=my-email@bar.net"

to Mutt set up my correct email address when writing the email to one
list and I end up with one configuration file where I keep a record of
all the mailing lists that I subscribed.

In 2015, I discovered [org mode](http://orgmode.org/) that is a great
tool to "keep notes, maintain TODO lists, planning projects, ..."
**but** can only be used on Emacs. I know there is some projects that
try to emulate org mode on other environments but they aren't complete
as the Emacs implementation. I started using Emacs because of org mode
and I decided to give a try to Gnus &lt;http://www.gnus.org/&gt;, one
email client on Emacs.

After more than one year using Emacs, there is a few features that I
like on it but there is some thing that I'm missing, probably because
the way to have it is to implemente them yourself in Lisp.

And since most of my time now I spend dealing with emails the lack of a
good tool to manage my emails started to bother me too much. So here is
the list of things thaat I want on my next email client.

Integrated
==========

The email client need to talk with a address book and a TODO list
management. And the address book need to talk with the TODO list
management because I want to have my contact birthdays on my TODO list.

GPG Support
===========

I want to start using GPG again!

Mailing list management
=======================

The email client need to know which email it need to use when I write a
email to the mailing list X.

Provide ways to unsubscribe to a mailing list is a plus.

Contact list
============

The address book need to support [contact
list](https://help.gnome.org/users/evolution/3.4/contacts-using-contact-lists.html.en),
know as
[mail-alias](https://www.emacswiki.org/emacs/BbdbMailingLists#toc1) on
[BBDB](http://savannah.nongnu.org/projects/bbdb/), to help me remember
who I need to email when I saw something interesting.

Time tracking
=============

The TODO list management should be capable of track how much time I
spend on my tasks and provide me with some statistics.

Possibility of integration with Google Calendar, Trello or Wunderlist is
a plus.

Easy to configure
=================

Configuration should be very easy since I don't want to spend days
looking on the web how to tailor it.

Bonus: Contact sync with Android phone
======================================

I'm **REALLY** missing the syncronization between my address book on my
computer and my mobile phone.

Candidates
==========

If no one know one **open source** email client how can suit my needs I
will try to test [Evolution](https://wiki.gnome.org/Apps/Evolution) and
[Kontact](https://userbase.kde.org/Kontact).

If Evolution and Kontact fail to make me happy I will probably need to
learn Lisp and implement what I need on Emacs.

Candidates not short listed
===========================

-   [Thunderbird](https://www.mozilla.org/en-US/thunderbird/)

    GPG support requires a
    [plugin](https://addons.mozilla.org/en-GB/thunderbird/addon/enigmail/).

    Mailing list management requires another
    plugin &lt;https://addons.mozilla.org/en-GB/thunderbird/addon/mailing-list-manager/&gt;\_
    that is too old, last release was on May, 2010.

-   [Claws](http://www.claws-mail.org/)

    I believe it doesn't have a TODO list management.

-   [Geary](https://wiki.gnome.org/Apps/Geary)

    I believe it doesn't have a TODO list management.

-   [Nylas Mail](https://nylas.com/)

    You need to configure a server to use with your client!

-   [Opera Mail](http://www.opera.com/computer/mail)

    I believe it doesn't have a TODO list management.
