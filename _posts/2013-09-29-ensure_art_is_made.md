---
layout: post
title: "Como garantir a produção de conteúdo"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<div class="note">
<div class="admonition-title">
<p>Note</p>
</div>
<p>Este post foi baseado no artigo &quot;<a href="http://falkvinge.net/2013/09/28/the-question-was-never-how-do-we-make-sure-artists-are-paid-it-was-always-how-do-we-ensure-art-is-made-and-available/">The question was never 'How do we make sure artists are paid'. It was always 'How do we ensure art is made and avaliable'</a> de Rick Falkvinge (<span data-role="download">cópia nesse servidor
&lt;ensure_art_is_made.html&gt;</span>).</p>
</div>
<p>No seu texto, Falkvinge cita o trecho da constituição dos Estados Unidos que trata do direito autoral cuja justificativa é</p>
<blockquote>
<p>…to promote the progress of science and the useful arts…</p>
</blockquote>
<p>Isso é, &quot;para promover o progresso da ciência e das artes úteis&quot; (em tradução literal). Note que em nenhum momento a justificativa é garantir o recebimento de dinheiro por parte do autor ou seu representante.</p>
<div class="more">

</div>
<p>Por esse motivo, ao repensar a lei de direito autoral deve-se tentar resolver os seguintes problemas:</p>
<ul>
<li>Como garantir que novos conteúdos são produzidos, e</li>
<li>Como garantir que esse conteúdo esteja disponível para o maior público possível.</li>
</ul>
<p>NÃO deve ser uma preocupação a forma como o criado do conteúdo irá ser compensado (que hoje sempre entra em debate).</p>
<p>Sobre a questão da compensação, <a href="http://josh.scriptorium.se/">Joshua_Tree</a> escreveu:</p>
<blockquote>
<p>Besides yourself and Cory Doctorow, there are millions and millions of people who create culture without demanding, or even expecting, economic compensation. The Internet is full of them. Blogs, YouTube, etcetera. From simple puns and lolcats to amazingly well-produced films – it’s all culture and it’s all created without money changing hands. This may seem shocking to the Lars Ulrichs of the world, but to the rest of us it’s no mystery.</p>
<p>The simple explanation is that expressing oneself is a far more important factor than money. This is also why people continue to remix culture to create new culture, even knowing that they might get sued. Or in even worse cases, why people continue to express themselves and create culture in places where the regime will crack down on them for doing so.</p>
</blockquote>
<p>Joshua_Tree cita o conteúdo presente na internet como produzido sem fins lucrativos e que a justificativa para isso é que o ato de expressar-se é mais importante que o dinheiro e a possibilidade de ser processada.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>copyright</p>
</div>
<div class="tags">
<p>Falkvinge</p>
</div>
<div class="comments">

</div>
