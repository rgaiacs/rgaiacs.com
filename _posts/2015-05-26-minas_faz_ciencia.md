---
layout: post
title: "Onde está a ciência?"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>&quot;Onde está a ciência?&quot; é o tema da <a href="http://issuu.com/fapemig/docs/onde_est___a_ci__ncia_0b8021a3d439a3/1?e=3684464/13091502">edição 61</a> da revista <a href="https://fapemig.wordpress.com/about/">Minas Faz Ciência</a> que é &quot;um projeto de divulgação científica mantido pela Fundação de Amparo à Pesquisa do Estado de Minas Gerais (<a href="http://www.fapemig.br/">FAPEMIG</a>).&quot;</p>
<p>Nessa edição encontra-se uma ótima reportagem sobre ciência aberta escrita pela jornalista Camila Mantovani que está de parabéns pelo trabalho.</p>
<p>Como eu tive dificuldade em ler a reportagem no <a href="http://issuu.com/" class="uri">http://issuu.com/</a>, que é a plataforma onde a versão digital da revista é publicada resolvi criar, rapidamente, uma versão em <span data-role="download">PDF &lt;minas-faz-ciencia-61.pdf&gt;</span> e em <span data-role="download">CBZ &lt;minas-faz-ciencia-61.cbz&gt;</span> (visto que eu só tinha acesso aos JPG representando cada uma das páginas).</p>
<p>Espero que você também goste de ler a reportagem.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>academia,ciência aberta</p>
</div>
<div class="tags">
<p>UFMG,FAPEMIG,Minas Faz Ciência</p>
</div>
<div class="comments">

</div>
