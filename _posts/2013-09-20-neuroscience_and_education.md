---
layout: post
title: "Neurociência e Educação"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Este post trata de uma das messas redondas que assisti no <span data-role="doc">SBNeC
&lt;sbnec&gt;</span> cujo título era &quot;Neurosience and Education: Building Bridges&quot; que foi coordenada por Cecilia Hedin Pereira da Universidade Federal do Rio de Janeiro e contou com a presença de:</p>
<ul>
<li>Leonor Guerra da Universidade Federal de Minas Gerais que apresentou &quot;NeuroEduca: Neuroscience in the classroom&quot;;</li>
<li>Marilia Z. Guimarães da Universidade Federal do Rio de Janeiro que apresentou &quot;Using digital technology and neuroscience in learning&quot;;</li>
<li>Sidarta Ribeiro da Universidade Federal do Rio Grande do Norte que apresentou &quot;Neuroscience and Education: how far is the bridge?&quot;;</li>
<li>Soo-Siang Lim da National Science Foundation que apresentou &quot;Science of Learning: Crossing Boundaries to Make a Difference&quot;.</li>
</ul>
<div class="more">

</div>
<h1 id="neuroeduca-neuroscience-in-the-classroom">NeuroEduca: Neuroscience in the classroom</h1>
<p><a href="http://www.icb.ufmg.br/neuroeduca/">NeuroEduca</a> é um projeto de extensão da UFMG que busca ajudar professores a melhorarem suas aulas ao instruí-los de informações neurológicas.</p>
<p>As informações fornecidas para os professores não limitam-se ao reconhecimento de características que podem indicar alguma &quot;anomalia&quot; neurológica mas também de questões &quot;psicológicas&quot;.</p>
<h1 id="using-digital-technology-and-neuroscience-in-learning">Using digital technology and neuroscience in learning</h1>
<p>Logo no início da apresentação Marilia mostrou um infográfico que ilustrava onde a tecnologia marca presença na área de educação:</p>
<ul>
<li>customização,</li>
<li>colaboração,</li>
<li>gamificação,</li>
<li>cognição,</li>
<li>conteúdo multi-&quot;objetivo&quot;.</li>
</ul>
<p>Ao longo da apresentação, Marilia falou mais sobre essas tecnologias e algumas investigações neurológicas que estão sendo feitas envolvendo tais tecnologias.</p>
<p>Como exemplo, foram apresentados o <a href="http://www.edumobi.com.br/">edumobi</a> e o <a href="http://www.duolingo.com/">duolingo</a>.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>academia</p>
</div>
<div class="tags">
<p>SBNeC</p>
</div>
<div class="comments">

</div>
