---
layout: post
title: Dotfiles e Git
author: "Raniere Silva"
categories: ftb
tags: [ftb]
image:
  feature: ftb.jpg
---
Dotfiles são os arquivos de configuração que constuma encontrar-se no
seu diretório *home* e que por padrão são ocultos. É comum fazer backup
dos dotfiles para o caso da máquina apresentar problema ou quando é
preciso utilizar uma outra máquina e portanto podemos utilizar o Git
para essa tarefa.

Utilizar links simbólicos ou *hardlinks* dentro de um repositório git
não funciona e portanto a única alternativa é criar um diretório para
armazenar os dotfiles e criar links simbólicos no diretório home
apontando para eles.

**Referências**
