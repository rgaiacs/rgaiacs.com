---
layout: post
title: "Dia do Bibliotecário"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Amanhã, 12 de Março de 2015, é o Dia do Bibliotecário e farei uma apresentação na USP. Meus slides encontram-se disponíveis <span data-role="download">aqui &lt;slides.zip&gt;</span>.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>academia,ciência aberta</p>
</div>
<div class="tags">
<p>USP,SIBiUSP,Dia do Bibliotecário</p>
</div>
<div class="comments">

</div>
