---
layout: post
title: "First time manager"
author: "Raniere Silva"
categories: blog
tags: []
image:
  feature: 2019-03-03-first-time-manager.jpg
  author: Paul L Dineen
  licence: CC-BY
  original_url: https://flic.kr/p/8ggJ7L
---

A while ago,
I read [Anna Goldfarb](https://AnnaGoldfarb.com)'s
article "[What I Wish I’d Known as a First-Time Manager](https://medium.com/s/office-politics/what-i-wish-id-known-as-a-first-time-manager-a864a079a982)"
([archive PDF available](/pdf/first-time-manager.pdf)).
Anna's words are worth sharing
so I decided to leave a link on this blog.

If you are too busy to read Anna's article fully,
skip to the last section: "Trust your team."