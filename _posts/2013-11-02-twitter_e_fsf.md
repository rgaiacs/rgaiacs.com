---
layout: post
title: "Twitter e a Free Software Foundation"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Alguns dias atŕas o Sergio Durigan escreveu em seu blog um <strong>ÓTIMO</strong> post com o título &quot;<a href="http://blog.sergiodj.net/post/2013-10-16-fsf-twitter-coherence/">About coherence, Twitter, and the Free Software Foundation</a>&quot; (<span data-role="download">cópia nesse servidor &lt;sergio-twitter-and-fsf.html&gt;</span>). Neste post o Sergio avisa que a <a href="https://fsf.org/">Free Software Foundation</a> (FSF) possui uma <a href="https://twitter.com/fsf">conta no Twitter</a> e fala dos motivos pelos quais acredita que isso está errado. A seguir encontra-se meus cometários sobre o post do Sergio e a conta no Twitter da FSF.</p>
<div class="more">

</div>
<h1 id="pilha-de-software">Pilha de software</h1>
<p>Para quem não é da área de computação costumamos falar em pilha de software quando para executar um programa precisamos de outro (e.g. para utilizar o Microsoft Office você precisa do Windows) e para aplicações web temos a pilha ilustrada abaixo.</p>
<figure>
<img src="/images/stack.svg" class="align-center" style="width:60.0%" />
</figure>
<p>Na camada mais inferior (hardware) utilizamos, dentre outros programas, a <a href="http://pt.wikipedia.org/wiki/BIOS">BIOS</a> que muitas vezes não é livre (embora existam alternativas livres como o <a href="http://www.coreboot.org/Welcome_to_coreboot">Core Boot</a>. Os servidores que formam a internet possuem como próxima camada de software depois da BIOS um kernel (a grande maioria utiliza um Linux) que é o responsável por &quot;converter&quot; instruções de alto nível para o hardware. E a aplicação web, que muitas vezes não é livre, precisa ser disponibilizada por um servidor HTTP, camada superior, como o Apache ou Nginx que é o responsável por filtrar as requisições feitas pelos usuários da aplicação e retransmiti-las ao interpretador da linguagem na qual a aplicação foi escrita (e.g. PHP, Ruby, Python, Java, Javascript).</p>
<p>Logo, a afirmação do Sergio de que o Twitter, Facebook e, provavelmente, até a Microsoft utilizam software livre é verdade. Infelizmente ele NÃO utilizam apenas esse tipo de software.</p>
<h1 id="atividades">Atividades</h1>
<p>Acho que podemos categorizar categorizar nossas atividades em uma das três categorias do digrama abaixo.</p>
<figure>
<img src="/images/actions.svg" class="align-center" style="width:60.0%" />
</figure>
<p>As categorias &quot;produzir conteúdo&quot; e &quot;consumir conteúdo&quot; referem apenas a atividades que fazemos (ou podemos fazer) sozinhos. Para essas duas categorias acredito que sempre podemos utilizar software livre e ter total controle sobre o processo. Infelizmente para a categoria &quot;colaborar&quot; isso é um pouco mais difícil pois muitas vezes não podemos encontrarmos fisicamente e acabamos fazendo virtualmente o que implica em dependeremos de um intermediário.</p>
<p>Esse intermediário pode ser uma pessoa, uma empresa ou um software. No caso do software gostaríamos que ele fosse livre e com a funcionalidades de federação.</p>
<div class="note">
<div class="admonition-title">
<p>Note</p>
</div>
<p>Federação, como definido na <a href="http://en.wikipedia.org/wiki/Federation_(information_technology)">Wikipédia</a>, são vários computadores funcionado de forma coletiva com base em padrões estabelecidos.</p>
</div>
<p>A federação é importante porque permite cada indivíduo utilizar seu próprio servidor no processo ou o de alguém em quem confia (não necessariamente uma empresa) sem a limitação de colaborar com pessoas que possuem conta no mesmo servidor.</p>
<h1 id="propaganda-gratuita">Propaganda gratuita</h1>
<p>Toda vez que falamos o nome de um produto, serviço, marca ou utilizamos estamos DIRETAMENTE fazendo propaganda gratuita para ele. No caso de &quot;produzir conteúdo&quot; e &quot;consumir conteúdo&quot; mencionados anteriormente essa propaganda gratuita possui uma muito inferior do que no caso de &quot;colaborar&quot;.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>compartilhamento,internet,liberdade,privacidade</p>
</div>
<div class="tags">
<p>FSF,Twitter,alternativas</p>
</div>
<div class="comments">

</div>
