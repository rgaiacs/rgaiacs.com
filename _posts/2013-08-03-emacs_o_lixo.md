---
layout: post
title: "GNU Emacs: o lixo"
author: "Raniere Silva"
categories: ftb
tags: [ftb]
image:
  feature: ftb.jpg
---
<div class="admonition note">

Este post é mais um capítulo na Guerra dos editores. Recomenda-se também
ler emacs\_o\_editor.

</div>

O GNU Emacs possui uma versão "modo texto" que é utilizando quando em um
terminal ou acessada por meio de :

    $ emacs -nw

quando em um emulador de terminal.

![](/images/emacsnw00.jpeg){width="80%"}

Assim como no "modo gráfico", o "modo texto" do GNU Emacs também é:

-   personalizável,
-   capaz de carregar vários arquivos

![](/images/emacsnw01.jpeg){width="80%"}

-   utilizar várias janelas e

![](/images/emacsnw02.jpeg){width="80%"}

-   "orientado" a módulos.

![](/images/emacsnw03.jpeg){width="80%"}

Infelizmente, o terminal e emuladores de terminal não suportam o mesmo
número de teclas e combinações de teclas como o ambiente gráfico. Isso
significa que vários atalhos presentes no modo gráfico do GNU Emacs não
funcionam (a menos de remapeamento) no modo texto. Algumas dessas teclas
são:

-   backspace,
-   del,
-   tab, ...

O concorrente do GNU Emacs é o Vim que foi desenvolvido para funcionar
em um terminal. Embora ele sofra da ausência das mesmas teclas, foi
projetado para não utilizá-la.

![](/images/vim00.jpeg){width="80%"}

Assim como o GNU Emacs, é possível customizar o Vim.

![](/images/vim01.jpeg){width="80%"}

E ele também é capaz de utilizar vários arquivos e abrir várias janelas.

![](/images/vim02.jpeg){width="80%"}

Ao contrário do GNU Emacs, o Vim não é orientado a módulos e por
consequência "não é possível" abrir um terminal dentro do Vim e outras
coisas. Felizmente é possível utilizar um multiplicador de terminal como
o Tmux ou GNU Screen.

![](/images/vim03.jpeg){width="80%"}

**Referências**
