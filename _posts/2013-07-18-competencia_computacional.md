---
layout: post
title: "Competência Computacional"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Nos últimos dez anos muitas coisas aconteceram e acabaram promovendo uma crise de gerações de cientistas, digo, com o surgimento de</p>
<ul>
<li>banda larga,</li>
<li>copyleft,</li>
<li>wikis,</li>
<li>sistema de controle de versão (e.g., git),</li>
<li>tablets e ereaders, ...</li>
</ul>
<p>alguns pesquisadores mais novos e nativos digitais (principalmente com essas novas tecnologias) questionaram a maneira como é feito ciência na atualidade que não mudou muito de como ocorria na geração passada.</p>
<p>Greg Wilson publicou um post sobre a questão de competência computacional para biólogos que acredito estar relacionada com essa crise de gerações e também ser válido para várias outras áreas. A seguir você encontra uma tradução de parte desse post.</p>
<div class="more">

</div>
<h1 id="tradução">Tradução</h1>
<div class="note">
<div class="admonition-title">
<p>Note</p>
</div>
<p>O texto abaixo é uma tradução parcial de <a href="http://software-carpentry.org/blog/2013/07/computational-competence-for-biologists.html">Computational Competence for Biologist</a> de Greg Wilson originalmente publicado no blog do Software Carpentry em 16/07/2013 e licenciado sob CC-BY.</p>
<p>Notas do tradutor encontram-se entre parênteses e alguns termos não traduzidos em itálico.</p>
</div>
<p>Em 8 e 9 de Julho, eu (Greg Wilson) tive o prazer de participar de um workshop de dois dias em <a href="http://www.sesync.org/">SESYNC</a> para debater o que acreditamos dever ensinar biólogos sobre computação. O workshop foi na verdade um pequeno encontro, mas dentre os participantes tinhamos representantes de cientistas da computação, engenheiros de sistemas, biólogos computacionais, biólogos (de campo) e alguns peixes fora d'água como eu.</p>
<p>Um dos trabalhos do grupo foi preparar um exame de proficiência para determinar quando um biólogo era computacionalmente competente. Esse exame foi inspirado pelo <a href="http://software-carpentry.org/blog/2013/02/dirac-dry-run-two.html">exame para CNH (carteira nacional de habilitação)</a> que tinhamos ajudado a compor para o consórcio de supercomputação DiRAC, embora não estejamos seriamente propondo tal exame. Na verdade, queriamos usá-lo para focar o debate sobre o que entendemos por &quot;competência computacional&quot; para biólogos.</p>
<p>O teste submetido pelos cinco grupos encontram-se abaixo (ver o post original). O que me chamou a atenção foi como o debate foi dominado por temas relacionados a dados ao invés de ferramentas computacionais. Também fiquei boquiaberto pela alta taxa de sintonia existente entre os grupos, embora isso tenha sido um resultado de como as perguntas foram propostas. Alguns temas comuns incluem:</p>
<ol>
<li>documentação de processos para outros,</li>
<li>possibilidade de reprodução dos resultados,</li>
<li>conhecimento de como testar/verificar os resultados,</li>
<li>gerenciamento/solução de erros,</li>
<li>lugares para divulgar/compartilhar os resultados como (Gitorious,) GitHub, BitBucket e Figshare - o conceito de compartilhar é mais importante que a marca e o lugar - para que o trabalho esteja disponível mesmo quando os alunos (pesquisadores) tenham mudado para outro lugar</li>
<li>gerenciamento de banco de dados - não apenas como escrever <em>queries</em> (buscas), mas também como criar um <em>schema</em> (organização) adequada.</li>
</ol>
<p>Se essas são as habilidades computacionais que biólogos acreditam que seus colegas precisam conhecer, então precisamos repensar e re-escrever nossos materiais. Felizmente, existe um público animado com o que estamos fazendo, e eles acreditam fortemente que estamos fazendo suas vidas melhores.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>none</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
