---
layout: post
title: "LaTeX Course at EnCPos"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>This week I will be teaching LaTeX in the Scientific Meeting of Phd Students of IMECC (in portuguese &quot;<a href="http://www.ime.unicamp.br/~encpos/VIII_EnCPos/">Encontro Científico dos Pós-Graduandos do IMECC</a>&quot;).</p>
<p>The course will be in October 16th, 17th and 18th with one 1:20 long each day. It will be a little different from my other LaTeX course (<span data-role="doc">read about it
here &lt;../../09/04/review_of_my_latex_course_at_semat&gt;</span>) and the schedule are:</p>
<dl>
<dt>1st day: <code>find / -name '*tex*'</code></dt>
<dd><p>Starting with words about the past, present and future of (La)TeX. After that some explanation about file formats/extensions and some of programs that are shipped in the LaTeX distribution.</p>
</dd>
<dt>2nd day: The preamble</dt>
<dd><p>The preamble of LaTeX documents is where we call for the packages that make our work easier. Information about some popular packages will be given.</p>
</dd>
<dt>3nd day: <code>amsmath</code>, TikZ and BibTeX</dt>
<dd><p>This are three of the most famous LaTeX packages. Some examples of use of this packages will be given.</p>
</dd>
</dl>
<div class="more">

</div>
<h1 id="general">General</h1>
<p>As in the previous course I plan to split each class in five rounds of 20 minutes. In each round I will teach some topic, give a example and let the students change the example.</p>
<p>For share the examples and some annotations we will be using <a href="https://pad.riseup.net/p/encpos-latex">this pad</a> (<span data-role="download">backup of the pad before
the course start &lt;encpos-latex.txt&gt;</span>) and for feedbacks <a href="https://pad.riseup.net/p/encpos-feedback-latex">this other pad</a> (<span data-role="download">backup of the
pad before the course start &lt;encpos-feedback-latex.txt&gt;</span>).</p>
<h1 id="st-day">1st Day</h1>
<dl>
<dt>Round 1.1</dt>
<dd><p>Some explanation about the pad and solving problems with the machines</p>
</dd>
<dt>Round 1.2</dt>
<dd><p>Introduction about TeX and Latex with some backgrounds.</p>
<p>This time we will not use a LaTeX IDE. Some explanations about the terminal will be given.</p>
</dd>
<dt>Round 1.3</dt>
<dd><p>Write some examples of LaTeX and give some basic explanations about the preamble, commands, environments and paragraphs.</p>
</dd>
<dt>Round 1.4</dt>
<dd><p>Compile the examples of the previous round and use of others programs that are shipped in the LaTeX distribution.</p>
</dd>
<dt>Round 1.5</dt>
<dd><p>Time to write a feedback of this class and answer to questions.</p>
</dd>
</dl>
<h1 id="nd-day">2nd Day</h1>
<dl>
<dt>Round 2.1</dt>
<dd><p>Information about encoding, <code>inputenc</code> package and <code>fontenc</code> package.</p>
</dd>
<dt>Round 2.2</dt>
<dd><p>Information about internationalization, <code>babel</code> package and <code>indentfirst</code> package.</p>
</dd>
<dt>Round 2.3</dt>
<dd><p>Information about table of contents, hyperlinks and <code>hyperref</code> package.</p>
</dd>
<dt>Round 2.4</dt>
<dd><p>Information about bitmap and <code>graphicx</code> package.</p>
</dd>
<dt>Round 2.5</dt>
<dd><p>Time to write a feedback of this class and answer to questions.</p>
</dd>
</dl>
<h1 id="nd-day-1">3nd Day</h1>
<dl>
<dt>Round 3.1</dt>
<dd><p>Introduction to in-line and display math mode.</p>
</dd>
<dt>Round 3.2 and 3.3</dt>
<dd><p>I like to try use GitHub Gist for review the codes of the students. I will ask the students to retype <span data-role="download">this pdf &lt;gist-ref.pdf&gt;</span> (build from <span data-role="download">this file &lt;gist-ref.tex&gt;</span>) starting from <span data-role="download">this LaTeX
document &lt;gist.tex&gt;</span> (<span data-role="download">the pdf &lt;gist.pdf&gt;</span>).</p>
<p>After the students retype the document they must copy and past it in a GitHub Gist and add the link in the pad. I will review the codes as soon as they finished the active.</p>
</dd>
<dt>Round 3.4</dt>
<dd><p>Some examples of TikZ.</p>
</dd>
<dt>Round 3.5</dt>
<dd><p>Some examples of BibTeX.</p>
</dd>
</dl>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>educação,Software Carpentry</p>
</div>
<div class="tags">
<p>LaTeX,EnCPos</p>
</div>
<div class="comments">

</div>
