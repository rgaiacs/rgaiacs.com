---
layout: post
title: "Broccoli, peas and love"
author: "Raniere Silva"
categories: teaching
tags: []
image:
  feature: 2019-09-26-broccoli-peas-love.jpg
  author:  Ella Olsson
  licence: Unsplash License
  original_url: https://unsplash.com/photos/mmnKI8kMxpc
---

Last Sunday,
I had dinner with a friend
and she asked me if I changed my mind about not get marry.
I did **not**.

Do you know a kid that don't like to eat their veggies?
I have a similar relationship with love.
I had a lot of very sour "first dates" that didn't evolve to a partnership.
This is like eat your veggies but never get dessert.
Once you learnt that you will not have dessert,
why bother to eat your veggies?

My friend asked me,
as a follow up question,
if I don't feel loneliness.
I think that even if you have a partner,
you will still going to feel loneliness sometimes.
And life teaches you some tips to cope with loneliness,
for example:
dinner with friends,
do sports,
<del>use drugs</del>.


