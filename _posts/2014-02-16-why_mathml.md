---
layout: post
title: "Why MathML"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<div class="admonition">
<p>Update</p>
<p>I suggest that you read the <a href="http://www.mozillazine.org/articles/article572.html">article wrote by MPT back in 1999</a> <a href="http://www.mozillazine.org/talkback.html?article=572">in response this questions</a>.</p>
</div>
<p><a href="http://www.w3.org/mathml">MathML</a> &quot;is a low-level specification for describing mathematics as a basis for machine to machine communication which provides a much needed foundation for the inclusion of mathematical expressions in Web pages&quot; designed by W3C and now part of HTML(5) and EPUB(3). Unfortunately, support MathML wasn't a priority for the big players that drive the Web and some times the question &quot;Why not deprecate MathML?&quot; raised.</p>
<p>This post try to summarize (with lots of quotes) the very long discussion about <strong>native</strong> MathML support in <a href="https://groups.google.com/forum/#!topic/mozilla.dev.platform/96dZw1jXTvM">Firefox</a> and <a href="https://code.google.com/p/chromium/issues/detail?id=152430&amp;can=1&amp;q=mathml&amp;colspec=ID%20Pri%20M%20Iteration%20ReleaseBlock%20Cr%20Status%20Owner%20Summary%20OS%20Modified">WebKit/Chromium</a> communities.</p>
<div class="admonition">
<p>Note</p>
<p>Frédéric Wang wrote three amazing blog posts with lots of technical details of MathML support for majors web browsers:</p>
<ul>
<li><a href="http://www.maths-informatique-jeux.com/blog/frederic/?post/2013/10/07/Post-Summit-Thoughts-on-the-MathML-Project">Post-Summit Thoughts on the MathML Project</a>,</li>
<li><a href="http://www.maths-informatique-jeux.com/blog/frederic/?post/2013/10/12/Funding-MathML-Developments-in-Gecko-and-WebKit">Funding MathML Developments in Gecko and WebKit</a> and</li>
<li><a href="http://www.maths-informatique-jeux.com/blog/frederic/?post/2014/01/05/Funding-MathML-Developments-in-Gecko-and-WebKit-%28part-2%29">Funding MathML Developments in Gecko and WebKit (part 2)</a>.</li>
</ul>
</div>
<div class="admonition">
<p>Note</p>
<p><a href="https://code.google.com/p/chromium/issues/detail?id=152430#c75">madisli</a> create a pro-cons list related to MathML. <span data-role="download">Download local copy of
the list &lt;pro-mathml.pdf&gt;</span>.</p>
</div>
<figure>
<img src="/images/pro-mathml.png" class="align-center" style="width:80.0%" />
</figure>
<div class="more">

</div>
<h1 id="mathml-is-too-specialized">MathML is too specialized</h1>
<p>Yes, it is as all other human languages.</p>
<blockquote>
<p>&quot;I personally see mathematical writing as language by itself and so not having it in the browsers is just like not supporting Arabic or Asian scripts (BTW MathML was implemented in Gecko a long time before HTML ruby). Just to add one point: mathematical expressions are also very often mixed with other content like text or diagrams and it makes sense to have HTML+SVG+MathML+CSS well integrated together.&quot; <a href="https://groups.google.com/d/msg/mozilla.dev.platform/96dZw1jXTvM/YipbZp7TdqAJ">Frédéric Wang</a></p>
</blockquote>
<p>And as <a href="https://groups.google.com/d/msg/mozilla.dev.platform/96dZw1jXTvM/rIzKSrRgk18J">Peter</a> said:</p>
<ul>
<li>MathML is widely used. Almost all publishers use XML workflows and in those MathML for math. Similarly, XML+MathML dominates technical writing;</li>
<li>In particular, the entire digital textbook market and thus the entire educational sector comes out of XML/MathML workflows right now;</li>
<li>MathML is the only format supported by math-capable accessibility tools right now.</li>
</ul>
<h1 id="mathml-reinvents-the-wheel-poorly">MathML reinvents the wheel, poorly</h1>
<blockquote>
<p>&quot;A suitable subset of TeX (not the entirety of TeX, as that is a huge, single-implementation technology that reputedly only Knuth ever fully understood) was the right choice all along, because: (1) TeX is already the universally adopted standard and (2) TeX is very friendly to manual writing, being concise and close to natural notation, with limited overhead (some backslashes and curly braces), while MathML is as tedious to handwrite as any other XML-based format.&quot; <a href="https://groups.google.com/d/msg/mozilla.dev.platform/96dZw1jXTvM/hkNn65-Spf4J">Benoit Jacob</a></p>
</blockquote>
<p>MathML was design for machine to machine communication which means that, for example, (1) &quot;f(x)&quot; meaning &quot;f of x&quot; <strong>must</strong> be different of &quot;f times x&quot; and (2) &quot;ab&quot; meaning &quot;a times b&quot; <strong>must</strong> be different from &quot;variable named 'ab'&quot;. For we humans is easy to solve this problem but machines need more information and that's the reason why MathML need to be tedious to handwrite.</p>
<p>A subset of TeX won't be sufficient because</p>
<blockquote>
<p>&quot;I've had to use various LaTeX packages (particularly <code>amsmath</code> and <code>amssymb</code>) in order to get all of the symbols and so on that I needed. I suspect that &quot;heavy&quot; users of TeX frequently need more than these two packages.&quot; <a href="https://groups.google.com/d/msg/mozilla.dev.platform/96dZw1jXTvM/nQ44bVJQk0YJ">Justin Lebar</a></p>
</blockquote>
<p>One of the solutions for symbols is using UTF8 (in this case MathML will be much smaller than TeX) but this means using a GUI that list UTF8 symbols or memorize their codes.</p>
<p>MathML is very stable (although the implementations aren't) but</p>
<blockquote>
<p>&quot;While TeX and the basic LaTeX packages are stable, most macro packages are unreliable. Speaking as a mathematician, it's often hard to compile my own TeX documents from a few years ago. You can also ask the arXiv folks how painful it is to do what they do.&quot; <a href="https://groups.google.com/d/msg/mozilla.dev.platform/96dZw1jXTvM/rIzKSrRgk18J">Peter</a></p>
</blockquote>
<p>MathML has a DOM. And what?</p>
<blockquote>
<p>&quot;That may not matter much for rendering, but it does if you want to support (WYSIWYG) editing.&quot; <a href="https://groups.google.com/d/msg/mozilla.dev.platform/96dZw1jXTvM/BT64G13R148J">Robert O'Callahan</a></p>
</blockquote>
<p>Although (La)TeX is much older that MathML there isn't a <strong>full native</strong> (La)TeX WYSIWYG editor (yes, there is <a href="www.lyx.org">LyX</a> but it use it's own format).</p>
<p><strong>Pure</strong> (La)TeX can only build static pages.</p>
<blockquote>
<p>&quot;We expose HTML and SVG content to Web applications by structuring that content as a tree and then exposing it using standard DOM APIs. These APIs let you examine, manipulate, parse and serialize content subtrees. They also let you handle events on that content. CSS also depends on content having a DOM tree structure for selectors and inheritance to work. You definitely need to able to handle events and apply CSS to elements of your math markup.&quot; <a href="https://groups.google.com/d/msg/mozilla.dev.platform/96dZw1jXTvM/uuFrWrUYouEJ">Robert O'Callahan</a></p>
</blockquote>
<h1 id="mathml-never-saw-much-traction">MathML never saw much traction</h1>
<blockquote>
<p>&quot;MathML never saw much traction outside of Mozilla, despite having been around for a decade. WebKit only got a very limited partial implementation recently, and Google removed it from Blink. The fact that it was just dropped from Blink says much about how little it's used: Google wouldn't have disabled a feature that's needed to render web pages in the real world. Opera got an implementation too, but Opera's engine has been phased out.&quot; <a href="https://groups.google.com/d/msg/mozilla.dev.platform/96dZw1jXTvM/hkNn65-Spf4J">Benoit Jacob</a></p>
</blockquote>
<p>How many users will shift to a web browser that have MathML native support? Even if all mathematical sciences (math, mathematical physics, large parts of CS) shift I believe that won't be enough to Mozilla, Apple, Google, Microsoft or any other player to put money in MathML implementation. But if all mathematical education resource move to the web using MathML some players will start put money (more at <span data-role="ref">education</span>).</p>
<p>The office suites &quot;MS Word and Libre Office produce MathML out of the box&quot; as <a href="https://groups.google.com/d/msg/mozilla.dev.platform/96dZw1jXTvM/rIzKSrRgk18J">Peter</a> remember.</p>
<p>And &quot;the ISO 32000-2 specification is expected to have support for MathML tagging of formulas in PDF documents.&quot; as inform by <a href="https://code.google.com/p/chromium/issues/detail?id=152430#c51">Deyan</a>.</p>
<h1 id="using-javascript-libraries">Using Javascript libraries</h1>
<blockquote>
<p>&quot;High-quality mathematical typography in browsers is now possible, without using MathML. Examples include <a href="http://www.mathjax.org/">MathJax</a>, which happily takes either TeX or MathML input and renders it without specific browser support, and of course <a href="http://mozilla.github.io/pdf.js/">PDF.js</a> which is theoretically able to render all PDFs including those generated by <code>pdftex</code>. Both approaches give far higher quality output than what any current MathML browser implementation offers.&quot; <a href="https://groups.google.com/d/msg/mozilla.dev.platform/96dZw1jXTvM/hkNn65-Spf4J">Benoit Jacob</a></p>
</blockquote>
<p>MathJax is nice but is a huge Javascript Library and for long documents take some time to properly convert/render the math elements (as <a href="https://groups.google.com/d/msg/mozilla.dev.platform/96dZw1jXTvM/rIzKSrRgk18J">Peter</a> said it is ~5 time slower than native support). Even people that use it want not need it:</p>
<blockquote>
<p>&quot;We run a large academic journal site which includes occasional math. The challenge for us with MathJax is that we either: (1) include math on every page on our site, or (2) build a math detector that includes MathJax only when we need it. Neither solution is completely ideal. We're going with (1), but it does mean extra javascript downloads and processing on all our pages, even though most don't actually have math on them. Caching helps, of course, but first-page views for every user are affected.&quot; <a href="https://code.google.com/p/chromium/issues/detail?id=152430#c49">msoko...@gmail.com</a></p>
</blockquote>
<p>Moreover</p>
<blockquote>
<p>&quot;The MathJax team is strongly in favor of native MathML implementation.&quot; <a href="https://groups.google.com/d/msg/mozilla.dev.platform/96dZw1jXTvM/9P2h8HPdUB8J">Frédéric Wang</a></p>
</blockquote>
<p>Even <strong>IF</strong></p>
<blockquote>
<p>&quot;tomorrow a competing browser solves these problems, and renders MathJax's HTML output fast, we will obviously have to follow. That can easily happen, especially as neither of our two main competitors is supporting MathML.&quot; <a href="https://groups.google.com/d/msg/mozilla.dev.platform/96dZw1jXTvM/1-Qnebp_nOAJ">Benoit Jacob</a></p>
</blockquote>
<p>we have to remember that some devices (e.g. ereaders) will have limited hardware for some reason (e.g. for ereaders is the battery duration).</p>
<div id="education">
<h2 id="education">Education</h2>
</div>
<p><a href="https://groups.google.com/d/msg/mozilla.dev.platform/96dZw1jXTvM/qVVRTV-qY_YJ">Peter</a> remember that math is a subject in basic education:</p>
<blockquote>
<p>&quot;It's also something that all school children will encounter for 9-12 years. IMHO, this makes it necessary to implement mathematical typesetting functionality.&quot;</p>
</blockquote>
<p>But <a href="https://groups.google.com/d/msg/mozilla.dev.platform/96dZw1jXTvM/wxrin0cyBWkJ">Benoit Jacob</a> refute this necessity:</p>
<blockquote>
<p>&quot;School children are only on the reading end of math typesetting, so for them, AFAICS, it doesn't matter that math is rendered with MathML or with MathJax's HTML+CSS renderer.&quot;</p>
</blockquote>
<p>Fortunately, <a href="https://groups.google.com/d/msg/mozilla.dev.platform/96dZw1jXTvM/rU-TGITRh_wJ">Brian Smith</a> give a great reply to Benoit:</p>
<blockquote>
<p>&quot;School children traditionally have been on the reading end of math typesetting because they get punished for writing in their math books. However, I fully expect that scribbling in online books will be highly encouraged going forward. School children are not going to write MathML or TeX markup. Instead they will use graphical WYSIWYG math editors. The importance of MathML vs. alternatives, then, will have to be judged by what those WYSIWYG end up using. WYSIWYG editing of even basic wiki pages is still almost completely unusable right now, so I don't think we're even close to knowing what's optimal as far as editing non-trivial mathematics goes.&quot;</p>
</blockquote>
<p>And</p>
<blockquote>
<p>&quot;Performance matters not only for the initial document rendering. When you do WYSIWYG editing performance characteristics matter in a lot more subtle ways. When you are editing big equations, or some really big document updates need to happen as close as possible to instant.&quot; <a href="https://groups.google.com/d/msg/mozilla.dev.platform/96dZw1jXTvM/lf9_3LB3GqIJ">Mihai Sucan</a></p>
</blockquote>
<p>Unfortunately we still lock with keyboards or WYSIWYG:</p>
<blockquote>
<p>&quot;Finally, people are also interested in handwritting recognition (see e.g. <a href="https://www.youtube.com/watch?v=26opB8DRf3c" class="uri">https://www.youtube.com/watch?v=26opB8DRf3c</a> or <a href="http://webdemo.visionobjects.com/portal.html" class="uri">http://webdemo.visionobjects.com/portal.html</a>).&quot; <a href="https://groups.google.com/d/msg/mozilla.dev.platform/96dZw1jXTvM/YipbZp7TdqAJ">Frédéric Wang</a></p>
</blockquote>
<p>Lot less important, color in math expressions can be very useful:</p>
<blockquote>
<p>&quot;Note that I've seen at least one PhD dissertation that made good use of color to highlight which terms of a 2-page-long equation canceled each other to produce the final (much shorter; iirc it was 0) result.</p>
<p>Of course that was in the PDF version; the print version had to be black-and-white, and was a lot harder to follow.&quot; <a href="https://groups.google.com/d/msg/mozilla.dev.platform/96dZw1jXTvM/urqCgpbgAmkJ">Boris Zbarsky</a></p>
</blockquote>
<h1 id="conclusions">Conclusions</h1>
<p>I hope that you already convinced that support MathML in web browser is important but there are some last amazing thoughts.</p>
<p>From <a href="https://groups.google.com/d/msg/mozilla.dev.platform/96dZw1jXTvM/rIzKSrRgk18J">Peter</a>:</p>
<blockquote>
<p>&quot;MathML still feels a lot like HTML 1 to me. It's only entered the web natively in 2012. We're lacking a lot of tools, in particular open source tools (authoring environments, cross-conversion, a11y tools etc).</p>
<p>But that's a bit like complaining in 1994 that HTML sucks and that there's TeX which is so much more natural with chapter and section and has higher typesetting quality anyway.</p>
<p>...</p>
<p>A statistical plot has no more reason to be an image than an equation -- it should be markup/data in the page and the browser should render it. Browsers may be the new printing press, but we are looking at Gutenberg's model here, not 20th century digital offset printing.</p>
<p>Anyway, the MathWG has fought extremely hard for 15 years to make mathematics a first class citizen on the web. Certainly, MathML is only the beginning for math on the web. But abandoning it now will throw scientific content back 20 years.</p>
<p>Personally, I don't want to wait for another Knuth to show up and fix the problem.&quot;</p>
</blockquote>
<p>From <a href="https://groups.google.com/d/msg/mozilla.dev.platform/96dZw1jXTvM/ahByHp1-LpYJ">someone at Design Science</a>:</p>
<blockquote>
<p>&quot;MathML was never intended to be typed by humans so it is no wonder that you find it a bad experience. TeX is a poor computer representation which is one reason why MathML was invented.</p>
<p>It is reasonable to have a discussion of the relative merits of entering math by typing TeX vs point-and-click editing of math (ie, direct manipulation editing). I am biased toward the latter but I can understand the feelings of those whose hands know TeX really well.</p>
<p>In short, both MathML and TeX have good reasons to exist and don't compete with each other in their primary categories.&quot;</p>
</blockquote>
<p>From <a href="https://code.google.com/p/chromium/issues/detail?id=152430#c63">Gerd</a></p>
<blockquote>
<p>&quot;Math is THE universal language, understood by every nation on this world and used to describe and analyse our world in physics, chemistry, biology and even economics.</p>
<p>Let me compare the situation to hypothetical one: In my native language, german, we have 'Umlaute': ä ö ü. Two main browers included them already in their charset. All others still have to read something like ae oe ue in their browser. To avoid this, some clever folks have come with a program, that searches for every ae an replaces it with a tiny image of 'ä' and so on. This only works when being online.</p>
<p>And now the suggestion is: No, we don`t implement the 'Umlaute', as the standard suggests, we make the replacement program faster. NO WAY!&quot;</p>
</blockquote>
<p>From <a href="https://code.google.com/p/chromium/issues/detail?id=152430#c83">gel</a>:</p>
<blockquote>
<p>&quot;Students are impatient, even at the undergraduate college level. It is important for math to be fully on the same playing field as classical html.&quot;</p>
</blockquote>
<p>From <a href="https://code.google.com/p/chromium/issues/detail?id=152430#c84">rynec</a>:</p>
<blockquote>
<p>&quot;I'm extremely disappointed to hear this. Researchers, educators and students have been waiting for a decent way to communicate on the web about technical topics for the better part of twenty years. MathML provides that.</p>
<p>Can we please prioritize a feature that actually has, you know, an actual social benefit?&quot;</p>
</blockquote>
<p>From <a href="https://code.google.com/p/chromium/issues/detail?id=152430#c86">William F Hammond</a>:</p>
<blockquote>
<p>&quot;Do we want our children to grow up with the impression that math is not important enough to be in web pages?&quot;</p>
</blockquote>
<p>From <a href="https://groups.google.com/d/msg/mozilla.dev.platform/96dZw1jXTvM/lf9_3LB3GqIJ">Mihai Sucan</a></p>
<blockquote>
<p>&quot;I do not care about the technology here - MathML or TeX. What I care about is for the web browsers to meet the technical demands for producing really good math rendering and editors. I want this not for the academics, not for professors who can write TeX documents. I want this for school children who cannot write math on paper, who are blind, or who have other physical disabilities. Manually writing LaTeX does not &quot;cut it&quot; at early stages, when children learn maths. Such tools are invaluable for them.&quot;</p>
</blockquote>
<p>From <a href="https://groups.google.com/d/msg/mozilla.dev.platform/96dZw1jXTvM/Uj1RW-Fa5wcJ">Frédéric Wang</a></p>
<blockquote>
<p>&quot;It seems that Benoit thought that MathML was not used at all and could easily be dropped or replaced. As others have tried to explain that's not true and there are already many concrete projects that have been developed for 15 years, several places where MathML plays a key role and many people keeping asking for MathML support in browsers. Certainly LaTeX can be parsed into a tree for WYSIWYG edition, we can convert ASCIIMath directly to HTML+CSS or accessibility tools could perhaps even read a formula without building a tree at all. But we don't care here since we are talking about the Web and about Gecko-based applications so we want to have an SGML format, a DOM, compatibility with all the HTML5 family and build tools on top of that. There are MathML-based projects to address the needs mentioned in this thread and there are already many pages with MathML content on the Web. So there is no reason to replace MathML by something that will help for the simplest cases (already addressed by existing tools as I said above) but won't work in general and will break all existing MathML contents and projects. The main remaining issue is the lack of browser support so dropping MathML from Gecko would definitely be the wrong choice and a very negative signal to the Web community, especially since one of the argument given is that Gecko should follow what Blink does. Mozilla should be prude to have had volunteers involved in MathML projects during all these years and see that as a benefit. Fortunately, I see that the majority of comments in this thread go in that direction.&quot;</p>
</blockquote>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>MathML</p>
</div>
<div class="tags">
<p>MathML</p>
</div>
<div class="comments">

</div>
