---
layout: post
title: Conflicts at Gitit
author: "Raniere Silva"
categories: ftb
tags: [ftb]
image:
  feature: ftb.jpg
---
Last post was about [ShareLaTeX](http://sharelatex.com/) support to Git.
I was very unhappy that, right now, to resolve Git conflicts at
ShareLaTeX I need to leave the web interface. Because of that, at this
post I will be investigate how [Gitit](https://github.com/jgm/gitit)
handle conflicts resolution.

Install Gitit
=============

Gitit is very easy to install in a GNU/Linux machine. If you are using
Debian or another distro based on it, like Ubuntu, you can :

    # apt-get install gitit

Creating a New Project
======================

To create a new (local) project:

    $ mkdir new-project
    $ gitit

That's it. If you visit <http://localhost:5001/> you will see your Gitit
project.

<div class="admonition">

Note

You can stop the local web server pressing `CTRL+C`.

</div>

Creating and Resolving Conflict
===============================

1.  Open two web browsers.

![](/images/conflict0.jpg){width="80%"}

2.  In the web browser on the left, select "Edit". Change :

        # Welcome to Gitit!

    to :

        # Welcome to Dracula's wiki!

![](/images/conflict1.jpg){width="80%"}

3.  In the web browser on the right, select "Edit". Change :

        # Welcome to Gitit!

    to :

        # Welcome to Wolfman's wiki!

![](/images/conflict2.jpg){width="80%"}

4.  In the web browser on the left, select "Save".

![](/images/conflict3.jpg){width="80%"}

5.  In the web browser on the right, select "Save".

![](/images/conflict4.jpg){width="80%"}

6.  Gitit will noticed that the version you was editing is outdated. It
    will give you the file so you can resolve the conflicts and merge
    the changes.

    Replace :

        <<<<<<< edited
        # Welcome to Wolfman's wiki!
        =======
        # Welcome to Dracula's wiki!
        >>>>>>> f1b85ef897ed69588aa24b88d3802531f620cefc

    with :

        # Welcome to Dracula and Wolfman's wiki!

![](/images/conflict5.jpg){width="80%"}

7.  Save the changes.

![](/images/conflict6.jpg){width="80%"}

Checking Git Log
================

If you want to check the Git log:

    $ cd wikidata
    $ git log --all --oneline --decorate --graph
    * 6d9cddc (HEAD, master) Wolfman's change
    * f1b85ef Dracula's change
    * d2e70e2 User’s guide (README)
    * 783d1cb Default help page
    * 4e28cea Default front page

As you can see, the log is very clean (without branches and merges).

<div class="admonition">

Note

I really want that something like this was possible at ShareLaTeX.

</div>
