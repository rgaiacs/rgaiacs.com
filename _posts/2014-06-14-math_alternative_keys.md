---
layout: post
title: "Math Alternative Keys"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Part of the symbols at the math virtual keyboard are accessible as alternative keys. This is the first list of it.</p>
<div class="admonition">
<p>Note</p>
<p>You can find a full list of Unicode characters and corresponding LaTeX command at <a href="http://milde.users.sourceforge.net/LUCR/Math/unimathsymbols.pdf" class="uri">http://milde.users.sourceforge.net/LUCR/Math/unimathsymbols.pdf</a>.</p>
</div>
<div class="more">

</div>
<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Main Key</td>
<td>Alternative Key</td>
</tr>
<tr class="even">
<td>Symbol | Unicode | (La)TeX</td>
<td>Symbol | Unicode | (La)TeX</td>
</tr>
<tr class="odd">
<td>√ | 0221A | \sqrt{}</td>
<td>n√ | | \sqrt[n]{}</td>
</tr>
<tr class="even">
<td><dl>
<dt>&lt; | 0003C | &lt;</dt>
<dd><div class="line-block">        | +
        |
        | +
        |</div>
</dd>
</dl></td>
<td><blockquote>
<p>≤ | 02264 | \leq</p>
</blockquote>
<dl>
<dt>--------+---------+--------------------+</dt>
<dd><p>≦ | 02266 | \leqq</p>
</dd>
<dt>--------+---------+--------------------+</dt>
<dd><p>≪ | 0226A | \ll</p>
</dd>
</dl></td>
</tr>
<tr class="odd">
<td><dl>
<dt>&gt; | 0003E | &gt;</dt>
<dd><div class="line-block">        | +
        |
        | +
        |</div>
</dd>
</dl></td>
<td><blockquote>
<p>≥ | 02265 | \geq</p>
</blockquote>
<dl>
<dt>--------+---------+--------------------+</dt>
<dd><p>≧ | 02267 | \geqq</p>
</dd>
<dt>--------+---------+--------------------+</dt>
<dd><p>≫ | 0226B | \gg</p>
</dd>
</dl></td>
</tr>
<tr class="even">
<td><dl>
<dt>= | 0003D | =</dt>
<dd><div class="line-block">        | +
        |
        | +
        |
        | +
        |</div>
</dd>
</dl></td>
<td><blockquote>
<p>≡ | 02261 | \equiv</p>
</blockquote>
<dl>
<dt>--------+---------+--------------------+</dt>
<dd><p>≠ | 02260 | \neq</p>
</dd>
<dt>--------+---------+--------------------+</dt>
<dd><p>~ | 0223C | \sim</p>
</dd>
<dt>--------+---------+--------------------+</dt>
<dd><p>≈ | 02248 | \approx</p>
</dd>
</dl></td>
</tr>
<tr class="odd">
<td><dl>
<dt>∫ | 0222B | \int</dt>
<dd><div class="line-block">        | +
        |
        | +
        |
        | +
        |
        | +
        |
        | +
        |
        | +
        |</div>
</dd>
</dl></td>
<td><blockquote>
<p>∬ | 0222C | \iint</p>
</blockquote>
<dl>
<dt>--------+---------+--------------------+</dt>
<dd><p>∭ | 0222D | \iint</p>
</dd>
<dt>--------+---------+--------------------+</dt>
<dd><p>∮ | 0222E | \oint</p>
</dd>
<dt>--------+---------+--------------------+</dt>
<dd><p>∯ | 0222F | \oiint</p>
</dd>
<dt>--------+---------+--------------------+</dt>
<dd><p>∰ | 02230 | \oiiint</p>
</dd>
<dt>--------+---------+--------------------+</dt>
<dd><p>∲ | 02232 | \varointclockwise</p>
</dd>
<dt>--------+---------+--------------------+</dt>
<dd><p>∳ | 02233 | \ointctrclockwise</p>
</dd>
</dl></td>
</tr>
<tr class="even">
<td><dl>
<dt>( | 00028 | (</dt>
<dd><div class="line-block">        | +
        |
        | +
        |
        | +
        |
        | +
        |
        | +
        |</div>
</dd>
</dl></td>
<td><blockquote>
<p>[ | 0005b | [</p>
</blockquote>
<dl>
<dt>--------+---------+--------------------+</dt>
<dd><p>{ | 0007b | \{</p>
</dd>
<dt>--------+---------+--------------------+</dt>
<dd><p>⟨ | 027E8 | \langle</p>
</dd>
<dt>--------+---------+--------------------+</dt>
<dd><p>⟪ | 027EA | \lang</p>
</dd>
<dt>--------+---------+--------------------+</dt>
<dd><p>⌈ | 02308 | \lceil</p>
</dd>
<dt>--------+---------+--------------------+</dt>
<dd><p>⌊ | 0230A | \lfloor</p>
</dd>
</dl></td>
</tr>
<tr class="odd">
<td><dl>
<dt>) | 00029 | )</dt>
<dd><div class="line-block">        | +
        |
        | +
        |
        | +
        |
        | +
        |
        | +
        |</div>
</dd>
</dl></td>
<td><blockquote>
<p>] | 0005c | ]</p>
</blockquote>
<dl>
<dt>--------+---------+--------------------+</dt>
<dd><p>} | 0007c | \}</p>
</dd>
<dt>--------+---------+--------------------+</dt>
<dd><p>⟩ | 027E9 | \rangle</p>
</dd>
<dt>--------+---------+--------------------+</dt>
<dd><p>⟫ | 027EB | \rang</p>
</dd>
<dt>--------+---------+--------------------+</dt>
<dd><p>⌉ | 02309 | \rceil</p>
</dd>
<dt>--------+---------+--------------------+</dt>
<dd><p>⌋ | 0230B | \rfloor</p>
</dd>
</dl></td>
</tr>
</tbody>
</table>
<h1 id="screenshot-gallery">Screenshot Gallery</h1>
<div class="admonition">
<p>Note</p>
<p>Still need to solve the lack of math font.</p>
</div>
<figure>
<img src="/images/sqrt.png" alt="Alternatives to square root." class="align-center" style="width:80.0%" /><figcaption>Alternatives to square root.</figcaption>
</figure>
<figure>
<img src="/images/less.png" alt="Alternatives to &#39;&lt;&#39;." class="align-center" style="width:80.0%" /><figcaption>Alternatives to '&lt;'.</figcaption>
</figure>
<figure>
<img src="/images/equal.png" alt="Alternatives to &#39;=&#39;." class="align-center" style="width:80.0%" /><figcaption>Alternatives to '='.</figcaption>
</figure>
<figure>
<img src="/images/int.png" alt="Alternatives to integral symbol." class="align-center" style="width:80.0%" /><figcaption>Alternatives to integral symbol.</figcaption>
</figure>
<figure>
<img src="/images/bracket.png" alt="Alternative to bracket." class="align-center" style="width:80.0%" /><figcaption>Alternative to bracket.</figcaption>
</figure>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>GSOC</p>
</div>
<div class="tags">
<p>GSOC2014</p>
</div>
<div class="comments">

</div>
