---
layout: post
title: Media Transfer Protocol - MTP
author: "Raniere Silva"
categories: ftb
tags: [ftb]
image:
  feature: ftb.jpg
---
Vivíamos muito bem utilizando pendrives, HD externos, ... como se fossem
uma unidade de armazenamento qualquer e portanto só precisávamos dos
comandos `dmesg`, para descobrir o endereço do dispositivo, e `mount`,
para montar o dispositivo. **Infelizmente** smartphones com memória
interna são acessíveis apenas através do Media Transfer Protocol (MTP).

Instalando
==========

No Debian :

    $ sudo apt-get install libmtp-common
    $ sudo apt-get install mtp-tools

Encontrando dispositivo
=======================

Utilize `mtp-detect` para verificar se possui algum dispositivo
conectado. :

    $ mtp-detect
    libmtp version: 1.1.9

    Listing raw device(s)
    Device 0 (VID=2717 and PID=ff60) is UNKNOWN.

Se você obter :

    libmtp version: 1.1.9

    Listing raw device(s)
       No raw devices found.

tente:

1.  Talvez você precise editar `/lib/udev/rules.d/69-libmtp.rules`. Por
    exemplo :

        # Xiaomi Redmi 2 (MTP)
        ATTR{idVendor}=="2717", ATTR{idProduct}=="ff60", SYMLINK+="libmtp-%k", MODE="660", GROUP="audio", ENV{ID_MTP_DEVICE}="1", ENV{ID_MEDIA_PLAYER}="1"

        # Xiaomi Redmi 2 Pro (MTP)
        ATTR{idVendor}=="2717", ATTR{idProduct}=="ff", SYMLINK+="libmtp-%k", MODE="660", GROUP="audio", ENV{ID_MTP_DEVICE}="1", ENV{ID_MEDIA_PLAYER}="1"

        # Xiaomi Redmi 2 (MTP+ADB)
        ATTR{idVendor}=="2717", ATTR{idProduct}=="ff68", SYMLINK+="libmtp-%k", MODE="660", GROUP="audio", ENV{ID_MTP_DEVICE}="1", ENV{ID_MEDIA_PLAYER}="1"

    Se você editar `/lib/udev/rules.d/69-libmtp.rules` você precisará :

        $ sudo service udev restart

2.  Editar `~/.android/adb_usb.ini`. Por exemplo :

        # ANDROID 3RD PARTY USB VENDOR ID LIST -- DO NOT EDIT.
        # USE 'android update adb' TO GENERATE.
        # 1 USB VENDOR ID PER LINE.
        0x2717

Após qualquer uma das tentativas anteriores, desconecte o cabo USB e
reconecte-o novamente.

Navegando no sistema de arquivos e outras operações
===================================================

Utilize `mtp-folders` para listar diretórios. Por exemplo :

    $ mtp-folders
    Attempting to connect device(s)
    Device 0 (VID=2717 and PID=ff60) is UNKNOWN.
    mtp-folders: Successfully connected
    Android device detected, assigning default bug flags
    Friendly name: (NULL)
    Storage: Internal storage
    1       Music
    2       Podcasts
    3       Ringtones
    4       Alarms
    5       Notifications
    6       Pictures
    7       Movies
    8       Download
    9       DCIM
    31        Screenshots
    10      Android
    11        data
    12          com.miui.systemAdSolution
    13            cache
    14          com.android.email
    15            files
    16              Ringtones
    17          com.android.deskclock
    18            files
    38          com.touchtype.swiftkey
    39            files
    40              mr_IN
    41              bn_IN
    42              id_ID
    43              ms_MY
    44              en_GB
    45              hg_IN
    46              ur_PK
    47              hi_IN
    48              ta_IN
    49              te_IN
    50              en_US
    51          com.google.android.googlequicksearchbox
    52            files
    53              pending_blobs
    54          com.miui.gallery
    55            cache
    19      MIUI
    20        backup
    21          AllBackup
    22        debug_log
    23          Mms
    24          com.miui.cloudservice
    OK.

Utilize `mtp-files` para obter uma lista de todos os arquivos.

Utilize `mtp-filetree` para obter os arquivos como uma árvore.

Utilize `mtp-getfile` para obter um arquivo. Você pode passar tando o
`File ID` como o endereço do arquivo.

Utilize `mtp-sendfile` para enviar um arquivo. Você precisa passar o
arquivo a ser enviado e o local de destino dele.

Utilize `mtp-delfile` para remover um arquivo. Você pode passar tanto o
`File ID` como o endereço do arquivo.

Montando o dispositivo
======================

<div class="admonition">

Requisito

Abra o arquivo `/etc/fuse.conf` e descomente a linha contendo
`user_allow_other`.

</div>

Também é possível montar o dispositivo utilizando:

> \$ mtpfs -o allow\_other \~/mnt

Para desmontar o dispositivo utilize:

> \$ fusermount -u \~/mnt

Referências
===========
