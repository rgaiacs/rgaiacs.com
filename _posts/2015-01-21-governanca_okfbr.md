---
layout: post
title: "Governança da Open Knowledge Brasil"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>A <a href="http://br.okfn.org/">Open Knowledge Brasil</a> (OKBr) consegui uma consultoria da <a href="http://www.integrah.com.br/">Integrah</a> para tentar uma melhoria na sua governança e o Daniel Maldaner será o consultor. O Daniel me pediu para responder algumas perguntas e achei adequado respondê-las publicamente.</p>
<div class="more">

</div>
<h1 id="qual-é-a-sua-contribuição-para-a-organização">Qual é a sua contribuição para a organização?</h1>
<p>Me envolvi com a OKBr depois do <a href="https://pt.wikiversity.org/wiki/Encontro_de_Acad%C3%AAmicos_pelo_Conhecimento_Livre">Encontro de Acadêmicos pelo Conhecimento Livre</a> que aconteceu em Junho 2013 e contou com a participação de vários membros da OKBr. Nessa data a OKBr ainda não existia formalmente, <a href="http://br.okfn.org/sobre/">a assembléia de fundação ocorreu em 4 de setembro de 2013</a>, mas a conversa para criação da organização já estava em estado avançado.</p>
<p>Minha maior contribuição para a OKBr tem sido através do <a href="http://cienciaaberta.net/">grupo de trabalho em ciência aberta</a> no qual tenho ajudado o <a href="https://mobiliza.org.br/channel/aleabdo">Alexandre Abdo</a> na tarefa de não deixar o grupo morrer.</p>
<p>Fora do grupo de trabalho em ciência aberta, tento dar feedbacks para os <a href="http://br.okfn.org/projetos/">projetos da OKBr</a> quando estes são lançados porque, infelizmente, é impossível acompanhar todos eles.</p>
<h1 id="na-sua-visão-o-que-deve-ser-conservado-na-organização">Na sua visão, o que deve ser conservado na organização?</h1>
<p>Acho que é mais fácil falar no que deve ser alterado e assumir que aqui que não for mencionado deve ser conservado.</p>
<h1 id="na-sua-visão-o-que-deve-ser-criado-na-organização">Na sua visão, o que deve ser criado na organização?</h1>
<p>Atualmente a OKBr está organizada em</p>
<ul>
<li><a href="http://br.okfn.org/conselhos/#consultivo">Conselho Consultivo</a></li>
<li><a href="http://br.okfn.org/conselhos/#fiscal">Conselho Fiscal</a></li>
<li><a href="http://br.okfn.org/conselhos/#deliberativo">Conselho Deliberativo</a></li>
<li>Diretoria Executiva</li>
<li><a href="http://br.okfn.org/time/">Funcionários</a></li>
<li>Comunidade ou Associados</li>
</ul>
<p>O Conselho Consultivo deveria deixar de existir por não estar definido no <a href="http://br.okfn.org/estatuto-social-da-open-knowledge-foundation-brasil/">estatudo da OKBr</a> e sua atividade não ser bastante clara.</p>
<p>O Conselho Deliberativo deveria emitir relatórios mensais ou bimestrais para a que a comunidade tivesse uma idéia das atividades realizadas pela OKBr naquele período. O <a href="http://br.okfn.org">blog da OKBr</a> possui bastante ruído para quem está interessado nas últimas atividades da OKBr.</p>
<p>A comunidade é muito fragmentada pois <a href="http://br.okfn.org/projetos/">boa parte dos projetos da OKBr</a> poderiam ser categorizados como protótipos ou localização de um projeto da <a href="http://okfn.org/">Open Knowledge Foundation</a>. Seria interessante ter alguns projetos que envolvessem a comunidade de maneira mais efetiva.</p>
<p>Um programa de parceiros que tivessem metas de contribuição em horas de trabalho ou em valores financeiros seria interessante. Por exemplo, um laboratório de alguma universidade poderia ser parceiro e se comprometer em realizar encontros semestrais para tratar de algum assunto e um coworkspace poderia ser parceiro e se comprometer a ajudar com a contabilidade.</p>
<p>Um programa de &quot;fellowship&quot; também seria interessante pois aumentaria a visibilidade do trabalho desenvolvido por alguns associados e aumentaria o engajamentos destes com a OKBr.</p>
<p>Por último, um programa de &quot;comprometimento&quot; para associados seria maravilhoso. Por exemplo, perguntar se alguém não gostaria de se comprometer em doar uma hora semanal para traduzir um pequeno texto de uma lista que temos interesse de divulgar.</p>
<h1 id="na-sua-visão-o-que-a-organização-deve-deixar-de-fazer">Na sua visão, o que a organização deve deixar de fazer?</h1>
<p>As atividades ainda estão muito centralizadas no diretor executivo que possui dificuldade em comunicar para a comunidade as tarefas que precisa de ajuda ou gostaria de delegar.</p>
<h1 id="quais-são-os-processos-que-impedem-o-desenvolvimento-da-organização">Quais são os processos que impedem o desenvolvimento da organização?</h1>
<p>A comunicação interna é o maior problema da organização. Boa parte dos informes <a href="http://br.okfn.org/">no blog da organização</a> e <a href="https://lists.okfn.org/pipermail/okfn-br/">na lista de email</a> são anúncios de realização. Se tivesse um maior números de convites para participação no blog e manifestações de interesse na lista de email provavelmente estaríamos fazendo mais coisas.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>OKBr</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
