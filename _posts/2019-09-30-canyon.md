---
layout: post
title: "Sesriem Canyon"
author: "Raniere Silva"
categories: travel
tags: ["Africa", "Namibia", "Sossusvlei", "Sesriem Canyon"]
image:
  feature: 2019-09-30-canyon.jpg
  author:  Raniere Silva
  licence: CC-BY
---

The last stop of my adventures in Namibia was the canyon.

{% include figure.html filename="2019-09-30-canyon-2.jpg" alternative_text="Photo of canyon" caption="Tree inside the canyon." author="Raniere Silva" licence="CC-BY" %}

{% include figure.html filename="2019-09-30-canyon-3.jpg" alternative_text="Photo of canyon" caption="View from inside the canyon." author="Raniere Silva" licence="CC-BY" %}

{% include figure.html filename="2019-09-30-canyon-4.jpg" alternative_text="Photo of canyon" caption="Raniere in the canyon." author="Raniere Silva" licence="CC-BY" %}

For a one last sun set.

{% include figure.html filename="2019-09-30-canyon-5.jpg" alternative_text="Photo of sun set" caption="Sun set." author="Raniere Silva" licence="CC-BY" %}