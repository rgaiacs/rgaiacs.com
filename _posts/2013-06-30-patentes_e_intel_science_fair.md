---
layout: post
title: "Patentes e Intel Science Fair"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p><a href="http://www.sciserv.org/isef/">Intel International Science and Engineering Fair</a> ou Intel ISEF é uma das maiores feiras de ciências do planeta voltada para estudantes não universitários. Para mim ela é importante pois:</p>
<ol type="1">
<li>motiva as gerações mais novas a fazer ciência e</li>
<li>é um ótimo espaço amostrar das mentes mais promissoras dos próximos 50 anos na área de ciência.</li>
</ol>
<p>Infelizmente na edição deste ano uma coisa muito surpreendente aconteceu: o ganhador do primeiro lugar na Intel ISEF foi processado por violação de patentes devido ao seu projeto vencedor. Para saber mais veja <a href="http://www.techdirt.com/articles/20130531/01375223265/teenager-cant-innovate-cell-phone-battery-because-patent-thicket.shtml">esse artigo no techdirt</a> e <a href="http://www.networkworld.com/news/2013/052913-battery-breakthrough-270237.html">esse outro no networkwold</a>.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>none</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
