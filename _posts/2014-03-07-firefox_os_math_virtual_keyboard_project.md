---
layout: post
title: "Firefox OS Math Virtual Keyboard Project"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>This is a little long description of my <a href="https://wiki.mozilla.org/Community:SummerOfCode14:Brainstorming#Firefox_OS_.2F_Boot2Gecko">Math virtual keyboard</a> proposal for GSOC.</p>
<figure>
<img src="/images/basic-features.png" class="align-center" style="width:80.0%" />
</figure>
<div class="more">

</div>
<p>To try explain why I believe that Firefox OS need a math virtual keyboard it's important that you know how math expressions can be virtually stored.</p>
<h1 id="plain-text-world">Plain text world</h1>
<p>In the first years of the digital ages, when only a few can use a computer, the only way to store information was in plain text files or binaries.</p>
<p>In the last years of this world, <a href="https://en.wikipedia.org/wiki/Donald_Knuth">Donald Knuth</a> release <a href="https://en.wikipedia.org/wiki/TeX">TeX</a>, a typesetting system with amazing mathematical support that use plain text files as input. For example, the following code represent the root of a quadratic equation.</p>
<div class="literalinclude">
<p>sample.tex</p>
</div>
<p>TeX became very famous and still be using nowadays in many places, e.g. Wikipedia, because is one of the ways to store mathematical expressions in plain text.</p>
<h1 id="gui-world">GUI world</h1>
<p>With the appearance of the personal computer and graphical user interfaces (GUI) we saw new ways to store information. Every program had their own file format/type that can only be read by the program that wrote it.</p>
<figure>
<img src="/images/libreoffice.jpg" class="align-center" style="width:40.0%" />
</figure>
<p>The great advantage of GUIs is to make easier to user execute some jobs. Instead of learning a &quot;crip&quot; language like TeX to write some mathematical expressions the user can clicks in some GUI and have the expression they want.</p>
<h1 id="web-world">Web world</h1>
<p>Today we want that many programs, in different devices, open the same file and for that some organizations wrote documents about how information <strong>must</strong> be stored. For mathematical expression one of this document is the recommendation wrote by W3C named <a href="http://www.w3.org/TR/MathML3/">MathML 3.0</a>.</p>
<p>In the case of some devices, e.g. cell phones and tablets, we only have available a virtual keyboard what is bad because, normally, we don't type as fast as we do in a real one but have the advantage of be easily customized.</p>
<h1 id="demo">Demo</h1>
<p>In the previous months I and Frédéric work in a <a href="https://github.com/fred-wang/TeXZilla">Javascript parser from TeX to MathML</a> and some demos that use this parser. One of this examples is a <a href="http://r-gaia-cs.github.io/TeXZilla-webapp/">mathematical expression notebook webapp for Firefox OS</a>. Unfortunately, type TeX's long commands is painful in current virtual keyboard and because of it we want to add math support to the keyboard.</p>
<figure>
<img src="/images/home.png" class="align-center" style="width:40.0%" />
</figure>
<p>We could wrote the math virtual keyboard in a another app but we believe that the user experience will be better with native support and other apps could take advantage of this feature.</p>
<figure>
<img src="/images/native-keyboard.png" class="align-center" style="width:40.0%" />
</figure>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>none</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
