---
layout: post
title: "Resolutions"
author: "Raniere Silva"
categories: blog
tags: []
image:
  feature: 2019-01-25-resolutions.jpg
  author: Guy Kilroy
  licence: CC-BY-SA
  original_url: https://flic.kr/p/rAMFRw
---

[Angela Robertson](https://twitter.com/arobertson98) wrote about resolutions of an open leader
at [opensource.com](https://opensource.com/open-organization/19/1/leadership-goals). Angela's list include

1. Share objectives and key results I've identified for my team. Report on progress monthly.
2. Write quarterly reflections and quarterly goals. Transparently share when I've made mistakes and what I've learned from the missteps.
3. Plan work in two-week sprints.
4. Write proposals down so I can share them transparently for feedback.
5. Travel to Israel to meet team members who work in the country.
6. Regularly unsubscribe to email lists or other notifications that might distract.
7. Co-lead a leadership development program.
8. Delegate transparently.
9. Hold weekly one-on-one and weekly team meetings.
10. Continue to work with my mentors.
11. Continue to serve as a mentor to others.
12. Support my partner's career.
13. Work with my youngest son on a coding project.
14. Become a better team member for my peers while also building and developing my team.
15. Take a meaningful vacation. No checking of email.
16. Read 12 books that relate to improving my leadership and management skills (audiobooks count).
17. Write one article a month for Opensource.com.
18. Visit 6 colleges with my older son and learn about their CS programs.
19. Continue to meditate regularly.

I really like Angela's list
and I should import some to my list,
specially

- Plan work in two-week sprints.
- Continue to work with my mentors.
- Continue to serve as a mentor to others.
- Take a meaningful vacation. No checking of email.
- Read 12 books.
