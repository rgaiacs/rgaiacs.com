---
layout: post
title: "IRC, Wiki, Git e Fórum como ferramentas para ensino de Linux e colaboratividade"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>A palestra &quot;IRC, Wiki, Git e Fórum como ferramentas para ensino de Linux e colaboratividade&quot; foi realizada por Gabriel Fedel, que tive a oportunidade de conhecer na semana anterior ao FISL14 em uma reunião sobre o projeto &quot;Jovens Hackers&quot; (maiores informações sobre o projeto <a href="https://fdc.etherpad.mozilla.org/13">nesse pad</a>).</p>
<figure>
<img src="/images/irc_wiki_git_e_forum_0.jpg" class="align-center" style="width:80.0%" />
</figure>
<p>Essa apresentação baseou-se no trabalho que ele realizou durante o primeiro semestre desse ano na faculdade em que é professor (ministrou duas disciplinas para cursos relacionados com computação).</p>
<figure>
<img src="/images/irc_wiki_git_e_forum_1.jpg" class="align-center" style="width:80.0%" />
</figure>
<p>Estava muito animado pela palestra do Gabriel pelo assunto me interessar e gostei muito dela.</p>
<p>No &quot;experimento&quot; realizado os alunos poderiam ganhar até um ponto extra na nota pelas atividades (mais relatório) em IRC, Wikis, Git, e Fóruns (o aluno deveria escolher um). Aproximandamente um quarto dos alunos mostraram interesse inicialmente em realizar as atividades sendo que alguns desistiram durante o semestre.</p>
<p>Em relação aos resultados apresentados, tenho os seguintes comentários:</p>
<ul>
<li>Gostaria que os alunos tivessem utilizado IRC e Git pois já existem bastante trabalhos feitos com Wikis e Fóruns.</li>
<li>Como fazer para os alunos não contribuam apenas durante a disciplina mas depois desta?</li>
</ul>
<div class="note">
<div class="admonition-title">
<p>Note</p>
</div>
<p>O Gabriel me enviou a palestra por email para que eu deixa-se disponível. Você pode fazer o download dela <span data-role="download">aqui &lt;irc_wiki_git_e_forum.odp&gt;</span>.</p>
</div>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>FISL</p>
</div>
<div class="tags">
<p>FISL14</p>
</div>
<div class="comments">

</div>
