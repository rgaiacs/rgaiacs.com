---
layout: post
title: "Acesso aberto ultrapassa 50% das publicações"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>No dia 07 desse mês, Devon Hanel publicou um post em <a href="http://opensource.com">opensource.com</a> com o título &quot;<a href="http://opensource.com/life/13/10/tipping-point-open-access-science">Open access to scientific knowledge has reached its tipping point</a> (<span data-role="download">cópia nesse servidor &lt;tipping-point-oa.html&gt;</span>) no qual divulga um <a href="http://www.science-metrix.com/eng/news_13_08.htm">estudo europeu</a> o qual afirma que daqui a dois anos 50% de todas as publicações acadêmicas estaram disponíveis gratuitamente (das publicações de 2011 50% já encontram-se disponíveis gratuitamente).</p>
<p>Uma informação importante desse estudo é que &quot;a maioria de artigos de alguns campos como biomedicina, biologia, matemática e estatísticas encontram-se gratuitamente disponíveis e que o acesso aberto é mais limitado nas ciências sociais, humanidades, ciências aplicadas, engenharia e tecnologia&quot; (tradução e adaptação do autor de trecho do estudo europeu).</p>
<p>O estudo também envolveu o Brasil que aprensentou 63% do artigos investigados em &quot;acesso aberto&quot; devido, em parte, a grande contribuição da Scielo.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>academia</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
