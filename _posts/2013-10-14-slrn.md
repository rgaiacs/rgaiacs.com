---
layout: post
title: slrn
author: "Raniere Silva"
categories: ftb
tags: [ftb]
image:
  feature: ftb.jpg
---
slrn é um newsreader ncurses criado em 1994 por John E. Davis. Nesse
artigo será apresentado algumas dicas.

Utilizando um browser
=====================

Por padrão `U` na visualização de uma notícia parceia o arquivo a
procura de links e lista-os para seleção para ser aberto por um web
browser.

A definição do web browser a ser utilizado é feita no arquivo
`~/.slrnrc` ao atribuir valores para as variáveis `non_Xbrowser`
(browser não gráfico) e `Xbrowser` (browser gráfico). No arquivo de
configuração padrão existe vários exemplos.

<div class="admonition note">

Para utilizar um web browser gráfico é preciso configurar a variável de
ambiente `DISPLAY`. Se essa variável não estiver configurada muito
provavelmente você deve utilizar :

    $ export DISPLAY:0

Para outras informações visite[^1].

</div>

**Referências**

[^1]: [Support Center - Knowledgebase - How do I set the DISPLAY
    variable on
    Linux](http://support.objectplanet.com/esupport/index.php?_m=knowledgebase&_a=viewarticle&kbarticleid=17)
