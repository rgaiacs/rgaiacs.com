---
layout: post
title: "Crowdsourcing"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Crowdsourcing é, como informado na <a href="http://pt.wikipedia.org/wiki/Crowdsourcing">Wikipédia</a>,</p>
<blockquote>
<p>é um modelo de produção que utiliza os esforços de voluntários, geralmente espalhados pela internet, para resolver problemas.</p>
</blockquote>
<p>Embora o leitor possa pensar que crowdsourcing seja algo novo devido a recente presença na mídia de plataformas de crowdfounding como o Kickstarter, o crowdsourcing é anterior ao <a href="https://pt.wikipedia.org/wiki/SETI">SETI</a> (1995) e <a href="http://www.gnu.org">Projeto GNU</a> (1983).</p>
<p>Um dos melhores exemplos, embora fictício, de crowdsourcing que já vi encontra-se no anime Angelic Layer (2001) que é baseado no <a href="http://pt.wikipedia.org/wiki/Angelic_Layer">manga</a> de mesmo nome (1999) escrito pelo grupo CLAMP. O trecho relacionado com crowdsourcing encontra-se no <a href="http://en.wikipedia.org/wiki/List_of_Angelic_Layer_episodes">episódio 14</a> que você pode assistir <span data-role="download">aqui &lt;angelic_layer_explain.webm&gt;</span> (dublagem disponível apenas em inglês).</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>none</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
