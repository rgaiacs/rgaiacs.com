---
layout: post
title: "Principles"
author: "Raniere Silva"
categories: blog
tags: []
image:
  feature: 2019-01-19-principles.jpg
---

I have "[Principles](https://www.principles.com/)" by Ray Dalio
on my bed side for a couples of months.
I found out about the book because of [Atul Jha](http://atuljha.com/)
and I'm looking to write something about the book since I started reading it.

The book has three parts:

1. Where I'm coming from
2. Life Principles
3. Work Principles

In the introduction,
Ray says
"I wouldn't mind if you decided to skil [the first] part of this book"
so I started with "Life Principles".
And they are five:

1. Embrace reality and deal with it
2. Use the 5-step process to get what you want out of life
3. Be radically open-minded
4. Understand that people are wired very differently
5. Learn how to make decisions effectively

The chapters aren't very long but they require time to reflect about their content.
One great thing about the book is that it has a summary.
My favourite lessons, extracted from the summary, are

## Embrace reality and deal with it

> Radical open-mindedness and radical transparency are invaluable for rapid learning and effective change.
> (...)
> Embracing radical truth and radical transparency will bring more meaningful work and more meaningful relationships.

## Use the 5-step process to get what you want out of life

> Identify and don't tolerate problems.
> (...)
> Diagnose problems to get at their root causes.

## Be radically open-minded

> Understand your ego barrier.
> (...)
> Understand your blind spot barrier.

## Understand that people are wired very differently

> Meaningful work and meaningful relationships aren't just nice things we chose for ourselves---they are genetically programmed into us.

## Learn how to make decisions effectively

> Convert your principles into algorithms
> and have the computer make decions alongside you.