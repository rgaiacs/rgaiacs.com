---
layout: post
title: Firefox Responsive Design View
author: "Raniere Silva"
categories: ftb
tags: [ftb]
image:
  feature: ftb.jpg
---
É possível "desenhar" páginas html que respondam a diferentes tamanhos
de tela utilizando CSS (por meio de seletores). Para testar a página é
preciso reajustar o tamanho do browser para os diferentes tamanhos de
tela ou utilizar uma ferramenta que faça isso (e.g. Responsive Design
View do Firefox).

Para habilitar essa funcionalidade do Firefox basta selecioná-la no
submenu "Web Developer".

Resoluções de Tela
==================

As resoluções pre-configuradas no Firefox encontram-se na tabela abaixo.

  -------------------- ---------------------------------------------------
  Resolução de tela    Dispositivos que costuma utilizar essa resolução
  320x480              Celulares
  360x640              
  768x1024             
  800x1280             
  980x1280             
  1280x600             Browsers na maioria dos notebooks
  1920x900             Browsers em monitores full-hd
  -------------------- ---------------------------------------------------

Lembrando que é possível rotacionar cada uma dessas resoluções.

**Referências**
