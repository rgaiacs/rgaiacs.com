---
layout: post
title: "Math Virtual Keyboard Specifications - Version 0.2"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>This is a updated to the math virtual keyboard specifications.</p>
<div class="more">

</div>
<h1 id="layout">Layout</h1>
<p>We can save one key having only one key to include the open and end brackets. The version save this key to add set operations.</p>
<figure>
<img src="/images/fxos-math-keyboard-4rows-v0.2a.svg" class="align-center" style="width:100.0%" />
</figure>
<h1 id="alternative-keys">Alternative Keys</h1>
<div class="admonition">
<p>Note</p>
<p>You can find a full list of Unicode characters and corresponding LaTeX command at <a href="http://milde.users.sourceforge.net/LUCR/Math/unimathsymbols.pdf" class="uri">http://milde.users.sourceforge.net/LUCR/Math/unimathsymbols.pdf</a>.</p>
</div>
<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Main Key</td>
<td>Alternative Key</td>
</tr>
<tr class="even">
<td>Symbol | Unicode | (La)TeX</td>
<td>Symbol | Unicode | (La)TeX</td>
</tr>
<tr class="odd">
<td>√x | | \sqrt{}</td>
<td>n√x | | \sqrt[n]{}</td>
</tr>
<tr class="even">
<td><dl>
<dt>&lt; | 0003C | &lt;</dt>
<dd><div class="line-block">        | +
        |
        | +
        |</div>
</dd>
</dl></td>
<td><blockquote>
<p>≤ | 02264 | \leq</p>
</blockquote>
<dl>
<dt>----------+---------+---------------------------------+</dt>
<dd><p>≦ | 02266 | \leqq</p>
</dd>
<dt>----------+---------+---------------------------------+</dt>
<dd><p>≪ | 0226A | \ll</p>
</dd>
</dl></td>
</tr>
<tr class="odd">
<td><dl>
<dt>&gt; | 0003E | &gt;</dt>
<dd><div class="line-block">        | +
        |
        | +
        |</div>
</dd>
</dl></td>
<td><blockquote>
<p>≥ | 02265 | \geq</p>
</blockquote>
<dl>
<dt>----------+---------+---------------------------------+</dt>
<dd><p>≧ | 02267 | \geqq</p>
</dd>
<dt>----------+---------+---------------------------------+</dt>
<dd><p>≫ | 0226B | \gg</p>
</dd>
</dl></td>
</tr>
<tr class="even">
<td><dl>
<dt>= | 0003D | =</dt>
<dd><div class="line-block">        | +
        |
        | +
        |
        | +
        |</div>
</dd>
</dl></td>
<td><blockquote>
<p>≡ | 02261 | \equiv</p>
</blockquote>
<dl>
<dt>----------+---------+---------------------------------+</dt>
<dd><p>≠ | 02260 | \neq</p>
</dd>
<dt>----------+---------+---------------------------------+</dt>
<dd><p>~ | 0223C | \sim</p>
</dd>
<dt>----------+---------+---------------------------------+</dt>
<dd><p>≈ | 02248 | \approx</p>
</dd>
</dl></td>
</tr>
<tr class="odd">
<td><code>|x|</code> | | <code>||</code></td>
<td>‖x‖ | | \</td>
</tr>
<tr class="even">
<td><dl>
<dt>∫ | 0222B | \int</dt>
<dd><div class="line-block">        | +
        |
        | +
        |
        | +
        |
        | +
        |
        | +
        |
        | +
        |</div>
</dd>
</dl></td>
<td><blockquote>
<p>∬ | 0222C | \iint</p>
</blockquote>
<dl>
<dt>----------+---------+---------------------------------+</dt>
<dd><p>∭ | 0222D | \iint</p>
</dd>
<dt>----------+---------+---------------------------------+</dt>
<dd><p>∮ | 0222E | \oint</p>
</dd>
<dt>----------+---------+---------------------------------+</dt>
<dd><p>∯ | 0222F | \oiint</p>
</dd>
<dt>----------+---------+---------------------------------+</dt>
<dd><p>∰ | 02230 | \oiiint</p>
</dd>
<dt>----------+---------+---------------------------------+</dt>
<dd><p>∲ | 02232 | \varointclockwise</p>
</dd>
<dt>----------+---------+---------------------------------+</dt>
<dd><p>∳ | 02233 | \ointctrclockwise</p>
</dd>
</dl></td>
</tr>
<tr class="odd">
<td>∑ | 02211 | \sum</td>
<td>∏ | 0220F | \prod</td>
</tr>
<tr class="even">
<td><dl>
<dt><code>(x)</code> | | ()</dt>
<dd><div class="line-block">        | +
        |
        | +
        |
        | +
        |
        | +
        |
        | +
        |</div>
</dd>
</dl></td>
<td><blockquote>
<p>[x] | | []</p>
</blockquote>
<dl>
<dt>----------+---------+---------------------------------+</dt>
<dd><p>{x} | | \{\}</p>
</dd>
<dt>----------+---------+---------------------------------+</dt>
<dd><p>⟨x⟩ | | \langle\rangle</p>
</dd>
<dt>----------+---------+---------------------------------+</dt>
<dd><p>⟪x⟫ | | \lang\lang</p>
</dd>
<dt>----------+---------+---------------------------------+</dt>
<dd><p>⌈x⌉ | | \lceil\rceil</p>
</dd>
<dt>----------+---------+---------------------------------+</dt>
<dd><p>⌊x⌋ | | \lfloor\rfloor</p>
</dd>
</dl></td>
</tr>
<tr class="odd">
<td><dl>
<dt>a b | | \begin{matrix}\end{matrix}</dt>
<dd><div class="line-block">        | +
        |
        | +
        |
        | +
        |
        | +
        |
        | +
        |</div>
</dd>
</dl></td>
<td><blockquote>
<p>(a b) | | \begin{pmatrix}\end{pmatrix}</p>
</blockquote>
<dl>
<dt>----------+---------+---------------------------------+</dt>
<dd><p>[a b] | | \begin{bmatrix}\end{bmatrix}</p>
</dd>
<dt>----------+---------+---------------------------------+</dt>
<dd><p>{a b} | | \begin{Bmatrix}\end{Bmatrix}</p>
</dd>
<dt>----------+---------+---------------------------------+</dt>
<dd><p>|a b| | | \begin{vmatrix}\end{vmatrix}</p>
</dd>
<dt>----------+---------+---------------------------------+</dt>
<dd><p>‖a b‖ | | \begin{Vmatrix}\end{Vmatrix}</p>
</dd>
<dt>----------+---------+---------------------------------+</dt>
<dd><p>{a | | \begin{cases}\end{cases}</p>
</dd>
</dl></td>
</tr>
<tr class="even">
<td><dl>
<dt>x̂ | 00302 | \hat{x}</dt>
<dd><div class="line-block">        | +
        |
        | +
        |
        | +
        |
        | +
        |
        | +
        |
        | +
        |
        | +
        |
        | +
        |
        | +
        |</div>
</dd>
</dl></td>
<td><blockquote>
<p>x̌ | 0030C | \check{x}</p>
</blockquote>
<dl>
<dt>----------+---------+---------------------------------+</dt>
<dd><p>x̀ | 00300 | \grave{x}</p>
</dd>
<dt>----------+---------+---------------------------------+</dt>
<dd><p>x́ | 00303 | \acute{x}</p>
</dd>
<dt>----------+---------+---------------------------------+</dt>
<dd><p>x̃ | 00303 | \tilde{x}</p>
</dd>
<dt>----------+---------+---------------------------------+</dt>
<dd><p>x̄ | 00304 | \bar{x}</p>
</dd>
<dt>----------+---------+---------------------------------+</dt>
<dd><p>x̆ | 00306 | \breve{x}</p>
</dd>
<dt>----------+---------+---------------------------------+</dt>
<dd><p>x̊ | 0030A | \mathring{x}</p>
</dd>
<dt>----------+---------+---------------------------------+</dt>
<dd><p>ẋ | 00307 | \dot{x}</p>
</dd>
<dt>----------+---------+---------------------------------+</dt>
<dd><p>ẍ | 00308 | \ddot{x}</p>
</dd>
<dt>----------+---------+---------------------------------+</dt>
<dd><p>x⃑ | 020D1 | \vec{x}</p>
</dd>
</dl></td>
</tr>
<tr class="odd">
<td><dl>
<dt>∪ | 0222A | \cup</dt>
<dd><div class="line-block">        | +
        |
        | +
        |</div>
</dd>
</dl></td>
<td><blockquote>
<p>∩ | 02229 | \cap</p>
</blockquote>
<dl>
<dt>----------+---------+---------------------------------+</dt>
<dd><p>∨ | 02228 | \vee</p>
</dd>
<dt>----------+---------+---------------------------------+</dt>
<dd><p>∧ | 02227 | \wedge</p>
</dd>
</dl></td>
</tr>
<tr class="even">
<td><dl>
<dt>∃ | 02203 | \exists</dt>
<dd><div class="line-block">        | +
        |
        | +
        |
        | +
        |
        | +
        |</div>
</dd>
</dl></td>
<td><blockquote>
<p>∄ | 02204 | \nexists</p>
</blockquote>
<dl>
<dt>----------+---------+---------------------------------+</dt>
<dd><p>∀ | 02200 | \forall</p>
</dd>
<dt>----------+---------+---------------------------------+</dt>
<dd><p>∁ | 02201 | \complement</p>
</dd>
<dt>----------+---------+---------------------------------+</dt>
<dd><p>∅ | 02205 | \vernothing</p>
</dd>
<dt>----------+---------+---------------------------------+</dt>
<dd><p>∞ | 0221E | \infty</p>
</dd>
</dl></td>
</tr>
<tr class="odd">
<td><dl>
<dt>⊂ | 02282 | \subset</dt>
<dd><div class="line-block">        | +
        |
        | +
        |
        | +
        |
        | +
        |
        | +
        |
        | +
        |</div>
</dd>
</dl></td>
<td><blockquote>
<p>⊃ | 02283 | \supset</p>
</blockquote>
<dl>
<dt>----------+---------+---------------------------------+</dt>
<dd><p>⊄ | 02284 | \nsubset</p>
</dd>
<dt>----------+---------+---------------------------------+</dt>
<dd><p>⊅ | 02285 | \nsupset</p>
</dd>
<dt>----------+---------+---------------------------------+</dt>
<dd><p>⊆ | 02286 | \subseteq</p>
</dd>
<dt>----------+---------+---------------------------------+</dt>
<dd><p>⊇ | 02287 | \supseteq</p>
</dd>
<dt>----------+---------+---------------------------------+</dt>
<dd><p>⊈ | 02288 | \nsubseteq</p>
</dd>
<dt>----------+---------+---------------------------------+</dt>
<dd><p>⊉ | 02289 | \nsupseteq</p>
</dd>
</dl></td>
</tr>
<tr class="even">
<td><dl>
<dt>← | 02190 | \leftarrow</dt>
<dd><div class="line-block">        | +
        |
        | +
        |
        | +
        |
        | +
        |</div>
</dd>
</dl></td>
<td><blockquote>
<p>↚ | 0219A | \nleftarrow</p>
</blockquote>
<dl>
<dt>----------+---------+---------------------------------+</dt>
<dd><p>⟵ | 027F5 | \longleftarrow</p>
</dd>
<dt>----------+---------+---------------------------------+</dt>
<dd><p>⇐ | 021D0 | \Leftarrow</p>
</dd>
<dt>----------+---------+---------------------------------+</dt>
<dd><p>⇍ | 021CD | \nLeftarrow</p>
</dd>
<dt>----------+---------+---------------------------------+</dt>
<dd><p>⟸ | 027F8 | \Longleftarrow</p>
</dd>
</dl></td>
</tr>
<tr class="odd">
<td><dl>
<dt>↔ | 02194 | \leftrightarrow</dt>
<dd><div class="line-block">        | +
        |
        | +
        |
        | +
        |
        | +
        |</div>
</dd>
</dl></td>
<td><blockquote>
<p>↮ | 021AE | \nleftrightarrow</p>
</blockquote>
<dl>
<dt>----------+---------+---------------------------------+</dt>
<dd><p>⟷ | 027F7 | \longleftrightarrow</p>
</dd>
<dt>----------+---------+---------------------------------+</dt>
<dd><p>⇔ | 021D4 | \Leftrightarrow</p>
</dd>
<dt>----------+---------+---------------------------------+</dt>
<dd><p>⇎ | 021CE | \nLeftrightarrow</p>
</dd>
<dt>----------+---------+---------------------------------+</dt>
<dd><p>⟺ | 027FA | Longleftrightarrow</p>
</dd>
</dl></td>
</tr>
<tr class="even">
<td><dl>
<dt>→ | 02192 | \rightarrow</dt>
<dd><div class="line-block">        | +
        |
        | +
        |
        | +
        |
        | +
        |</div>
</dd>
</dl></td>
<td><blockquote>
<p>↛ | 0219B | \nrightarrow</p>
</blockquote>
<dl>
<dt>----------+---------+---------------------------------+</dt>
<dd><p>⟶ | 027F6 | \longrightarrow</p>
</dd>
<dt>----------+---------+---------------------------------+</dt>
<dd><p>⇒ | 021D2 | \Rightarrow</p>
</dd>
<dt>----------+---------+---------------------------------+</dt>
<dd><p>⇏ | 021CF | \nRightarrow</p>
</dd>
<dt>----------+---------+---------------------------------+</dt>
<dd><p>⟹ | 027F9 | \Longrightarrow</p>
</dd>
</dl></td>
</tr>
<tr class="odd">
<td><dl>
<dt>. | 0002e | .</dt>
<dd><div class="line-block">        | +
        |
        | +
        |</div>
</dd>
</dl></td>
<td><blockquote>
<p>∴ | 02234 | \therefore</p>
</blockquote>
<dl>
<dt>----------+---------+---------------------------------+</dt>
<dd><p>∵ | 02235 | \because</p>
</dd>
<dt>----------+---------+---------------------------------+</dt>
<dd><dl>
<dt>| 0003a |</dt>
<dd>
</dd>
</dl>
</dd>
</dl></td>
</tr>
</tbody>
</table>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>GSOC</p>
</div>
<div class="tags">
<p>GSOC2014</p>
</div>
<div class="comments">

</div>
