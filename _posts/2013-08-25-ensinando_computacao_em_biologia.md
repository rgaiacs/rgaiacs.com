---
layout: post
title: "Ensinando computação em biologia"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Um ótimo vídeo com trechos de entrevistas com Dr. C. Titus Brown, Kaitlin Thaney e Dr. Greg Wilson na SESYNC (National Socio-Environmental Synthesis Center) foi divulgado<a href="#fn1" class="footnote-ref" id="fnref1"><sup>1</sup></a> (<span data-role="download">cópia nesse servidor
&lt;sesync-cyberinfra.webm&gt;</span>).</p>
<p><strong>Referencias</strong></p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>ciência aberta</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
<section class="footnotes">
<hr />
<ol>
<li id="fn1"><p><a href="http://www.sesync.org/video-oci-edu">VIDEO: Materials &amp; Workshops for Cyberinfrastructure Education in Biology</a><a href="#fnref1" class="footnote-back">↩</a></p></li>
</ol>
</section>
