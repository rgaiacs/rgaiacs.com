---
layout: post
title: "Software Carpentry Remote Workshop at University of Campinas"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<figure>
<img src="/images/screen.jpg" class="align-center" style="width:80.0%" />
</figure>
<p>On June 04-05 I participated at the best Software Carpentry Workshop so far for those that I attended.</p>
<div class="admonition">
<p>Thanks</p>
<p>I'm grateful to <a href="http://ea2.unicamp.br/">EA2</a> that allows their room to be used for the workshop and also for provided coffee for the event.</p>
</div>
<div class="admonition">
<p>Thanks</p>
<p>I'm grateful to Jennifer Shelton, Maneesha Sane and Natalie Robinson, the Software Carpentry instructors that taught at the workshop.</p>
</div>
<div class="admonition">
<p>Thanks</p>
<p>I'm grateful to Eric Lopes, Felipe Bocca and Renato Augusto Corrêa dos Santos that helped the students a lot during the workshop.</p>
</div>
<div class="more">

</div>
<h1 id="learners">Learners</h1>
<p>At this workshop we had 23 learners <strong>from at least three countries</strong> with many affiliations (<a href="http://unicamp.br/">University of Campinas</a>, <a href="http://usp.br/">University of São Paulo</a>, <a href="http://ufscar.br/">Federal University of São Carlos</a>, <a href="http://embrapa.br/">EMBRAPA</a> and <a href="http://cnpem.br/">CNPEM</a>) and carriers stages (first-year undergraduates, PHD students and senior researchers).</p>
<figure>
<img src="/images/learners.jpg" class="align-center" style="width:80.0%" />
</figure>
<p>All learners attended both days of workshop and stay until the end.</p>
<h1 id="unix-shell">UNIX Shell</h1>
<p>The workshop started with Jennifer Shelton's lesson about the UNIX Shell. We didn't have any installation problem or serious one and the helpers managed all the small problems. The only think that I still need to understand is why the default behavior of <code>sort</code> at Mac OS X is different from the default behavior of <code>sort</code> at GNU/Linux and equals to <code>sort -n</code>. Maybe this is another case of Mac OS X and GNU/Linux difference that I will try to memorize for the future (I already know two: <code>/Users</code> vs <code>/home</code> and <code>date</code>).</p>
<figure>
<img src="/images/helper.jpg" class="align-center" style="width:80.0%" />
</figure>
<h1 id="r---first-day">R - First Day</h1>
<p>In the afternoon of the second day, Natalie Robinson taught the begin of <a href="https://swcarpentry.github.io/r-novice-inflammation/">Software Carpentry's R standard lesson</a>. Again, we didn't have any installation problem or serious one and the helpers managed all the small problems.</p>
<h1 id="rmarkdown">RMarkdown</h1>
<p>At the end of the second day, I taught a RMarkdown lesson just to advertise this awesome tool. Here we had serious problems because (1) I didn't ask the learners to download <a href="http://yihui.name/knitr/">knitr</a> in advance, (2) the internet connection wasn't very fast, and (3) in some cases <a href="http://cran.r-project.org/">a CRAN mirror</a> could not be found.</p>
<p>Also, a few learners had a old version of <a href="http://rstudio.com/">RStudio</a> that only have the option to convert RMarkdown to HTML and a different template that the one that I was using.</p>
<h1 id="pizza">Pizza</h1>
<p>To close the first day, I and the helpers joined to have some pizza and talk about what happened in the first day and others topics like open science, open access, open data, ...</p>
<h1 id="git">Git</h1>
<p>I was lazy replying some emails and made a mistake with time zones. <strong>Sorry very much to Maneesha Sane for my mistake.</strong></p>
<p>Because of my mistake, I taught the first part of the Git lesson. Again, we didn't have any installation problem or serious one and the helpers managed all the small problems.</p>
<figure>
<img src="/images/room.jpg" class="align-center" style="width:80.0%" />
</figure>
<h1 id="github">GitHub</h1>
<p>Maneesha Sane taught the second part of the Git lesson that include collaboration using GitHub and resolution of conflicts during collaboration.</p>
<p>The only problem in this part was with learners that need to use a proxy at their work and the proxy was messing when trying to send data to GitHub using Git.</p>
<h1 id="r---second-day">R - Second Day</h1>
<p>In the afternoon of the second day, Natalie Robinson joined us again to taught plots in R using the built in library.</p>
<h1 id="wrap-up">Wrap Up</h1>
<p>To close the workshop, I talked a little about Software Carpentry and Felipe talked about <a href="http://cran.r-project.org/web/packages/ggplot2/index.html">ggplot</a>.</p>
<h1 id="lhc">LHC</h1>
<p>Some participants of the workshop want to visit the hackerspace that exists in Campinas (we call it LHC since is the acronym for &quot;Laboratório Hacker de Campinas&quot;) and we went there after the workshop and talked for a while.</p>
<h1 id="feedbacks">Feedbacks</h1>
<p>Learners liked the initiative of a workshop to cover computational tools and the lessons.</p>
<p>The complains were:</p>
<ul>
<li>speaker hasn't a good sound (we replaced the speaker on the second day for a better one),</li>
<li>slow internet connection,</li>
<li>poor coffee break, and</li>
<li>echo (at instructors side).</li>
</ul>
<div class="admonition">
<p>Note</p>
<p>I'm waiting to get the feedback from the instructors to know how hard was the workshop.</p>
</div>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>academia,educação,Software Carpentry</p>
</div>
<div class="tags">
<p>Campinas,UNICAMP,UNIX Shell,R,Git,GitHub</p>
</div>
<div class="comments">

</div>
