---
layout: post
title: "Life of Trees"
author: "Raniere Silva"
categories: blog
tags: []
image:
  feature: 2019-02-25-life-of-trees.jpg
  author: David Sim
  licence: CC-BY
  original_url: https://flic.kr/p/6NaAxs
---

In the last three years,
my friend [Dayane Machado](https://twitter.com/machadodft) suggested me incredible books to read,
including "[The Art of Asking](http://amandapalmer.net/theartofasking/)" by [Amanda Palmer](http://amandapalmer.net/).
The lastest recommendation was "[The Hidden Life of Trees](https://www.goodreads.com/book/show/28256439-the-hidden-life-of-trees)" by Peter Wohlleben.

I only read one third of the book so far
but I think that Peter wrote a book about people and not trees.
About how people can living in harmony.

> When trees grow together, nutrients and water can be optimally divided among them all so that each tree can grow into the best tree it ca be. (...) a tree can be only as strong as the forest that surrounds it.