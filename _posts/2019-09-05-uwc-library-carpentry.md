---
layout: post
title: "Teaching Library Carpentry at the University of the Western Cape"
author: "Raniere Silva"
categories: teaching
tags: ["Africa", "South Africa", "Cape Town", "University of the Western Cape", "Library Carpentry"]
image:
  feature: 2019-09-05-uwc-library-carpentry.jpg
  author:  Raniere Silva
  licence: CC-BY
---

Thanks to the financial support of [South African Centre for Digital Language Resources](https://www.sadilar.org/) (SADiLaR)
and
[University of the Western Cape](https://uwc.ac.za) (UWC) Library Services,
Rooweither Mabuya,
Oghenere Salubi
and I
were able to teach a [Library Carpentry](https://librarycarpentry.org/) workshop
on [3-4 September](https://uwc-eresearch.github.io/2019-09-03-UWC_Bellville/)
at UWC.

The workshop was hosted at the University Main Library
and had 16 learners,
most from information science.

{% include figure.html filename="2019-09-05-uwc-library-carpentry-2.jpg" alternative_text="Photo of University Main Library." caption="Lobby of University Main Library." author="Raniere Silva" licence="CC-BY" %}

On the first day,
Rooweither taught an introduction to data
and regular expressions.
The cheatsheet print out of regular expressions
that Rooweither provided to the learners
was very helpful during the exercises.
In the afternoon,
I taught an introduction to Bash.
Given time constraints,
I was able to cover only until for-loops
and didn't explore how to find data within files
that is one of the applications of regular expressions.

The second day started with me teaching Git and GitHub.
The Library Carpentry lesson has a section about GitHub Pages
but many learners had problems with that section mainly due
GitHub not provide a log of the error when building the page
and, sometimes,
GitHub be slow to update the website after a new commit.
Also,
some learners didn't receive the email with the link to confirm
their account creation request.
After lunch,
Oghenere led the lesson about OpenRefine
and I learnt two new things
(the difference of rows and records and General Refine Expression Language (GREL)).

{% include figure.html filename="2019-09-05-uwc-library-carpentry-3.jpg" alternative_text="Photo of Oghenere teaching." caption="Oghenere teaching OpenRefine." author="Raniere Silva" licence="CC-BY" %}

The overwall feedback from learners was very positive
and I hope this workshop contribute to share of knowledge
regarding best data practice among researchers based at the University of the Western Cape
and their collaborators.

{% include figure.html filename="2019-09-05-uwc-library-carpentry-4.jpg" alternative_text="Photo of the instructors." caption="From left to right: Rooweither Mabuya, Oghenere Salubi and Raniere Silva." author="Raniere Silva" licence="CC-BY" %}