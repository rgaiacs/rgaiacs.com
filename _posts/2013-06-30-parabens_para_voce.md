---
layout: post
title: "Parabéns para você"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Você já ouviu pelo menos alguma vez a versão americana do &quot;Parabéns para você&quot; (o &quot;Happy Birthday&quot;), certo? Caso nunca tenha ouvido, veja alguns <a href="http://www.youtube.com/results?search_query=happy+birthday">destes vídeos</a>.</p>
<p>Entretanto acredito que você não sabia que &quot;Happy Birthday&quot; é a música mais lucrativa de todos os tempos. Isso mesmo, a Warnet/Chappell Music ganha rios de dinheiro pelo licenciamento dessa música.</p>
<p>A melhor parte é que essa musica muito provavelmente está no domínio público e os direitos autorais dela são questionáveis.</p>
<p>Para mais informações, visite <a href="http://www.techdirt.com/articles/20130613/11165823451/filmmaker-finally-aims-to-get-court-to-admit-that-happy-birthday-is-public-domain.shtml">esse artigo de techdirt</a>.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>none</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
