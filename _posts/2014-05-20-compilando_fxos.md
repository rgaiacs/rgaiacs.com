---
layout: post
title: Compilando Firefox OS para o Alcatel One Touch Fire
author: "Raniere Silva"
categories: ftb
tags: [ftb]
image:
  feature: ftb.jpg
---
<div class="admonition note">

Este post encontra-se em construção.

</div>

No post anterior &lt;../19/atualizando\_alcatel&gt; foi apresentado como
atualizar o Alcatel One Touch Fire para uma versão mais recente do
Firefox OS utilizando uma imagem disponíveis em
<http://elsimpicuitico.wordpress.com/firefoxos/>.

Neste post será coberto como compilar seu próprio Firefox OS e atualizar
o Alcatel One Touch com sua própria versão.

Preparação
==========

Para atualizar o Gaia você precisará compilar a sua versão do mesmo.
Para isso, siga os passos abaixo:

1.  Baixar o repositório B2G:

        $ git clone git://github.com/mozilla-b2g/B2G.git

2.  Acessar o diretório criado:

        $ cd B2G

3.  Configurar o diretório para o dispositivo desejado (`hamachi` é o
    *codename* do Alcatel One Touch Fire, outras informações em[^1]):

        $ ./config.sh hamachi

Compilando
==========

Para compilar:

    $ ./build.sh

<div class="admonition note">

Compilar o Firefox OS não é uma tarefa fácil e a dificuldade já começa
na hora de satisfazer as dependências (mais informações em[^2]).

</div>

Espero que sua compilação não falhe. Se falhar, espero que encontre a
solução na lista de problemas que tive.

Erros e Soluções
================

Os erros a seguir encontram-se na ordem que eles devem aparecer ao
tentar compilar o Firefox OS em um GNU/Linux logo após sua instalação.

<div class="admonition error">

`cannot bind 'local:5037'`

Tive esse problema ao misturar chamadas ao `adb` do usuário normal e do
root. Resolvi o problema removendo o arquivo `/tmp/5037`.

</div>

<div class="admonition error">

`????????????    no permissions`

Esse problema deve-se a premissões. Siga as instruções do
[jaga](http://stackoverflow.com/a/18307760/1802726), i.e., adicione:

    SUBSYSTEM==”usb”, ENV{DEVTYPE}==”usb_device”, \ MODE=”0666″

ao arquivo `/etc/udev/rules.d/91-permissions.rules`.

</div>

<div class="admonition error">

`device/qcom/b2g_common/treeid.sh: line 42: repo: command not found`

Esse é um problema de `PATH` que não deveria estar acontecendo. Para
contorná-lo você pode utilizar:

    $ export PATH=$(pwd):$PATH

</div>

<div class="admonition error">

`device/qcom/b2g_common/vendorsetup.sh: line 87: out/lastpatch.projects: No such file or directory`

Esse é outro problema que não deveria estar acontecendo. =(

</div>

<div class="admonition error">

`remote object '/system/lib/libOmxAdpcmDec.so' does not exist`

Tive esse problema ao utilizar
[v1.4-20140430](http://elsimpicuitico.wordpress.com/firefoxos/). Quando
migrei para
[master-20140422a](http://elsimpicuitico.wordpress.com/firefoxos/) esse
problema foi resolvido.

</div>

<div class="admonition error">

`GNUmakefile:4: Makefile: No such file or directory`

Esse é outro erro que não deveria estar acontecendo. =(

</div>

Referências
===========

[^1]: [Phone and device
    specs](https://developer.mozilla.org/en-US/Firefox_OS/Developer_phone_guide/Phone_specs)

[^2]: [Firefox OS build
    prerequisites](https://developer.mozilla.org/en-US/Firefox_OS/Firefox_OS_build_prerequisites)
