---
layout: post
title: "Principles app"
author: "Raniere Silva"
categories: blog
tags: []
image:
  feature: 2019-03-10-principles-app.jpg
  author: Tim Green
  licence: CC-BY
  original_url: https://flic.kr/p/4WTufT
---

Ray Dalio,
in [Principles](https://www.principles.com),
mentions a couple of apps used at Bridgewater:

- Coach: a library of common situations which are linked to the relevant principles to help people handle them;
- Dot Collector: used to allow people to express their thoughts and see others' thoughts in real time;
- Baseball Cards: data visualisation fo a person's strengths and weakness;
- Combinator: staff-job assignment based on staff profile;
- Issue Log: recording of mistakes;
- Pain Button: record emotions for later reflection;
- Dispute Resolver: paths for resolving disagreements;
- Daily Update: improve communication;
- Contract Tool: make and monitor commitments;
- Process Flow Diagram: visualisation of processes;

Some of the apps (e.g. Dot Collector and Issue Log) can be implemented on top of other online services (e.g. Google Docs)
but others can **not**. Would be great to have free and open versions of those apps.