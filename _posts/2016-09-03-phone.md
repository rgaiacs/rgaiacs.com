---
layout: post
title: Looking for a new phone
author: "Raniere Silva"
categories: ftb
tags: [ftb]
image:
  feature: ftb.jpg
---
I don't like to change phone every year but the world is against me. The
power buton of my first Motorola broked around March 2014 and I brought
a Firefox OS device. In 2015, Mozilla reduce their workforce around
Firefox OS and I bricked my device in one of the updates what made me
need to replace it with a Red Mi 2 Pro. My Red Mi 2 Pro doesn't get 4G
here in the UK and I'm considering buy a new phone.

[Andy South](https://twitter.com/southmapr) [recommended
me](https://twitter.com/southmapr/status/772137137227632641) the
[Fairphone 2](https://www.fairphone.com/phone/). I liked the fact that
Fairphone 2 is build with modules, specialy now that [Google killed
Project
Ara](http://www.technobuffalo.com/2016/09/02/project-ara-google-modular-smartphone-cancelled/).
Fairphone 2 comes Android™ 5.1 Lollipop but you can replace it with some
more free operating system.

I still need to look on Fairphone forum before decide if I going to buy
one but for now it looks more appealing than the other options in the
market. Also, I'm [waiting
comments](http://social.libreplanetbr.org/notice/94923) from some
friends.
