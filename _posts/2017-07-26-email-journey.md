---
layout: post
title: "My E-mail (client) Journey"
author: "Raniere Silva"
categories: tools
tags: [jekyll]
image:
  feature: 2017-07-26-email-journey.jpg
  author: Enrico
  original_url: https://flic.kr/p/2PDDxR
  licence: CC-BY
---

E-mail can be a very difficult thing to deal with
but is the only federated wide-used system out there
so we are stuck with it for now.
In this post I will review how I try to domesticate that beast.

## My first e-mail

I created my first e-mail account in 2004.
It was a [MSN Hotmail](https://en.wikipedia.org/wiki/Outlook.com#MSN_Hotmail) account
since at that time everyone was using MSN Hotmail to chat.
2004 was also the year that Google announced Gmail
but I wasn't cool enough to receive a invitation.
I didn't used too much my MSN Hotmail account because at the time I didn't have
internet at home.

## Yahoo! Mail

2005 was the year that I first had internet at home
and the time that I move from MSN Hotmail to [Yahoo! Mail](https://en.wikipedia.org/wiki/Yahoo!_Mail).
I don't remember why I migrate to Yahoo! Mail.
I stayed with Yahoo! Mail until 2009
and during those years I only used e-mail to subscribe to online games.

## Gmail

In 2009,
I started using [Gmail](https://en.wikipedia.org/wiki/Gmail) and Google Office Suite.
Gmail (graphical) interface was better than Yahoo! Mail
and didn't have big advertisements.
In addition,
you could enable many extensions
and I discovered that you could use filters
to automatically move new e-mails to folders,
a very nice way to organise your inbox.

## Mutt

2009 was also the year that I first heard about [Vim](https://en.wikipedia.org/wiki/Vim_(text_editor)).
Some of my friends were always telling me
all the magic stuffs that you could do with Vim.
In 2010, I decided to give Vim a try
and spend some time using it.
After a few months,
I got in love with Vim
and, as lovers, I want to do everything with Vim,
including dealing with my pile of daily emails.

To be able to use Vim to compose my emails,
I setup [Mutt](https://en.wikipedia.org/wiki/Mutt_(email_client)),
"a text-based email client for Unix-like systems" as Wikipedia describe it,
in 2011.
As any Unix-like system application,
Mutt "mostly" does only one thing very well,
display one of your inboxes,
and everything else is delegate to another application.
Because of this Unix-like way to do things,
I had to also setup
[fetchmail](https://en.wikipedia.org/wiki/Fetchmail) to retrieve my e-mails,
[procmail](https://en.wikipedia.org/wiki/Procmail) to filter my e-mails,
[msmtp](https://en.wikipedia.org/wiki/Simple_Mail_Transfer_Protocol) to send my e-mails
and [abook](https://sourceforge.net/projects/abook/) as my address book.
At the end,
I was managing tons of dotfiles.

Did worth migrate from Gmail to Mutt?
Yes, I had the feeling that my e-mail was domesticated.
The side effect was that my inbox was offline
since I was using fetchmail instead of [offlineimap](https://en.wikipedia.org/wiki/OfflineIMAP)
because synchronise large inboxes is challenge to offlineimap.

## Gnus

I used Mutt for a couple of years
but in 2015 I needed to clock my working hours
and after looking in many tools to do it
I ended up with [Org-mode](https://en.wikipedia.org/wiki/Org-mode).
Org-mode works on top of Emacs
and despite my love with Vim
I started a affair with Emacs.
At the end of 2015,
I was configuring [Gnus](https://en.wikipedia.org/wiki/Gnus)
as my new e-mail client just because it works on top of Emacs.

I could keep my fetchmail + procmail + msmtp stack
but to keep my e-mails in the cloud
I replaced that stack with Gnus built-in imap support.
For address book,
I replaced abook with [BBDB](http://savannah.nongnu.org/projects/bbdb/).

Was I happy using Gnus?
No!
In part,
my unhappiness was because I didn't have the time to polish my `~/.gnus`.
But the main reason why I was unsatisfied was that
after I started using Gnus as feed reader as well
it toke a long time to be ready to use
and crash a lot of time when starting with a "wrong type argument" error.

## Evolution

After my disappointment with Gnus,
I start looking for a new e-mail client in 2017.
The ones that made my list where
[Evolution](https://en.wikipedia.org/wiki/Evolution_(software)),
[KMail](https://en.wikipedia.org/wiki/Kontact#E-Mail),
[Thunderbird](https://en.wikipedia.org/wiki/Mozilla_Thunderbird)
and [Nylas N1](https://en.wikipedia.org/wiki/Nylas_N1).
Nylas N1 was shiny but the local setup was a nightmare (and [now is dead](https://github.com/nylas/nylas-mail/issues/3564)),
Thunderbird [was getting a new home](https://blog.mozilla.org/thunderbird/2017/05/thunderbirds-future-home/)
and KMail requires many KDE applications
when my [GNU/Linux distribution installation](https://manjaro.org/) uses Gnome applications
so I decided to give Evolution a try.

Because [GUADEC 2017](https://2017.guadec.org),
the Gnome Conference,
is this weekend I will write about my experience with Evolution
in another post.
