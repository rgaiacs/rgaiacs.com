---
layout: post
title: "Wikimedia, parabéns pelos 10 aninhos"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<div class="note">
<div class="admonition-title">
<p>Note</p>
</div>
<p>Agradecimento ao <a href="https://github.com/everton137">Tom</a> pela revisão do texto inicialmente escrito <a href="http://okfnpad.org/lU6nDS2THa">nesse pad</a>.</p>
</div>
<p>&quot;Dez anos atrás, em 20 de Junho de 2003, Jimmy Wales anunciou a fundação da organização sem fins lucrativos Wikimedia Foundation. Ele confiou a essa nova organização a administração da Wikipédia, lançada dois anos e meio antes.&quot; (Retirado de <a href="http://blog.wikimedia.org/2013/06/20/ten-years-of-supporting-free-knowledge/">Ten years of supporting free knowledge</a> e tradução do autor.) E é com enorme prazer que desejo um Feliz Aniversário a Wikimedia e votos de muitos anos de vida pela frente.</p>
<div class="more">

</div>
<p>É difícil lembrar a web antes da Wikipédia, época do Altavista, Geosites e outros. Encontrar alguma informação no idioma desejado, de qualidade e que pudesse ser reutilizada era uma tarefa difícil (ainda hoje não é muito fácil, mas a Wikipédia facilitou muito a nossa vida).</p>
<p>Nestes anos que se passaram, muito se questionou sobre a qualidade dos artigos da Wikipédia mas espero que hoje muitos já tenham sido convencidos que boa parte dos artigos que compõem a Wikipédia são de ótima qualidade e uma das poucas fontes, se não a única, cujo conteúdo pode ser reaproveitado para qualquer fim.</p>
<p>Além disso, a Wikipédia e seus (sites) irmãos nos mostram que somos capazes de criar coisas maravilhosas ao trabalharmos juntos. Ela já tem servido de inspiração para inúmeros outros projetos (não só outras wikis) e espero que não deixe de ser. Gostaria de citar os nomes de dois projetos recentes que surgiram por influência da Wikipedia.</p>
<p>O primeiro deles é o Grupo de Trabalho de Ciência Aberta que começou está se estruturando no Brasil e organizou um encontro no início deste mês no qual estavam presentes pessoas das mais diversas áreas.</p>
<p>O segundo deles é o Mozilla Science Lab anunciado na semana passada que é uma proposta da Mozilla Foundation para aproximar a web e as ferramentas colaborativas possibilitadas por essa como wikis da comunidade científica.</p>
<p>Torço para daqui a 10 anos posso voltar a escrever dando os parabéns ao Grupo de Trabalho de Ciência Aberta e ao Mozilla Science Lab pelos seus 10 aninhos e suas várias conquistas e a Wikimedia Foundation pelos 20 anos e os vários frutos gerados.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>none</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
