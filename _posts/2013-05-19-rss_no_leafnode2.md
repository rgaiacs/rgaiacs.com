---
layout: post
title: RSS no Leafnode-2
author: "Raniere Silva"
categories: ftb
tags: [ftb]
image:
  feature: ftb.jpg
---
Leafnode-2 é um servidor de NNTP que foi configurado em leafnode2. Nesse
post veremos como adicionar feeds RSS para serem lidos no Leafnode-2.

Grupos locais
=============

Uma das características do Leafnode-2 é a possibilidade de grupos
locais. Os grupos locais são informados no arquivo
`/etc/leafnode/local.groups`. Para a criação de grupos locais adicione
linhas seguindo a sintaxe abaixo:

    news.group.name<TAB>status<TAB>descrição

e.g., :

    local.test       y    Local Unmoderated Group

no arquivo `/etc/leafnode/local.groups` tomando o cuidado de utilizar
tab para separa os campos.

<div class="admonition hint">

No final deste post é apresentado como gerar o arquivo de grupos com
base na lista de feeds RSS.

</div>

RSS2Leafnode
============

O download dos feeds RSS e encaminhamento para o Leafnode-2 ficam por
conta do RSS2Leafnode, um script Perl.

As dependências do RSS2Leafnode são:

-   Perl 5.10 (ou superior)
-   XML::Twig
-   LWP networking
-   MIME-tools
-   Leafnode-2

Para verificar se as dependências estão devidamente instaladas utilize:

    $ ./Makefile.PL
    Writing Makefile for rss2leafnode
    Writing MYMETA.yml

Se aparecer alguma notificação sobre falta de dependência como :

    Warning: prerequisite Sort::Key::Top 0 not found.

utilize o `ctan` para instalá-la:

    # ctan Sort::Key:Top

Com as dependências resolvidas, para instalar:

    $ make
    # make install

A configuração é feita por meio do arquivo `~/.rss2leafnode`:

Arquivo de grupos
=================

Se você utilizar:

    fetch_rss('nome.grupo.', 'http://endereco.do.grupo.com'); # Descricao

você pode utilizar:

    $ sed -ne "s/^fetch_rss('//" -e "s/'.*\#\s/\ty\t/p" .rss2leafnode

para criar a lista de grupos. :

    $ sed -ne "s/^fetch_rss('//" -e "s/'.*\#\s/\ty\t/p" .rss2leafnode
    r2l.weather y   Adelaide weather
    r2l.misc    y   archive.org
    r2l.weather y   Adelaide weather

**Referências**
