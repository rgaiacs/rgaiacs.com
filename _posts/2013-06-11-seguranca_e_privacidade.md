---
layout: post
title: "Segurança e privacidade"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Na <a href="http://www.revista.espiritolivre.org">Revista Espírito Livre</a> apareceu a reportagem &quot;<a href="http://www.observatoriodaimprensa.com.br/news/view/_ed750_seguranca_sempre_vence_a_batalha_contra_privacidade">Segurança sempre vence a batalha contra privacidade</a>&quot; (<span data-role="download">cópia local
&lt;seguranca-sempre-vence-a-batalha-contra-privacidade.html&gt;</span>) que foi originalmente publicada pelo <a href="http://www.observatoriodaimprensa.com.br">Observatório da Imprensa</a> e corresponde a tradução de Celso Paciornik para o texto de Chris Cilliza.</p>
<div class="more">

</div>
<p>Um dos pontos interessantes da reportagem é</p>
<blockquote>
<p>Uma pesquisa Pew, de 2011, mostrou que 42% disseram que a Lei Patriota era “uma ferramenta necessária que ajuda o governo a encontrar terroristas”, enquanto 34% disseram que ela “vai longe demais”.</p>
</blockquote>
<p>que mostra o quanto o medo faz as pessoas deixarem de valorizar a sua privacidade.</p>
<p>Outro ponto interessante é</p>
<blockquote>
<p>O pensamento é: &quot;Se não estou violando nenhuma lei, por que me aborrecer com o fato de o governo investigar algumas ligações telefônicas se isso ajudar a barrar um ataque?&quot;</p>
</blockquote>
<p>Existem duas falhas nesse pensamento:</p>
<ol>
<li>É considerado que o &quot;governo&quot; não irá tentar lhe fazer nenhum mal, o que não é necessariamente verdade.</li>
<li>O governo é feito por pessoas. As poucas pessoas que tiverem acesso a essas informações podem utilizá-las para ganho pessoal.</li>
</ol>
<p>Por esses motivos acredito que deve-se preferir a privacidade.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>none</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
