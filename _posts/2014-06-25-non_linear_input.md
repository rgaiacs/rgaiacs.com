---
layout: post
title: "Nonlinear Input for (La)TeX IME"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Since (La)TeX is a programming language (yes, it is a Turing-complete language) many of the math symbols are available as commands/functions and to help user type a text is need that the IME change the cursor position based on user inputs. At this post I will show what the IME that I'm writing should do with the cursor.</p>
<div class="more">

</div>
<h1 id="notation">Notation</h1>
<p>To represent the cursor position I will use Unicode black vertical rectangle, <code>▮</code>.</p>
<h1 id="change-cursor-key">Change Cursor Key</h1>
<p>We need to define a key to move the cursor to the next &quot;place holder&quot;. The <code>RETURN</code> key is suitable to do this because have <code>\n</code> inside a command or environment is rare and most of the cases it happens its purpose is make easy to read the source code ((La)TeX treats <code>\n\n</code> as a new paragraph but <code>\n</code> is ignored).</p>
<h1 id="general-commands">General Commands</h1>
<p>(La)TeX commands are, normally, of the form <code>\command[]{}</code> and the jumps are showed below:</p>
<ul>
<li><p>Before press the key that will insert the command:</p>
<pre><code>▮</code></pre></li>
<li><p>After press the key that inserted the command:</p>
<pre><code>\command[▮]{}</code></pre></li>
<li><p>After press <code>RETURN</code>:</p>
<pre><code>\command[]{▮}</code></pre></li>
<li><p>After press <code>RETURN</code>:</p>
<pre><code>\command[]{}▮</code></pre></li>
</ul>
<h1 id="general-environments">General Environments</h1>
<p>(La)TeX environments are, normally, of the form <code>\begin{environment}[]\end{environment}</code> and the jumps are showed below:</p>
<ul>
<li><p>Before press the key that will insert the environment:</p>
<pre><code>▮</code></pre></li>
<li><p>After press the key that inserted the environment:</p>
<pre><code>\begin{environment}[▮]\end{environment}</code></pre></li>
<li><p>After press <code>RETURN</code>:</p>
<pre><code>\begin{environment}[]▮\end{environment}</code></pre></li>
<li><p>After press <code>RETURN</code>:</p>
<pre><code>\begin{environment}[]\end{environment}▮</code></pre></li>
</ul>
<h1 id="special-cases">Special Cases</h1>
<h2 id="vertical-bar">Vertical Bar</h2>
<ul>
<li><p>Before press the key:</p>
<pre><code>▮</code></pre></li>
<li><p>After press the key:</p>
<pre><code>|▮|</code></pre></li>
<li><p>After press <code>RETURN</code>:</p>
<pre><code>||▮</code></pre></li>
</ul>
<h2 id="double-vertical-bar">Double Vertical bar</h2>
<ul>
<li><p>Before press the key:</p>
<pre><code>▮</code></pre></li>
<li><p>After press the key:</p>
<pre><code>\|▮\|</code></pre></li>
<li><p>After press <code>RETURN</code>:</p>
<pre><code>\|\|▮</code></pre></li>
</ul>
<h2 id="subscript-and-superscript">Subscript and Superscript</h2>
<ul>
<li><p>Before press the key:</p>
<pre><code>▮</code></pre></li>
<li><p>After press the key:</p>
<pre><code>{▮}_{}^{}</code></pre></li>
<li><p>After press <code>RETURN</code>:</p>
<pre><code>{}_{▮}^{}</code></pre></li>
<li><p>After press <code>RETURN</code>:</p>
<pre><code>{}_{}^{▮}</code></pre></li>
<li><p>After press <code>RETURN</code>:</p>
<pre><code>{}_{}^{}▮</code></pre></li>
</ul>
<h2 id="underset-and-overset">Underset and Overset</h2>
<ul>
<li><p>Before press the key:</p>
<pre><code>▮</code></pre></li>
<li><p>After press the key:</p>
<pre><code>\underset{▮}{\overset{}{}}</code></pre></li>
<li><p>After press <code>RETURN</code>:</p>
<pre><code>\underset{}{\overset{▮}{}}</code></pre></li>
<li><p>After press <code>RETURN</code>:</p>
<pre><code>\underset{}{\overset{}{▮}}</code></pre></li>
<li><p>After press <code>RETURN</code>:</p>
<pre><code>\underset{}{\overset{}{}}▮</code></pre></li>
</ul>
<h1 id="references">References</h1>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>GSOC</p>
</div>
<div class="tags">
<p>GSOC2014</p>
</div>
<div class="comments">

</div>
