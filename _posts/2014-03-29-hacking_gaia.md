---
layout: post
title: Hacking Gaia with Firefox OS Simulator (Updated)
author: "Raniere Silva"
categories: ftb
tags: [ftb]
image:
  feature: ftb.jpg
---
Almost a year ago [Salvador de la Puente
González](http://unoyunodiez.com/sobre-mi/) wrote a
[post](http://unoyunodiez.com/2013/07/26/hacking-gaia-with-firefox-os-simulator/)
(under CC-BY) about hacking Firefox OS default user applications (named
Gaia) using the browser Simulator.

This is a updated version of Salvador's post with parts of Salvador's
original.

Step 0: set-up
==============

Install the nightly version of Firefox. You will find the files needed
[at Nightly build page](http://nightly.mozilla.org/). After install the
nightly version start it.

![](/images/nightly00.png){width="25%"}

In the menu bar (you need to press "Alt" to show it) go to "Tools" -&gt;
"Web Developer" -&gt; "App Manager" or enter "<about:app-manager>" in
the URL bar.

![](/images/app-manager00.png){width="25%"}

In the bottom of the page you will find a "Start Simulator", click on
it.

![](/images/app-manager01.png){width="25%"}

Click on "Add".

![](/images/install-simulator00.png){width="25%"}

Click on "Install Simulator".

![](/images/install-simulator01.png){width="25%"}

Click on the Simulator version that you want to install (you can choose
the 1.3 since it will soon became the stable one). It will download the
Simulator and install it (this step can take some minutes depending of
your internet connection).

After install the Simulator goes back to App Manager.

![](/images/app-manager02.png){width="25%"}

Select the Simulator version that you want to use. A new windows will
appear with Firefox OS (running under the Simulator).

![](/images/app-manager03.png){width="25%"}

On the left of the App Manager there are some tabs. Select the "Device"
one.

![](/images/app-manager04.png){width="25%"}

This is a list of all the webapps available in your Simulator.

For now you can close the window with Firefox OS. We will come back to
it in the last step.

Step 1: build Gaia
==================

The source code of Gaia is available at
[GitHub](https://github.com/mozilla-b2g/gaia). Let's download it:

    $ git clone https://github.com/mozilla-b2g/gaia.git

And move inside `gaia` directory:

    $ cd gaia

To build Gaia:

    $ make

After finishing, you’ll end with a new folder called profile. The
profile in Firefox OS is the same as in Firefox, it holds the extensions
(nope, you can not develop extensions for Firefox OS), user preferences
and, in this case, the webapps pre-installed in the device.

So you have Nightly, Firefox OS Simulator and latest Gaia with a working
profile. The concept is simple: the Simulator uses a static Gaia profile
to run the simulation. The Simulator profile has a webapps folder inside
with all Gaia’s applications. We are going to replace some of these
application by those in our profile.

Step 2: hacking the keyboard
============================

Open `apps/keyboard/js/keboard.js` with your favorite editor. Search for
"Normal key" and goes to some text like:

    // Normal key

> default:
>
> :   
>
>     if (target.dataset.compositekey) {
>
>     :   // Keys with this attribute set send more than a single
>         character // Like ".com" or "2nd" or (in Catalan) "l·l". var
>         compositeKey = target.dataset.compositekey; for (var i = 0; i
>         &lt; compositeKey.length; i++) {
>         inputMethod.click(compositeKey.charCodeAt(i)); }
>
>     } else { inputMethod.click(keyCode); } break;
>
> }

> }

Below the default keyword, add:

    console.log('Key pressed: ' + keyCode);

Save and close your editor. Regenerate the profile with:

    $ make

    $ tree -L 1 profile                
    profile
    +── defaults
    +── extensions
    +── settings.json
    +── user.js
    +── webapps

    3 directories, 2 files
    $  tree -L 1 profile/webapps
    profile/webapps
    +── {27c3437c-8608-49f5-ad4a-9706600fa6d4}
    +── {8301cfeb-0dff-4d20-98ae-d33967b7e9ca}
    +── bluetooth.gaiamobile.org
    +── browser.gaiamobile.org
    +── calendar.gaiamobile.org
    +── camera.gaiamobile.org
    +── clock.gaiamobile.org
    +── communications.gaiamobile.org
    +── costcontrol.gaiamobile.org
    +── {d6caaa1c-64b6-449e-bc70-70990eb94553}
    +── demo-keyboard.gaiamobile.org
    +── ds-test.gaiamobile.org
    +── email.gaiamobile.org
    +── emergency-call.gaiamobile.org
    +── fl.gaiamobile.org
    +── fm.gaiamobile.org
    +── gallery.gaiamobile.org
    +── geoloc.gaiamobile.org
    +── homescreen.gaiamobile.org
    +── keyboard.gaiamobile.org
    +── marketplace.firefox.com
    +── membuster.gaiamobile.org
    +── music2.gaiamobile.org
    +── music.gaiamobile.org
    +── pdfjs.gaiamobile.org
    +── ringtones.gaiamobile.org
    +── search.gaiamobile.org
    +── setringtone.gaiamobile.org
    +── settings.gaiamobile.org
    +── share-receiver.gaiamobile.org
    +── sheet-app-1.gaiamobile.org
    +── sheet-app-2.gaiamobile.org
    +── sheet-app-3.gaiamobile.org
    +── sms.gaiamobile.org
    +── system.gaiamobile.org
    +── template.gaiamobile.org
    +── test-agent.gaiamobile.org
    +── test-container.gaiamobile.org
    +── test-fxa-client.gaiamobile.org
    +── test-ime.gaiamobile.org
    +── test-keyboard-app.gaiamobile.org
    +── test-otasp.gaiamobile.org
    +── test-receiver-1.gaiamobile.org
    +── test-receiver-2.gaiamobile.org
    +── test-receiver-inline.gaiamobile.org
    +── test-wappush.gaiamobile.org
    +── uitest.gaiamobile.org
    +── uitest-privileged.gaiamobile.org
    +── video.gaiamobile.org
    +── wallpaper.gaiamobile.org
    +── wappush.gaiamobile.org
    +── webapps.json

    51 directories, 1 file

Step 3: replacing the keyboard application in the Simulator
===========================================================

Now we need to find where is the `webapps` directory of the Simulator.
In Linux it is normally inside:

    ~/.mozilla/firefox/YOUR_FIREFOX_PROFILE/extensions/fxos_X_Y_simulator@mozilla.org/profile

where `YOUR_FIREFOX_PROFILE` is your Firefox profile (if you don't know
what I'm talking about, use the directory that has `default` on its
name) and `X_Y` is the version of the Simulator (if you install 1.3 it
will be `1_3`).

<div class="admonition">

Warning

You probably want to backup the `webapps` directory before continue.

</div>

Once you locate the `webapps` directory of the Simulator, copy our build
of the keyboard to replace the current one at the Simulator:

    $ cp -r profile/webapps/keyboard.gaiamobile.org \
    ~/.mozilla/firefox/YOUR_FIREFOX_PROFILE/extensions/fxos_X_Y_simulator@mozilla.org/profile/webapps/

<div class="admonition">

Note

If you prefer, you can also use a symbolic link to avoid to copy the
webapps every time you make some changes.

</div>

Step 4: debugging
=================

How do we know we are using our keyboard? More important, how to debug
our code!? Did you remember we added a log instruction in step 1? Let’s
show how to see these traces. First, run Firefox OS Simulator.

![](/images/app-manager-debug00.png){width="25%"}

At the "Device" tab of App Manager, search for "Built-in Keyboard" and
next to it you will see "Debug", click on it.

![](/images/app-manager-debug01.png){width="25%"}

A debugging tab for Keyboard will be create. This tab is very similar to
Firefox debug tool.

![](/images/ffos-home00.png){width="25%"}

Now at Firefox OS window, select the Messages app (it's the second icon
at the bottom).

![](/images/ffos-messages00.png){width="25%"}

Create a new message (the icon just after "Messages" at the top of the
screen) and type one key of the keyboard.

![](/images/ffos-messages01.png){width="25%"}

Now go back to App Manager.

![](/images/app-manager-debug02.png){width="25%"}

You will find something similar to :

    "Key pressed: 101"

at the console of the Debug tab.

Further hacking ...
===================

Now you want to continue hacking Gaia or its components. It is as easy
as repeat steps from 2 to 4. Not forget to regenerate the profile by
launching `make` each time you modify a Gaia application (and to copy
the new build of your webapps if you aren't using symbolic links)!!

Hope it helps!
