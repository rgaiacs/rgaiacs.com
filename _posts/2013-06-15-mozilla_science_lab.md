---
layout: post
title: "Mozilla Science Lab"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Meses atrás apareceu uma vaga na Mozilla para Diretor da Mozilla Science Lab, um novo projeto da Mozilla. Na época fiquei muito animado com o anúncio da vaga mas a única informação sobre o laboratório de ciência que era possível encontrar era o anúncio da vaga. Ontem, no blog da Mozilla foi feito a anúncio do novo projeto.</p>
<div class="more">

</div>
<p>O texto com o anúncio encontra-se <a href="https://blog.mozilla.org/blog/2013/06/14/5992/">nesse post</a> no blog da Mozilla intitulado &quot;Introducing the Mozilla Science Lab&quot;. Nele descobrimos que o projeto será liderado por <a href="http://kaythaney.com/">Kaitlin Thaney</a> uma defensora da ciência aberta de longa data que ajudou a fundar e coordenar o programa da <a href="http://creativecommons.org/science">Creative Commons</a>.</p>
<p>Kaitlin será acessorada por Greg Wilson, fundador da <a href="tabopen%20http://software-carpentry.org/blog/index.html">Software Carpentry</a> (uma programa que ensina um de computação para pesquisadores para que a tecnologia seja aliada destes).</p>
<p>A seguir apresento os problemas atuais que desejo serem resolvidos pela Mozilla Science Lab.</p>
<h1 id="colaboração-com-wikimedia">Colaboração com Wikimedia</h1>
<p>Colaboração por meio de wikis é uma ótima forma de colaboração entre pesquisadores que, infelizmente, ainda não é muito utilizada por dois motivos:</p>
<ol>
<li>muitos pesquisadores não gostam de utilizar linguagem de marcação,</li>
<li>o suporte a equações matemáticas em páginas webs ainda é precário.</li>
</ol>
<p>Espero que nos próximos anos esses dois problemas sejam resolvidos e possamos colaborar mais.</p>
<h1 id="melhoria-no-etherpad">Melhoria no Etherpad</h1>
<p>Para quem não sabe o <a href="http://etherpad.org/">Etherpad</a> é um software que possibilita edição em tempo real de um arquivo de texto &quot;plano&quot;. Até onde eu sei ele serviu de inspiração para o Google criar sua suite de escritório online.</p>
<p>Embora seja uma fantástica ferramenta, o Etherpad ainda precisa de algumas melhorias, principalmente em relação a suportar linguagens de marcação.</p>
<h1 id="integração-git-etherpad-wiki">Integração Git + Etherpad + Wiki</h1>
<p>Na minha opinião a integração dessas três ferramentas são a chave para a ciência no século 21. A Wiki com seu robusto banco de dados que possa ser editado em tempo real por meio do Etherpad e também offline por meio do Git levará a colaboração ao máximo até onde consigo imaginar.</p>
<h1 id="epub3-substituir-pdf">EPUB(3) substituir PDF</h1>
<p>Hoje boa parte dos textos acadêmicos (relatórios técnicos, trabalhos de conclusão de curso, dissertações de mestrado, teses de doutorado, artigos em periódicos, livros, ...) são divulgados no formato PDF que nada mais é que a versão eletrônica do mesmo material impresso. Por esse motivo o PDF não é uma formato adequado para fazer mineração de dados.</p>
<p>Já o EPUB, que é baseado em HTML, é um formato mais proprício para mineração de dados que é uma das áreas mais promissoras hoje em dia. A dificuldade na adoção do EPUB são:</p>
<ol>
<li>falta de conhecimento por parte da comunidade,</li>
<li>poucos programas para ler EPUBs (principalmente em sua versão 3),</li>
<li>problemas na conversão dos formatos usualmente utilizados para criação de documentos científicos (DOC, ODT, LaTeX, ...) para EPUB.</li>
</ol>
<h1 id="maior-participação-do-brasil">Maior participação do Brasil</h1>
<p>Embora o Brasil possua um publicação científica de respeito, as iniciativas rumo a ciência aberta ainda são bastante recentes. Até onde sei, a comunidade brasileira da Mozilla é grande e poderia ajudar a mudar essa realidade.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>none</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
