---
layout: post
title: "Workshop pelo Conhecimento Livre"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>O <a href="http://www.cienciaaberta.net/?page_id=18">Workshop pelo Conhecimento Livre</a> ocorreu no dia 08/06/2013 na Casa Nexo Cultural (ao contrário do encontro no dia anterior, este não foi transmitido ao vivo). A seguir você encontrá minha opinião sobre as oficinas.</p>
<div class="note">
<div class="admonition-title">
<p>Note</p>
</div>
<p>Como este post foi escrito depois do evento ele não encontra-se muito preciso. Parte do conteúdo foi retirado de <a href="http://okfnpad.org/eUZnuVUjIc" class="uri">http://okfnpad.org/eUZnuVUjIc</a>.</p>
</div>
<div class="more">

</div>
<h1 id="lista-de-participantes">Lista de Participantes</h1>
<ul>
<li>Teresa: interesse por livros.</li>
<li>Everton (Tom): OKF Brasil - Rede pelo Conhecimento Livre</li>
<li>Henrique: professor sociologia Unifesp - <a href="http://blog.pimentalab.net" class="uri">http://blog.pimentalab.net</a></li>
<li>Paulo: Ciência da Informação, mestrando na ECA.</li>
<li>Rafael: prof. fisica UFRGS - centro de tecnologias livres</li>
<li>Raniere: graduando em matemática UNICAMP</li>
<li>Leila: BIREME</li>
<li>Vera Lia: eng.produçao</li>
<li>Marcia: bibliotecária FESP(?) - <a href="http://bibmais.wordpress.com" class="uri">http://bibmais.wordpress.com</a></li>
<li>Miguel:</li>
<li>Caru:</li>
<li>Celio:</li>
<li>Elaine:</li>
</ul>
<h1 id="educação-aberta-e-recursos-educacionais-abertos">Educação aberta e recursos educacionais abertos</h1>
<p>Os participantes foram divididos em três grupos que debateram brevemente um dos seguintes temas:</p>
<ul>
<li>Licenças Abertas</li>
<li>Legislação relacionadas com REAs</li>
<li>Onde é possível encontrar REA</li>
</ul>
<p>Depois um membro de cada grupo resumiu o debate para os demais participantes.</p>
<h1 id="ferramentas-científicas-abertas">Ferramentas científicas abertas</h1>
<p>Ocorreram três &quot;oficinas&quot;:</p>
<ul>
<li>R (Software livre estatístico), por Naty.</li>
<li>Pen-drives bootáveis, por Rafael.</li>
<li>Git (Software para controle de versão), por Raniere.</li>
</ul>
<h1 id="acesso-aberto-e-revisão-de-publicações-científicas">Acesso aberto e revisão de publicações científicas</h1>
<h1 id="dados-científicos-abertos">Dados científicos abertos</h1>
<p>Everton apresentou ferramentas da OKF.</p>
<h1 id="ciência-cidadã">Ciência cidadã</h1>
<h1 id="wikipesquisas">Wikipesquisas</h1>
<p>Alexandre mostrou como editar uma wiki e alguns dos recursos disponíveis.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>none</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
