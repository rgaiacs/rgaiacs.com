---
layout: post
title: Developer Mode no Red Mi 2 Pro
author: "Raniere Silva"
categories: ftb
tags: [ftb]
image:
  feature: ftb.jpg
---
<div class="admonition">

Agradecimentos

Esse post é uma versão longa da resposta disponível em
<http://en.miui.com/thread-24025-1-1.html>.

</div>

Nesse post você encontrará screenshots de como habilitar o modo de
desenvolvedor no Red Mi 2 Pro. O modo de desenvolvedor permite você
utilizar o [ADB](https://developer.android.com/tools/help/adb.html).

<div class="admonition">

Aviso

Habilitar o modo de desenvolvedor não é a mesma coisa que conseguir
permissões administrativas no aparelho.

</div>

Acesse as configurações do seu aparelho.

![](/images/Screenshot_2015-10-28-16-59-56.png){width="80%"}

Vá até o final das opções.

![](/images/Screenshot_2015-10-28-17-00-11.png){width="80%"}

A última opção é "About phone". Selecione-o.

![](/images/Screenshot_2015-10-28-17-00-14.png){width="80%"}

Dependendo da versão do sistema operacional você precisa clicar na opção
"Android version" ou "MIUI version" várias vezes até ser notificado que
o modo de desenvolvedor foi habilitado.

![](/images/Screenshot_2015-10-28-17-01-46.png){width="80%"}

Com o modo de desenvolvedor habilitado você provavelmente vai querer
habilitar algumas opções. Voltando as configurações do seu aparelho,
procure por "Additional settings" e vá até o final das opções.

![](/images/Screenshot_2015-10-28-17-04-58.png){width="80%"}

Selecione "Developer options".

![](/images/Screenshot_2015-10-28-17-05-04.png){width="80%"}

Habilite o "USB debugging".

![](/images/Screenshot_2015-10-28-17-05-06.png){width="80%"}

Se seu dispositivo estiver conectado ao computador você irá receber uma
notificação confirmando que o computador pode acessar o dispositivo.

![](/images/Screenshot_2015-10-28-17-05-23.png){width="80%"}

Você provavelmente quer aceitar o acesso pelo computador.

Se você olhar a área de notificação irá verificar que o "USB debugging"
está ativado.

![](/images/Screenshot_2015-10-28-17-06-15.png){width="80%"}

Para terminar, apenas verifique que o dispositivo é reconhecido pelo seu
computador:

    $ adb devices
    List of devices attached 
    4a082719        devic

Agora é só hackear.
