---
layout: post
title: Revisitando meu \~/.emacs
author: "Raniere Silva"
categories: ftb
tags: [ftb]
image:
  feature: ftb.jpg
---
Depois de usar o [Emacs](https://www.gnu.org/software/emacs/) por mais
de um ano eu resolvi revisitar meu `~./emacs`, isso é, meu arquivo de
configuração do Emacs.

É possível utilizar `~/.emacs`, `~/.emacs.el` ou `~/.emacs.d/init.el`
para o arquivo de configuração. Resolvi utilizar o último para manter
todos os arquivos no mesmo diretório. Você encontrará meus arquivos no
[GitLab](https://gitlab.com/rgaiacs/emacs-init-file).
