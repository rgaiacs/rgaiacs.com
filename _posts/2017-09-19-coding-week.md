---
layout: post
title: "National Coding Week: Full Stack of Pancakes"
author: "Raniere Silva"
categories: tools
tags: [jekyll]
image:
  feature: 2017-09-19-coding-week.jpg
  author: numb3r
  original_url: https://flic.kr/p/9uDKEh
  licence: CC-BY
---

My friend [Niall Beard](https://twitter.com/niall_beard) recommended me to [Sophie Ashcroft](https://twitter.com/ms_s_ashcroft)
as a Git instructor for the [National Coding Week](https://codingweek.org/) [activity that Sophie is organising](https://www.eventbrite.com/e/get-git-tickets-37090791556)
with other members of [Full Stack of Pancakes](http://fullstackofpancakes.com/).
After exchanged a few emails, the information that I have to try to plan the lesson was

- 2 hours long
- basic level
- mix of backgrounds
- focus on motivation

It is going to be a challenge and based on some of my previous experience teaching Git,
the motivation part is very hard if you work with just one file or with isolated files,
i.e. that don't interact with any other.
Over dinner,
I was thinking of what game I could implemente in [Liquid template language](https://shopify.github.io/liquid/)
in less than 2 hours that could be interesting for learners to get motivated to learn Git.
The solution, at the moment, is a [Rock Paper Scissors](https://github.com/rgaiacs/rock-paper-scissors) game.
If you look at the Git repository, it only has

- `_data/player1.yml`
- `_data/player2.yml`
- `.gitignore`
- `Gemfile`
- `LICENSE`
- `_config.yml`
- `index.html`

I tried to reduce the number of files and dependecies at the maximum.
All the game logic is in `index.html` with instructions.
[The website](https://rgaiacs.github.io/rock-paper-scissors/) will change based on
`_data/player1.yml` and `_data/player2.yml`,
which I hope will help learners understand the advantages of use Git.