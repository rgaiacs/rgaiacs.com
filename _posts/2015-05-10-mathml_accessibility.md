---
layout: post
title: "MathML Accessibility"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Frédéric Wang wrote a <a href="http://www.maths-informatique-jeux.com/blog/frederic/?post/2015/05/06/MathML-Accessibility#mathml_accessibility_audio_demos">nice post</a> about MathML Accessibility that has a comparison table between <a href="http://www.nvaccess.org">NVDA</a>, <a href="https://www.apple.com/accessibility/osx/voiceover/">VoiceOver</a> and <a href="https://wiki.gnome.org/Projects/Orca">Orca</a>.</p>
<p>I promised to Frédéric to provide a sample of the support of Firefox OS' screen reader and you can find it <span data-role="download">here &lt;demo.ogg&gt;</span> (it is a <a href="https://en.wikipedia.org/wiki/Ogg">ogg file</a> with the video in <a href="http://www.theora.org/">theora</a> and the audio in <a href="http://www.vorbis.com/">vorbis</a>).</p>
<div class="admonition">
<p>Note</p>
<p>For the test I used <a href="https://marketplace.firefox.com/app/brafunmat8?src=search">this app</a>.</p>
</div>
<div class="more">

</div>
<h1 id="mathml">MathML</h1>
<p>The screen reader doesn't support MathML. So far it only speak the numbers.</p>
<h1 id="navigation">Navigation</h1>
<p>Sometimes it goes back many elements what is really bad. You can notice this bug right in the begin of the video and also at the end.</p>
<h1 id="enabling-screen-reader">Enabling Screen Reader</h1>
<p>Right now you need to go into the developer settings to enable listing the screen reader at the accessibility settings and them go to the accessibility settings to enable the screen reader.</p>
<div class="admonition">
<p>Note</p>
<p>If you want to ddisable the screen reader you will have to be a little patient!</p>
</div>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>Firefox OS,Mozilla,Mozilla Brasil,MathML</p>
</div>
<div class="tags">
<p>acessibilidade,bug,screen reader</p>
</div>
<div class="comments">

</div>
