---
layout: post
title: "Formatação de trabalho"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>No meio acadêmico, cada órgão responsável por &quot;divulgar&quot; as mais novas pesquisas possui um formato único para submissão de trabalhos. Via de regra, um dos três formatos abaixos são usados:</p>
<ul>
<li>arquivo <code>.tex</code>,</li>
<li>arquivo <code>.pdf</code>,</li>
<li>arquivo <code>.doc</code> (não tenho conhecimento do uso do formato aberto <code>.odt</code>).</li>
</ul>
<p>O arquivo <code>.tex</code> é muito utilizado na ciências exatas e engenharias por ter sido um dos primeiros formatos a ter suporte a equações matemáticas e devido a sua qualidade profissional.</p>
<p>O arquivo <code>.doc</code> é muito utilizado nas ciências humanas e sociais por seus usuários gostarem de visualizar o resultado final (uso de editores do tipo <a href="http://en.wikipedia.org/wiki/WYSIWYG">WYSIWYG</a>).</p>
<p>Já os arquivos <code>.pdf</code> são utilizados quando deseja-se satisfazer gregos e troianos.</p>
<p>Pessoalmente não gosto de utilizar editores do tipo WYSIWTG pois a formatação deles é autamente instável (isso é tópico para um outro post). Para mim, o pior é não poder enviar o documento em um formato aberto, <code>odt</code>.</p>
<div class="more">

</div>
<p>Ontem, recebi por email o aviso para submissão de um resumo para o Congresso Interno de Iniciação o qual encontra-se abaixo:</p>
<pre><code>Prezado/a RANIERE GAIA COSTA DA SILVA , RA92767
C/c Prof./a Dr./a (retirado para preservar a identidade)

Informamos que a inscrição para o XXI Congresso Interno de Iniciação Científica da UNICAMP deverá ser realizada no período de 3 a 7 de junho de 2013, através do endereço http://www.prp.unicamp.br/pibic , conforme os prazos constantes no Termo de Compromisso assinado por bolsista e orientador/a na ocasião da outorga da bolsa de Iniciação Científica do PIBIC/PIBITI do CNPq  e do SAE/UNICAMP.

Ao fazer a inscrição o/a aluno/a deverá anexar o Resumo do Trabalho, formatado de acordo com as normas especificadas no final desta mensagem.

Para publicação do trabalho no Caderno de Resumos é fundamental que o Resumo e as informações da inscrição estejam rigorosamente corretos, segundo as normas estabelecidas.
Alertamos, também, para alocaçÃo da ÁREA em que a inscrição será feita, conforme especificações abaixo, devido à alocação do seu painel no dia da apresentação no XXI Congresso.

-          pertencentes às ENGENHARIAS são alocadas na Área de TECNOLÓGICAS.
-          pertencentes às Áreas de EXATAS são alocadas nesta Área.
-          Pertencentes às Áreas de HUMANAS são alocadas nesta Área.
-          Pertencentes às Áreas de ARTES são alocadas nesta Área.
-          Pertencentes às Áreas de BIOMÉDICAS são alocadas nesta Área.

Pedimos a colaboração de todos para que elaborem o Resumo dentro das normas e verifiquem com o/a orientador/a a área correta na qual a inscrição deverá ser feita.

Contando com a sua especial atenção e providências, antecipamos os agradecimentos.
Cordialmente,

(retirado para preservar a identidade)
PIBIC/ Pró-Reitoria de Pesquisa
Universidade Estadual de Campinas
Caixa Postal 6197
13083-872 - Campinas (SP) - Brasil
Tel.: 55-19-3521-4891 - Fax: 55-19-3521-4892
http://www.prp.rei.unicamp.br/pibic/
http://www.informacoes.unicamp.br/docs/guia_ruas.pdf

INSCRIÇÕES E NORMAS
Período de inscrição: 3 a 7 de junho de 2013
Instruções para a digitação e envio do Resumo do Trabalho

O   Caderno de Resumos  do XXI Congresso Interno de Iniciação Científica da UNICAMP  será disponibilizado eletronicamente no site do congresso, como ocorre anualmente.  Ressaltamos que é de total responsabilidade do/a bolsista a prévia revisão do/a orientador/a quanto ao conteúdo do resumo do trabalho enviado, portanto, a inscrição deve ser feita somente quando esse estiver revisado pelo/a orientador/a e totalmente sem erros.

Favor elaborar o Resumo do Trabalho seguindo as especificações abaixo:

O Resumo deverá conter: a) uma breve descrição do estudo; b) objetivos e metodologia; c) resultados; d) conclusões*.
FORMATAÇÂO:

1. Editor de textos: Microsoft Word
2. Número máximo de linhas: 15 linhas
3. Fonte: Arial
4. Tamanho e estilo da fonte: tamanho 10, estilo normal, alinhamento: JUSTIFICADO

OBS.:  O arquivo do resumo enviado será automaticamente carregado e renomeado pelo sistema, com o número do seu RA (registro acadêmico).
As informações como título do trabalho, palavras-chave, orientador/a, co-autores, autor, tipo de bolsa devem ser informadas corretamente na ficha de inscrição do congresso e serão utilizadas na elaboração do Caderno de Resumos que será publicado.
A participação de todos os/as bolsistas é obrigatória mesmo para aqueles que cancelaram a bolsa, conforme Termo de Compromisso e normas do PIBIC.
*Caso não haja conclusão do trabalho, devido ao período da vigência da bolsa, o resumo deve ser enviado com a descrição da pesquisa que foi desenvolvida durante os meses da bolsa.


Instruções para a utilização do sistema via Internet:

1) Recomenda-se o uso dos seguintes navegadores: Explorer 6.0, FireFox 5.0 ou Netscape 4.x. Versões superiores dos navegadores podem ser utilizadas.
2) O sistema irá pedir seu RA e RG e MatrÍcula do orientador, e em seguida dados relativos à sua inscrição.
3) Posteriormente, o sistema solicitará o envio do seu arquivo word com o resumo do trabalho. Para isso, deverá ser utilizado o botão  [Procurar...]  disponível na Ficha de Inscrição.

Maiores informações no site do PIBIC, link congressos  http://www.prp.rei.unicamp.br/pibic/ , onde podem ser consultados os congressos anteriores.</code></pre>
<p>Desejava ser capaz de enviar meu resumo em formato aberto e resolvi perguntar se poderia fazê-lo dessa forma:</p>
<pre><code>Olá,

tenho uma pergunta sobre a formatação do resumo para o Congresso PIBIC.

&gt; FORMATAÇÃO:
&gt; 
&gt; 1. Editor de textos: Microsoft Word
&gt; 2. Número máximo de linhas: 15 linhas
&gt; 3. Fonte: Arial
&gt; 4. Tamanho e estilo da fonte: tamanho 10, estilo normal, alinhamento: JUSTIFICADO
&gt; 
&gt; OBS.:  O arquivo do resumo enviado será automaticamente carregado e renomeado pelo sistema, com o número do seu RA (registro acadêmico).
&gt; As informações como título do trabalho, palavras-chave, orientador/a, co-autores, autor, tipo de bolsa devem ser informadas corretamente na ficha de inscrição do congresso e serão utilizadas na elaboração do Caderno de Resumos que será publicado.
&gt; A participação de todos os/as bolsistas é obrigatória mesmo para aqueles que cancelaram a bolsa, conforme Termo de Compromisso e normas do PIBIC.
&gt; *Caso não haja conclusão do trabalho, devido ao período da vigência da bolsa, o resumo deve ser enviado com a descrição da pesquisa que foi desenvolvida durante os meses da bolsa.

Posso utilizar o LibreOffice (http://www.libreoffice.org/)? Serei capaz de
enviar o arquivo em um formato aberto (.odt,
http://en.wikipedia.org/wiki/OpenDocument)?

Att,
Raniere</code></pre>
<p>Recebi a seguinte resposta:</p>
<pre><code>Olá Raniere, bom dia!

O nosso sistema só irá aceitar arquivos com a extensão .doc (Microsoft Word).

Atenciosamente,



(retirado para preservar a identidade)

PIBIC/ Pró-Reitoria de Pesquisa

Universidade Estadual de Campinas

Caixa Postal 6197

13083-872 – Campinas (SP) – Brasil

Tel.: 55-19-3521-4891 – Fax: 55-19-3521-4892

http://www.prp.rei.unicamp.br/pibic/

http://www.informacoes.unicamp.br/docs/guia_ruas.pdf

(retirado a mensagem original)</code></pre>
<p>A imposição de utilizar formatos proprietários é uma das coisas que me afasta de querer seguir carreira no meio acadêmico.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>none</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
