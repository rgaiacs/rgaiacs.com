---
layout: post
title: MathML Layout Tests
author: "Raniere Silva"
categories: ftb
tags: [ftb]
image:
  feature: ftb.jpg
---
<div class="admonition">

Note

Work in progress.

</div>

Today someone showed up at \#mathml requesting help:

    2014-09-22 07:21:06     cerkiewny       raniere: hmm but I dont know how to make it work, I mean the things ment ioned in the bug are from matjax so its different syntax and I think the output image is different as well
    2014-09-22 07:22:21     cerkiewny       raniere: from what I understood we want to have some reference image fro m rendered mathML than some reference html displaying this image only and compare it to the output of mathml, am I right? so my question is how do we get to this image?

This reminder me the difficult that I have to hacking Gecko's layout
engine so I decide to wrote a small "how to" for create MathML layout
tests.

Compiling Firefox
=================

Before you starting creating MathML layout tests you need to compile
your own version of Firefox and before that you need to download Firefox
source code. To download Firefox source code you can use:

    $ git clone git@github.com:mozilla/gecko-dev.git

Before you start compiling Firefox you probably want to check if you
need to do something depending of the operating system that you are
using. You will find this information at
<https://developer.mozilla.org/en-US/docs/Mozilla/Developer_guide/Build_Instructions>.

To build Firefox from the source code that you just download:

    $ cd gecko-dev
    $ ./mach build

Running Tests
=============

To run MathML tests:

    $ ./mach reftest layout/reftests/mathml/reftest.list

Hopefully all the tests will pass. If something goes wrong please open a
bug at <https://bugzilla.mozilla.org>.

Understanding Tests
===================

Now that we already run the tests once we can start figuring out how it
work.

The file `layout/reftests/mathml/reftest.list` is a list of the tests to
run. If we look at the first lines we will get:

    $ head -n 20 layout/reftests/mathml/reftest.list
    == dir-1.html dir-1-ref.html
    == dir-2.html dir-2-ref.html
    == dir-3.html dir-3-ref.html
    == dir-4.html dir-4-ref.html
    == dir-5.html dir-5-ref.html
    == dir-6.html dir-6-ref.html
    == dir-6a.html dir-6a-ref.html
    == dir-7.html dir-7-ref.html
    fails == dir-8.html dir-8-ref.html
    fails == dir-9.html dir-9-ref.html # Bug 787215
    == dir-10.html dir-10-ref.html
    random-if(B2G&&browserIsRemote) == dir-11.html dir-11-ref.html
    == displaystyle-1.html displaystyle-1-ref.html
    == displaystyle-2.html displaystyle-2-ref.html
    == displaystyle-3.html displaystyle-3-ref.html
    random-if(B2G&&browserIsRemote) == displaystyle-4.html
    displaystyle-4-ref.html
    skip-if(B2G&&browserIsRemote) random-if(smallScreen&&Android) fuzzy(255,200)
    == mirror-op-1.html mirror-op-1-ref.html
    != mirror-op-2.html mirror-op-2-ref.html
    != mirror-op-3.html mirror-op-3-ref.html
    != mirror-op-4.html mirror-op-4-ref.html

Isn't hard to figure out what each line represent. `==` indicates that
the browser must get the same render for both files and `!=` that the
browser must get different render for files. `fails` indicates that is
expected that the test fails for some reason. There are others key works
but let skip them, if you need you can look at[^1].

You can pick any test from the list but let start with a simple one:

    $ cat layout/reftests/mathml/mi-mathvariant-1.xhtml    
    <html xmlns="http://www.w3.org/1999/xhtml">
    <body>
      <math xmlns="http://www.w3.org/1998/Math/MathML">
        <mi>cos</mi>
        <mo>&#x2061;</mo>
        <mi>x</mi>
      </math>
    </body>
    </html>

The `cos` must be render with normal font and the `x` with italic, lets
add this to the reference of the previous test:

    $ cat layout/reftests/mathml/mi-mathvariant-1-ref.xhtml 
    <html xmlns="http://www.w3.org/1999/xhtml">
    <body>
      <math xmlns="http://www.w3.org/1998/Math/MathML">
        <mi mathvariant="normal">cos</mi>
        <mo>&#x2061;</mo>
        <mi mathvariant="italic">x</mi>
      </math>
    </body>
    </html>

**Referências**

[^1]: [Mozilla Developer Network: Creating reftest-based unit
    tests](https://developer.mozilla.org/en-US/docs/Creating_reftest-based_unit_tests)
