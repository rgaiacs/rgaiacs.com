---
layout: post
title: "GNU Emacs: o editor"
author: "Raniere Silva"
categories: ftb
tags: [ftb]
image:
  feature: ftb.jpg
---
<div class="admonition note">

Este post é mais um capítulo na Guerra dos editores. Recomenda-se também
ler emacs\_o\_lixo.

</div>

O GNU Emacs é para muitos o melhor editor de texto, dentre outros
motivos, uma vez que é possível personalizá-lo.

Essa personalização do GNU Emacs é feita no arquivo `~/.emacs` e uma
versão minimalista do meu é :

    ;; Change background color to black.
    (if (display-graphic-p)
      (progn
        ;; if graphic
        (invert-face 'default)
        (set-face-attribute 'default nil :height 100)
        ;; else
      )
    )

    ;; Add line number.
    (global-linum-mode 1)
    ;; Hook to not number shell mode.
    (add-hook 'shell-mode-hook (lambda() (linum-mode 0)))

    ;; Remove the menu bar.
    (menu-bar-mode -1)

    ;; Remove the tool bar.
    (tool-bar-mode -1)

Com as configurações acima temos que a tela de boas vindas do GNU Emacs
será:

![](/images/emacs00.jpeg){width="80%"}

Para o GNU Emacs, cada arquivo corresponde a um buffer (buffer é chamado
em outros editores de aba). Isso significa que é possível carregar
vários arquivos no GNU Emacs.

![](/images/emacs01.jpeg){width="80%"}

Outra funcionalidade importante é abrir duas ou mais janelas
simultaneamente e "lado-a-lado".

![](/images/emacs02.jpeg){width="80%"}

Além da características listadas anteriormente, o GNU Emacs trabalha em
modos e existe modos para quase tudo (inclusive para emular um
terminal).

![](/images/emacs03.jpeg){width="80%"}

Sem sombra de dúvidas, essas características tornam o GNU Emacs um ótimo
editor.
