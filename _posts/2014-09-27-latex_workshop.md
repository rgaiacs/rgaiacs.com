---
layout: post
title: "LaTeX Workshop"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<figure>
<img src="/images/writelatex.jpg" class="align-center" style="width:80.0%" />
</figure>
<p>Today happened the LaTeX + Git workshop that the laboratory where I &quot;work&quot; was organizing with the sponsorship of <a href="http://writelatex.com/">writeLaTeX</a>. At this post you will find my post workshop thoughts. Before I start to write about it I want to thanks</p>
<ul>
<li>Abel Siqueira, that helped teaching the Git lessons,</li>
<li>Kally Chung, that helped with the organization,</li>
<li>Panaggio Ricardo and Wanderson Luiz, that helped solving students issues,</li>
<li>Melissa Devens, that helped with the D-day logistic,</li>
<li>Eric Lopes, that took the photos and also help solving students issues, and</li>
<li>Andre Garcia, that make possible that we use the amazing room from EA2 during the Saturday.</li>
</ul>
<div class="more">

</div>
<h1 id="overview">Overview</h1>
<p>In general, based on the feedbacks from the students (<span data-role="download">you can check it here &lt;latex_workshop_feedback.txt&gt;</span>, if you read portuguese), the workshop was very nice.</p>
<figure>
<img src="/images/greg.jpg" class="align-center" style="width:80.0%" />
</figure>
<figure>
<img src="/images/bia.jpg" class="align-center" style="width:80.0%" />
</figure>
<p>We started around 9:30am with a &quot;key note&quot; short-talk from <a href="http://third-bit.com/">Greg Wilson</a> who explains why students should use LaTeX and Git follow by some words of our host, represented by Prof. Beatriz Jansen Ferreira.</p>
<p>At 10:00am the LaTeX lesson started. We had a little more troubles that we expected (slow internet connection and many environments) but we could cover the very basic of LaTeX with success.</p>
<figure>
<img src="/images/abel.jpg" class="align-center" style="width:80.0%" />
</figure>
<p>Abel started the &quot;Introduction to Git&quot; at 11:30am and we go until 1:00pm when we stop for lunch. At 2:30pm we come back from lunch and Abel started taught how to use Git to backup your work and collaborate with others that goes until 4:30pm when we stop for coffee break.</p>
<p>We come back at 5:00pm when I returned to talk about LaTeX and some of the problems that happens when you try to collaborate with others (mostly due different environments) and how we can use writeLaTeX to solve this problems.</p>
<p>The workshop ended at 6:00pm.</p>
<h1 id="issue-overview">Issue Overview</h1>
<p>Mostly of the issues are the same from previous workshops that I participate but this isn't an excuse to don't talk about them.</p>
<h1 id="issue-1---need-more-time">Issue 1 - Need more time</h1>
<p>I know that only one day isn't enough and I couldn't agree more with one of the students that said:</p>
<blockquote>
<p>&quot;Will be nice if we had a three days workshop.&quot;</p>
</blockquote>
<p>Let Greg Wilson know when you and your co-workers can stop <strong>ALL</strong> your activities for three days and I have almost sure that he can find someone to teach a three days workshop <em>if</em> you also agree to find a room for it.</p>
<h1 id="issue-2---novices-and-intermediates-at-the-same-room">Issue 2 - Novices and Intermediates at the same room</h1>
<p>One of our students said</p>
<blockquote>
<p>&quot;I didn't like very much the LaTeX lesson because it wasn't basic.&quot;</p>
</blockquote>
<p>and another one said</p>
<blockquote>
<p>&quot;The LaTeX lesson was too much basic.&quot;</p>
</blockquote>
<p>Know in advance if the students are novices or not isn't a easy task and because of it we always disappoint one or two students.</p>
<h1 id="issue-3---didnt-was-exactly-what-i-expected">Issue 3 - Didn't was exactly what I expected</h1>
<p>One student said</p>
<blockquote>
<p>&quot;I hope that the workshop cover more LaTeX than Git.&quot;</p>
</blockquote>
<p>Sometimes I think that some people didn't check all the information that we wrote. From the begin, the schedule of the workshop shows that Git will be a major topic of the workshop.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>none</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
