---
layout: post
title: "GSoC: May 19 - May 25"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>This is one of the reports about my GSoC project and cover the first week of &quot;Students coding&quot;.</p>
<div class="more">

</div>
<h1 id="building-firefox-os">Building Firefox OS</h1>
<p>So far I was using only Firefox OS Simulator to test my patches. You can find the approach used <a href="http://ftb.rgaiacs.com/2014/03/29/hacking_gaia.html">in this tutorial</a> that is a updated version of one wrote by my mentor.</p>
<p>At the begin of this week I updated my hamachi device with the last image available at <a href="http://elsimpicuitico.wordpress.com/firefoxos/" class="uri">http://elsimpicuitico.wordpress.com/firefoxos/</a> and in the process I wrote a <a href="http://ftb.rgaiacs.com/2014/05/19/atualizando_alcatel.html">tutorial</a> (only available in portuguese).</p>
<p>After updated my hamachi device I tried to build my own image of Firefox OS but couldn't make it due some errors raised by the script used to build the image. In the process I wrote <a href="http://ftb.rgaiacs.com/2014/05/20/atualizando_app.html">another tutorial</a> (also only available in portuguese).</p>
<p>Next week I should receive one of the Firefox OS tablets from <a href="https://wiki.mozilla.org/FirefoxOS/TCP">TCP</a> and hope to have less problems to build my own image.</p>
<h1 id="bug-983043">Bug 983043</h1>
<p><a href="https://bugzilla.mozilla.org/show_bug.cgi?id=983043">Bug 983043</a> is related with the keyboard recommendations from UX team and is where you find the lasted recommendation available.</p>
<p>Discovery this bug toke me some time but I was really happy after found it.</p>
<h1 id="emoji">Emoji</h1>
<p>When searching for Bug 983043 I found <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=993899">Bug 993899</a> that request the keyboard to support emoji.</p>
<p>I wrote a small prototype for it based on a old UX recommendation but now I'm waiting a reply to <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=993899#c9">Rudy Lu</a>'s question:</p>
<blockquote>
<p>&quot;(...) the question would be if we could switch to emoji layout with just the ime switch key (the globe key) or we need to add another mode-switching key for this, (...)&quot;</p>
</blockquote>
<p>from UX team that will help with my GSoC project.</p>
<h1 id="bug-1011477">Bug 1011477</h1>
<p>This bug is related with <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=1011477">multi-character value mixed with alternate keys</a>. When I try using a updated version of Gaia it was solved. I didn't search to find the patch that fix this issue but I should do it.</p>
<h1 id="bug-1011482">Bug 1011482</h1>
<p>This bug is related with <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=1011482">need value and compositeKey fields for alt keys</a>. I started to work on it but didn't finish yet.</p>
<p>Some lessons learned from my mentor when filing a bug requesting a new feature:</p>
<ul>
<li>don't talk in first person;</li>
<li>try to find a user case;</li>
<li>explain it;</li>
<li>support how the current implementation does not cover the problem and</li>
<li>what you think it could be solved.</li>
</ul>
<h1 id="bug-1011661">Bug 1011661</h1>
<p>This bug is related with <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=1011661">uppercase and lowercase for compositekey</a>.</p>
<p>I need to update my patch with a test case but first I need to learn how write test cases for Gaia.</p>
<h1 id="others-bugs">Others bugs</h1>
<p>This is some small bugs that I work on.</p>
<ul>
<li><a href="https://bugzilla.mozilla.org/show_bug.cgi?id=1014142">Bug 1014142</a> - URL Keyboard should following the recommendations</li>
<li><a href="https://bugzilla.mozilla.org/show_bug.cgi?id=796530">Bug 796530</a> - No Keyboard support for Spanish Latin America</li>
</ul>
<h1 id="plans-for-may-26---june-01">Plans for May 26 - June 01</h1>
<p>Build my own version of Firefox OS to test the <a href="https://github.com/r-gaia-cs/gaia/tree/greek-latex">Greek letter keyboard</a> and the <a href="https://github.com/r-gaia-cs/gaia/tree/math-latex">basic math keyboard</a> properly.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>GSOC</p>
</div>
<div class="tags">
<p>GSOC2014</p>
</div>
<div class="comments">

</div>
