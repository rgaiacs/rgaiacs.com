---
layout: post
title: "Pós-Graduação"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>No início do mês, <a href="http://www.davidabanks.org/about-me/">David Banks</a> publicou em seu <a href="http://www.davidabanks.org/about-me/">blog</a> e em <a href="http://thesocietypages.org/cyborgology/2013/07/23/grad-students-are-ruining-everything/">Cyborgology</a> da &quot;The Society Pages&quot; (ver cópia <span data-role="download">aqui
&lt;grad-students-are-ruining-everything.html&gt;</span>) um artigo (licenciado sob CC-BY-NC-SA) sobre os problemas da pós-graduação.</p>
<div class="more">

</div>
<p>O primeiro problema apontado por Banks é os pós-graduandos não serem serem proficientes em algumas tarefas &quot;essenciais&quot; para suas atividades como editar fotografias e vídeos, criar uma página web, ... de forma que passam semanas para aprender como realizar essas tarefas e se mostrar isso para seus colegas ficará conhecido como &quot;a pessoa a quem pedir ajuda para editar uma fotografia ...&quot; quando muitas vezes o departamento deveria ter um profissional para auxiliar em algumas dessas tarefas.</p>
<p>O segundo problema é que universidades estão salvando montanhas de dinheiro ao substituir funcionários por pós-graduandos que estão desesperados para adicionar uma linha em seus currículos.</p>
<p>O terceiro problema está relacionada com as ferramentas de trabalho dos pós-graduandos que muitas vezes precisam ser custeadas do próprio bolso.</p>
<p>Uma solução apresentada para os problemas é trocar a posição de estudante pela de professor assitente.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>educação</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
