---
layout: post
title: "Vem depressa..."
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Em 2009, quando fui na Bienal do Livro em São Paulo (capital) conheci a Michele na fila do busão da bienal e acabamos visitando a exposição juntos. Ela mantém um blog, <a href="http://olhapravoce.blogspot.com.br/">olha pra você</a>, que acompanho e vez ou outro encontro alguns textos muito bons. Gostei muito do <a href="http://olhapravoce.blogspot.com.br/2013/06/vem-depressa-pra-mimque-eu-nao-sei.html">último post</a> que resolvi divulgá-lo.</p>
<div class="more">

</div>
<p>A baixo encontra-se cópia do post <a href="http://olhapravoce.blogspot.com.br/2013/06/vem-depressa-pra-mimque-eu-nao-sei.html">&quot;Vem depressa pra mim, que eu não sei esperar&quot;...</a>:</p>
<blockquote>
<p>Feriadão conversei muito com um grande novo amigo, cinquentão, e mesmo nessa altura do &quot;campeonato&quot;, ele confessou ter medo de se entregar a uma mulher, e se machucar...</p>
<p>Rsrs, porra, perdi minhas esperanças...</p>
<p>Esse medo não acaba nunca???</p>
<p>Um cara viajado, experiente, pegou muita mulher, inteligentíssimo, admirável, enfim... E tem medo!</p>
<p>É claro que o ser humano tem medo de se machucar, por já ter se machucado... E é bem provável que quem continua entrando de &quot;cabeça&quot;, é porque não faz idéia do que é se arrebentar!</p>
<p>Principalmente, como esse amigo disse, quando a pessoa além de bonita, é cativante, interessante, tem um bom papo, bom humor, conversa sobre tudo numa boa... É complicado!</p>
<p>E às vezes não é nada disso... Às vezes os dois não tem sintonia de nada disso, mas um olha pra outro e abre aquele sorriso, com aquela satisfação de se ver... Dá medo mesmo, rsrs!</p>
<p>E desta forma, o ser humano jegue deixa de aproveitar o momento, porque ele sabe que por conta desse momento, ele pode sofrer por aaaaaaaaaaanos... E aí é preferível abrir mão do prazer, pra não pagar com a dor... Mas, será mesmo que existe um meio de não se machucar com isso? Existe algum manual de como tirar tudo isso de letra?</p>
<p>Tem um ditado que diz que a felicidade é feita de momentos... E deve ser mesmo, já que o &quot;constante&quot; é uma porcaria... E os momentos devem ser aproveitados...</p>
<p>É bom aproveitar cada abraço... E apertar até doer os braços, os ombros, as costas...</p>
<p>E num momento juntos, entregar sua verdade, seu coração, sem reservas, sem hesitar...</p>
<p>Não são as palavras, mas os gestos, a ação, a verdadeira troca, o que marca, o que fica, pra sempre...</p>
<p>E só entendemos a grandiosdade dessas &quot;pequenas&quot; coisas, quando já não fazem mais parte de nosso cotidiano, quando fica só um retrato em cima do criado mudo!</p>
<p>Lembra daquele abraço surpresa?</p>
<p>Aquele beijo, aquele toque nos lugares onde despertava em você o ser humano mais terno, ou o mais ardente...</p>
<p>A companhia agradável...</p>
<p>É lamentável que quando temos não dizemos ao outro, TUDO o que realmente sentimos, o quanto é satisfatória a presença, tudo o que nos agrada... Se já sabe que vai sofrer, viva tudo, todo o agora, de corpo inteiro... Jamais ser você mesmo é se humilhar, se rebaixar, ou mesmo ser vergonhoso, ridículo!</p>
<p>Bonito é abrir nosso coração, doar o que há de mais puro em nós, porque todo o resto já existe monstruosamente exagerado, na face da Terra!</p>
<p>Bonito é a troca de carinho, toque, palavras, olhar, presença!</p>
<p>Bonito é deitar a cabeça no travesseiro, com sorriso no rosto...</p>
<p>É receber a ligação quando você menos espera: &quot;Saudades!&quot;</p>
<p>É tremer, bambear as pernas, pagar mico...</p>
<p>Talvez toda essa beleza REALMENTE não exista...</p>
<p>Talvez a humanidade esteja aqui pelo único intuído de reprodução e crescimento da natalidade...</p>
<p>Talvez, o legal mesmo é fazer sexo sem responsabilidade, sem perguntar nomes e telefones...</p>
<p>E talvez, TAMBÉM, eu veja graça nisso um dia!</p>
</blockquote>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>none</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
