---
layout: post
title: "Typing Math"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>In this post I will try to explain some of the UX problems that I need to solve as soon as possible for my project.</p>
<div class="more">

</div>
<h1 id="description">Description</h1>
<p>In LaTeX the solution of quadratic equation is represented by :</p>
<pre><code>x = \frac{-b \pm \sqrt{b^2 - 4 a c}}{2 a}</code></pre>
<p>and will be render as</p>
<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><semantics><mrow><mi>x</mi><mo>=</mo><mfrac><mrow><mo>-</mo><mi>b</mi><mo>±</mo><msqrt><mrow><msup><mi>b</mi><mn>2</mn></msup><mo>-</mo><mn>4</mn><mi>a</mi><mi>c</mi></mrow></msqrt></mrow><mrow><mn>2</mn><mi>a</mi></mrow></mfrac></mrow><annotation encoding="TeX">x = \frac{-b \pm \sqrt{b^2 - 4 a c}}{2 a}</annotation></semantics></math>
<p>Using Firefox OS native English keyboard to type LaTeX representation of the solution of quadratic equation the user need to type:</p>
<table>
<tbody>
<tr class="odd">
<td><blockquote>
<p>x</p>
</blockquote></td>
<td>?123</td>
<td><blockquote>
<p>ALT</p>
</blockquote></td>
<td><blockquote>
<p>=</p>
</blockquote></td>
<td><blockquote>
<p>\</p>
</blockquote></td>
<td><blockquote>
<p>ABC</p>
</blockquote></td>
<td><blockquote>
<p>f</p>
</blockquote></td>
<td><blockquote>
<p>r</p>
</blockquote></td>
<td><blockquote>
<p>a</p>
</blockquote></td>
<td><blockquote>
<p>c</p>
</blockquote></td>
<td>?123</td>
<td><blockquote>
<p>ALT</p>
</blockquote></td>
<td><blockquote>
<p>{</p>
</blockquote></td>
<td><blockquote>
<p>ALT</p>
</blockquote></td>
<td><blockquote>
<p>--</p>
</blockquote></td>
<td><blockquote>
<p>ABC</p>
</blockquote></td>
<td><blockquote>
<p>b</p>
</blockquote></td>
<td><blockquote>
<p>...</p>
</blockquote></td>
</tr>
</tbody>
</table>
<p>You can check the keys with the screenshot below:</p>
<figure>
<img src="/images/en.png" alt="All three layers of Firefox OS native English keyboard." class="align-center" style="width:90.0%" /><figcaption>All three layers of Firefox OS native English keyboard.</figcaption>
</figure>
<p>One of the goals of my project is reduce the number of switches between keyboard layers and also the number of key typed.</p>
<figure>
<img src="/images/hacking-style.png" alt="Prototype of math keyboard." class="align-center" style="width:30.0%" /><figcaption>Prototype of math keyboard.</figcaption>
</figure>
<p>Having enabled the English keyboard and my currently prototype (<a href="https://github.com/r-gaia-cs/gaia/commit/dc2bb41524d0d9d788b0b6a0b92888da08cc87b4">available at GitHub</a>) to type the same equation the user will need to type:</p>
<table>
<tbody>
<tr class="odd">
<td><blockquote>
<p>x</p>
</blockquote></td>
<td><blockquote>
<p><img src="/images/u1F310.png" alt="g" /></p>
</blockquote></td>
<td><blockquote>
<p>=</p>
</blockquote></td>
<td><blockquote>
<p>x/y</p>
</blockquote></td>
<td><blockquote>
<p>--</p>
</blockquote></td>
<td><blockquote>
<p><img src="/images/u1F310.png" alt="g" /></p>
</blockquote></td>
<td><blockquote>
<p>b</p>
</blockquote></td>
<td><blockquote>
<p>...</p>
</blockquote></td>
</tr>
</tbody>
</table>
<h1 id="ux-problem">UX Problem</h1>
<p>So far I believe that I need to add five layouts to the standard ones:</p>
<ol type="a">
<li>lowercase Greek letters</li>
<li>uppercase Greek letters</li>
<li>basic math operators</li>
<li>math functions</li>
<li>set operators</li>
</ol>
<p><strong>How quickly switch between this keyboard layouts?</strong> Use the globe with meridians is suitable?</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>GSOC</p>
</div>
<div class="tags">
<p>GSOC2014</p>
</div>
<div class="comments">

</div>
