---
layout: post
title: "Election"
author: "Raniere Silva"
categories: travelling
tags: ["Africa", "Botswana", "Gaborone"]
image:
  feature: 2019-09-22-election.jpg
  author:  Raniere Silva
  licence: CC-BY
---

Botswana will have general election on 23 October
and I have the opportunity to see some election propaganda.
The propaganda to "Reopenning of [BCL Mine](https://en.wikipedia.org/wiki/BCL_Limited)"
was the topic of some conversation that I had.

From Wikipedia,

> Following lower market prices [of copper] the [BCL] mine was shut down in 2016, making over 5.000 people unemployed [or 10% of Selebi-Phikwe population].

What can companies like
Apple, that is using 100 percent recycled aluminum,
and [Fairphone](https://www.fairphone.com), that is using [fair materials](https://www.fairphone.com/en/our-goals/fair-materials/),
do to avoid social problems like the one that happen in Selebi-Phikwe?