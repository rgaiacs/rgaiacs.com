---
layout: post
title: "Privacidade e localização de servidores"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Na <a href="http://www.revista.espiritolivre.org">Revista Espírito Livre</a> apareceu a reportagem &quot;<a href="http://www.telesintese.com.br/index.php/indice-geral-plantao-em-destaque/23171-telecom-deve-regular-mundo-ip-para-evitar-quebra-de-sigilos-defende-vice-presidente-da-anatel">Jarbas Valente defende lei que obrigue Google e Facebook a ter data center no país</a>&quot; (<span data-role="download">cópia local &lt;telecom-deve-regular-mundo-ip.html&gt;</span>) que foi originalmente publicada na <a href="http://www.telesintese.com.br/">tele.síntese</a> que foi escrito por Mirian Aquino.</p>
<div class="more">

</div>
<p>A fala, abaixo, de Valente me chamou a atenção:</p>
<blockquote>
<p>Para ele, então, as telecomunicações - neste caso, a Anatel - que já preserva o sigilo dos sinais de voz e dados das telecomunicações - poderia assegurar que o sigilo da internet também fosse preservado. Ele observou que, atualmente, os dados dos e-mails, perfis e contas de todos os brasileiros não estão no Brasil, mas sim na base de dados dos provedores globais, como Google ou Facebook. Valente defende que seja crida uma legislação brasileira para obrigar que os provedores de internet tenham data centers no Brasil. &quot; A nuvem da internet é física, e os servidores devem ficar fisicamente em nosso território&quot;, defende ele.</p>
</blockquote>
<p>Até onde eu sei, para que o sigilo da internet fosse preservado precisariamos usar a <a href="https://www.torproject.org/">rede Tor</a> e basicamente é uma questão de educação e não de legislação.</p>
<p>Em segundo lugar, essa &quot;legislação basileira para obrigar que os provedores de internet tenham data center no Brasil&quot; não ajudaria em nada a privacidade dos usuários. É apenas uma forma do governo resolver um problema que possue atualmente quando precisa de alguma informação sigilosa e o &quot;provedor de internet&quot; se recusa a fornecer pois os dados encontram-se em outro país como no caso mais recente:</p>
<blockquote>
<p>O Google Brasil, em sua defesa, alegou não ser possível cumprir a ordem judicial uma vez que todos os dados do serviço Gmail estão armazenados nos servidores dos Estados Unidos e, desta forma, sujeitos às legislações daquele país. A empresa sugeriu uma forma diplomática para se obter esses dados. [<a href="http://info.abril.com.br/noticias/mercado/justica-determina-que-google-brasil-libere-dados-de-e-mails-06062013-12.shl">Fonte: Revista Info</a>]</p>
</blockquote>
<p>Se o governo possue interesse em respeitar a privacidade dos brasileiros as seguintes medidas deveriam ser adotadas:</p>
<ol>
<li><p>Completa migração para IPV6 e obrigar as operadoras de internet a vender apenas IP fixo.</p>
<p>Ao migrar para IPV6 existiria um número de IP suficiente a serem utilizados e o IP fixo permitiria os brasileiros hospedarem os serviços que desejassem.</p></li>
<li><p>Proibir as operadoras de internet de bloquear portas.</p>
<p>Atualmente as operadoras costuma bloquear algumas portas, como por exemplo a 80 (protocolo http que corresponde a web), o que impossibilita do brasileiro hospedar alguns serviços da maneira usual.</p></li>
<li><p>Baratear computadores de placa única do tamanho de um cartão de crédito como a <a href="http://pt.wikipedia.org/wiki/Raspberry_Pi">Raspberry Pi</a> e Beagleboard
&lt;http://pt.wikipedia.org/wiki/Beagleboard&gt;.</p>
<p>Esses computadores conseguem rodar a maioria dos serviços de internet que uma pessoa pode precisar (hospedagem de sites, email, chat, ...), não consomem muita energia e são razoavelmente baratos lá fora (em torno de US$ 50,00). Infelizmente aqui no Brasil elas acabam saindo bem caras, principalmente devido ao imposto.</p></li>
</ol>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>none</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
