---
layout: post
title: "Mediocridade"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Dias atrás eu listei alguns post de blogs sobre o Movimento Passe Livre, para saber mais <span data-role="doc">../22/movimento_passe_livre</span>.</p>
<p>Ontem, um amigo meu, Sergio Durigan Junior, escreveu sua opinião sobre as manifestações populares das últimas semanas e outras coisas. Como compartilho da sua opinião, resolvi indicar o post dele que você encontra <a href="http://blog.sergiodj.net/post/2013-06-29-a-era-da-mediocridade/">aqui</a> (<span data-role="download">cópia nesse servidor &lt;a-ere-da-mediocidade.html&gt;</span>).</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>none</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
