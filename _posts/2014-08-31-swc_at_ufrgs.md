---
layout: post
title: "Software Carpentry na UFRGRS"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<figure>
<img src="/images/grupo.jpg" class="align-center" style="width:80.0%" />
</figure>
<div class="admonition">
<p>Note</p>
<p>If you don't speek portuguese you will find some informations at <a href="http://software-carpentry.org/blog/2014/08/ufrgs-bootcamp.html">Software Carpentry Blog</a>.</p>
</div>
<div class="admonition">
<p>Nota</p>
<p>As fotos originais utilizadas nesse post encontram-se disponíveis em <span data-role="download">imagens do workshop &lt;ufrgs.zip&gt;</span>.</p>
</div>
<p>Na última quinta e sexta, Alex e eu fomos os instrutores no workshop da Software Carpentry na Universidade Federal do Rio Grande do Sul. Tivemos em torno de 30 alunos dos mais variados níveis (desde alunos do ensino médio até professores titulares da universidade) e apresentamos Bash, Git e Python ao longo dos dois dias.</p>
<div class="more">

</div>
<figure>
<img src="/images/front.jpg" class="align-center" style="width:50.0%" />
</figure>
<p>Primeiramente gostaria de agradecer a todos que ajudaram a organizar e realizar esse workshop, especialmente o Rafael Pezzi e o Lucas Leal. O workshop ocorreu em um dos laboratórios de computação da universidade e utilizamos uma máquina virtual (configurado pelo nosso anfitrião) nas máquinas do laboratório.</p>
<figure>
<img src="/images/back.jpg" class="align-center" style="width:50.0%" />
</figure>
<p>Na manhã do primeiro dia, ensinei alguns comandos do terminal. A maioria dos alunos já tinham utilizado-o anteriormente mas eles gostaram de descobrir alguns novos comandos (por exemplo, <code>find</code>) e que podem criar seus próprios shell scripts. Durante a tarde, Alex realizou uma introdução ao Python cobrindo a criação de variáveis, laços, condicionais e funções.</p>
<figure>
<img src="/images/alex.jpg" class="align-center" style="width:50.0%" />
</figure>
<p>Na manhã do segundo dia, ensinei Git. Essa era a sessão que a maioria dos estudantes estavam aguardando e eles gostaram do que foi apresentado. Na tarde, Alex apresentou as bibliotecas científicas básicas de Python (Numpy e Matplotlib) e Pandas reescrevendo o mesmo exemplo com as diferentes bibliotecas. Os estudantes ficaram impressionados com a facilidade de utilizar Pandas para fazer análise de dados.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>Software Carpentry,Mozilla Science Lab,Mozilla Brasil</p>
</div>
<div class="tags">
<p>UFRGS,CTA</p>
</div>
<div class="comments">

</div>
