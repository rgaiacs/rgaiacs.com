---
layout: post
title: "Escrita Científica"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Nos últimos dias eu tenho falado com algumas pessoas sobre escrita científica, os avanços tecnológicos na última década e como vários cientistas ainda ignoram essas novas tecnologias.</p>
<p>Nesse post você vai descobrir quais são essas tecnologias.</p>
<div class="more">

</div>
<h1 id="arxiv">arXiv</h1>
<figure>
<img src="/images/arxiv.png" class="align-center" style="width:80.0%" />
</figure>
<p><a href="http://arxiv.org/">arXiv</a> &quot;é um serviço de impressão eletrônica no campo da física, matemática, ciência da computação, biologia quantitativa, economia quantitativa e estatística&quot; (tradução da definição presente no site) financiado por <a href="http://www.library.cornell.edu/aboutus">Cornell University Library</a>, <a href="https://simonsfoundation.org/">Simons Foundation</a> e <a href="https://confluence.cornell.edu/x/9YhjCg">outros</a>.</p>
<p>No arXiv é possível publicar pre-prints em acesso aberto, i.e. qualquer pessoa pode ler seu trabalho de graça, <strong>de forma gratuita</strong>.</p>
<p>Anne Gentil-Beccot, Salvatore Mele e Travis C. Brooks já <a href="http://arxiv.org/abs/0906.5418">mostram</a> que a publicação de pre-prints aumenta o número de citações de um trabalho.</p>
<figure>
<img src="/images/citacao-media.jpg" alt="Gráfico retirado do trabalho de Anne Gentil-Beccot, Salvatore Mele e Travis C. Brooks disponível em http://arxiv.org/abs/0906.5418." class="align-center" style="width:80.0%" /><figcaption>Gráfico retirado do trabalho de Anne Gentil-Beccot, Salvatore Mele e Travis C. Brooks disponível em <a href="http://arxiv.org/abs/0906.5418" class="uri">http://arxiv.org/abs/0906.5418</a>.</figcaption>
</figure>
<figure>
<img src="/images/citacao-acumulada.jpg" alt="Gráfico retirado do trabalho de Anne Gentil-Beccot, Salvatore Mele e Travis C. Brooks disponível em http://arxiv.org/abs/0906.5418." class="align-center" style="width:80.0%" /><figcaption>Gráfico retirado do trabalho de Anne Gentil-Beccot, Salvatore Mele e Travis C. Brooks disponível em <a href="http://arxiv.org/abs/0906.5418" class="uri">http://arxiv.org/abs/0906.5418</a>.</figcaption>
</figure>
<p>Se o arxiv não aceita trabalhos na sua área, existem vários outros, e.g. <a href="http://www.biorxiv.org/" class="uri">http://www.biorxiv.org/</a>.</p>
<h1 id="authorea">Authorea</h1>
<figure>
<img src="/images/authorea.png" class="align-center" style="width:80.0%" />
</figure>
<p><a href="https://www.authorea.com/">Authorea</a> é uma plataforma online para escrita colaborativa. Plataformas online para escrita colaborativa é algo velho na internet (o <a href="https://en.wikipedia.org/wiki/Google_Docs,_Sheets,_and_Slides#History">Google Docs começou em 2005</a>) mas o número de opções que oferece funcionalidades para a escrita científica, i.e. suporte a equações matemáticas, referência cruzada, citação e referência bibliográfica, é bem limitado.</p>
<p>Além do Authorea oferecer todas as funcionalidades para a escrita científica, ele também oferece</p>
<ul>
<li>suporte a Markdown e LaTeX,</li>
<li>suporte a Endnote, Mendeley e Zotero, e</li>
<li>controle de versão via Git.</li>
</ul>
<p>Mas a melhor parte é que ele foi feito pensando na web. Depois que você terminou de escrever você só precisa tornar público seu texto que ele será disponibilizado em HTML (isso permite visualizações interativas) e seus leitores podem comentar seu texto. <strong>Como periódicos ainda não migraram para a web, você pode baixar o texto em PDF, Word, LaTeX e ZIP.</strong></p>
<div class="admonition">
<p>Nota</p>
<p>Para aqueles que <strong>precisam</strong> utilizar LaTeX as opções são <a href="http://sharelatex.com/">ShareLaTeX</a>, que <a href="http://github.com/sharelatex/">possui o código livre</a>, e <a href="http://overleaf.com/">Overleaf</a> .</p>
</div>
<h1 id="ipython-notebook-e-rmarkdown">IPython Notebook e RMarkdown</h1>
<figure>
<img src="/images/nbview.png" class="align-center" style="width:80.0%" />
</figure>
<p>No caso da sua pesquisa depender de resultados computacionais você provavelmente já ouvi falar do <a href="https://jupyter.org/">IPython Notebook</a> e do <a href="http://rmarkdown.rstudio.com/">RMarkdown</a>. Ambos são dois novos formatos de arquivos para <a href="https://en.wikipedia.org/wiki/Literate_programming">programação literária</a> que a cada dia ganha mais usuários.</p>
<p>Se você escrever seu texto em um desses novos formatos você pode, <strong>sem muita dificuldade</strong>, convertê-los para HTML, PDF, Word e LaTeX utilizando o <a href="http://pandoc.org/">Pandoc</a>.</p>
<h1 id="git-gitlab-ou-github">Git + GitLab ou GitHub</h1>
<figure>
<img src="/images/github.png" class="align-center" style="width:80.0%" />
</figure>
<p>Git é o estado da arte em controle de versão. A maior parte dos usuários começaram a utilizá-lo para versionar algum software que estavam desenvolvendo e acabaram adotando-o para qualquer outro projeto que estejam trabalhando pois ele ajuda a coordenar o trabalho de uma equipe.</p>
<p>Em termos de colaboração, o GitHub tornou-se a principal plataforma utilizada e tem investido bastante para o uso por pesquisadores, e.g. agora ele <a href="https://github.com/blog/1995-github-jupyter-notebooks-3">oferece pré-visualização para IPython Notebook</a>, é <a href="https://github.com/blog/1986-announcing-git-large-file-storage-lfs">otimizado para grandes arquivos binários</a> e lhe possibilita <a href="https://github.com/blog/1840-improving-github-for-science">obter um DOI</a>.</p>
<div class="admonition">
<p>GitLab</p>
<p>Se você deseja utilizar uma solução baseada em software livre, o maior nome hoje é o <a href="http://gitlab.com/">GitLab</a>.</p>
</div>
<h1 id="open-science-framework">Open Science Framework</h1>
<figure>
<img src="/images/osf.png" class="align-center" style="width:80.0%" />
</figure>
<p>No Git, o usuário tem total controle sobre as versões e que informação é transmitida de uma cópia para outra. Embora isso seja importante para algumas pessoas o preço a ser pago é um pouco alto (&quot;nada&quot; é automatizado no Git) e por esse motivo algumas pessoas desejam utilizar algo que seja um pouco mais automatizado em detrimento de um menor poder de controle.</p>
<p>Quando Git não é uma opção, uma alternativa é o <a href="https://osf.io/">Open Science Framework</a>.</p>
<h1 id="conclusões">Conclusões</h1>
<p>A tecnologia para escrita científica colaborativa, que não envolva trocar PDF/DOC por email, já está disponível para uso. A única coisa que falta são os pesquisadores testá-las e adotá-las nos seu dia-a-dia.</p>
<div class="admonition">
<p>Treinamento</p>
<p>Uma das barreiras para a adoção de uma nova tecnologia é os novos usuários serem treinados para utilizá-la. Em termos da escrita científica colaborativa e das tecnologias aqui listadas, a <a href="http://software-carpentry.org/">Software Carpentry</a> tem feito um ótimo trabalho.</p>
</div>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>academia,ciência aberta,educação</p>
</div>
<div class="tags">
<p>Git,LaTeX,ShareLaTeX,OSF</p>
</div>
<div class="comments">

</div>
