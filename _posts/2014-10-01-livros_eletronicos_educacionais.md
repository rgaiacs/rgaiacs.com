---
layout: post
title: "Livros Eletrônicos Educacionais"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Hoje eu vi <a href="http://porvir.org">PROVIR</a>, um dos blogs que acompanho, a <a href="http://porvir.org/garimpo/projetos-sobre-livros-eletronicos-em-discussao/20141001">reprodução</a> de uma <a href="http://www12.senado.gov.br/jornal/edicoes/2014/09/26/projetos-sobre-livros-eletronicos-em-discussao">notícia publicada no Jornal do Senado</a> sobre livros eletrônicos. Resolvi conferir os projetos mencionados na notícia e aqui encontram-se minhas impressões.</p>
<div class="more">

</div>
<h1 id="pls-1142010"><a href="http://www.senado.leg.br/atividade/materia/detalhes.asp?p_cod_mate=96609">PLS 114/2010</a></h1>
<p>&quot;Aprovado em caráter terminativo na Comissão de Educação, Cultura e Esporte do Senado (CE) em 2012, aguarda votação na Câmara. De autoria do senador Acir Gurgacz (PDT-RO), o texto tem o objetivo de alterar a Política Nacional do Livro (Lei 10.753/2003) para garantir aos conteúdos (e-books) e equipamentos de leitura digital (e-readers) os mesmos benefícios tributários do livro impresso. De acordo com a Constituição, os livros são livres de impostos.&quot; (Retirado de <a href="http://www12.senado.gov.br/jornal/edicoes/2014/09/26/projetos-sobre-livros-eletronicos-em-discussao" class="uri">http://www12.senado.gov.br/jornal/edicoes/2014/09/26/projetos-sobre-livros-eletronicos-em-discussao</a>)</p>
<p><a href="http://www.senado.leg.br/atividade/materia/getPDF.asp?t=114698&amp;tp=1">No texto do PLS 114/2010</a> temos, destaque feito pelo autor desse texto:</p>
<blockquote>
<p>&quot;Considera-se livro, para efeitos desta Lei, a publicação de textos escritos em fichas ou folhas, (...), assim como a publicação desses textos <strong>convertidos</strong> em form ato digital, magnético ou ótico, ou impressos no Sistema Braille.&quot;</p>
</blockquote>
<p>Note a presença da palavra &quot;convertidos&quot;. Na minha leitura um livro digital só é considerado livro, pelos termos da lei, se ele primeiro for impresso. Isso é um retrocesso. Atualmente vários livros só existem no formato digital e muitos só são impressos depois de obterem imenso sucesso na internet <strong>quando estavam disponíveis apenas no formato digital</strong>.</p>
<h1 id="pls-3942012">PLS 394/2012</h1>
<p>&quot;Propõe a redução a zero das alíquotas do PIS-Pasep e da Contribuição para o Financiamento da Seguridade Social (Cofins) sobre a receita da venda a varejo de softwares educacionais e livros eletrônicos para utilização em tablets.&quot; (Retirado de <a href="http://www12.senado.gov.br/jornal/edicoes/2014/09/26/projetos-sobre-livros-eletronicos-em-discussao" class="uri">http://www12.senado.gov.br/jornal/edicoes/2014/09/26/projetos-sobre-livros-eletronicos-em-discussao</a>)</p>
<p><a href="http://legis.senado.leg.br/mateweb/arquivos/mate-pdf/116016.pdf">No texto do PLS 394/2012</a> temos:</p>
<blockquote>
<p>&quot;programas de computador (softwares) educacionais e livros eletrônicos (e-book), para utilização em máquinas automáticas de processamento de dados, portáteis, sem teclado, que tenham uma unidade central de processamento com entrada e saída de dados por meio de uma tela sensível ao toque de área superior a 140 cm² (cento e quarenta centímetros quadrados) e inferior a 600 cm² (seiscentos centímetros quadrados) (...)&quot;</p>
</blockquote>
<p>A proposta parece muito boa, o único problema é quem vai definir o que é um software educacional e um software não-educacional. <a href="http://libreoffice.org/">LibreOffice</a> é um software educacional? Posso utilizá-lo para escrever uma apostila para os alunos. <a href="http://openshot.org/">OpenShot</a> é um software educacional? Posso utilizá-lo para criar ótimos vídeos.</p>
<h1 id="pls-1092013">PLS 109/2013</h1>
<p>&quot;Determina o fornecimento de tablets aos estudantes das escolas públicas de educação básica até 2023.&quot; (Retirado de <a href="http://www12.senado.gov.br/jornal/edicoes/2014/09/26/projetos-sobre-livros-eletronicos-em-discussao" class="uri">http://www12.senado.gov.br/jornal/edicoes/2014/09/26/projetos-sobre-livros-eletronicos-em-discussao</a>)</p>
<p><a href="http://legis.senado.leg.br/mateweb/arquivos/mate-pdf/124889.pdf">No texto do PLS 109/2013</a> temos,</p>
<blockquote>
<p>&quot;Até o início do ano letivo de 2023, as escolas públicas disponibilizarão, para uso individual, computadores portáteis, de tamanho pequeno, fina espessura e com tela sensível ao toque – os <em>tablets</em> –, a todos os seus alunos, a partir do sexto ano do ensino fundamental, até a conclusão do ensino médio, para o desenvolvimento de atividades de aprendizagem.</p>
</blockquote>
<p>A proposta parece muito bonita, os únicos problemas são:</p>
<ul>
<li>falta de garantia de que esse investimento irá ajudar no ensino dos alunos,</li>
<li>acessibilidade para alunos com necessidades especiais,</li>
<li>treinamento dos professores,</li>
<li>oferta de internet rápida aos alunos (<strong>tanto na escola como em casa</strong>) pois grande parte dos materiais educacionais encontram-se na internet ou são dependentes desta,</li>
<li>logística de entrega dos equipamentos,</li>
<li>logística de reparo dos equipamentos.</li>
</ul>
<p>O Senador Cristovam Buarque <a href="http://legis.senado.leg.br/mateweb/arquivos/mate-pdf/142732.pdf">submeteu uma emenda</a> que tenta &quot;resolver&quot; a falta de garantia do investimento em tecnologia ser convertido em melhora no ensino. O único problema é que &quot;uma avaliação quantitativa&quot; é muito vago.</p>
<p>O segundo artigo desse PLS diz que &quot;Os equipamentos (...) deverão ter acesso à rede mundial de computadores e contar com programas e aplicativos de natureza didática, inclusive aqueles específicos para os alunos com necessidades especiais.&quot; Isso resolve a questão de acessibilidade se o governo for <a href="http://store.apple.com/br/ipad">comprar equipamentos com preço superior a R$ 1.499,00</a> para cada um dos alunos. De toda forma, o artigo é vago em dizer &quot;deverão ter acesso à rede mundial de computadores&quot; porque um equipamento como conexão wifi tem esse acesso <strong>desde que conectado a uma rede wifi que por sua vez esteja conectada na internet</strong>.</p>
<p>O terceiro artigo aborda o problema de capacitação dos professores embora até onde eu saiba isso nunca foi muito efetivo.</p>
<h1 id="site-do-senado">Site do Senado</h1>
<p>Na página inicial do <a href="http://www.senado.leg.br/">Senado</a> não achei onde encontram-se os projetos de lei. Ao <a href="http://www12.senado.gov.br/noticias/busca?SearchableText=PLS+114%2F2010">procurar por &quot;PSL 114/2010&quot;</a> os resultados encontrados foram de notícias e não o texto do projeto.</p>
<p>Utilizando um buscador, descobri que você deve ir em <a href="http://www.senado.leg.br/atividade/">Atividade Legislativa</a> para encontrar o texto original.</p>
<p>Infelizmente ele não deixa você abrir dois projetos em abas.</p>
<p>E para acompanhar um projeto você ainda precisa ter um cadastro no sistema deles. Eles poderiam fornecer um feed RSS.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>REA</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
