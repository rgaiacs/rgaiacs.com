---
layout: post
title: "Smoke Test for Virtual Keyboard"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>I and my mentor agree to do an smoke test for the project and when the keyboard pass it we submit will submit the keyboard to Marketplace. Below is the smoke test.</p>
<div class="more">

</div>
<table style="width:94%;">
<colgroup>
<col style="width: 81%" />
<col style="width: 12%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Test Description</td>
<td>Status</td>
</tr>
<tr class="even">
<td><p>Switch row works?</p>
<p>Click on switch row keys.</p></td>
<td><span data-role="raw-html">&lt;span style=&quot;color:green;&quot;&gt;PASS&lt;/span&gt;</span></td>
</tr>
<tr class="odd">
<td><p>Latin keyboard works?</p>
<p>Click on latin letter.</p></td>
<td><span data-role="raw-html">&lt;span style=&quot;color:green;&quot;&gt;PASS&lt;/span&gt;</span></td>
</tr>
<tr class="even">
<td><p>Shift key for latin letters works?</p>
<p>Click on shift key and one latin letter.</p></td>
<td><span data-role="raw-html">&lt;span style=&quot;color:green;&quot;&gt;PASS&lt;/span&gt;</span></td>
</tr>
<tr class="odd">
<td><p>Shift key lock for latin letters works?</p>
<p>Lock the shif key and click on latin letter.</p></td>
<td><span data-role="raw-html">&lt;span style=&quot;color:green;&quot;&gt;PASS&lt;/span&gt;</span></td>
</tr>
<tr class="even">
<td><p>Backspace works for latin letter?</p>
<p>Click on latin letter and on backspace.</p></td>
<td><span data-role="raw-html">&lt;span style=&quot;color:green;&quot;&gt;PASS&lt;/span&gt;</span></td>
</tr>
<tr class="odd">
<td><p>Alternative latin letters works?</p>
<p>Click and hold on latin letter (e.g. &quot;A&quot;).</p></td>
<td><span data-role="raw-html">&lt;span style=&quot;color:green;&quot;&gt;PASS&lt;/span&gt;</span></td>
</tr>
<tr class="even">
<td><p>Alternate keyboard works?</p>
<p>Click on alternate key.</p></td>
<td><span data-role="raw-html">&lt;span style=&quot;color:green;&quot;&gt;PASS&lt;/span&gt;</span></td>
</tr>
<tr class="odd">
<td><p>Backspace works for alternate keys works?</p>
<p>Click on alternate key and on backspace.</p></td>
<td><span data-role="raw-html">&lt;span style=&quot;color:green;&quot;&gt;PASS&lt;/span&gt;</span></td>
</tr>
<tr class="even">
<td><p>Alternative alternate key works?</p>
<p>Click and hold on alternative key (e.g. &quot;1&quot;).</p></td>
<td><span data-role="raw-html">&lt;span style=&quot;color:green;&quot;&gt;PASS&lt;/span&gt;</span></td>
</tr>
<tr class="odd">
<td><p>Symbol keyboard works?</p>
<p>Click on symbols key.</p></td>
<td><span data-role="raw-html">&lt;span style=&quot;color:green;&quot;&gt;PASS&lt;/span&gt;</span></td>
</tr>
<tr class="even">
<td><p>Backspace works for symbols?</p>
<p>Click on symbols key and on backspace.</p></td>
<td><span data-role="raw-html">&lt;span style=&quot;color:green;&quot;&gt;PASS&lt;/span&gt;</span></td>
</tr>
<tr class="odd">
<td><p>Greek keyboard works?</p>
<p>Click on greek letter.</p></td>
<td><span data-role="raw-html">&lt;span style=&quot;color:green;&quot;&gt;PASS&lt;/span&gt;</span></td>
</tr>
<tr class="even">
<td><p>Shift key for greek letters works?</p>
<p>Click on shift key and one greek letter.</p></td>
<td><span data-role="raw-html">&lt;span style=&quot;color:red;&quot;&gt;FAIL&lt;/span&gt;</span></td>
</tr>
<tr class="odd">
<td><p>Shift key lock for greek letters works?</p>
<p>Lock the shif key and click on greek letter.</p></td>
<td><span data-role="raw-html">&lt;span style=&quot;color:green;&quot;&gt;PASS&lt;/span&gt;</span></td>
</tr>
<tr class="even">
<td><p>Backspace works for latin letter?</p>
<p>Click on latin letter and on backspace.</p></td>
<td><span data-role="raw-html">&lt;span style=&quot;color:red;&quot;&gt;FAIL&lt;/span&gt;</span></td>
</tr>
<tr class="odd">
<td><p>Math Symbol keyboard works?</p>
<p>Click on math symbol key.</p></td>
<td><span data-role="raw-html">&lt;span style=&quot;color:green;&quot;&gt;PASS&lt;/span&gt;</span></td>
</tr>
<tr class="even">
<td><p>Backspace works for math symbols?</p>
<p>Click on math symbol and on backspace.</p></td>
<td><span data-role="raw-html">&lt;span style=&quot;color:red;&quot;&gt;FAIL&lt;/span&gt;</span></td>
</tr>
<tr class="odd">
<td><p>Alternative math symbol works?</p>
<p>Click and hold on math symbol (e.g. &quot;∫&quot;).</p></td>
<td><span data-role="raw-html">&lt;span style=&quot;color:green;&quot;&gt;PASS&lt;/span&gt;</span></td>
</tr>
<tr class="even">
<td><p>Jumps works for math symbols?</p>
<p>Click on math symbol and on &quot;RETURN&quot;.</p></td>
<td><span data-role="raw-html">&lt;span style=&quot;color:green;&quot;&gt;PASS&lt;/span&gt;</span></td>
</tr>
<tr class="odd">
<td><p>Math Functions keyboard works?</p>
<p>Click on math function key.</p></td>
<td><span data-role="raw-html">&lt;span style=&quot;color:green;&quot;&gt;PASS&lt;/span&gt;</span></td>
</tr>
<tr class="even">
<td><p>Backspace works for math function?</p>
<p>Click on math symbols and on backspace.</p></td>
<td><span data-role="raw-html">&lt;span style=&quot;color:red;&quot;&gt;FAIL&lt;/span&gt;</span></td>
</tr>
</tbody>
</table>
<h1 id="fail-report">Fail Report</h1>
<ul>
<li>At greek keyboard, the shift key isn't disable after press one key.</li>
<li>Click twice on a greek letter and on backspace. It should remove only one of the letters.</li>
<li>Click twice on a math symbol and on backspace. It should remove only one of the symbols.</li>
<li>Click twice on a math symbol and on backspace. It should remove only one of the functions.</li>
</ul>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>GSOC</p>
</div>
<div class="tags">
<p>GSOC2014</p>
</div>
<div class="comments">

</div>
