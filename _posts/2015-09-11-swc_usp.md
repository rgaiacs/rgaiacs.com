---
layout: post
title: "Software Carpentry at USP"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p><span data-role="doc">Last year &lt;../../../2014/09/07/swc_at_ccsl&gt;</span> Software Carpentry ran two workshops at USP and they ran another workshop this week.</p>
<div class="admonition">
<p>Thanks ...</p>
<p>... to Anna Penna, the amazing host of this workshop that I'm in debit with.</p>
</div>
<div class="admonition">
<p>Thanks ...</p>
<p>... to <a href="http://fdte.org.br/">FDTE</a>, <a href="http://ccsl.ime.usp.br/">CCSL</a> for the financial support for the workshop.</p>
</div>
<div class="admonition">
<p>Thanks ...</p>
<p>... to Diego Rabatone Oliveira and Luciano Ramalho that volunteer their time to teach at the workshop.</p>
</div>
<div class="admonition">
<p>Thanks ...</p>
<p>... to Luciano Issoe and Yuri Teixeira that volunteer their time helping the learners during the workshop without it the workshop won't be so great.</p>
</div>
<div class="admonition">
<p>Thanks ...</p>
<p>... to Haydee Svab that also helped organizing the workshop.</p>
</div>
<div class="more">

</div>
<h1 id="shell">Shell</h1>
<p>The workshop started with <a href="https://swcarpentry.github.io/shell-novice/">Software Carpentry's standard lesson about the shell</a>. Unfortunately I didn't have time to cover shell scripts, <code>grep</code> and <code>find</code>.</p>
<p>I only made two changes when taught the lesson:</p>
<ul>
<li>reword of pipes and filters (this already has a pull request, <a href="https://github.com/swcarpentry/shell-novice/pull/225" class="uri">https://github.com/swcarpentry/shell-novice/pull/225</a>) and</li>
<li>cover loops before pipes and filters.</li>
</ul>
<p>During the coffee break, one of the learners mention to me that s/he was expecting to learn more about <code>awk</code> and <code>sed</code>. I will enjoy teach one advanced shell workshop covering <a href="https://swcarpentry.github.io/shell-extras/">Software Carpentry's extra material about the shell</a>.</p>
<h1 id="python">Python</h1>
<p>In the afternoon of the first day, Luciano Ramalho taught the introduction to Python. Luciano is famous at the Python community and has a lot of experience teaching programming.</p>
<h1 id="git">Git</h1>
<p>The second day started with the Git session leaded by Diego Rabatone Oliveira. As usual, 3 hours wasn't enough time to fully cover <a href="http://swcarpentry.github.io/git-novice/">Software Carpentry's standard lesson about Git</a>.</p>
<h1 id="python---plots">Python - Plots</h1>
<p>When we planned the workshop we proposal to cover tests during the afternoon of the second day <strong>mostly</strong> because I want to teach it. At the last minute I choose to change the program and use the afternoon to cover</p>
<ul>
<li>plots with <a href="http://matplotlib.org/">matplotlib</a>,</li>
<li>plots with <a href="http://bokeh.pydata.org/">Bokeh</a> and</li>
<li>use of scripts on Jupyter Notebooks</li>
</ul>
<p>because</p>
<ol type="1">
<li>plots were only briefly covered on the first day,</li>
<li>learners requested it on the feedback of the first day and</li>
<li>plot is something that beginners will use more.</li>
</ol>
<p>Two learners contacted one of the helpers and mention that they were only attending the workshop because of the session about tests. Since was only two students and the original plan was live coding the section from <a href="http://physics.codes/">Effective Computation in Physics</a> about tests I lead my copy to the students, suggested that they follow the section they are interested and asked questions if needed. <strong>Unfortunatelly</strong> they leave the room early and I couldn't talk with them.</p>
<h1 id="bad-things">Bad things</h1>
<p>We need to work for</p>
<ul>
<li>have a good speed (learns complained about <strong>too fast</strong>),</li>
<li>have more exercises (one option is move the exercises for the middle of the lesson and another one is use slides with the exercises --- I need to find time to review <a href="https://github.com/rgaiacs/swc-lesson-template/pull/5">this pull request from Rémi</a>),</li>
<li>have more time (that could solve the previous problem),</li>
<li>have more days and less time per day (that will be possible when we have at USP the same number of instructors at Berkeley or Melbourne).</li>
</ul>
<h1 id="conclusions">Conclusions</h1>
<p>This was one workshop as many others from Software Carpentry.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>Software Carpentry</p>
</div>
<div class="tags">
<p>USP,CCSL,FDTE</p>
</div>
<div class="comments">

</div>
