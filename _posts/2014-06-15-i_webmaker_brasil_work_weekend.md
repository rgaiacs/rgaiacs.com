---
layout: post
title: "I Webmaker Brasil Work Weekend"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>At this Sunday I attended Panaggio &amp; Mel's house for I Webmaker Brasil Work Weekend. During it, I worked at some records from <a href="http://softwarelivre.org/fisl15">FISL15</a> and Panaggio, Mel and Coragem at some issues for Maker Party Week.</p>
<p>I still need to edit most of the records from FISL15 but the one that required more work was done and you can watch it (<span data-role="download">720p &lt;walking-with-fox-at-fisl720.mp4&gt;</span> or <span data-role="download">360p &lt;walking-with-fox-at-fisl360.mp4&gt;</span>). This video shows Fox's walkings during FISL15 and Firefox Launch Party. The others videos are records from some talks at Mozilla's Room and just need a simple cut at the begin and another one at the end.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>Webmaker</p>
</div>
<div class="tags">
<p>Webmaker Brasil</p>
</div>
<div class="comments">

</div>
