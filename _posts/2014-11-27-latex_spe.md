---
layout: post
title: "SPE's LaTeX Course"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>This week I taught a LaTeX course for the <a href="http://www.speunicamp.org.br/">Student Chapter of Society of Petroleum Engineers at the University of Campinas</a>.</p>
<figure>
<img src="/images/students.jpg" alt="Photo of the students." class="align-center" style="width:80.0%" /><figcaption>Photo of the students.</figcaption>
</figure>
<div class="more">

</div>
<h1 id="sharelatex">ShareLaTeX</h1>
<p>This is the fifth time that I taught a LaTeX course and the first time that I use <a href="http://sharelatex.com/">ShareLaTeX</a> in a course.</p>
<p>Why did I do this? Because in the last year I was maintaining a thesis template and many users report problems due encoding, missing packages and compiling rules when using it locally. Using ShareLaTeX is a nice way to avoid this type of problem for novices.</p>
<p>Another reason to use ShareLaTeX was that SPE asked to give instructions on how format article for scientific journals. ShareLaTeX has a <a href="http://sharelatex.com/templates">nice collection of templates</a> that include many scientific journals template and this makes the live of young researchers easy.</p>
<figure>
<img src="/images/room2.jpg" class="align-center" style="width:80.0%" />
</figure>
<h1 id="students">Students</h1>
<p>I had 10 students (see the photo at the begin of this post). Most of them are graduate students and members of SPE (only one student is undergraduate and another one is from life science).</p>
<p>SPE asked before the course what was the previous knowledge of LaTeX and most of the students reported that this was the first time they are trying to use/learn LaTeX.</p>
<h1 id="course">Course</h1>
<p>The course was divided in three days with classes of 1:30 each day. The topics covered in each day was:</p>
<dl>
<dt>1st Day</dt>
<dd><ul>
<li><code>\documentclass</code></li>
<li><code>\usepackage</code></li>
<li><code>\begin{document}</code></li>
<li>Paragraph</li>
<li>Lists</li>
</ul>
</dd>
<dt>2nd Day</dt>
<dd><ul>
<li><code>\input</code></li>
<li><code>\label</code></li>
<li><code>\ref</code></li>
<li>Table</li>
<li>Figure</li>
<li>Equations</li>
</ul>
</dd>
<dt>3rd Day</dt>
<dd><ul>
<li>BibTeX and biblatex</li>
<li>beamer</li>
</ul>
</dd>
</dl>
<figure>
<img src="/images/room.jpg" class="align-center" style="width:80.0%" />
</figure>
<h1 id="feedback">Feedback</h1>
<p>Students said that they like the course and using ShareLaTeX.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>academia,ciência aberta,educação,ensino</p>
</div>
<div class="tags">
<p>SPE,LaTeX,ShareLaTeX,UNICAMP</p>
</div>
<div class="comments">

</div>
