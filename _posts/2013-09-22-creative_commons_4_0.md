---
layout: post
title: "Creative Commons 4.0"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<div class="note">
<div class="admonition-title">
<p>Note</p>
</div>
<p>Este post foi inspirado pelo <a href="http://creativecommons.org/weblog/entry/39587">anúncio do release candidate da licença Creative Commons 4.0</a>.</p>
</div>
<p>Para que não sabe as licenças Creative Commons são licenças do tipo copyleft voltadas para textos, música, vídeos, ... em oposição a GPL que é voltada para software. A primeira versão das licenças Creative Commons foi publicada no fim de 2002 e provavelmente nesse ano será lançada a quarta versão.</p>
<div class="more">

</div>
<h1 id="versão-3.0">Versão 3.0</h1>
<p>Na versão 3.0 existiam 6 licenças:</p>
<ul>
<li><a href="http://creativecommons.org/licenses/by/3.0/legalcode">CC-BY 3.0</a> (<span data-role="download">versão em texto plano &lt;cc3-by.txt&gt;</span>)</li>
<li><a href="http://creativecommons.org/licenses/by-sa/3.0/legalcode">CC-BY-SA 3.0</a> (<span data-role="download">versão em texto plano &lt;cc3-by-sa.txt&gt;</span>)</li>
<li><a href="http://creativecommons.org/licenses/by-nd/3.0/legalcode">CC-BY-ND 3.0</a> (<span data-role="download">versão em texto plano &lt;cc3-by-nd.txt&gt;</span>)</li>
<li><a href="http://creativecommons.org/licenses/by-nc/3.0/legalcode">CC-BY-NC 3.0</a> (<span data-role="download">versão em texto plano &lt;cc3-by-nc.txt&gt;</span>)</li>
<li><a href="http://creativecommons.org/licenses/by-nc-sa/3.0/legalcode">CC-BY-NC-SA 3.0</a> (<span data-role="download">versão em texto plano &lt;cc3-by-nc-sa.txt&gt;</span>)</li>
<li><a href="http://creativecommons.org/licenses/by-nc-nd/3.0/legalcode">CC-BY-NC-ND 3.0</a> (<span data-role="download">versão em texto plano &lt;cc3-by-nc-nd.txt&gt;</span>)</li>
</ul>
<h1 id="versão-4.0">Versão 4.0</h1>
<p>Existia um debate se a licença CC-BY-ND deveria ou não ser mantida mas pelo que tudo indica ela será mantida na versão 4.0 e desta forma continuaremos com as 6 licenças:</p>
<ul>
<li><a href="http://staging.creativecommons.org/licenses/by/3.0/legalcode">CC-BY 4.0 Draft</a> (<span data-role="download">versão em texto plano &lt;cc4-by.txt&gt;</span>)</li>
<li><a href="http://staging.creativecommons.org/licenses/by-sa/3.0/legalcode">CC-BY-SA 4.0 Draft</a> (<span data-role="download">versão em texto plano &lt;cc4-by-sa.txt&gt;</span>)</li>
<li><a href="http://staging.creativecommons.org/licenses/by-nd/3.0/legalcode">CC-BY-ND 4.0 Draft</a> (<span data-role="download">versão em texto plano &lt;cc4-by-nd.txt&gt;</span>)</li>
<li><a href="http://staging.creativecommons.org/licenses/by-nc/3.0/legalcode">CC-BY-NC 4.0 Draft</a> (<span data-role="download">versão em texto plano &lt;cc4-by-nc.txt&gt;</span>)</li>
<li><a href="http://staging.creativecommons.org/licenses/by-nc-sa/3.0/legalcode">CC-BY-NC-SA 4.0 Draft</a> (<span data-role="download">versão em texto plano &lt;cc4-by-nc-sa.txt&gt;</span>)</li>
<li><a href="http://staging.creativecommons.org/licenses/by-nc-nd/3.0/legalcode">CC-BY-NC-ND 4.0 Draft</a> (<span data-role="download">versão em texto plano &lt;cc4-by-nc-nd.txt&gt;</span>)</li>
</ul>
<h1 id="sumário-de-mudanças">Sumário de mudanças</h1>
<p>Parece que existia um problema ao misturar materiais licenciados com versões distintas de licenças Creative Commons. Agora, todo o material misturado em uma obra CC-BY-SA 4.0 estará licenciado sob uma única licença (a da versão mais nova da CC-BY-SA utilizada no material).</p>
<p>Na versão 4.0 foi adicionada informações que evitam utilização de qualquer tecnologia que restrinja o reuso do material como por exemplo DRM.</p>
<h1 id="outras-mudanças">Outras mudanças</h1>
<p>Agora não será mais permitido ao licenciador adaptar a licença para introduzir avisos que deverão ser uma &quot;anexo&quot; a licença.</p>
<p>Mudanças no tratamento de bancos de dados foram feitas para tornar o texto mais claro.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>none</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
