---
layout: post
title: "Online Form Review: Google Form"
author: "Raniere Silva"
categories: tools
tags: [jekyll]
image:
  feature: 2017-06-07-google-form-review-cover.jpg
  author: Ben Nuttall
  original_url: https://flic.kr/p/EM6KDJ
  licence: CC-BY
---

Today we are going to review [Google Forms](https://www.google.co.uk/forms/about/).
To use it you need a Google account
which you already have
if you already signed to any other of Google's product.

# Possibility to download the answers

You can download the responses as a `.csv`
or create a Google Spreadsheet with the responses
that you can download later in many formats.

# Email notification

You can receive email after people complete the form.
If you want to email responders you need to use Google Script
or a third-party app.

# Answer type

- short text

  ![short text widget](/images/2017-06-07-google-form-review-short-text.jpg)

  Extra feature is "Response Validation".

- paragraph

  ![paragraph widget](/images/2017-06-07-google-form-review-paragraph.jpg)

  Extra feature is "Response Validation".

- multiple choice

  ![multiple choice widget](/images/2017-06-07-google-form-review-multiple-choice.jpg)

  You can include "Other" as one choice

  ![multiple choice widget](/images/2017-06-07-google-form-review-multiple-choice-other.jpg)

  Extra features are: "Go to section based on answer" and "Shuffle option order".

- checkboxes

  ![checkboxes widget](/images/2017-06-07-google-form-review-checkboxes.jpg)

  You can include "Other" as one choice

  ![checkboxes widget](/images/2017-06-07-google-form-review-checkboxes-other.jpg)

  Extra features are: "Response Validation" and "Shuffle option order".

- dropdown

  ![dropdown widget](/images/2017-06-07-google-form-review-dropdown.jpg)

  Extra features are: "Go to section based on answer" and "Shuffle option order".

- linear scale

  ![linear scale widget](/images/2017-06-07-google-form-review-linear-scale.jpg)

- date

  ![date widget](/images/2017-06-07-google-form-review-date.jpg)

  Extra features are: "Include time" and "Include year".

- time

  ![time widget](/images/2017-06-07-google-form-review-time.jpg)

- multiple choice grid

  ![multiple choice grid
   widget](/images/2017-06-07-google-form-review-multiple-choice-grid.jpg)

  Extra features are: "Limit to one response per column" and "Shuffle row order".

- file upload

  **You can't upload files.**

# Mark question as required

Possible.

# Split the form into sections

Possible.

# Customisation on the fly

Possible for some question types.

# Response validation

Possible for some question types.

# Word Completion

Not possible.

## Evaluation table

<table>
  <thead>
    <tr>
      <th>Criteria</th>
      <th>Points</th>
      <th>Maximum of Points</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Possibility to download the answers</td>
      <td>1</td>
      <td>1</td>
    </tr>
    <tr>
      <td>Email notification</td>
      <td>1</td>
      <td>1</td>
    </tr>
    <tr>
      <td>Full range of answer type</td>
      <td>2</td>
      <td>2</td>
    </tr>
    <tr>
      <td>Mark question as required</td>
      <td>1</td>
      <td>1</td>
    </tr>
    <tr>
      <td>Split the form into sections</td>
      <td>1</td>
      <td>1</td>
    </tr>
    <tr>
      <td>Customisation on the fly</td>
      <td>1</td>
      <td>1</td>
    </tr>
    <tr>
      <td>Response validation</td>
      <td>1</td>
      <td>1</td>
    </tr>
    <tr>
      <td>Word Completion</td>
      <td>0</td>
      <td>2</td>
    </tr>
  </tbody>
  <tfoot>
    <tr>
      <td>Total</td>
      <td>8</td>
      <td>10</td>
    </tr>
  </tfoot>
</table>
