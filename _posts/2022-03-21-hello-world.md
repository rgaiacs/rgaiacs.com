---
layout: post
title: "Hello, World!"
author: "Raniere Silva"
categories: life
tags: ["blog"]
image:
  feature: 2022-03-21-hello-world.jpg
  author: Porapak Apichodilok
  licence: Pexels License
  original_url: https://www.pexels.com/photo/person-holding-world-globe-facing-mountain-346885
---

It's over a year from the last blog post
and
a couple of months with the blog unavailable.
Stay tuned for more news soon.