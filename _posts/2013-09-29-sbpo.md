---
layout: post
title: "SBPO"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Na semana retrasada, participei da <a href="http://www.sbpo2013.ect.ufrn.br/">XLV Simpósio Brasileiro de Pesquisa Operacional</a> que ocorreu em Natal (RN).</p>
<div class="more">

</div>
<figure>
<img src="/images/sbpo-capa-resumo.jpeg" class="align-center" style="width:80.0%" />
</figure>
<p>Participei do minicurso &quot;Data Envelopment Analysis - DEA, a ferramenta de Benchmarking do Setor de Energeia Elétrica Brasileiro&quot; ministrado por Ana Lucia Miranda Lopes (UFMG), <span data-role="doc">apresentei um trabalho na sessão de Trabalhos de
Iniciação Científica &lt;sbpo_tic&gt;</span> e assisti as seguintes apresentações</p>
<ol>
<li>&quot;On the quest for efficiency in healthcare: OR saving lives&quot; por Paul Harper (Cardiff University);</li>
<li>&quot;Bi-objective Multicas Packing Problem&quot; por Romerito Andrade (UEPB), Marco Coldbarg (UFRN) e Elizabeth Coldbarg (URFN);</li>
<li>&quot;Clustering Search applied to the Periodic Vehicle Routing Problem: A case study in waste collection&quot; por Eliseu Araújo (UNIFESP), Kelly Poldi (UNIFESP) e Antonio Chaves (UNIFESP);</li>
<li>&quot;A GRASP heuristic to optimize the materialization of views in the cloud&quot; por Vilmar Jefté Rodrigues de Souza (UFMG), Michael David de Souza Dutra (UFMG), Bruno Bachelet (UBP) e Laurent dOrazio (UBP);</li>
<li>&quot;Assembling a New and Improved Transposition Distance Database&quot; por Jamile Gonçalves (UFABC), Letícia Rodrigues Bueno (UFABC) e Rodrigo Hausen (UFABC);</li>
<li>&quot;Elementary shortest-paths visiting a given set of nodes&quot; por Rafael Castro de Andrade (UFC);</li>
<li>&quot;Solving bilevel combinatorial optimization as bilinear min-max optimization via a branch-and-cut algorithm&quot; por Artur Alves Pessoa (UFF), Michael Poss (Université de Technologie de Compiegne), Marcos Costa Roboredo (UFF) e Luiz Aizemberg (UFF);</li>
<li>&quot;Multicore Scalability and Efficiency Analysis of the standard simplex algorithm&quot; por Demétrios Coutinho (UFRN) e Samuel Souza (UFRN);</li>
<li>&quot;VNS based algorithms to the High School Timetabling Problem&quot; por Landir Saviniee (UEM), Ademir Constantino (UEM) e Weslye Romão (UEM);</li>
<li>&quot;Uma Proposta para Alocação de Horários de Professores e Turmas em Instituições de Ensino Superior Utilizando uma Heuristica VNS/VND&quot; por Bruno Xavier (DATACI), Alcione Silva (DATACI), Dalessandro Soares Vianna (UFF), Helder Costa (UFF) e Willen Coelho (IFES);</li>
<li>Uma eficiente heurística baseada na estratégia de divisão-e-conquista para o school timetabling problem&quot; por Camilo Bornia Poulsen (UFRGS) e Denise Lindstrom Bandeira (UFRGS);</li>
<li>Uma Distribuição Equilibrada de Salas e Professores em uma IES&quot; por Valdir Melo (UFRJ), Angelo Siqueira (UNIGRANRIO) e Abel Rodolfo Garcia Lozano (UERJ);</li>
<li>&quot;Ultra Innovation Using Simulation&quot; por Charles Harell (Brigham Young University e ProModel Corporation);</li>
<li>&quot;Alguns resultados em Grafos Ptolemaicos&quot; por Lilian Markenzon (UFRJ) e Christina Waga (UERF);</li>
<li>&quot;Um algoritmo (1/2)-aproximativo para o problema do máximo subgrafo acíclico sob restrições disjuntivas negativas&quot; por Sílvia Mapa (UFMG) e Sebastian Alberto Urrutia (UFMG);</li>
<li>&quot;Algoritmos e Complexidade para Dois Jogos de Bloco&quot; por André Castro Ramos (UFC) e Rudimi Sampaio (UFC);</li>
<li>&quot;Um método proximal para problemas de otimização multiobjetivo quase-convexa&quot; por Hellena Christina Apolinário (UFT), Kely Villacorta (UFRJ) e Paulo Oliveira (UFRJ);</li>
<li>&quot;Velocidade de convergência do Método do Gradiente aplicado à minimização de funções&quot; por Tatiane Cazarin da Silva (UFPR), Ademir Ribeiro (UFPR) e Gislaine Periçaro (UNESPAR);</li>
<li>&quot;Uma abordagem de pré-condicionamento híbrida para resolver os sistemas lineares do método de pontos interiores iterativamente&quot; por Carla Chidini (UNICAMP) e Aurelio Ribeiro L. de Oliveira (UNICAMP); e</li>
<li>&quot;Algoritmo Genético Aplicado ao Controle da Dengue&quot; por Daniela Cantane (UNESP), Helenice Florentino (UNESP), Fernando Santos (UNESP) e Bettina Bannwart (UNESP).</li>
</ol>
<h1 id="on-the-quest-for-efficiency-in-healthcare-or-saving-lives">On the quest for efficiency in healthcare: OR saving lives</h1>
<p>Essa foi a palestra inaugural do evento e na minha opinião a melhor. Paul Harper é um ótimo palestrante e o trabalho que desenvolveu, reduzir o tempo de atendimento de ambulâncias no Reino Unido, é animador.</p>
<h1 id="ultra-innovation-using-simulation">Ultra Innovation Using Simulation</h1>
<p>Essa foi a palestra de encerramento do segundo dia do evento. Achei uma droga e mais me pareceu propaganda do ProModel que qualquer outra coisa.</p>
<h1 id="pesquisa-operacional-na-educação">Pesquisa Operacional na Educação</h1>
<p>No primeiro dia assisti uma sessão voltada a educação. Na minha opinião essa sessão poderia ser renomeada para &quot;Resolvendo o Problema do Horário Escolar com Heurística&quot; porque os quatro trabalhos que assisti era sobre isso.</p>
<p>Além disso, até onde eu lembro, todos os trabalhos apresentados tratavam de resolver um caso particular do Problema do Horário Escolar que foi feito pelos autores/consultores.</p>
<h1 id="meta-heurísticas">Meta-heurísticas</h1>
<p>Não sou grande fã de (meta-)heurísticas para resolver o problema principal porque elas não possuem garantia de encontrar o melhor resultado possível e também porque os resultados obtidos &quot;não podem ser reproduzidos&quot; uma vez que utilizam-se da aleatoriedade e de médias.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>academia</p>
</div>
<div class="tags">
<p>SBPO</p>
</div>
<div class="comments">

</div>
