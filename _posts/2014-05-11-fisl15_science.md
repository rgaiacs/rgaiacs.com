---
layout: post
title: "Open Science at FISL 15"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p><a href="http://softwarelivre.org/fisl15">FISL</a> is an floss event that happens in Brazil and its 15th edition ended yesterday. This post is about open science talks that happens at FISL.</p>
<div class="more">

</div>
<h1 id="mozilla-science-lab">Mozilla Science Lab</h1>
<figure>
<img src="/images/fisl15-science.jpg" class="align-center" style="width:25.0%" />
</figure>
<p>I gave an talk about <a href="http://mozillascience.org/">Mozilla Science Lab</a> (my slide deck are available <a href="https://github.com/r-gaia-cs/fisl15-mozilla-science">here</a>).</p>
<p>Only a few people attended to my talk, maybe because:</p>
<ul>
<li>it took place at Mozilla's room,</li>
<li>this year FISL don't include the &quot;open academia&quot; as a topic for the talks and/or</li>
<li>the days that FISL happened wasn't nice for folks from academia.</li>
</ul>
<p>Most of the attendees are from Mozilla Brazilian community. None of them knew about Mozilla Science Lab before the talk and I hope they told about it to their friends.</p>
<p>At my talk I gave a overview of the (almost) first year of Mozilla Science Lab project and discuss with some attends about some topics related with open science.</p>
<h1 id="ciência-e-software-livre-ou-lá-e-de-volta-outra-vez">Ciência e Software Livre, ou Lá e de Volta Outra Vez</h1>
<figure>
<img src="/images/fisl15-abdo.jpg" alt="Photo took by Cassiana Martins." class="align-center" style="width:25.0%" /><figcaption><a href="https://www.flickr.com/photos/fislquinze/14142039532/in/photostream/">Photo took by Cassiana Martins</a>.</figcaption>
</figure>
<p>&quot;<a href="https://pt.wikiversity.org/wiki/Utilizador:Solstag/Ci%C3%AAncia_e_Software_Livre,_ou_L%C3%A1_e_de_Volta_Outra_Vez">Ciência e Software Livre, ou Lá e de Volta Outra Vez</a>&quot; it the title of the talk that <a href="https://pt.wikiversity.org/wiki/Utilizador:Solstag">Alexandre Hannud Abdo</a> gave at FISL.</p>
<div class="note">
<div class="admonition-title">
<p>Note</p>
</div>
<p>Alexandre's talk is available online
&lt;http://hemingway.softwarelivre.org/fisl15/high/41a/sala41a-high-201405091001.ogv&gt; (you need a browser or multimedia player compatible with <a href="http://en.wikipedia.org/wiki/Ogg">Ogg format</a>).</p>
</div>
<p>Alexandre is a great open science advocate and in his talk he gave a awesome overview of science across the centuries.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>academia,ciência aberta,Software Carpentry,Mozilla Science Lab,FISL</p>
</div>
<div class="tags">
<p>FISL15</p>
</div>
<div class="comments">

</div>
