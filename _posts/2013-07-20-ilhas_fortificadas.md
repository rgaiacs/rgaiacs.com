---
layout: post
title: "Ilhas fortificadas"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Um dos grande problemas da internet hoje são as ilhas fortificadas (ou chiqueirinhos) criados pelos grandes nomes da área como:</p>
<ul>
<li>Facebook,</li>
<li>Google,</li>
<li>Apple,</li>
<li>Microsoft,</li>
<li>Amazon,</li>
<li>Twitter, ...</li>
</ul>
<p>que deixam os usuários presos aos seus serviços. A seguir você encontra uma lista de exemplos pelas quais os nomes anteriores são ilhas fortificadas.</p>
<figure>
<img src="/images/ilhas_fortificadas.svg" class="align-center" style="width:80.0%" />
</figure>
<div class="note">
<div class="admonition-title">
<p>Note</p>
</div>
<p>As ilustrações da figura anterior foram retiradas do site <a href="http://openclipart.org/">OpenClipArt</a> e encontram-se em domínio público.</p>
</div>
<div class="more">

</div>
<h1 id="facebook">Facebook</h1>
<ol>
<li>A busca só é eficiente para usuários.</li>
<li>Só é possível enviar email para <code>xxx@facebook.com</code> por meio de um endereço vinculado a alguma conta.</li>
<li>Só é possível utilizar a API se for usuário com uma conta de desenvolvedor.</li>
</ol>
<h1 id="google">Google</h1>
<ol>
<li>O serviço Google Plus possui os mesmos problemas do Facebook.</li>
<li>Abandono de compatibilidade com XMPP.</li>
</ol>
<h1 id="apple">Apple</h1>
<ol>
<li>Seus serviços só podem ser utilizados por hardwares produzidos pela mesma.</li>
</ol>
<h1 id="microsoft">Microsoft</h1>
<ol>
<li>Em alguns serviços utiliza tecnologia desenvolvida por ela, utilizada apenas por ela e que só funciona em seu sistema operacional, e.g., Silverlight.</li>
<li>Uso de práticas anti-competitivas, e.g., fraco suporte a formatos abertos.</li>
</ol>
<h1 id="amazon">Amazon</h1>
<ol>
<li>Uso de formato próprio e falta de suporte a formatos abertos, e.g., Kindle.</li>
</ol>
<h1 id="twitter">Twitter</h1>
<ol>
<li>Abandono de API pública.</li>
</ol>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>none</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
