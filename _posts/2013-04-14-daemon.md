---
layout: post
title: "Daemon"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p><a href="http://en.wikipedia.org/wiki/Daemon_(technothriller_series)">Daemon</a>, de <a href="http://en.wikipedia.org/wiki/Daniel_Suarez">Daniel Suarez</a>, é o último livro que li (agora estou lendo sua continação, Freedom).</p>
<p>A sinopse do livro é:</p>
<blockquote>
<p>Matthew Sobol era um gênio da indústria de games, um bilionário criador de uma dezena de jogos online que viraram febre. Sua morte prematura deixou milhares de jogadores deprimidos em todo o mundo. Mas os fãs de Sobol não foram os únicos que notaram a morte dele: assim que o obituário dele é lido na internet, um daemon que estava dormente é ativado, iniciando uma corrente de eventos em um mundo hipereficiente e interconectado planejado por Sobol antes de sua morte.</p>
<p>Com os segredos do gênio enterrados junto com ele e com novas fases do plano sendo reveladas, implantadas e protegidas por seu Daemon, surgirá uma resistência disposta a decifrar os intrincados planos e livrar o mundo da ameaça do inimigo sem rosto. Se a resistência falhar, todos terão de aprender a viver em uma sociedade na qual o ser humano não está mais no controle. (Retirado da <a href="http://www.editoraplaneta.com.br/descripcion_libro/7409">Editora Planeta Brasil</a>.)</p>
</blockquote>
<div class="warning">
<div class="admonition-title">
<p>Warning</p>
</div>
<p>O texto a seguir possui spoiler. Esteja avisado.</p>
</div>
<div class="more">

</div>
<p>A história começa com três mortes, a de Matthew Sobol e de dois de seus funcionários. A morte dos dois funcionários é investigada pelo Detetive Peter Sebeck que logo no início da investigação junta-se com Jon Ross, um especialista em informática.</p>
<p>Peter e Ross acabam descobrindo o daemon criado por Sobol e o caso passa para uma força tarefa liderada pela Agente Natalie Philips e outras pessoas.</p>
<p>O daemon começa a criar uma rede de serviçais por meio de um dos jogos on-line de Sobol. Essa rede é altamente distribuída de modo que a maioria dos serviçais só possui conhecimentos de outros dois ou três e é por meio desta rede que o daemon atua no mundo real.</p>
<p>No fim do livro, a humanidade está dividida em três grupos: os serviçais do daemon, aqueles quer querem destruí-lo e aqueles que querem controlá-lo.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>livros</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
