---
layout: post
title: Drag and drop
author: "Raniere Silva"
categories: ftb
tags: [ftb]
image:
  feature: ftb.jpg
---
Este é um exemplo de como implementar a funcionalidade de arrastar e
soltar utilizando HTML, CSS e Javascript e foi fortemente baseada [nesse
tutorial](http://www.html5rocks.com/en/tutorials/dnd/basics/).

O HTML e CSS
============

Antes de mais nada, utilizaremos esse html &lt;dnd-0.html&gt; e
esse css &lt;dnd.css&gt;.

No HTML temos `draggable="true"` para que o web browser saiba que o
objeto pode ser movido.

No CSS temos `cursor:move;` apenas para indicar que o elemento poderá
ser movido (neste momento ele ainda não é).

Iniciando o Drag
================

Para iniciar o drag precisa-se adicionar o evento `dragstart` aos
elementos. Para isso utilizamos esse javascript &lt;dnd-1.js&gt; e esse
html &lt;dnd-1.html&gt;.

Completanto o Drag (Drop)
=========================

Para realizar o drop é preciso interromper o comportamento padrão do web
browser utilizando `preventDefault()` e `stopPropagation()`.

Para isso utilizamos esse javascript &lt;dnd-2.js&gt; e esse
html &lt;dnd-2.html&gt;.

Transferência de Dados
======================

A propriedade `dataTransfer` é a responsável por toda a mágica. Ela
precisa ser atribuída no `dragstart` e processada no `drop`.

Para isso utilizamos esse javascript &lt;dnd-3.js&gt; e esse
html &lt;dnd-3.html&gt;.

Movimentação
============

A visualização da movimentação pode ser configurada com os eventos
`dragenter` e `dragleave`.
