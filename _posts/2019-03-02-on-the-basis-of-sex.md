---
layout: post
title: "On the basis of sex"
author: "Raniere Silva"
categories: blog
tags: []
image:
  feature: 2019-03-02-on-the-basis-of-sex.jpg
  author: Michael Coghlan
  licence: CC-BY-SA
  original_url: https://flic.kr/p/pBJVHp
---

Yesterday I watched "[On the Basis of Sex](https://en.wikipedia.org/wiki/On_the_Basis_of_Sex)",
a film based on the life of Ruth Bader Ginsburg.
The plot of the film is around sex or gender, as you prefer, discrimination.
The story is very inspiring and interesting from the history point of view
but the film doesn't bring up any strong emotions.

"On the Basis of Sex" reminded me of Steven Spielberg's [Amistad](https://en.wikipedia.org/wiki/Amistad_(film))
that is a must watch for everyone.