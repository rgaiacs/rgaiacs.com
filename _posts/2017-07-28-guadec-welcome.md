---
layout: post
title: "GUADEC 2017: Welcome"
author: "Raniere Silva"
categories: conference
tags: [jekyll]
image:
  feature: 2017-07-28-guadec-welcome.jpg
  author: Naotake Murayama
  original_url: https://flic.kr/p/bSLVuz
  licence: CC-BY
---

And today is the first day of [GUADEC 2017](https://2017.guadec.org/).

Yesterday, the organisers hosted a great [welcome drink](https://twitter.com/guadec/status/890604404746838016)
full of amazing people.

I will soon leaving for the
[venue](https://twitter.com/guadec/status/890673145786576896)
and will not miss

- The GNOME Way by Allan Day,
- Newcomer Genesis Evolution by Carlos Soriano and Bastian Ilsø,
- The Battle Over Our Technology by Karen Sandler and
- Different ways of outreaching newcomers by Julita Inca.

Follow [guadec](https://twitter.com/search?f=tweets&vertical=default&q=guadec)
on Twitter.
