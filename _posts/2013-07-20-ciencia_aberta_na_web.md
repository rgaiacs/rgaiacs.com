---
layout: post
title: "Ciência Aberta na Web"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<div class="note">
<div class="admonition-title">
<p>Note</p>
</div>
<p>Este post foi inspirado e baseado em <a href="http://software-carpentry.org/blog/2013/07/sloan-proposal-round-2.html">Sloan Foundation Proposal Round 2</a> e Greg Wilson que foi originalmente publicado em 05/07/2013 no <a href="http://software-carpentry.org/blog/">blog da Software Carpentry</a> sob a licença CC-BY.</p>
</div>
<p>Para aqueles que não sabem, a Mozilla não é apenas a desenvolvedora do navegador <a href="http://www.mozilla.org/en-US/firefox/new/">Firefox</a> mas uma grande fomentadora de inovação na web por meio de vários projetos livres (ou abertos) que lidera ou participa como, por exemplo:</p>
<ul>
<li><a href="https://mozillalabs.com/en-US/persona/">Persona</a>: um sistema de identificação/autenticação descentralizado,</li>
<li><a href="https://mozillalabs.com/en-US/Popcorn/">Popcorn</a>: uma biblioteca para manipulação de páginas webs baseadas na reprodução de audio e vídeo,</li>
<li><a href="https://mozillalabs.com/en-US/bigbluebutton/">Big Blue Button</a>: uma plataforma livre para video conferência voltada para o ensino,</li>
<li><a href="https://webmaker.org/">WebMaker</a>: um grupo empenhado em serem facilitadores para que novas pessoas aprendam não só utilizar a web mas também criá-la.</li>
</ul>
<p>Há mais de um mês atrás ela oficialmente lançou o <a href="https://wiki.mozilla.org/ScienceLab">Mozilla Science Lab</a> (ver o anúncio em<a href="#fn1" class="footnote-ref" id="fnref1"><sup>1</sup></a>,<a href="#fn2" class="footnote-ref" id="fnref2"><sup>2</sup></a> e<a href="#fn3" class="footnote-ref" id="fnref3"><sup>3</sup></a>) que será dirigido por Kaitlin Thaney e terá como líder de projeto Greg Wilson <a href="#fn4" class="footnote-ref" id="fnref4"><sup>4</sup></a>. A seguir espero explicar qual o objetivo da Mozilla e seus parceiros nesse novo projeto.</p>
<div class="more">

</div>
<p>&quot;A internet foi criada por pesquisadores para acelerar a ciência. Mas aproximadamente 30 anos depois, apenas uma minoria utiliza a internet para algo além da publicação e distribuição. A internet tem alterado fundamentalmente a natureza do comércio, educação, cultura e sociedade civil por meio da comunicação aberta, acesso a informação e inovação em escala global. Mas com poucas exceções, o trabalho científico ainda é feito individualmente e offline.&quot; (Tradução do autor de trecho em<a href="#fn5" class="footnote-ref" id="fnref5"><sup>5</sup></a>.)</p>
<p>Embora existam várias iniciativas para auxiliar pesquisadores a utilizar a web e outras ferramentas computacionais (controle de versão e scripts, por exemplo) de forma a serem mais eficiente em suas pesquisas, como é o caso da <a href="http://software-carpentry.org">Software Carpentry</a>, esses pesquisadores continuam trabalhando individualmente e offline. Por esse motivo, &quot;a internet continua uma plataforma para debate e divulgação de resultados, mas não uma ferramenta para conduzir, fortalecer e expandir o trabalhos desses pesquisadores.&quot; (Tradução do autor de trecho em<a href="#fn6" class="footnote-ref" id="fnref6"><sup>6</sup></a>.)</p>
<p>Foi identificado quatro fatores que bloqueiam cientistas de utilizarem a internet para mais que debate e divulgação. São elas:</p>
<ol type="1">
<li>Habilidade: parte da comunidade científica não é nativa digital nem possuem domínio sobre linguagens de marcação e protocolos presentes na internet.</li>
<li>Tecnologia: embora tecnologia para a colaboração já exista (agregadores de notícias, wikis, pads, sistemas de controle de versão, ...) a maioria requer muito trabalho antes que possa ser utilizada o qual encontra-se fora do alcance dos pesquisadores.</li>
<li>Prática: os valores de trabalhar de forma aberta e em comunidade conflitam com as normas estabelecidas na academia em torno de originalidade, publicação e atribuição.</li>
<li>Escala: a falta de modelos e facilitadores, além de conflito entre os poucos existentes inibe o crescimento da ciência aberta.</li>
</ol>
<p>Os fatores 1 e 2 estão sendo resolvidos a medida que mais pesquisadores aderem a cultura livre e auxiliam seus pares nessa mudança. Os fatores 3 e 4 pretendem ser resolvidos pelo novo projeto da Mozilla ao convidar cientistas respeitados e já engajados com a ciência aberta para juntos guiarem o desenvolvimento de ferramentas, práticas e comunidades em torno de acelerar a ciência utilizando a internet.</p>
<p>Todos são convidados a participarem do Mozilla Science Lab. Atualizações sobre o projeto serão veiculadas no <a href="http://kaythaney.com/">blog da Kaitlin</a>.</p>
<p><strong>Referências</strong></p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>none</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
<section class="footnotes">
<hr />
<ol>
<li id="fn1"><p><a href="https://blog.mozilla.org/blog/2013/06/14/5992/" class="uri">https://blog.mozilla.org/blog/2013/06/14/5992/</a><a href="#fnref1" class="footnote-back">↩</a></p></li>
<li id="fn2"><p><a href="http://kaythaney.com/2013/06/14/announcing-the-mozilla-science-lab/" class="uri">http://kaythaney.com/2013/06/14/announcing-the-mozilla-science-lab/</a><a href="#fnref2" class="footnote-back">↩</a></p></li>
<li id="fn3"><p><a href="http://software-carpentry.org/blog/2013/06/mozilla-science-lab-announcement.html" class="uri">http://software-carpentry.org/blog/2013/06/mozilla-science-lab-announcement.html</a><a href="#fnref3" class="footnote-back">↩</a></p></li>
<li id="fn4"><p><a href="https://wiki.mozilla.org/ScienceLab" class="uri">https://wiki.mozilla.org/ScienceLab</a><a href="#fnref4" class="footnote-back">↩</a></p></li>
<li id="fn5"><p><a href="http://software-carpentry.org/blog/2013/07/sloan-proposal-round-2.html" class="uri">http://software-carpentry.org/blog/2013/07/sloan-proposal-round-2.html</a><a href="#fnref5" class="footnote-back">↩</a></p></li>
<li id="fn6"><p><a href="http://software-carpentry.org/blog/2013/07/sloan-proposal-round-2.html" class="uri">http://software-carpentry.org/blog/2013/07/sloan-proposal-round-2.html</a><a href="#fnref6" class="footnote-back">↩</a></p></li>
</ol>
</section>
