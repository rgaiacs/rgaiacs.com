---
layout: post
title: "Mathml March Meeting"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>This is a report about the Mozilla March IRC Meeting (see the announcement <a href="https://groups.google.com/d/msg/mozilla.dev.tech.mathml/ckVF5mf_MbA/XcGbBMlkzTsJ">here</a>). The topics of the meeting can be found <a href="https://etherpad.mozilla.org/mathml-2015-03">in this PAD</a> (<span data-role="download">local copy of the
PAD &lt;pad.txt&gt;</span>) and the <a href="http://logs.glob.uno/?c=mozilla%23mathml&amp;s=14+Jan+2015&amp;e=14+Jan+2015">IRC log</a> (<span data-role="download">local copy of the IRC log &lt;irc.txt&gt;</span>) is also available.</p>
<p>The next meeting will be in April 8th at 8pm UTC (<a href="http://www.timeanddate.com/worldclock/meetingdetails.html?year=2015&amp;month=4&amp;day=8&amp;hour=20&amp;min=0&amp;sec=0&amp;p1=240&amp;p2=137&amp;p3=179&amp;p4=233&amp;p5=195&amp;p6=37">check the time at your location here</a>). Please add topics in the <a href="https://etherpad.mozilla.org/mathml-2015-04">PAD</a>.</p>
<div class="more">

</div>
<h1 id="accessibility">Accessibility</h1>
<blockquote>
<p>&quot;Web accessibility means that people can use the web.&quot; <a href="http://alistapart.com/author/agibson">Anne Gibson</a> at <a href="http://alistapart.com/article/reframing-accessibility-for-the-web">Reframing Accessibility for the Web</a>.</p>
</blockquote>
<div class="admonition">
<p>Note</p>
<p>Thanks to <a href="https://medium.com/@gradualclearing">Sarah Horton</a> to write <a href="https://medium.com/@gradualclearing/math-is-hard-people-with-disabilities-matter-a393812d2927">Math is hard. People with disabilities matter</a> where I first read about Anne Gibson text.</p>
</div>
<p>This month happened the <a href="http://www.csun.edu/cod/conference/2015/sessions/index.php/public/website_pages/view/1">30th Annual International Technology and Persons with Disabilities Conference</a> that had a few talks about math and MathML, see a selection at the end of this post.</p>
<p>During our meeting, Frédéric Wang talk a lot about accessibility and <a href="http://www.nvaccess.org/">NVDA</a>. If you want to know more, a nice start point is <a href="http://metrc.uoregon.edu/">Mathematics eText Research Center</a> (MeTRC).</p>
<h1 id="rendering-example-collection">Rendering Example Collection</h1>
<p>When developing and testing one platform that will use MathML, e.g. Wikipédia, is difficult to know how MathML will look on different environments, e.g. Firefox with fonts, Firefox without fonts, Chrome with fonts, Chrome without fonts, ...</p>
<p>We could start extending the <a href="https://developer.mozilla.org/en-US/docs/Mozilla/MathML_Project/MathML_Torture_Test">Torture Test</a> by having &quot;As rendered by &lt;Web Browser&gt; with &lt;Font&gt;&quot;. Until we have this you can use <a href="http://browsershots.org/" class="uri">http://browsershots.org/</a> and request to see screenshots of Torture Test.</p>
<h1 id="gecko">Gecko</h1>
<p>Frédéric worked in many bugs at Gecko.</p>
<h1 id="how-to-help">How to Help</h1>
<ol type="1">
<li>Setup a collection of HTML pages with MathML to be screenshoted. You can start with the examples linked at <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=897065" class="uri">https://bugzilla.mozilla.org/show_bug.cgi?id=897065</a>.</li>
</ol>
<h1 id="upcoming-events">Upcoming Events</h1>
<table>
<thead>
<tr class="header">
<th>Date</th>
<th>Name</th>
<th>Web Site</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>2015/07/13-17</td>
<td>Conference on Intelligent Computer Mathematics (CICM)</td>
<td><a href="http://www.cicm-conference.org/2015/" class="uri">http://www.cicm-conference.org/2015/</a></td>
</tr>
</tbody>
</table>
<h1 id="appendix-interested-csun-talks">Appendix: Interested CSUN talks</h1>
<ul>
<li><a href="http://www.csun.edu/cod/conference/2015/sessions/index.php/public/presentations/view/186">Enabling Math: Web, Word &amp; PDF, Emerging Solutions &amp; Overcoming Issues</a></li>
<li><a href="http://www.csun.edu/cod/conference/2015/sessions/index.php/public/presentations/view/363">Math ML Support in JAWS 16</a></li>
<li><a href="http://www.csun.edu/cod/conference/2015/sessions/index.php/public/presentations/view/226">Latest Developments in Accessible Math in Browsers</a></li>
<li><a href="http://www.csun.edu/cod/conference/2015/sessions/index.php/public/presentations/view/10">Development of a Math-Learning App for Students with Visual Impairments</a></li>
<li><a href="http://www.csun.edu/cod/conference/2015/sessions/index.php/public/presentations/view/42">Intuitive New Tools that Make Mainstream Math and Graphics Accessible</a></li>
<li><a href="http://www.csun.edu/cod/conference/2015/sessions/index.php/public/presentations/view/176">Creating the Most Accessible Book in the World</a></li>
<li><a href="http://www.csun.edu/cod/conference/2015/sessions/index.php/public/presentations/view/198">Rubber Hits Road: Test Results for Accessible Browsers and Authoring Tools</a></li>
</ul>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>MathML,Mozilla</p>
</div>
<div class="tags">
<p>MathML meetings</p>
</div>
<div class="comments">

</div>
