---
layout: post
title: "CONFOA 2013"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Queria ter escrito mais sobre o CONFOA antes mas só agora venho fazê-lo.</p>
<figure>
<img src="/images/confoa.jpeg" class="align-center" style="width:40.0%" />
</figure>
<div class="note">
<div class="admonition-title">
<p>Note</p>
</div>
<p>Esta é uma cópia de um post meu no <a href="http://www.cienciaaberta.net" class="uri">http://www.cienciaaberta.net</a>.</p>
</div>
<div class="more">

</div>
<h1 id="english-version">English version</h1>
<p>In 6th and 7th of October happen the <a href="http://www.acessoaberto.pt/c/index.php/confoa2013/2013">4th CONFOA</a> (Portuguese-Brazilian conference about Open Access) from it I would like to rise three topics to debate.</p>
<p>The conference have lots of presentations about open access, open data, institutional repository, copyright, ..., but I only can remember TWO presentations that the slides have a copyleft license:</p>
<ul>
<li>Disponibilidad en acesso aberto de la produccion científica de los países de África lusófona. Martins Fernando Cuambe and Gema Bueno de la Fuente.</li>
<li>Cross-Ref, DOI (Digital Object Identifier) e serviços: Estudo comparativo luso-brasileiro. Edilson Damasio.</li>
</ul>
<p><strong>So presentations are one type of content that should be open?</strong></p>
<p>From the <a href="http://www.budapestopenaccessinitiative.org/boai-10-recommendations">Budapest recommendations</a> we have the following definition of open access (boldface from the author of this post):</p>
<blockquote>
<p>By &quot;open access&quot; to [peer-reviewed research literature], we mean its free availability on the public internet, &lt;strong&gt;permitting any users to read, download, copy, distribute, print, search, or link to the full texts of these articles, crawl them for indexing, pass them as data to software, or use them for any other lawful purpose, without financial, legal, or technical barrier other than those inseparable from gaining access to the internet itself.&lt;/strong&gt; The only constraint on reproduction and distribution, and the only role for copyright in this domains, should be to give authors control over the integrity of their work and the right to be properly acknowledged and cited.</p>
</blockquote>
<p>In Brazil, most of the called &quot;open&quot; documents ONLY permit the reading. In my humble opinion, <strong>we must start calling this type of document &quot;free beer&quot; access</strong>.</p>
<p>Here in Brazil, like to use the Creative Commons licenses with the noncommercial clause. I asked Marcos Alves de Souza from brazilian's 'Ministério da Cultura&quot; for a definition of noncommercial use and he say that it didn't exist. So in my humble opinion, <em>once we can't make clear what is noncommercial we should stop using the noncommercial clause of Creative Commons</em> because the use of this clause only slow the innovation as say many times in the conference.</p>
<h1 id="versão-em-português">Versão em português</h1>
<p>Nos dias 6 e 7 de Outubro ocorreu a <a href="http://www.acessoaberto.pt/c/index.php/confoa2013/2013">4ª CONFOA</a> (Conferência Luso-Brasileira sobre Acesso Aberto) a partir da qual eu gostaria de levantar alguns tópicos para debate.</p>
<p>Na conferência tivemos várias apresentações sobre acesso aberto, dados abertos, repositórios institucionais, direitos autorais, ..., mas eu consigo lembrar de apenas DUAS apresentações cujos slides estava marcados com uma licença aberta:</p>
<ul>
<li>Disponibilidad en acesso aberto de la produccion científica de los países de África lusófona. Martins Fernando Cuambe and Gema Bueno de la Fuente.</li>
<li>Cross-Ref, DOI (Digital Object Identifier) e serviços: Estudo comparativo luso-brasileiro. Edilson Damasio.</li>
</ul>
<p><strong>Então slides é um tipo de conteúdo que não deve ser aberto?</strong></p>
<p>Nas <a href="http://www.budapestopenaccessinitiative.org/boai-10-recommendations">recomendações de Budapest</a> encontramos a definição abaixo para acesso aberto (tradução e destaque pelo autor deste post):</p>
<blockquote>
<p>Por &quot;acesso aberto&quot; da literatura científica revisada por pares, nós entendemos que ela encontra-se livremente disponível ao público da internet, &lt;strong&gt;permitindo qualquer um lê-lo, baixá-lo, copiá-lo, distribuí-lo imprimi-lo, fazer busca nele, ou criar links para o texto completo desses artigos, utilizá-lo para produção de índices, passá-lo como informação/entrada para um programa de computador (software), ou utilizá-lo para qualquer outro propósito legal, sem barreiras financeiras, legais ou técnicas outras que não inseparáveis da obtenção do acesso da internet.&lt;/strong&gt; A única restrição para a reprodução e distribuição, e a única regra para o direito autoral neste domínio, de ser dar ao autor o controle sobre a integridade do seu trabalho e o direito de ser adequadamente reconhecido e citado.</p>
</blockquote>
<p>No Brasil, muitos chamam de documentos &quot;abertos&quot; aqueles que podemos apenas ler. Na minha opinião, <strong>devemos começar a chamar esse tipo de documento apenas de acesso grátis</strong> para evitar mal entendidos.</p>
<p>Também no Brasil, as pessoas gostam de utilizar as licenças Creative Commons com a condição de uso não comercial. Pedi ao senhor Marcos Alves de Souza do Ministério da Cultura por uma definição de uso comercial e ele disse que tal não existe. Desta forma e na minha opinião, <strong>uma vez que não conseguimos fazer uma diferença clara do que é uso comercial devemos parar de utilizar a condição de uso não comercial das licenças Creative Commons</strong> pois o uso dessa restrição apenas atrasa o processo de inovação como foi dito várias vezes na conferência.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>ciência aberta,educação</p>
</div>
<div class="tags">
<p>CONFOA</p>
</div>
<div class="comments">

</div>
