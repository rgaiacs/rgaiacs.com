---
layout: post
title: "LaTeX Course at Semat"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>In the new three days I will be teaching a little of LaTeX to some undergraduate students at <a href="http://www.ime.unicamp.br/~semat/programacao.html">2nd Semat (page in portuguese)</a>. Bellow I give more informations of how I have planned the course.</p>
<div class="more">

</div>
<h1 id="general">General</h1>
<p>In each day the class will start at 14:00 and end at 15:15. For each day I planned to split it in five rounds of 15 minutes. In each round I will teach some topic, give a example and let the students change the example.</p>
<p>For share the examples and some annotations we will be using <a href="https://pad.riseup.net/p/semat-latex">this pad</a> (<span data-role="download">backup of the pad before
the course start &lt;semat-latex.txt&gt;</span>) and for feedbacks <a href="https://pad.riseup.net/p/semat-feedback-latex">this other pad</a> (<span data-role="download">backup of the pad before
the course start &lt;semat-feedback-latex.txt&gt;</span>).</p>
<h1 id="st-day">1st Day</h1>
<p>The first day will be guide by the concept map below.</p>
<figure>
<img src="/images/latex1.jpeg" class="align-center" style="width:60.0%" />
</figure>
<dl>
<dt>Round 1.1</dt>
<dd><p>The students will be ask to draw a concept map of LaTeX that can be used for change the next class.</p>
</dd>
<dt>Round 1.2</dt>
<dd><p>Introduction about TeX and LaTeX with some history backgrounds.</p>
<p>We will use TeXworks as the LaTeX IDE and some explanations about the graphic user interface will be give with some examples of TeX and LaTeX code.</p>
</dd>
<dt>Round 1.3</dt>
<dd><p>Base on the previous example of LaTeX will be explained:</p>
<ul>
<li>preamble,</li>
<li>commands,</li>
<li>environments,</li>
<li>encoding.</li>
</ul>
</dd>
<dt>Round 1.4</dt>
<dd><p>Informations about paragraphs, line breaks and fonts will be give here.</p>
</dd>
<dt>Round 1.5</dt>
<dd><p>How to get help is very important when learning something and the below links will be inform:</p>
<ul>
<li><a href="http://en.wikibooks.org/wiki/LaTeX" class="uri">http://en.wikibooks.org/wiki/LaTeX</a>,</li>
<li><a href="http://tex.stackexchange.com/" class="uri">http://tex.stackexchange.com/</a>,</li>
<li><a href="http://latex-community.org/forum/" class="uri">http://latex-community.org/forum/</a></li>
<li><a href="http://texample.net/" class="uri">http://texample.net/</a></li>
<li><a href="http://texblog.net/" class="uri">http://texblog.net/</a></li>
<li><a href="http://www.golatex.de/index.html" class="uri">http://www.golatex.de/index.html</a></li>
<li>Newsgroup about (La)TeX: comp.text.tex (or at <a href="https://groups.google.com/forum/#!forum/comp.text.tex">Google Groups</a>)</li>
</ul>
</dd>
</dl>
<h1 id="nd-day">2nd Day</h1>
<p>The second day will be focus in (La)TeX math mode and guide by the concept map below.</p>
<figure>
<img src="/images/latex2.jpeg" class="align-center" style="width:60.0%" />
</figure>
<dl>
<dt>Round 2.1</dt>
<dd><p>Introduction to the in-line math mode and some operations.</p>
</dd>
<dt>Round 2.2</dt>
<dd><p>Introduction to more operations and symbols.</p>
</dd>
<dt>Round 2.3</dt>
<dd><p>Introduction to amsmath package and the display math mode.</p>
</dd>
<dt>Round 2.4</dt>
<dd><p>When using display math mode a nice feature of LaTeX is the cross reference tools that will be show here.</p>
</dd>
<dt>Round 2.5</dt>
<dd><p>This will be reserved as extra time.</p>
</dd>
</dl>
<h1 id="rd-day">3rd Day</h1>
<p>The third and last day will be guide by the concept map below.</p>
<figure>
<img src="/images/latex3.jpeg" class="align-center" style="width:60.0%" />
</figure>
<dl>
<dt>Round 3.1</dt>
<dd><p>How to insert a table.</p>
</dd>
<dt>Round 3.2</dt>
<dd><p>How to insert a picture.</p>
</dd>
<dt>Round 3.3</dt>
<dd><p>Introduction to TikZ.</p>
</dd>
<dt>Round 3.4</dt>
<dd><p>Introduction to TikZ node feature.</p>
</dd>
<dt>Round 3.5</dt>
<dd><p>The students will be ask to draw a second concept map of LaTeX that can be used for get what they learn.</p>
</dd>
</dl>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>educação,ensino,Software Carpentry</p>
</div>
<div class="tags">
<p>LaTeX</p>
</div>
<div class="comments">

</div>
