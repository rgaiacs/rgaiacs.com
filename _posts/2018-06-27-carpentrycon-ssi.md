---
layout: post
title: "CarpentryCon2018"
author: "Raniere Silva"
categories: blog
tags: [Software Sustainability Institute, CarpentryCon, CarpentryCon2018, The Carpentries, Software Carpentry, Data Carpentry, Dublin]
image:
  feature: 2018-06-27-carpentrycon.jpg
  author: Bérénice Batut
  original_url: https://flic.kr/p/27GsjLq
  licence: CC-BY
crosspost:
  name: Software Sustainability Institute
  url: https://www.software.ac.uk/blog/2018-06-27-carpentrycon-2018
---

By
[Raniere Silva](http://www.software.ac.uk/about/people/raniere-silva),
Software Sustainability Institute, [Aleksandra
Nenadic](http://www.software.ac.uk/about/people/aleksandra-nenadic),
Software Sustainability Institute, [Mario
Antonioletti](http://www.software.ac.uk/about/people/mario-antonioletti),
Software Sustainability Institute.

[Software Carpentry](https://software-carpentry.org/) \"restarted once
again in January 2012 with a new grant from the Sloan Foundation, and
backing from the Mozilla Foundation\"
\[[1](http://dx.doi.org/10.12688/f1000research.3-62.v1)\]. Soon after,
the Software Sustainability Institute launched the Software Carpentry
movement in the UK by organising and staffing the first workshops
together with Greg Wilson
\[[3](https://www.software.ac.uk/blog/2012-05-02-uks-first-software-carpentry-boot-camp),
[4](https://software.ac.uk/blog/2012-05-16-now-its-our-turn-software-carpentry-newcastle)\].
 Since then, the Institute has been The Carpentries' Platinum partner
and has provided help to [The
Carpentries](https://software.ac.uk/blog/2012-05-16-now-its-our-turn-software-carpentry-newcastle)
movement by funding a workshop administrator position to coordinate
workshops in the UK and Europe. The Institute\'s staff regularly teach
at workshops and have been actively promoting The Carpentries'
programmes to educational institutions around the UK. Throughout this
time, we have witnessed and supported the UK and worldwide expansion of
the Carpentries' community and the rise of a number of new Carpentries
([Data](http://www.datacarpentry.org/),
[Library](https://librarycarpentry.org/), HPC, Social Sciences, Digital
Humanities, etc.). In the UK alone, there are now 15 institutions that
have their own relationships with The Carpentries and are actively
running Carpentries programmes. After six years of working and
communicating with The Carpentries' staff and community via email and
various video conferencing tools at all sorts of early or late hours, we
finally met them face-to-face for the very first time at [CarpentryCon
2018](http://www.carpentrycon.org/). It was all very exciting!

It had been two years in planning before CarpentryCon 2018 finally took
place at the [University College Dublin (UCD)](http://www.ucd.ie/) from
the 30th May to 1st of June 2018. About 120 delegates from five
continents came together to meet for the first time and put faces to
already familiar names, share war stories, ideas and best practices,
learn new skills and discuss how to make the Carpentries even better.
This was the first CarpentryCon event which will hopefully become a
regular biennial meeting.

{% include figure.html filename="2018-06-27-carpentrycon-room.jpg" alternative_text="Photo of room" caption="Room with discussion during CarpentryCon" author="Bérénice Batut" original_url="https://flic.kr/p/252g8aQ" licence="CC-BY" %}

Five of
the Institute's members of staff attended and had a fantastic time at
CarpentryCon. Keynotes, skill-up sessions, breakout sessions and
workshops were perfectly aligned with each other to the point that the
event looked more like a cathedral than a bazaar
\[[2](https://en.wikipedia.org/wiki/The_Cathedral_and_the_Bazaar)\]. And
let's not forget the great food and the venue provided by the UCD hosts
at the O\'Brien Centre for Science. Opportunities for networking were
kicked off on the first day via a [Story Circle
Icebreaker](https://github.com/carpentries/carpentrycon/blob/master/Sessions/2018-05-30/00-Icebreaker-Story-Circles.md)
and were only limited by the 24 hours in a day. All attendees were
friendly and respectful as can be expected from a community that prides
itself on having and adhering to its Code of Conduct. We contributed by
doing lightning talks and posters, participating in various discussions
and holding skill-up sessions to help people get acquainted with the
tools and processes used by the Carpentries and help them better
integrate in the community.

## Building Carpentry Workshop Websites

{% include figure.html filename="2018-06-27-carpentrycon-aleksandra.jpg" alternative_text="Photo of Aleksandra Nenadic" caption="Aleksandra Nenadic" author="Mario Antoniletti" licence="CC-BY" %}

Aleksandra Nenadic (in collaboration with [Elizabeth
Wickes](https://ischool.illinois.edu/people/faculty/wickes1) from The
Carpentries' current Executive Council) ran a [session on building
Carpentry workshop
websites](https://docs.google.com/document/d/1iD4i31SUpfFDzQJGXJNf6T8zfpGOtZ56vpsM2kXgr9g/edit?usp=sharing)
on day 2
\[[5](https://docs.google.com/document/d/1iD4i31SUpfFDzQJGXJNf6T8zfpGOtZ56vpsM2kXgr9g/edit?usp=sharing)\]
for around 20 attendees. We kicked off by talking about workshop
identifiers - how to choose them and keep them consistent across various
Carpentry systems used to record or collect information related to
workshops (record keeping system, survey system, workshop repository,
etc.). Next we moved onto creating and customising workshop websites
starting from the [Carpentries' workshop website
template](https://github.com/carpentries/workshop-template), what common
errors are how to troubleshoot them when things go wrong. The session
overran by half an hour to make sure we covered everything and we felt
that people really appreciated the skill-up. Being able to do everything
from GitHub's web interface (and not having to set up and work from the
command line) definitely helped lower the barrier for people to start
doing it on their own. For someone who has made tens of such websites so
far, it made us realise another blind spot we suffer from -- never
assume people will know what to do or that something is obvious. And
despite all the written instructions it is so much more effective and
powerful to learn in a live-coding session alongside an instructor. Two
excellent points from the Carpentries' instructor training. We are
definitely planning on repeating this skill-up at the next suitable
opportunity.

## Lesson Development Carpentry-style

{% include figure.html filename="2018-06-27-carpentrycon-team.jpg" alternative_text="Photo of Software Sustainability Institute staff" caption="Aleksandra, Mario and Raniere" author="Raniere Silva" licence="CC-BY" %}

Another contribution to the CarpentryCon programme was related to the
Carpentries' lesson development process. On the first day, Raniere Silva
and [François Michonneau](https://twitter.com/fmic_) facilitated the
skill-up \"Contributing on GitHub\" that covered how to submit a pull
request to one of The Carpentries' lessons only using GitHub's web
interface and no command-line tools. The idea was to try and lower the
barrier to lesson contribution in order to get more people to
participate in lesson development as people sometimes feel intimidated
having to set up and work from a command line. Later on the first day,
François Michonneau led the breakout session \"Lesson Infrastructure:
how can we make it easier to develop and contribute to lessons?\" which
was well attended and generated a [long list of things that could be
improved](https://carpentries.org/blog/2018/06/lesson-infrastructure-report/).

On the third and last day, Raniere Silva hosted the \"Bring and Build
Your Own Lesson \'Carpentries-style\'\" workshop. This workshop was
again limited to only using the GitHub's web interface to edit the
lesson and demonstrated how useful continuous integration can be to find
why your lesson is not being built and where it is broken. This workshop
lead to [some
improvements](https://github.com/carpentries/styles/issues/287) to The
Carpentries lesson template.

## Conclusions

The Institute's collaboration with The Carpentries has been very
successful and we are looking forward to a continued collaboration in
the years to come. In this respect, we recommend attending the Software
Sustainability Institute's next Collaborations Workshop
in early April 2019.
Sign here to be the first to receive the official announcement.
See you there, friends.

## References

1.  Wilson, G. Software Carpentry: lessons learned \[version 1;
    referees: 3 approved\]. F1000Research 2014, 3:62 (doi:
    [10.12688/f1000research.3-62.v1](http://dx.doi.org/10.12688/f1000research.3-62.v1))

2.  Raymond, E. S. The Cathedral & the Bazaar: Musings on Linux and Open
    Source by an Accidental Revolutionary. O\'Reilly Media, Inc.\", 2001

3.  Jackson, M. The UK\'s first Software Carpentry boot camp. 2
    May 2012.
    <https://www.software.ac.uk/blog/2012-05-02-uks-first-software-carpentry-boot-camp>

4.  Jackson, M. \"Now it\'s our turn\" - Software Carpentry at
    Newcastle. 16 May 2012.
    <https://software.ac.uk/blog/2012-05-16-now-its-our-turn-software-carpentry-newcastle>

5.  Wickes, E., Nenadic, A. CarpentryCon 2018 Workshop Building Skill-Up
    Session, available at
    <https://docs.google.com/document/d/1iD4i31SUpfFDzQJGXJNf6T8zfpGOtZ56vpsM2kXgr9g/edit?usp=sharing>
