---
layout: post
title: "Vídeos do FISL14"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<div class="note">
<div class="admonition-title">
<p>Note</p>
</div>
<p>Desculpas pelo pico de posts (muito deles incompletos) que surgiram ontem. Estou tentando fazer um relato pessoal das palestras que assisti.</p>
</div>
<p>O FISL é o Fórum Internacional de Software Livre que ocorre todo ano em Porto Alegre no mês de julho. <a href="http://softwarelivre.org/fisl14?lang=pt">FISL14</a> é a 14ª edição do FISL que ocorreu na última semana.</p>
<p>Algumas das palestras do FISL14 foram gravadas, transmitidas ao vivo e agora encontram-se disponíveis para baixar na <a href="http://fisl.org.br/14/papers_ng/public/fast_grid?event_id=3">página da grade de palestras</a>. Se você não foi ao evento essa é uma ótima oportunidade para saber o que rolou lá e caso tenha ido de assitir aquela palestra que não consegui.</p>
<div class="note">
<div class="admonition-title">
<p>Note</p>
</div>
<p>Algumas das palestras do FISL13, edição do ano passado também encontram-se disponíveis para download na <a href="http://fisl.org.br/13/papers_ng/public/fast_grid?event_id=3">grade de programação do respectivo ano</a>.</p>
</div>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>FISL</p>
</div>
<div class="tags">
<p>FISL14</p>
</div>
<div class="comments">

</div>
