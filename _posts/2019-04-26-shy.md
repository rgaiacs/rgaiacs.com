---
layout: post
title: "Shy"
author: "Raniere Silva"
categories: book
tags: ["The Mastery of Love"]
image:
  feature: 2019-04-26-shy.jpg
  author: Alyssa Yung
  licence: Unsplash License
  original_url: https://unsplash.com/photos/-Gx7tuwO8_U
---

"Shy" is one of the first English wors that you will learn in a English language course.
[Cambridge dictionary](https://dictionary.cambridge.org/dictionary/english/shy) defines it as
"uncomfortable with other people and unwilling to talk to them".
On "The Mastery of Love",
Don Miguel Ruiz wrote

> But if you are not so lucky,
> the domestication can be so strong
> and the wounds so deep,
> that you can even be afraid to speak.
> The result is, "Oh, I am shy"
> Shyness is the fear of expressing yourself.
> You may believe you don't know how to dance
> or how to sing,
> but this is just repression of the normal human instinct to express love.

It is a interesting way to see things
and I agree with Miguel.