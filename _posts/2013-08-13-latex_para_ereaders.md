---
layout: post
title: LaTeX para ereaders
author: "Raniere Silva"
categories: ftb
tags: [ftb]
image:
  feature: ftb.jpg
---
Ereader são dispositivos eletrônicos produzidos exclusivamente para a
leitura que são cada vez mais comuns. Esses aparelhos utilizam o formato
EPUB como padrão e proporcionam uma ótima experiência de leitura.
Infelizmente nem toda obra encontra-se disponível no formato EPUB,
algumas vezes apenas como PDF que é um péssimo formato para os ereaders.
Nesse post irei apresentar algumas dicas de como gerar PDF otimizados
para ereaders utilizando o LaTeX.

Funcionalidades para leitura de PDF
===================================

<div class="admonition note">

Os exemplos apresentados aqui foram produzidos utilizando:

-   folktale.tex
-   folktale-ebook.tex
-   convert.sh

Para obtê-los, baixe os três arquivos anteriores e execute o shell
script :

    $ ./convert.sh

Para que shell script funcione adequadamente você irá precisar do LaTeX
e Imagemagick instalado.

</div>

A maioria dos ereaders possui pelo menos uma das seguintes
funcionalidades para tentar melhorar a leitura de PDFs. Para ilustração
dessas funcionalidades, considere as imagens abaixo.

![](/images/folktale-normal-4.jpg)

![](/images/folktale-normal-5.jpg)

*Trim* ou recorte

:   O ereader tenta recortar as margens bancas do PDF.

    **Desvantagem**: Nem sempre funciona adequadamente (algumas vezes
    corta pouco e outras demais) e a fonte flutua se páginas tiverem a
    caixa de texto de tamanhos diferentes.

    **Exemplo**: Kindle básico.

![](/images/folktale-trim-4.jpg)

![](/images/folktale-trim-5.jpg)

*Reflow* ou modo texto

:   O ereader considera apenas o texto disponibilizado no PDF para
    busca.

    **Desvantagem**: Nem sempre funciona (quando o PDF consiste apenas
    de imagens), perda da formatação, mistura do texto com cabeçalhos e
    rodapés, quebra do texto nas mudanças de página.

    **Exemplo**: Nook.

![](/images/folktale-text-4.jpg)

![](/images/folktale-text-5.jpg)

Zoom

:   O ereader permite zoom.

    **Desvantagem**: Taxa de atualização da imagem muito baixa e
    trabalho de adicionar e retirar o zoom constatemente.

    **Exemplo**: Kobo e Kindle.

![](/images/folktale-zoom-4.jpg)

![](/images/folktale-zoom-8.jpg)

Otimização
==========

A otimização do PDF para ereaders consiste básicamente em alterar o
tamanho da página como indicado a seguir:

    --- folktale.tex    2013-08-24 22:54:56.934589279 -0300
    +++ folktale-ebook.tex  2013-08-24 22:54:56.934589279 -0300
    @@ -1,4 +1,6 @@
     \documentclass[]{book}
    +\usepackage[papersize={160mm,200mm},margin=2mm]{geometry}
    +\sloppy
     \begin{document}
     \title{Japanese fairy tales}
     \author{Yei Theodora Ozaki}

A tela de 6' dos ereaders costuma medir, aproximadamente, 92mm por 124mm
de modo que deveria-se utilizar essas medidas para o tamanho do papel.
Ocorre que a caixa de texto costuma ficar muito pequena de forma que
recomendo utilizar valores próximos próximos de 1,5 as medidas da tela
do ereader (i.e. 138mm por 186mm). A escolha de 160mm por 200mm foi
empírica.

Os cabeçalhos e rodapés ocupam um espaço preciso da página e por isso é
bom removê-los utilizando `/sloppy`.

![](/images/folktale-ebook-normal-4.jpg)

![](/images/folktale-ebook-normal-5.jpg)

Como você pode observar, a leitura fica bem mais agradável quando
comparada com a versão não otimizada.

Outras otimizações
==================

No caso de equações, recomenda-se utilizar o pacote `breqn` que quebra
automaticamente equações em destaque.

Já no caso de figuras, recomenda-se utilizar o ambiente `figure` para
que ela possa ser movida para uma página adequada e que seja utilizado
um tamanho baseado no tamanho da caixa de texto, e.g.:

    /includegraphics[width=0.8/textwidth]{figura.jpg}

E para o caso de tabelas, recomenda-se utilizar o ambiente `table` e
evitar de construir tabelas muito grandes.
