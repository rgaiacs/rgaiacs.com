---
layout: post
title: Carregando ereader
author: "Raniere Silva"
categories: ftb
tags: [ftb]
image:
  feature: ftb.jpg
---
Tentar carregar um ereader pela porta USB de um computador rodando
GNU/Linux sem interromper a leitura não costuma ser uma coisas muito
fácil. A seguir você irá saber como ejetar o dispositivo adequadamente
(desmontar ou remover com segurança não permitem que você continue a
leitura sem interromper o carregamento).

Ao conectar o ereader na porta USB muito provavelmente o sistema
operacional irá montá-lo como um disco de armazenamento externo. Utilize
`fdisk` para saber qual o dispositivo que corresponde ao seu ereader (a
maneira mais fácil é pelo espaço em disco) como indicado abaixo:

    # fdisk -l
    ...

    Disk /dev/sdf: 1977 MB, 1977606144 bytes
    61 heads, 62 sectors/track, 1021 cylinders, total 3862512 sectors
    Units = sectors of 1 * 512 = 512 bytes
    Sector size (logical/physical): 512 bytes / 512 bytes
    I/O size (minimum/optimal): 512 bytes / 512 bytes
    Disk identifier: 0x00000000

        Device Boot      Start         End      Blocks   Id  System

    ...

No exemplo anterior o ereader corresponde ao dispositivo `/dev/sdf`
(muito provavelmente seu ereader ira corresponder a um dispositivo com
um nome da forma `/dev/sdX` mas não será `/dev/sdf`).que possui
`1977 MB` de espaço.

Uma vez identificado o dispositivo, precisamos ejetá-lo:

    # eject /dev/sdf

<div class="admonition note">

Para maiores informações sobre o comando `eject` utilize a "man page":

    $ man eject

</div>

No caso do Kindle, como ele só possue memória interna você só precisa
ejetá-lo. No caso do Kobo ou Nook que possuem cartão de memória, você
irá precisar ejetar ambos os dispositivos (o cartão de memória e o
ereader), i.e., irá precisar utilizar o comando `eject` duas vezes para
dispositivos diferentes.

**Referências**
