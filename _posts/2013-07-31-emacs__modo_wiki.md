---
layout: post
title: "GNU Emacs: Modo Wiki"
author: "Raniere Silva"
categories: ftb
tags: [ftb]
image:
  feature: ftb.jpg
---
Para quem não sabe, o [GNU Emacs](http://www.gnu.org/s/emacs) possui
alguns modos para edição de páginas em wikis que funcionam com o
[MediaWiki](http://www.mediawiki.org) (e.g., Wikipédia e irmãos). Neste
post será apresentado um passo-a-passo de como configurar o GNU Emacs
para editar a Wikipédia.

<div class="admonition warning">

[Existem vários grupos de usuários na
MediaWiki](https://pt.wikipedia.org/wiki/Wikip%C3%A9dia:Tipos_de_usu%C3%A1rios#Grupos_de_usu.C3.A1rios).
Se você for um usuário novo irá precisar resolver um CAPTCHA para poder
efetivar algumas edições. É recomendado que faça algumas edições
utilizando a interface web para ser promovido a usuário autoconfirmado
pois este não precisa resolver o CAPTCHA.

</div>

Instalação do Modo
==================

Estamos interessados em baixar a página de uma wiki, editá-la usando o
GNU Emacs e depois submeter a nova versão da página. Uma lista de alguns
dos modos que fazem essa tarefa encontra-se [nessa página da
EmacsWiki](http://www.emacswiki.org/emacs/WikiModeDiscussion). Neste
post iremos utilizar o [Media Wiki
Mode](http://www.emacswiki.org/emacs/MediaWikiMode).

Baixe o arquivo [mediawiki.el
daqui](http://www.emacswiki.org/emacs/mediawiki.el) e salve-o em
`~/.emacs.d/mediawiki/` (maiores informações sobre `~/.emacs.d`
[aqui](http://www.emacswiki.org/emacs/DotEmacsDotD)).

Posteriormente adicione o caminho `~/emacs.d/mediawiki` ao arquivo
`~/emacs` (maiores informações sobre o [o arquivo inicial
aqui](http://www.emacswiki.org/emacs/InitFile)). Você deve adicionar a
linha abaixo no `~/emacs`:

    (add-to-list 'load-path "~/.emacs.d/mediawiki/")

Maiores informações [sobre a linha anterior
aqui](http://www.emacswiki.org/emacs/LoadPath).

Para que o módulo seja carregado ao iniciar o GNU Emacs, adicione a
linha :

    (require 'mediawiki)

no final do arquivo `~/.emacs`.

<div class="admonition note">

Caso você não tenha adicionado `(require 'mediawiki)` no arquivo
`~/.emacs` será necessário carregar o script dentro do GNU Emacs
utilizando :

    M-x load-library RET mediawiki RET

</div>

Configuração
============

No GNU Emacs:

    M-x customize-variable RET mediawiki-site-alist RET

Adicione as informações pedidas para cada wiki que desejar editar (se
desejar pode deixar o campo de senha vazio e ele será requerido quando
for editar alguma página).

![](/images/emacs__modo_wiki-0.png){width="80%"}

Depois de inserir as informações selecione a opção para aplicar e salvar
as informações ou :

    C-x C-s 

Testando
========

<div class="admonition note">

Se você estiver utilizando o modo texto do GNU Emacs alguns atalhos
podem não funcionar uma vez que a combinação das teclas não existe no
terminal. Por exemplo, no xterm `TAB` corresponde a `C-i`. [Maiores
informações nessa resposta do
stackoverflow](http://stackoverflow.com/a/4345759/1802726).

</div>

Para informar a wiki que deseja editar :

    M-x mediawiki-site RET {nome do site} RET

![](/images/emacs__modo_wiki-1.png){width="80%"}

Para informar o artigo que deseja editar :

    M-x mediawiki-open RET {título do artigo} RET

![](/images/emacs__modo_wiki-2.png){width="80%"}

Depois de fazer as modificações desejadas utiliza-se `C-x C-s` para
submeter as modificações. Se desejar salvar as modificações e fechar o
buffer utilize `C-c C-c`.

Para acessar um artigo referenciado no artigo que está editando
utiliza-se `C-RET`.

Se houver a necessidade de recarregar a página utiliza-se `M-g`.

**Referências**
