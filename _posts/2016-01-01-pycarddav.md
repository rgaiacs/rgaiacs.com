---
layout: post
title: pyCardDAV
author: "Raniere Silva"
categories: ftb
tags: [ftb]
image:
  feature: ftb.jpg
---
[pyCardDAV](http://lostpackets.de/pycarddav/pages/usage.html) é uma
coleção de scripts em Python que podem ser utilizados pelo Mutt para
acessar contatos.

Instalação
==========

<div class="admonition">

Nota

Você precisa utilizar Python 2. (T\_T)

</div>

    $ sudo pip install pycarddav

Configuração
============

Edite o arquivo `~/.config/pycard/pycard.conf`. Caso o arquivo não
existe utilize
<https://raw.githubusercontent.com/geier/pycarddav/master/pycard.conf.sample>
como ponto de partida.

Syncronização
=============

Para sincronizar o banco de dados local com o servidor utilize :

    $ pycardsyncer
