---
layout: post
title: MDN Translation Review
author: "Raniere Silva"
categories: ftb
tags: [ftb]
image:
  feature: ftb.jpg
---
[Florian Scholz](https://mozillians.org/en-US/u/fscholz/) and [Jean-Yves
Perrier](https://mozillians.org/en-US/u/teoli/) had request to test and
give some feedbacks about the new features of [MDN (Mozilla Developer
Network](https://developer.mozilla.org/) related with the translation of
the pages.

In this post you will find my feedbacks.

Documentation Status Page
=========================

I really like the [Documantation
Status](https://developer.mozilla.org/en-US/docs/MDN/Doc_status) (e.g.
[MathML Documentation
Status](https://developer.mozilla.org/en-US/docs/MDN/Doc_status/MathML)).
It gives a great overview of what I can do to help build a better web.

![](/images/documentation00.png){width="25%"}

Start a translation
===================

![](/images/translation00.png){width="25%"}

To start the translation of a page, click on "LANGUAGES" -&gt; "Add a
translation".

![](/images/translation01.png){width="25%"}

You will go to a page with all the languages that MDN is being
translate. Select the language that you want to start the translation.

After select the language you will go to a page where you will find the
original text and the text editor for the translation side-by-side.

Translation
===========

![](/images/translation02.png){width="25%"}

The side-by-side view is great but I believe it can be improved adding a
white box right before the original text to match the CKEditor menu
(when translating a long paragraph can be a little hard to use the
side-by-side view).

![](/images/translation03.png){width="25%"}

I believe that the "Localization in Progress" flag at the top of the
documentation will be better (I missed it in my first translation).

Disable the Tags field and use the tags of the original document will be
great.

Maybe keep the "Save" button fixed at the top of the screen will be
great.

Solving "conflicts"
===================

When the original document is updated the translation became outdated.
Is very nice that MDN have a feature to inform the translator about it.

![](/images/translation04.png){width="25%"}

Find the changes in the already translated document can be a little hard
and some times the changes only update some URL that don't need to be
translated. I'm know that what I will suggest isn't easy to implement
(maybe is impossible or not due MDN store HTML) but a merge feature that
apply the changes of the original document into the translated one can
help sometimes, specially in the case of only update URL.

For this merge feature, the first time a translator goes to a outdated
document, before it get the current side-by-side page, it get a more
simple view with the changes in the original document and this changes
applied to the current translate document (replacing paragraphs and URL
outdated). The translator select which changes should be apply, save
this changes and goes to the side-by-side page to translate the outdated
paragraphs or fix minor changes.
