---
layout: post
title: "GSoC: Pencil Down (August 11 - August 17)"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>This is the last report about my GSoC project and cover the thirteenth week of “Students coding”.</p>
<p>At this last week I worked at the auto capitalization and deployed a land page for the project: <a href="http://r-gaia-cs.github.io/gsoc2014/" class="uri">http://r-gaia-cs.github.io/gsoc2014/</a>.</p>
<p>Bellow you will find more details about the past week and some thoughts about the project as a hole.</p>
<div class="more">

</div>
<h1 id="auto-capitalization">Auto Capitalization</h1>
<p>The default keyboard has auto capitalization. This was one small feature that I want to add to the project before it end and is done.</p>
<h1 id="land-page">Land Page</h1>
<p>Taking with my mentor he suggest that I wrote a land page for the project. I wrote it and add animations for each feature of the keyboard.</p>
<h1 id="whats-cool-about-this-summer">What’s cool about this summer</h1>
<ul>
<li>Working with bleed edge technology.</li>
<li>Learn more about Firefox OS.</li>
<li>Build my own Firefox OS (Gecko + Gaia) and flash it.</li>
<li>Collaborate with the core team of one app from Gaia.</li>
<li>Work with something that people use.</li>
<li>Have <a href="http://psbots.blogspot.com.br/2014/08/gsoc-update-layouts-completed-awaiting.html">another GSoC</a> project to &quot;compare&quot; with</li>
</ul>
<h1 id="whats-not-cool-about-this-summer">What’s not cool about this summer</h1>
<ul>
<li>Rebase my patch queue at the begin of every week makes my patch queue broke some times.</li>
<li>Get a little help from UX team would help a lot.</li>
<li>Rudy and Tim helped a lot but if some of their advices as made at the begin something probably will be different</li>
<li>The merge/release date of Firefox match the begin and end of GSoC.</li>
</ul>
<h1 id="acknowledgements">Acknowledgements</h1>
<p>What to highlight the help of my mentor, Salvador de la Puente González, Timothy Chien and Rudy Lu.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>GSOC,Mozilla,Mozilla Brasil</p>
</div>
<div class="tags">
<p>GSOC2014</p>
</div>
<div class="comments">

</div>
