---
layout: post
title: "Welcome to Forres"
author: "Raniere Silva"
categories: blog
tags: [Scotland, flowers, photos]
image:
  feature: 2018-07-30-forres.jpg
  author: Raniere Silva
  licence: CC-BY
---

Last weekend,
I visited Forres.

{% include figure.html filename="2018-07-30-forres-info.jpg" alternative_text="Photo of sign with information about Fores" caption="Information about Forres" author="Raniere Silva" licence="CC-BY" %}

Forres won many UK and Scottish awards due the beautiful gardens.
And I wanted the share photos of the one that I crossed.

{% include figure.html filename="2018-07-30-forres-0.jpg" alternative_text="Photo of Forres' garden" caption="Forres' garden" author="Raniere Silva" licence="CC-BY" %}

{% include figure.html filename="2018-07-30-forres-1.jpg" alternative_text="Photo of Forres' garden" caption="Forres' garden" author="Raniere Silva" licence="CC-BY" %}

{% include figure.html filename="2018-07-30-forres-2.jpg" alternative_text="Photo of Forres' garden" caption="Forres' garden" author="Raniere Silva" licence="CC-BY" %}

{% include figure.html filename="2018-07-30-forres-3.jpg" alternative_text="Photo of Forres' garden" caption="Forres' garden" author="Raniere Silva" licence="CC-BY" %}

{% include figure.html filename="2018-07-30-forres-4.jpg" alternative_text="Photo of Forres' garden" caption="Forres' garden" author="Raniere Silva" licence="CC-BY" %}