---
layout: post
title: "Being a minimalist"
author: "Raniere Silva"
categories: life
tags: ["Life Style", "Minimalist"]
image:
  feature: 2020-02-26-minimalist.jpg
  author: Tanalee Youngblood
  licence: Unsplash License
  original_url: https://unsplash.com/photos/fsdWYNTymjI
---

In 2019,
I read a few books
and
watched some YouTube videos
about minimalism.

This year,
I decided to try live as a minimalism.
I'm limiting my possessions in Ouro Preto, Brazil
but I still have some possessions in the UK
that I didn't have time to discart or move to Brazil.
