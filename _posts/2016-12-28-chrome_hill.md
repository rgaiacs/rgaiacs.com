---
layout: post
title: "Chrome Hill"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Chrome Hill is close to Hollinsclough.</p>
<p><img src="/images/chrome_hill01.jpg" alt="image" class="align-center" style="width:80.0%" /></p>
<div class="more">

</div>
<p><img src="/images/chrome_hill02.jpg" alt="image" class="align-center" style="width:80.0%" /></p>
<p><img src="/images/chrome_hill03.jpg" alt="image" class="align-center" style="width:80.0%" /></p>
<p><img src="/images/chrome_hill04.jpg" alt="image" class="align-center" style="width:80.0%" /></p>
<p><img src="/images/chrome_hill05.jpg" alt="image" class="align-center" style="width:80.0%" /></p>
<p><img src="/images/chrome_hill06.jpg" alt="image" class="align-center" style="width:80.0%" /></p>
<p><img src="/images/chrome_hill07.jpg" alt="image" class="align-center" style="width:80.0%" /></p>
<p><img src="/images/chrome_hill08.jpg" alt="image" class="align-center" style="width:80.0%" /></p>
<p><img src="/images/chrome_hill09.jpg" alt="image" class="align-center" style="width:80.0%" /></p>
<p><img src="/images/chrome_hill10.jpg" alt="image" class="align-center" style="width:80.0%" /></p>
<p><img src="/images/chrome_hill11.jpg" alt="image" class="align-center" style="width:80.0%" /></p>
<h1 id="panorama">Panorama</h1>
<p><img src="/images/chrome_hill_pano01.jpg" alt="image" class="align-center" style="width:80.0%" /></p>
<p><img src="/images/chrome_hill_pano02.jpg" alt="image" class="align-center" style="width:80.0%" /></p>
<p><img src="/images/chrome_hill_pano03.jpg" alt="image" class="align-center" style="width:80.0%" /></p>
<p><img src="/images/chrome_hill_pano04.jpg" alt="image" class="align-center" style="width:80.0%" /></p>
<p><img src="/images/chrome_hill_pano05.jpg" alt="image" class="align-center" style="width:80.0%" /></p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>hiking</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
