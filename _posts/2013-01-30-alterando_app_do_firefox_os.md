---
layout: post
title: Alterando app do Firefox OS
author: "Raniere Silva"
categories: ftb
tags: [ftb]
image:
  feature: ftb.jpg
---
Como informado no último post, o Firefox OS é baseado em HTML5, CSS3 e
Javascript e por esse motivo é fácil modificar os apps uma vez que

html

:   Especifica o conteúdo e posição deste no app.

css

:   Especifica o estilo a ser utilizado por cada pedaço de conteúdo
    especificado no html.

javascript

:   Responsável por tornar o app dinâmico.

Para alterar um app do Firefox OS devemos primeiro saber onde ele está
localizado. Supondo que esteja utilizando o simulador, você pode
utilizar o comando abaixo para descobrir o diretório em que
encontrara-se todos os apps:

    $ find .mozilla | grep profile/webapps$

Neste post vamos primeiro modificar o app calendar cuja tela inicial é
representada abaixo.

![](/images/ff_calendar_before.png)

Primeiro devemos fazer backup do app. Os apps encontram-se dentro de um
arquivo zip de maneira que o workflow a ser seguido, depois de extrair
os arquivos, para verificar alguma modificação é

1.  alterar os arquivos desejados,
2.  atualizar os arquivos no zip,
3.  reiniciar o simulador.

Para compor o app são utilizados várias imagens, podemos localizar essas
imagens utilizando o comando:

    $ find style | grep .png$

Alterando a imagem style/building\_blocks/headers/imagens/ui/header.png
por meio do GIMP[^1] a tela inicial passa a ser como representado
abaixo.

![](/images/ff_calendar_after02.png)

Desfazendo as alterações na imagem anterior e alterando a linha 30 de
style/building\_blocks.css para :

    color: red;

obtemos

![](/images/ff_calendar_after01.png)

Bom divertimento com suas próprias modificações.

[^1]: <http://www.gimp.org/>
