---
layout: post
title: "Jovem Hacker"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Na quarta-feira da última semana eu fui dar uma mão para o <a href="http://panaggio.net/">Panaggio</a> e a <a href="http://blog.melc.at">Mel</a> com as atividades do <a href="http://jovemhacker.org/">Jovem Hacker</a>.</p>
<figure>
<img src="/images/jh2.jpg" class="align-center" style="width:80.0%" />
</figure>
<div class="more">

</div>
<p>Panaggio e Mel continuam fazendo uma atividade maravilhosa voltada ao letramento digital no Brasil. Nesse dia, eles estavam ensinando a garotada (15-18 anos) à escrever suas páginas HTML do zero.</p>
<figure>
<img src="/images/jh1.jpg" class="align-center" style="width:80.0%" />
</figure>
<p>Achei a sala ruim pois mais de 60% dos alunos ficam de costas para a lousa/tela de projeção e os outros 40% estavam embaixo da lousa/tela de projeção. Talvez escreva um post sobre essa assunto em breve.</p>
<p>Em relação aos alunos, não achei eles completamente motivados para aprender. A equipe do Jovem Hacker discutiu esse ponto bastante e acredito que falhamos na missão. Alguns dos motivos para a falta de interesse dos alunos que consegui pensar até agora foram (1) o conteúdo apresentado não possui aplicação imediata e (2) o programa utilizado na atividade (Atom) não é chamativo.</p>
<p>Também em relação aos alunos, alguns deles se distraiam com o celular além de conversarem entre si sobre assuntos diversos constantemente.</p>
<p>Se eu fosse repetir essa atividade novamente, pediria que eles visualizassem a página que eles estão criando através do celular (e não do browser no computador) e ao invés de começar apresentando as tags de texto eu brincaria com propriedades de CSS para animar o fundo de forma que eles tivessem um &quot;papel de parede&quot; ou &quot;tela de descanso&quot;.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>educação,ensino,Mozilla Brasil,Webmaker</p>
</div>
<div class="tags">
<p>Jovem Hacker,Campinas</p>
</div>
<div class="comments">

</div>
