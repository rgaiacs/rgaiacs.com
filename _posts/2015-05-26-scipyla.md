---
layout: post
title: "Conferência SciPy Latino Americana 2015"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Na semana passada eu participei da <a href="http://scipyla.org/conf/2015/">Conferência SciPy Latino Americana 2015</a>.</p>
<p><img src="/images/scipy.png" alt="Logo da Conferência SciPy Latino Americana 2015" class="align-center" style="width:80.0%" /></p>
<div class="admonition">
<p>Aviso</p>
<p>Algumas atividades aconteceram no mesmo horário e não pude participar.</p>
</div>
<div class="more">

</div>
<h1 id="dia-01">Dia 01</h1>
<p>Infelizmente eu não participei do primeiro dia. Tinha algumas obrigações que só me permitiam me deslocar no primeiro dia da conferência.</p>
<div class="admonition">
<p>Nota</p>
<p>Antes da conferência tiveram alguns tutoriais introdutórios de Python que também não consegui participar.</p>
</div>
<h1 id="dia-02">Dia 02</h1>
<div class="admonition">
<p>Nota</p>
<p>Durante todo o dia foi realizado a <a href="http://conf.scipyla.org/activity/teen_track">Teen</a> <a href="http://argentinaenpython.com.ar/track-teen-scipy-la-2015/">Track</a> organizada pelo <a href="https://twitter.com/argenpython">Manuel</a> e pela <a href="https://twitter.com/EllaQuimica">Johanna</a>. Não assisti a Teen Track mas os comentários que circularam eram muito bons.</p>
</div>
<p>O segundo dia começou com um tutorial sobre <a href="https://pypi.python.org/pypi/simpleai">SimpleAI</a> por <a href="http://github.com/fisadev">Juan Pedro Fisanoti</a> que foi bom e consegui acompanhar os exemplos (depois de descobrir que não funcionava com Python3).</p>
<p><img src="/images/freelancer.jpg" alt="Foto da palestra sobre Freelanscience." class="align-center" style="width:80.0%" /></p>
<p>Depois do almoço tivemos as seguintes apresentações:</p>
<ul>
<li>&quot;Per Python ad astra&quot; por <a href="http://pybonacci.org/">Juan Luis Cano Rodríguez</a>, <strong>infelizmente</strong> eu perdi porque demoramos muito no almoço;</li>
<li>&quot;Information Extraction, herramientas y visuzlización, todo con IEPY&quot; por Javier Mansilla, que foi uma ótima demonstração do IEPY;</li>
<li>&quot;Freelanscience&quot; por <a href="https://www.linkedin.com/in/pablogabrielcelayes">Pablo Gabriel Celayes</a>, na qual foi apresentado ótimas dicas para iniciar uma carreira como desenvolvedor freelancer de Python (científico);</li>
<li>&quot;Machine Learning y problemas pequeños&quot; por <a href="http://github.com/fisadev">Juan Pedro Fisanoti</a>, na qual foi apresentados alguns problemas interessantes que podem ser resolvidos com machine learning.</li>
</ul>
<p>Depois do intervalo da tarde, eu falei sobre a <a href="http://software-carpentry.org/">Software Carpentry</a>.</p>
<div class="admonition">
<p>Slides</p>
<p>Você encontra os slides da minha apresentação <span data-role="download">aqui &lt;swc.zip&gt;</span>. Os slides estão em HTML, então você deve descomprimir o arquivo zip e abrir o arquivo <code>index.html</code> no seu navegador web favorito.</p>
</div>
<p>Depois da apresentação algumas pessoas vieram falar comigo manifestando interesse em participar da Software Carpentry. Fiquei bastante contente e nos próximos dias vou retomar a conversa com cada um.</p>
<p>Para encerrar as atividades do segundo dia foram apresentadas as &quot;Lightning talks&quot;. Para quem não está acostumado com essa prática, são apresentações de no máximo 5 minutos (algumas vezes de no máximo 1 minuto) que recebem inscrições durante o evento. Todas as &quot;Lightning talks&quot; foram muito boas mas gostaria de destacar</p>
<ul>
<li>a apresentação do Manuel sobre <a href="http://openstreetmap.org/">OpenStreetMap</a> no qual fez uma comparação com o Google Maps.</li>
</ul>
<p><img src="/images/openstreetmaps.png" alt="Screenshot do OpenStreeMap de Posadas." class="align-center" style="width:80.0%" /></p>
<ul>
<li><a href="https://github.com/kikocorreoso">Kiko Correoso</a> proferiu a melhor palestra sobre acesso aberto, tratando sobre a <strong>falta</strong> de lógica na forma como a comunicação é transmitida, que eu já assisti. Estou esperando a organização da conferência disponibilizar os vídeos para eu manter uma cópia pessoal dessa palestra.</li>
<li><p><a href="https://twitter.com/ccordoba12">Carlos Cordoba</a> falou sobre o <a href="http://conda.pydata.org/">Conda</a>, um gerenciador de pacotes <strong>open source</strong> escrito em Python.</p>
<p>Eu já tinha ouvido falar do Conda e tinham me recomendado utilizá-lo. Depois da apresentação do Carlos eu fiquei super animado para testar o Conda.</p></li>
</ul>
<h1 id="dia-02---jantar">Dia 02 - Jantar</h1>
<p>Na noite do segundo dia saímos para jantar em uma pizzaria. Durante a janta acertei os últimos detalhes do workshop de Git com o Fisanoti, conversei com o Manuel e a Johanna sobre o <a href="http://argentinaenpython.com.ar/track-teen-scipy-la-2015/">fabuloso projeto que eles desenvolvem na Argentina</a> e também conversei com o Carlos e o João Pimentel sobre Conda, Spyder e outras coisas técnicas.</p>
<p>Depois da janta fizemos uma caminhada na beira do rio e conversei com o <a href="https://github.com/xmnfw">Ivan Ogassavara</a> e o João Pimentel sobre acesso aberto, ciência aberta, ...</p>
<div class="admonition">
<p>Nota</p>
<p>Fiquei super contente quando o Ivan mencionou o <a href="https://github.com/ctb">C. Titus Brown</a> e o <a href="https://github.com/luizirber">Luiz Irber</a>.</p>
</div>
<h1 id="dia-03">Dia 03</h1>
<div class="admonition">
<p>Nota</p>
<p>Durante a parte da manhã, teve a palestra do Patricio Del Boca e do Najuel Defossé que não pude assistir.</p>
</div>
<p>O terceiro dia começou com o workshop de Git que ministrei em conjunto com o Fisanoti. O workshop começou com a sala cheia e terminamos com aproximadamente 10 pessoas. O número de participantes diminuiu porque depois do intervalo tinha outra atividade na sala ao lado.</p>
<p><img src="/images/git.jpg" alt="Foto do workshop de Git." class="align-center" style="width:80.0%" /></p>
<p>Tivemos que improvisar um pouco o workshop porque a internet estava lenta e acabamos separando a demonstração da prática na última parte do workshop. Felizmente, nosso improviso não alterou em nada o que tínhamos planejado apresentar.</p>
<p>Após o almoço, que também demorou mais que o esperado, tivemos as apresentações</p>
<ul>
<li>&quot;Procesamiente de Imágenes con Python y OpenCV para extracción del perfil de escaneo en listones de madera machihembrada&quot; por Alejandro Dario, que mostrou coisas bem legais relacionadas com scanner 3D;</li>
<li>&quot;<a href="https://github.com/gems-uff/noworkflow">noWorkflow</a>: capturing provenance from Python scripts&quot; por <a href="https://twitter.com/joaofelipenp">João Pimentel</a>, que apresentou uma ferramenta para provenance que é algo em alta quando se fala sobre ciência aberta;</li>
<li>&quot;<a href="http://bokeh.pydata.org/">Bokeh</a>: Complejas visualizaciones a la vuelta de la esquina&quot; por <a href="https://twitter.com/damian_avila">Damián Avila</a>, que apresentou uma ferramenta para criação de visualizações interativas em navegadores web;</li>
<li>&quot;<a href="https://github.com/spyder-ide/spyder/">Spyder</a>: An IDE designed for scientists&quot; por Carlos Cordoba, que apresentou as novas funcionalidades do Spyder --- uma ótima IDE para quem está pretendendo migrar do MATLAB para Python.</li>
</ul>
<p><img src="/images/bokeh.jpg" alt="Foto da palestra sobre Boken." class="align-center" style="width:80.0%" /></p>
<p>Todas as apresentações foram muito boas e só me fizeram adicionar mais coisas na minha TODO list.</p>
<h1 id="conclusões">Conclusões</h1>
<p>A equipe organizadora do evento está de parabéns pelo ótimo evento. Acredito que todos aprenderam muito, trocaram experiência, divertiram-se e fizeram novos amigos.</p>
<h1 id="conferência-scipy-latino-americana-2016">Conferência SciPy Latino Americana 2016</h1>
<p>Graças ao Ivan Ogassavara já temos a cidade sede para o evento do próximo ano: Florianópolis. Espero que o evento no próximo ano seja melhor que o desse ano e conte com a presença de mais brasileiros e também dos nossos amigos argentinos, uruguaios, paraguaios, ... e espanhóis.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>Python,SciPy</p>
</div>
<div class="tags">
<p>SciPyLA2015</p>
</div>
<div class="comments">

</div>
