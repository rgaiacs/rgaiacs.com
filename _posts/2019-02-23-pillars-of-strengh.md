---
layout: post
title: "Pillars of Strengh"
author: "Raniere Silva"
categories: blog
tags: []
image:
  feature: 2019-02-23-pillars-of-strengh.jpg
  author: John Mason
  licence: CC-BY
  original_url: https://flic.kr/p/r1Aeoo
---

A while ago I read a [blog post](https://ambervstheworldblog.wordpress.com/2018/05/20/compound-depression/) from [Amber](https://ambervstheworldblog.wordpress.com)
that was extremally insightful.
Amber wrote

> Spiralling into depression and feeling thoughts of suicide is not a good place to be. Personally, I am finding that a single problem is never the cause. What leads me there is when depression, sadness or loss affects more than one of my key aspects of life.
>
> Job – friends – family – sport – art – identity
>
> These are my main pillars of strength and bring me strength when I need it. (I added identity on the end because of my gender dysphoria, really it belongs in a category of its own as it sits more as an overarching sense of self that affects my life in general).
>
> What I have found is that when depression takes over too many aspects of my life, it leads to a spiral of bad thoughts, self-deprecation and thoughts of suicide. My performance at work slips and I struggle. I have a problem with friends and I struggle. I get pushed away from sport and I struggle.
>
> When there feels like there is no outlet it can be extremely difficult to see a way out of depression. This is when suicide leaks in and plagues thoughts. Not because it is an easy way out, far from it. Suicide starts to look like not just an idea, but it starts to look like the only option because all other bridges have been burnt.
>
> There is another dangerous thought that stops recovery. Let’s say I feel terrible about my job, my identity is clouded by dysphoria, I have a fall out with a friend and sports have pushed me away. In times like this it would sense to reach out to family, or perhaps other friends. Except my mind tells me that I would be bringing all of this burden and depression into their lives, so they are better off without me.
>
> Let me tell you from experience, this is bullshit. This is depression winning. It is depression talking. Don’t listen to it.
>
> What I have learnt is that compound depression is what hurts me. If I keep them separate and don’t think of them as a whole then they are manageable. Step back and separate.
>
> Tackle one at a time and seek help when needed.

I hope that Amber's word helps you as much as it did to me.