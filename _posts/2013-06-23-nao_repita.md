---
layout: post
title: "Não repita"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Em computação existe uma famosa expressão: &quot;Don't repeat yourself&quot; (em português seria algo como &quot;Não repita a si mesmo&quot;). Essa frase está associada como uma boa prática de programação de reduzir a repetição de código pois:</p>
<ul>
<li>economiza espaço (não que isso seja um problema hoje),</li>
<li>torna a leitura mais fácil,</li>
<li>torna a manutenção do código mais fácil,</li>
<li>evite problemas decorrentes da mudança em apenas uma das partes repetidas, ...</li>
</ul>
<p>Será que esse princípio é seguido em outros lugares?</p>
<div class="more">

</div>
<h1 id="internet">Internet</h1>
<p>Via de regra na internet ele não é seguido e descobrir isso não é muito difícil, basta uma simples busca utilizando seu buscador favorito para encontrar várias páginas que possuem conteúdo muito semelhante.</p>
<p>Ocorre que para a web isso não é algo ruim devido a sua descentralização. Essa repetição é o que garante que alguma informação não será perdida mesmo quando alguma página for removida.</p>
<p>Além disso, muitas vezes os usuários da internet repetem um conteúdo como forma de aprendizagem pessoal ou registro para consulta pessoal futura.</p>
<h1 id="redes-privadas">Redes privadas</h1>
<p>Algumas redes privadas possuem uma enorme quantidade de informação que só podem ser acessadas de dentro delas. Dentro de uma rede privada os usuários tentam não repetir informação mas essa acaba sendo repetida entre as redes privadas.</p>
<p>Semanas atrás tive uma conversa sobre isso referente ao uso de wikis para a área de educação que só são acessíveis para membros da instituição e o motivo para isso era competição entre as instituições. Para ilustrar, podemos citar dois cursinhos pré-vestibular onde cada um produz seu próprio conteúdo (que acabam sendo muito parecidos) e disponibilizam apenas para seus alunos. Na minha opinião, os esforços seriam melhor empregados se ambos os conteúdos fossem abertos.</p>
<p><strong>Referências</strong></p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>none</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
