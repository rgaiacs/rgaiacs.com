---
layout: post
title: Encriptando
author: "Raniere Silva"
categories: ftb
tags: [ftb]
image:
  feature: ftb.jpg
---
Embora a grande maioria dos usuários acreditem que o email seja uma
forma segura (e privativa) de comunicar-se, ele não é.

Um dos problemas do uso de email é a forma como a informação é
transmitida. Se for utilizado o protocolo HTTP o conteúdo do email é
visível por todos os intermediários entre o usuário e o provedor de
email. Para corrigir isso deve-se utilizar o protocolo HTTPS.

Um outro problema é a interface web fornecida pelos provedores de email
e utilizada pela grande maioria. O provedor de email sabe o conteúdo dos
emails que você escreveu e está escrevendo. A única forma de corrigir
isso é encriptar suas mensagens localmente e depois enviá-las por email.

O estado da arte de criptografia para email hoje é o modelo de chave
pública-privada e o programa mais utilizado é o GNU Privacy Guard (ou
GPG). A seguir mostro como encriptar uma mensagem a ser enviada.

O primeiro passo é adquirir a chave pública da pessoa para a qual deseja
enviar o email. Você pode pedir para ela enviar a chave pública ou
utilizar um dos servidores que hospedam chaves públicas como o [MIT PGP
Public Key Server](http://pgp.mit.edu/). Como um exemplo, minha chave
pública encontra-se
[aqui](http://pgp.mit.edu:11371/pks/lookup?op=vindex&search=0x7BCC72FA54A575CD).
Para os comandos a seguir irei utilizar essa chave &lt;sample.gpg&gt;.

Depois de baixar a chave pública, você deve informar o GPG dela:

    $ gpg --import sample.gpg
    gpg: key 9C596357: public key "Foo Bar <sample@ftb.rgaiacs.com>" imported
    gpg: Total number processed: 1
    gpg:               imported: 1  (RSA: 1)

onde `sample.gpg` corresponde a chave desejada.

Depois de informar o GPG da chave pública, podemos encriptar a mensagem
que desejamos (um arquivo qualquer, neste caso utilizaremos este arquivo
&lt;para\_encriptar.txt&gt;). :

    $ gpg -a -r "Foo Bar" -e para_encriptar.txt

onde `"Foo Bar"` é o nome do destinatário (pode ser um endereço de email
também) e `para_encriptar.txt` é o arquivo a ser encriptado. Pode
ocorrer do GPG pedir confirmação do comando, neste caso confirme
digitando `y` e depois `Enter`.

Será criado o arquivo `para_encriptar.txt.asc` que corresponde ao
conteúdo do arquivo `para_encriptar.txt` criptografado para que apenas o
usuário de nome `Foo Bar` seja capaz de ler seu conteúdo. Abaixo
encontra-se o conteúdo do arquivo `para_encriptar.txt.asc`.

Agora é só enviar o arquivo `para_encriptar.txt.asc` por email para o
usuário `"Foo Bar"`.
