---
layout: post
title: "EuroPython 2017"
author: "Raniere Silva"
categories: python 
tags: [jekyll]
image:
  feature: 2017-07-10-europython.jpg
  author: Siri B.L.
  original_url: https://flic.kr/p/7jhEbu
  licence: CC-BY
---

I'm at [EuroPython 2017](https://ep2017.europython.eu) this week.
If you are in Rimini,
we should meet.
