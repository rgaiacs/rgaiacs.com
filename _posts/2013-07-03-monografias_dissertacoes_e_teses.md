---
layout: post
title: "Monografias, Dissertações e Teses com Git e LaTeX"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Essa foi a primeira vez que apresento alguma coisa em um grande evento para um público que não conheço. Estava muito nervoso e por isso falei muito rápido cobrindo o pretendido antes do tempo esperado.</p>
<p>Eu falei sobre o modelo para dissertações e teses do IMECC que desenvolvi com alguns colegas. Os slides que utilizei encontram-se <span data-role="download">aqui
&lt;monografias_dissertacoes_e_teses.pdf&gt;</span> mas boa parte da apresentação foi mostrando o modelo, exemplos do LaTeX e um pouco do Git por meio da sua interface gráfica.</p>
<p>Futuramente vou adicionar parte das telas da interface gráfica demonstradas no FISL no modelo.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>FISL</p>
</div>
<div class="tags">
<p>FISL14</p>
</div>
<div class="comments">

</div>
