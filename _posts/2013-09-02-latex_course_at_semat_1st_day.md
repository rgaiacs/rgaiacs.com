---
layout: post
title: "LaTeX Course at 1st Day of Semat"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Today was the first day of my course about LaTeX that I talk yesterday (<span data-role="doc">see the post here &lt;../01/latex_course&gt;</span>). Around 20 students attend it and you will find more information about that below.</p>
<div class="more">

</div>
<p>I try to follow the plan with the pads (<span data-role="download">one for the contents
&lt;semat-latex.txt&gt;</span> and <span data-role="download">other for feedbacks
&lt;semat-feedback-latex.txt&gt;</span>) and the rounds:</p>
<dl>
<dt>Round 1.1</dt>
<dd><p>Ask students to draw a concept map of what they already know about LaTeX and what they want to know.</p>
</dd>
<dt>Round 1.2</dt>
<dd><p>Talk about the history of LaTeX and the IDE (<a href="https://en.wikipedia.org/wiki/Integrated_development_environment">integrated development environment</a>) that we are using, TeXworks.</p>
</dd>
<dt>Round 1.3</dt>
<dd><p>The name of the LaTeX related things as commands and environments.</p>
</dd>
<dt>Round 1.4</dt>
<dd><p>Information about paragraphs line breaks and fonts.</p>
</dd>
<dt>Round 1.5</dt>
<dd><p>Give some places where to look for help.</p>
</dd>
</dl>
<h1 id="concept-maps">Concept maps</h1>
<p>Looking for the concepts maps I deduce that most of the students never use LaTeX.</p>
<p>Since I told that they can write &quot;What they want to know?&quot; almost all have give me some tip for it.</p>
<p>After I &quot;read&quot; the concept maps I try to write all the questions in the pad.</p>
<p>Below you will find some of the concept maps they produce. Since it was in portuguese I make a digital version in english of it using <a href="http://dia-installer.de/">Dia</a>.</p>
<figure>
<img src="/images/latex0002.svg" class="align-center" style="width:60.0%" />
</figure>
<p><span data-role="download">The original of the concept map above &lt;latex0002.jpeg&gt;</span></p>
<figure>
<img src="/images/latex0003.svg" class="align-center" style="width:60.0%" />
</figure>
<p><span data-role="download">The original of the concept map above &lt;latex0003.jpeg&gt;</span></p>
<figure>
<img src="/images/latex0004.svg" class="align-center" style="width:60.0%" />
</figure>
<p><span data-role="download">The original of the concept map above &lt;latex0004.jpeg&gt;</span></p>
<figure>
<img src="/images/latex0005.svg" class="align-center" style="width:60.0%" />
</figure>
<p><span data-role="download">The original of the concept map above &lt;latex0005.jpeg&gt;</span></p>
<h1 id="the-pad">The pad</h1>
<p>I really expected that the student edit the pad. Only one students do it and after I ask him to add in it what he has done.</p>
<p>When adding the code examples in the pad I enclosing it with HTML tags like below:</p>
<pre><code>&lt;code&gt;
\documentclass{article}
\begin{document}
Exemplo de documento LaTeX.
\end{document}
&lt;/code&gt;</code></pre>
<p>Many students copy and paste the HTML and it cause troubles for the LaTeX engine.</p>
<p>And when talking about the <code>texdoc</code> command (that as far as I know can be used only from the command line) I must have give clear instructions for a GNU/Linux and Windows machine. Most students don't know that the dollar sign, <code>$</code>, in the Unix-like shell and the greater than symbol, <code>&gt;</code>, in DOS command line must not be input and is just a visual sign.</p>
<h1 id="the-video-projector">The video projector</h1>
<p>Most of the time the screen in the video projector was showing only the pad.</p>
<figure>
<img src="/images/screen0.jpg" class="align-center" style="width:60.0%" />
</figure>
<p>And just when I'm editing the example it show the IDE (I copy and past the example in the pad).</p>
<figure>
<img src="/images/screen1.jpg" class="align-center" style="width:60.0%" />
</figure>
<p>Now I think that will be better if I keep the screen in the video projector only showing the IDE as below.</p>
<figure>
<img src="/images/screen2.jpg" class="align-center" style="width:60.0%" />
</figure>
<h1 id="the-feedbacks">The feedbacks</h1>
<p>Only one of the students give me a feedback. She has written:</p>
<blockquote>
<p>&quot;The class was good but I think that it should have more examples and the instructors do the 'commands' with us since many people like me didn't know how to use the IDE.&quot;</p>
</blockquote>
<p>In my opinion, this comments show how bad a graphic user interface is bad when teaching something that are base in plain text, like a programming language, since there are no way of save a log of the stuffs that you do.</p>
<p>In the second day I will back in how to use the IDE and try to give more examples (unfortunately teach a basic of LaTeX is very hard like any programming language).</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>educação,ensino,Software Carpentry</p>
</div>
<div class="tags">
<p>LaTeX</p>
</div>
<div class="comments">

</div>
