---
layout: post
title: "Hacking Gecko or 1011020"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>For some time now I want to be able to contribute to native support of MathML at Gecko but</p>
<ol type="1">
<li>I only know C (not C++),</li>
<li>I only program command line interfaces (not graphical user interface),</li>
<li>I never program a &quot;graphical framework&quot;, ...</li>
</ol>
<p><a href="http://www.maths-informatique-jeux.com/blog/frederic/">Frédéric</a> recommended that I start with <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=1011020">Bug 1011020</a> because shouldn't be very hard to fix this bug. So I decided to give it a try.</p>
<div class="more">

</div>
<h1 id="reproducing-the-bug">Reproducing the bug</h1>
<p>The bug is about the bars of fractions, radicals etc be invisible at <strong>some</strong> zoom levels. For example, I had to zoom out three times (or to 67%) for reproduce the bug with the following example.</p>
<math display="block">
<msqrt>
<mn>2</mn>
</msqrt>
</math>
<p>And if I zoom out another time (or to 50%) the bar is visible.</p>
<h1 id="compiling">Compiling</h1>
<p>I already have compile Firefox in the past and this was easy this time.</p>
<div class="admonition">
<p>Note</p>
<p>If you are going to compile Firefox for the first time, take a look at <a href="https://developer.mozilla.org/en/docs/Simple_Firefox_build" class="uri">https://developer.mozilla.org/en/docs/Simple_Firefox_build</a>.</p>
</div>
<h1 id="trying-gdb">Trying GDB</h1>
<p>Since I already program in C and GDB help me a lot when debugging I decided to try using it for Firefox. Unfortunately it didn't go very good since Firefox was very slow and because of this I decided to go back and use <code>printf</code>.</p>
<h1 id="finding-the-write-file">Finding the write file</h1>
<p>To work with the <code>msqrt</code> element I :</p>
<pre><code>$ ls layout/mathml | grep -P &#39;(sqrt|root).*cpp&#39;
nsMathMLmrootFrame.cpp
nsMathMLmsqrtFrame.cpp</code></pre>
<p>and</p>
<blockquote>
<p>$ grep -i pixel layout/mathml/nsMathMLmsqrtFrame.cpp $ grep -i bar layout/mathml/nsMathMLmsqrtFrame.cpp</p>
</blockquote>
<p>Since didn't find anything, I :</p>
<pre><code>$ grep -i pixel layout/mathml/nsMathMLmrootFrame.cpp 
  nscoord oneDevPixel = aFontMetrics-&gt;AppUnitsPerDevPixel();
                                oneDevPixel);
                                oneDevPixel);
  nscoord onePixel = nsPresContext::CSSPixelsToAppUnits(1);
  printf(&quot;onePixel      = %d\n&quot;, onePixel);
  if (ruleThickness &lt; onePixel) {
    ruleThickness = onePixel;
  // adjust clearance psi to get an exact number of pixels -- this
  nscoord delta = psi % onePixel;
    psi += onePixel - delta; // round up</code></pre>
<p>and because of it I start reading <code>layout/mathml/nsMathMLmrootFrame.cpp</code> and adding some <code>printf</code> on it. Unfortunately no information was send to the terminal and I <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=1011020#c12">asked</a> for help. Frédéric reply suggesting to look at <code>layout/mathml/nsMathMLmencloseFrame.cpp</code> that first didn't make much sense to me but after I looked at the <a href="http://www.w3.org/TR/MathML3/chapter3.html#presm.menclose">documentation</a> was very clear to me since I can use <code>menclose</code> as a replacement for <code>msqrt</code>, e.g.</p>
<math display="block">
<menclose notation="radical">
<mn>2</mn>
</menclose>
</math>
<div class="admonition">
<p>Note</p>
<p>I should have look at <code>layout/mathml/nsMathMLmsqrtFrame.h</code> and find out that it is related with <code>layout/mathml/nsMathMLmencloseFrame.cpp</code>.</p>
</div>
<p>Then I added the <code>printf</code> at <code>layout/mathml/nsMathMLmencloseFrame.cpp</code> and started get some information (after I discovered that <code>nscoord</code> is <code>int</code>).</p>
<h1 id="onepixel-mrulethickness-and-mradicalrulethickness"><code>onePixel</code>, <code>mRuleThickness</code> and <code>mRadicalRuleThickness</code></h1>
<p>Reading <code>layout/mathml/nsMathMLmencloseFrame.cpp</code> looks that the relevant variables was <code>onePixel</code>, <code>mRuleThickness</code> and <code>mRadicalRuleThickness</code> so I printed it on the terminal and take some notes.</p>
<table>
<colgroup>
<col style="width: 13%" />
<col style="width: 18%" />
<col style="width: 15%" />
<col style="width: 22%" />
<col style="width: 29%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Zoom Level</td>
<td>Bar is visible</td>
<td><code>onePixel</code></td>
<td><code>mRuleThickness</code></td>
<td><code>mRadicalRuleThickness</code></td>
</tr>
<tr class="even">
<td>100%</td>
<td>Yes</td>
<td>60</td>
<td>120</td>
<td>39</td>
</tr>
<tr class="odd">
<td>90%</td>
<td>Yes</td>
<td>60</td>
<td>134</td>
<td>44</td>
</tr>
<tr class="even">
<td>80%</td>
<td>Yes</td>
<td>60</td>
<td>75</td>
<td>42</td>
</tr>
<tr class="odd">
<td>67%</td>
<td>Yes</td>
<td>60</td>
<td>90</td>
<td>42</td>
</tr>
<tr class="even">
<td>50%</td>
<td>No</td>
<td>60</td>
<td>120</td>
<td>45</td>
</tr>
<tr class="odd">
<td>30%</td>
<td>Yes</td>
<td>60</td>
<td>200</td>
<td>56</td>
</tr>
</tbody>
</table>
<p>Right now I'm trying to find what this numbers have of special that could help me fixing the bug.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>MathML,Mozilla,Mozilla Brasil</p>
</div>
<div class="tags">
<p>MathML,Firefox,Gecko</p>
</div>
<div class="comments">

</div>
