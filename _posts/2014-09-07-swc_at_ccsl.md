---
layout: post
title: "Software Carpentry no CCSL/USP"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<figure>
<img src="/images/DSC01730.JPG" class="align-center" style="width:80.0%" />
</figure>
<div class="admonition">
<p>Note</p>
<p>If you don't speek portuguese you will find some informations at <a href="http://software-carpentry.org/blog/index.html">Software Carpentry Blog</a>.</p>
</div>
<div class="admonition">
<p>Nota</p>
<p>As fotos originais utilizadas nesse post encontram-se disponíveis em <span data-role="download">imagens do primeiro dia &lt;ccsl1.zip&gt;</span> e <span data-role="download">imagens do segundo dia &lt;ccsl2.zip&gt;</span>.</p>
</div>
<p>Na semana passada, Alex e eu ministramos dois workshops para a Software Carpentry no Centro de Competência em Software Livre na Universidade de São Paulo.</p>
<div class="more">

</div>
<h1 id="primeira-turma">Primeira Turma</h1>
<figure>
<img src="/images/DSC01648.JPG" class="align-center" style="width:80.0%" />
</figure>
<p>Na primeira turma tivemos 18 inscritos e 27 nomes na lista de espera. Estávamos esperando uma sala cheia mas apenas 10 alunos apareceram. Boa parte dos alunos eram bioinformatas.</p>
<figure>
<img src="/images/DSC01661.JPG" class="align-center" style="width:80.0%" />
</figure>
<p>O primeiro dia começou com Alex ensinando o terminal Unix. A maioria dos alunos já tinha utilizado-a anteriormente mas gostaram de descobrir alguns comandos novos e que poderiam escrever seus próprios <em>shell scripts</em>. Na tarde do primeiro dia eu ensinei Python. Essa não foi a melhor sessão porque eu fiquei com a impressão errada dos alunos: a maior parte das perguntas viam dos alunos que já tinham programado alguma vez (em Perl) e estavam tentando fazer associações com a linguagem que conheciam.</p>
<figure>
<img src="/images/DSC01666.JPG" class="align-center" style="width:80.0%" />
</figure>
<p>O segundo dia começou com Alex ensinando Git. Essa foi a melhor aula de Git que eu já assisti. Alex estava muito calmo seguindo exatamente a lição disponível no nosso site e os alunos conseguiram seguir sem problemas (a única alteração foi que a parte de colaboração e conflitos foi feita em pares de forma semelhante ao <a href="https://github.com/swcarpentry/bc/pull/694">Pull Request enviado por Thomas Kluyver</a> que eu pretendo revisar nos próximos dias). No fim da sessão de Git os alunos queriam respostas para algumas perguntas avançadas e entendemos a sessão por mais uma hora. Na tarde, alteramos nosso plano original e dividimos a turma em dois grupos.</p>
<figure>
<img src="/images/DSC01671.JPG" class="align-center" style="width:80.0%" />
</figure>
<p>Os alunos que já tinham programado anteriormente ficaram com o Alex e tiveram uma apresentação das bibliotecas Numpy, matplotlib e Pandas.</p>
<figure>
<img src="/images/DSC01680.JPG" class="align-center" style="width:80.0%" />
</figure>
<p>Os alunos que estavam começando a programar pela primeira vez ficaram comigo para uma sessão de exercícios utilizando o que tinha sido visto na primeira aula: variáveis, funções, condicionais e laços.</p>
<h1 id="segunda-turma">Segunda Turma</h1>
<figure>
<img src="/images/DSC01685.JPG" class="align-center" style="width:80.0%" />
</figure>
<p>Na segunda turma tivemos 18 inscritos e 23 nomes na lista de espera. Tivemos uma sala cheia (com aproximadamente 18 participantes).</p>
<figure>
<img src="/images/DSC01698.JPG" class="align-center" style="width:80.0%" />
</figure>
<p>O primeiro dia começou com Alex ensinando o terminal Unix. Alguns estudantes gostaram dessa primeira sessão mas muitos ainda estavam se perguntando &quot;por que eu estou aprendendo isso?&quot;. Na tarde eu ensinei Python e pela primeira vez alguém disse que eu estava devagar demais. Eu tinha reduzido a velocidade propositalmente mas tive que interromper a lição algumas vezes porque alguns estudantes estavam tendo problemas com Anaconda e IPython Notebook.</p>
<figure>
<img src="/images/DSC01702.JPG" class="align-center" style="width:80.0%" />
</figure>
<p>O segundo dia começou com Alex ensinando Git. Novamente alguns estudantes tiveram problemas com a instalação (todos os casos eram de estudantes utilizando Mac OS X) que foram resolvidos depois de algum tempo. Novamente a aula do Alex foi maravilhosa mas os alunos não fizeram perguntas avançadas no final. Durante a tarde eu terminei a lição de Python (pois não tinha ensinado laços no primeiro dia) e apresentei alguns exemplos de uso da matplotlib e Pandas.</p>
<figure>
<img src="/images/DSC01720.JPG" class="align-center" style="width:80.0%" />
</figure>
<h1 id="extra">Extra</h1>
<p>Durante o workshop aprendi que o processo de divisão manual é diferente entre o Brasil e os Estados Unidos.</p>
<figure>
<img src="/images/DSC01654.JPG" class="align-center" style="width:80.0%" />
</figure>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>Software Carpentry,Mozilla Science Lab,Mozilla Brasil</p>
</div>
<div class="tags">
<p>USP,CCSL</p>
</div>
<div class="comments">

</div>
