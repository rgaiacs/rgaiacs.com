---
layout: post
title: TeXZilla at AMO
author: "Raniere Silva"
categories: ftb
tags: [ftb]
image:
  feature: ftb.jpg
---
**Note**: This post was update in January 29th base on Frédéric's
comments.

TeXZilla is the name of one [(La)TeX to MathML parser write in
Javascript](https://github.com/fred-wang/TeXZilla) and the name of one
[Mozilla
Add-ons](https://addons.mozilla.org/en-US/firefox/addon/texzilla/) that
use the Javascript parse. Both was written by Frédéric Wang and are
under Mozilla Public License.

In this post I will review the Add-ons.

Install
=======

Install the Add-ons is very easy, just go to
<https://addons.mozilla.org/en-US/firefox/addon/texzilla/> and click in
"Add to Firefox".

![](/images/texzilla/01.png){width="80%"}

You will see one warning since this Add-ons wasn't review by Mozilla. If
you are OK with that, the plugin will be installed and one icon be add
on the top right corner.

![](/images/texzilla/02.png){width="80%"}

First impression
================

If you click on TeXZilla icon it will create a new tab.

![](/images/texzilla/03.png){width="80%"}

The are some examples of formulas in the third drop-down list. You can
choose one of them and the some text will be insert in the text box and
the MathML representation below.

![](/images/texzilla/04.png){width="80%"}

You can edit the text in the text box and press "TAB". The MathML
representation will be update.

![](/images/texzilla/05.png){width="80%"}

The first issue that I found is related with error in (La)TEX since the
information don't help very much (<del>but I know that write this
kind of information is boring</del> the [issue
\#16](https://github.com/fred-wang/TeXZilla/issues/16) have more
information about it).

![](/images/texzilla/06.png){width="80%"}

Matrix
======

I what to try linear algebra equations, but let's start with just some
subscripts.

![](/images/texzilla/09.png){width="80%"}

As you can see in the picture above, it's not compatible with (La)TeX
since for (La)TeX `a_11` is equals to `a_{1}1` that is different from
`a_{11}`. This should be fixed with [issue
\#13](https://github.com/fred-wang/TeXZilla/issues/13).

It work properly with `\mathrm`, `\text`, and `\operatorname`. If want
to know the difference between them, read [this question at
tex.stackexchange.com](http://tex.stackexchange.com/q/48459).

For matrices you can use the `matrix` and `pmatrix` environment (that
came from `amsmath` package).

![](/images/texzilla/matrix01.png){width="80%"}

![](/images/texzilla/matrix02.png){width="80%"}

<del>But it not support bmatrix environment (don't know
why)</del> The picture above is out-of-date since I [send a
patch](https://github.com/fred-wang/TeXZilla/pull/15) to fix the problem
with `bmatrix`.

![](/images/texzilla/matrix03.png){width="80%"}

More tests
==========

It support the `cases` environment (that's great) but need some
improvements handle `\text` (Frédéric "wonders whether TeXZilla should
convert the space in the token elements into non-break space").

![](/images/texzilla/10.png){width="80%"}

<del>It don't support the align environment neither breaking line (but this is OK
since Firefox don't fully support MathML yet)</del> Frédéric used
[itex2MML](http://golem.ph.utexas.edu/~distler/blog/itex2MML.html) as
reference and it support `aligned` that is similar to `align`, both from
`amsmath` package, but to use **only** inside math environments.
TeXZilla also support [others
environments](https://github.com/fred-wang/TeXZilla/pull/17) from
`amsmath` package.

Conclusions
===========

It's a great plugin that enables the user test MathML and I hope get
more people involved.
