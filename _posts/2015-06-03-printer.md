---
layout: post
title: Impressora HP LaserJet Professional P1102w
author: "Raniere Silva"
categories: ftb
tags: [ftb]
image:
  feature: ftb.jpg
---
Meu irmão tem uma impressora HP LaserJet Professional P1102w. Esse foram
os passos para configurá-la:

    # apt-get install hplip
    # hp-setup endereço.ip.da.impressora

onde `endereço.ip.da.impressora` é o endereço IP da impressora que eu
descobri através do roteador.
