---
layout: post
title: "Walvis Bay"
author: "Raniere Silva"
categories: travel
tags: ["Africa", "Namibia", "Walvis Bay"]
image:
  feature: 2019-09-28-walvis-bay.jpg
  author:  Raniere Silva
  licence: CC-BY
---

Walvis Bay has a lagoon that is the home of flamingos.

{% include figure.html filename="2019-09-28-walvis-bay-2.jpg" alternative_text="Photo of Flamingos" caption="Flamingos flying." author="Raniere Silva" licence="CC-BY" %}

{% include figure.html filename="2019-09-28-walvis-bay-3.jpg" alternative_text="Photo of Flamingos" caption="More flamingos." author="Raniere Silva" licence="CC-BY" %}
