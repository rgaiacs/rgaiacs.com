---
layout: post
title: "Swakopmund"
author: "Raniere Silva"
categories: travel
tags: ["Africa", "Namibia", "Swakopmund"]
image:
  feature: 2019-09-27-swakopmund.jpg
  author:  Raniere Silva
  licence: CC-BY
---

Swakopmund is a old town that goes back to the German colonial time.

{% include figure.html filename="2019-09-27-swakopmund-2.jpg" alternative_text="Photo of Swakopmund" caption="Lightning house." author="Raniere Silva" licence="CC-BY" %}

{% include figure.html filename="2019-09-27-swakopmund-3.jpg" alternative_text="Photo of Swakopmund" caption="Sea." author="Raniere Silva" licence="CC-BY" %}

{% include figure.html filename="2019-09-27-swakopmund-4.jpg" alternative_text="Photo of Swakopmund" caption="Houses in front of beach road." author="Raniere Silva" licence="CC-BY" %}

