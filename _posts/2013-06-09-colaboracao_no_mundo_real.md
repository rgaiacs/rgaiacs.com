---
layout: post
title: "Colaboração no mundo real"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>No mês passado quando fui pegar um ônibus perto de casa ví um pequeno pedaço de papel colado no poste indicativo de parada de ônibus que me chamou a atenção.</p>
<figure>
<img src="/images/IMG_20130428_110328.jpg" class="align-center" style="width:80.0%" />
</figure>
<p>Abaixo temos uma imagem mais nítida do texto que estava no papelzinho.</p>
<figure>
<img src="/images/IMG_20130428_110339.jpg" class="align-center" style="width:80.0%" />
</figure>
<div class="more">

</div>
<p>O texto era:</p>
<pre><code>Que ônibus passa aqui?

Esse espaço é para você escrever quais linhas de ônibus
você sabe que passam nesse poto. Já que nossa cidade
não tem sinalização, vamos usar o poder de colaboração
das pessoas para ajudar que não costuma pegar ônibus
por aqui. Juntos a gente constrói uma cidade muito melhor.</code></pre>
<p>Eu achei a iniciativa muito fantática, principalmente por ser um exemplo no mundo real de algo que já funciona muito bem no mundo virtual.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>none</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
