---
layout: post
title: "EuroPython 2018: A Jupyter Enhancement Proposals"
author: "Raniere Silva"
categories: blog
tags: [python, europython, jupyter]
image:
  feature: 2018-07-28-europython-jupyter.jpg
  author: Dave Bezaire
  original_url: https://flic.kr/p/298Tbvj
  licence: CC-BY-SA
---

On 25 July,
I attended [EuroPython](https://ep2018.europython.eu/),
a fantastic conference related with Python,
to talk about [Jupyter](http://jupyter.org/) and the enhancement proposal that I submited last year.
The original plan was to present with my friend [Tania Sanchez Monroy](https://bitsandchips.me)
but she had to cancel in the last minute.
[My slides are available]({% link pdfs/EuroPython2018.pdf %})
and my notes are below.

{% include figure.html filename="2018-07-28-europython-jupyter-icms.jpg" alternative_text="Photo from hackday at ICMS" caption="Photo from hackday at ICMS" author="Raniere Silva" licence="CC-BY" %}

In early 2017,
I was invited by [Alexander Konovalov](https://alex-konovalov.github.io/)
to attend Computational Mathematics with Jupyter at International Centre for Mathematical Sciences (ICMS)
in Edinburgh.
During the event,
Alexander Konovalov,
[Michael Croucher](http://www.walkingrandomly.com/),
Tania Allard
and I
talked about how we write lessons with Jupyter so far.
I contributed to Software Carpentry lessons back in 2014
when they still used Jupyter notebooks, IPython notebook at that time,
for the Python lesson.
Alexander Konovalov wrote GAP lessons for Software Carpentry
and was interested to develop the lessons in Jupyter notebooks.
Michael Croucher and Tania Allard were working at the University of Sheffield
with some professors to build courses in Jupyter notebooks
and were interested to have a better presentation for the lessons being develop.
The cell strucuture of Jupyter notebook
imposes a limitation that we could have executed code inside call out boxes
but authors were OK with it.
During that week,
Tania worked on some prototypes to convert Jupyter notebooks Markdown files
that Jekyll could use to build a beautiful website.

{% include figure.html filename="2018-07-28-europython-jupyter-sheffield.jpg" alternative_text="Screenshot of Tania's project" caption="Screenshot of http://bitsandchips.me/Modules-template/" author="Raniere Silva" licence="CC-BY" %}

Fastforwarding a few months,
Thomas Kluyver and I were on EuroPython 2017
and one attendee told us that they use Jupyter Notebook to write some reports
but their employer want to have the company logo on the report.
So far,
they copy and paste the content of the Jupyter Notebook
on the Microsoft Office Word/LibreOffice Writer template
provided by their employer
but this process requires some manual intervention,
e.g. to remove the execution number at the left of the coding cells.
They asked if we knew of one way to make this task more time efficient.
Thomas mentioned that [nbconvert](https://nbconvert.readthedocs.io/) allow users to use templates when converting Jupyter Notebook
to another format that could be HTML, Python script, Markdown, LaTeX and others.
Work or contribute with a
worldwide distributed team
means that you will receive email and messages all day long.
Unfortunately,
you can only use custom templates if you call nbconvert from the command line.

After EuroPython 2017,
I started looking if was possible to include custom templates to Jupyter Notebook web interface.
I knew was possible,
the question was how much of code would have to change.
On my investigation,
I discovered that I would need to include information related with the default template
to the Jupyter Notebook file
and this would require changes on the [Jupyter Notebook specification](http://nbformat.readthedocs.io/).

At the end,
I wrote a [Jupyter Enhancement Proposals](https://github.com/jupyter/enhancement-proposals/pull/23)
and submitted it.
The proposal received a few comments
but I end up without time to develop some prototypes
to push the proposal forward.
The Jupyter code developers were very friendly when reviewing the proposal.

My take home intentions to my audience were
talk with colleagues about things that you don't like on open source tools
and core developers are welcome to your suggestions.