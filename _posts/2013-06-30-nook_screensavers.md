---
layout: post
title: Nook screensavers
author: "Raniere Silva"
categories: ftb
tags: [ftb]
image:
  feature: ftb.jpg
---
<div class="admonition note">

Embora esse blog seja voltado a falar se software livre e o Nook rode
uma versão do Android fechado resolvi escrever sobre como mudar a tela
de descanço (screensaver) dele.

</div>

Depois de conectar o Nook via USB a um computador, montá-lo e acessá-lo
você irá se depara com a seguinte estrutura de diretórios:

    /media/NOOK/
    ├── My Files
    │   ├── Books
    │   ├── Documents
    │   ├── Magazines
    │   └── Newspapers
    └── screensavers

Para criar uma nova coleção de telas de descanço, basta criar uma pasta
dentro de `screensavers` com o nome desejado e adicionar nela as imagens
a serem utilizadas. Você ficará com a seguinte estrutura de diretórios:

    /media/NOOK/
    ├── driveinfo.calibre
    ├── LOST.DIR
    ├── metadata.calibre
    ├── My Files
    │   ├── Books
    │   ├── Documents
    │   ├── Magazines
    │   └── Newspapers
    └── screensavers
        └── family
            ├── photo01.JPG
            ├── photo02.JPG
            └── photo02.JPG

Devido a tela do Nook reproduzir apenas escala de cinza e ter uma
resolução dde 600 X 800 é preciso modificar as imagens para que elas
estarem nessa forma.

Depois disso, basta desmontar o Nook, desconectá-lo e ir no painel de
controles para selecionar a tela de descanço desejada.
