---
layout: post
title: "Tecnologias na educação"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Nessa semana li no <a href="http://goodereader.com/blog/tablet-slates/apple-taps-into-education-with-products-and-services">Goodereaders</a> que a Apple criou uma seção denominada &quot;<a href="http://www.apple.com/education/">Apple and Education</a>&quot; na sua página (<a href="http://www.apple.com/br/education/">também existe uma versão em português focada no Brasil</a>).</p>
<p>De certa forma é interessante ver a diferença entre os aplicativos educacionais proprietários que rodam nos aparelhos da maçã e o REAs (Recursos Educacionais Abertos).</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>educação</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
