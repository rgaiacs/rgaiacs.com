---
layout: post
title: "Mais um blog."
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>O conteúdo presente na web é tão vasto que (quase) tudo o que existe hoje e deveria ser anunciado e/ou comentado já foi. Então por que criar mais um blog para falar do que já foi falado? Porque</p>
<ul>
<li>ao escrever posso informar alguém sobre algo que ela nunca tomaria conhecimento,</li>
<li>escrever é uma ótima forma de refletir sobre seus próprios pensamentos,</li>
<li>todos deveriam manter um registro pessoal sobre suas ideias.</li>
</ul>
<p>E sobre o que pretendo escrever nesse blog?</p>
<ul>
<li>Questionamentos sobre questões filosóficas.</li>
<li>Coisas novas que aprendi.</li>
<li>Textos que li e gostei.</li>
<li>E anúncios referentes ao meu domínio rgaiacs.com.</li>
</ul>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>none</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
