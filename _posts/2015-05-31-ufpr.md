---
layout: post
title: "Workshop de ferramentas computacionais na UFPR"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<div class="admonition">
<p>Update</p>
<p>O Abel Siqueira <a href="https://abelsiqueira.github.io/resultado-da-oficina-de-ferramentas-computacionais-para-pesquisadores/">também escreveu sobre o workshop</a>.</p>
</div>
<p>Na semana passada, 28-31/05, estive em Curitiba para realizar um workshop de ferramentas computacionais na UFPR <strong>em conjunto</strong> com um amigo e concluir alguns projetos.</p>
<div class="admonition">
<p>Agradecimentos</p>
<p>Obrigado ao <a href="http://abelsiqueira.github.io/">Abel Siqueira</a> e à Kally Chung por serem ótimos anfitriões e terem me aguentado durante esses quatro dias.</p>
</div>
<div class="admonition">
<p>Agradecimentos</p>
<p>Obrigado à <a href="http://people.ufpr.br/~ewkaras/">Elizabeth Karas</a>, à <a href="http://people.ufpr.br/~mael/">Mael Sachine</a> e ao <a href="http://people.ufpr.br/~lucaspedroso/">Lucas Pedroso</a> pelo interesse no workshop e auxílio em sua realização.</p>
</div>
<div class="more">

</div>
<h1 id="workshop">Workshop</h1>
<div class="admonition">
<p>Agradecimentos</p>
<p>Obrigado pelo auxílio da Kally Chung e do <a href="https://fernandomayer.github.io/">Fernando Mayer</a> no workshop.</p>
</div>
<p>O workshop ocorreu durante todo o sábado, 30, e com a presença de aproximadamente 20 participantes na parte da manhã, quando foi apresentado o <a href="https://www.gnu.org/software/bash/">terminal UNIX</a> e <a href="http://julialang.org/">Julia</a>, e de 12 na parte da tarde, quando foi apresentado <a href="http://git-scm.com/">Git</a> e <a href="http://github.com/">GitHub</a>.</p>
<p>Problemas com os softwares utilizados foram mínimos. Para a próxima vez vou tentar lembrar de reforçar a necessidade de um navegador web recente porque o GitHub <strong>só</strong> funciona com versões recentes dos navegadores web mais populares (e.g. Firefox, Chrome/Chromium, Safari e IE).</p>
<p>As sessões também foram bem tranquilas, sem problemas.</p>
<p>No final, pedimos para os participantes escreverem feedback nos post-its utilizados. Pelos feedbacks os participantes gostaram do workshop e a maioria dos feedbacks &quot;negativos&quot; enquadram-se em</p>
<ul>
<li>&quot;gostaria de mais tempo&quot; e</li>
<li>&quot;gostaria que fosse mais profundo&quot;</li>
</ul>
<p>que mostra uma necessidade para mais workshops desse tipo.</p>
<h1 id="material---terminal-unix">Material - terminal UNIX</h1>
<p>Uma boa leitura para apender mais sobre o terminal Unix é a <a href="http://swcarpentry.github.io/novice-shell">lição de Bash</a> da <a href="http://software-carpentry.org/">Software Carpentry</a> que foi utilizado no workshop.</p>
<h1 id="material---julia">Material - Julia</h1>
<p>Os slides utilizados encontram-se em <a href="https://gitlab.com/abelsiqueira/pres-julia" class="uri">https://gitlab.com/abelsiqueira/pres-julia</a>.</p>
<p>O material mais completo sobre Julia é a <a href="http://docs.julialang.org/en/release-0.3/">documentação oficial</a> que infelizmente pode ser muito técnica para alguns.</p>
<p>Outros materiais encontram-se listados em <a href="http://julialang.org/learning/" class="uri">http://julialang.org/learning/</a>.</p>
<h1 id="material---gitgithub">Material - Git/GitHub</h1>
<p>Os slides utilizados encontram-se em <a href="https://github.com/abelsiqueira/workshop" class="uri">https://github.com/abelsiqueira/workshop</a>.</p>
<p>Uma boa leitura para revisar o que foi apresentado no workshop é a <a href="http://swcarpentry.github.io/git-novice/">lição de Git</a> da Software Carpentry.</p>
<p>Um ótimo conjunto de dicas sobre Git encontra-se em <a href="http://gitready.com/" class="uri">http://gitready.com/</a>.</p>
<p>Aqueles que desejarem aprofundar seus conhecimentos em Git encontram em <a href="https://git-scm.com/documentation/external-links" class="uri">https://git-scm.com/documentation/external-links</a> uma longa lista de bons materiais (alguns gratuitos e outros <strong>pagos</strong>).</p>
<h1 id="perporf">perporf</h1>
<p><a href="https://github.com/ufpr-opt/perprof-py/">perprof</a> é um projeto que eu e o Abel começamos no final de 2013 para facilitar a visualização de perfis de desempenho.</p>
<figure>
<img src="/images/perprof.jpg" class="align-center" style="width:80.0%" />
</figure>
<p>Durante o período que estive em Curitiba, eu e o Abel</p>
<ul>
<li>adicionamos suporte ao <a href="http://bokeh.pydata.org/">Bokeh</a>,</li>
<li>fizemos pequenas correções, e</li>
<li>lançamos a versão 1.1 do perprof.</li>
</ul>
<p>Para aqueles que gostam de números, foram</p>
<ul>
<li>13 commits,</li>
<li>6 pull requests fechados,</li>
<li>5 issues fechados, e</li>
<li>3 novos issues para lembrar de tarefas futuras.</li>
</ul>
<h1 id="jors">JORS</h1>
<p>Como na academia é importante ter algo que possa ser publicado, o perprof precisava de um artigo ou relatório técnico sobre ele. Eu e o Abel também voltamos a trabalhar no relatório técnico que pretendemos enviar para o <a href="http://openresearchsoftware.metajnl.com/">Journal of Open Research Software</a>.</p>
<figure>
<img src="/images/jors.jpg" class="align-center" style="width:80.0%" />
</figure>
<p>Nesses dias, fechamos os últimos detalhes que estavam pendentes para a submissão do trabalho, que pode ser conferido <a href="https://github.com/ufpr-opt/perprof-jors/">aqui</a>.</p>
<p>Para aqueles que gostam de números, foram</p>
<ul>
<li>8 commits,</li>
<li>3 pull requests fechados, e</li>
<li>3 issues fechados.</li>
</ul>
<h1 id="conclusões">Conclusões</h1>
<p>Os dias em Curitiba foram muito produtivos, pois além do workshop tivemos progresso com o perprof. Também trocamos valiosas informações nas conversas durante os intervalos e translado de casa para a UFPR.</p>
<p>Espero que o Abel consiga tempo para realizar outros workshops e também pessoas para auxiliá-lo nessa tarefa.</p>
<p>Também espero poder voltar em Curitiba, preferencialmente à passeio mas à trabalho também não tem problema.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>academia,educação,Software Carpentry</p>
</div>
<div class="tags">
<p>Curitiba,UFPR,Git,Julia</p>
</div>
<div class="comments">

</div>
