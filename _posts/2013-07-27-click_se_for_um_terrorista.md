---
layout: post
title: "Click se for um terrorista"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>No dia 23/07/2013 Evgeny Morozov, pesquisador-visitante da Universidade de Stanford, escreveu o artigo &quot;<a href="http://www1.folha.uol.com.br/colunas/evgenymorozov/2013/07/1300648-big-data-poderia-ter-impedido-o-11-de-setembro.shtml">'Big data' poderia ter impedido o 11 de Setembro</a> (tradução de Paulo Migliacci) para a Folha de São Paulo (<span data-role="download">ver cópia
local &lt;big-data-poderia-ter-impedido-o-11-de-setembro.html&gt;</span>).</p>
<p>Um dos pontos do artigo de Morozov é que &quot;precisamos apenas estabelecer algum relacionamento entre os terroristas desconhecidos do amanhã e os terroristas estabelecidos de hoje&quot;. Isso significa que:</p>
<div class="more">

</div>
<ul>
<li><p>se a maioria dos terroristas de hoje utilizam</p>
<ul>
<li><a href="http://pt.wikipedia.org/wiki/The_Onion_Router">Tor</a>,</li>
<li><a href="http://pt.wikipedia.org/wiki/GPG">gpg</a> (GNU Privacy Guard),</li>
<li><a href="http://pt.wikipedia.org/wiki/P2p">p2p</a> (e.g., torrent),</li>
</ul>
<p>e você também utiliza esses serviços é um terrorista;</p></li>
<li><p>se os manifestantes do <a href="http://saopaulo.mpl.org.br">Movimento Passe Livre</a> responsáveis pelas manifestações dos últimos meses no Brasil utilizam</p>
<ul>
<li>Facebook e</li>
<li>Twittert</li>
</ul>
<p>e você também utiliza esses serviços é um membro do movimento;</p></li>
<li><p>se pessoas que possuem intolerância a lactose ao irem no supermercado não compram</p>
<ul>
<li>leite,</li>
<li>queijo,</li>
<li>manteiga,</li>
<li>chocolate,</li>
<li>pão,</li>
<li>biscoito,</li>
<li>pizza, ...</li>
</ul>
<p>e você também possui esse perfil de compra então você também possui intolerância a lactose.</p></li>
</ul>
<p>Claro que nenhuma das relações acima são obrigatoriamente verdadeiras mas elas acabam surgido no ambiente do &quot;big data&quot; uma vez que nesse ambiente &quot;não precisamos perguntar por que as coisas são como são, desde que possamos influenciá-las para que sejam o que desejamos que sejam&quot;. E esse é um dos grandes perigos do &quot;big data&quot;.</p>
<div class="note">
<div class="admonition-title">
<p>Note</p>
</div>
<p>Relacionado com o tema, correlações, sugiro os livros <a href="http://www.freakonomics.com/books/freakonomics/">Freakonomics</a> e <a href="http://www.freakonomics.com/books/superfreakonomics/">SuperFreakonomics</a> e o <a href="http://www.freakonomics.com/movie/">filme</a> baseado nos livros anteriores.</p>
</div>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>none</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
