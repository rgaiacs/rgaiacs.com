---
layout: post
title: "Definitivamente, o Melhor Jogo"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Você sabe aqueles jogos de simulação, e.g., <a href="http://pt.wikipedia.org/wiki/RollerCoaster_Tycoon">RollerCoaster Tycoon</a>, <a href="http://pt.wikipedia.org/wiki/Zoo_Tycoon_(jogo_eletr%C3%B4nico)">Zoo Tycoon</a>, <a href="http://pt.wikipedia.org/wiki/SimCity_(s%C3%A9rie)">SimCity</a>, ... Pois o último jogo neste estilo é um simulador de uma empresa que produz jogos de computador, o <a href="http://www.greenheartgames.com/app/game-dev-tycoon/">Game Dev Tycoon</a>. Esse pode parecer mais um jogo qualquer mas IMHO ele é, definitivamente, o melhor jogo de todos os tempos e o motivo encontra-se <a href="http://www.greenheartgames.com.nyud.net/2013/04/29/what-happens-when-pirates-play-a-game-development-simulator-and-then-go-bankrupt-because-of-piracy/">neste post</a>.</p>
<div class="more">

</div>
<p>Para quem não sabe inglês, a seguir encontra-se uma &quot;tradução&quot;.</p>
<blockquote>
<p>Quando lanchamos nosso jogo, fizemos algo não usual e único. Nos lançamos uma versão crackeada do nosso jogo minutos depois do lançamento em nossa loja.</p>
<p>A versão crackead foi disponibilizada por meio da rede torrent com a ajuda de alguns amigos. Em pouco tempo o torrent estava consumindo toda minha velocidade de upload com conexões provenientes de todas as partes do mundo.</p>
<p>Essa versão crackead que disponibilizamos é muito parecida com a versão que vendemos exceto por um detalhe: depois de um tempo o jogador começa a receber uma mensagem (identica as demais do jogo) informando que o jogo que ele está comercializando está sendo pirateado. Com a pirataria, a empresa do jogador vai a falência e o jogo termina.</p>
<p>Depois de algum tempo, começaram a surgir comentários na WEB sobre nosso jogo em que os jogadores perguntavam como que eles poderiam evitar a pirataria dos seus jogos. Que ironia.</p>
</blockquote>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>none</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
