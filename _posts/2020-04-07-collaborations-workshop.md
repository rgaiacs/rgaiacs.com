---
layout: post
title: "Collaborations Workshop 2020"
author: "Raniere Silva"
categories: conferences
tags: ["Software Sustainability Institute", "Collaborations Workshop", "UK", "remote"]
image:
  feature: 2020-04-07-collaborations-workshop.jpg
  author: Liliana Qu
  licence: Splash License
  original_url: https://unsplash.com/photos/LfaN1gswV5c
---

From 31 March 2020 until 2 April 2020,
the [Software Sustainability Institute](https://software.ac.uk/cw20)
hosted the first online edition of [Collaborations Workshop](https://software.ac.uk/cw20)
due [COVIN-19 pandemic](https://en.wikipedia.org/wiki/2019%E2%80%9320_coronavirus_pandemic).
They were planning to have the workshop
in Belfast, UK
but, due the pandemic,
they decided to move it online
with only [two weeks before the event](https://twitter.com/SoftwareSaved/status/1237675883596599297).

I'm not a big fan of online events
because I don't believe that you get the same human interaction
that a in-person event provide.
When I saw the announcement that Collaborations Workshop would be online,
I knew it was something that I want to be part of
because the Software Sustainability Institute moved successfully their Fellow selection
from in-person to online in 2017
and
they had experience to make the online Collaborations Workshop a big success.

The Software Sustainability Institute used
[Zoom](https://zoom.us/) for video
and
[Slack](http://slack.com/) for text.
I imagine the use of Zoom
is because of their breakout rooms
and
the use of Slack
is because of many people are using it
already.
Zoom worked very well
and some attendees used virtual background.
Unfortunately,
were impossible to see all the ~100 attendees in one screen
at the same time
(and your 8k resolution monitor doesn't solve this problem, yet).
Messages on Slack
were friendly
and
the most popular channel was
`pets-at-cw`
where attendees shared photos of their cats, dogs and other pets.

*Keynote*,
*Mini-workshops and demos*,
and *Lightning Talks*
were good
and I recommend you to watch them
when they
release the recording
on [YouTube](https://www.youtube.com/user/SoftwareSaved).
Given the Institute experience
with the online Fellows selection,
the *Discussion groups and speed blogging*
and *Collaborative Ideas/Hack Day Ideas Session*
went incredible well
with the help of Zoom's breakout rooms.
The transition to the breakout rooms
and the return to the main room
were one click away from all attendees.
In the next few weeks,
I believe the Software Sustainability Institute
will start publishing the speed blogs
in their website
and you can [subscribe to their week news](https://software.ac.uk/subscribe)
to be notified of the posts.

The Hack Day
wasn't engaged for me,
maybe because many ideas were related with online events
and I was four hours behind all other attendees (time zone speaking).
From the presentations,
I noticed that many attendees had a fun
working in a project not related with their day to day work.
And [some](https://twitter.com/yoyehudi/status/1246131835232542721)
learnt that you should not use your personal Twitter account
to be the bot of your Hack Day project.

Big congratulations to [Rachael Ainsworth](https://twitter.com/rachaelevelyn)
and all the Software Sustainability Institute team
to run successfully their first ever Collaborations Workshop,
and, lets hope, their last online one
because they have the best conference social programme
with board games and walks in addition to lunches and dinners.