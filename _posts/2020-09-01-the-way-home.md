---
layout: post
title: "Life Without Technology"
author: "Raniere Silva"
categories: life
tags: ["Minimalism"]
image:
  feature: 2020-09-01-the-way-home.jpg
  author: Michele Tardivo
  licence: Unsplash License
  original_url: https://unsplash.com/photos/G0OnHccvwcA
---

A friend of mine recommended Mark Boyle's
"The Way Home" book
to me.
I enjoyed reading the book,
where Mark tell his experience living without technology,
in the last two weeks.
The conflict that Mark mentioned a couple of times during the book,
have to use technology,
spoke to me
as it happens constantly when debating the use of free and open source software.

Spoiler alert!
For me,
the saddest moment in the book was

> I received a letter, this morning, from Kirty.
> (...)
> This morning's letter is different.
> In it she tells me that she has decided
> she won't be coming back here to live.
> She has other paths
> she feels called to explore,
> and she wants to give all of her energy to that,
> on her own.

This part was sad because of my broken heart.