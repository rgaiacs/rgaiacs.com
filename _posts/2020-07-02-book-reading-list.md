---
layout: post
title: "July's Book Reading List"
author: "Raniere Silva"
categories: book
tags: ["Reading List Challenge"]
image:
  feature: 2020-07-02-book-reading-list.jpg
  author: Debby Hudson
  licence: Splash License
  original_url: https://unsplash.com/photos/ERb-JXVwAfo
---

In June,
I read the latest novel of Robert J. Sawyer:
"[The Oppenheimer Alternative](https://www.goodreads.com/book/show/49706374-the-oppenheimer-alternative)".
Robert J. Sawyer is one of my favourite authors
but I didn't enjoy "The Oppenheimer Alternative"
as much as his other novels,
such as the [WWW trilogy](https://www.goodreads.com/series/46141-www).
I hope I have better lucky this month with my reading list.

## Biography & Memoir

"[Making It Up As I Go Along](https://www.goodreads.com/book/show/27710299-making-it-up-as-i-go-along)" by Marian Keyes.

## Philosophy & Psychology & Spirituality & Self Help

"[Start with Why](https://www.goodreads.com/book/show/7108725-start-with-why)" by Simon Sinek.

## Fantasy & Fiction

"[The Ten Thousand Doors of January](https://www.goodreads.com/book/show/43521657-the-ten-thousand-doors-of-january)"
by Alix E. Harrow.

## Romance & Young Adult

"[Swipe Right](https://www.goodreads.com/book/show/49440626-swipe-right)"
by Stephie Chapman .

