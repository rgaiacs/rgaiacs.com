---
layout: post
title: "GSoC: June 30 - July 06"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>This is one of the reports about my GSoC project and cover the seventh week of “Students coding”.</p>
<p>At this seventh week I worked on &quot;small&quot; issues. Bellow you will find more details about the past week and the plans for this one.</p>
<div class="more">

</div>
<h1 id="math-environment-key">Math Environment Key</h1>
<p>I <a href="https://github.com/r-gaia-cs/gaia/issues/3">added a math environment key</a>. Instead of using alternate for math display environment I chose to let the user type the math environment key twice.</p>
<h1 id="font-size">Font Size</h1>
<p>Have the font size of the last row changed when moving from one layout to another is disturbing. To solve it I set the CSS for every key instead for the layout. I know that this wasn't a great solution but I didn't find a better one.</p>
<h1 id="styling-math">Styling Math</h1>
<p>Some <a href="https://en.wikibooks.org/wiki/LaTeX/Mathematics#Formatting_mathematics_symbols">mathematical fields use &quot;special&quot; fonts</a>, e.g. Fraktur font for Lie Algebras and Blackboard bold for sets. After think how to support this fonts I decide to include it as alternates keys at the Latin layout.</p>
<figure>
<img src="/images/styled-keys.png" class="align-center" style="width:50.0%" />
</figure>
<p>Doing this create a small issue at the nonlinear input since it behave like:</p>
<ul>
<li><p>Before press 𝒩:</p>
<pre><code>▮</code></pre></li>
<li><p>After press 𝒩:</p>
<pre><code>\mathcal{▮N}</code></pre></li>
</ul>
<p>But will be better if it behave like:</p>
<ul>
<li><p>Before press 𝒩:</p>
<pre><code>▮</code></pre></li>
<li><p>After press 𝒩:</p>
<pre><code>\mathcal{N▮}</code></pre></li>
</ul>
<h1 id="alternate-keys-list-overflows">Alternate Keys List Overflows</h1>
<p>Some keys have more than ten alternate keys and because of it some aren't available at the screen.</p>
<p>This is an <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=934209">&quot;old&quot; issue</a> that I start trying to solve. This isn't a easy one and I still need to address the align of the alternate keys and avoid Javascript inline CSS injection.</p>
<figure>
<img src="/images/two-rows-alternate.png" class="align-center" style="width:50.0%" />
</figure>
<h1 id="alternate-keys-need-uppercase-value-to-display">Alternate Keys Need Uppercase Value to Display</h1>
<p>One of my patches broke the ability of the keyboard show the key that will be insert <strong>with the properly case value</strong> after type it. I just notice it at the end of this week.</p>
<h1 id="others-bugs">Others Bugs</h1>
<p>I also work at <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=1022609">Bug 1022609</a>.</p>
<h1 id="plans-for-july-07---july-13">Plans for July 07 - July 13</h1>
<p>Next week I will be at <a href="http://cermat.org/events/MathUI/14/">MathUI</a>, a conference about Math User Interfaces. This is a great opportunity to get feedbacks for the project.</p>
<p>And during the flight I will try to solve the issues describe above.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>GSOC</p>
</div>
<div class="tags">
<p>GSOC2014</p>
</div>
<div class="comments">

</div>
