---
layout: post
title: "Oslo - Hiking"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<figure>
<img src="/images/IMG_20160703_110534_BURST17.jpg" alt="Landscape." class="align-center" style="width:80.0%" /><figcaption>Landscape.</figcaption>
</figure>
<div class="more">

</div>
<figure>
<img src="/images/IMG_20160703_105410.jpg" alt="Signs." class="align-center" style="width:80.0%" /><figcaption>Signs.</figcaption>
</figure>
<figure>
<img src="/images/IMG_20160703_110543_BURST16.jpg" alt="Landscape." class="align-center" style="width:80.0%" /><figcaption>Landscape.</figcaption>
</figure>
<figure>
<img src="/images/IMG_20160703_115419.jpg" alt="Landscape." class="align-center" style="width:80.0%" /><figcaption>Landscape.</figcaption>
</figure>
<figure>
<img src="/images/IMG_20160703_133514.jpg" alt="Landscape." class="align-center" style="width:80.0%" /><figcaption>Landscape.</figcaption>
</figure>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>none</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
