---
layout: post
title: "EPUB in Firefox OS"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<div class="note">
<div class="admonition-title">
<p>Note</p>
</div>
<p>I had to thanks <a href="https://mozillians.org/en-US/u/brunovilar/">Bruno Vilar</a> that lend me the device used for the review.</p>
</div>
<p>I have done a review of the EPUB apps for Firefox OS and it support to MathML:</p>
<ul>
<li><a href="http://ftb.rgaiacs.com/2013/12/18/firefox_os_epubreader.html">EPUBReader</a>,</li>
<li><a href="http://ftb.rgaiacs.com/2013/12/18/firefox_os_ab_fb2_reader.html">Ab FB2 reader</a> and</li>
<li><a href="http://ftb.rgaiacs.com/2013/12/18/firefox_os_mathml.html">MathML support</a>.</li>
</ul>
<p><strong>Update in 24/12/2013 thanks to Frédéric Wang</strong>: one of the extensions of Firefox to read EPUB is <a href="http://lucidor.org/lucifox/">LUCIFOX</a> (it's GPLv3) but it didn't have a version for Firefox OS. One of the Javascript libraries to read EPUB is <a href="http://futurepress.github.io/epub.js/">Epub.js</a> (it's BSD) but it require node.js. And any of this readers support interactive EPUB.</p>
<div class="more">

</div>
<h1 id="device-information">Device Information</h1>
<p>For the review I used the <a href="http://www.vivoblog.com.br/chegou-o-alcatel-one-touch-fire-com-firefox-os-voce-muda-ele-tambem.html">Alcatel One Touch Fire</a>.</p>
<p><img src="/images/system-information.png" alt="Screenshot of the information of the device." class="align-center" /></p>
<p>It was running the Firefox OS 1.1.0.0-prerelease (based on Gecko 18.1) and the 02002 version of the firmware.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>Mozilla,MathML</p>
</div>
<div class="tags">
<p>Firefox OS,ebooks,ereader,MathML</p>
</div>
<div class="comments">

</div>
