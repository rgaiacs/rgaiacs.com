---
layout: post
title: "LaTeX, HTML e Web"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>LaTeX é uma linguagem de marcação para o sistema de tipografia TeX que foi criado por Leslie Lamport no início dos anos 80. O HTML é uma linguagem de marcação de hyper-texto proposta por Tim Berners-Lee em 1980, especificado e implementado em 1990 (em conjunto com o que viria a ser a World Wide Web).</p>
<p>Francis Diebold escreveu uma maravilhosa série de três post abordando o LaTeX e a Web:</p>
<ul>
<li><a href="http://fxdiebold.blogspot.com.br/2013/11/the-e-writing-jungle-part-one-latex-and_7.html">The e-Writing Jungle Part 1: LaTeX to pdf to the Web</a> (<span data-role="download">cópia local &lt;the-e-writing-jungle-part-1.html&gt;</span>),</li>
<li><a href="http://fxdiebold.blogspot.com.br/2013/11/the-e-writing-jungle-part-2-html5-and.html">The e-Writing Jungle Part 2: The MathML Impasse and the MathJax Solution</a> (<span data-role="download">cópia local &lt;the-e-writing-jungle-part-2.html&gt;</span>),</li>
<li><a href="http://fxdiebold.blogspot.com.br/2013/12/the-e-writing-jungle-part-3-web-based-e.html">The e-Writing Jungle Part 3: Web-Based e-books Using Python / Sphinx</a> (<span data-role="download">cópia local &lt;the-e-writing-jungle-part-3.html&gt;</span>),</li>
</ul>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>none</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
