---
layout: post
title: "Patenteando genes"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p><a href="http://thesocietypages.org/cyborgology/author/jennydavis/">jennydavis</a> publicou um artigo intitulado &quot;<a href="http://thesocietypages.org/cyborgology/2013/04/19/an-attempt-to-patent-the-sun/">An Attempt to Patent The Sun</a>&quot; (ou &quot;Uma tentativa de patentear o Sol&quot; em tradução literal) no qual aborda o caso da tentativa de uma empresa patentear dois genes, um relacionado com o câncer de mama e o outro com o câncer de ovário), e as implicações dessa patente ser homologada.</p>
<div class="more">

</div>
<p>A seguir encontra-se uma tradução do artigo.</p>
<blockquote>
<p>São os genes humanos patenteáveis? No início desta semana, a corte suprema (SCOTUS) ouviu os argumentos para responder a essa pergunta. Especificamente, a empresa de biotecnologia Myriad Genetics, Inc. quer defender sua patente do isolamento dos genes BRAC1 e BRAC2 - dois genes relacionados ao câncer de mama e de ovário. Com a concessões dessa patente a empresa teria por 20 anos o controle do monopólio sobre os genes para a pesquisa, diagnóstico, e finalidades do tratamento. Um grupo de profissionais médicos, cientistas, e pacientes estão desafiando a patente.</p>
<p>Os critérios para uma patente médica são tais que ferramentas, medicamentações, produtos químicos produzidos laboratório, etc pudem ser patenteados, a &quot;natureza&quot; não pode ser patenteada. Isso é, uma patente deve tratar de algo criado, não meramente descoberto (apesar de quanto a descoberta tenha custado). Os oponentes da patente de BRAC evocam frequentemente Jonas Salk, quem disse a famosa resposta à potencial patente de sua vacina da poliomielite: &quot;Não há nenhuma patente. Poderia você patentear o sol?&quot;</p>
<p>Aqueles que se opõem a patente discutem que os genes, como parte do corpo humano, estão eliminados de patentes. Myriad Genetics, por sua vez, argumenta que o processo laborioso de localizar destes genes como indicadores de cancer de mama e de ovário hereditário e a isolação dos mesmos é um processo de desnaturalização, e que os genes isolados são algo separável do corpo natural para ser patenteável.</p>
<p>Os debates sobre a patente destes dois genes levam para a uma pergunta simples (com uma resposta menos simples): Podem os genes humanos ser definidos como uma tecnologia? Em caso afirmativo, são certamente patenteáveis. Se não, não são patenteáveis.</p>
<p>A tecnologia, no sentido mais simples, pode ser definida como uma ferramenta com que os seres humanos aumentam sua existência. Os seres humanos existiram sempre não somente com, mas igualmente com tecnologias. Contudo, como aquele que aumenta e é aumentado, a orgânica, tecnologia é necessariamente separavel do orgânico. Para manter sua patente, Myriad Genetics teria que mostrar que BRAC1 e BRAC2 são algo que não materiais corporais orgânicos.</p>
<p>Tal separação, eu discuto, não é possível. Os genes humanos não aumentam o corpo, são o corpo, presente antes da descoberta, antes da nomeação, antes de sua relação à doença genética. As ferramentas da descoberta, então, são as tecnologias, o processo de nomeação, diagnosticando, etc que é um processo tecnologico. No entanto, os genes permanecem orgânicos.</p>
<p>Para não ser antipático, eu discuti comigo mesmo que o corpo pode ser compreendido como a tecnologia - ou mais específicamente - como uma ação materializada. Isto é, nós fazemos o sentido de nossos corpos com a língua, experimentamos nossos corpos em maneiras cultural encaixadas, alteramos nossos corpos fora da motivação social e com efeito comunicativo. Desta maneira, o processo de descobrir, de nomear, de diagnosticar, e de tratar os genes é um processo tecnologico. No entanto outra vez, os genes em si não são uma tecnologia.</p>
<p>Para discutir que BRAC1 e BRAC2 são simultaneamente orgânicos e tecnologicos é preciso cruzar a linha teórica, fina e porosa, de uma perspectiva aumentada em uma perspectiva fortemente aumentada forte. É empregar o tratamento problemático de tecnologico e orgânico como, não apenas, mutuamente constitutivo, mas iguais. Tal perspectiva não reconhece que cada - enquanto que compreendido sempre em termos do outro - um possui propriedades distingas. O decisão final, dependerá da capacidade da corte para distinguir a delicada distinção entre a constituição mútua e mistura não diferenciada.</p>
<p>O significado deste caso, naturalmente, vai para além dos debates da torre-de-babel sobre a precisão teórica. Ao contrário, de perguntar &quot;quem será beneficiado, e quem será prejudicado?&quot; nós vemos que, na mesma base, este caso coloca companhias farmacéuticas contra pacientes. Aqueles que discutem contra a patente discutem não somente que é ilegal, mas que retardará a investigação e desenvolvimento, o acesso aos testes e o tratamento, e finalmente, conduzem à perda de vidas. O resultado do caso determinará conseqüentemente a extensão a que parte do corpo pode ser cooptado para o benefício financeiro, e talvez mais importante, ajustará a precedencia da prioridade, estabelecendo uma hierarquia clara dos interesses entre as instituições da ciência e medicina e distante do menos poderoso povos que confiam nestas instituições para seu bem estar.</p>
<p>PIC do título. Através de: <a href="http://forward.com/articles/112419/plaintiffs-in-breast-cancer-gene-suit-hope-to-over/" class="uri">http://forward.com/articles/112419/plaintiffs-in-breast-cancer-gene-suit-hope-to-over/</a>.</p>
</blockquote>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>none</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
