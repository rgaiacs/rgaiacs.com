---
layout: post
title: "MathML meeting"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>This is a report about the Mozilla MathML IRC Meeting (see the announcement <a href="http://groups.google.com/forum/#!topic/mozilla.dev.tech.mathml/M023MHJ7_hY">here</a>).</p>
<p>The meeting was good to break the silence in <code>#mathml</code> channel, meet some folks that work with MathML and start talking about regular virtual meetings and the MathML development.</p>
<div class="note">
<div class="admonition-title">
<p>Note</p>
</div>
<p>We use <a href="https://etherpad.mozilla.org/1z2lJKVZqN">this PAD</a> to list the topics for the meeting (<span data-role="download">backup of the pad here &lt;pad.txt&gt;</span>) and the log of it is available <span data-role="download">here &lt;meeting-log.txt&gt;</span>.</p>
</div>
<div class="more">

</div>
<h1 id="who-has-attend-the-meeting">Who has attend the meeting?</h1>
<p>Just following the channel log:</p>
<dl>
<dt>fscholz</dt>
<dd><p>I'm Florian Scholz, a long time Mozilla contributor to MDN and since October Technical Writer on that same thing. I am the author of MathML reference material on ... MDN! :) Based in Bremen, Germany.</p>
</dd>
<dt>qheaden</dt>
<dd><p>I'm Quentin Headen. I'm a current CS student and contributor to Mozilla projects including MathML and Instantbird.</p>
</dd>
<dt>teoli</dt>
<dd><p>I'm Jean-Yves Perrier, long time contributor (MDN) and Technical Writer on MDN. Try to make sure that MathML new features are relnoted and added to Fx XY for developers.</p>
</dd>
<dt>roc</dt>
<dd><p>I'm Robert O'Callahan, (very) longtime Gecko layout/graphics/media developer and Web standards contributor</p>
</dd>
<dt>distler</dt>
<dd><p>I'm Jacques Distler, Professor of Physics at the University of Texas at Austin. In my copious free time, I write software which helps people put MathML on the web.</p>
</dd>
<dt>raniere</dt>
<dd><p>I'm Raniere Silva, a apply math undegraduate student from Brazil and keeping in touch with Mozilla Science Lab.</p>
</dd>
<dt>WG9s</dt>
<dd><p>I am Bill Gianopoulos. I got involved in MathML because when Incremental reflow landed it completely broke the old MathML cade and it kind of had to be rewritten. THis troubled me since MathML was a big differntiator for Gecko versus other browser engines. So I started by mostly helping test pending patches, etc. I am based in Massachusetts, US.</p>
</dd>
<dt>karl</dt>
<dd><p>i work for Mozilla on Gecko. I did some work on MathML in Gecko a few years ago. I have limited time to spend on MathML now, but do reviews (even if i may take time getting to them, sorry)</p>
</dd>
<dt>fredw</dt>
<dd><p>the mathml superstar who we all know :)</p>
</dd>
<dt>Jesse</dt>
<dd><p>i'm Jesse Ruderman. i write randomized-testing programs (&quot;fuzzers&quot;) to find bugs in gecko</p>
</dd>
<dt>bruce</dt>
<dd><p>Hi all; I'm Bruce Miller, a member of the Math WG (W3C); developer of LaTeXML (LaTeX to XML (&amp; MathML, HTML...) converter); developer of DLMF (<a href="http://dlmf.nist.gov/" class="uri">http://dlmf.nist.gov/</a>)</p>
</dd>
<dt>davidc</dt>
<dd><p>I'm David Carlisle, editor of MathML spec and one of the developers/maintainers of latex2e</p>
</dd>
<dt>jkitch</dt>
<dd><p>I'm James Kitchener, I work on fixing MathML bugs within Firefox.</p>
</dd>
</dl>
<h1 id="external-communication">External Communication</h1>
<p>fredw say that</p>
<blockquote>
<p>So basically last October, Florian, Bill &amp; I ran Mozilla MathML booths at Santa Clara / Brussels. And we realized that many people are interested in Education/Science on the Web But while we got positive feedback on the MathML project we noticed that most people have never really used MathML (or even heard about it).</p>
</blockquote>
<p>I suggest a blog aggregator (e.g. <a href="http://www.planetplanet.org/">planet</a> and <a href="http://intertwingly.net/code/venus/">venus</a>, the second one suggested by distler). Happens that such aggregator &quot;already&quot; exist: <a href="http://www.w3.org/Math/planet/" class="uri">http://www.w3.org/Math/planet/</a> (it is host by W3C).</p>
<p>fscholz said that we could have guest post on <a href="https://hacks.mozilla.org/">Mozilla Hacks</a> to get more viewers. The current editor of Hacks is <a href="https://hacks.mozilla.org/author/robertnyman/">Robert Nyman</a> and someone could send one email about writing a post about what is possible with MathML</p>
<h1 id="internal-communication">Internal Communication</h1>
<p>Our communication is spliced in the IRC channel, mail list, bugzilla, MDN (Mozilla Developer Network) and MozillaWiki. Except for the IRC channel a new contributor can find our activity in the previous months.</p>
<div class="note">
<div class="admonition-title">
<p>Note</p>
</div>
<p>davidc asked if there is a public log of the channel.</p>
</div>
<p>fscholz is looking for a event about MathML and there is two options now: Remo sponsored event and Doc sprint / hackathon. The best choice isn't clear yet.</p>
<h1 id="projects-using-mathml">Projects Using MathML</h1>
<div class="note">
<div class="admonition-title">
<p>Note</p>
</div>
<p>As fredw say &quot;Anybody using MathJax uses MathML&quot;.</p>
</div>
<p>Wikipedia plans on using MathML but still have some bugs before it goes on production. It will be using MathJax for browser that not support MathML but the edition still be in TeX. <a href="http://www.mediawiki.org/wiki/User:Jiabao_wu/GSoC_2013_Project_Work#VisualEditor_Mathematical_Equations_Plugin">There is a GSOC project for a visual editor</a>. .</p>
<p><a href="http://www.instiki.org/show/HomePage">Instiki</a> is another wiki engine that support Mathml.</p>
<div class="note">
<div class="admonition-title">
<p>Note</p>
</div>
<p>distler wrote all papers with Instiki. The last one can be found at <a href="http://arxiv.org/pdf/1309.2299v1.pdf" class="uri">http://arxiv.org/pdf/1309.2299v1.pdf</a>.</p>
</div>
<h1 id="mozilla-science-lab">Mozilla Science Lab</h1>
<p>Although MathML would fit in the Mozilla Science Lab they are now focus in open journals articles, source codes, databases and not very much in using web standards to do it.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>MathML,Mozilla</p>
</div>
<div class="tags">
<p>MathML meetings</p>
</div>
<div class="comments">

</div>
