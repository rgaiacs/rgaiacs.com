---
layout: post
title: Another Hacking Gaia Tutorial
author: "Raniere Silva"
categories: ftb
tags: [ftb]
image:
  feature: ftb.jpg
---
More than two months ago I had [update a
tutorial](../../03/29/hacking_gaia.html) that [Salvador de la Puente
González wrote about hacking
Gaia](http://unoyunodiez.com/2013/07/26/hacking-gaia-with-firefox-os-simulator/),
the Firefox OS UI. Most of the steps are the same but using Simulator
2.0 is a little different and at this blog post I will cover this
changes.

Step 0: set-up
==============

<div class="admonition">

Note

If you want pictures related with this part, go to the [previous
tutorial](../../03/29/hacking_gaia.html).

</div>

1.  Install Firefox. If you want you can try use the nightly version of
    Firefox but I'm getting a little problem with it, for more
    information see [Bug
    1020520](https://bugzilla.mozilla.org/show_bug.cgi?id=1020520).
2.  Enter `about:app-namager` in the URL bar.
3.  Click on "Start Simulator at the bottom of the page.
4.  Click on "Add".
5.  Click on "Install Simulator".
6.  Select "Install Firefox OS 2.0 Simulator (unstable)" and follow the
    steps to install the simulator.

Step 1: Download and hack Gaia
==============================

<div class="admonition">

Note

We will use the Keyboad app for example but you can use any other app.

</div>

Using a terminal:

    $ git clone https://github.com/mozilla-b2g/gaia.git
    $ cd gaia

Open `~/gaia/apps/keyboard/js/keyboard.js` with your favorite editor.
Search for "Normal key" and goes to some text like:

    // Normal key

> default:
>
> :   
>
>     if (target.dataset.compositekey) {
>
>     :   // Keys with this attribute set send more than a single
>         character // Like ".com" or "2nd" or (in Catalan) "l·l". var
>         compositeKey = target.dataset.compositekey; for (var i = 0; i
>         &lt; compositeKey.length; i++) {
>         inputMethodManager.currentIMEngine.click(compositeKey.charCodeAt(i));
>         }
>
>     } else { inputMethodManager.currentIMEngine.click(keyCode); }
>     break;
>
Below the `default` keyword, add:

    console.log('Key pressed: ' + keyCode);

Save the file and close your editor. Back to the terminal, compile the
Keyboard App.

> \$ APP=keyboard make

Step 2: Hacking Simulator
=========================

<div class="admonition">

Note

Again, we will use the Keyboad app for example but you can use any other
app.

</div>

The secret of this step is replace the Keyboad app at Simulator with a
symbolic link to our own build of Keyboard app. In another terminal:

    $ cd ~/.mozilla/firefox
    $ cd $(ls | grep default)
    $ ls | grep default
    w7wcv14w.default-1384255853558/
    $ cd extensions/fxos_2_0_simulator@mozilla.org/profile/webapps
    $ rm -rf keyboard.gaiamobile.org
    $ ln -s ~/gaia/profile/webapps/keyboard.gaiamobile.org/

<div class="admonition">

Note

`$(ls | grep default)` will give you the name of your profile folder. If
you have change your profile you will need to replace `default` with
your profile name.

</div>

Step 3: Launch Simulator and Debugging
======================================

Go back to Firefox at the tab with App Manager. In App Manager, select
the "Device" tab.

![](/images/app-manager.png){width="50%"}

Launch Simulator.

![](/images/app-manager-device.png){width="50%"}

Locate the System app and select "DEBUG".

![](/images/app-manager-debug.png){width="50%"}

Go to the Simulator window, open one app that use the keyboard, select a
input field and type any key (I had type 'Q'). After type some key, go
back to Firefox and you will see :

    "Key pressed: 81"

at the end of your log.

![](/images/app-manager-log.png){width="50%"}

Step 4: Continue hacking
========================

Now that you know how to debug Gaia, continue hacking it.
