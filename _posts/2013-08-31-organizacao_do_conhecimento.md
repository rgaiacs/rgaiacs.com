---
layout: post
title: "Organização do conhecimento"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Quando comecei meu curso de inglês em uma escola de idiomas, uma das primeiras coisas que minha professora disse foi &quot;You must try to think in english.&quot;. Lembrei dessa fala dela quando estava lendo o segundo capítulo de &quot;How Learning Works: Seven Research-Based Principles for Smart Teaching&quot; de Susan A. Ambrose et. al. cujo título é &quot;How does the way students organize knowledge effect their learning?&quot; (&quot;Como a forma de organizar o conhecimento afeta o aprendizado?&quot; em tradução literal). Neste post irei falar sobre esse capítulo.</p>
<div class="more">

</div>
<p>Logo no início do capítulo é informado que a forma de organizar o conhecimento influencia o aprendizado e a forma de aplicá-lo. E a forma de organização é diferente entre alunos/novatos e intrutores/veteranos.</p>
<p>Os alunos tende a possui uma organização esparça e superficial enquanto que os instrutores uma organização mais densa e profunda que favorece o aprendizado e aplicação do conhecimento.</p>
<p>Tomando como exemplo o aprendizado de um novo idioma, alguém que apenas foi alfabetizado pode ter uma organização como ilustrado abaixo.</p>
<figure>
<img src="/images/map0.svg" class="align-center" style="width:60.0%" />
</figure>
<p>Ao iniciar o aprendizado do novo idioma, o aluno pode tentar associar as palavras do novo idioma a tradução para o idioma que já conhece (como ilustrado abaixo). Nessa forma de organização, sempre que o aluno precisar de uma palavra na nova língua ele irá precisar encontrar a correspondente no idioma que já é proficiente.</p>
<figure>
<img src="/images/map1.svg" class="align-center" style="width:60.0%" />
</figure>
<p>Alguém que já é fluente no segundo idioma possuirá uma maior número de associação entre as palavras do segundo idioma de forma que será capaz de acessá-las sem ter que utilizar a correspondente no idioma que primeiro aprendeu.</p>
<figure>
<img src="/images/map2.svg" class="align-center" style="width:60.0%" />
</figure>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>educação,ensino,Software Carpentry</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
