---
layout: post
title: "My year as Treasurer for The Carpentries"
author: "Raniere Silva"
categories: report
tags: ["The Carpentries", "Software Carpentry", "Data Carpentry", "Library Carpentry", "Executive Council"]
image:
  feature: 2019-11-07-carpentries-ec.jpg
  author: Bryan Minear
  licence: CC-BY
  original_url: https://unsplash.com/photos/RMePDGG3SU4
---

I have been involved with The Carpentries since 2013
and, during 2019, I was the Treasurer for them.
In December, 2019, The Carpentries will be hosting election for the next Executive Council
and until November 24, 2019, members of the community can stand up for election.
Details about the election and how to stand for election are available
on [The Carpentries' blog](https://carpentries.org/blog/2019/10/executive-council-elections-2019/).
In this post,
I want to describe my year as member of the Executive Council
and motivate you to stand up for election
and shape the future of The Carpentries.

## Work as Executive Council Member

During 2019,
the Executive Council met monthly for one hour.
The time of the meeting was choose with the help of a [Doodle poll](https://doodle.com/)
to accommodate all members requirements.
We avoid change the time of the meeting
but we did it twice over the year
due daylight/summer time and new school term.
Each meeting requires at least 30 minutes to get familiar with the agenda
that is always made available the week before.
The agenda is composed of topics submited by Executive Council members
and the Executive Director.

The Executive Council also met in-person once during the year for two days.
Travel and accommodation costs are covered by The Carpentries.
The goal of the in-person meeting is to make progress in important topics,
for example, strategic plan.

## Work as Treasurer

The Treasurer’s responsibilities are to:

- Assist the Executive Director in preparing financial reports for both the Executive Council and the community at large.
- Assist the Executive Director in preparing the annual budget and presenting the budget to the Executive Council for approval.
- Ensure that an up-to-date report on the status of accounts can be given at each Council meeting.

To keep up with this responsibilities,
the Administrative Support Specialist,
the Executive Director
and I
met once per month for a 30 minutes meeting to review the monthly financial report,
this meeting always happen the week before the Executive Council monthly meeting.
The updated financial books were sent to me a week in advance of our meeting
and I spent, in average, 10 minutes to review the new information in the books.

Also,
I met a few times with the Director of Membership to discuss the annual budget
given that membership is part of the income for The Carpentries.

## Why be a Executive Council Member?

Members of the Executive Council are **not** paid.
The expectation is that you, as a member,
contribute to shape the future of the organisation.
And you can learn during your contribution,
I learnt a lot when reviewing the financial books.