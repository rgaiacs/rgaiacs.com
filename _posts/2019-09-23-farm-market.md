---
layout: post
title: "Oranjezicht City Farm-Market"
author: "Raniere Silva"
categories: travel
tags: ["Africa", "South Africa", "Cape Town"]
image:
  feature: 2019-09-23-farm-market.jpg
  author:  Raniere Silva
  licence: CC-BY
---

I spent my last day in Cape Town visiting the [Oranjezicht City Farm-Market](https://g.page/OZCFarmMarket?share).
The place is magic and I find a great vegan plate.

{% include figure.html filename="2019-09-23-farm-market-2.jpg" alternative_text="Photo of Oranjezicht City Farm-Market" caption="Oranjezicht City Farm-Market." author="Raniere Silva" licence="CC-BY" %}

{% include figure.html filename="2019-09-23-farm-market-3.jpg" alternative_text="Photo of Oranjezicht City Farm-Market" caption="Oranjezicht City Farm-Market." author="Raniere Silva" licence="CC-BY" %}

{% include figure.html filename="2019-09-23-farm-market-4.jpg" alternative_text="Photo of Oranjezicht City Farm-Market" caption="Oranjezicht City Farm-Market." author="Raniere Silva" licence="CC-BY" %}