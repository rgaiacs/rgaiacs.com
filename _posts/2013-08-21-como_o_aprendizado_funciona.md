---
layout: post
title: "Como o aprendizado funciona"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Semana passada iniciei o treinamento para instrutor da <a href="http://software-carpentry.org">Software Carpentry</a>, uma ONG cujo objetivo é ajudar cientistas a serem mais produtivos ao fazer melhor uso das tecnologias disponíveis hoje.</p>
<div class="more">

</div>
<p>O treinamento começou através de uma reunião via toll-free (o nome dado para o 0800 nos Estados Unidos e Canadá) na qual Greg falou como o treinamento funciona, os objetivos dos mesmos e o material (&quot;How Learning Works: Seven Research-Based Principles for Smart Teaching&quot; de Susan A. Ambrose et. al.).</p>
<p>Dessa primeira reunião aproveitei, dentre outras coisas:</p>
<ol type="1">
<li><p>As definições de novato, competente e experiente em alguma ferramenta/software.</p>
<p>Novato é alguém que nunca utilizou o software. Ele não sabe o que o software faz (para que serve), suas potencialidades e limitações, onde encontrar ajuda, ...</p>
<p>Competente é alguém que já utilizou o software várias vezes. Ele sabe para que serve o software e onde encontrar ajuda. Se tiver que repetir uma tarefa previamente realizada irá executá-la muito rapidamente mas para uma tarefa nova provavelmente precisará procurar por ajuda.</p>
<p>Experiente é alguém que já utilizou o software por meses/anos. Além de conhecer o software, ele conhece várias de suas possibilidades e limitações. Saberá responder quase qualquer pergunta que alguém possa fazer ou descobrí-la por conta própria.</p>
<p>O objetivo dos instrutores é transformar novatos em competentes pois isso pode ser alcançado em poucas dezenas de horas (para transformar um competente em experiente é necessário milhares de horas).</p></li>
<li><p>A proposta de utilizar pad durante aulas.</p>
<p>Para quem não sabe um pad é um documento (de texto puro) em um servidor rodando o <a href="http://etherpad.org/">Ehterpad</a>. Um pad pode ser editado em tempo real por vários usuários que não precisam criar conta em nenhum serviço e a identificação é facultativa através de um apelido.</p>
<p>Como foi dito na reunião, o pad pode ser utilizado tanto como notas (colaborativas) da aula como também para perguntas e respostas daqueles que são muito tímidos para fazê-las verbalmente.</p>
<p>O único problema do pad é uma possível sobrecarga de informação nos alunos.</p></li>
</ol>
<p>Esse treinamento irá durar aproximadamente 8 semanas de forma que nesse periodo devo escrever mais sobre o mesmo.</p>
<p>Maiores informações sobre o treinamento encontra-se disponível <a href="http://teaching.software-carpentry.org/2013/08/18/round-6-1-concept-maps/">aqui</a> (<span data-role="download">cópia nesse servidor &lt;swc61.html&gt;</span>).</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>educação,ensino,Software Carpentry</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
