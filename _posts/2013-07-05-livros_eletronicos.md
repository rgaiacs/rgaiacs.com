---
layout: post
title: "Livros eletrônicos iterativos com HTML5 e ePUB3"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Essa palestra foi apresentada pelo Jomar Silva (ou HomemBit) e foi baseada nos resultados de um hackaton promovido pela Intel no primeiro semestre desse ano.</p>
<figure>
<img src="/images/livros_eletronicos_0.jpg" class="align-center" style="width:80.0%" />
</figure>
<p>Na primeira parte da palestra o Jomar falou um pouco sobre formatos de livros digitais dando maiores detalhes do ePUB3.</p>
<figure>
<img src="/images/livros_eletronicos_1.jpg" class="align-center" style="width:80.0%" />
</figure>
<p>Na segunda parte ele mostrou o exemplo desenvolvido durante o hackaton que é uma aplicação web para auxiliar no ensino de equações de primeiro e segundo grau e da fórmula de Baskara.</p>
<p>A aplicação desenvolvida pode ser incluída em um livro eletrônico no formato ePUB3 sem perda de funcionalidade.</p>
<p>Como o Jomar deixou bem claro em sua palestra, a grande vantagem do ePUB3 sobre páginas webs e aplicativos nativos é sua distribuição.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>FISL</p>
</div>
<div class="tags">
<p>FISL14</p>
</div>
<div class="comments">

</div>
