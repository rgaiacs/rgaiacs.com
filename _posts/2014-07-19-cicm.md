---
layout: post
title: "MathUI 2014 - 9th Workshop on Mathematical User Interfaces"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>This is a report about <a href="http://cermat.org/events/MathUI/14/">MathUI 2014</a>.</p>
<div class="admonition">
<p>Note</p>
<p>Thanks very much to <a href="https://kwarc.info/node/12024">Andrea Kohlhase</a> for the photos.</p>
</div>
<figure>
<img src="/images/cicm0.jpg" class="align-center" style="width:50.0%" />
</figure>
<div class="more">

</div>
<h1 id="firefox-os">Firefox OS</h1>
<figure>
<img src="/images/cicm1.jpg" class="align-center" style="width:50.0%" />
</figure>
<p>I and Frédéric gave a <a href="http://fred-wang.github.io/MathUI2014/">talk</a> about some of HTML(5) features support by Gecko that can be used when developing math resources. All the features cover by our talk can be used at Firefox OS (and Firefox for Android) what makes Firefox one nice platform to develop math resources.</p>
<p>At MathUI, Firefox OS was also mention by <a href="http://ceur-ws.org/Vol-1186/paper-05.pdf">Paul Libbrecht</a>.</p>
<h1 id="demo-session">Demo Session</h1>
<p>MathUI had a demo session where the only demo available was the ones that I and Frédéric wrote. People liked the math support at Firefox OS but had complains about some missing features of the platform (e.g. can't select text at apps neither at input boxes).</p>
<p>I already know about the missing features before the demo session and that Mozilla is working to implement it. Unfortunately this missing features makes hard convince someone to adopt Firefox OS.</p>
<h1 id="others-talks">Others talks</h1>
<p>MathUI also had some awesome talks with suggestions of complex demos that could be used to show that Firefox OS is the best platform for science. Some of this demos are:</p>
<ul>
<li>search engine for formula in a give page/app (related with <a href="http://ceur-ws.org/Vol-1186/paper-02.pdf">Design of Search Interfaces for Mathematicians</a> by Andrea Kohlhase, <a href="http://cicm-conference.org/2014/slides/SchubotzCicmDp.pdf">Content Based Formula Search</a> by Moritz Schubotz and <a href="http://link.springer.com/chapter/10.1007/978-3-319-08434-3_12">Search Interfaces for Mathematicians</a> by Andrea Kohlhase.</li>
<li>math communication tool (related with <a href="http://ceur-ws.org/Vol-1186/paper-07.pdf">Towards a Universal Interface for Real-Time Mathematical Communication (online)</a> by Marco Pollanen, Jeff Hooper, Bruce Cater and Sohee Kang.</li>
<li>math exploration (related with <a href="http://ceur-ws.org/Vol-1186/paper-04.pdf">Developing visualisations for spreadsheet formulae: towards increasing the accessibility of science, technology, engineering and maths subjects</a> by Roxanne Leitão and Chris Roast.</li>
</ul>
<p>And the keynote of <a href="http://link.springer.com/chapter/10.1007/978-3-319-08434-3_1">Jaime Carvalho e Silva</a> was also inspiring.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>MathML</p>
</div>
<div class="tags">
<p>MathUI2014</p>
</div>
<div class="comments">

</div>
