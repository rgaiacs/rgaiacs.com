---
layout: post
title: ShareLaTeX and GitHub
author: "Raniere Silva"
categories: ftb
tags: [ftb]
image:
  feature: ftb.jpg
---
<div class="admonition">

Update

This blog post was update in January 20th with excerpts from a email
that [James Allen](https://twitter.com/thejpallen) send to me.

</div>

In this post I will test the bridge between
[ShareLaTeX](https://www.sharelatex.com/) and
[GitHub](http://github.com/) focusing at dealing with conflicts.

Create Repository at GitHub
===========================

The first step was create a Git repository at GitHub to use for this
test.

![](/images/github-create-repo.jpg){width="80%"}

If you already used GitHub this should be easy. I created
<https://github.com/r-gaia-cs/testing-sharelatex> and added
<https://github.com/abelsiqueira/abelsiqueira.github.io/blob/master/disciplinas/topal2015/ex2015-01-06.tex>
to it.

Import Repository at ShareLaTeX
===============================

The second step was import the Git repository at ShareLaTeX.

![](/images/sharelatex-import.jpg){width="80%"}

Clicking on "New Project" you will find "Import from GitHub".

<div class="admonition">

Note

This option is only available for [paid
subscriptions](https://www.sharelatex.com/user/subscription/plans).

</div>

![](/images/sharelatex-auth.jpg){width="80%"}

The first time that you use this option you will need to authorise
ShareLaTeX to access your GitHub account.

![](/images/github-auth.jpg){width="80%"}

After you authorise ShareLaTeX to access your GitHub account you can
click **again** on "New Project" and "Import from GitHub". You will find
a list of all your GitHub repositories and just have to select the one
you want to import.

![](/images/sharelatex-add.jpg){width="80%"}

After import your project you can edit it using ShareLaTeX's web IDE.

![](/images/sharelatex-ide.jpg){width="80%"}

Create New Revision on ShareLaTeX
=================================

To create a new revision on ShareLaTeX you need to:

1.  Open the menu of the web IDE (at the top left corner).

![](/images/sharelatex-menu.jpg){width="80%"}

2.  Select "GitHub".

![](/images/sharelatex-push.jpg){width="80%"}

3.  Select "Push ShareLaTeX changes to GitHub".

![](/images/sharelatex-commit.jpg){width="80%"}

4.  Write the commit message.

And that's all. You can download the new revision to your local machine
as you always do, e.g. `git fetch` or `git pull`.

<div class="admonition">

BUG

If you try to create a new revison without change any file Git will not
allow you to do that:

    $ git commit -m 'Without change'
    On branch master
    Your branch is up-to-date with 'origin/master'.
    nothing added to commit but untracked files present

For some reason GitHub allow a new revision without any change:

    $ git show HEAD
    commit 30f4a3abe26bb79e662acf3c965b6332ddf56f71
    Author: Raniere Silva <raniere@ime.unicamp.br>
    Date:   Tue Jan 20 00:53:26 2015 -0200

        Empty commit

**James informed me that:**

> The offer to 'push' even though there aren't any changes is simply
> because we don't have a simple way to detect if any changes have
> actually been made, at least not without adding a lot of complexity to
> our backend.
>
> So we made the call to get something out that worked and was useful to
> people. Even though these things can create an odd user experience in
> certain cases, they hopefully won't stop it being usable.

</div>

Import New Revision
===================

<div class="admonition">

Note

Here I will cover the case that has a conflict since it is the generic
case.

</div>

To update your project on ShareLaTeX you need to:

1.  Open the menu of the web IDE (at the top left corner).

![](/images/sharelatex-menu.jpg){width="80%"}

2.  Select "GitHub".

![](/images/sharelatex-pull.jpg){width="80%"}

3.  Select "Pull GitHub changes into ShareLaTeX".

![](/images/sharelatex-conflict.jpg){width="80%"}

4.  Manually merge the revisions. **This can't be done at the web IDE.**
5.  Select "I have manually merged. Continue".

<div class="admonition">

BUG

ShareLaTeX isn't smart enough to check if you realy fix the conflict and
merge the versions. If you say that you merged the changes he will
believe in you and use the version from GitHub without changes. **Yes,
you will "lose" the work that you do at the web IDE.**

**James informed me that:**

> When it comes to overwriting changes made in ShareLaTeX, these will
> still be available in the track changes (we version every change to a
> document in ShareLaTeX ourselves as well), and can be rolled back if a
> mistake happens with the manual merge.

To get the change track made by ShareLaTeX you need to select the
"Recent Changes" option at the top left corner of the web IDE.

![](/images/sharelatex-changes.jpg){width="80%"}

And if you wish you can restore your document.

</div>

6.  Write the commit message.

<div class="admonition">

BUG

For some reason, after pull the changes ShareLaTeX ask you to push the
changes. **This don't create any aditional commit.**

</div>

Conclusions
===========

<div class="admonition">

Note

**James' "final words" about the bugs was:**

> The bugs you mention are spot on, and something we're aware of. There
> are both there by design, at least in the sense that we had to make
> some compromises given the features available in the GitHub API, and
> to stop the complexity spiralling out of control.

</div>

Have this bridge is a nice feature but in my opinion this will not solve
the problem of collaborating with someone that don't know Git because
many times it require to resolve the conflicts locally and allow user to
lie about had resolve the conflict in a way that looks like lose the
work done.

Since this feature still in beta I hope that ShareLaTeX team improve it
in the sort future.
