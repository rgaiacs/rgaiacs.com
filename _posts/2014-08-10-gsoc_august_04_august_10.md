---
layout: post
title: "GSoC: August 04 - August 10"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>This is last but one of the reports about my GSoC project and cover the twelfth week of “Students coding”.</p>
<p>At this twelfth week I worked to have the keyboard available at Marketplace and small improvements..</p>
<p>Bellow you will find more details about the past week and the plans for the last one.</p>
<div class="more">

</div>
<h1 id="marketplace">Marketplace</h1>
<figure>
<img src="/images/latex-keyboard.svg" class="align-center" style="width:25.0%" />
</figure>
<p>To make the keyboard available at Marketplace I will use the <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=1029539">script that I previous wrote</a> with the patch wrote by Salvador de la Puente González that makes the keyboard one <strong>privileged</strong> instead of <strong>certified</strong>. I sent a &quot;draft&quot; version to get a previous review from Marketplace reviewer team.</p>
<h1 id="improve-compositekeyclick">Improve compositeKeyClick</h1>
<p>I made <a href="https://github.com/r-gaia-cs/gaia/commit/148697ca2233e08d52ddd4049048a7da5a285bc5">small improvements</a> at the <code>compositeKeyClick</code> function that now works with delimiters.</p>
<h1 id="improve-jump">Improve jump</h1>
<p>I made <a href="https://github.com/r-gaia-cs/gaia/commit/7a686d6dd30064fe93e064302155ef7a0a72d0c2">some changes</a> at the <code>handleReturn</code> function that now works with delimiters:</p>
<ul>
<li><p>commands:</p>
<pre><code>\command[▮]{}
\command[]{▮}
\command[]{}▮</code></pre></li>
<li><p>(commands) delimiters:</p>
<pre><code>\left▮\right
\left\right▮</code></pre></li>
<li><p>environments:</p>
<pre><code>\begin{environment}[▮]\end{environment}
\begin{environment}[]▮\end{environment}
\begin{environment}[]\end{environment}▮</code></pre></li>
<li><p>vertical bars:</p>
<pre><code>|▮|
||▮</code></pre></li>
<li><p>double vertical bars:</p>
<pre><code>\|▮\|
\|\|▮</code></pre></li>
<li><p>subscript and superscript:</p>
<pre><code>{▮}_{}^{}
{}_{▮}^{}
{}_{}^{▮}
{}_{}^{}▮</code></pre></li>
<li><p>underset and overset:</p>
<pre><code>\underset{▮}{\overset{}{}}
\underset{}{\overset{▮}{}}
\underset{}{\overset{}{▮}}
\underset{}{\overset{}{}}▮</code></pre></li>
</ul>
<h1 id="add-auto-capitalization">Add auto capitalization</h1>
<p>One of the features of the native keyboard is the auto capitalization. This is one feature that I didn't include in my proposal my want to include as the last feature of my GSoC project. I <a href="https://github.com/r-gaia-cs/gaia/commit/03e463e7d9f1ab23e793ada4d37f5ad391340d18">implemented this feature</a> but there is a small bug that I still need to fix.</p>
<h1 id="plans-for-august-11---august-17">Plans for August 11 - August 17</h1>
<p>And for the last week before pencils down I'm planning to</p>
<ul>
<li>fix the bug at auto capitalization,</li>
<li>send the final version to Marketplace and</li>
<li>wrote some documentation.</li>
</ul>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>GSOC</p>
</div>
<div class="tags">
<p>GSOC2014</p>
</div>
<div class="comments">

</div>
