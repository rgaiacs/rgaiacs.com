---
layout: post
title: "First Release of Math Virtual Keyboard"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Although there still many important features to add and minor fixes I would like to make available the first &quot;all-in-one&quot; prototype of the math virtual keyboard.</p>
<div class="more">

</div>
<h1 id="extra-patch">Extra Patch</h1>
<p>Last week I wrote about the <span data-role="doc">prototype that use the IME Switch Key
&lt;../03/flatfish_and_la_tex&gt;</span> and this &quot;all-in-one&quot; prototype join the previous layouts into one and <a href="https://github.com/r-gaia-cs/gaia/commit/a6ea0d13e3ece405ae6cedd927bfedadc4b82556">add some changes</a> into <code>apps/keyboard/js/keyboard.js</code> that request some conversation with Keyboard App owners and Firefox OS UX team.</p>
<h1 id="how-to-test">How to Test</h1>
<p>Clone my fork of Gaia and move to <code>gsoc-v1</code> tag. :</p>
<pre><code>$ git clone https://github.com/r-gaia-cs/gaia.git
$ cd gaia
$ git checkout gsoc-v1</code></pre>
<p>With the device connect and recognize:</p>
<pre><code>$ GAIA_KEYBOARD_LAYOUTS=en,latex PRODUCTION=1 NOFTU=1 REMOTE_DEBUGER=1 make reset-gaia</code></pre>
<div class="admonition">
<p>Note</p>
<p>If you are using Flatfish, you need to prepend <code>GAIA_DEVICE_TYPE=tablet</code> at the last command and <a href="https://github.com/mozilla-b2g/gaia/pull/20113">disable the vertical homescreen</a>.</p>
</div>
<h1 id="screenshots">Screenshots</h1>
<figure>
<img src="/images/default.png" alt="Screenshot QUERTY layout." class="align-center" style="width:50.0%" /><figcaption>Screenshot QUERTY layout.</figcaption>
</figure>
<figure>
<img src="/images/symbols.png" alt="Screenshot of symbols layout." class="align-center" style="width:50.0%" /><figcaption>Screenshot of symbols layout.</figcaption>
</figure>
<figure>
<img src="/images/greek.png" alt="Screenshot of greek letters layoyt." class="align-center" style="width:50.0%" /><figcaption>Screenshot of greek letters layoyt.</figcaption>
</figure>
<figure>
<img src="/images/math.png" alt="Screenshot of math symbols." class="align-center" style="width:50.0%" /><figcaption>Screenshot of math symbols.</figcaption>
</figure>
<figure>
<img src="/images/functions.png" alt="Screenshot of math functions." class="align-center" style="width:50.0%" /><figcaption>Screenshot of math functions.</figcaption>
</figure>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>GSOC</p>
</div>
<div class="tags">
<p>GSOC2014</p>
</div>
<div class="comments">

</div>
