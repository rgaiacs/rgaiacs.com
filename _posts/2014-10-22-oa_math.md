---
layout: post
title: "The Status of Math in Open Access"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>This week, October 20–26, 2014, held the <a href="http://www.openaccessweek.org/">Open Access Week</a> (<a href="http://www.sparc.arl.org/news/2014-OA-Week-Kickoff">see the announcement</a>) and will end with <a href="http://2014.mozillafestival.org/">Mozilla Festival</a> that has a <a href="http://mozillascience.org/mozfest-2014/">awesome science track</a>. This post has some thoughts about math and open access and this two events.</p>
<div class="more">

</div>
<h1 id="open-access-and-math">Open Access and Math</h1>
<p>From <a href="https://en.wikisource.org/wiki/Budapest_Open_Access_Initiative">Budapest Open Access Initiative</a> we have</p>
<blockquote>
<p>&quot;By &quot;open access&quot; to this literature, we mean its free availability on the public internet, permitting any users to read, download, copy, distribute, print, search, or link to the full texts of these articles, crawl them for indexing, pass them as data to software, or use them for any other lawful purpose, without financial, legal, or technical barriers other than those inseparable from gaining access to the internet itself. The only constraint on reproduction and distribution, and the only role for copyright in this domain, should be to give authors control over the integrity of their work and the right to be properly acknowledged and cited.&quot;</p>
</blockquote>
<p>Unfortunately, right now, any open access math resource don't fulfill the Budapest definition because sometime users can't read or search or pass math as data to software due <strong>technical barriers</strong>. Continue to read to understand this.</p>
<h1 id="devices">Devices</h1>
<p>Users should be capable of read math regardless of their device. Did you try read math in a small device?</p>
<figure>
<img src="/images/small-screen.png" alt="Screenshot of math on small screen." class="align-center" /><figcaption>Screenshot of math on small screen.</figcaption>
</figure>
<p>In some cases math equations are bigger than the screen and the device doesn't break the equation across lines neither allow user to scroll or zoom.</p>
<h1 id="math-as-images">Math as Images</h1>
<p>Today, most of the time that you find math on the web it is a bitmap image (think of a JPG file). This doesn't help open access because you can't <strong>remix</strong>, <strong>search</strong> or <strong>pass the equation to software</strong>. If you have some visual disability you also can't <strong>read</strong> the equation.</p>
<p>There is a solution? Yes, <a href="http://www.w3.org/mathml">MathML</a>. MathML is a W3C standard design to make possible <strong>remix</strong>, <strong>search</strong> and <strong>machine readble</strong> (what make possible support solutions for people with visual disability).</p>
<h1 id="mathml-authoring-tools">MathML Authoring Tools</h1>
<p>We already have a <a href="http://www.w3.org/Math/Software/mathml_software_cat_authoring.html">long list of authoring tools that support MathML output</a>. Targeting open access I want to highlight <a href="http://johnmacfarlane.net/pandoc/">pandoc</a>, that can convert from many formats to HTML+MathML, and <a href="https://github.com/brucemiller/LaTeXML">LaTeXML</a>, that convert LaTeX to HTML+MathML and support many of LaTeX packages.</p>
<h1 id="mathml-reading-tools">MathML Reading Tools</h1>
<p>Tools to render MathML is the biggest bottle neck for MathML adoption. The engine used by Firefox and Chrome have support for MathML (<strong>Chrome isn't ship with MathML support</strong>) but Gecko, Firefox's engine, still needs to support part of W3C specification (<a href="http://www.w3.org/TR/MathML3/chapter3.html#presm.linebreaking">linebreaking of expressions</a> and <a href="http://www.w3.org/TR/MathML3/chapter3.html#presm.elementary">elemetary math</a>) and WebKit, Chrome's engine, needs lots of improvements and, also, support part of W3C specification.</p>
<figure>
<img src="/images/mathml-on-firefox.jpg" alt="Screenshot of MathML render by Gecko on Firefox." class="align-center" /><figcaption>Screenshot of MathML render by Gecko on Firefox.</figcaption>
</figure>
<figure>
<img src="/images/mathml-on-epiphany.jpg" alt="Screenshot of MathML render by Webkit on Epiphany." class="align-center" /><figcaption>Screenshot of MathML render by Webkit on Epiphany.</figcaption>
</figure>
<div class="admonition">
<p>Note</p>
<p>Most of, if not all, the implementation of MathML support in Gecko and WebKit was done by volunteers <a href="http://www.ulule.com/mathematics-ebooks/">that sometimes are luck to successed with crowd founding projects to push MathML support</a>.</p>
</div>
<h1 id="mathml-on-open-access-week-and-mozfest">MathML on Open Access Week and MozFest</h1>
<p>I'm a little sad that I didn't see anyone talking about MathML during Open Access Week and, sometimes, the conversation in Open Access groups only focus licenses and paywalls.</p>
<p>Regardless MozFest, I also didn't see any proposal related with MathML but I hope to hear something from the <a href="https://etherpad.mozilla.org/mozscience-authoringtools">authoring tool section</a>.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>academia,ciência aberta,educação,MathML,Mozilla,Mozilla Science Lab,REA,Webmaker</p>
</div>
<div class="tags">
<p>ereader,LaTeX,LaTeXML,pandoc,Open Access Week 2014,MozFest 2014</p>
</div>
<div class="comments">

</div>
