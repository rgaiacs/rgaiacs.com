---
layout: post
title: "REA e uso não comercial"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Na sexta-feira e sábado passados, 07/06/2013 e 08/06/2013, participei do <a href="http://www.casanexocultural.com.br/">Encontro pelo Conhecimento Livre</a> que foi muito bom. Um dos temas tratados no encontro foi os recursos educacionais abertos (REA) e alguns dos problemas do Projeto de Lei 1513/2011.</p>
<div class="more">

</div>
<p>No PL1513/2011 temos</p>
<blockquote>
<p>Art. 2º Para os fins desta lei entende-se por:</p>
<p>I – Recursos Educacionais: Entende-se como Recursos Educacionais os obras a serem utilizados para fins educacionais, pedagógicos e científicos, como livros e materiais didáticos complementares, objetos educacionais, multimídia, jogos, teses e dissertações, artigos científicos e acadêmicos, entre outros;</p>
<p>III – Licença Livre: Entende-se como licença livre a licença de direito autoral ou de software que permita que terceir os usufruam de direitos patrimoniais sobre certa obra como, especificamente, o direito de cópia, distribuição, transmissão, publicação, retransmissão, criação de obras derivadas, desde que:</p>
<p>§1. preservado o direito de atribuicao do autor, especificamente, o direito a ter seu nome, pseudônimo ou sinal convencional do autor vinculado e citado;</p>
<p>§2. a utilização não seja intencionada ou direcionada à obtenção de vantagem comercial ou compensação monetária privada diretas;</p>
<p>§3. as obras derivadas sejam licenciadas sob a mesma licença que a obra original.</p>
</blockquote>
<p>No Artigo 2º, Seção I é incluído software, por meio do uso das palavras &quot;multimídia&quot; e &quot;jogos&quot;, como um recurso educacional.</p>
<p>No Artigo 2º, Seção III, Parágrafo 2 é considerado como licença livre apenas aquelas que obrigam o uso não comercial da obra.</p>
<p>Primeiramente, é importante destacar que essa definição de licença livre não está de acordo com a definição mundialmente aceita, para software, que é publicada pela <a href="http://www.fsf.org">Free Software Foundation</a> uma vez feita a limitação a uso não comercial.</p>
<p>Em segundo lugar, é importante deixar claro que a limitação a uso não comercial cria um entrave para a divulgação, disseminação e aperfeiçoamento dos recursos educacionais abertos. A seguir ilustramos algumas dos possíveis entraves:</p>
<ol>
<li>A impressão de livros por uma gráfica regional para uso por alunos da mesma região seria inviável pois a gráfica teria que vender os livros a preço de custo.</li>
<li>O transporte de livros por uma transportadora para uma região remota para uso por alunos desta região seria inviável pois a transportadora teria que prestar o serviço a preço de custo.</li>
<li>Um fabricante/montador/vendedor/reparador de computadores não poderia fornecer seus produtos previamente configurados com algum desses recursos educacionais pois o serviço prestado visa o lucro.</li>
<li>Uma escola/cursinho particular não poderia utilizar nem aperfeiçoar um recurso pois a atividade exercida visa o lucro.</li>
<li>Nenhum desenvolvedor poderia dar consultoria/suporte para nenhum destes recursos pois sua atividade visa o lucro.</li>
</ol>
<p>Estou convencido de que a restrição a usos comerciais dos recursos eduacionais abertos será muito prejudicial por impossibilitar várias atividades (algumas listadas acima).</p>
<div class="note">
<div class="admonition-title">
<p>Note</p>
</div>
<p>Alguns meses atrás <a href="http://blog.ninapaley.com/">Nina Paley</a> foi questionada sobre o uso da cláusula &quot;não comercial&quot; das licenças Creative Commons. A resposta dela pode ser encontrada <a href="http://baixacultura.org/2013/03/11/faca-arte-nao-leis-entrevista-com-nina-paley/">em português</a> (<span data-role="download">cópia local &lt;faca_arte_nao_leis.html&gt;</span>) ou <a href="http://questioncopyright.org/make_art_not_law">em inglês</a> (<span data-role="download">cópia local
&lt;make_art_not_law.html&gt;</span>).</p>
</div>
<div class="note">
<div class="admonition-title">
<p>Note</p>
</div>
<p>Algumas semanas atrás <a href="http://cecm.usp.br/~eris/">Alexandre Abdo</a> escreveu um artigo para <a href="http://arede.inf.br">ARede</a> também tratando dos problemas da cláusula &quot;não comercial&quot;. O artigo pode ser encontrado <a href="http://arede.inf.br/edicao-n-91-maio-2013/5621-raitequi-libertando-a-forca-do-comum">aqui</a> (<span data-role="download">cópia local &lt;liberando_a_forca_do_comum.html&gt;</span>).</p>
</div>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>none</p>
</div>
<div class="tags">
<p>PL1513/2011</p>
</div>
<div class="comments">

</div>
