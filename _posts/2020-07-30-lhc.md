---
layout: post
title: "Laboratório Hacker de Campinas Convida"
author: "Raniere Silva"
categories: web
tags: ["Encryption"]
image:
  feature: 2020-07-30-lhc.jpg
  author: Anchises
  licence: Twitter
  original_url: https://twitter.com/anchisesbr/status/343481599272292352
---

Estarei falando sobre reproducibilidade
na quinta-feira, às 20:00 UTC-3
na reunião online do Laboratório Hacker de Campinas.
Registre-se pelo [Meetup](https://www.meetup.com/LabHackerCampinas/events/271930565/).