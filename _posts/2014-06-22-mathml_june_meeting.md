---
layout: post
title: "MathML June Meeting"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<div class="admonition">
<p>Note</p>
<p>Sorry for the delay in write this.</p>
</div>
<p>This is a report about the Mozilla MathML June IRC Meeting (see the announcement <a href="https://groups.google.com/forum/#!topic/mozilla.dev.tech.mathml/tp0eI6Vy1RU">here</a>). The topics of the meeting can be found <a href="https://etherpad.mozilla.org/mathml-2014-06">in this PAD</a> (<span data-role="download">local copy of the
PAD &lt;pad.txt&gt;</span>) and the <a href="http://logs.glob.uno/?c=mozilla%23mathml&amp;s=12+Jun+2014&amp;e=12+Jun+2014">IRC log</a> (<span data-role="download">local copy of the IRC log &lt;irc.txt&gt;</span>) is also available.</p>
<p>In the last 4 weeks the MathML team closed 3 bugs, worked in others 4 and open 3 new ones (this are only the ones tracked by Bugzilla).</p>
<p>The next meeting will be in July 17th (the third week of July due MathUI) at #mathml IRC channel. Please add topics in the <a href="https://etherpad.mozilla.org/mathml-2014-07">PAD</a>.</p>
<div class="more">

</div>
<h1 id="mathui-2014">MathUI 2014</h1>
<p>The <a href="http://cermat.org/events/MathUI/14/">MathUI 2014 9th Workshop on Mathematical User Interfaces</a> will happen on July 10th.</p>
<h1 id="science-lab-multi-site-sprint">Science Lab Multi-Site Sprint</h1>
<p>Mozilla Science Lab will run a <a href="http://mozillascience.org/673/">Multi-Site Sprint</a> on July 22-23 and invite everyone to join it. The list of all projects for the sprint are at <a href="https://etherpad.mozilla.org/swc-sprint-2014">this PAD</a> and you can join one or <strong>add a new one</strong>.</p>
<h1 id="math-at-mediawiki">Math at MediaWiki</h1>
<p>Frédéric Wang reviewed Moritz Schubotz's patches for MediaWiki and it is already merge at development branch. There was a problem when testing it on the MediaWiki machines that is been investigate.</p>
<h1 id="firefox-addons">Firefox Addons</h1>
<p>Frédéric Wang add two new addons:</p>
<ul>
<li><a href="https://addons.mozilla.org/en-US/firefox/addon/mathml-font-settings/">MathML Font Settings</a> and</li>
<li><a href="https://addons.mozilla.org/en-US/firefox/addon/mathml-zoom/">MathML Zoom</a>.</li>
</ul>
<h1 id="texzilla-0.9.7">TeXZilla 0.9.7</h1>
<p>Frédéric Wang release a new version of <a href="https://github.com/fred-wang/TeXZilla">TeXZilla</a> that include new features like stream filter and equations to SVG. More information about the new version is available at <a href="http://www.maths-informatique-jeux.com/blog/frederic/?post/2014/06/03/TeXZilla-0.9.7-Released" class="uri">http://www.maths-informatique-jeux.com/blog/frederic/?post/2014/06/03/TeXZilla-0.9.7-Released</a>.</p>
<h1 id="gecko">Gecko</h1>
<p>We had contributions from Frédéric Wang, James Kitchener, Karl Tomlinson and Paul Masson .</p>
<h1 id="documentation">Documentation</h1>
<p>Frédéric Wang add new font instructions for Gecko &gt;= 31 when math table is used.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>MathML,Mozilla</p>
</div>
<div class="tags">
<p>MathML meetings</p>
</div>
<div class="comments">

</div>
