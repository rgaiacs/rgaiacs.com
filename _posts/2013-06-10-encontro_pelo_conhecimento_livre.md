---
layout: post
title: "Encontro pelo Conhecimento Livre"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>O <a href="http://www.cienciaaberta.net/?page_id=13">Encontro pelo Conhecimento Livre</a> ocorreu no dia 07/06/2013 na USP tendo sido transmitida ao vivo pela IPTV da USP. A seguir você encontrará minha opinião sobre as palestras.</p>
<div class="note">
<div class="admonition-title">
<p>Note</p>
</div>
<p>Como este post foi escrito depois do evento ele não encontra-se muito preciso.</p>
</div>
<div class="more">

</div>
<h1 id="acesso-aberto">Acesso aberto</h1>
<p><a href="http://cameronneylon.net/">Cameron Neylon</a> (PLOS, videoconf) falou sobre a experiência da <a href="http://www.plos.org/">PLOS</a>, uma das maiores revistas de acesso aberto da atualidade.</p>
<p><a href="http://www.eca.usp.br/prof/sueli/">Sueli Ferreira</a> (USP) comentou sobre o esforço para a criação dos repositórios institucionais com uso de metadados e o quanto a colaboração entre as universidades paulistas nesse esforço será vantajoso.</p>
<h1 id="ferramentas-científicas-abertas">Ferramentas científicas abertas</h1>
<p><a href="http://meta.wikimedia.org/wiki/Wikimedia_Blog/Drafts/Rafael_Pezzi_profile/en">Rafael Pezzi</a> (UFRGS) abordou a questão de hardware livre partindo da sua experiência no <a href="http://cta.if.ufrgs.br/">Centro de Tecnologia Acadêmica IF-UFRGS</a>.</p>
<p><a href="http://www.ime.usp.br/~kon/">Fabio Kon</a> (CCSL/IME-USP) abordou a questão de software livre partindo de sua experiência com o grupo de software livre do IME-USP e colaborações feitas com outras áreas.</p>
<p><a href="https://twiki.cern.ch/twiki/bin/view/Main/DanielTavares">Daniel Tavares</a> (LNLS) também abordou a questão de hardware livre partindo de sua experiência no <a href="http://www.lnls.br">Laboratório Nacional de Luz Síncrotron</a>.</p>
<h1 id="dados-científicos-abertos">Dados científicos abertos</h1>
<p><a href="http://social.stoa.usp.br/profile/ewout">Ewout ter Haar</a> (USP) falou sobre dados abertos e privacidade (os slides encontram-se disponíveis <a href="http://slid.es/ewout/dados-abertos-e-privacidade">aqui</a>). Como esta questão é bastante delicada creio que foi um dos melhores momentos do evento.</p>
<p><a href="http://www.gpopai.usp.br/wiki/index.php/Usu%C3%A1rio:Jorge">Jorge Machado</a> (USP) comentou sua experiência em pedir a liberação de dados pelo governo utilizando a <a href="http://www.planalto.gov.br/ccivil_03/_ato2011-2014/2011/lei/l12527.htm">Lei de acesso à informação</a> (ou Lei 12.527/2011). Foi dado ênfase no classificação dos periódicos pela CAPES e das informações da plataforma Lattes.</p>
<p><a href="https://uspdigital.usp.br/tycho/CurriculoLattesMostrar?codpub=17A254BA5721">Robson Souza</a> (USP) falou sobre a questão de dados abertos no campo das ciências moleculares que já a bastante tempo possui repositórios púbicos.</p>
<h1 id="educação-aberta">Educação aberta</h1>
<p><a href="http://rea.net.br/site/debora-sebriam/">Debora Sebriam</a> (REA-BR) falou sobre o que são considerados Recursos Educacionais Abertos e o fracasso da Lei Estadual de São Paulo decorrente do veto do governador.</p>
<p><a href="http://pan.nied.unicamp.br/equipe/equipe_detalhes.php?id=44">Tel Amiel</a> (Unicamp) também tratou sobre Recursos Educacionais Abertos de modo a complementar o que foi levantado pela Debora.</p>
<h1 id="ciência-cidadã">Ciência cidadã</h1>
<p><a href="http://www.fau.usp.br/fau/ensino/docentes/deptecnologia/a_rozestraten/index.html">Artur Rozestraten</a> (USP) falou sobre o projeto <a href="http://www.arquigrafia.org.br">Arquigrafia</a> que recuperou slides/fotos antigas da Faculdade de Arquitetura e Urbanismo da USP, tornou este arquivo público e tornou possível que professores, alunos e entusiastas aumentassem o acervo com novas fotos. Se o Arquigrafia colaborasse com o <a href="http://mediagoblin.org/">MediaGoblin</a> iria gostar mais ainda do projeto.</p>
<p><a href="https://garoa.net.br/wiki/Usu%C3%A1rio:Oda">Eduardo Oda</a> (GHC) falou sobre o <a href="https://garoa.net.br">Garoa Hacker Clube</a>, sua atividades, funcionamento, ...</p>
<h1 id="wikipesquisas">Wikipesquisas</h1>
<p><a href="http://cecm.usp.br/~eris/">Alexandre Hannud Abdo</a> (USP) abordou o uso de wikis para a colaboração entre pesquisadores.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>ciência aberta</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
