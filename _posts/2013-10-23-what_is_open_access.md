---
layout: post
title: "What is Open Access"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Estamos na semana do acesso aberto. Para quem não sabe o que significa acesso aberto um bom ponto de partida é <a href="http://youtu.be/L5rVH1KGBCY">esse vídeo</a> (<span data-role="download">cópia nesse servidor &lt;what_is_oa.webm&gt;</span>) que foi publicado no ano passado.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>academia,ciência aberta</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
