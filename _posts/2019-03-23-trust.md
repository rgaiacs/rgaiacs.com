---
layout: post
title: "Trust"
author: "Raniere Silva"
categories: blog
tags: []
image:
  feature: 2019-03-23-trust.jpg
  author: Rachael
  licence: Unsplash License
  original_url: https://unsplash.com/photos/U4zpPfvogJ4
---

I have been [climbing](https://en.wikipedia.org/wiki/Climbing) for more than a year now.
Most of the time I go to [Manchester Climbing Centre](https://manchesterclimbingcentre.com/)
because is a walk distance of my place
and they have awesome [indoor](https://en.wikipedia.org/wiki/Indoor_climbing) [lead climbing](https://en.wikipedia.org/wiki/Lead_climbing) walks.
Unfortunately,
they only have a small [indoor](https://en.wikipedia.org/wiki/Indoor_climbing) [bouldering](https://en.wikipedia.org/wiki/Bouldering) area
which means that if you are starting climbing
you might end up bored because you can only climb a few of the routes,
this was what happen with me when I started.

On Tuesday,
I met someone, Ruby, who was boudering for the first time,
they just had finished the boudering 30 minutes intruction course.
I invited Ruby to climb with my two boudering partners, Tom and Bernard,
and we all had a fun session.

On Wednesday,
I met someone else, Ivan, who was visiting Manchester Climbing Centre due a work week in town.
Ivan is a incredible climber
and I had a very inspiring night.

The rest of the week,
I was thinking about one of the books that I read in 2018,
[Liars and Outliers: Enabling the Trust that Society Needs to Thrive](https://en.wikipedia.org/wiki/Liars_and_Outliers) by Bruce Schneier.
The book talk about trust system that we developed over the years
and is the backbone of our capacity to leave in society.
I believe that climbing is the ultimate team-building exercises
and everyone should do it regularly.

{% include figure.html filename="2019-03-23-trust-overhanging.jpg" alternative_text="Photo of climbing" caption="Climbing is for everyone." author="roya ann miller" licence="Unsplash License" original_url="https://unsplash.com/photos/G2QYE9czCEw" %}