---
layout: post
title: "Encontro de Hackers GNU: desafio estratégicos para o Software Livre - 2ª edição"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Na segunda edição do Encontro de Hackers GNU, participaram da mesa:</p>
<ul>
<li>Felipe Sanches,</li>
<li>Rodrigo da Silva,</li>
<li>Alexandre Oliva.</li>
</ul>
<figure>
<img src="/images/encontro_de_hackers_gnu_0.jpg" class="align-center" style="width:80.0%" />
</figure>
<p>Richard Stallman apareceu no encontro, ficou por um tempo e depois foi embora pois não entendia o que estava sendo dito (em português).</p>
<figure>
<img src="/images/encontro_de_hackers_gnu_1.jpg" class="align-center" style="width:80.0%" />
</figure>
<p>Durante o encontro o Felipe e o Rodrigo falaram sobre como foram introduzidos ao software livre, sua época universitária na USP e o envolvimento atual com a <a href="http://metamaquina.com.br/">Metamáquina</a>, uma impressora 3D que é hardware e software aberto.</p>
<p>Também foi falado na dificuldade de adquirir hardware compatível com GNU/Linux devido a dificuldade em encontrar as especificações técnicas que muitas vezes não são claras, trocas constantes de componentes sem mudança no código do produto e outros problemas.</p>
<p>Também foram resolvidas algumas dúvidas em relação a nomenclatura que a plateia tinha, assim espero, como por exemplo:</p>
<dl>
<dt>GNU</dt>
<dd><p>Refere-se ao sistema operacional livre iniciado por Richard Stallman. Como o kernel nunca ficou pronto pode estar se referindo apenas aos vários programas do Projeto GNU como emacs, gcc, gdb, glibc, coreutils, ...</p>
</dd>
<dt>Projeto GNU</dt>
<dd><p>Refere-se ao projeto iniciado por Richard Stallman para escrever um sistema operacional livre.</p>
</dd>
<dt>Linux</dt>
<dd><p>É um kernel escrito pelo Linus Torvalds. Ao juntar o kernel Linux com os programas do Projeto GNU temos um sistema operacional.</p>
<p>De forma errada é comum chamar de Linux o sistema operacional composto pelo kernel Linux e vários dos programas do projeto GNU.</p>
</dd>
<dt>Distribuições</dt>
<dd><p>É o empacotamento de um kernel com outros programas que muitas vezes conta com um gerenciador de pacotes que permite ao usuário instalar novos programas.</p>
<p>Debian, Fedora, Arch, Gentoo, Ubuntu, Mint, ... são distribuições.</p>
</dd>
</dl>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>FISL</p>
</div>
<div class="tags">
<p>FISL14</p>
</div>
<div class="comments">

</div>
