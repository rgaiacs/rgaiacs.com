---
layout: post
title: "Segunda rodada do treinamento da Software Carpentry"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Nessa quinta-feira aconteceu o segundo encontro do treinamento da <a href="http://software-carpentry.org">Software Carpentry</a> que estou fazendo. (<span data-role="download">Backup do pad aqui. &lt;swc-pad62.txt&gt;</span>)</p>
<div class="more">

</div>
<p>Nesse segundo encontro tivemos que responder a três perguntas sobre as atividades desenvolvidas nas últimas duas semanas:</p>
<ol>
<li><p>Quanto tempo foi utilizando para produzir o mapa conceitual (tanto para a concepção como para produção)?</p>
<p>Na média foram gastos 30 minutos.</p></li>
<li><p>O que você aprendeu sobre o tópico do exerício?</p>
<p>Muitos notaram que, muitas vezes, o que achamos ser muito básico requer conhecimentos ainda mais básicos e por isso é difícil preparar algo sem conhecer o seu público.</p></li>
<li><p>O que você aprendeu sobre mapas conceituais pelos comentários?</p>
<p>Sobre os mapas conceituais, alguns gostaram e pretende utilizá-los futuramente enquanto que outros não.</p>
<p>O nível de detalhamento a ser inserido em um mapa conceitual também é algo não muito claro.</p></li>
</ol>
<p>Depois dessa três perguntas, Greg perguntou que outras técnicas costumamos utilizar para organizar nosso conhecimento além de mapas conceituais, texto e slides.</p>
<p>Ele deixou para pesquisarmos sobre <a href="https://en.wikipedia.org/wiki/Understanding_by_Design">Understanding by Design</a>, Learning by Design, Backward Instructional Design que até onde eu entendi é um conjunto de técnicas voltadas para o ensino focado no objetivo dos alunos aprenderem o conteúdo e serem capazes de aplicá-lo.</p>
<p>Greg também tentou fazer a distinção entre &quot;summative assessment&quot; e &quot; formative assessment&quot;.</p>
<dl>
<dt><a href="https://en.wikipedia.org/wiki/Summative_assessment">summative assessment</a></dt>
<dd><p>São exames voltados para mensuarar o quanto um indivíduo sabe de algum assunto. É a base para rankings, ...</p>
</dd>
<dt><a href="https://en.wikipedia.org/wiki/Formative_assessment">formative assessment</a></dt>
<dd><p>São exames (normalmente qualitativos) voltados para reajustar as aulas seguintes com base no que os alunos aprenderam ou não. what I will teach you next, and what gaps do you personally have in your knowledge at this point.</p>
</dd>
</dl>
<p>Um tópico que surgiu foi porque &quot;summative assessment&quot; são utilizados. Greg disse que de tempos em tempos educadores retornam a essa pergunta sugerindo utilizar apenas &quot;formative assessment&quot; mas que nossa sociedade &quot;precisa&quot; de rankings e por isso logo os &quot;summative assessment&quot; retornam.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>educação,ensino,Software Carpentry</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
