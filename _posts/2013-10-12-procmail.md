---
layout: post
title: Procmail
author: "Raniere Silva"
categories: ftb
tags: [ftb]
image:
  feature: ftb.jpg
---
[Procmail](http://www.procmail.org/) é um agente entregador de email
(MDA) que pode ser utilizado como filtro localmente.[^1]

A seguir será apresentado como criar e testar regras a serem utilizadas
pelo procmail.

Regras
======

As regras seguem a seguinte sintaxe:

    :0 [flags] [ : [locallockfile] ]
    <zero ou mais condições (uma por linha)>
    <ação a ser executada (em apenas uma linha)>

As condições, que sempre começam com `*`, costuma ser expressas na forma
de uma expressão regular de forma que é satisfeita se a expressão
regular for encontrada no email.

Para filtrar os enviados de `algum-endereco@email.br` e salvá-los em
`algum-endereco` pode-se utilizar seguinte regra:

    :0
    * ^From.*algum-endereco@email.br
    algum-endereco

Testando
========

Para testar regras deve-se:

1.  salvar uma mensagem em um arquivo de texto,
2.  verificar se o cabeçalho satisfaz a regra que deseja-se testar,
3.  enviar/*pipe* o conteúdo do arquivo/mensagem para o procmail.

O último passo pode ser ser obtido utilizando:

    $ procmail rcfile < seu_arquivo.txt

No caso de desejar testar com o filtro com um
[mailbox](http://en.wikipedia.org/wiki/Email_box) será preciso utilizar
o formail:

    $ formail -s procmail < /tmp/inbox

E para verificar o resultado:

    $ mutt -f /tmp/inbox

Exemplo
=======

Considere esse email &lt;exemplo.email&gt;. Desejamos escrever uma regra
para filtrá-lo com base no `List-Id`.

Para testar:

-   salvamos o arquivo em `/tmp/exemplo.mail`,
-   criamos um diretório chamado `/tmp/mail`,
-   escrevemos o arquivo `proctest` contendo:

        PATH:/bin:/usr/bin
        VERBOSE=on

        MAILDIR=/tmp/mail

        :0:
        * ^List-Id:.*dev-tech-mathml.lists.mozilla.org
        mathml/

-   alteramos as permissões do arquivo `proctest`:

        $ chmod 0640 proctest

Finalmente, invocamos o `procmail`:

    $ procmail /tmp/proctest < /tmp/exemplo.mail

O log será impresso na tela de modo que será possível visualizar o que
ocorreu. Para visualizar o email:

    $ mutt -f /tmp/mail/mathml

Note que estamos utilizando o formato maildir.

Mais informações
================

Para maiores informações sugere-se as man pages fornecidas juntamente
com o procmail:

    $ man procmail
    $ man procmailrc
    $ man procmailex

**Referências**

[^1]: [Wikipedia - The Free Enclyclopedia -
    procmail](http://en.wikipedia.org/wiki/Procmail)
