---
layout: post
title: "Pirataria e livros eletronicos"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Ernesto escreveu um post intitulado &quot;<a href="http://torrentfreak.com/piracy-doesnt-harm-ebook-sales-publisher-says-130820/">Piracy Doesn’t Harm eBook Sales, Publisher Says</a>&quot; (<span data-role="download">cópia nesse servidor &lt;piracy-ebook-sales.html&gt;</span>) para o TorrentFrek a respeito de um pronunciamento da Springer afirmando que a pirataria e o compartilhamento de arquivos não afeta consideravelmente seu portifólio. A seguir apresento minha opinião do motivo disso ocorrer.</p>
<div class="more">

</div>
<h1 id="o-produto">O produto</h1>
<p>O principal produto da Springer são livros dos quais muitos são técnicos. Isso significa que o público interessado em seu produtos não é muito grande (principalmente para livros técnicos avançados) ao contrário de uma música ou vídeo que pode, a princípio, interessar a qualquer pessoa.</p>
<h1 id="o-uso">O uso</h1>
<p>Livros são consumidos de uma maneira diferente de música e vídeos (os conteúdos mais compartilhados). Enquanto que um vídeo será assistido apenas uma vez e uma faixa de música algumas centenas de vezes, a apreciação de ambas ocorre de forma passiva. Um livro técnico precisará ser consultado inúmeras vezes e consumido de forma ativa (adicionado anotações).</p>
<h1 id="a-versão-digital">A versão digital</h1>
<p>Nos anos 90 as músicas passaram a serem comercializadas na forma digital e nos anos 2000 foi a vez dos vídeos, sendo que em ambos os casos a qualidade do formato digital &quot;é superior ao analógico&quot; e muito mais fácil de armazenar. Apenas nos anos 2010 foi que os livros efetivamente começaram a serem comercializados na forma digital mas para livros técnicos a versão superior é muito inferior a versão impressa e não compatível com os hábitos dos usuários da versão impressa.</p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>copyright,e-coisas</p>
</div>
<div class="tags">
<p>ebooks</p>
</div>
<div class="comments">

</div>
