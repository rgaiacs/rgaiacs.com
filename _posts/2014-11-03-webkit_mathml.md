---
layout: post
title: WebKit and MathML
author: "Raniere Silva"
categories: ftb
tags: [ftb]
image:
  feature: ftb.jpg
---
Some interested bugs from WebKit related with MathML

-   [Implement the MathML Operator
    Dictionary](https://bugs.webkit.org/show_bug.cgi?id=99620)
-   [Add Support for mspace
    element](https://bugs.webkit.org/show_bug.cgi?id=115610)
-   [background colors do not apply to &lt;mo&gt; elements
    &lt;<https://bugs.webkit.org/show_bug.cgi?id=130470>&gt;]()
-   [The MathML mtext element should use a regular font by
    default](https://bugs.webkit.org/show_bug.cgi?id=41626)
-   [Implement mathcolor and mathbackground
    attributes](https://bugs.webkit.org/show_bug.cgi?id=41895)
-   [Implement MathML mfenced
    element](https://bugs.webkit.org/show_bug.cgi?id=42472)
-   [MathML merror element style
    adjustments](https://bugs.webkit.org/show_bug.cgi?id=43432)
