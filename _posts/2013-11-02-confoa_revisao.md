---
layout: post
title: "CONFOA - Revisão do Programa"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<div class="note">
<div class="admonition-title">
<p>Note</p>
</div>
<p>Você encontrará os slides em <a href="http://www.slideshare.net/ConfOA" class="uri">http://www.slideshare.net/ConfOA</a>.</p>
</div>
<p>Esta é uma revisão do CONFOA de 2013. O post é bastante longo (e foi escrito com o único objetivo registro histórico).</p>
<figure>
<img src="/images/confoa-pergunta.jpg" class="align-center" style="width:60.0%" />
</figure>
<div class="note">
<div class="admonition-title">
<p>Note</p>
</div>
<p>Agradecimentos ao Alexandre Abdo pela foto acima.</p>
</div>
<h1 id="reencontros">(Re)Encontros</h1>
<p>Como informado <a href="http://software-carpentry.org/blog/2013/10/the-state-of-open-science.html">nesse post da Software Carpentry</a> o lado social da ciência ainda é fortemente conduzido presencialmente em conferências e na CONFOA foi possível reencontrar vários amigos.</p>
<figure>
<img src="/images/confoa-amigos.jpg" class="align-center" style="width:60.0%" />
</figure>
<div class="note">
<div class="admonition-title">
<p>Note</p>
</div>
<p>Agradecimentos à Carolina Rossini pela foto acima.</p>
</div>
<h1 id="políticas-públicas-de-acesso-aberto">Políticas Públicas de Acesso Aberto</h1>
<p>Teve como moderador Hélio Nogueira da Cruz (Vice-Reitor da Universidade de São Paulo) e como painelistas:</p>
<ul>
<li>Carlos Henrique de Brito Cruz (Diretor Científico da FAPESP),</li>
<li>Heather Joseph (SPARC) apresentando &quot;Developing a framework for open access policies in the United States&quot; e</li>
<li>João Nuno Ferreira (Membro do Conselho Diretivo FCT IP) apresentando &quot;Política da FCT sobre Acesso Aberto&quot;&gt;</li>
</ul>
<p>Cruz apresentou um panorama das atividades da FAPESP (apoio a Scielo, projeto de Biblioteca Virtual, publicação da Revista FAPESP), falou sobre uma das motivações do acesso aberto (permitir ao contribuinte saber como SEU dinheiro está sendo gasto) e também sobre a nova política da FAPESP de requerer repositórios institucionais.</p>
<p>Tanto as apresentações de Joseph como de Ferreira foram muito boas e tiveram vários pontos em comum como o uso de licenças permissivas (e.g. CC-BY), redução do tempo de embargo (que hoje é de 12 meses) e do preço das publicações cujo custo tem diminuído mas essa redução não tem chegado aos consumidores.</p>
<p>Gostei muito de um dos slides da apresentação de Joseph que diz:</p>
<blockquote>
<p>&quot;É extremamente difícil colocar o gênio de volta na lâmpada uma vez que a lâmpada é verdadeiramente aberta.&quot; (Tradução do autor.)</p>
</blockquote>
<p>A FAPESP iniciou uma nova política de requerer repositórios institucionais nos quais os projetos financiados devem depositar seus trabalhos.</p>
<h1 id="comunicações-i---políticas-de-informação">Comunicações I - Políticas de Informação</h1>
<h1 id="direitos-autorais-e-acesso-aberto">Direitos Autorais e Acesso Aberto</h1>
<h1 id="comunicações-ii---percepção-do-acesso-aberto">Comunicações II - Percepção do Acesso Aberto</h1>
<h1 id="a-ciência-aberta-e-a-gestão-de-dados-de-pesquisa">A Ciência Aberta e a Gestão de Dados de Pesquisa</h1>
<h1 id="comunicações-iii---tecnologias-aplicadas-a-repositórios-científicos">Comunicações III - Tecnologias Aplicadas a Repositórios Científicos</h1>
<h1 id="comunicações-iv---revistas-e-editoras-de-acesso-aberto">Comunicações IV - Revistas e Editoras de Acesso Aberto</h1>
<h1 id="comunicações-v---repositórios-institucionais">Comunicações V - Repositórios Institucionais</h1>
<h1 id="oficina---gestão-de-propriedade-intelectual-para-o-acesso-aberto-ao-conhecimento-científico">Oficina - Gestão de Propriedade Intelectual para o Acesso Aberto ao Conhecimento Científico</h1>
<h1 id="oficina---gestão-de-dados-científicos-o-papel-das-bibliotecas">Oficina - Gestão de dados científicos: o papel das bibliotecas</h1>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>ciência aberta,educação</p>
</div>
<div class="tags">
<p>CONFOA</p>
</div>
<div class="comments">

</div>
