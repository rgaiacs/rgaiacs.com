---
layout: post
title: Python e NNTP
author: "Raniere Silva"
categories: ftb
tags: [ftb]
image:
  feature: ftb.jpg
---
Python possui uma biblioteca/módulo que implementa o lado do cliente do
protocolo NNTP[^1].

Neste post iremos mostrar como utilizar essa biblioteca para enviar
novas mensagens para um grupo local do Leafnode-2 (o Leafnode-2 foi
configurado em ../../../2013/05/19/leafnode2.

Grupos locais no Leafnode-2
===========================

Os grupos locais são informados no arquivo `/etc/leafnode/local.groups`.
Para a criação de grupos locais adicione linhas seguindo a sintaxe
abaixo:

    news.group.name<TAB>status<TAB>descrição

e.g., :

    local.test       y    Local Unmoderated Group

no arquivo `/etc/leafnode/local.groups` tomando o cuidado de utilizar
tab para separa os campos.

Recuperando informações dos grupos
==================================

Para recuperar informações do grupo `local.test` utilizamos:

    >>> import nntplib
    >>> s = nntplib.NNTP('localhost')
    >>> s.group('local.test')
    ('211 0 0 0 local.test group selected', 0, 0, 0, 'local.test')

A tupla de retorno é da forma `(response, count, first, last, name)`,
onde `count` é o número de artigos presentes no grupo, `first` é o
número do primeiro artigo no grupo, `last` é o número do último artigo e
`name` é o nome do grupo.

Caso o grupo não exista, um erro é levantado:

    >>> s.group('local.foo')
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
      File "/usr/local/lib/python3.3/nntplib.py", line 654, in group resp = self._shortcmd('GROUP ' + name)
      File "/usr/local/lib/python3.3/nntplib.py", line 504, in _shortcmd return self._getresp()
      File "/usr/local/lib/python3.3/nntplib.py", line 447, in _getresp
      raise NNTPTemporaryError(resp)
    nntplib.NNTPTemporaryError: 411 No such group

Formato de artigos
==================

Os artigos são compostos por um header e um body. O body corresponde ao
texto do artigo e deve terminar com uma linha com um único ponto. Já o
headers possui informações relacionadas com o artigo e os campos
obrigatórios são :

    From: your_username@your_host
    Newsgroups: my.newsgroup.1,my.newsgroup.2
    Subject: My_subject

Um artigo válido é :

    From: foo@bar.baz
    Newsgroups: local.test
    Subject: Testing

    This is a test
    .

Postando em um grupo
====================

Para postar um novo artigo no grupo `local.test` utilizamos:

    >>> import nntplib
    >>> s = nntplib.NNTP('localhost')
    >>> f = open('article.txt', 'rb')
    >>> s.post(f)
    '240 Article posted successfully.'

O argumento do método `post` deve ser um objeto arquivo aberto para
leitura binária e deve representar um novo artigo bem formatado,
incluindo os headers. Caso o novo artigo não esteja corretamente escrito
e o servidor recusá-lo, um erro é levantado.

Para verificar se o novo artigo realmente foi adicionado no grupo
utilizamos :

    >>> s.group('local.test')
    ('211 1 1 1 local.test group selected', 1, 1, 1, 'local.test')

Se o número de artigos tiver sido incrementado então o artigo foi
devidamente adicionado.

**Referencias**

[^1]: <http://docs.python.org/3.3/library/nntplib.html>
