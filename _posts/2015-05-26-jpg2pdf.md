---
layout: post
title: Convertendo JPG para PDF
author: "Raniere Silva"
categories: ftb
tags: [ftb]
image:
  feature: ftb.jpg
---
Algumas vezes você encontra um livro ou revista na internet como JPG e
você gostaria de criar um PDF com todos os JPGs para facilitar a
leitura. Nesse post você vai aprender como.

<div class="admonition">

Aviso

É provavel que o PDF seja maior que o conjunto de todos os JPGs. Talvez
você prefira criar um
[CBZ](https://en.wikipedia.org/wiki/Comic_book_archive).

</div>

ImageMagick
===========

[ImageMagick](http://www.imagemagick.org/) é uma biblioteca para
manipulação de imagens e pode converter arquivos JPG em PDF. Para essa
tarefa você deve :

    $ convert *.jpg colecao.pdf

<div class="admonition">

Aviso

Normalmente eu repado uma pequeno aumento na pixelização ao redor do
texto. Infelizmente não descobrir como evitar esse problema.

</div>

<div class="admonition">

Dica

Você talvez precise ordenar os arquivos JPG considerando números. Para
isso :

    $ convert $(ls -v *.jpg) colecao.pdf

</div>

<div class="admonition">

Dica

Pode ser que você tenha problemas de memória se tentar converter vários
JPG em um PDF. Para contornar esse problema, como informado em
<https://stackoverflow.com/questions/8700958/limiting-imagemagick-memory-use>
:

    $ export MAGICK_MEMORY_LIMIT=1024
    $ convert $(ls -v *.jpg) colecao.pdf

Para maiores informações sobre essa e outras variáveis de ambiente
visite <http://www.imagemagick.org/script/resources.php#environment>.

</div>

CBZ
===

No CBZ as imagens devem ser nomeados utilizando zeros, 0, à esquerda
para que todos os nomes possuam o mesmo tamanho. Por exemplo, você deve
utilizar

-   `01.jpg`
-   `02.jpg`
-   `03.jpg`
-   `04.jpg`
-   `05.jpg`
-   `06.jpg`
-   `07.jpg`
-   `08.jpg`
-   `09.jpg`
-   `10.jpg`

ao invés de

-   `1.jpg`
-   `2.jpg`
-   `3.jpg`
-   `4.jpg`
-   `5.jpg`
-   `6.jpg`
-   `7.jpg`
-   `8.jpg`
-   `9.jpg`
-   `10.jpg`

Com os nomes corretos, você só precisa comprimir todos os arquivos.
Provavelmente você vai utilizar :

    $ zip colecao.zip *.jpg
