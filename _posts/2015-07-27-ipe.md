---
layout: post
title: "S2 Ipê S2"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>I love when <a href="https://en.wikipedia.org/wiki/Handroanthus_impetiginosus">ipês</a> has flowers.</p>
<figure>
<img src="/images/photo-ipe1.jpg" class="align-center" style="width:80.0%" />
</figure>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>none</p>
</div>
<div class="tags">
<p>Campinas,Ipê</p>
</div>
<div class="comments">

</div>
