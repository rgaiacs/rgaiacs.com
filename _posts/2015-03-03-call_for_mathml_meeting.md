---
layout: post
title: "Call for MathML March Meeting"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Last month we didn't have our MathML month meeting but this month it will happen. Please reserve March 11th at 8pm UTC (<a href="http://www.timeanddate.com/worldclock/meetingdetails.html?year=2015&amp;month=3&amp;day=11&amp;hour=20&amp;min=0&amp;sec=0&amp;p1=240&amp;p2=137&amp;p3=179&amp;p4=233&amp;p5=195&amp;p6=37">check the time at your location here</a>) and add topics in the <a href="https://etherpad.mozilla.org/mathml-2015-03">PAD</a>.</p>
<div class="more">

</div>
<h1 id="google-summer-of-code">Google Summer of Code</h1>
<p>This year <a href="http://blog.queze.net/post/2015/03/03/Mozilla-not-accepted-for-Google-Summer-of-Code-2015">Mozilla wasn't accepted as for GSoC</a>. This is sad since we can't get someone to improve MathML support at Firefox but I see it as one opportunity to improve MathML on others open source projects like</p>
<ul>
<li><a href="https://wiki.gnome.org/Outreach/SummerOfCode/2015/Ideas">GNOME</a></li>
<li><a href="https://community.kde.org/GSoC/2015/Ideas">KDE</a></li>
<li><a href="https://wiki.documentfoundation.org/Development/GSoC/Ideas">LibreOffice</a></li>
<li><a href="https://www.mediawiki.org/wiki/Google_Summer_of_Code_2015">MediaWiki</a></li>
</ul>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>MathML,Mozilla</p>
</div>
<div class="tags">
<p>MathML meetings</p>
</div>
<div class="comments">

</div>
