---
layout: post
title: "GNU: 30 anos de luta"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p>Quem apresentou essa palestra foi o Alexandre Oliva.</p>
<figure>
<img src="/images/gnu__30_anos_de_luta_0.jpg" class="align-center" style="width:80.0%" />
</figure>
<p>Nesse ano o Projeto GNU completa 30 anos. Na palestra o Oliva falou sobre as origens do projeto, as quatro liberdades, ...</p>
<p>Muitas pessoas acreditam que o Projeto GNU é socialista ou contra o capitalista sendo isso uma grande mentira. O Projeto GNU é favorável ao capitalismo uma vez que permite o uso comercial dos seus softwares por qualquer pessoal. A única diferença com o capitalismo padrão é a possibilidade do acúmulo de capital por mais pessoas (se uma pessoa acumula R$100.000,00 ou dez pessoas acumulam R$10.000,00 o sistema continua acumulando R$100.000,00).</p>
<p>O software livre também possibilita o acúmulo de conhecimento, não nas mãos de uma única empresa mas nas mãos de uma comunidade da qual qualquer pessoa pode fazer parte.</p>
<p>Como todo trabalho hoje só é possível se estivermos apoiado nos ombros de gigantes e de suas contribuições que já encontram-se em domínio público (e.g., uma língua escrita) é errado privatizar o conhecimento gerado.</p>
<figure>
<img src="/images/gnu__30_anos_de_luta_1.jpg" class="align-center" style="width:80.0%" />
</figure>
<p>O Oliva também fez uma analogia entre saber dirigir e programar:</p>
<blockquote>
<p>Saber programar é como saber dirigir. Aprendemos a dirigir para sermos capazes de ir a onde desejarmos e, de forma análoga, aprendemos a programar para automatizar tarefas que temos interesse. É equivocada a ideia de que aprendemos a programar para viramos programadores pois é falsa a assertiva de que aprendemos a dirigir para sermos mecânicos.</p>
</blockquote>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>FISL</p>
</div>
<div class="tags">
<p>FISL14</p>
</div>
<div class="comments">

</div>
