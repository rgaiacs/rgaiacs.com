---
layout: post
title: "Sesriem"
author: "Raniere Silva"
categories: travel
tags: ["Africa", "Namibia", "Sossusvlei", "Sesriem"]
image:
  feature: 2019-09-29-sesriem.jpg
  author:  Raniere Silva
  licence: CC-BY
---

Sesriem is where we went for the sun rise after leave the camping site before 6am.

{% include figure.html filename="2019-09-29-sesriem-2.jpg" alternative_text="Photo of dune" caption="View from the top of Sesriem." author="Raniere Silva" licence="CC-BY" %}
