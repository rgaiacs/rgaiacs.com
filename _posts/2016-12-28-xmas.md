---
layout: post
title: "Manchester Christmas Market"
author: "Raniere Silva"
categories: blog
tags: [blog]
image:
  feature: blog.jpg
---
<div class="note">
<p>Note</p>
<p>This post was previous published at <code>blog.rgaiacs.com</code>. Some links might got broken during the conversion from reStructuredText to Markdown.</p>
</div>

<p><img src="/images/xmas-01.jpg" alt="image" class="align-center" style="width:80.0%" /></p>
<div class="more">

</div>
<p><img src="/images/xmas-02.jpg" alt="image" class="align-center" style="width:80.0%" /></p>
<p><img src="/images/xmas-03.jpg" alt="image" class="align-center" style="width:80.0%" /></p>
<p><img src="/images/xmas-04.jpg" alt="image" class="align-center" style="width:80.0%" /></p>
<div class="author">
<p>default</p>
</div>
<div class="categories">
<p>hiking</p>
</div>
<div class="tags">
<p>none</p>
</div>
<div class="comments">

</div>
