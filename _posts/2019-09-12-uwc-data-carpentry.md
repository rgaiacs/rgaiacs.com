---
layout: post
title: "Teaching Data Carpentry at the University of the Western Cape"
author: "Raniere Silva"
categories: teaching
tags: ["Africa", "South Africa", "Cape Town", "University of the Western Cape", "Data Carpentry"]
image:
  feature: 2019-09-12-uwc-data-carpentry.jpg
  author:  Raniere Silva
  licence: CC-BY
---

Thanks to the financial support of the [University of the Western Cape](https://uwc.ac.za) (UWC) eResearch Office,
Amieroh Abrahams,
Peter van Heusden,
Oghenere Salubi
and I
were able to teach a [Data Carpentry](https://datacarpentry.org/) workshop
on [10-11 September](https://uwc-eresearch.github.io/2019-09-10-UWC_Bellville/)
at UWC.

The workshop was hosted by the [South African National Biodiversity Institute](https://www.sanbi.org) (SANBI)
and had more than 30 learners
from all domains.

{% include figure.html filename="2019-09-12-uwc-data-carpentry-2.jpg" alternative_text="Photo of instructor teaching" caption="Amieroh teaching introduction to R." author="Raniere Silva" licence="CC-BY" %}

The workshop started with me teaching
data organization in spreadsheets
and
OpenRefine for data cleaning.
We were able to cover the full lessons
but I noticed that the lesson doesn't cover how use the "split multi-valued cells" function
and the data used during the lesson don't have any multi-valued cell
so I didn't teach it.
After lunch,
Amieroh led the introduction to R.
Amieroh did a amazing job explaining
object,
assign,
call,
function,
arguments,
options,
comments,
and
missing data.

{% include figure.html filename="2019-09-12-uwc-data-carpentry-3.jpg" alternative_text="Photo of instructor helping learner." caption="Oghenere helping learner during the workshop." author="Raniere Silva" licence="CC-BY" %}

The second day was dedicated to R.
In the morning,
Peter demonstrated how to work with data:
load,
subset,
reorder,
format,
summarize,
filter,
group
and many other operations using basic R and tidyverse.
In the afternoon,
I demonstrated how to use ggplot2 to create some data visualisations.

Despite some instalation issues (see appendix),
the overwall feedback from learners was very positive.
We advertised different groups that attendees could join to continue their learning journey
and we hope they facilitate others to gain more data skills.

{% include figure.html filename="2019-09-12-uwc-data-carpentry-4.jpg" alternative_text="Photo of the instructors." caption="From left to right: Raniere Silva, Oghenere Salubi, Amieroh Abrahams and Peter van Heusden." author="Raniere Silva" licence="CC-BY" %}

I want to thank Oghenere for all the local organisation without the two workshops
at the University of the Western Cape wouldn't happen.
I really appreciate Oghenere's time to show me UWC.

{% include figure.html filename="2019-09-12-uwc-data-carpentry-5.jpg" alternative_text="Photo of Raniere Silva." caption="Raniere teaching data visualisation." author="Oghenere Salubi" licence="CC-BY" %}

And start planning to attend [CarpentryCon 2021](https://carpentries.org/blog/2019/07/carpentrycon2020-theme-venue/)
that will take place in South Africa!

## Appendix

Some learners had machines running Ubuntu (I believe 18.04LTS)
and the version of R available from the package repository was 3.4.4.
This version of R is a bit old and didn't support the installation of tidyverse from CRAN.
Peter did a amazing job to help learners with this problem during the workshop.