---
layout: post
title: "101 Screts For Your Twenties: Loneliness"
author: "Raniere Silva"
categories: book
tags: ["101 Screts For Your Twenties"]
image:
  feature: 2019-04-12-into-the-wild.jpg
  author:  Grégoire Bertaud
  licence: Unsplash License
  original_url: https://unsplash.com/photos/wK_DZlAJJ_Q
---

Continue with my notes of Paul Angone's "101 Secrets For Your Twenties",
another cluster of secrets is about when "you're in a very intense season or place of questioning".
Paul wrote

> But don't allow loneliness to become isolation.
>
> Don't pull your head inside your shell thinking only you can protect yourself.
> Don't got on a dangerous Great Alaskan Adventurure to live off the land all by yourself.
> That's not a search for life, that's suicide.
>
> We need to know, and to be known.
>
> Invite a friend or two over for dinner.
> Talk, laugh once or twice---even if it's forced,
> and before the meal is over you might just notice your friends are chewing on the same questions you are.
> And at that moment of honest conversation,
> you will see light in the dark and dusty corners.