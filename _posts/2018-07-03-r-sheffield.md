---
layout: post
title: "R ❤️Python"
author: "Raniere Silva"
categories: blog
tags: [Memory]
image:
  feature: 2018-07-03-r-sheffield.jpg
  author: Mark Richards
  original_url: https://flic.kr/p/9Qjr5j
  licence: CC-BY-SA
---

[Anna Krystalli](https://twitter.com/annakrystalli) invited me
to talk a little bit about [reticulate](https://rstudio.github.io/reticulate/)
at the [Sheffield R Users Group](https://www.meetup.com/SheffieldR-Sheffield-R-Users-Group/)
on July 3.
From the description of the event

> This month we’ve got special guest Raniere Silva visiting all the way from Manchester and we'll focus the whole session on how R & Python can play nicely together. Whether you are an R developer that uses Python for some of your work or a member of data science team that uses both languages, reticulate can dramatically streamline your workflow!
>
> We'll kick off with a talk by Raniere in which he'll discuss the basics of Python and R interoperability through package reticulate.
>
> The rest of the session we leave open for YOUR examples of R ❤️ python workflows. Examples can be at any stage of development, even if (if not especially if) you are stuck on something. The idea is to get as broad a sample as possible of the many ways the two languages can interact and help out if we can.

All the content of my talk is available on [GitLab](https://rgaiacs.gitlab.io/sheffield-r-users-group-2018-07-03/).