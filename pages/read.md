---
layout: misc
title: Books that I read
library:
  - year: 2020
    books:
    - title: "Machines Like Me"
      author: "Ian McEwan"
    - title: "Stories We Need to Know"
      author: "Allan G. Hunter"
    - title: "Overcomplicated"
      author: "Samuel Arbesman"
    - title: "The Half-Life of Facts"
      author: "Samuel Arbesman"
    - title: "Childless Living"
      author: "Lisette Schuitemaker"
    - title: "Automating Inequality"
      author: "Virginia Eubanks"
    - title: "The Way Home"
      author: "Mark Boyl"
    - title: "Learning Agile"
      author: "Andrew Stellman, Jennifer Greene"
    - title: "Arnesto Modesto: The World's Most Ineffectual Time Traveler"
      author: "Darren Johnson"
    - title: "Start With Why"
      author: "Simon Sinek"
    - title: "Swipe Right"
      author: "Stephie Chapman"
    - title: "Michael O'Leary: A Life of Full Flight"
      author: "Alan Ruddock"
    - title: "The Oppenheimer Alternative"
      author: "Robert J. Sawyer"
    - title: "Just as You Are"
      author: "Kate Mathieson"
    - title: "The Friend Zone"
      author: "Abby Jimenez"
    - title: "Odds On"
      author: "Michael Crichton (Writing as John Lange)"
    - title: "The Bullet Journal Method"
      author: "Ryder Carroll"
    - title: "The Advocacy"
      author: "Melissa Fischer"
    - title: "Six Minutes of Grace"
      author: "Cameron D. Conway"
    - title: "The Healing Code"
      author: "Alex Loyd, Ben Johnson"
    - title: "Groups that Thrive"
      author: "Joel Comiskey, Jim Egli"
    - title: "Apex"
      author: "Ramez Naam"
    - title: "Crux"
      author: "Ramez Naam"
    - title: "Nexus"
      author: "Ramez Naam"
  - year: 2019
    books:
    - title: "You are the Beloved"
      author: "Henri J. M. Nouwen"
    - title: "Rich Dad Poor Dad"
      author: "Robert T. Kiyosaki"
    - title: "Tribe of Mentors: Short Life Advice from the Best in the World"
      author: "Timothy Ferriss"
    - title: "30 First Dates"
      author: "Stacey Wiedower"
    - title: "The Art of Discarding: How to get rid of clutter and find joy"
      author: "Nagisa Tatsumi, Angus Turvill"
    - title: "The Magic of Self-Respect: Awakening to your Own Awareness"
      author: "Osho"
    - title: "The Moment of Lift: How Empowering Women Changes the World"
      author: "Melinda Gates"
    - title: "When I Say No, I Feel Guilty"
      author: "Manuel J. Smith"
    - title: "Rest: Why You Get More Done When You Work Less"
      author: "Alex Soojung-Kim Pang"
    - title: "Goodbye, Things: On Minimalist Living"
      author: "Fumio Sasaki"
    - title: "The Psychology of Time Travel"
      author: "Kate Mascarenhas"
    - title: "Morning: How to make time: A manisfesto"
      author: "Allan Jenkins"
    - title: "Sorry I'm Late, I Didn't Want to Come"
      author: "Jessica Pan"
    - title: "Goodbye, Things"
      author: "Fumio Sasaki"
    - title: "Are We Nearly There Yet?"
      author: "Lucy Vine"
    - title: "Digital Minimalism"
      author: "Cal Newport"
    - title: "Dear Lily"
      author: "Drew Davies"
    - title: "It's Called a Breakup Because It's Broken: The Smart Girl's Breakup Buddy"
      author: "Greg Behredt, Amiira Ruotola-Behrendt"
    - title: "Next Time You Feel Suicidal? instead live and celebrate your life in your own way"
      author: "Osho"
    - title: "Essentialism: The Disciplined Pursuit of Less"
      author: "Greg McKeown"
    - title: "The Hidden Life of Trees: What They Feel, How They Communicate – Discoveries from a Secret World"
      author: "Peter Wohlleben"
    - title: "The First Fifteen Lives of Harry August"
      author: "Claire North"
    - title: "A New Earth"
      author: "Eckhart Tolle"
    - title: "Emotional Manipulation Tatics: 35 Covert Tactics Manipulators Use to Control Relationships"
      author: Tess Binder
    - title: "The Mastery of Love: A Practical Guide to the Art of Relationship: A Toltec Wisdom Series"
      author: don Miguel Ruiz, Janet Mills"
    - title: "Packing Light: Thoughts on Living Life with Less Baggage"
      author: "Allison Fallon"
    - title: "101 Questions You Need to Ask in Your Twenties (And Let's Be Honest, Your Thirties Too)"
      author: "Paul Angone"
    - title: "101 Secrets For Your Twenties"
      author: "Paul Angone"
    - title: "The Four Agreements: A Practical Guide to Personal Freedom"
      author: Don Miguel Ruiz
  - year: 2018
    books:
    - title: The First Fifteen Lives of Harry August
      author: Catherine Webb
    - title: Principles
      author: Ray Dalio
    - title: My Favourite Half-Night Stand
      author: Christina Lauren
    - title: "Family Constellations - A Practical Guide to Uncovering the Origins of Family Conflict"
      author: "Bert Hellinger, Joy Manne"
    - title: A Christmas Date
      author: Camilla Isley
    - title: I Have Never
      author: Camilla Isley
    - title: "The Art of Happiness - 10th Anniversary Edition"
      author: Dalai Lama, Howard C. Cutler
    - title: "Jony Ive - The Genius Behind Apple’s Greatest Products"
      author: Leander Kahney
    - title: "The One Device - The Secret History of the iPhone"
      author: Brian Merchant
    - title: Tudo Ou Nada
      author: Roberto Shinyashiki
    - title: "The Astonishing Power of Emotions: Let Your Feelings Be Your Guide"
      author: Esther Hicks, Jerry Hicks
    - title: The Law of Attraction
      author: Esther Hicks
    - title: The Amazing Power of Deliberate Intent
      author: Esther Hicks, Jerry Hicks
    - title: Accidental Tryst
      author: Natasha Boyd
    - title: Quantum Night
      author: Robert J. Sawyer
    - title: Present Over Perfect
      author: "Shauna Niequist, Brene Brown"
    - title: "The Vortex: Where the Law of Attraction Assembles All"
      author: "Esther Hicks, Jerry Hicks"
    - title: A Thousand Splendid Suns
      author: Khaled Hosseini
    - title: Perfect Match
      author: Zoe May
    - title: Happiness for Humans
      author: P. Z. Reizin
  - year: 2017
    books:
    - title: How Not to Fall
      author: Emily Foster
    - title: Boomerang
      author: Noelle August
    - title: "Dash And Lily's Book Of Dares"
      author: "Rachel Cohn, David Levithan"
    - title: "Crosstalk"
      author: "Connie Willis"
    - title: Another Day
      author: David Levithan
    - title: Every Day
      author: David Levithan
    - title: Code of Conduct
      author: Brad Thor
    - title: Thirteen Reasons Why
      author: Jay Asher
    - title: Life in Outer Space
      author: Melissa Keil
    - title: "The Hidden School: Return of the Peaceful Warrior"
      author: Dan Millman
    - title: All Our Wrong Todays
      author: Elan Mastai
    - title: Creativity, Inc.
      author: Ed Catmull
    - title: Change Agent
      author: Daniel Suarez
    - title: The Machine Stops
      author: E. M. Forster
    - title: Final Girls
      author: Mira Grant
    - title: Trojan Horse
      author: Mark Russinovich
    - title: A Little Something Different
      author: Sandy Hall
    - title: "The Art of Asking: How I learned to stop worrying and let people help"
      author: Amanda Palmer
    - title: Ready Player One
      author: Ernest Cline
    - title: "The Circle"
      author: "Dave Eggers"
    - title: Kill Process
      author: William Hertling
  - year: 2016
    books:
    - title: The Discussion Book
      author: "Stephen D. Brookfield and others"
    - title: "Chimera"
      author: "Mira Grant"
    - title: "Symbiont"
      author: "Mira Grant"
    - title: "Parasite"
      author: "Mira Grant"
    - title: Zero Day
      author: Mark Russinovich
    - title: To All the Boys I've Loved Before
      author: Jenny Han
    - title: One Week Girlfriend
      author: Monica Murphy
    - title: My Favourite Mistake
      author: Chelsea M. Cameron
    - title: My True Love Gave to Me
      author: Stephanie Perkins
    - title: Kindred Spirits
      author: Rainbow Rowell
---
<ul>
{% for shelf in page.library %}
<li>{{ shelf.year }}</li>
<ul>
{% for book in shelf.books %}
<li>"{{ book.title }}" by {{ book.author }}.</li>
{% endfor %}
</ul>
{% endfor %}
</ul>
