---
layout: misc
title: CV
---
{% assign github = "" %}
{% assign gitlab = "" %}
{% for item in site.data.settings.social %}
{% if item.icon == "github" %}
{% assign github = item %}
{% endif %}
{% if item.icon == "gitlab" %}
{% assign gitlab = item %}
{% endif %}
{% if item.icon == "twitter" %}
{% assign twitter = item %}
{% endif %}
{% endfor %}

<span class="outbox no-print" id="show-full-cv">
The full curriculum is available <a onclick="toggle_full();">here</a>.
</span>

<span class="outbox no-print" id="show-short-cv">
The short curriculum is available <a onclick="toggle_full();">here</a>.
</span>

# Raniere Gaia Costa da Silva
{:.print-only}

- <a href="mailto:{{ site.data.settings.author.email }}" class="menu-link"><i class="fa fa-envelope"
aria-hidden="true" target="_blank"></i> {{ site.data.settings.author.email }}</a>
- <a href="tel:{{ site.data.settings.author.phone }}" class="menu-link"><i class="fa fa-phone"
aria-hidden="true" target="_blank"></i> {{ site.data.settings.author.phone }}</a>
{:.print-only .social}

- <a href="{{ github.link }}" class="menu-link"><i class="fa fa-{{ github.icon }}"
aria-hidden="true" target="_blank"></i> {{ github.text }}</a>
- <a href="{{ gitlab.link }}" class="menu-link"><i class="fa fa-{{ gitlab.icon }}"
aria-hidden="true" target="_blank"></i> {{ gitlab.text }}</a>
- <a href="{{ twitter.link }}" class="menu-link"><i class="fa fa-{{ twitter.icon }}"
aria-hidden="true" target="_blank"></i> {{ twitter.text }}</a>
{:.print-only .social}

<div class="print-only line"></div>

## Professional Experience

-   University of Manchester

    -   Research Software Specialist, University of Manchester (February 2016 --
        May 2019)

        Worked as Community Officer for the Software Sustainability Institute.
        Led the organisation of various events, including two editions of Collaborations Workshop (2018 and 2019).
        Expanded the Software Sustainability Institute Fellowship Programme to 130 Fellows by cooperating,
        including as workshop instructor,
        with UK researchers from all domains.
        Developed, maintained and deployed the internal customer relationship management for the Fellowship Programme.

-   Web Developer Contractor (October 2015 -- February 2016)

    Full-stack software developer using React.

-   {:.full} Content Developer Contractor (April 2015 -- June 2015)

    Created content for a Web Developer introduction course targeting
    14--16 year old for [Jovem Hacker](http://jovemhacker.org/),
    a project sponsored by Campinas City Council.

-   Google Summer of Code

    -   Student (May 2014 -- August 2014)

        Created a math virtual keyboard in JavaScript for Firefox OS to help LaTeX users.

-   University of Campinas

    -   Undergraduate Research Assistant (August 2012 -- December 2013)

        Contributed a preprocessor heuristic for Interior Point Method in C to PCx.
        This research project was shortlisted by the Brazilian Operational Research Society
        for the best undergraduate project prize in 2013.

    -   Undergraduate Teaching Assistant (August 2011 -- July 2012)

        Helped students with their assignments: Numerical Calculus on the
        second half of 2011 and Combinatorial Optimisation on the first half
        of 2012.

## Volunteer Experience

-   The Carpentries

    -   Member of Executive Council (January 2018 -- February 2019)

        Solidified the merge of Software Carpentry and Data Carpentry.
        
    -   Member of Software Carpentry Steering Committee (January 2015 -- December 2016)

        Created an independent foundation to manage Software Carpentry.
        
     -  Instructor (July 2013 – present)

        Taught more than 15 workshops and co-authored the Bash, Git and Python
        lesson.
        
-   Google Summer of Code

    -   Mentor and Organisation Administrator for NumFOCUS (January 2015 – December 2016)

        The first two time that NumFOCUS applied and participated in Google Summer of
        Code, with 10 students in total, as umbrella organisation for their sponsored
        and affiliated projects. 

## Educational Background

-   {:.full} Earned on March 8, 2017

    University of California, Irvine on Coursera ([Academic English: Writing, a 5-course specialization](https://www.coursera.org/account/accomplishments/specialization/LGBDEN3NLJQL))

-   03/2009 - 07/2015

    University of Campinas, Brazil (B.A. in Apply Mathematics).

## Software Contribution

-   [lowfat](https://github.com/softwaresaved/lowfat),
    The Software Sustainability Institute's low effort Fellowship Administration Tool.

    Author and maintainer (April 2016 - May 2019)

-   [perprof](https://github.com/lpoo/perprof-py),
    a Python module for performance profiling.

    Co-author and Co-maintainer (November 2013 - December 2014)

-   [PCx](http://pages.cs.wisc.edu/~swright/PCx/),
    an interior-point predictor-corrector linear programming package.

    Maintainer (August 2012 - December 2014)

## Publication List

-   __Costa da Silva R__, Sufi S, Aragon Camarasa S (2019) Collaborations Workshop 2018 (CW18) Report – Culture Change, Productivity and Sustainability. Research Ideas and Outcomes 5: e30250. https://doi.org/10.3897/rio.5.e30250
-   Druskat, Stephan; __Silva, Raniere__. Improving research software citability with the Citation File Format. [9ª Conferência Luso-Brasileira sobre Acesso Aberto](http://confoa.rcaap.pt/2018/).
-   Sufi S, Nenadic A, __Silva R__, Duckles B, Simera I, et al. (2018) Ten simple rules for measuring the impact of workshops. PLOS Computational Biology 14(8): e1006191.https://doi.org/10.1371/journal.pcbi.1006191
-   __Silva, Raniere__ (2017): Track 1 Lightning Talk: Research Software in Brazil. figshare. Paper. https://doi.org/10.6084/m9.figshare.5328043.v1
-   Queiroz, Francisco; __Silva, Raniere__; Miller, Jonah; Brockhauser, Sandor; Fangohr, Hans (2017): Track 1 Paper: Good Usability Practices in Scientific Software Development. figshare. Paper. https://doi.org/10.6084/m9.figshare.5331814.v3
-   Soares Siqueira, A., __Costa da Silva, R.G.__ & Santos, L.-R., (2016). Perprof-py: A Python Package for Performance Profile of Mathematical Optimization Software. Journal of Open Research Software. 4(1), p.e12. DOI: http://doi.org/10.5334/jors.81
-   __Costa da Silva, Raniere Gaia__ and & Wang, Frédéric. [Firefox OS Web Apps for Science](http://ceur-ws.org/Vol-1186/paper-08.pdf).
    [Conferences on Intelligent Computer Mathematics - CICM 2014](http://www.cicm-conference.org/2014), July 2014.
-   __Raniere Gaia Costa da Silva__, Aurelio Ribeiro L. de Oliveira. [Implementação Eficiente Da Heurística De Reordenamento De Cuthill-Mckee Reversa](http://www.din.uem.br/sbpo/sbpo2013/pdf/arq0008.pdf).
    [XLV Simpósios Brasileiros de Pesquisa Operacional](http://www.din.uem.br/sbpo/sbpo2013/sumario/pic.html), September 2013.

## Poster List

-   Implementação paralela da heurística de reordenamento de Cuthill-McKee reversa.
    [XXII Congresso Interno de Iniciação Científica](http://www.prp.rei.unicamp.br/pibic/congressos/xxiicongresso/), October 2014.
-   [perprof-py -- A Python Package for Performance Profile](https://github.com/r-gaia-cs/perprof-brazopt-2014).
    [X Brazilian Workshop on Continuous Optimization](http://www.impa.br/opencms/pt/eventos/store/evento_1404), March, 2014.
-   Implementação eficiente da heurística de reordenamento de Cuthill-McKee Reversa.
    [XXI Congresso Interno de Iniciação Científica](http://www.prp.rei.unicamp.br/pibic/congressos/xxicongresso/), October 2013.

## Speaking and Teaching Experience

<!-- Author Full Name. Title of the work. Conference, Month Year. Note. -->

-   __Raniere Silva__. Introduction to Python.
    [Data Carpentry Pilot for Medicine Undergrad Students](https://anenadic.github.io/2019-03-16-manchester/), March 2019.
-   __Raniere Silva__. Introduction to Bash, Git & R.
    [Software Carpentry Workshop at Universidade Federal do Ceará](https://rgaiacs.github.io/2018-09-10-labvis/), September 2018.
-   __Raniere Silva__ (2018). [A Jupyter Enhancement Proposal Story](https://ep2018.europython.eu/conference/talks/a-jupyter-enhancement-proposal-story).
    [EuroPython](https://ep2018.europython.eu/), July 2018.
-   __Raniere Silva__, Caroline Jay. Can Research Software Engineers help measuring behaviour?
    [Measuring Behavior 2018](https://www.measuringbehavior.org/), June 2018.
-   __Raniere Silva__. Bring and Build Your Own Lesson 'Carpentries-style'.
    [CarpentryCon 2018](http://www.carpentrycon.org/), June 2018.
-   __Raniere Silva__, François Michonneau. Contributing on GitHub.
    [CarpentryCon 2018](http://www.carpentrycon.org/), June 2018.
-   __Raniere Silva__. Introduction to Bash.
    [Software Carpentry Workshop at the University of Manchester](https://anenadic.github.io/2018-05-15-swc-manchester/), May 2018.
-   Alice Harpole, Mateusz Kuzak, Céline Boudier, __Raniere Silva__.
    [Community teaching practices](https://ep2017.europython.eu/conference/talks/community-teaching-practices). [EuroPython 2017](https://ep2017.europython.eu/), July 2017.
-   __Raniere Silva__. Introduction to Python.
    [Software Carpentry Workshop at the Queen Mary University London](https://anenadic.github.io/2017-06-21-qmul/), June 2017.
-   __Raniere Silva__. Introduction to Git.
    [Software Carpentry Workshop at the University of Nottingham](https://anenadic.github.io/2016-10-05-nottingham/), October 2016.
-   {:.full} __Raniere Silva__. Introduction to Bash & Git.
    [Software Carpentry Workshop at Centro de Competência em Software Livre](https://rgaiacs.github.io/2016-05-27-ccsl/), May 2016.
-   {:.full} __Raniere Silva__. Introduction to Bash.
    [Software Carpentry Workshop at Universidade Estadual de Campinas (UNICAMP)](https://rgaiacs.github.io/2016-05-23-unicamp/), May 2016.
-   {:.full} __Raniere Silva__. Introduction to Bash & Git.
    [Software Carpentry Workshop at SciPy Latin America](https://rgaiacs.github.io/2016-05-16-scipyla/), May 2016.
-   {:.full} __Raniere Silva__. Introduction to Bash.
    [Software Carpentry Workshop at Universidade Federal do Ceará](https://github.com/r-gaia-cs/2015-06-19-labvis), June 2015.
-   {:.full} __Raniere Silva__. Introduction to Git.
    [Software Carpentry Workshop at Universidade Federal do Paraná](https://github.com/r-gaia-cs/2015-05-30-ufpr), May 2015.
-   __Raniere Silva__. Introduction to Git.
    [SciPy Latin America 2015](http://www.scipyla.org/conf/2015/), May 2015.
-   {:.full} __Raniere Silva__. Introduction to Git.
    Workshop organized by Student Association of Computer Science from University of Campinas, March 2015.
-   {:.full} __Raniere Silva__. Libraries and the cloud. 
    [University of São Paulo](http://bibliotecas.usp.br/), March 2015. As part of the celebration of the day of librarians.
-   {:.full} __Raniere Silva__. Introduction to Git.
    [Software Carpentry Workshop at Universidade Federal de Santa Catarina](https://github.com/dbarneche/2014-12-11-ufsc), December 2014.
-   __Raniere Silva__. Introduction to LaTeX.
    Workshop for [Student Chapter of Society of Petroleum Engineers at University of Campinas](http://www.speunicamp.org.br/?page_id=2252), September 2014.
-   {:.full} __Raniere Silva__. Introduction to LaTeX.
    [Workshop for IMECC](https://lpoo.github.io/workshop-2014-09-20-latex/), September 2014.
-   {:.full} __Raniere Silva__. Introduction to Python.
    [Software Carpentry Workshop at São Paulo University (Second Edition)](https://r-gaia-cs.github.io/2014-09-04-ccsl/), September 2014.
-   {:.full} __Raniere Silva__. Introduction to Python.
    [Software Carpentry Workshop at São Paulo University](https://r-gaia-cs.github.io/2014-09-01-ccsl/), September 2014.
-   {:.full} __Raniere Silva__. Introduction to Git.
    [Software Carpentry Workshop at Universidade Federal do Rio Grande do Sul](https://acviana.github.io/2014-08-28-ufrgs/), August 2014.
-   {:.full} __Raniere Silva__. Introduction to Git.
    [Software Carpentry Workshop at Open Science, Open Questions Conference](https://r-gaia-cs.github.io/2014-08-18-ufrj/), August 2014.
-   {:.full} __Raniere Silva__, Frédéric Wang. [Firefox OS Web Apps for Science](http://cermat.org/events/MathUI/14/proceedings/FirefoxOS-for-Science/slides/index.html).
    [MathUI 2014 - 9th Workshop on Mathematical User Interfaces](http://cermat.org/events/MathUI/14/), July 2014.
-   {:.full} __Raniere Silva__. Introduction to Git.
    [Software Carpentry Workshop at Universidade Federal do Rio Grande](https://r-gaia-cs.github.io/2014-05-12-furg/), May 2014.
-   {:.full} __Raniere Silva__. Mathematic at Digital Education.
    [15º Fórum Internacional de Software Livre](http://softwarelivre.org/fisl15), May 2014.
-   {:.full} __Raniere Silva__. (Open) Science and the Web.
    [15º Fórum Internacional de Software Livre](http://softwarelivre.org/fisl15), May 2014.
-   {:.full} __Raniere Silva__. Introduction to Git.
    [Workshop at IMECC](https://github.com/r-gaia-cs/2014-02-21-imecc), February 2014.
-   {:.full} __Raniere Silva__. Introduction to LaTeX.
    [VII Encontro Científico dos Pós-Graduandos do IMECC](http://www.ime.unicamp.br/~encpos/VIII_EnCPos/), October 2013.
-   {:.full} __Raniere Silva__. Implementação eficiente da heurística de reordenamento de Cuthill-McKee Reversa.
    [XLV Simpósio Brasileiro de Pesquisa Operacional](http://www.sbpo2013.ect.ufrn.br/), September 2013.
-   {:.full} __Raniere Silva__. Introduction to LaTeX.
    [II Semana da Matemática](http://www.ime.unicamp.br/~semat), September 2013.
-   {:.full} __Raniere Silva__. Monografias, Dissertações e Teses com Git e LaTeX.
    [14º Fórum Internacional de Software Livre](http://softwarelivre.org/fisl14), July 2013.
-   {:.full} __Raniere Silva__. Introduction to LaTeX.
    [I Semana da Matemática](http://www.ime.unicamp.br/~semat), September 2012.

## Organising Experience

<!-- Title of the event/meeting. Month Year. Note.

Roles:

-   Steering committee
-   General Chair
-   Local chair / Local Arrangements Committee
-   Finance Chair and Treasurer
-   Program Chair
-   Session chairs
-   Publications Chair
-   Publicity & Public Relations Chair
-   Registration Committee
-   Exhibits Committee
-->

-   [Collaborations Workshops](https://www.software.ac.uk/cw19). April 2019.

    Worked as General Chair and involved with finance, programme, publicity and registration.
-   [Collaborations Workshops](https://www.software.ac.uk/cw18). March 2018.

    Worked as General Chair and involved with finance, programme, publicity and registration.
-   [Collaborations Workshops](https://www.software.ac.uk/cw17). March 2017.

    Worked as Deputy General Chair and involved with programme, publicity and registration.
-   [Docker Containers for Reproducible Research Workshop](https://www.software.ac.uk/c4rr). June 2017.

    Worked as General Chair and involved with finance, programme, publicity and registration.
-   [Research Data Visualisation Workshop](http://www.software.ac.uk/rdvw). July 2016.

    Worked as General Chair and involved with finance, programme, publicity and registration.
-   [Software Carpentry Workshop at Centro de Competência em Software Livre](https://rgaiacs.github.io/2016-05-27-ccsl/). May 2016.
-   [Software Carpentry Workshop at Universidade Estadual de Campinas (UNICAMP)](https://rgaiacs.github.io/2016-05-23-unicamp/). May 2016.
-   [Software Carpentry Workshop at SciPy Latin America](https://rgaiacs.github.io/2016-05-16-scipyla/). May 2016.
-   [SciPy Latin America](http://scipyla.org/conf/2016/). May 2016.

    Worked as Co-General Chair and involved with finance, programme, publicity and registration.
-   [Software Carpentry Workshop at Universidade Federal do Paraná](https://github.com/r-gaia-cs/2015-05-30-ufpr). May 2015.
-   [Software Carpentry Workshop at São Paulo University (Second Edition)](https://r-gaia-cs.github.io/2014-09-04-ccsl/), September 2014.
-   [Software Carpentry Workshop at São Paulo University](https://r-gaia-cs.github.io/2014-09-01-ccsl/), September 2014.

<script>
{% include cv.js %}
</script>
