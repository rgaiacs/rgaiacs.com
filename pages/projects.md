---
layout: misc
title: Projects
---
You can check some projects at my
[GitLab account](https://gitlab.com/rgaiacs)
and
[GitHub account](https://github.com/rgaiacs).
