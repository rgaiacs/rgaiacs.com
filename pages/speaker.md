---
layout: misc
title: Speaker
---

## Bio

I work as the Community Officer for the [Software Sustainability Institute](https://www.software.ac.uk/).
When possible, I volunteer as a instructor/mentor at some local Python workshops,
including [Software Carpentry](https://software-carpentry.org/) and [Django Girls Manchester](https://djangogirls.org/manchester).
As part of my work and volunteer activities,
I'm always looking for ways to improve the relationship between novice developers and their more experience fellows.
In my spare time, I like to read sci-fi.

## Future talks

- Contributing on GitHub.
  [CarpentryCon 2018](http://www.carpentrycon.org/).
  2018-05-30.
- Bring and Build Your Own Lesson 'Carpentries-style'.
  [CarpentryCon 2018](http://www.carpentrycon.org/).
  2018-05-30.

## Past talks

In reverse order.

- Track 1 Lightning Talk: Research Software in Brazil.
  [Workshop on Sustainable Software for Science: Practice and Experiences (WSSSPE5.1)](http://wssspe.researchcomputing.org.uk/wssspe5-1/).
  2017-09-06.
- Track 1 Paper: Good Usability Practices in Scientific Software Development.
  [Workshop on Sustainable Software for Science: Practice and Experiences (WSSSPE5.1)](http://wssspe.researchcomputing.org.uk/wssspe5-1/).
  2017-09-06.

  Co-authored with Francisco Queiroz, Jonah Miller, Sandor Brockhauser and Hans Fangohr.
