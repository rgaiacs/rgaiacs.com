---
layout: misc
title: About
---

My name is Raniere Gaia Costa da Silva
but sometimes you will see me writing only Raniere Silva.
I worked for the [Software Sustainability Institute](https://www.software.ac.uk/)
as Community Officer between 2016 and 2019
and I have a bachelor's degree in applied mathematics
from the [University of Campinas](http://www.unicamp.br/) (Brazil).
I like to program, specially for **open** science.

In the last years I’m contributing with [Mozilla](http://mozilla.org) to
push science on the web by following the work of [Mozilla Science
Lab](http://mozillascience.org/) and participating at [Software
Carpentry](http://software-carpentry.org/) and [Mozilla MathML
Team](https://wiki.mozilla.org/MathML:Home_Page).

## More

I keep a [list of books that I read]({% link pages/read.md %})
and
a [list of places where I will be speaking]({% link pages/speaker.md %}).

## Profile Pictures

{% include profile.html filename="profile-2018.jpg" alternative_text="Profile picture" caption="2018" author="Raniere Silva" licence="CC-BY" %}

{% include profile.html filename="profile-2016.jpg" alternative_text="Profile picture" caption="2016-2018" author="Raniere Silva" licence="CC-BY" %}