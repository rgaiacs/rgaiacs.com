---
layout: misc
title: Contact
---

Except for phone calls,
I have notification disabled on all my personal devices
so might take a couple of hours for me to reply you.

{% for item in site.data.settings.social %}
{% if item.status == true %}
- <a href="{{ item.link }}" class="menu-link"><i class="fa fa-{{ item.icon }}"
aria-hidden="true" target="_blank"></i></a> {{ item.alt }}: <a href="{{ item.link }}" class="menu-link">{{ item.text }}</a>
{% endif %}
{% endfor %}

For members of gift economy,
you are welcome to contact me using

- <a href="https://www.trustroots.org/profile/rgaiacs" class="menu-link">Trustroots</a>
- <a href="https://www.bewelcome.org/members/rgaiacs">BeWelcome</a>