/* Javascript to toggle long lists */

showing_full_cv = true;

elements_to_toggle = document.getElementsByClassName("full")

toggle_full = function() {
    if (showing_full_cv == true) {
        document.getElementById("show-full-cv").style = "display:none;";
        document.getElementById("show-short-cv").style = "";
    }
    else {
        document.getElementById("show-full-cv").style = "";
        document.getElementById("show-short-cv").style = "display:none;";
    }

    for (var element of elements_to_toggle) {
        if (showing_full_cv == true) {
            element.style = "";
        }
        else {
            element.style = "display:none;";
        }
    }

    if (showing_full_cv == true) {
        showing_full_cv = false;
    }
    else {
        showing_full_cv = true;
    }
}

toggle_full();
