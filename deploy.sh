#!/bin/bash
#
# Usage:
#
#     $ ./ftb.sh --list-only
#     $ ./ftb.sh
docker-compose exec jekyll jekyll build
rsync -rvuhPt --delete $1 _site/ vps225507.vps.ovh.ca:~/rgaiacs.com/
